package com.navdy.service.library.util;

public class NetworkActivityTracker extends java.lang.Thread {
    private static final int LOGGING_INTERVAL = 60000;
    private static final com.navdy.service.library.util.NetworkActivityTracker sInstance = new com.navdy.service.library.util.NetworkActivityTracker();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.NetworkActivityTracker.class);
    private java.util.concurrent.atomic.AtomicLong bytesReceived = new java.util.concurrent.atomic.AtomicLong(0);
    private java.util.concurrent.atomic.AtomicLong bytesSent = new java.util.concurrent.atomic.AtomicLong(0);
    private double highestReceivedBandwidth = 0.0d;
    private double highestSentBandwidth = 0.0d;
    private long starttime = java.lang.System.currentTimeMillis();

    public static com.navdy.service.library.util.NetworkActivityTracker getInstance() {
        return sInstance;
    }

    private NetworkActivityTracker() {
    }

    public void addBytesSent(int bytes) {
        this.bytesSent.addAndGet((long) bytes);
    }

    public void addBytesReceived(int bytes) {
        this.bytesReceived.addAndGet((long) bytes);
    }

    public void run() {
        long lastBytesSent = this.bytesSent.get();
        long lastBytesReceived = this.bytesReceived.get();
        long lastTime = this.starttime;
        boolean endLoop = false;
        do {
            try {
                sleep(com.here.odnp.util.OdnpConstants.ONE_MINUTE_IN_MS);
            } catch (java.lang.InterruptedException e) {
                endLoop = true;
            }
            long currentByesSent = this.bytesSent.get();
            long currentBytesReceived = this.bytesReceived.get();
            long currentTime = java.lang.System.currentTimeMillis();
            long intervalBytesSent = currentByesSent - lastBytesSent;
            long intervalBytesReceived = currentBytesReceived - lastBytesReceived;
            long interval = currentTime - lastTime;
            long sinceStart = currentTime - this.starttime;
            double intervalSentBandwidth = (((double) intervalBytesSent) * 1000.0d) / ((double) interval);
            double intervalReceivedBandwidth = (((double) intervalBytesReceived) * 1000.0d) / ((double) interval);
            lastBytesSent = currentByesSent;
            lastBytesReceived = currentBytesReceived;
            lastTime = currentTime;
            this.highestSentBandwidth = java.lang.Math.max(this.highestSentBandwidth, intervalSentBandwidth);
            this.highestReceivedBandwidth = java.lang.Math.max(this.highestReceivedBandwidth, intervalReceivedBandwidth);
            if (intervalBytesSent > 0 || intervalBytesReceived > 0) {
                sLogger.i(java.lang.String.format("Total time: %dms interval: %dms sent: %d bytes received: %d bytes totalSent: %d bytes totalReceived: %d bytes sentBandwidth: %.2f bytes/sec receivedBandwidth: %.2f bytes/sec maxSentBandwidth: %.2f bytes/sec maxReceivedBandwidth: %.2f bytes/sec", new java.lang.Object[]{java.lang.Long.valueOf(sinceStart), java.lang.Long.valueOf(interval), java.lang.Long.valueOf(intervalBytesSent), java.lang.Long.valueOf(intervalBytesReceived), java.lang.Long.valueOf(currentByesSent), java.lang.Long.valueOf(currentBytesReceived), java.lang.Double.valueOf(intervalSentBandwidth), java.lang.Double.valueOf(intervalReceivedBandwidth), java.lang.Double.valueOf(this.highestSentBandwidth), java.lang.Double.valueOf(this.highestReceivedBandwidth)}));
                continue;
            }
        } while (!endLoop);
    }
}
