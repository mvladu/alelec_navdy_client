package com.navdy.service.library.util;

public abstract class Listenable<T extends com.navdy.service.library.util.Listenable.Listener> {
    private final java.lang.Object listenerLock = new java.lang.Object();
    private final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    protected java.util.HashSet<java.lang.ref.WeakReference<T>> mListeners = new java.util.HashSet<>();

    protected interface EventDispatcher<U extends com.navdy.service.library.util.Listenable, T extends com.navdy.service.library.util.Listenable.Listener> {
        void dispatchEvent(U u, T t);
    }

    public interface Listener {
    }

    public boolean addListener(T newListener) {
        if (newListener == null) {
            this.logger.e("attempted to add null listener");
            return false;
        }
        synchronized (this.listenerLock) {
            java.util.Iterator iterator = this.mListeners.iterator();
            while (iterator.hasNext()) {
                T listener = (com.navdy.service.library.util.Listenable.Listener) ((java.lang.ref.WeakReference) iterator.next()).get();
                if (listener == null) {
                    iterator.remove();
                } else if (listener.equals(newListener)) {
                    return false;
                }
            }
            this.mListeners.add(new java.lang.ref.WeakReference(newListener));
            return true;
        }
    }

    public boolean removeListener(T listenerToRemove) {
        boolean z = false;
        if (listenerToRemove != null) {
            synchronized (this.listenerLock) {
                java.util.Iterator iterator = this.mListeners.iterator();
                while (true) {
                    if (!iterator.hasNext()) {
                        break;
                    }
                    T listener = (com.navdy.service.library.util.Listenable.Listener) ((java.lang.ref.WeakReference) iterator.next()).get();
                    if (listener == null) {
                        iterator.remove();
                    } else if (listener.equals(listenerToRemove)) {
                        iterator.remove();
                        z = true;
                        break;
                    }
                }
            }
        } else {
            this.logger.e("attempted to remove null listener");
        }
        return z;
    }

    protected void dispatchToListeners(com.navdy.service.library.util.Listenable.EventDispatcher dispatcher) {
        java.util.HashSet<java.lang.ref.WeakReference<T>> clonedListeners;
        synchronized (this.listenerLock) {
            clonedListeners = (java.util.HashSet) this.mListeners.clone();
        }
        java.util.Iterator iterator = clonedListeners.iterator();
        while (iterator.hasNext()) {
            T listener = (com.navdy.service.library.util.Listenable.Listener) ((java.lang.ref.WeakReference) iterator.next()).get();
            if (listener == null) {
                iterator.remove();
            } else {
                dispatcher.dispatchEvent(this, listener);
            }
        }
    }
}
