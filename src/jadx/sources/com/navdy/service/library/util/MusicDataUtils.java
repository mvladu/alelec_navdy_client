package com.navdy.service.library.util;

public final class MusicDataUtils {
    public static final java.lang.String ALTERNATE_SEPARATOR = "_";
    private static final java.lang.String SEPARATOR = " // ";

    public static java.lang.String photoIdentifierFromTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
        if (trackInfo == null) {
            return null;
        }
        return trackInfo.index == null ? songIdentifierFromTrackInfo(trackInfo) : trackInfo.index.toString();
    }

    public static java.lang.String songIdentifierFromTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
        return songIdentifierFromTrackInfo(trackInfo, SEPARATOR);
    }

    public static java.lang.String songIdentifierFromTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo, java.lang.String separator) {
        if (trackInfo == null) {
            return null;
        }
        return (java.lang.String.valueOf(trackInfo.name) + separator + java.lang.String.valueOf(trackInfo.album) + separator + java.lang.String.valueOf(trackInfo.author)).replaceAll("[^A-Za-z0-9_]", "_");
    }

    public static java.lang.String songIdentifierFromArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse artworkResponse) {
        if (artworkResponse == null) {
            return null;
        }
        return (java.lang.String.valueOf(artworkResponse.name) + SEPARATOR + java.lang.String.valueOf(artworkResponse.album) + SEPARATOR + java.lang.String.valueOf(artworkResponse.author)).replaceAll("[^A-Za-z0-9_]", "_");
    }
}
