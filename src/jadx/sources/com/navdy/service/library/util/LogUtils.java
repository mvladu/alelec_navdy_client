package com.navdy.service.library.util;

public final class LogUtils {
    private static final int BUFFER_SIZE = 32768;
    private static final java.lang.String CRASH_DUMP_FILE = "crash.log";
    private static final java.lang.String CRASH_DUMP_STAGING = "staging";
    private static final byte[] CRLF = "\r\n".getBytes();
    private static com.navdy.service.library.util.LogUtils.RotatingLogFile[] LOG_FILES = {new com.navdy.service.library.util.LogUtils.RotatingLogFile("a.log", 4), new com.navdy.service.library.util.LogUtils.RotatingLogFile("obd.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("mfi.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("lighting.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("dial.log", 4), new com.navdy.service.library.util.LogUtils.RotatingLogFile("obdRaw.log", 2)};
    private static com.navdy.service.library.util.LogUtils.RotatingLogFile[] LOG_FILES_FOR_SNAPSHOT = {new com.navdy.service.library.util.LogUtils.RotatingLogFile("a.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("obd.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("mfi.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("lighting.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("dial.log", 2), new com.navdy.service.library.util.LogUtils.RotatingLogFile("obdRaw.log", 2)};
    private static final java.lang.String LOG_MARKER = "--------- beginning of main";
    private static final java.lang.String SYSTEM_LOG_PATH = ".logs";
    private static final java.lang.String TAG = com.navdy.service.library.util.LogUtils.class.getName();
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.LogUtils.class);

    private static class RotatingLogFile {
        int maxFiles;
        java.lang.String name;

        RotatingLogFile(java.lang.String name2, int maxFiles2) {
            this.name = name2;
            this.maxFiles = maxFiles2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0072 A[SYNTHETIC, Splitter:B:20:0x0072] */
    public static synchronized void dumpLog(android.content.Context context, java.lang.String fileName) throws java.lang.Throwable {
        synchronized (com.navdy.service.library.util.LogUtils.class) {
            if (new java.io.File(fileName).exists()) {
                com.navdy.service.library.util.IOUtils.deleteFile(context, fileName);
            }
            java.lang.Process process = null;
            java.io.BufferedOutputStream bufferedOutputStream = null;
            java.io.BufferedInputStream bufferedInputStream = null;
            java.io.InputStream processInputStream = null;
            byte[] buffer = new byte[32768];
            java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream(fileName);
            try {
                java.io.BufferedOutputStream bufferedOutputStream2 = new java.io.BufferedOutputStream(fileOutputStream, 32768);
                try {
                    process = java.lang.Runtime.getRuntime().exec(new java.lang.String[]{"logcat", "-d", "-v", "time"});
                    processInputStream = process.getInputStream();
                    java.io.BufferedInputStream bufferedInputStream2 = new java.io.BufferedInputStream(processInputStream);
                    while (true) {
                        try {
                            int len = bufferedInputStream2.read(buffer);
                            if (len <= 0) {
                                break;
                            }
                            bufferedOutputStream2.write(buffer, 0, len);
                        } catch (Throwable th) {
                            th = th;
                            bufferedInputStream = bufferedInputStream2;
                            bufferedOutputStream = bufferedOutputStream2;
                            com.navdy.service.library.util.IOUtils.fileSync(fileOutputStream);
                            com.navdy.service.library.util.IOUtils.closeStream(bufferedOutputStream);
                            com.navdy.service.library.util.IOUtils.closeStream(bufferedInputStream);
                            com.navdy.service.library.util.IOUtils.closeStream(fileOutputStream);
                            com.navdy.service.library.util.IOUtils.closeStream(processInputStream);
                            if (process != null) {
                            }
                            throw th;
                        }
                    }
                    bufferedOutputStream2.flush();
                    com.navdy.service.library.util.IOUtils.fileSync(fileOutputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(bufferedOutputStream2);
                    com.navdy.service.library.util.IOUtils.closeStream(bufferedInputStream2);
                    com.navdy.service.library.util.IOUtils.closeStream(fileOutputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(processInputStream);
                    if (process != null) {
                        try {
                            process.destroy();
                        } catch (Throwable e) {
                            sLogger.e(e);
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedOutputStream = bufferedOutputStream2;
                    com.navdy.service.library.util.IOUtils.fileSync(fileOutputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(bufferedOutputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(bufferedInputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(fileOutputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(processInputStream);
                    if (process != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                com.navdy.service.library.util.IOUtils.fileSync(fileOutputStream);
                com.navdy.service.library.util.IOUtils.closeStream(bufferedOutputStream);
                com.navdy.service.library.util.IOUtils.closeStream(bufferedInputStream);
                com.navdy.service.library.util.IOUtils.closeStream(fileOutputStream);
                com.navdy.service.library.util.IOUtils.closeStream(processInputStream);
                if (process != null) {
                    try {
                        process.destroy();
                    } catch (Throwable e2) {
                        sLogger.e(e2);
                    }
                }
                throw th;
            }
        }
        return;
    }

    public static synchronized void createCrashDump(android.content.Context context, java.lang.String path, java.lang.Thread crashThread, java.lang.Throwable throwable, boolean nativeCrash) {
        java.lang.String processName;
        java.lang.String stagingPath;
        synchronized (com.navdy.service.library.util.LogUtils.class) {
            java.io.FileOutputStream crashInfo = null;
            try {
                processName = com.navdy.service.library.util.SystemUtils.getProcessName(context, android.os.Process.myPid());
                android.util.Log.v(TAG, "creating CrashDump");
                java.io.File stagingDir = new java.io.File(path + java.io.File.separator + CRASH_DUMP_STAGING);
                if (stagingDir.exists()) {
                    com.navdy.service.library.util.IOUtils.deleteDirectory(context, stagingDir);
                }
                com.navdy.service.library.util.IOUtils.createDirectory(stagingDir);
                stagingPath = stagingDir.getAbsolutePath();
                java.io.FileOutputStream crashInfo2 = new java.io.FileOutputStream(stagingPath + java.io.File.separator + CRASH_DUMP_FILE);
                try {
                    writeCrashInfo(crashInfo2, throwable, processName, crashThread, nativeCrash);
                    com.navdy.service.library.util.IOUtils.fileSync(crashInfo2);
                    com.navdy.service.library.util.IOUtils.closeStream(crashInfo2);
                    crashInfo = null;
                    copyComprehensiveSystemLogs(stagingPath);
                } catch (Throwable th) {
                    th = th;
                    crashInfo = crashInfo2;
                    com.navdy.service.library.util.IOUtils.closeStream(crashInfo);
                    throw th;
                }
            } catch (Throwable th2) {
                t = th2;
            }
            java.lang.String tag = "";
            if (nativeCrash) {
                tag = "_native";
            }
            zipStaging(context, path + java.io.File.separator + processName + tag + "_crash_" + java.lang.System.currentTimeMillis() + ".zip", stagingPath);
            com.navdy.service.library.util.IOUtils.deleteDirectory(context, new java.io.File(stagingPath));
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    private static void writeCrashInfo(java.io.FileOutputStream fileOutputStream, java.lang.Throwable throwable, java.lang.String processName, java.lang.Thread crashThread, boolean nativeCrash) throws java.lang.Throwable {
        java.lang.Runtime runtime = java.lang.Runtime.getRuntime();
        java.io.BufferedOutputStream bufferedOutputStream = new java.io.BufferedOutputStream(fileOutputStream);
        bufferedOutputStream.write(("process: " + processName).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("time: " + java.util.Calendar.getInstance().getTime().toString()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("serial: " + android.os.Build.SERIAL).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("device: " + android.os.Build.DEVICE).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("model: " + android.os.Build.MODEL).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("hw: " + android.os.Build.HARDWARE).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("id: " + android.os.Build.ID).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("version: " + android.os.Build.VERSION.SDK_INT).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("bootloader: " + android.os.Build.BOOTLOADER).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("board: " + android.os.Build.BOARD).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("brand: " + android.os.Build.BRAND).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("manufacturer: " + android.os.Build.MANUFACTURER).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("cpu_abi: " + android.os.Build.CPU_ABI).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("cpu_abi2: " + android.os.Build.CPU_ABI2).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("processors: " + runtime.availableProcessors()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        if (nativeCrash) {
            bufferedOutputStream.write("------ native crash -----".getBytes());
        } else {
            bufferedOutputStream.write("------ crash -----".getBytes());
        }
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("thread name crashed:" + crashThread.getName()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        java.io.StringWriter sw = new java.io.StringWriter();
        java.io.PrintWriter pw = new com.navdy.service.library.log.FastPrintWriter((java.io.Writer) sw, false, 256);
        throwable.printStackTrace(pw);
        pw.flush();
        bufferedOutputStream.write(sw.toString().getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write("------ state -----".getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("total:" + runtime.totalMemory()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("free:" + runtime.freeMemory()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("max:" + runtime.maxMemory()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("native heap size: " + android.os.Debug.getNativeHeapSize()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("native heap allocated: " + android.os.Debug.getNativeHeapAllocatedSize()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("native heap free: " + android.os.Debug.getNativeHeapFreeSize()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        java.util.Map<java.lang.Thread, java.lang.StackTraceElement[]> stackTraces = java.lang.Thread.getAllStackTraces();
        bufferedOutputStream.write(("------ thread stackstraces count = " + stackTraces.size() + " -----").getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        for (java.util.Map.Entry<java.lang.Thread, java.lang.StackTraceElement[]> entry : stackTraces.entrySet()) {
            java.lang.Thread t = (java.lang.Thread) entry.getKey();
            bufferedOutputStream.write(("\"" + t.getName() + "\"").getBytes());
            bufferedOutputStream.write(CRLF);
            bufferedOutputStream.write(("    Thread.State: " + t.getState()).getBytes());
            bufferedOutputStream.write(CRLF);
            java.lang.StackTraceElement[] elements = (java.lang.StackTraceElement[]) entry.getValue();
            int length = elements.length;
            for (int i = 0; i < length; i++) {
                bufferedOutputStream.write(("       at " + elements[i]).getBytes());
                bufferedOutputStream.write(CRLF);
            }
            bufferedOutputStream.write(CRLF);
            bufferedOutputStream.write(CRLF);
        }
        bufferedOutputStream.flush();
    }

    private static java.lang.String getSystemLog() {
        java.lang.String contents = null;
        try {
            return com.navdy.service.library.util.IOUtils.convertFileToString((android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + java.io.File.separator + SYSTEM_LOG_PATH) + java.io.File.separator + "a.log");
        } catch (java.io.IOException e) {
            android.util.Log.i(TAG, "Failed to grab system log");
            return contents;
        }
    }

    public static java.lang.String systemLogStr(int bytes, java.lang.String str) {
        java.lang.String contents = getSystemLog();
        if (contents == null) {
            return contents;
        }
        int llen = contents.length();
        int end = llen;
        if (str != null) {
            int slen = str.length();
            int strIdx = contents.lastIndexOf(str);
            if (strIdx != -1) {
                end = java.lang.Math.min(strIdx + slen + 10, llen);
            }
        }
        return contents.substring(java.lang.Math.max(end - bytes, 0), end);
    }

    public static void copyComprehensiveSystemLogs(java.lang.String stagingPath) throws java.lang.Throwable {
        copySystemLogs(stagingPath, LOG_FILES);
    }

    public static void copySnapshotSystemLogs(java.lang.String stagingPath) throws java.lang.Throwable {
        copySystemLogs(stagingPath, LOG_FILES_FOR_SNAPSHOT);
    }

    public static void copySystemLogs(java.lang.String stagingPath, com.navdy.service.library.util.LogUtils.RotatingLogFile[] logFilesRequirement) throws java.lang.Throwable {
        java.lang.String srcPath = android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + java.io.File.separator + SYSTEM_LOG_PATH;
        for (com.navdy.service.library.util.LogUtils.RotatingLogFile logFile : logFilesRequirement) {
            java.lang.String filename = logFile.name;
            int count = logFile.maxFiles;
            for (int i = 0; i < count; i++) {
                java.lang.String logFileName = filename;
                if (i > 0) {
                    logFileName = logFileName + "." + i;
                }
                com.navdy.service.library.util.IOUtils.copyFile(srcPath + java.io.File.separator + logFileName, stagingPath + java.io.File.separator + logFileName);
            }
        }
    }

    public static void zipStaging(android.content.Context context, java.lang.String filePath, java.lang.String stagingPath) throws java.lang.Throwable {
        java.util.zip.ZipOutputStream zipOutputStream;
        android.util.Log.i(TAG, "zip started:" + filePath);
        java.io.FileOutputStream fos = null;
        java.io.FileInputStream fis = null;
        java.util.zip.ZipOutputStream zos = null;
        try {
            long l1 = android.os.SystemClock.elapsedRealtime();
            byte[] buffer = new byte[16384];
            java.io.FileOutputStream fos2 = new java.io.FileOutputStream(filePath);
            try {
                zipOutputStream = new java.util.zip.ZipOutputStream(fos2);
            } catch (Throwable th) {
                th = th;
                fos = fos2;
                com.navdy.service.library.util.IOUtils.closeStream(zos);
                com.navdy.service.library.util.IOUtils.closeStream(fis);
                com.navdy.service.library.util.IOUtils.closeStream(fos);
                throw th;
            }
            try {
                java.io.File[] files = new java.io.File(stagingPath).listFiles();
                if (files != null) {
                    int i = 0;
                    java.io.FileInputStream fileInputStream = null;
                    while (i < files.length) {
                        try {
                            fis = new java.io.FileInputStream(files[i]);
                            zipOutputStream.putNextEntry(new java.util.zip.ZipEntry(files[i].getName()));
                            while (true) {
                                int length = fis.read(buffer);
                                if (length <= 0) {
                                    break;
                                }
                                zipOutputStream.write(buffer, 0, length);
                            }
                            zipOutputStream.closeEntry();
                            com.navdy.service.library.util.IOUtils.closeStream(fis);
                            i++;
                            fileInputStream = null;
                        } catch (Throwable th2) {
                            th = th2;
                            zos = zipOutputStream;
                            fis = fileInputStream;
                            fos = fos2;
                            com.navdy.service.library.util.IOUtils.closeStream(zos);
                            com.navdy.service.library.util.IOUtils.closeStream(fis);
                            com.navdy.service.library.util.IOUtils.closeStream(fos);
                            throw th;
                        }
                    }
                    fis = fileInputStream;
                }
                android.util.Log.i(TAG, "zip finished:" + filePath + " time:" + (android.os.SystemClock.elapsedRealtime() - l1));
                com.navdy.service.library.util.IOUtils.closeStream(zipOutputStream);
                com.navdy.service.library.util.IOUtils.closeStream(fis);
                com.navdy.service.library.util.IOUtils.closeStream(fos2);
            } catch (Throwable th3) {
                th = th3;
                zos = zipOutputStream;
                fos = fos2;
                com.navdy.service.library.util.IOUtils.closeStream(zos);
                com.navdy.service.library.util.IOUtils.closeStream(fis);
                com.navdy.service.library.util.IOUtils.closeStream(fos);
                throw th;
            }
        } catch (Throwable th4) {
            t = th4;
            com.navdy.service.library.util.IOUtils.closeStream(fos);
            com.navdy.service.library.util.IOUtils.deleteFile(context, filePath);
            android.util.Log.i(TAG, "zip error", t);
            throw t;
        }
    }

    public static synchronized void createKernelCrashDump(android.content.Context context, java.lang.String path, java.lang.String[] kernelCrashFiles) {
        synchronized (com.navdy.service.library.util.LogUtils.class) {
            try {
                java.io.File stagingDir = new java.io.File(path + java.io.File.separator + CRASH_DUMP_STAGING);
                if (stagingDir.exists()) {
                    com.navdy.service.library.util.IOUtils.deleteDirectory(context, stagingDir);
                }
                com.navdy.service.library.util.IOUtils.createDirectory(stagingDir);
                int count = 0;
                java.lang.String stagingPath = stagingDir.getAbsolutePath();
                for (java.lang.String str : kernelCrashFiles) {
                    if (com.navdy.service.library.util.IOUtils.copyFile(str, stagingPath + java.io.File.separator + new java.io.File(str).getName())) {
                        sLogger.v("copied:" + str);
                        count++;
                    }
                }
                if (count > 0) {
                    java.lang.String fileName = path + java.io.File.separator + "kernel_crash_" + java.lang.System.currentTimeMillis() + ".zip";
                    zipStaging(context, fileName, stagingPath);
                    sLogger.v("kernel dump created:" + fileName);
                } else {
                    sLogger.v("no kernel crash files");
                }
                com.navdy.service.library.util.IOUtils.deleteDirectory(context, new java.io.File(stagingPath));
            } catch (Throwable t) {
                android.util.Log.e(TAG, "createKernelCrashDump", t);
            }
        }
        return;
    }
}
