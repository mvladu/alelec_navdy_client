package com.navdy.service.library.task;

public final class TaskManager {
    private static final int INITIAL_CAPACITY = 10;
    private static final boolean VERBOSE = false;
    private static java.util.concurrent.atomic.AtomicLong orderCounter = new java.util.concurrent.atomic.AtomicLong(1);
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.task.TaskManager.class);
    private static final java.util.Comparator<java.lang.Runnable> sPriorityComparator = new com.navdy.service.library.task.TaskManager.PriorityTaskComparator(null);
    private static final com.navdy.service.library.task.TaskManager sSingleton = new com.navdy.service.library.task.TaskManager();
    private static final java.util.concurrent.atomic.AtomicInteger threadNameCounter = new java.util.concurrent.atomic.AtomicInteger(0);
    private final android.util.SparseArray<java.util.concurrent.ExecutorService> executors = new android.util.SparseArray<>();
    private boolean initialized;

    class Anon1 implements java.util.concurrent.ThreadFactory {
        Anon1() {
        }

        public java.lang.Thread newThread(java.lang.Runnable r) {
            java.lang.Thread thread = new java.lang.Thread(r);
            thread.setName("navdy_" + com.navdy.service.library.task.TaskManager.threadNameCounter.incrementAndGet());
            thread.setDaemon(true);
            return thread;
        }
    }

    private static class PriorityRunnable implements java.lang.Runnable {
        final long order;
        final int priority;
        final java.lang.Runnable runnable;

        PriorityRunnable(java.lang.Runnable runnable2, com.navdy.service.library.task.TaskManager.TaskPriority priority2, long order2) {
            this.runnable = runnable2;
            this.priority = priority2.getValue();
            this.order = order2;
        }

        public void run() {
            try {
                this.runnable.run();
            } catch (Throwable ex) {
                com.navdy.service.library.task.TaskManager.sLogger.e("TaskManager:", ex);
            }
        }
    }

    private static final class PriorityTask<T> extends java.util.concurrent.FutureTask<T> implements java.lang.Comparable<com.navdy.service.library.task.TaskManager.PriorityTask<T>> {
        private final com.navdy.service.library.task.TaskManager.PriorityRunnable priorityRunnable;

        public PriorityTask(com.navdy.service.library.task.TaskManager.PriorityRunnable runnable, java.util.concurrent.Callable<T> tCallable) {
            super(tCallable);
            this.priorityRunnable = runnable;
        }

        public PriorityTask(com.navdy.service.library.task.TaskManager.PriorityRunnable runnable, T result) {
            super(runnable, result);
            this.priorityRunnable = runnable;
        }

        public int compareTo(com.navdy.service.library.task.TaskManager.PriorityTask<T> o) {
            int diff = this.priorityRunnable.priority - o.priorityRunnable.priority;
            if (diff == 0) {
                return (int) (this.priorityRunnable.order - o.priorityRunnable.order);
            }
            return diff;
        }

        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.navdy.service.library.task.TaskManager.PriorityTask)) {
                return false;
            }
            com.navdy.service.library.task.TaskManager.PriorityTask other = (com.navdy.service.library.task.TaskManager.PriorityTask) obj;
            if (this.priorityRunnable.priority == other.priorityRunnable.priority && this.priorityRunnable.order == other.priorityRunnable.order) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (int) this.priorityRunnable.order;
        }
    }

    private static class PriorityTaskComparator implements java.util.Comparator<java.lang.Runnable>, java.io.Serializable {
        private PriorityTaskComparator() {
        }

        /* synthetic */ PriorityTaskComparator(com.navdy.service.library.task.TaskManager.Anon1 x0) {
            this();
        }

        public int compare(java.lang.Runnable left, java.lang.Runnable right) {
            return ((com.navdy.service.library.task.TaskManager.PriorityTask) left).compareTo((com.navdy.service.library.task.TaskManager.PriorityTask) right);
        }
    }

    public static class TaskManagerExecutor extends java.util.concurrent.ThreadPoolExecutor {
        TaskManagerExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, java.util.concurrent.TimeUnit unit, java.util.concurrent.BlockingQueue<java.lang.Runnable> workQueue, java.util.concurrent.ThreadFactory threadFactory) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
        }

        protected <T> java.util.concurrent.RunnableFuture<T> newTaskFor(java.lang.Runnable runnable, T value) {
            return new com.navdy.service.library.task.TaskManager.PriorityTask((com.navdy.service.library.task.TaskManager.PriorityRunnable) runnable, value);
        }
    }

    public enum TaskPriority {
        LOW(1),
        NORMAL(2),
        HIGH(3);
        
        private final int val;

        private TaskPriority(int val2) {
            this.val = val2;
        }

        public int getValue() {
            return this.val;
        }
    }

    public static com.navdy.service.library.task.TaskManager getInstance() {
        return sSingleton;
    }

    private TaskManager() {
    }

    public void init() {
        if (this.initialized) {
            throw new java.lang.IllegalStateException("already initialized");
        }
        this.initialized = true;
    }

    public void addTaskQueue(int queueId, int poolSize) {
        if (this.initialized) {
            throw new java.lang.IllegalStateException("already initialized");
        } else if (this.executors.get(queueId) != null) {
            throw new java.lang.IllegalArgumentException("already exists");
        } else {
            this.executors.put(queueId, createExecutor(poolSize));
        }
    }

    public java.util.concurrent.ExecutorService getExecutor(int queueId) {
        return (java.util.concurrent.ExecutorService) this.executors.get(queueId);
    }

    private java.util.concurrent.ExecutorService createExecutor(int poolSize) {
        return new com.navdy.service.library.task.TaskManager.TaskManagerExecutor(poolSize, poolSize, 60, java.util.concurrent.TimeUnit.SECONDS, new java.util.concurrent.PriorityBlockingQueue(10, sPriorityComparator), new com.navdy.service.library.task.TaskManager.Anon1());
    }

    public java.util.concurrent.Future execute(java.lang.Runnable runnable, int queueId, com.navdy.service.library.task.TaskManager.TaskPriority taskPriority) {
        if (!this.initialized) {
            sLogger.w("Task manager not initialized", new java.lang.Throwable());
            return null;
        } else if (runnable != null) {
            return ((java.util.concurrent.ExecutorService) this.executors.get(queueId)).submit(new com.navdy.service.library.task.TaskManager.PriorityRunnable(runnable, taskPriority, orderCounter.getAndIncrement()));
        } else {
            throw new java.lang.IllegalArgumentException();
        }
    }

    public java.util.concurrent.Future execute(java.lang.Runnable runnable, int queueId) {
        return execute(runnable, queueId, com.navdy.service.library.task.TaskManager.TaskPriority.NORMAL);
    }
}
