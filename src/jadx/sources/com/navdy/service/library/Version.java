package com.navdy.service.library;

public class Version {
    public static final com.navdy.service.library.Version PROTOCOL_VERSION = new com.navdy.service.library.Version(1, 0);
    public final int majorVersion;
    public final int minorVersion;

    public Version(int majorVersion2, int minorVersion2) {
        this.majorVersion = majorVersion2;
        this.minorVersion = minorVersion2;
    }

    public java.lang.String toString() {
        return this.majorVersion + "." + this.minorVersion;
    }
}
