package com.navdy.service.library.device.connection;

public abstract class ConnectionInfo {
    private static final int DEVICE_ID_GROUP = 1;
    public static final com.navdy.service.library.util.RuntimeTypeAdapterFactory<com.navdy.service.library.device.connection.ConnectionInfo> connectionInfoAdapter = com.navdy.service.library.util.RuntimeTypeAdapterFactory.of(com.navdy.service.library.device.connection.ConnectionInfo.class).registerSubtype(com.navdy.service.library.device.connection.BTConnectionInfo.class, "BT").registerSubtype(com.navdy.service.library.device.connection.TCPConnectionInfo.class, "TCP").registerSubtypeAlias(com.navdy.service.library.device.connection.BTConnectionInfo.class, "EA");
    private static final java.util.regex.Pattern deviceIdFromServiceName = java.util.regex.Pattern.compile("Navdy\\s(.*)");
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.connection.ConnectionInfo.class);
    private final com.navdy.service.library.device.connection.ConnectionType connectionType;
    @com.google.gson.annotations.SerializedName("deviceId")
    protected final com.navdy.service.library.device.NavdyDeviceId mDeviceId;

    public abstract com.navdy.service.library.device.connection.ServiceAddress getAddress();

    public static boolean isValidNavdyServiceInfo(android.net.nsd.NsdServiceInfo serviceInfo) {
        if (!com.navdy.service.library.device.connection.ConnectionType.getServiceTypes().contains(serviceInfo.getServiceType())) {
            sLogger.d("Unknown service type: " + serviceInfo.getServiceType());
            return false;
        } else if (deviceIdFromServiceName.matcher(serviceInfo.getServiceName()).matches()) {
            return true;
        } else {
            sLogger.d("Not a Navdy: " + serviceInfo.getServiceName());
            return false;
        }
    }

    protected ConnectionInfo(com.navdy.service.library.device.connection.ConnectionType connectionType2) {
        this.connectionType = connectionType2;
        this.mDeviceId = com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID;
    }

    public static com.navdy.service.library.device.connection.ConnectionInfo fromConnection(com.navdy.service.library.device.connection.Connection connection) {
        if (connection == null) {
            return null;
        }
        return connection.getConnectionInfo();
    }

    public ConnectionInfo(com.navdy.service.library.device.NavdyDeviceId id, com.navdy.service.library.device.connection.ConnectionType connectionType2) {
        if (id == null) {
            throw new java.lang.IllegalArgumentException("Device id required.");
        }
        this.mDeviceId = id;
        this.connectionType = connectionType2;
    }

    public ConnectionInfo(android.net.nsd.NsdServiceInfo serviceInfo) {
        if (serviceInfo == null) {
            throw new java.lang.IllegalArgumentException("serviceInfo can't be null");
        }
        java.lang.String deviceIdString = parseDeviceIdStringFromServiceInfo(serviceInfo);
        if (deviceIdString == null) {
            throw new java.lang.IllegalArgumentException("Service info doesn't contain valid device id");
        }
        this.connectionType = com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF;
        this.mDeviceId = new com.navdy.service.library.device.NavdyDeviceId(deviceIdString);
    }

    public static com.navdy.service.library.device.connection.ConnectionInfo fromServiceInfo(android.net.nsd.NsdServiceInfo serviceInfo) {
        java.lang.String serviceType = serviceInfo.getServiceType();
        if (serviceType.startsWith(".")) {
            serviceType = serviceType.substring(1);
        }
        if (!serviceType.endsWith(".")) {
            serviceType = serviceType + ".";
        }
        com.navdy.service.library.device.connection.ConnectionType type = com.navdy.service.library.device.connection.ConnectionType.fromServiceType(serviceType);
        if (type == null) {
            return null;
        }
        switch (type) {
            case TCP_PROTOBUF:
                return new com.navdy.service.library.device.connection.TCPConnectionInfo(serviceInfo);
            default:
                sLogger.e("No connectioninfo from mDNS info for type: " + type);
                return null;
        }
    }

    public com.navdy.service.library.device.NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }

    public com.navdy.service.library.device.connection.ConnectionType getType() {
        return this.connectionType;
    }

    public java.lang.String toString() {
        return getType() + " id: " + this.mDeviceId + " " + getAddress();
    }

    protected java.lang.String parseDeviceIdStringFromServiceInfo(android.net.nsd.NsdServiceInfo serviceInfo) {
        java.lang.String serviceName = serviceInfo.getServiceName().replace("\\032", " ");
        if (android.text.TextUtils.isEmpty(serviceName)) {
            return null;
        }
        java.util.regex.Matcher matcher = deviceIdFromServiceName.matcher(serviceName);
        if (!matcher.matches() || matcher.groupCount() < 1) {
            return null;
        }
        return matcher.group(1);
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return this.mDeviceId.equals(((com.navdy.service.library.device.connection.ConnectionInfo) o).mDeviceId);
    }

    public int hashCode() {
        return this.mDeviceId.hashCode();
    }
}
