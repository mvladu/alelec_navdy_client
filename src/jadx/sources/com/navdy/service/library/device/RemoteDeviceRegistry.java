package com.navdy.service.library.device;

public class RemoteDeviceRegistry extends com.navdy.service.library.util.Listenable<com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener> implements com.navdy.service.library.device.discovery.RemoteDeviceScanner.Listener {
    public static final int MAX_PAIRED_DEVICES = 5;
    public static final java.lang.String PREFS_FILE_DEVICE_REGISTRY = "DeviceRegistry";
    public static final java.lang.String PREFS_KEY_DEFAULT_CONNECTION_INFO = "DefaultConnectionInfo";
    public static final java.lang.String PREFS_KEY_PAIRED_CONNECTION_INFOS = "PairedConnectionInfos";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.RemoteDeviceRegistry.class);
    private static com.navdy.service.library.device.RemoteDeviceRegistry sRemoteDeviceRegistry;
    protected android.content.Context mContext;
    protected com.google.gson.Gson mGson = new com.google.gson.GsonBuilder().registerTypeAdapterFactory(com.navdy.service.library.device.connection.ConnectionInfo.connectionInfoAdapter).create();
    protected java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> mKnownConnectionInfo = new java.util.HashSet();
    protected java.util.HashSet<com.navdy.service.library.device.discovery.RemoteDeviceScanner> mRemoteDeviceScanners = new java.util.HashSet<>();
    protected android.content.SharedPreferences mSharedPrefs;
    private java.util.ArrayList<com.navdy.service.library.device.connection.ConnectionInfo> pairedConnections;

    class Anon1 implements com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListEventDispatcher {
        Anon1() {
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDeviceRegistry source, com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener listener) {
            listener.onDeviceListChanged(new java.util.HashSet<>(com.navdy.service.library.device.RemoteDeviceRegistry.this.mKnownConnectionInfo));
        }
    }

    class Anon2 extends com.google.gson.reflect.TypeToken<java.util.List<com.navdy.service.library.device.connection.ConnectionInfo>> {
        Anon2() {
        }
    }

    class Anon3 extends com.google.gson.reflect.TypeToken<java.util.List<com.navdy.service.library.device.connection.ConnectionInfo>> {
        Anon3() {
        }
    }

    protected interface DeviceListEventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.service.library.device.RemoteDeviceRegistry, com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener> {
    }

    public interface DeviceListUpdatedListener extends com.navdy.service.library.util.Listenable.Listener {
        void onDeviceListChanged(java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> set);
    }

    public static com.navdy.service.library.device.RemoteDeviceRegistry getInstance(android.content.Context c) {
        if (sRemoteDeviceRegistry == null) {
            sRemoteDeviceRegistry = new com.navdy.service.library.device.RemoteDeviceRegistry(c.getApplicationContext());
        }
        return sRemoteDeviceRegistry;
    }

    private RemoteDeviceRegistry(android.content.Context context) {
        this.mContext = context;
        loadPairedConnections();
    }

    public java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> getKnownConnectionInfo() {
        return this.mKnownConnectionInfo;
    }

    public void addRemoteDeviceScanner(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        scanner.addListener(this);
        this.mRemoteDeviceScanners.add(scanner);
    }

    public void removeRemoteDeviceScanner(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        scanner.removeListener(this);
        this.mRemoteDeviceScanners.remove(scanner);
    }

    public void startScanning() {
        this.mKnownConnectionInfo.clear();
        java.util.Iterator it = this.mRemoteDeviceScanners.iterator();
        while (it.hasNext()) {
            ((com.navdy.service.library.device.discovery.RemoteDeviceScanner) it.next()).startScan();
        }
    }

    public void stopScanning() {
        java.util.Iterator it = this.mRemoteDeviceScanners.iterator();
        while (it.hasNext()) {
            ((com.navdy.service.library.device.discovery.RemoteDeviceScanner) it.next()).stopScan();
        }
    }

    public void refresh() {
        sLogger.i("refreshing paired connections info");
        loadPairedConnections();
    }

    public void addDiscoveredConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        if (this.mKnownConnectionInfo.contains(connectionInfo)) {
            sLogger.d("Already contains: " + connectionInfo);
            return;
        }
        this.mKnownConnectionInfo.add(connectionInfo);
        sendDeviceListUpdate();
    }

    protected void sendDeviceListUpdate() {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDeviceRegistry.Anon1());
    }

    public void setDefaultConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_DEFAULT_CONNECTION_INFO, this.mGson.toJson((java.lang.Object) connectionInfo, (java.lang.reflect.Type) com.navdy.service.library.device.connection.ConnectionInfo.class)).apply();
        } catch (java.lang.Exception e) {
            sLogger.e("Exception " + e);
        }
    }

    public com.navdy.service.library.device.connection.ConnectionInfo getDefaultConnectionInfo() {
        com.navdy.service.library.device.connection.ConnectionInfo connectionInfo = null;
        try {
            java.lang.String connectionInfoText = this.mSharedPrefs.getString(PREFS_KEY_DEFAULT_CONNECTION_INFO, null);
            if (connectionInfoText == null) {
                return null;
            }
            connectionInfo = (com.navdy.service.library.device.connection.ConnectionInfo) this.mGson.fromJson(connectionInfoText, com.navdy.service.library.device.connection.ConnectionInfo.class);
            return connectionInfo;
        } catch (com.google.gson.JsonParseException e) {
            sLogger.e("Unable to read connection info: ", e);
        }
    }

    public void addPairedConnection(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        if (this.pairedConnections.size() >= 5) {
            this.pairedConnections.remove(4);
        }
        this.pairedConnections.add(0, connectionInfo);
        savePairedConnections();
    }

    public void removePairedConnection(android.bluetooth.BluetoothDevice device) {
        java.util.Iterator<com.navdy.service.library.device.connection.ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            com.navdy.service.library.device.connection.ConnectionInfo info = (com.navdy.service.library.device.connection.ConnectionInfo) listIterator.next();
            if ((info instanceof com.navdy.service.library.device.connection.BTConnectionInfo) && info.getAddress().getAddress().equals(device.getAddress())) {
                listIterator.remove();
            }
        }
        savePairedConnections();
    }

    public com.navdy.service.library.device.connection.ConnectionInfo findDevice(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        java.util.Iterator<com.navdy.service.library.device.connection.ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            com.navdy.service.library.device.connection.ConnectionInfo info = (com.navdy.service.library.device.connection.ConnectionInfo) listIterator.next();
            if (info.getDeviceId().equals(deviceId)) {
                return info;
            }
        }
        return null;
    }

    public void removePairedConnection(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        savePairedConnections();
    }

    public java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> getPairedConnections() {
        return this.pairedConnections;
    }

    public boolean hasPaired() {
        return this.pairedConnections.size() > 0;
    }

    public void savePairedConnections() {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_PAIRED_CONNECTION_INFOS, this.mGson.toJson((java.lang.Object) this.pairedConnections, new com.navdy.service.library.device.RemoteDeviceRegistry.Anon2().getType())).commit();
        } catch (java.lang.Exception e) {
            sLogger.e("Exception saving paired connections", e);
        }
    }

    private void loadPairedConnections() {
        this.mSharedPrefs = this.mContext.getSharedPreferences(PREFS_FILE_DEVICE_REGISTRY, 4);
        java.util.ArrayList<com.navdy.service.library.device.connection.ConnectionInfo> list = new java.util.ArrayList<>();
        try {
            java.lang.String connectionInfoText = this.mSharedPrefs.getString(PREFS_KEY_PAIRED_CONNECTION_INFOS, null);
            java.lang.reflect.Type listType = new com.navdy.service.library.device.RemoteDeviceRegistry.Anon3().getType();
            if (connectionInfoText != null) {
                list = (java.util.ArrayList) this.mGson.fromJson(connectionInfoText, listType);
            }
        } catch (com.google.gson.JsonParseException e) {
            sLogger.e("Unable to read connection infos: ", e);
        }
        sLogger.v("Read pairing list of:" + java.util.Arrays.toString(list.toArray()));
        this.pairedConnections = list;
    }

    public void onScanStarted(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        sLogger.e("Scan started: " + scanner.toString());
    }

    public void onScanStopped(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner) {
        sLogger.e("Scan stopped: " + scanner.toString());
    }

    public void onDiscovered(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner, java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> devices) {
        for (com.navdy.service.library.device.connection.ConnectionInfo connectionInfo : devices) {
            if (connectionInfo == null) {
                sLogger.e("missing connection info.");
            } else {
                addDiscoveredConnectionInfo(connectionInfo);
                if (!(connectionInfo instanceof com.navdy.service.library.device.connection.BTConnectionInfo) && connectionInfo.getDeviceId().getBluetoothAddress() != null) {
                    addDiscoveredConnectionInfo(new com.navdy.service.library.device.connection.BTConnectionInfo(connectionInfo.getDeviceId(), new com.navdy.service.library.device.connection.ServiceAddress(connectionInfo.getDeviceId().getBluetoothAddress(), com.navdy.service.library.device.connection.ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF));
                }
            }
        }
    }

    public void onLost(com.navdy.service.library.device.discovery.RemoteDeviceScanner scanner, java.util.List<com.navdy.service.library.device.connection.ConnectionInfo> list) {
    }

    public com.navdy.service.library.device.connection.ConnectionInfo getLastPairedDevice() {
        try {
            if (this.pairedConnections.size() > 0) {
                return (com.navdy.service.library.device.connection.ConnectionInfo) this.pairedConnections.get(0);
            }
            return null;
        } catch (Throwable th) {
            return null;
        }
    }
}
