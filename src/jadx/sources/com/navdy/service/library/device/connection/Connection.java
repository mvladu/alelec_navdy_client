package com.navdy.service.library.device.connection;

public abstract class Connection extends com.navdy.service.library.util.Listenable<com.navdy.service.library.device.connection.Connection.Listener> {
    private static java.util.Map<com.navdy.service.library.device.connection.ConnectionType, com.navdy.service.library.device.connection.Connection.ConnectionFactory> factoryMap = new java.util.HashMap();
    private static com.navdy.service.library.device.connection.Connection.ConnectionFactory sDefaultFactory = new com.navdy.service.library.device.connection.Connection.Anon1();
    protected com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    protected com.navdy.service.library.device.connection.ConnectionInfo mConnectionInfo;
    protected com.navdy.service.library.device.connection.Connection.Status mStatus;

    static class Anon1 implements com.navdy.service.library.device.connection.Connection.ConnectionFactory {
        Anon1() {
        }

        public com.navdy.service.library.device.connection.Connection build(android.content.Context context, com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
            switch (com.navdy.service.library.device.connection.Connection.Anon5.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[connectionInfo.getType().ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    com.navdy.service.library.device.connection.ServiceAddress address = connectionInfo.getAddress();
                    if (address.getService().equals(com.navdy.service.library.device.connection.ConnectionService.ACCESSORY_IAP2.toString())) {
                        address = new com.navdy.service.library.device.connection.ServiceAddress(address.getAddress(), com.navdy.service.library.device.connection.ConnectionService.DEVICE_IAP2.toString(), address.getProtocol());
                    }
                    return new com.navdy.service.library.device.connection.SocketConnection(connectionInfo, (com.navdy.service.library.network.SocketFactory) new com.navdy.service.library.network.BTSocketFactory(address));
                case 5:
                    return new com.navdy.service.library.device.connection.SocketConnection(connectionInfo, (com.navdy.service.library.network.SocketFactory) new com.navdy.service.library.network.TCPSocketFactory(connectionInfo.getAddress()));
                default:
                    throw new java.lang.IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
            }
        }
    }

    class Anon2 implements com.navdy.service.library.device.connection.Connection.EventDispatcher {
        Anon2() {
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.Connection source, com.navdy.service.library.device.connection.Connection.Listener listener) {
            listener.onConnected(source);
        }
    }

    class Anon3 implements com.navdy.service.library.device.connection.Connection.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.device.connection.Connection.ConnectionFailureCause val$cause;

        Anon3(com.navdy.service.library.device.connection.Connection.ConnectionFailureCause connectionFailureCause) {
            this.val$cause = connectionFailureCause;
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.Connection source, com.navdy.service.library.device.connection.Connection.Listener listener) {
            listener.onConnectionFailed(source, this.val$cause);
        }
    }

    class Anon4 implements com.navdy.service.library.device.connection.Connection.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.device.connection.Connection.DisconnectCause val$cause;

        Anon4(com.navdy.service.library.device.connection.Connection.DisconnectCause disconnectCause) {
            this.val$cause = disconnectCause;
        }

        public void dispatchEvent(com.navdy.service.library.device.connection.Connection source, com.navdy.service.library.device.connection.Connection.Listener listener) {
            listener.onDisconnected(source, this.val$cause);
        }
    }

    static /* synthetic */ class Anon5 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionType = new int[com.navdy.service.library.device.connection.ConnectionType.values().length];

        static {
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.BT_TUNNEL.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError e5) {
            }
        }
    }

    public interface ConnectionFactory {
        com.navdy.service.library.device.connection.Connection build(android.content.Context context, com.navdy.service.library.device.connection.ConnectionInfo connectionInfo);
    }

    public enum ConnectionFailureCause {
        UNKNOWN,
        CONNECTION_REFUSED,
        CONNECTION_TIMED_OUT
    }

    public enum DisconnectCause {
        UNKNOWN,
        NORMAL,
        ABORTED
    }

    protected interface EventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.service.library.device.connection.Connection, com.navdy.service.library.device.connection.Connection.Listener> {
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onConnected(com.navdy.service.library.device.connection.Connection connection);

        void onConnectionFailed(com.navdy.service.library.device.connection.Connection connection, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause connectionFailureCause);

        void onDisconnected(com.navdy.service.library.device.connection.Connection connection, com.navdy.service.library.device.connection.Connection.DisconnectCause disconnectCause);
    }

    public enum Status {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING
    }

    public abstract boolean connect();

    public abstract boolean disconnect();

    public abstract com.navdy.service.library.network.SocketAdapter getSocket();

    public static com.navdy.service.library.device.connection.Connection instantiateFromConnectionInfo(android.content.Context context, com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        com.navdy.service.library.device.connection.Connection.ConnectionFactory factory = (com.navdy.service.library.device.connection.Connection.ConnectionFactory) factoryMap.get(connectionInfo.getType());
        if (factory != null) {
            return factory.build(context, connectionInfo);
        }
        throw new java.lang.IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
    }

    static {
        registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF, sDefaultFactory);
        registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.BT_TUNNEL, sDefaultFactory);
        registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF, sDefaultFactory);
        registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.BT_IAP2_LINK, sDefaultFactory);
        registerConnectionType(com.navdy.service.library.device.connection.ConnectionType.EA_PROTOBUF, sDefaultFactory);
    }

    public static void registerConnectionType(com.navdy.service.library.device.connection.ConnectionType type, com.navdy.service.library.device.connection.Connection.ConnectionFactory factory) {
        factoryMap.put(type, factory);
    }

    public Connection(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        this.mConnectionInfo = connectionInfo;
        this.mStatus = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED;
    }

    public com.navdy.service.library.device.connection.Connection.Status getStatus() {
        return this.mStatus;
    }

    public com.navdy.service.library.device.connection.ConnectionType getType() {
        return this.mConnectionInfo.getType();
    }

    public com.navdy.service.library.device.connection.ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }

    protected void dispatchConnectEvent() {
        dispatchToListeners(new com.navdy.service.library.device.connection.Connection.Anon2());
    }

    protected void dispatchConnectionFailedEvent(com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        dispatchToListeners(new com.navdy.service.library.device.connection.Connection.Anon3(cause));
    }

    protected void dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        dispatchToListeners(new com.navdy.service.library.device.connection.Connection.Anon4(cause));
    }
}
