package com.navdy.service.library.device.connection.tunnel;

public class Tunnel extends com.navdy.service.library.device.connection.ProxyService {
    static final java.lang.String TAG = com.navdy.service.library.device.connection.tunnel.Tunnel.class.getSimpleName();
    private final com.navdy.service.library.network.SocketAcceptor acceptor;
    private final com.navdy.service.library.network.SocketFactory connector;
    final java.util.ArrayList<com.navdy.service.library.device.connection.tunnel.TransferThread> transferThreads = new java.util.ArrayList<>();

    public Tunnel(com.navdy.service.library.network.SocketAcceptor acceptor2, com.navdy.service.library.network.SocketFactory connector2) {
        setName(TAG);
        this.acceptor = acceptor2;
        this.connector = connector2;
    }

    public void run() {
        android.util.Log.d(TAG, java.lang.String.format("start: %s -> %s", new java.lang.Object[]{this.acceptor, this.connector}));
        while (true) {
            try {
                com.navdy.service.library.network.SocketAdapter fromSock = this.acceptor.accept();
                android.util.Log.v(TAG, "accepted connection (" + fromSock + ")");
                com.navdy.service.library.network.SocketAdapter toSock = null;
                java.io.InputStream fromInput = null;
                java.io.InputStream toInput = null;
                java.io.OutputStream fromOutput = null;
                java.io.OutputStream toOutput = null;
                try {
                    toSock = this.connector.build();
                    toSock.connect();
                    android.util.Log.v(TAG, "connected (" + toSock + ")");
                    android.util.Log.d(TAG, java.lang.String.format("starting transfer: %s -> %s", new java.lang.Object[]{fromSock, toSock}));
                    fromInput = fromSock.getInputStream();
                    toOutput = toSock.getOutputStream();
                    com.navdy.service.library.device.connection.tunnel.TransferThread tt1 = new com.navdy.service.library.device.connection.tunnel.TransferThread(this, fromInput, toOutput, true);
                    toInput = toSock.getInputStream();
                    fromOutput = fromSock.getOutputStream();
                    com.navdy.service.library.device.connection.tunnel.TransferThread tt2 = new com.navdy.service.library.device.connection.tunnel.TransferThread(this, toInput, fromOutput, false);
                    tt1.start();
                    tt2.start();
                    tt1.join();
                    tt2.join();
                    fromSock.close();
                    toSock.close();
                } catch (java.lang.InterruptedException e) {
                } catch (java.io.IOException e2) {
                    android.util.Log.e(TAG, "transfer failed:" + e2.getMessage());
                    com.navdy.service.library.util.IOUtils.closeStream(toInput);
                    com.navdy.service.library.util.IOUtils.closeStream(toOutput);
                    com.navdy.service.library.util.IOUtils.closeStream(toSock);
                    com.navdy.service.library.util.IOUtils.closeStream(fromInput);
                    com.navdy.service.library.util.IOUtils.closeStream(fromOutput);
                    com.navdy.service.library.util.IOUtils.closeStream(fromSock);
                }
            } catch (java.lang.Exception e3) {
                android.util.Log.e(TAG, "accept failed:" + e3.getMessage());
                cancel();
                return;
            }
        }
    }

    public void cancel() {
        try {
            this.acceptor.close();
        } catch (java.io.IOException e) {
            android.util.Log.e(TAG, "close failed:" + e.getMessage());
        }
        java.util.Iterator it = ((java.util.ArrayList) this.transferThreads.clone()).iterator();
        while (it.hasNext()) {
            ((com.navdy.service.library.device.connection.tunnel.TransferThread) it.next()).cancel();
        }
    }
}
