package com.navdy.service.library.device.connection;

public class ServiceAddress {
    private final java.lang.String address;
    private final java.lang.String protocol;
    private final java.lang.String service;

    public ServiceAddress(java.lang.String device, java.lang.String service2) {
        this.address = device;
        this.service = service2;
        this.protocol = "NA";
    }

    public ServiceAddress(java.lang.String device, java.lang.String service2, java.lang.String protocol2) {
        this.address = device;
        this.service = service2;
        this.protocol = protocol2;
    }

    public java.lang.String getAddress() {
        return this.address;
    }

    public java.lang.String getService() {
        return this.service;
    }

    public java.lang.String getProtocol() {
        return this.protocol;
    }

    public java.lang.String toString() {
        return this.address + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + this.service + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + this.protocol;
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        com.navdy.service.library.device.connection.ServiceAddress that = (com.navdy.service.library.device.connection.ServiceAddress) o;
        if (!this.address.equals(that.address) || !this.service.equals(that.service)) {
            return false;
        }
        return this.protocol.equals(that.protocol);
    }

    public int hashCode() {
        return (((this.address.hashCode() * 31) + this.service.hashCode()) * 31) + this.protocol.hashCode();
    }
}
