package com.navdy.service.library.device.connection;

public class TCPConnectionInfo extends com.navdy.service.library.device.connection.ConnectionInfo {
    public static final java.lang.String FORMAT_SERVICE_NAME_WITH_DEVICE_ID = "Navdy %s";
    public static final int SERVICE_PORT = 21301;
    public static final java.lang.String SERVICE_TYPE = "_navdybus._tcp.";
    @com.google.gson.annotations.SerializedName("address")
    java.lang.String mHostName;

    public static android.net.nsd.NsdServiceInfo nsdServiceWithDeviceId(com.navdy.service.library.device.NavdyDeviceId deviceId) {
        android.net.nsd.NsdServiceInfo serviceInfo = new android.net.nsd.NsdServiceInfo();
        serviceInfo.setServiceName(java.lang.String.format(FORMAT_SERVICE_NAME_WITH_DEVICE_ID, new java.lang.Object[]{deviceId.toString()}));
        serviceInfo.setServiceType(SERVICE_TYPE);
        serviceInfo.setPort(SERVICE_PORT);
        return serviceInfo;
    }

    public TCPConnectionInfo(com.navdy.service.library.device.NavdyDeviceId deviceId, java.lang.String hostName) {
        super(deviceId, com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF);
        if (hostName == null) {
            throw new java.lang.IllegalArgumentException("hostname required");
        }
        this.mHostName = hostName;
    }

    public TCPConnectionInfo(android.net.nsd.NsdServiceInfo serviceInfo) {
        super(serviceInfo);
        java.lang.String hostName = serviceInfo.getHost().getHostAddress();
        if (hostName == null) {
            throw new java.lang.IllegalArgumentException("hostname not found in service record");
        }
        this.mHostName = hostName;
    }

    public com.navdy.service.library.device.connection.ServiceAddress getAddress() {
        return new com.navdy.service.library.device.connection.ServiceAddress(this.mHostName, java.lang.String.valueOf(SERVICE_PORT));
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass() || !super.equals(o)) {
            return false;
        }
        return this.mHostName.equals(((com.navdy.service.library.device.connection.TCPConnectionInfo) o).mHostName);
    }

    public int hashCode() {
        return (super.hashCode() * 31) + this.mHostName.hashCode();
    }
}
