package com.navdy.service.library.device.discovery;

public class BTRemoteDeviceScanner extends com.navdy.service.library.device.discovery.RemoteDeviceScanner {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.class);
    private android.bluetooth.BluetoothAdapter btAdapter;
    private java.util.Set<java.lang.String> devicesSeen;
    private java.lang.Runnable handleStartScan = new com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.Anon1();
    private java.lang.Runnable handleStopScan = new com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.Anon2();
    private final android.content.BroadcastReceiver mReceiver = new com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.Anon3();
    private boolean scanning = false;
    private int taskQueue;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (!com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.scanning) {
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.scanning = true;
                android.content.IntentFilter filter = new android.content.IntentFilter();
                filter.addAction("android.bluetooth.device.action.FOUND");
                filter.addAction("android.bluetooth.device.action.UUID");
                filter.addAction("android.bluetooth.device.action.NAME_CHANGED");
                filter.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.mContext.registerReceiver(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.mReceiver, filter);
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.devicesSeen = new java.util.HashSet();
                if (com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.btAdapter != null) {
                    if (com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.btAdapter.isDiscovering()) {
                        com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.btAdapter.cancelDiscovery();
                    }
                    com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.btAdapter.startDiscovery();
                    com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.dispatchOnScanStarted();
                }
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            if (com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.scanning) {
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.scanning = false;
                if (com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.btAdapter != null) {
                    com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.btAdapter.cancelDiscovery();
                }
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.mContext.unregisterReceiver(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.mReceiver);
            }
        }
    }

    class Anon3 extends android.content.BroadcastReceiver {
        Anon3() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            java.lang.String action = intent.getAction();
            if ("android.bluetooth.device.action.FOUND".equals(action) || "android.bluetooth.device.action.NAME_CHANGED".equals(action)) {
                android.bluetooth.BluetoothDevice device = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                java.lang.String name = intent.getStringExtra("android.bluetooth.device.extra.NAME");
                java.lang.String address = device.getAddress();
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.sLogger.d("Scanned: " + address + " - name = " + name);
                if (name != null && com.navdy.service.library.device.discovery.BTDeviceBroadcaster.isDisplay(name) && !com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.devicesSeen.contains(address)) {
                    com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.devicesSeen.add(address);
                    com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.dispatchOnDiscovered((com.navdy.service.library.device.connection.ConnectionInfo) new com.navdy.service.library.device.connection.BTConnectionInfo(new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId.Type.BT, address, name), new com.navdy.service.library.device.connection.ServiceAddress(address, com.navdy.service.library.device.connection.ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF));
                }
            } else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.this.dispatchOnScanStopped();
            }
        }
    }

    public BTRemoteDeviceScanner(android.content.Context context, int serialTaskQueue) {
        super(context);
        this.taskQueue = serialTaskQueue;
        this.btAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    }

    public boolean startScan() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.handleStartScan, this.taskQueue);
        return true;
    }

    public boolean stopScan() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.handleStopScan, this.taskQueue);
        return true;
    }
}
