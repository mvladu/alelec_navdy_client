package com.navdy.service.library.device.connection.tunnel;

class TransferThread extends java.lang.Thread {
    static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.connection.tunnel.TransferThread.class);
    final com.navdy.service.library.util.NetworkActivityTracker activityTracker = com.navdy.service.library.util.NetworkActivityTracker.getInstance();
    private volatile boolean canceled;
    final java.io.InputStream inStream;
    final java.io.OutputStream outStream;
    final com.navdy.service.library.device.connection.tunnel.Tunnel parentThread;
    final boolean sending;

    public TransferThread(com.navdy.service.library.device.connection.tunnel.Tunnel parentThread2, java.io.InputStream inStream2, java.io.OutputStream outStream2, boolean sending2) {
        sLogger.v("new transfer thread (" + this + "): " + inStream2 + " -> " + outStream2);
        setName(com.navdy.service.library.device.connection.tunnel.TransferThread.class.getSimpleName());
        this.parentThread = parentThread2;
        this.inStream = inStream2;
        this.outStream = outStream2;
        this.sending = sending2;
        parentThread2.transferThreads.add(this);
    }

    public void run() {
        byte[] buffer = new byte[16384];
        while (true) {
            try {
                int bytes = this.inStream.read(buffer);
                if (bytes < 0) {
                    break;
                }
                if (sLogger.isLoggable(2)) {
                    byte[] copyOf = java.util.Arrays.copyOf(buffer, bytes);
                    sLogger.v(java.lang.String.format("transfer (%s -> %s): %d bytes%s", new java.lang.Object[]{this.inStream, this.outStream, java.lang.Integer.valueOf(bytes), ""}));
                }
                this.outStream.write(buffer, 0, bytes);
                this.outStream.flush();
                if (this.sending) {
                    this.activityTracker.addBytesSent(bytes);
                } else {
                    this.activityTracker.addBytesReceived(bytes);
                }
            } catch (Throwable e) {
                if (!this.canceled) {
                    sLogger.e("Exception", e);
                }
            }
        }
        sLogger.d("socket was closed");
        cancel();
    }

    public void cancel() {
        this.canceled = true;
        com.navdy.service.library.util.IOUtils.closeStream(this.inStream);
        com.navdy.service.library.util.IOUtils.closeStream(this.outStream);
        this.parentThread.transferThreads.remove(this);
    }
}
