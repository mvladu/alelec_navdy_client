package com.navdy.service.library.device.link;

public class ReaderThread extends com.navdy.service.library.device.link.IOThread {
    private final com.navdy.service.library.device.connection.ConnectionType connectionType;
    private boolean isNetworkLinkReadyByDefault = false;
    private final com.navdy.service.library.device.link.LinkListener linkListener;
    private final com.navdy.service.library.events.NavdyEventReader mmEventReader;
    private java.io.InputStream mmInStream;

    public ReaderThread(com.navdy.service.library.device.connection.ConnectionType connectionType2, java.io.InputStream inputStream, com.navdy.service.library.device.link.LinkListener listener, boolean isNetworkLinkReadyWhenConnected) {
        setName("ReaderThread");
        this.logger.d("create ReaderThread");
        this.mmInStream = inputStream;
        this.mmEventReader = new com.navdy.service.library.events.NavdyEventReader(this.mmInStream);
        this.linkListener = listener;
        this.connectionType = connectionType2;
        this.isNetworkLinkReadyByDefault = isNetworkLinkReadyWhenConnected;
    }

    public void run() {
        this.logger.i("BEGIN");
        com.navdy.service.library.device.connection.Connection.DisconnectCause cause = com.navdy.service.library.device.connection.Connection.DisconnectCause.NORMAL;
        this.linkListener.linkEstablished(this.connectionType);
        if (this.isNetworkLinkReadyByDefault) {
            this.linkListener.onNetworkLinkReady();
        }
        while (true) {
            if (this.closing) {
                break;
            }
            try {
                if (this.logger.isLoggable(3)) {
                    this.logger.v("before read");
                }
                byte[] eventData = this.mmEventReader.readBytes();
                if (eventData == null || eventData.length <= 524288) {
                    if (this.logger.isLoggable(3)) {
                        this.logger.v("after read len:" + (eventData == null ? -1 : eventData.length));
                    }
                    if (eventData != null) {
                        this.linkListener.onNavdyEventReceived(eventData);
                    } else if (!this.closing) {
                        this.logger.e("no event data parsed. assuming disconnect.");
                        cause = com.navdy.service.library.device.connection.Connection.DisconnectCause.ABORTED;
                    }
                } else {
                    throw new java.lang.RuntimeException("reader Max packet size exceeded [" + eventData.length + "] bytes[" + com.navdy.service.library.util.IOUtils.bytesToHexString(eventData, 0, 50) + "]");
                }
            } catch (java.lang.Exception e) {
                if (!this.closing) {
                    this.logger.e("disconnected: " + e.getMessage());
                    cause = com.navdy.service.library.device.connection.Connection.DisconnectCause.ABORTED;
                }
            }
        }
        this.logger.i("Signaling connection lost cause:" + cause);
        this.linkListener.linkLost(this.connectionType, cause);
        this.logger.i("Exiting thread");
        com.navdy.service.library.util.IOUtils.closeStream(this.mmInStream);
        this.mmInStream = null;
    }

    public void cancel() {
        super.cancel();
        com.navdy.service.library.util.IOUtils.closeStream(this.mmInStream);
        this.mmInStream = null;
    }
}
