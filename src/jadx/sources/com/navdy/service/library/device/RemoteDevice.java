package com.navdy.service.library.device;

public class RemoteDevice extends com.navdy.service.library.util.Listenable<com.navdy.service.library.device.RemoteDevice.Listener> implements com.navdy.service.library.device.connection.Connection.Listener, com.navdy.service.library.device.link.LinkListener {
    public static final int MAX_PACKET_SIZE = 524288;
    private static final java.lang.Object lock = new java.lang.Object();
    public static final com.navdy.service.library.device.RemoteDevice.LastSeenDeviceInfo sLastSeenDeviceInfo = new com.navdy.service.library.device.RemoteDevice.LastSeenDeviceInfo();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.RemoteDevice.class);
    protected boolean convertToNavdyEvent;
    public long lastMessageSentTime;
    protected com.navdy.service.library.device.connection.Connection mActiveConnection;
    protected com.navdy.service.library.device.connection.ConnectionInfo mConnectionInfo;
    protected final android.content.Context mContext;
    protected com.navdy.service.library.device.NavdyDeviceId mDeviceId;
    protected com.navdy.service.library.events.DeviceInfo mDeviceInfo;
    protected final java.util.concurrent.LinkedBlockingDeque<com.navdy.service.library.device.link.EventRequest> mEventQueue;
    protected final android.os.Handler mHandler;
    private com.navdy.service.library.device.link.Link mLink;
    private com.navdy.service.library.device.RemoteDevice.LinkStatus mLinkStatus;
    private volatile boolean mNetworkLinkReady;
    protected java.util.concurrent.atomic.AtomicBoolean mNetworkReadyEventDispatched;
    private com.squareup.wire.Wire mWire;

    class Anon1 implements com.navdy.service.library.device.RemoteDevice.EventDispatcher {
        Anon1() {
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDevice source, com.navdy.service.library.device.RemoteDevice.Listener listener) {
            listener.onDeviceConnecting(source);
        }
    }

    class Anon2 implements com.navdy.service.library.device.RemoteDevice.EventDispatcher {
        Anon2() {
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDevice source, com.navdy.service.library.device.RemoteDevice.Listener listener) {
            listener.onDeviceConnected(source);
        }
    }

    class Anon3 implements com.navdy.service.library.device.RemoteDevice.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.device.connection.Connection.ConnectionFailureCause val$cause;

        Anon3(com.navdy.service.library.device.connection.Connection.ConnectionFailureCause connectionFailureCause) {
            this.val$cause = connectionFailureCause;
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDevice source, com.navdy.service.library.device.RemoteDevice.Listener listener) {
            listener.onDeviceConnectFailure(source, this.val$cause);
        }
    }

    class Anon4 implements com.navdy.service.library.device.RemoteDevice.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.device.connection.Connection.DisconnectCause val$cause;

        Anon4(com.navdy.service.library.device.connection.Connection.DisconnectCause disconnectCause) {
            this.val$cause = disconnectCause;
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDevice source, com.navdy.service.library.device.RemoteDevice.Listener listener) {
            listener.onDeviceDisconnected(source, this.val$cause);
        }
    }

    class Anon5 implements com.navdy.service.library.device.RemoteDevice.EventDispatcher {
        final /* synthetic */ byte[] val$eventData;

        Anon5(byte[] bArr) {
            this.val$eventData = bArr;
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDevice source, com.navdy.service.library.device.RemoteDevice.Listener listener) {
            listener.onNavdyEventReceived(source, this.val$eventData);
        }
    }

    class Anon6 implements com.navdy.service.library.device.RemoteDevice.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.events.NavdyEvent val$event;

        Anon6(com.navdy.service.library.events.NavdyEvent navdyEvent) {
            this.val$event = navdyEvent;
        }

        public void dispatchEvent(com.navdy.service.library.device.RemoteDevice source, com.navdy.service.library.device.RemoteDevice.Listener listener) {
            listener.onNavdyEventReceived(source, this.val$event);
        }
    }

    protected interface EventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.service.library.device.RemoteDevice, com.navdy.service.library.device.RemoteDevice.Listener> {
    }

    public static class LastSeenDeviceInfo {
        private com.navdy.service.library.events.DeviceInfo mDeviceInfo;

        public com.navdy.service.library.events.DeviceInfo getDeviceInfo() {
            return this.mDeviceInfo;
        }
    }

    public enum LinkStatus {
        DISCONNECTED,
        LINK_ESTABLISHED,
        CONNECTED
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause connectionFailureCause);

        void onDeviceConnected(com.navdy.service.library.device.RemoteDevice remoteDevice);

        void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice remoteDevice);

        void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.device.connection.Connection.DisconnectCause disconnectCause);

        void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.events.NavdyEvent navdyEvent);

        void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice remoteDevice, byte[] bArr);
    }

    public interface PostEventHandler {
        void onComplete(com.navdy.service.library.device.RemoteDevice.PostEventStatus postEventStatus);
    }

    public enum PostEventStatus {
        SUCCESS,
        DISCONNECTED,
        SEND_FAILED,
        UNKNOWN_FAILURE
    }

    public RemoteDevice(android.content.Context context, com.navdy.service.library.device.NavdyDeviceId deviceId, boolean convertToNavdyEvent2) {
        this.mLinkStatus = com.navdy.service.library.device.RemoteDevice.LinkStatus.DISCONNECTED;
        this.mNetworkLinkReady = false;
        this.mNetworkReadyEventDispatched = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.mContext = context;
        this.mDeviceId = deviceId;
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.mEventQueue = new java.util.concurrent.LinkedBlockingDeque<>();
        this.convertToNavdyEvent = convertToNavdyEvent2;
        if (convertToNavdyEvent2) {
            this.mWire = new com.squareup.wire.Wire((java.lang.Class<?>[]) new java.lang.Class[]{com.navdy.service.library.events.Ext_NavdyEvent.class});
        }
    }

    public RemoteDevice(android.content.Context context, com.navdy.service.library.device.connection.ConnectionInfo connectionInfo, boolean convertToNavdyEvent2) {
        this(context, connectionInfo.getDeviceId(), convertToNavdyEvent2);
        this.mConnectionInfo = connectionInfo;
    }

    public com.navdy.service.library.device.NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }

    public boolean setActiveConnection(com.navdy.service.library.device.connection.Connection connection) {
        boolean z;
        synchronized (lock) {
            this.mActiveConnection = connection;
            if (this.mActiveConnection != null) {
                this.mActiveConnection.addListener(this);
                this.mConnectionInfo = this.mActiveConnection.getConnectionInfo();
            }
            if (isConnected()) {
                dispatchConnectEvent();
            }
            z = this.mActiveConnection != null;
        }
        return z;
    }

    public void removeActiveConnection() {
        synchronized (lock) {
            if (this.mActiveConnection != null) {
                this.mActiveConnection.removeListener(this);
                this.mActiveConnection = null;
            }
        }
    }

    public com.navdy.service.library.device.connection.ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }

    public com.navdy.service.library.events.DeviceInfo getDeviceInfo() {
        return this.mDeviceInfo;
    }

    public void setDeviceInfo(com.navdy.service.library.events.DeviceInfo deviceInfo) {
        this.mDeviceInfo = deviceInfo;
        if (deviceInfo != null) {
            sLastSeenDeviceInfo.mDeviceInfo = deviceInfo;
        }
    }

    public boolean connect() {
        boolean shouldDispatch;
        if (getConnectionStatus() != com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED) {
            return false;
        }
        if (this.mConnectionInfo == null) {
            sLogger.e("can't connect without a connectionInfo");
            return false;
        } else if (setActiveConnection(com.navdy.service.library.device.connection.Connection.instantiateFromConnectionInfo(this.mContext, this.mConnectionInfo))) {
            synchronized (lock) {
                if (!this.mActiveConnection.connect() || getConnectionStatus() != com.navdy.service.library.device.connection.Connection.Status.CONNECTING) {
                    shouldDispatch = false;
                } else {
                    shouldDispatch = true;
                }
            }
            if (shouldDispatch) {
                dispatchConnectingEvent();
                sLogger.i("Starting connection");
                return true;
            }
            sLogger.e("Unable to connect");
            removeActiveConnection();
            return false;
        } else {
            sLogger.e("unable to find connection of type: " + this.mConnectionInfo.getType());
            return false;
        }
    }

    public synchronized boolean disconnect() {
        com.navdy.service.library.device.connection.Connection oldConnection;
        boolean z;
        if (getConnectionStatus() == com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED) {
            sLogger.e("can't disconnect; already disconnected");
            z = true;
        } else {
            synchronized (lock) {
                oldConnection = this.mActiveConnection;
                this.mActiveConnection = null;
            }
            if (oldConnection != null) {
                z = oldConnection.disconnect();
            } else {
                z = false;
            }
        }
        return z;
    }

    public boolean postEvent(com.squareup.wire.Message message) {
        return postEvent(com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(message), (com.navdy.service.library.device.RemoteDevice.PostEventHandler) null);
    }

    public boolean postEvent(com.navdy.service.library.events.NavdyEvent event) {
        return postEvent(event, (com.navdy.service.library.device.RemoteDevice.PostEventHandler) null);
    }

    public boolean postEvent(com.navdy.service.library.events.NavdyEvent event, com.navdy.service.library.device.RemoteDevice.PostEventHandler postEventHandler) {
        return postEvent(event.toByteArray(), postEventHandler);
    }

    public boolean postEvent(byte[] eventData) {
        return postEvent(eventData, (com.navdy.service.library.device.RemoteDevice.PostEventHandler) null);
    }

    public boolean postEvent(byte[] eventData, com.navdy.service.library.device.RemoteDevice.PostEventHandler postEventHandler) {
        boolean ret = this.mEventQueue.add(new com.navdy.service.library.device.link.EventRequest(this.mHandler, eventData, postEventHandler));
        this.lastMessageSentTime = android.os.SystemClock.elapsedRealtime();
        if (sLogger.isLoggable(2)) {
            sLogger.v("NAVDY-PACKET Event queue size [" + this.mEventQueue.size() + "]");
        }
        return ret;
    }

    public com.navdy.service.library.device.connection.Connection.Status getConnectionStatus() {
        com.navdy.service.library.device.connection.Connection.Status status;
        synchronized (lock) {
            if (this.mActiveConnection == null) {
                status = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED;
            } else {
                status = this.mActiveConnection.getStatus();
            }
        }
        return status;
    }

    public boolean startLink() {
        if (this.mLink != null) {
            throw new java.lang.IllegalStateException("Link already started");
        }
        sLogger.i("Starting link");
        this.mLink = com.navdy.service.library.device.link.LinkManager.build(getConnectionInfo());
        com.navdy.service.library.network.SocketAdapter adapter = null;
        synchronized (lock) {
            if (!(this.mLink == null || this.mActiveConnection == null)) {
                adapter = this.mActiveConnection.getSocket();
            }
        }
        if (adapter != null) {
            return this.mLink.start(adapter, this.mEventQueue, this);
        }
        return false;
    }

    public void setLinkBandwidthLevel(int bandwidthLevel) {
        if (this.mLink != null && this.mLinkStatus == com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED) {
            this.mLink.setBandwidthLevel(bandwidthLevel);
        }
    }

    public int getLinkBandwidthLevel() {
        if (this.mLink == null || this.mLinkStatus != com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED) {
            return -1;
        }
        return this.mLink.getBandWidthLevel();
    }

    public com.navdy.service.library.device.RemoteDevice.LinkStatus getLinkStatus() {
        return this.mLinkStatus;
    }

    private void setLinkStatus(com.navdy.service.library.device.RemoteDevice.LinkStatus state) {
        if (this.mLinkStatus != state) {
            com.navdy.service.library.device.RemoteDevice.LinkStatus oldState = this.mLinkStatus;
            this.mLinkStatus = state;
            dispatchStateChangeEvents(this.mLinkStatus, oldState);
        }
    }

    private void dispatchStateChangeEvents(com.navdy.service.library.device.RemoteDevice.LinkStatus newState, com.navdy.service.library.device.RemoteDevice.LinkStatus oldState) {
        java.lang.String deviceId = getDeviceId().toString();
        switch (newState) {
            case CONNECTED:
                if (oldState == com.navdy.service.library.device.RemoteDevice.LinkStatus.DISCONNECTED) {
                    dispatchLocalEvent(new com.navdy.service.library.events.connection.ConnectionStateChange(deviceId, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                }
                dispatchLocalEvent(new com.navdy.service.library.events.connection.ConnectionStateChange(deviceId, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                if (this.mNetworkLinkReady && this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
                    dispatchLocalEvent(new com.navdy.service.library.events.connection.NetworkLinkReady());
                    sLogger.d("dispatchStateChangeEvents: dispatching the Network Link Ready message");
                    return;
                }
                return;
            case DISCONNECTED:
                if (oldState == com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED) {
                    dispatchLocalEvent(new com.navdy.service.library.events.connection.ConnectionStateChange(deviceId, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                }
                dispatchLocalEvent(new com.navdy.service.library.events.connection.ConnectionStateChange(deviceId, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
                return;
            case LINK_ESTABLISHED:
                if (oldState == com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED) {
                    dispatchLocalEvent(new com.navdy.service.library.events.connection.ConnectionStateChange(deviceId, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                    return;
                } else {
                    dispatchLocalEvent(new com.navdy.service.library.events.connection.ConnectionStateChange(deviceId, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                    return;
                }
            default:
                return;
        }
    }

    public boolean stopLink() {
        sLogger.i("Stopping link");
        if (this.mLink != null) {
            sLogger.i("Closing link");
            this.mNetworkReadyEventDispatched.set(false);
            this.mNetworkLinkReady = false;
            this.mLink.close();
            this.mLink = null;
            this.mEventQueue.clear();
        }
        return true;
    }

    public synchronized void linkEstablished(com.navdy.service.library.device.connection.ConnectionType type) {
        switch (type) {
            case BT_PROTOBUF:
            case TCP_PROTOBUF:
            case EA_PROTOBUF:
                setLinkStatus(com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED);
                if (this.mLink != null) {
                    this.mLink.setBandwidthLevel(1);
                    break;
                }
                break;
            case BT_IAP2_LINK:
                setLinkStatus(com.navdy.service.library.device.RemoteDevice.LinkStatus.LINK_ESTABLISHED);
                if (this.mLink != null) {
                    this.mLink.setBandwidthLevel(1);
                    break;
                }
                break;
            default:
                sLogger.e("Unknown connection type: " + type);
                break;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0013  */
    public void onNavdyEventReceived(byte[] eventData) {
        com.navdy.service.library.events.NavdyEvent event;
        int eventTypeIndex;
        if (this.convertToNavdyEvent) {
            event = null;
            try {
                event = (com.navdy.service.library.events.NavdyEvent) this.mWire.parseFrom(eventData, com.navdy.service.library.events.NavdyEvent.class);
            } catch (Throwable th) {
            }
            if (event == null) {
                dispatchNavdyEvent(event);
                return;
            }
            return;
        }
        dispatchNavdyEvent(eventData);
        return;
        sLogger.e("Ignoring invalid navdy event[" + eventTypeIndex + "]", e);
        if (event == null) {
        }
    }

    public synchronized void linkLost(com.navdy.service.library.device.connection.ConnectionType type, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        if (getConnectionStatus() == com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED) {
            cause = com.navdy.service.library.device.connection.Connection.DisconnectCause.NORMAL;
        }
        this.mNetworkReadyEventDispatched.set(false);
        this.mNetworkLinkReady = false;
        switch (type) {
            case BT_PROTOBUF:
            case TCP_PROTOBUF:
                setLinkStatus(com.navdy.service.library.device.RemoteDevice.LinkStatus.DISCONNECTED);
                markDisconnected(cause);
                break;
            case EA_PROTOBUF:
                if (getLinkStatus() == com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED) {
                    setLinkStatus(com.navdy.service.library.device.RemoteDevice.LinkStatus.LINK_ESTABLISHED);
                    break;
                }
                break;
            case BT_IAP2_LINK:
                setLinkStatus(com.navdy.service.library.device.RemoteDevice.LinkStatus.DISCONNECTED);
                markDisconnected(cause);
                break;
            default:
                sLogger.e("Unknown connection type: " + type);
                break;
        }
    }

    public void onNetworkLinkReady() {
        this.mNetworkLinkReady = true;
        if (getLinkStatus() != com.navdy.service.library.device.RemoteDevice.LinkStatus.CONNECTED) {
            sLogger.d("Network Link is ready, but state is not connected");
        } else if (this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
            dispatchLocalEvent(new com.navdy.service.library.events.connection.NetworkLinkReady());
            sLogger.d("onNetworkLinkReady: dispatching the Network Link Ready message");
        }
    }

    private void dispatchLocalEvent(com.squareup.wire.Message message) {
        com.navdy.service.library.events.NavdyEvent event = com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(message);
        if (this.convertToNavdyEvent) {
            dispatchNavdyEvent(event);
        } else {
            dispatchNavdyEvent(event.toByteArray());
        }
    }

    public boolean isConnected() {
        return getConnectionStatus() == com.navdy.service.library.device.connection.Connection.Status.CONNECTED;
    }

    public boolean isConnecting() {
        return getConnectionStatus() == com.navdy.service.library.device.connection.Connection.Status.CONNECTING;
    }

    @android.support.annotation.Nullable
    public com.navdy.service.library.device.connection.ConnectionInfo getActiveConnectionInfo() {
        synchronized (lock) {
            if (this.mActiveConnection == null) {
                return null;
            }
            com.navdy.service.library.device.connection.ConnectionInfo connectionInfo = this.mActiveConnection.getConnectionInfo();
            return connectionInfo;
        }
    }

    private void markDisconnected(com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        removeActiveConnection();
        dispatchDisconnectEvent(cause);
    }

    protected void dispatchConnectingEvent() {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDevice.Anon1());
    }

    protected void dispatchConnectEvent() {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDevice.Anon2());
    }

    protected void dispatchConnectFailureEvent(com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDevice.Anon3(cause));
    }

    protected void dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDevice.Anon4(cause));
    }

    protected void dispatchNavdyEvent(byte[] eventData) {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDevice.Anon5(eventData));
    }

    protected void dispatchNavdyEvent(com.navdy.service.library.events.NavdyEvent event) {
        dispatchToListeners(new com.navdy.service.library.device.RemoteDevice.Anon6(event));
    }

    public void onConnected(com.navdy.service.library.device.connection.Connection connection) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                sLogger.i("Connected");
                dispatchConnectEvent();
            } else {
                sLogger.e("Received connect event for unknown connection");
            }
        }
    }

    public void onConnectionFailed(com.navdy.service.library.device.connection.Connection connection, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                removeActiveConnection();
            }
        }
        sLogger.e("Connection failed: " + cause);
        dispatchConnectFailureEvent(cause);
    }

    public void onDisconnected(com.navdy.service.library.device.connection.Connection connection, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        synchronized (lock) {
            if (this.mActiveConnection == connection) {
                removeActiveConnection();
            }
        }
        java.lang.Object connectionInfo = connection.getConnectionInfo();
        com.navdy.service.library.log.Logger logger = sLogger;
        java.lang.StringBuilder append = new java.lang.StringBuilder().append("Disconnected ");
        if (connectionInfo == null) {
            connectionInfo = "unknown";
        }
        logger.i(append.append(connectionInfo).append(" - ").append(cause).toString());
        dispatchDisconnectEvent(cause);
    }
}
