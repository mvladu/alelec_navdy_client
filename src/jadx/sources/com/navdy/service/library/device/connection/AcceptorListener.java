package com.navdy.service.library.device.connection;

public class AcceptorListener extends com.navdy.service.library.device.connection.ConnectionListener {
    private com.navdy.service.library.network.SocketAcceptor acceptor;
    private com.navdy.service.library.device.connection.ConnectionType connectionType;

    private class AcceptThread extends com.navdy.service.library.device.connection.ConnectionListener.AcceptThread {
        private volatile boolean closing;

        public AcceptThread() throws java.io.IOException {
            super();
            setName("AcceptThread-" + com.navdy.service.library.device.connection.AcceptorListener.this.acceptor.getClass().getSimpleName());
        }

        public void run() {
            com.navdy.service.library.device.connection.AcceptorListener.this.logger.d(getName() + " started, closing:" + this.closing);
            com.navdy.service.library.device.connection.AcceptorListener.this.dispatchStarted();
            com.navdy.service.library.network.SocketAdapter socket = null;
            try {
                socket = com.navdy.service.library.device.connection.AcceptorListener.this.acceptor.accept();
                com.navdy.service.library.device.connection.AcceptorListener.this.dispatchConnected(new com.navdy.service.library.device.connection.SocketConnection(com.navdy.service.library.device.connection.AcceptorListener.this.acceptor.getRemoteConnectionInfo(socket, com.navdy.service.library.device.connection.AcceptorListener.this.connectionType), socket));
            } catch (Throwable e) {
                if (!this.closing) {
                    com.navdy.service.library.device.connection.AcceptorListener.this.logger.e("Socket accept() failed", e);
                }
                com.navdy.service.library.util.IOUtils.closeStream(socket);
            } finally {
                cancel();
            }
            com.navdy.service.library.device.connection.AcceptorListener.this.dispatchStopped();
            com.navdy.service.library.device.connection.AcceptorListener.this.logger.i("END " + getName());
        }

        public void cancel() {
            com.navdy.service.library.device.connection.AcceptorListener.this.logger.d("Socket cancel " + this);
            this.closing = true;
            com.navdy.service.library.util.IOUtils.closeStream(com.navdy.service.library.device.connection.AcceptorListener.this.acceptor);
        }
    }

    public AcceptorListener(android.content.Context context, com.navdy.service.library.network.SocketAcceptor acceptor2, com.navdy.service.library.device.connection.ConnectionType connectionType2) {
        super(context, "Acceptor/" + acceptor2.getClass().getSimpleName());
        this.acceptor = acceptor2;
        this.connectionType = connectionType2;
    }

    public com.navdy.service.library.device.connection.ConnectionType getType() {
        return this.connectionType;
    }

    /* access modifiers changed from: protected */
    public com.navdy.service.library.device.connection.AcceptorListener.AcceptThread getNewAcceptThread() throws java.io.IOException {
        return new com.navdy.service.library.device.connection.AcceptorListener.AcceptThread();
    }

    public java.lang.String toString() {
        return getClass().getSimpleName() + (this.acceptor != null ? com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + this.acceptor.getClass().getSimpleName() : "");
    }
}
