package com.navdy.service.library.device.link;

public class WriterThread extends com.navdy.service.library.device.link.IOThread {
    private final com.navdy.service.library.device.link.WriterThread.EventProcessor mEventProcessor;
    private final java.util.concurrent.LinkedBlockingDeque<com.navdy.service.library.device.link.EventRequest> mEventQueue;
    private final com.navdy.service.library.events.NavdyEventWriter mmEventWriter = new com.navdy.service.library.events.NavdyEventWriter(this.mmOutStream);
    private java.io.OutputStream mmOutStream;

    public interface EventProcessor {
        byte[] processEvent(byte[] bArr);
    }

    public WriterThread(java.util.concurrent.LinkedBlockingDeque<com.navdy.service.library.device.link.EventRequest> queue, java.io.OutputStream outputStream, com.navdy.service.library.device.link.WriterThread.EventProcessor processor) {
        setName("WriterThread");
        this.logger.d("create WriterThread");
        this.mmOutStream = outputStream;
        this.mEventQueue = queue;
        this.mEventProcessor = processor;
    }

    public void run() {
        this.logger.i("starting WriterThread loop.");
        while (!this.closing) {
            com.navdy.service.library.device.link.EventRequest request = null;
            try {
                request = (com.navdy.service.library.device.link.EventRequest) this.mEventQueue.take();
            } catch (java.lang.InterruptedException e) {
                this.logger.i("WriterThread interrupted");
            }
            if (this.closing) {
                break;
            } else if (request == null) {
                this.logger.e("WriterThread: unable to retrieve event from queue");
            } else {
                byte[] eventData = request.eventData;
                if (eventData.length > 524288) {
                    throw new java.lang.RuntimeException("writer Max packet size exceeded [" + eventData.length + "] bytes[" + com.navdy.service.library.util.IOUtils.bytesToHexString(eventData, 0, 50) + "]");
                }
                if (this.mEventProcessor != null) {
                    eventData = this.mEventProcessor.processEvent(eventData);
                }
                if (eventData != null) {
                    boolean sent = false;
                    try {
                        this.mmEventWriter.write(request.eventData);
                        sent = true;
                    } catch (java.io.IOException e2) {
                        if (!this.closing) {
                            this.logger.e("Error writing event:" + e2.getMessage());
                        }
                    }
                    if (!sent) {
                        this.logger.i("send failed");
                        request.callCompletionHandlers(com.navdy.service.library.device.RemoteDevice.PostEventStatus.SEND_FAILED);
                    }
                }
            }
        }
        this.logger.i("Writer thread ending");
    }

    public void cancel() {
        super.cancel();
        com.navdy.service.library.util.IOUtils.closeStream(this.mmOutStream);
        this.mmOutStream = null;
    }
}
