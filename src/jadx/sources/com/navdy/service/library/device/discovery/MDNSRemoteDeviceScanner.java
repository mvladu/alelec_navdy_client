package com.navdy.service.library.device.discovery;

public class MDNSRemoteDeviceScanner extends com.navdy.service.library.device.discovery.RemoteDeviceScanner {
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.class);
    protected java.util.Timer discoveryTimer;
    protected android.net.nsd.NsdManager.DiscoveryListener mDiscoveryListener;
    protected boolean mDiscoveryRunning;
    protected android.os.Handler mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
    protected android.net.nsd.NsdManager mNsdManager;
    protected java.util.ArrayList<android.net.nsd.NsdManager.ResolveListener> mResolveListeners = new java.util.ArrayList<>();
    protected android.net.nsd.NsdServiceInfo mResolvingService;
    protected final java.lang.String mServiceType;
    protected java.util.Queue<android.net.nsd.NsdServiceInfo> mUnresolvedServices = new java.util.LinkedList();

    class Anon1 extends java.util.TimerTask {
        Anon1() {
        }

        public void run() {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Scan timer fired: " + com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mServiceType);
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mNsdManager.discoverServices(com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mServiceType, 1, com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mDiscoveryListener);
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Scan started: " + com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mServiceType);
        }
    }

    class Anon2 implements android.net.nsd.NsdManager.DiscoveryListener {
        Anon2() {
        }

        public void onDiscoveryStarted(java.lang.String regType) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mDiscoveryRunning = true;
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.discoveryTimer = null;
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.d("Service discovery started");
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.dispatchOnScanStarted();
        }

        public void onServiceFound(android.net.nsd.NsdServiceInfo service) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.d("Service discovery success " + service);
            if (com.navdy.service.library.device.connection.ConnectionInfo.isValidNavdyServiceInfo(service)) {
                com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.resolveService(service);
            }
        }

        public void onServiceLost(android.net.nsd.NsdServiceInfo service) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("service lost" + service);
        }

        public void onDiscoveryStopped(java.lang.String serviceType) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mDiscoveryRunning = false;
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.i("Discovery stopped: " + serviceType);
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.dispatchOnScanStopped();
        }

        public void onStartDiscoveryFailed(java.lang.String serviceType, int errorCode) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Start Discovery failed: Error code:" + errorCode);
        }

        public void onStopDiscoveryFailed(java.lang.String serviceType, int errorCode) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Stop Discovery failed: Error code:" + errorCode);
        }
    }

    class Anon3 implements android.net.nsd.NsdManager.ResolveListener {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.service.library.device.connection.ConnectionInfo val$connectionInfo;

            Anon1(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
                this.val$connectionInfo = connectionInfo;
            }

            public void run() {
                com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.dispatchOnDiscovered(this.val$connectionInfo);
            }
        }

        Anon3() {
        }

        public void onResolveFailed(android.net.nsd.NsdServiceInfo serviceInfo, int errorCode) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mResolveListeners.remove(this);
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Resolve failed" + errorCode);
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.resolveComplete();
        }

        public void onServiceResolved(android.net.nsd.NsdServiceInfo serviceInfo) {
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mResolveListeners.remove(this);
            com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Resolve Succeeded. " + serviceInfo);
            try {
                com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.mHandler.post(new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.Anon3.Anon1(com.navdy.service.library.device.connection.ConnectionInfo.fromServiceInfo(serviceInfo)));
                com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.this.resolveComplete();
            } catch (java.lang.IllegalArgumentException e) {
                com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.sLogger.e("Unable to process serviceInfo");
            }
        }
    }

    public MDNSRemoteDeviceScanner(android.content.Context context, java.lang.String serviceType) {
        super(context);
        this.mServiceType = serviceType;
    }

    public boolean startScan() {
        sLogger.e("Starting scan: " + this.mServiceType);
        initNsd();
        if (this.mNsdManager == null) {
            sLogger.e("Can't scan: no NsdManager: " + this.mServiceType);
            return false;
        } else if (this.discoveryTimer != null || this.mDiscoveryRunning) {
            sLogger.e("Can't start: already started " + this.mServiceType);
            return true;
        } else {
            this.discoveryTimer = new java.util.Timer();
            this.discoveryTimer.schedule(new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.Anon1(), 2000);
            return true;
        }
    }

    public boolean stopScan() {
        sLogger.e("Stopping scan: " + this.mServiceType);
        if (this.mNsdManager == null) {
            sLogger.e("Can't stop: already stopped: " + this.mServiceType);
            return false;
        } else if (this.discoveryTimer != null) {
            this.discoveryTimer.cancel();
            this.discoveryTimer = null;
            return true;
        } else {
            if (this.mDiscoveryListener != null) {
                try {
                    this.mNsdManager.stopServiceDiscovery(this.mDiscoveryListener);
                } catch (java.lang.Exception e) {
                    if (this.mDiscoveryRunning) {
                        sLogger.e("Problem stopping nsd service discovery", e);
                    }
                }
                this.mDiscoveryListener = null;
                this.mDiscoveryRunning = false;
            }
            return true;
        }
    }

    public void initNsd() {
        initializeDiscoveryListener();
    }

    public void initializeDiscoveryListener() {
        if (this.mNsdManager == null) {
            this.mNsdManager = (android.net.nsd.NsdManager) this.mContext.getSystemService("servicediscovery");
        }
        if (this.mDiscoveryListener == null) {
            this.mDiscoveryListener = new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.Anon2();
        }
    }

    protected void resolveService(android.net.nsd.NsdServiceInfo service) {
        if (this.mResolvingService == null) {
            this.mResolvingService = service;
            android.net.nsd.NsdManager.ResolveListener resolveListener = getNewResolveListener();
            this.mResolveListeners.add(resolveListener);
            this.mNsdManager.resolveService(service, resolveListener);
            return;
        }
        this.mUnresolvedServices.add(service);
    }

    protected void resolveComplete() {
        this.mResolvingService = null;
        if (this.mUnresolvedServices.size() > 0) {
            resolveService((android.net.nsd.NsdServiceInfo) this.mUnresolvedServices.remove());
        }
    }

    public android.net.nsd.NsdManager.ResolveListener getNewResolveListener() {
        return new com.navdy.service.library.device.discovery.MDNSRemoteDeviceScanner.Anon3();
    }
}
