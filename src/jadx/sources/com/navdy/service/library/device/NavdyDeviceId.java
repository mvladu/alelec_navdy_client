package com.navdy.service.library.device;

@com.google.gson.annotations.JsonAdapter(com.navdy.service.library.device.NavdyDeviceId.Adapter.class)
public class NavdyDeviceId {
    protected static final java.lang.String ATTRIBUTE_NAME = "N";
    protected static final java.lang.String ATTRIBUTE_SEPARATOR = ";";
    protected static final java.lang.String RESERVED_CHARACTER_REGEX = ";|/";
    public static final com.navdy.service.library.device.NavdyDeviceId UNKNOWN_ID = new com.navdy.service.library.device.NavdyDeviceId("UNK/FF:FF:FF:FF:FF:FF;N/Unknown");
    protected static final java.lang.String VALUE_SEPARATOR = "/";
    static com.navdy.service.library.device.NavdyDeviceId sThisDeviceId;
    protected java.lang.String mDeviceId;
    protected com.navdy.service.library.device.NavdyDeviceId.Type mDeviceIdType;
    protected java.lang.String mDeviceName;
    protected java.lang.String mSerializedDeviceId;

    public static class Adapter extends com.google.gson.TypeAdapter {
        public void write(com.google.gson.stream.JsonWriter out, java.lang.Object value) throws java.io.IOException {
            out.value(value.toString());
        }

        public java.lang.Object read(com.google.gson.stream.JsonReader in) throws java.io.IOException {
            return new com.navdy.service.library.device.NavdyDeviceId(in.nextString());
        }
    }

    public enum Type {
        UNK,
        BT,
        EMU,
        EA
    }

    public NavdyDeviceId(java.lang.String serializedDeviceId) {
        if (serializedDeviceId == null || serializedDeviceId.equals("")) {
            throw new java.lang.IllegalArgumentException("Cannot create empty NavdyDeviceId");
        } else if (!parseDeviceIdString(serializedDeviceId)) {
            throw new java.lang.IllegalArgumentException("Invalid device ID string.");
        } else {
            this.mSerializedDeviceId = createSerializedDeviceId();
        }
    }

    public NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId.Type type, java.lang.String id, java.lang.String deviceName) {
        this.mDeviceIdType = type;
        this.mDeviceId = normalizeDeviceIdString(id);
        this.mDeviceName = normalizeDeviceName(deviceName);
        this.mSerializedDeviceId = createSerializedDeviceId();
    }

    public NavdyDeviceId(android.bluetooth.BluetoothDevice device) {
        this(com.navdy.service.library.device.NavdyDeviceId.Type.BT, device.getAddress(), android.text.TextUtils.isEmpty(device.getName()) ? "unknown" : device.getName());
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        com.navdy.service.library.device.NavdyDeviceId deviceId = (com.navdy.service.library.device.NavdyDeviceId) o;
        if (!this.mDeviceId.equals(deviceId.mDeviceId) || this.mDeviceIdType != deviceId.mDeviceIdType) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.mDeviceId.hashCode() * 31) + this.mDeviceIdType.hashCode();
    }

    public java.lang.String toString() {
        return this.mSerializedDeviceId;
    }

    private java.lang.String createSerializedDeviceId() {
        java.lang.String deviceId = this.mDeviceIdType.toString() + "/" + this.mDeviceId;
        if (!android.text.TextUtils.isEmpty(this.mDeviceName)) {
            return deviceId + ";N/" + this.mDeviceName;
        }
        return deviceId;
    }

    protected java.lang.String normalizeDeviceIdString(java.lang.String deviceId) {
        return deviceId.toUpperCase(java.util.Locale.US);
    }

    protected java.lang.String normalizeDeviceName(java.lang.String deviceName) {
        if (deviceName == null) {
            return null;
        }
        java.lang.String name = deviceName.replaceAll(RESERVED_CHARACTER_REGEX, "");
        if (name.equals("")) {
            return null;
        }
        return name;
    }

    protected boolean parseDeviceIdString(java.lang.String deviceIdString) {
        java.lang.String[] attributes = deviceIdString.split(ATTRIBUTE_SEPARATOR);
        if (attributes.length == 0) {
            return false;
        }
        for (java.lang.String unparsedAttribute : attributes) {
            java.lang.String[] idParts = unparsedAttribute.split("/");
            if (idParts.length < 2) {
                return false;
            }
            java.lang.String attributeText = idParts[0];
            java.lang.String valueText = idParts[1];
            if (attributeText.equals(ATTRIBUTE_NAME)) {
                this.mDeviceName = normalizeDeviceName(valueText);
            } else {
                try {
                    this.mDeviceIdType = com.navdy.service.library.device.NavdyDeviceId.Type.valueOf(idParts[0]);
                    this.mDeviceId = normalizeDeviceIdString(idParts[1]);
                } catch (java.lang.IllegalArgumentException e) {
                    throw new java.lang.IllegalArgumentException("Invalid device ID type");
                }
            }
        }
        return true;
    }

    public java.lang.String getBluetoothAddress() {
        if (this.mDeviceIdType == com.navdy.service.library.device.NavdyDeviceId.Type.BT || this.mDeviceIdType == com.navdy.service.library.device.NavdyDeviceId.Type.EA) {
            return this.mDeviceId;
        }
        return null;
    }

    public java.lang.String getDisplayName() {
        return android.text.TextUtils.isEmpty(this.mDeviceName) ? toString() : this.mDeviceName;
    }

    public java.lang.String getDeviceName() {
        return this.mDeviceName;
    }

    public static com.navdy.service.library.device.NavdyDeviceId getThisDevice(android.content.Context context) {
        if (sThisDeviceId == null || sThisDeviceId.equals(UNKNOWN_ID)) {
            android.bluetooth.BluetoothAdapter btAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
            if (btAdapter != null) {
                java.lang.String btAddr = btAdapter.getAddress();
                if (android.text.TextUtils.isEmpty(btAddr)) {
                    sThisDeviceId = UNKNOWN_ID;
                } else {
                    sThisDeviceId = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId.Type.BT, btAddr, btAdapter.getName());
                }
            } else if (context != null) {
                java.lang.String androidId = android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id");
                if (android.text.TextUtils.isEmpty(androidId)) {
                    sThisDeviceId = UNKNOWN_ID;
                } else {
                    sThisDeviceId = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId.Type.EMU, androidId, "emulator");
                }
            }
        }
        return sThisDeviceId;
    }
}
