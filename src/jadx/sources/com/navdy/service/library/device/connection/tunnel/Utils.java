package com.navdy.service.library.device.connection.tunnel;

public class Utils {
    static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static java.lang.String bytesToHex(byte[] bytes) {
        return bytesToHex(bytes, false);
    }

    public static java.lang.String bytesToHex(byte[] bytes, boolean spaces) {
        int charsPerByte = spaces ? 3 : 2;
        char[] hexChars = new char[(bytes.length * charsPerByte)];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 255;
            hexChars[j * charsPerByte] = hexArray[v >>> 4];
            hexChars[(j * charsPerByte) + 1] = hexArray[v & 15];
            if (spaces) {
                hexChars[(j * charsPerByte) + 2] = ' ';
            }
        }
        return new java.lang.String(hexChars);
    }

    public static java.lang.String toASCII(byte[] bytes) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (byte aByte : bytes) {
            int v = aByte & 255;
            sb.append((v < 32 || v >= 128) ? org.apache.commons.io.FilenameUtils.EXTENSION_SEPARATOR : (char) v);
        }
        return sb.toString();
    }
}
