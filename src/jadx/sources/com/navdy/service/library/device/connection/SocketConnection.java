package com.navdy.service.library.device.connection;

public class SocketConnection extends com.navdy.service.library.device.connection.Connection {
    private com.navdy.service.library.network.SocketFactory connector;
    protected boolean mBlockReconnect;
    protected com.navdy.service.library.device.connection.SocketConnection.ConnectThread mConnectThread;
    private com.navdy.service.library.network.SocketAdapter socket;

    private class ConnectThread extends java.lang.Thread {
        private com.navdy.service.library.network.SocketAdapter mmSocket;

        public ConnectThread() {
            setName("ConnectThread");
        }

        public void run() {
            com.navdy.service.library.device.connection.SocketConnection.this.logger.i("BEGIN mConnectThread");
            com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause = com.navdy.service.library.device.connection.Connection.ConnectionFailureCause.UNKNOWN;
            try {
                this.mmSocket = com.navdy.service.library.device.connection.SocketConnection.this.connector.build();
                this.mmSocket.connect();
                synchronized (com.navdy.service.library.device.connection.SocketConnection.this) {
                    com.navdy.service.library.device.connection.SocketConnection.this.mConnectThread = null;
                }
                com.navdy.service.library.device.connection.SocketConnection.this.connected(this.mmSocket);
            } catch (java.lang.Exception e) {
                com.navdy.service.library.device.connection.SocketConnection.this.logger.e("Unable to connect - " + e.getMessage());
                if (e instanceof java.net.SocketException) {
                    java.lang.String message = e.getMessage();
                    if (message.contains("ETIMEDOUT")) {
                        cause = com.navdy.service.library.device.connection.Connection.ConnectionFailureCause.CONNECTION_TIMED_OUT;
                    } else if (message.contains("ECONNREFUSED")) {
                        cause = com.navdy.service.library.device.connection.Connection.ConnectionFailureCause.CONNECTION_REFUSED;
                    }
                }
                synchronized (com.navdy.service.library.device.connection.SocketConnection.this) {
                    com.navdy.service.library.device.connection.SocketConnection.this.logger.d("Connect failed, closing socket");
                    com.navdy.service.library.device.connection.SocketConnection.this.closeQuietly(this.mmSocket);
                    this.mmSocket = null;
                    com.navdy.service.library.device.connection.Connection.Status oldStatus = com.navdy.service.library.device.connection.SocketConnection.this.mStatus;
                    com.navdy.service.library.device.connection.SocketConnection.this.mStatus = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED;
                    switch (oldStatus) {
                        case CONNECTING:
                            com.navdy.service.library.device.connection.SocketConnection.this.connectionFailed(cause);
                            return;
                        case DISCONNECTING:
                            com.navdy.service.library.device.connection.SocketConnection.this.dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection.DisconnectCause.NORMAL);
                            return;
                        default:
                            com.navdy.service.library.device.connection.SocketConnection.this.logger.w("unexpected state during connection failure: " + oldStatus);
                            return;
                    }
                }
            }
        }

        public void cancel() {
            com.navdy.service.library.device.connection.SocketConnection.this.logger.d("[SOCK]Cancelling connect, closing socket");
            com.navdy.service.library.device.connection.SocketConnection.this.closeQuietly(this.mmSocket);
            this.mmSocket = null;
        }
    }

    public SocketConnection(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo, com.navdy.service.library.network.SocketFactory connector2) {
        super(connectionInfo);
        this.connector = connector2;
    }

    public SocketConnection(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo, com.navdy.service.library.network.SocketAdapter socket2) {
        super(connectionInfo);
        setExistingSocketConnection(socket2);
    }

    protected synchronized void setExistingSocketConnection(com.navdy.service.library.network.SocketAdapter socketAdapter) {
        this.mBlockReconnect = true;
        this.mStatus = com.navdy.service.library.device.connection.Connection.Status.CONNECTING;
        connected(socketAdapter);
    }

    public synchronized boolean connect() {
        boolean z = false;
        synchronized (this) {
            if (!this.mBlockReconnect) {
                this.logger.d("connect to: " + this.mConnectionInfo);
                if (this.mStatus != com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED) {
                    this.logger.d("can't connect: state: " + this.mStatus);
                } else {
                    this.mStatus = com.navdy.service.library.device.connection.Connection.Status.CONNECTING;
                    this.mConnectThread = new com.navdy.service.library.device.connection.SocketConnection.ConnectThread();
                    this.mConnectThread.start();
                    z = true;
                }
            }
        }
        return z;
    }

    public synchronized boolean disconnect() {
        if (this.mStatus == com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED || this.mStatus == com.navdy.service.library.device.connection.Connection.Status.DISCONNECTING) {
            this.logger.d("can't disconnect: state: " + this.mStatus);
        } else {
            this.logger.d("disconnect - connectThread:" + this.mConnectThread + " socket:" + this.socket);
            this.mStatus = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTING;
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            } else if (this.socket != null) {
                try {
                    this.socket.close();
                } catch (java.io.IOException e) {
                    this.logger.e("Exception closing socket" + e.getMessage());
                }
                this.socket = null;
            } else {
                this.mStatus = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED;
                dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection.DisconnectCause.NORMAL);
            }
        }
        return true;
    }

    public com.navdy.service.library.network.SocketAdapter getSocket() {
        return this.socket;
    }

    public synchronized void connected(com.navdy.service.library.network.SocketAdapter socket2) {
        this.logger.i("connected");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mStatus != com.navdy.service.library.device.connection.Connection.Status.CONNECTING || socket2 == null) {
            this.logger.e("No longer connecting - disconnecting.");
            closeQuietly(socket2);
            this.mStatus = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED;
            dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection.DisconnectCause.NORMAL);
        } else {
            this.socket = socket2;
            this.mStatus = com.navdy.service.library.device.connection.Connection.Status.CONNECTED;
            dispatchConnectEvent();
        }
    }

    private void connectionFailed(com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        dispatchConnectionFailedEvent(cause);
    }

    private void closeQuietly(com.navdy.service.library.network.SocketAdapter socket2) {
        if (socket2 != null) {
            try {
                socket2.close();
            } catch (Throwable e) {
                this.logger.e("[SOCK] Exception while closing", e);
            }
        }
    }
}
