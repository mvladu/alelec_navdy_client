package com.navdy.service.library.log;

public class TagFilter implements com.navdy.service.library.log.Filter {
    private boolean invert;
    private java.util.regex.Pattern pattern;

    public static com.navdy.service.library.log.TagFilter pass(java.lang.String... tags) {
        return new com.navdy.service.library.log.TagFilter(startsWithAny(tags));
    }

    public static com.navdy.service.library.log.TagFilter block(java.lang.String... tags) {
        return new com.navdy.service.library.log.TagFilter(startsWithAny(tags), 0, true);
    }

    public static java.lang.String startsWithAny(java.lang.String... tags) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder("^(?:");
        builder.append(android.text.TextUtils.join(com.navdy.client.app.framework.models.Destination.IDENTIFIER_CONTACT_ID_SEPARATOR, tags));
        builder.append(")");
        return builder.toString();
    }

    public TagFilter(java.lang.String regex, int flags, boolean invert2) {
        this.invert = false;
        this.pattern = java.util.regex.Pattern.compile(regex, flags);
        this.invert = invert2;
    }

    public TagFilter(java.lang.String regex) {
        this(regex, 0, false);
    }

    public TagFilter(java.util.regex.Pattern pattern2, boolean invert2) {
        this.invert = false;
        this.pattern = pattern2;
        this.invert = invert2;
    }

    public boolean matches(int logLevel, java.lang.String tag, java.lang.String message) {
        boolean matches = this.pattern.matcher(tag).find();
        if (this.invert) {
            return !matches;
        }
        return matches;
    }
}
