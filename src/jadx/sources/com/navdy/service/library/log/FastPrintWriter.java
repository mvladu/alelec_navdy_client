package com.navdy.service.library.log;

public class FastPrintWriter extends java.io.PrintWriter {
    private static java.io.Writer sDummyWriter = new com.navdy.service.library.log.FastPrintWriter.Anon1();
    private final boolean mAutoFlush;
    private final int mBufferLen;
    private final java.nio.ByteBuffer mBytes;
    private java.nio.charset.CharsetEncoder mCharset;
    private boolean mIoError;
    private final java.io.OutputStream mOutputStream;
    private int mPos;
    private final android.util.Printer mPrinter;
    private final java.lang.String mSeparator;
    private final char[] mText;
    private final java.io.Writer mWriter;

    static class Anon1 extends java.io.Writer {
        Anon1() {
        }

        public void close() throws java.io.IOException {
            throw new java.lang.UnsupportedOperationException("Shouldn't be here");
        }

        public void flush() throws java.io.IOException {
            close();
        }

        public void write(char[] buf, int offset, int count) throws java.io.IOException {
            close();
        }
    }

    public FastPrintWriter(java.io.OutputStream out) {
        this(out, false, 8192);
    }

    public FastPrintWriter(java.io.OutputStream out, boolean autoFlush) {
        this(out, autoFlush, 8192);
    }

    public FastPrintWriter(java.io.OutputStream out, boolean autoFlush, int bufferLen) {
        super(sDummyWriter, autoFlush);
        if (out == null) {
            throw new java.lang.NullPointerException("out is null");
        }
        this.mBufferLen = bufferLen;
        this.mText = new char[bufferLen];
        this.mBytes = java.nio.ByteBuffer.allocate(this.mBufferLen);
        this.mOutputStream = out;
        this.mWriter = null;
        this.mPrinter = null;
        this.mAutoFlush = autoFlush;
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.mSeparator = java.lang.System.lineSeparator();
        } else {
            this.mSeparator = "\n";
        }
        initDefaultEncoder();
    }

    public FastPrintWriter(java.io.Writer wr) {
        this(wr, false, 8192);
    }

    public FastPrintWriter(java.io.Writer wr, boolean autoFlush) {
        this(wr, autoFlush, 8192);
    }

    public FastPrintWriter(java.io.Writer wr, boolean autoFlush, int bufferLen) {
        super(sDummyWriter, autoFlush);
        if (wr == null) {
            throw new java.lang.NullPointerException("wr is null");
        }
        this.mBufferLen = bufferLen;
        this.mText = new char[bufferLen];
        this.mBytes = null;
        this.mOutputStream = null;
        this.mWriter = wr;
        this.mPrinter = null;
        this.mAutoFlush = autoFlush;
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.mSeparator = java.lang.System.lineSeparator();
        } else {
            this.mSeparator = "\n";
        }
        initDefaultEncoder();
    }

    public FastPrintWriter(android.util.Printer pr) {
        this(pr, 512);
    }

    public FastPrintWriter(android.util.Printer pr, int bufferLen) {
        super(sDummyWriter, true);
        if (pr == null) {
            throw new java.lang.NullPointerException("pr is null");
        }
        this.mBufferLen = bufferLen;
        this.mText = new char[bufferLen];
        this.mBytes = null;
        this.mOutputStream = null;
        this.mWriter = null;
        this.mPrinter = pr;
        this.mAutoFlush = true;
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.mSeparator = java.lang.System.lineSeparator();
        } else {
            this.mSeparator = "\n";
        }
        initDefaultEncoder();
    }

    private final void initEncoder(java.lang.String csn) throws java.io.UnsupportedEncodingException {
        try {
            this.mCharset = java.nio.charset.Charset.forName(csn).newEncoder();
            this.mCharset.onMalformedInput(java.nio.charset.CodingErrorAction.REPLACE);
            this.mCharset.onUnmappableCharacter(java.nio.charset.CodingErrorAction.REPLACE);
        } catch (java.lang.Exception e) {
            throw new java.io.UnsupportedEncodingException(csn);
        }
    }

    public boolean checkError() {
        boolean z;
        flush();
        synchronized (this.lock) {
            z = this.mIoError;
        }
        return z;
    }

    protected void clearError() {
        synchronized (this.lock) {
            this.mIoError = false;
        }
    }

    protected void setError() {
        synchronized (this.lock) {
            this.mIoError = true;
        }
    }

    private final void initDefaultEncoder() {
        this.mCharset = java.nio.charset.Charset.defaultCharset().newEncoder();
        this.mCharset.onMalformedInput(java.nio.charset.CodingErrorAction.REPLACE);
        this.mCharset.onUnmappableCharacter(java.nio.charset.CodingErrorAction.REPLACE);
    }

    private void appendLocked(char c) throws java.io.IOException {
        int pos = this.mPos;
        if (pos >= this.mBufferLen - 1) {
            flushLocked();
            pos = this.mPos;
        }
        this.mText[pos] = c;
        this.mPos = pos + 1;
    }

    private void appendLocked(java.lang.String str, int i, int length) throws java.io.IOException {
        int BUFFER_LEN = this.mBufferLen;
        if (length > BUFFER_LEN) {
            int end = i + length;
            while (i < end) {
                int next = i + BUFFER_LEN;
                appendLocked(str, i, next < end ? BUFFER_LEN : end - i);
                i = next;
            }
            return;
        }
        int pos = this.mPos;
        if (pos + length > BUFFER_LEN) {
            flushLocked();
            pos = this.mPos;
        }
        str.getChars(i, i + length, this.mText, pos);
        this.mPos = pos + length;
    }

    private void appendLocked(char[] buf, int i, int length) throws java.io.IOException {
        int BUFFER_LEN = this.mBufferLen;
        if (length > BUFFER_LEN) {
            int end = i + length;
            while (i < end) {
                int next = i + BUFFER_LEN;
                appendLocked(buf, i, next < end ? BUFFER_LEN : end - i);
                i = next;
            }
            return;
        }
        int pos = this.mPos;
        if (pos + length > BUFFER_LEN) {
            flushLocked();
            pos = this.mPos;
        }
        java.lang.System.arraycopy(buf, i, this.mText, pos, length);
        this.mPos = pos + length;
    }

    private void flushBytesLocked() throws java.io.IOException {
        int position = this.mBytes.position();
        if (position > 0) {
            this.mBytes.flip();
            this.mOutputStream.write(this.mBytes.array(), 0, position);
            this.mBytes.clear();
        }
    }

    private void flushLocked() throws java.io.IOException {
        if (this.mPos > 0) {
            if (this.mOutputStream != null) {
                java.nio.CharBuffer charBuffer = java.nio.CharBuffer.wrap(this.mText, 0, this.mPos);
                java.nio.charset.CoderResult result = this.mCharset.encode(charBuffer, this.mBytes, true);
                while (!result.isError()) {
                    if (result.isOverflow()) {
                        flushBytesLocked();
                        result = this.mCharset.encode(charBuffer, this.mBytes, true);
                    } else {
                        flushBytesLocked();
                        this.mOutputStream.flush();
                    }
                }
                throw new java.io.IOException(result.toString());
            } else if (this.mWriter != null) {
                this.mWriter.write(this.mText, 0, this.mPos);
                this.mWriter.flush();
            } else {
                int nonEolOff = 0;
                int sepLen = this.mSeparator.length();
                int len = sepLen < this.mPos ? sepLen : this.mPos;
                while (nonEolOff < len && this.mText[(this.mPos - 1) - nonEolOff] == this.mSeparator.charAt((this.mSeparator.length() - 1) - nonEolOff)) {
                    nonEolOff++;
                }
                if (nonEolOff >= this.mPos) {
                    this.mPrinter.println("");
                } else {
                    this.mPrinter.println(new java.lang.String(this.mText, 0, this.mPos - nonEolOff));
                }
            }
            this.mPos = 0;
        }
    }

    public void flush() {
        synchronized (this.lock) {
            try {
                flushLocked();
                if (this.mOutputStream != null) {
                    this.mOutputStream.flush();
                } else if (this.mWriter != null) {
                    this.mWriter.flush();
                }
            } catch (java.io.IOException e) {
                setError();
            }
        }
    }

    public void close() {
        synchronized (this.lock) {
            try {
                flushLocked();
                if (this.mOutputStream != null) {
                    this.mOutputStream.close();
                } else if (this.mWriter != null) {
                    this.mWriter.close();
                }
            } catch (java.io.IOException e) {
                setError();
            }
        }
    }

    public void print(char[] charArray) {
        synchronized (this.lock) {
            try {
                appendLocked(charArray, 0, charArray.length);
            } catch (java.io.IOException e) {
            }
        }
    }

    public void print(char ch) {
        synchronized (this.lock) {
            try {
                appendLocked(ch);
            } catch (java.io.IOException e) {
            }
        }
    }

    public void print(java.lang.String str) {
        if (str == null) {
            str = java.lang.String.valueOf(null);
        }
        synchronized (this.lock) {
            try {
                appendLocked(str, 0, str.length());
            } catch (java.io.IOException e) {
                setError();
            }
        }
    }

    public void print(int inum) {
        if (inum == 0) {
            print("0");
        } else {
            super.print(inum);
        }
    }

    public void print(long lnum) {
        if (lnum == 0) {
            print("0");
        } else {
            super.print(lnum);
        }
    }

    public void println() {
        synchronized (this.lock) {
            try {
                appendLocked(this.mSeparator, 0, this.mSeparator.length());
                if (this.mAutoFlush) {
                    flushLocked();
                }
            } catch (java.io.IOException e) {
                setError();
            }
        }
    }

    public void println(int inum) {
        if (inum == 0) {
            println("0");
        } else {
            super.println(inum);
        }
    }

    public void println(long lnum) {
        if (lnum == 0) {
            println("0");
        } else {
            super.println(lnum);
        }
    }

    public void println(char[] chars) {
        print(chars);
        println();
    }

    public void println(char c) {
        print(c);
        println();
    }

    public void write(char[] buf, int offset, int count) {
        synchronized (this.lock) {
            try {
                appendLocked(buf, offset, count);
            } catch (java.io.IOException e) {
            }
        }
    }

    public void write(int oneChar) {
        synchronized (this.lock) {
            try {
                appendLocked((char) oneChar);
            } catch (java.io.IOException e) {
            }
        }
    }

    public void write(java.lang.String str) {
        synchronized (this.lock) {
            try {
                appendLocked(str, 0, str.length());
            } catch (java.io.IOException e) {
            }
        }
    }

    public void write(java.lang.String str, int offset, int count) {
        synchronized (this.lock) {
            try {
                appendLocked(str, offset, count);
            } catch (java.io.IOException e) {
            }
        }
    }

    public java.io.PrintWriter append(java.lang.CharSequence csq, int start, int end) {
        if (csq == null) {
            csq = com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID;
        }
        java.lang.String output = csq.subSequence(start, end).toString();
        write(output, 0, output.length());
        return this;
    }
}
