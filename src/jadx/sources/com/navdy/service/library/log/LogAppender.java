package com.navdy.service.library.log;

public interface LogAppender {
    void close();

    void d(java.lang.String str, java.lang.String str2);

    void d(java.lang.String str, java.lang.String str2, java.lang.Throwable th);

    void e(java.lang.String str, java.lang.String str2);

    void e(java.lang.String str, java.lang.String str2, java.lang.Throwable th);

    void flush();

    void i(java.lang.String str, java.lang.String str2);

    void i(java.lang.String str, java.lang.String str2, java.lang.Throwable th);

    void v(java.lang.String str, java.lang.String str2);

    void v(java.lang.String str, java.lang.String str2, java.lang.Throwable th);

    void w(java.lang.String str, java.lang.String str2);

    void w(java.lang.String str, java.lang.String str2, java.lang.Throwable th);
}
