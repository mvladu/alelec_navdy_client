package com.navdy.service.library.events.settings;

public final class ScreenConfiguration extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_MARGINBOTTOM = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_MARGINLEFT = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_MARGINRIGHT = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_MARGINTOP = java.lang.Integer.valueOf(0);
    public static final java.lang.Float DEFAULT_SCALEX = java.lang.Float.valueOf(1.0f);
    public static final java.lang.Float DEFAULT_SCALEY = java.lang.Float.valueOf(1.0f);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer marginBottom;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer marginLeft;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer marginRight;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer marginTop;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float scaleX;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 6, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float scaleY;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.settings.ScreenConfiguration> {
        public java.lang.Integer marginBottom;
        public java.lang.Integer marginLeft;
        public java.lang.Integer marginRight;
        public java.lang.Integer marginTop;
        public java.lang.Float scaleX;
        public java.lang.Float scaleY;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.settings.ScreenConfiguration message) {
            super(message);
            if (message != null) {
                this.marginLeft = message.marginLeft;
                this.marginTop = message.marginTop;
                this.marginRight = message.marginRight;
                this.marginBottom = message.marginBottom;
                this.scaleX = message.scaleX;
                this.scaleY = message.scaleY;
            }
        }

        public com.navdy.service.library.events.settings.ScreenConfiguration.Builder marginLeft(java.lang.Integer marginLeft2) {
            this.marginLeft = marginLeft2;
            return this;
        }

        public com.navdy.service.library.events.settings.ScreenConfiguration.Builder marginTop(java.lang.Integer marginTop2) {
            this.marginTop = marginTop2;
            return this;
        }

        public com.navdy.service.library.events.settings.ScreenConfiguration.Builder marginRight(java.lang.Integer marginRight2) {
            this.marginRight = marginRight2;
            return this;
        }

        public com.navdy.service.library.events.settings.ScreenConfiguration.Builder marginBottom(java.lang.Integer marginBottom2) {
            this.marginBottom = marginBottom2;
            return this;
        }

        public com.navdy.service.library.events.settings.ScreenConfiguration.Builder scaleX(java.lang.Float scaleX2) {
            this.scaleX = scaleX2;
            return this;
        }

        public com.navdy.service.library.events.settings.ScreenConfiguration.Builder scaleY(java.lang.Float scaleY2) {
            this.scaleY = scaleY2;
            return this;
        }

        public com.navdy.service.library.events.settings.ScreenConfiguration build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.settings.ScreenConfiguration(this);
        }
    }

    public ScreenConfiguration(java.lang.Integer marginLeft2, java.lang.Integer marginTop2, java.lang.Integer marginRight2, java.lang.Integer marginBottom2, java.lang.Float scaleX2, java.lang.Float scaleY2) {
        this.marginLeft = marginLeft2;
        this.marginTop = marginTop2;
        this.marginRight = marginRight2;
        this.marginBottom = marginBottom2;
        this.scaleX = scaleX2;
        this.scaleY = scaleY2;
    }

    private ScreenConfiguration(com.navdy.service.library.events.settings.ScreenConfiguration.Builder builder) {
        this(builder.marginLeft, builder.marginTop, builder.marginRight, builder.marginBottom, builder.scaleX, builder.scaleY);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.settings.ScreenConfiguration)) {
            return false;
        }
        com.navdy.service.library.events.settings.ScreenConfiguration o = (com.navdy.service.library.events.settings.ScreenConfiguration) other;
        if (!equals((java.lang.Object) this.marginLeft, (java.lang.Object) o.marginLeft) || !equals((java.lang.Object) this.marginTop, (java.lang.Object) o.marginTop) || !equals((java.lang.Object) this.marginRight, (java.lang.Object) o.marginRight) || !equals((java.lang.Object) this.marginBottom, (java.lang.Object) o.marginBottom) || !equals((java.lang.Object) this.scaleX, (java.lang.Object) o.scaleX) || !equals((java.lang.Object) this.scaleY, (java.lang.Object) o.scaleY)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.marginLeft != null ? this.marginLeft.hashCode() : 0) * 37;
        if (this.marginTop != null) {
            i = this.marginTop.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.marginRight != null) {
            i2 = this.marginRight.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.marginBottom != null) {
            i3 = this.marginBottom.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.scaleX != null) {
            i4 = this.scaleX.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.scaleY != null) {
            i5 = this.scaleY.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
