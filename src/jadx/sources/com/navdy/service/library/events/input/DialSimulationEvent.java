package com.navdy.service.library.events.input;

public final class DialSimulationEvent extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction DEFAULT_DIALACTION = com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_CLICK;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction dialAction;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.input.DialSimulationEvent> {
        public com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction dialAction;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.input.DialSimulationEvent message) {
            super(message);
            if (message != null) {
                this.dialAction = message.dialAction;
            }
        }

        public com.navdy.service.library.events.input.DialSimulationEvent.Builder dialAction(com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction dialAction2) {
            this.dialAction = dialAction2;
            return this;
        }

        public com.navdy.service.library.events.input.DialSimulationEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.input.DialSimulationEvent(this);
        }
    }

    public enum DialInputAction implements com.squareup.wire.ProtoEnum {
        DIAL_CLICK(1),
        DIAL_LONG_CLICK(2),
        DIAL_LEFT_TURN(3),
        DIAL_RIGHT_TURN(4);
        
        private final int value;

        private DialInputAction(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DialSimulationEvent(com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction dialAction2) {
        this.dialAction = dialAction2;
    }

    private DialSimulationEvent(com.navdy.service.library.events.input.DialSimulationEvent.Builder builder) {
        this(builder.dialAction);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.input.DialSimulationEvent)) {
            return false;
        }
        return equals((java.lang.Object) this.dialAction, (java.lang.Object) ((com.navdy.service.library.events.input.DialSimulationEvent) other).dialAction);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.dialAction != null ? this.dialAction.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
