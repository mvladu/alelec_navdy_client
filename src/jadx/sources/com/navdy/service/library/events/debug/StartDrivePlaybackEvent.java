package com.navdy.service.library.events.debug;

public final class StartDrivePlaybackEvent extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.Boolean DEFAULT_PLAYSECONDARYLOCATION = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean playSecondaryLocation;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.debug.StartDrivePlaybackEvent> {
        public java.lang.String label;
        public java.lang.Boolean playSecondaryLocation;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.debug.StartDrivePlaybackEvent message) {
            super(message);
            if (message != null) {
                this.label = message.label;
                this.playSecondaryLocation = message.playSecondaryLocation;
            }
        }

        public com.navdy.service.library.events.debug.StartDrivePlaybackEvent.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.debug.StartDrivePlaybackEvent.Builder playSecondaryLocation(java.lang.Boolean playSecondaryLocation2) {
            this.playSecondaryLocation = playSecondaryLocation2;
            return this;
        }

        public com.navdy.service.library.events.debug.StartDrivePlaybackEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.debug.StartDrivePlaybackEvent(this);
        }
    }

    public StartDrivePlaybackEvent(java.lang.String label2, java.lang.Boolean playSecondaryLocation2) {
        this.label = label2;
        this.playSecondaryLocation = playSecondaryLocation2;
    }

    private StartDrivePlaybackEvent(com.navdy.service.library.events.debug.StartDrivePlaybackEvent.Builder builder) {
        this(builder.label, builder.playSecondaryLocation);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.debug.StartDrivePlaybackEvent)) {
            return false;
        }
        com.navdy.service.library.events.debug.StartDrivePlaybackEvent o = (com.navdy.service.library.events.debug.StartDrivePlaybackEvent) other;
        if (!equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals((java.lang.Object) this.playSecondaryLocation, (java.lang.Object) o.playSecondaryLocation)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.label != null) {
            result = this.label.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.playSecondaryLocation != null) {
            i = this.playSecondaryLocation.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
