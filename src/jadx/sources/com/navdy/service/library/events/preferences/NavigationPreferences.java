package com.navdy.service.library.events.preferences;

public final class NavigationPreferences extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_ALLOWAUTOTRAINS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_ALLOWFERRIES = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_ALLOWHIGHWAYS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_ALLOWHOVLANES = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_ALLOWTOLLROADS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_ALLOWTUNNELS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_ALLOWUNPAVEDROADS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_PHONETICTURNBYTURN = java.lang.Boolean.valueOf(false);
    public static final com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic DEFAULT_REROUTEFORTRAFFIC = com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.REROUTE_CONFIRM;
    public static final com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType DEFAULT_ROUTINGTYPE = com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_FASTEST;
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_SHOWTRAFFICINOPENMAP = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_SHOWTRAFFICWHILENAVIGATING = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_SPOKENCAMERAWARNINGS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_SPOKENSPEEDLIMITWARNINGS = java.lang.Boolean.valueOf(true);
    public static final java.lang.Boolean DEFAULT_SPOKENTURNBYTURN = java.lang.Boolean.valueOf(true);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean allowAutoTrains;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean allowFerries;
    @com.squareup.wire.ProtoField(tag = 14, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean allowHOVLanes;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean allowHighways;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean allowTollRoads;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean allowTunnels;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean allowUnpavedRoads;
    @com.squareup.wire.ProtoField(tag = 16, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean phoneticTurnByTurn;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic rerouteForTraffic;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType routingType;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean showTrafficInOpenMap;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean showTrafficWhileNavigating;
    @com.squareup.wire.ProtoField(tag = 15, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean spokenCameraWarnings;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean spokenSpeedLimitWarnings;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean spokenTurnByTurn;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.NavigationPreferences> {
        public java.lang.Boolean allowAutoTrains;
        public java.lang.Boolean allowFerries;
        public java.lang.Boolean allowHOVLanes;
        public java.lang.Boolean allowHighways;
        public java.lang.Boolean allowTollRoads;
        public java.lang.Boolean allowTunnels;
        public java.lang.Boolean allowUnpavedRoads;
        public java.lang.Boolean phoneticTurnByTurn;
        public com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic rerouteForTraffic;
        public com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType routingType;
        public java.lang.Long serial_number;
        public java.lang.Boolean showTrafficInOpenMap;
        public java.lang.Boolean showTrafficWhileNavigating;
        public java.lang.Boolean spokenCameraWarnings;
        public java.lang.Boolean spokenSpeedLimitWarnings;
        public java.lang.Boolean spokenTurnByTurn;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.NavigationPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.rerouteForTraffic = message.rerouteForTraffic;
                this.routingType = message.routingType;
                this.spokenTurnByTurn = message.spokenTurnByTurn;
                this.spokenSpeedLimitWarnings = message.spokenSpeedLimitWarnings;
                this.showTrafficInOpenMap = message.showTrafficInOpenMap;
                this.showTrafficWhileNavigating = message.showTrafficWhileNavigating;
                this.allowHighways = message.allowHighways;
                this.allowTollRoads = message.allowTollRoads;
                this.allowFerries = message.allowFerries;
                this.allowTunnels = message.allowTunnels;
                this.allowUnpavedRoads = message.allowUnpavedRoads;
                this.allowAutoTrains = message.allowAutoTrains;
                this.allowHOVLanes = message.allowHOVLanes;
                this.spokenCameraWarnings = message.spokenCameraWarnings;
                this.phoneticTurnByTurn = message.phoneticTurnByTurn;
            }
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder rerouteForTraffic(com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic rerouteForTraffic2) {
            this.rerouteForTraffic = rerouteForTraffic2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder routingType(com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType routingType2) {
            this.routingType = routingType2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder spokenTurnByTurn(java.lang.Boolean spokenTurnByTurn2) {
            this.spokenTurnByTurn = spokenTurnByTurn2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder spokenSpeedLimitWarnings(java.lang.Boolean spokenSpeedLimitWarnings2) {
            this.spokenSpeedLimitWarnings = spokenSpeedLimitWarnings2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder showTrafficInOpenMap(java.lang.Boolean showTrafficInOpenMap2) {
            this.showTrafficInOpenMap = showTrafficInOpenMap2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder showTrafficWhileNavigating(java.lang.Boolean showTrafficWhileNavigating2) {
            this.showTrafficWhileNavigating = showTrafficWhileNavigating2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder allowHighways(java.lang.Boolean allowHighways2) {
            this.allowHighways = allowHighways2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder allowTollRoads(java.lang.Boolean allowTollRoads2) {
            this.allowTollRoads = allowTollRoads2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder allowFerries(java.lang.Boolean allowFerries2) {
            this.allowFerries = allowFerries2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder allowTunnels(java.lang.Boolean allowTunnels2) {
            this.allowTunnels = allowTunnels2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder allowUnpavedRoads(java.lang.Boolean allowUnpavedRoads2) {
            this.allowUnpavedRoads = allowUnpavedRoads2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder allowAutoTrains(java.lang.Boolean allowAutoTrains2) {
            this.allowAutoTrains = allowAutoTrains2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder allowHOVLanes(java.lang.Boolean allowHOVLanes2) {
            this.allowHOVLanes = allowHOVLanes2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder spokenCameraWarnings(java.lang.Boolean spokenCameraWarnings2) {
            this.spokenCameraWarnings = spokenCameraWarnings2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences.Builder phoneticTurnByTurn(java.lang.Boolean phoneticTurnByTurn2) {
            this.phoneticTurnByTurn = phoneticTurnByTurn2;
            return this;
        }

        public com.navdy.service.library.events.preferences.NavigationPreferences build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.preferences.NavigationPreferences(this);
        }
    }

    public enum RerouteForTraffic implements com.squareup.wire.ProtoEnum {
        REROUTE_CONFIRM(0),
        REROUTE_AUTOMATIC(1),
        REROUTE_NEVER(2);
        
        private final int value;

        private RerouteForTraffic(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum RoutingType implements com.squareup.wire.ProtoEnum {
        ROUTING_FASTEST(0),
        ROUTING_SHORTEST(1);
        
        private final int value;

        private RoutingType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavigationPreferences(java.lang.Long serial_number2, com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic rerouteForTraffic2, com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType routingType2, java.lang.Boolean spokenTurnByTurn2, java.lang.Boolean spokenSpeedLimitWarnings2, java.lang.Boolean showTrafficInOpenMap2, java.lang.Boolean showTrafficWhileNavigating2, java.lang.Boolean allowHighways2, java.lang.Boolean allowTollRoads2, java.lang.Boolean allowFerries2, java.lang.Boolean allowTunnels2, java.lang.Boolean allowUnpavedRoads2, java.lang.Boolean allowAutoTrains2, java.lang.Boolean allowHOVLanes2, java.lang.Boolean spokenCameraWarnings2, java.lang.Boolean phoneticTurnByTurn2) {
        this.serial_number = serial_number2;
        this.rerouteForTraffic = rerouteForTraffic2;
        this.routingType = routingType2;
        this.spokenTurnByTurn = spokenTurnByTurn2;
        this.spokenSpeedLimitWarnings = spokenSpeedLimitWarnings2;
        this.showTrafficInOpenMap = showTrafficInOpenMap2;
        this.showTrafficWhileNavigating = showTrafficWhileNavigating2;
        this.allowHighways = allowHighways2;
        this.allowTollRoads = allowTollRoads2;
        this.allowFerries = allowFerries2;
        this.allowTunnels = allowTunnels2;
        this.allowUnpavedRoads = allowUnpavedRoads2;
        this.allowAutoTrains = allowAutoTrains2;
        this.allowHOVLanes = allowHOVLanes2;
        this.spokenCameraWarnings = spokenCameraWarnings2;
        this.phoneticTurnByTurn = phoneticTurnByTurn2;
    }

    private NavigationPreferences(com.navdy.service.library.events.preferences.NavigationPreferences.Builder builder) {
        this(builder.serial_number, builder.rerouteForTraffic, builder.routingType, builder.spokenTurnByTurn, builder.spokenSpeedLimitWarnings, builder.showTrafficInOpenMap, builder.showTrafficWhileNavigating, builder.allowHighways, builder.allowTollRoads, builder.allowFerries, builder.allowTunnels, builder.allowUnpavedRoads, builder.allowAutoTrains, builder.allowHOVLanes, builder.spokenCameraWarnings, builder.phoneticTurnByTurn);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.NavigationPreferences)) {
            return false;
        }
        com.navdy.service.library.events.preferences.NavigationPreferences o = (com.navdy.service.library.events.preferences.NavigationPreferences) other;
        if (!equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals((java.lang.Object) this.rerouteForTraffic, (java.lang.Object) o.rerouteForTraffic) || !equals((java.lang.Object) this.routingType, (java.lang.Object) o.routingType) || !equals((java.lang.Object) this.spokenTurnByTurn, (java.lang.Object) o.spokenTurnByTurn) || !equals((java.lang.Object) this.spokenSpeedLimitWarnings, (java.lang.Object) o.spokenSpeedLimitWarnings) || !equals((java.lang.Object) this.showTrafficInOpenMap, (java.lang.Object) o.showTrafficInOpenMap) || !equals((java.lang.Object) this.showTrafficWhileNavigating, (java.lang.Object) o.showTrafficWhileNavigating) || !equals((java.lang.Object) this.allowHighways, (java.lang.Object) o.allowHighways) || !equals((java.lang.Object) this.allowTollRoads, (java.lang.Object) o.allowTollRoads) || !equals((java.lang.Object) this.allowFerries, (java.lang.Object) o.allowFerries) || !equals((java.lang.Object) this.allowTunnels, (java.lang.Object) o.allowTunnels) || !equals((java.lang.Object) this.allowUnpavedRoads, (java.lang.Object) o.allowUnpavedRoads) || !equals((java.lang.Object) this.allowAutoTrains, (java.lang.Object) o.allowAutoTrains) || !equals((java.lang.Object) this.allowHOVLanes, (java.lang.Object) o.allowHOVLanes) || !equals((java.lang.Object) this.spokenCameraWarnings, (java.lang.Object) o.spokenCameraWarnings) || !equals((java.lang.Object) this.phoneticTurnByTurn, (java.lang.Object) o.phoneticTurnByTurn)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.rerouteForTraffic != null) {
            i = this.rerouteForTraffic.hashCode();
        } else {
            i = 0;
        }
        int i16 = (hashCode + i) * 37;
        if (this.routingType != null) {
            i2 = this.routingType.hashCode();
        } else {
            i2 = 0;
        }
        int i17 = (i16 + i2) * 37;
        if (this.spokenTurnByTurn != null) {
            i3 = this.spokenTurnByTurn.hashCode();
        } else {
            i3 = 0;
        }
        int i18 = (i17 + i3) * 37;
        if (this.spokenSpeedLimitWarnings != null) {
            i4 = this.spokenSpeedLimitWarnings.hashCode();
        } else {
            i4 = 0;
        }
        int i19 = (i18 + i4) * 37;
        if (this.showTrafficInOpenMap != null) {
            i5 = this.showTrafficInOpenMap.hashCode();
        } else {
            i5 = 0;
        }
        int i20 = (i19 + i5) * 37;
        if (this.showTrafficWhileNavigating != null) {
            i6 = this.showTrafficWhileNavigating.hashCode();
        } else {
            i6 = 0;
        }
        int i21 = (i20 + i6) * 37;
        if (this.allowHighways != null) {
            i7 = this.allowHighways.hashCode();
        } else {
            i7 = 0;
        }
        int i22 = (i21 + i7) * 37;
        if (this.allowTollRoads != null) {
            i8 = this.allowTollRoads.hashCode();
        } else {
            i8 = 0;
        }
        int i23 = (i22 + i8) * 37;
        if (this.allowFerries != null) {
            i9 = this.allowFerries.hashCode();
        } else {
            i9 = 0;
        }
        int i24 = (i23 + i9) * 37;
        if (this.allowTunnels != null) {
            i10 = this.allowTunnels.hashCode();
        } else {
            i10 = 0;
        }
        int i25 = (i24 + i10) * 37;
        if (this.allowUnpavedRoads != null) {
            i11 = this.allowUnpavedRoads.hashCode();
        } else {
            i11 = 0;
        }
        int i26 = (i25 + i11) * 37;
        if (this.allowAutoTrains != null) {
            i12 = this.allowAutoTrains.hashCode();
        } else {
            i12 = 0;
        }
        int i27 = (i26 + i12) * 37;
        if (this.allowHOVLanes != null) {
            i13 = this.allowHOVLanes.hashCode();
        } else {
            i13 = 0;
        }
        int i28 = (i27 + i13) * 37;
        if (this.spokenCameraWarnings != null) {
            i14 = this.spokenCameraWarnings.hashCode();
        } else {
            i14 = 0;
        }
        int i29 = (i28 + i14) * 37;
        if (this.phoneticTurnByTurn != null) {
            i15 = this.phoneticTurnByTurn.hashCode();
        }
        int result2 = i29 + i15;
        this.hashCode = result2;
        return result2;
    }
}
