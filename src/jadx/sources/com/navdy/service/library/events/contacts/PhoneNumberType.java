package com.navdy.service.library.events.contacts;

public enum PhoneNumberType implements com.squareup.wire.ProtoEnum {
    PHONE_NUMBER_HOME(1),
    PHONE_NUMBER_WORK(2),
    PHONE_NUMBER_MOBILE(3),
    PHONE_NUMBER_OTHER(4);
    
    private final int value;

    private PhoneNumberType(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
