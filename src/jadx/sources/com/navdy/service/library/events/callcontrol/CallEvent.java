package com.navdy.service.library.events.callcontrol;

public final class CallEvent extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_ANSWERED = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_CONTACT_NAME = "";
    public static final java.lang.Long DEFAULT_DURATION = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_INCOMING = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_NUMBER = "";
    public static final java.lang.Long DEFAULT_START_TIME = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean answered;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String contact_name;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 6, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long duration;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean incoming;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long start_time;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.callcontrol.CallEvent> {
        public java.lang.Boolean answered;
        public java.lang.String contact_name;
        public java.lang.Long duration;
        public java.lang.Boolean incoming;
        public java.lang.String number;
        public java.lang.Long start_time;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.callcontrol.CallEvent message) {
            super(message);
            if (message != null) {
                this.incoming = message.incoming;
                this.answered = message.answered;
                this.number = message.number;
                this.contact_name = message.contact_name;
                this.start_time = message.start_time;
                this.duration = message.duration;
            }
        }

        public com.navdy.service.library.events.callcontrol.CallEvent.Builder incoming(java.lang.Boolean incoming2) {
            this.incoming = incoming2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.CallEvent.Builder answered(java.lang.Boolean answered2) {
            this.answered = answered2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.CallEvent.Builder number(java.lang.String number2) {
            this.number = number2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.CallEvent.Builder contact_name(java.lang.String contact_name2) {
            this.contact_name = contact_name2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.CallEvent.Builder start_time(java.lang.Long start_time2) {
            this.start_time = start_time2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.CallEvent.Builder duration(java.lang.Long duration2) {
            this.duration = duration2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.CallEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.callcontrol.CallEvent(this);
        }
    }

    public CallEvent(java.lang.Boolean incoming2, java.lang.Boolean answered2, java.lang.String number2, java.lang.String contact_name2, java.lang.Long start_time2, java.lang.Long duration2) {
        this.incoming = incoming2;
        this.answered = answered2;
        this.number = number2;
        this.contact_name = contact_name2;
        this.start_time = start_time2;
        this.duration = duration2;
    }

    private CallEvent(com.navdy.service.library.events.callcontrol.CallEvent.Builder builder) {
        this(builder.incoming, builder.answered, builder.number, builder.contact_name, builder.start_time, builder.duration);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.callcontrol.CallEvent)) {
            return false;
        }
        com.navdy.service.library.events.callcontrol.CallEvent o = (com.navdy.service.library.events.callcontrol.CallEvent) other;
        if (!equals((java.lang.Object) this.incoming, (java.lang.Object) o.incoming) || !equals((java.lang.Object) this.answered, (java.lang.Object) o.answered) || !equals((java.lang.Object) this.number, (java.lang.Object) o.number) || !equals((java.lang.Object) this.contact_name, (java.lang.Object) o.contact_name) || !equals((java.lang.Object) this.start_time, (java.lang.Object) o.start_time) || !equals((java.lang.Object) this.duration, (java.lang.Object) o.duration)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.incoming != null ? this.incoming.hashCode() : 0) * 37;
        if (this.answered != null) {
            i = this.answered.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.number != null) {
            i2 = this.number.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.contact_name != null) {
            i3 = this.contact_name.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.start_time != null) {
            i4 = this.start_time.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.duration != null) {
            i5 = this.duration.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
