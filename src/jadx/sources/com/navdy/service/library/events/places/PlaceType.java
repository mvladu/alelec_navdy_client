package com.navdy.service.library.events.places;

public enum PlaceType implements com.squareup.wire.ProtoEnum {
    PLACE_TYPE_UNKNOWN(0),
    PLACE_TYPE_ATM(1),
    PLACE_TYPE_BANK(2),
    PLACE_TYPE_SCHOOL(3),
    PLACE_TYPE_STORE(4),
    PLACE_TYPE_GAS(5),
    PLACE_TYPE_AIRPORT(6),
    PLACE_TYPE_PARKING(7),
    PLACE_TYPE_TRANSIT(8),
    PLACE_TYPE_BAR(9),
    PLACE_TYPE_ENTERTAINMENT(10),
    PLACE_TYPE_HOSPITAL(11),
    PLACE_TYPE_COFFEE(12),
    PLACE_TYPE_RESTAURANT(13),
    PLACE_TYPE_GYM(14),
    PLACE_TYPE_PARK(15),
    PLACE_TYPE_CONTACT(1001),
    PLACE_TYPE_CALENDAR_EVENT(1002);
    
    private final int value;

    private PlaceType(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
