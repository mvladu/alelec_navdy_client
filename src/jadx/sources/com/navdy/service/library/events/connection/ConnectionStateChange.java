package com.navdy.service.library.events.connection;

public final class ConnectionStateChange extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_REMOTEDEVICEID = "";
    public static final com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState DEFAULT_STATE = com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String remoteDeviceId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState state;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.connection.ConnectionStateChange> {
        public java.lang.String remoteDeviceId;
        public com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState state;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.connection.ConnectionStateChange message) {
            super(message);
            if (message != null) {
                this.remoteDeviceId = message.remoteDeviceId;
                this.state = message.state;
            }
        }

        public com.navdy.service.library.events.connection.ConnectionStateChange.Builder remoteDeviceId(java.lang.String remoteDeviceId2) {
            this.remoteDeviceId = remoteDeviceId2;
            return this;
        }

        public com.navdy.service.library.events.connection.ConnectionStateChange.Builder state(com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState state2) {
            this.state = state2;
            return this;
        }

        public com.navdy.service.library.events.connection.ConnectionStateChange build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.connection.ConnectionStateChange(this);
        }
    }

    public enum ConnectionState implements com.squareup.wire.ProtoEnum {
        CONNECTION_CONNECTED(1),
        CONNECTION_DISCONNECTED(2),
        CONNECTION_LINK_ESTABLISHED(3),
        CONNECTION_LINK_LOST(4),
        CONNECTION_VERIFIED(5);
        
        private final int value;

        private ConnectionState(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public ConnectionStateChange(java.lang.String remoteDeviceId2, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState state2) {
        this.remoteDeviceId = remoteDeviceId2;
        this.state = state2;
    }

    private ConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange.Builder builder) {
        this(builder.remoteDeviceId, builder.state);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.connection.ConnectionStateChange)) {
            return false;
        }
        com.navdy.service.library.events.connection.ConnectionStateChange o = (com.navdy.service.library.events.connection.ConnectionStateChange) other;
        if (!equals((java.lang.Object) this.remoteDeviceId, (java.lang.Object) o.remoteDeviceId) || !equals((java.lang.Object) this.state, (java.lang.Object) o.state)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.remoteDeviceId != null) {
            result = this.remoteDeviceId.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.state != null) {
            i = this.state.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
