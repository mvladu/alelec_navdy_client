package com.navdy.service.library.events.notification;

public enum NotificationsError implements com.squareup.wire.ProtoEnum {
    NOTIFICATIONS_ERROR_UNKNOWN(1),
    NOTIFICATIONS_ERROR_AUTH_FAILED(2),
    NOTIFICATIONS_ERROR_BOND_REMOVED(3);
    
    private final int value;

    private NotificationsError(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
