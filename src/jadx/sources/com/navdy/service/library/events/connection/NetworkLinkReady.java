package com.navdy.service.library.events.connection;

public final class NetworkLinkReady extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.connection.NetworkLinkReady> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.connection.NetworkLinkReady message) {
            super(message);
        }

        public com.navdy.service.library.events.connection.NetworkLinkReady build() {
            return new com.navdy.service.library.events.connection.NetworkLinkReady(this);
        }
    }

    public NetworkLinkReady() {
    }

    private NetworkLinkReady(com.navdy.service.library.events.connection.NetworkLinkReady.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.connection.NetworkLinkReady;
    }

    public int hashCode() {
        return 0;
    }
}
