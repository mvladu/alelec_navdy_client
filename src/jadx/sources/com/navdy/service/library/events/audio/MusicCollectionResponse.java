package com.navdy.service.library.events.audio;

public final class MusicCollectionResponse extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> DEFAULT_CHARACTERMAP = java.util.Collections.emptyList();
    public static final java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> DEFAULT_MUSICCOLLECTIONS = java.util.Collections.emptyList();
    public static final java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> DEFAULT_MUSICTRACKS = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.audio.MusicCharacterMap.class, tag = 4)
    public final java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> characterMap;
    @com.squareup.wire.ProtoField(tag = 1)
    public final com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.audio.MusicCollectionInfo.class, tag = 2)
    public final java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> musicCollections;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.audio.MusicTrackInfo.class, tag = 3)
    public final java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> musicTracks;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCollectionResponse> {
        public java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> characterMap;
        public com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo;
        public java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> musicCollections;
        public java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> musicTracks;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCollectionResponse message) {
            super(message);
            if (message != null) {
                this.collectionInfo = message.collectionInfo;
                this.musicCollections = com.navdy.service.library.events.audio.MusicCollectionResponse.copyOf(message.musicCollections);
                this.musicTracks = com.navdy.service.library.events.audio.MusicCollectionResponse.copyOf(message.musicTracks);
                this.characterMap = com.navdy.service.library.events.audio.MusicCollectionResponse.copyOf(message.characterMap);
            }
        }

        public com.navdy.service.library.events.audio.MusicCollectionResponse.Builder collectionInfo(com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo2) {
            this.collectionInfo = collectionInfo2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionResponse.Builder musicCollections(java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> musicCollections2) {
            this.musicCollections = checkForNulls(musicCollections2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionResponse.Builder musicTracks(java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> musicTracks2) {
            this.musicTracks = checkForNulls(musicTracks2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionResponse.Builder characterMap(java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> characterMap2) {
            this.characterMap = checkForNulls(characterMap2);
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionResponse build() {
            return new com.navdy.service.library.events.audio.MusicCollectionResponse(this);
        }
    }

    public MusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionInfo collectionInfo2, java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> musicCollections2, java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> musicTracks2, java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> characterMap2) {
        this.collectionInfo = collectionInfo2;
        this.musicCollections = immutableCopyOf(musicCollections2);
        this.musicTracks = immutableCopyOf(musicTracks2);
        this.characterMap = immutableCopyOf(characterMap2);
    }

    private MusicCollectionResponse(com.navdy.service.library.events.audio.MusicCollectionResponse.Builder builder) {
        this(builder.collectionInfo, builder.musicCollections, builder.musicTracks, builder.characterMap);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCollectionResponse)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicCollectionResponse o = (com.navdy.service.library.events.audio.MusicCollectionResponse) other;
        if (!equals((java.lang.Object) this.collectionInfo, (java.lang.Object) o.collectionInfo) || !equals(this.musicCollections, o.musicCollections) || !equals(this.musicTracks, o.musicTracks) || !equals(this.characterMap, o.characterMap)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 1;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.collectionInfo != null ? this.collectionInfo.hashCode() : 0) * 37;
        if (this.musicCollections != null) {
            i = this.musicCollections.hashCode();
        } else {
            i = 1;
        }
        int i4 = (hashCode + i) * 37;
        if (this.musicTracks != null) {
            i2 = this.musicTracks.hashCode();
        } else {
            i2 = 1;
        }
        int i5 = (i4 + i2) * 37;
        if (this.characterMap != null) {
            i3 = this.characterMap.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
