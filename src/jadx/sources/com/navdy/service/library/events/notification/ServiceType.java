package com.navdy.service.library.events.notification;

public enum ServiceType implements com.squareup.wire.ProtoEnum {
    SERVICE_ANCS(1),
    SERVICE_PANDORA(2);
    
    private final int value;

    private ServiceType(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
