package com.navdy.service.library.events.notification;

public final class NotificationEvent extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.notification.NotificationAction> DEFAULT_ACTIONS = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_APPID = "";
    public static final java.lang.String DEFAULT_APPNAME = "";
    public static final java.lang.Boolean DEFAULT_CANNOTREPLYBACK = java.lang.Boolean.valueOf(false);
    public static final com.navdy.service.library.events.notification.NotificationCategory DEFAULT_CATEGORY = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER;
    public static final java.lang.String DEFAULT_ICONRESOURCENAME = "";
    public static final java.lang.Integer DEFAULT_ID = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_IMAGERESOURCENAME = "";
    public static final java.lang.String DEFAULT_MESSAGE = "";
    public static final okio.ByteString DEFAULT_PHOTO = okio.ByteString.EMPTY;
    public static final java.lang.String DEFAULT_SOURCEIDENTIFIER = "";
    public static final java.lang.String DEFAULT_SUBTITLE = "";
    public static final java.lang.String DEFAULT_TITLE = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.notification.NotificationAction.class, tag = 7)
    public final java.util.List<com.navdy.service.library.events.notification.NotificationAction> actions;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String appId;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String appName;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean cannotReplyBack;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.notification.NotificationCategory category;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String iconResourceName;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer id;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String imageResourceName;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String message;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.BYTES)
    public final okio.ByteString photo;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String sourceIdentifier;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String subtitle;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String title;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.notification.NotificationEvent> {
        public java.util.List<com.navdy.service.library.events.notification.NotificationAction> actions;
        public java.lang.String appId;
        public java.lang.String appName;
        public java.lang.Boolean cannotReplyBack;
        public com.navdy.service.library.events.notification.NotificationCategory category;
        public java.lang.String iconResourceName;
        public java.lang.Integer id;
        public java.lang.String imageResourceName;
        public java.lang.String message;
        public okio.ByteString photo;
        public java.lang.String sourceIdentifier;
        public java.lang.String subtitle;
        public java.lang.String title;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.notification.NotificationEvent message2) {
            super(message2);
            if (message2 != null) {
                this.id = message2.id;
                this.category = message2.category;
                this.title = message2.title;
                this.subtitle = message2.subtitle;
                this.message = message2.message;
                this.appId = message2.appId;
                this.actions = com.navdy.service.library.events.notification.NotificationEvent.copyOf(message2.actions);
                this.photo = message2.photo;
                this.iconResourceName = message2.iconResourceName;
                this.imageResourceName = message2.imageResourceName;
                this.sourceIdentifier = message2.sourceIdentifier;
                this.cannotReplyBack = message2.cannotReplyBack;
                this.appName = message2.appName;
            }
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder id(java.lang.Integer id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder category(com.navdy.service.library.events.notification.NotificationCategory category2) {
            this.category = category2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder title(java.lang.String title2) {
            this.title = title2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder subtitle(java.lang.String subtitle2) {
            this.subtitle = subtitle2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder message(java.lang.String message2) {
            this.message = message2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder appId(java.lang.String appId2) {
            this.appId = appId2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder actions(java.util.List<com.navdy.service.library.events.notification.NotificationAction> actions2) {
            this.actions = checkForNulls(actions2);
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder photo(okio.ByteString photo2) {
            this.photo = photo2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder iconResourceName(java.lang.String iconResourceName2) {
            this.iconResourceName = iconResourceName2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder imageResourceName(java.lang.String imageResourceName2) {
            this.imageResourceName = imageResourceName2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder sourceIdentifier(java.lang.String sourceIdentifier2) {
            this.sourceIdentifier = sourceIdentifier2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder cannotReplyBack(java.lang.Boolean cannotReplyBack2) {
            this.cannotReplyBack = cannotReplyBack2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent.Builder appName(java.lang.String appName2) {
            this.appName = appName2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.notification.NotificationEvent(this);
        }
    }

    public NotificationEvent(java.lang.Integer id2, com.navdy.service.library.events.notification.NotificationCategory category2, java.lang.String title2, java.lang.String subtitle2, java.lang.String message2, java.lang.String appId2, java.util.List<com.navdy.service.library.events.notification.NotificationAction> actions2, okio.ByteString photo2, java.lang.String iconResourceName2, java.lang.String imageResourceName2, java.lang.String sourceIdentifier2, java.lang.Boolean cannotReplyBack2, java.lang.String appName2) {
        this.id = id2;
        this.category = category2;
        this.title = title2;
        this.subtitle = subtitle2;
        this.message = message2;
        this.appId = appId2;
        this.actions = immutableCopyOf(actions2);
        this.photo = photo2;
        this.iconResourceName = iconResourceName2;
        this.imageResourceName = imageResourceName2;
        this.sourceIdentifier = sourceIdentifier2;
        this.cannotReplyBack = cannotReplyBack2;
        this.appName = appName2;
    }

    private NotificationEvent(com.navdy.service.library.events.notification.NotificationEvent.Builder builder) {
        this(builder.id, builder.category, builder.title, builder.subtitle, builder.message, builder.appId, builder.actions, builder.photo, builder.iconResourceName, builder.imageResourceName, builder.sourceIdentifier, builder.cannotReplyBack, builder.appName);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.notification.NotificationEvent)) {
            return false;
        }
        com.navdy.service.library.events.notification.NotificationEvent o = (com.navdy.service.library.events.notification.NotificationEvent) other;
        if (!equals((java.lang.Object) this.id, (java.lang.Object) o.id) || !equals((java.lang.Object) this.category, (java.lang.Object) o.category) || !equals((java.lang.Object) this.title, (java.lang.Object) o.title) || !equals((java.lang.Object) this.subtitle, (java.lang.Object) o.subtitle) || !equals((java.lang.Object) this.message, (java.lang.Object) o.message) || !equals((java.lang.Object) this.appId, (java.lang.Object) o.appId) || !equals(this.actions, o.actions) || !equals((java.lang.Object) this.photo, (java.lang.Object) o.photo) || !equals((java.lang.Object) this.iconResourceName, (java.lang.Object) o.iconResourceName) || !equals((java.lang.Object) this.imageResourceName, (java.lang.Object) o.imageResourceName) || !equals((java.lang.Object) this.sourceIdentifier, (java.lang.Object) o.sourceIdentifier) || !equals((java.lang.Object) this.cannotReplyBack, (java.lang.Object) o.cannotReplyBack) || !equals((java.lang.Object) this.appName, (java.lang.Object) o.appName)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.id != null ? this.id.hashCode() : 0) * 37;
        if (this.category != null) {
            i = this.category.hashCode();
        } else {
            i = 0;
        }
        int i12 = (hashCode + i) * 37;
        if (this.title != null) {
            i2 = this.title.hashCode();
        } else {
            i2 = 0;
        }
        int i13 = (i12 + i2) * 37;
        if (this.subtitle != null) {
            i3 = this.subtitle.hashCode();
        } else {
            i3 = 0;
        }
        int i14 = (i13 + i3) * 37;
        if (this.message != null) {
            i4 = this.message.hashCode();
        } else {
            i4 = 0;
        }
        int i15 = (i14 + i4) * 37;
        if (this.appId != null) {
            i5 = this.appId.hashCode();
        } else {
            i5 = 0;
        }
        int hashCode2 = (((i15 + i5) * 37) + (this.actions != null ? this.actions.hashCode() : 1)) * 37;
        if (this.photo != null) {
            i6 = this.photo.hashCode();
        } else {
            i6 = 0;
        }
        int i16 = (hashCode2 + i6) * 37;
        if (this.iconResourceName != null) {
            i7 = this.iconResourceName.hashCode();
        } else {
            i7 = 0;
        }
        int i17 = (i16 + i7) * 37;
        if (this.imageResourceName != null) {
            i8 = this.imageResourceName.hashCode();
        } else {
            i8 = 0;
        }
        int i18 = (i17 + i8) * 37;
        if (this.sourceIdentifier != null) {
            i9 = this.sourceIdentifier.hashCode();
        } else {
            i9 = 0;
        }
        int i19 = (i18 + i9) * 37;
        if (this.cannotReplyBack != null) {
            i10 = this.cannotReplyBack.hashCode();
        } else {
            i10 = 0;
        }
        int i20 = (i19 + i10) * 37;
        if (this.appName != null) {
            i11 = this.appName.hashCode();
        }
        int result2 = i20 + i11;
        this.hashCode = result2;
        return result2;
    }
}
