package com.navdy.service.library.events.audio;

public final class MusicCollectionFilter extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_FIELD = "";
    public static final java.lang.String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String field;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String value;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCollectionFilter> {
        public java.lang.String field;
        public java.lang.String value;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCollectionFilter message) {
            super(message);
            if (message != null) {
                this.field = message.field;
                this.value = message.value;
            }
        }

        public com.navdy.service.library.events.audio.MusicCollectionFilter.Builder field(java.lang.String field2) {
            this.field = field2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionFilter.Builder value(java.lang.String value2) {
            this.value = value2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCollectionFilter build() {
            return new com.navdy.service.library.events.audio.MusicCollectionFilter(this);
        }
    }

    public MusicCollectionFilter(java.lang.String field2, java.lang.String value2) {
        this.field = field2;
        this.value = value2;
    }

    private MusicCollectionFilter(com.navdy.service.library.events.audio.MusicCollectionFilter.Builder builder) {
        this(builder.field, builder.value);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCollectionFilter)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicCollectionFilter o = (com.navdy.service.library.events.audio.MusicCollectionFilter) other;
        if (!equals((java.lang.Object) this.field, (java.lang.Object) o.field) || !equals((java.lang.Object) this.value, (java.lang.Object) o.value)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.field != null) {
            result = this.field.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.value != null) {
            i = this.value.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
