package com.navdy.service.library.events.audio;

public final class MusicCharacterMap extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_CHARACTER = "";
    public static final java.lang.Integer DEFAULT_OFFSET = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String character;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer offset;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCharacterMap> {
        public java.lang.String character;
        public java.lang.Integer offset;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCharacterMap message) {
            super(message);
            if (message != null) {
                this.character = message.character;
                this.offset = message.offset;
            }
        }

        public com.navdy.service.library.events.audio.MusicCharacterMap.Builder character(java.lang.String character2) {
            this.character = character2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCharacterMap.Builder offset(java.lang.Integer offset2) {
            this.offset = offset2;
            return this;
        }

        public com.navdy.service.library.events.audio.MusicCharacterMap build() {
            return new com.navdy.service.library.events.audio.MusicCharacterMap(this);
        }
    }

    public MusicCharacterMap(java.lang.String character2, java.lang.Integer offset2) {
        this.character = character2;
        this.offset = offset2;
    }

    private MusicCharacterMap(com.navdy.service.library.events.audio.MusicCharacterMap.Builder builder) {
        this(builder.character, builder.offset);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.MusicCharacterMap)) {
            return false;
        }
        com.navdy.service.library.events.audio.MusicCharacterMap o = (com.navdy.service.library.events.audio.MusicCharacterMap) other;
        if (!equals((java.lang.Object) this.character, (java.lang.Object) o.character) || !equals((java.lang.Object) this.offset, (java.lang.Object) o.offset)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.character != null) {
            result = this.character.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.offset != null) {
            i = this.offset.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
