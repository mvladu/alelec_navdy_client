package com.navdy.service.library.events.navigation;

public final class NavigationSessionDeferRequest extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_DELAY = java.lang.Integer.valueOf(0);
    public static final java.lang.Long DEFAULT_EXPIRETIMESTAMP = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_ORIGINDISPLAY = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_REQUESTID = "";
    public static final java.lang.String DEFAULT_ROUTEID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer delay;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long expireTimestamp;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean originDisplay;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String requestId;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String routeId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationSessionDeferRequest> {
        public java.lang.Integer delay;
        public java.lang.Long expireTimestamp;
        public java.lang.Boolean originDisplay;
        public java.lang.String requestId;
        public java.lang.String routeId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest message) {
            super(message);
            if (message != null) {
                this.requestId = message.requestId;
                this.routeId = message.routeId;
                this.delay = message.delay;
                this.originDisplay = message.originDisplay;
                this.expireTimestamp = message.expireTimestamp;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest.Builder requestId(java.lang.String requestId2) {
            this.requestId = requestId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest.Builder routeId(java.lang.String routeId2) {
            this.routeId = routeId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest.Builder delay(java.lang.Integer delay2) {
            this.delay = delay2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest.Builder originDisplay(java.lang.Boolean originDisplay2) {
            this.originDisplay = originDisplay2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest.Builder expireTimestamp(java.lang.Long expireTimestamp2) {
            this.expireTimestamp = expireTimestamp2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationSessionDeferRequest(this);
        }
    }

    public NavigationSessionDeferRequest(java.lang.String requestId2, java.lang.String routeId2, java.lang.Integer delay2, java.lang.Boolean originDisplay2, java.lang.Long expireTimestamp2) {
        this.requestId = requestId2;
        this.routeId = routeId2;
        this.delay = delay2;
        this.originDisplay = originDisplay2;
        this.expireTimestamp = expireTimestamp2;
    }

    private NavigationSessionDeferRequest(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest.Builder builder) {
        this(builder.requestId, builder.routeId, builder.delay, builder.originDisplay, builder.expireTimestamp);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationSessionDeferRequest)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationSessionDeferRequest o = (com.navdy.service.library.events.navigation.NavigationSessionDeferRequest) other;
        if (!equals((java.lang.Object) this.requestId, (java.lang.Object) o.requestId) || !equals((java.lang.Object) this.routeId, (java.lang.Object) o.routeId) || !equals((java.lang.Object) this.delay, (java.lang.Object) o.delay) || !equals((java.lang.Object) this.originDisplay, (java.lang.Object) o.originDisplay) || !equals((java.lang.Object) this.expireTimestamp, (java.lang.Object) o.expireTimestamp)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.requestId != null ? this.requestId.hashCode() : 0) * 37;
        if (this.routeId != null) {
            i = this.routeId.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.delay != null) {
            i2 = this.delay.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.originDisplay != null) {
            i3 = this.originDisplay.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.expireTimestamp != null) {
            i4 = this.expireTimestamp.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
