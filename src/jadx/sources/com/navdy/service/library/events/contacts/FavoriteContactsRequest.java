package com.navdy.service.library.events.contacts;

public final class FavoriteContactsRequest extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_MAXCONTACTS = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer maxContacts;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.contacts.FavoriteContactsRequest> {
        public java.lang.Integer maxContacts;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.contacts.FavoriteContactsRequest message) {
            super(message);
            if (message != null) {
                this.maxContacts = message.maxContacts;
            }
        }

        public com.navdy.service.library.events.contacts.FavoriteContactsRequest.Builder maxContacts(java.lang.Integer maxContacts2) {
            this.maxContacts = maxContacts2;
            return this;
        }

        public com.navdy.service.library.events.contacts.FavoriteContactsRequest build() {
            return new com.navdy.service.library.events.contacts.FavoriteContactsRequest(this);
        }
    }

    public FavoriteContactsRequest(java.lang.Integer maxContacts2) {
        this.maxContacts = maxContacts2;
    }

    private FavoriteContactsRequest(com.navdy.service.library.events.contacts.FavoriteContactsRequest.Builder builder) {
        this(builder.maxContacts);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.contacts.FavoriteContactsRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.maxContacts, (java.lang.Object) ((com.navdy.service.library.events.contacts.FavoriteContactsRequest) other).maxContacts);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.maxContacts != null ? this.maxContacts.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
