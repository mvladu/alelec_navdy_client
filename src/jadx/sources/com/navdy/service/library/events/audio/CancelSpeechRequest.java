package com.navdy.service.library.events.audio;

public final class CancelSpeechRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String id;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.CancelSpeechRequest> {
        public java.lang.String id;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.CancelSpeechRequest message) {
            super(message);
            if (message != null) {
                this.id = message.id;
            }
        }

        public com.navdy.service.library.events.audio.CancelSpeechRequest.Builder id(java.lang.String id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.audio.CancelSpeechRequest build() {
            return new com.navdy.service.library.events.audio.CancelSpeechRequest(this);
        }
    }

    public CancelSpeechRequest(java.lang.String id2) {
        this.id = id2;
    }

    private CancelSpeechRequest(com.navdy.service.library.events.audio.CancelSpeechRequest.Builder builder) {
        this(builder.id);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.CancelSpeechRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.id, (java.lang.Object) ((com.navdy.service.library.events.audio.CancelSpeechRequest) other).id);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.id != null ? this.id.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
