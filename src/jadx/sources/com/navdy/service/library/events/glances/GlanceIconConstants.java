package com.navdy.service.library.events.glances;

public enum GlanceIconConstants implements com.squareup.wire.ProtoEnum {
    GLANCE_ICON_NAVDY_MAIN(0),
    GLANCE_ICON_MESSAGE_SIDE_BLUE(1);
    
    private final int value;

    private GlanceIconConstants(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
