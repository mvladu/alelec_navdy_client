package com.navdy.service.library.events.audio;

public enum MusicRepeatMode implements com.squareup.wire.ProtoEnum {
    MUSIC_REPEAT_MODE_UNKNOWN(1),
    MUSIC_REPEAT_MODE_OFF(2),
    MUSIC_REPEAT_MODE_ONE(3),
    MUSIC_REPEAT_MODE_ALL(4);
    
    private final int value;

    private MusicRepeatMode(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
