package com.navdy.service.library.events.file;

public final class PreviewFileRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_FILENAME = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String filename;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.PreviewFileRequest> {
        public java.lang.String filename;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.PreviewFileRequest message) {
            super(message);
            if (message != null) {
                this.filename = message.filename;
            }
        }

        public com.navdy.service.library.events.file.PreviewFileRequest.Builder filename(java.lang.String filename2) {
            this.filename = filename2;
            return this;
        }

        public com.navdy.service.library.events.file.PreviewFileRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.PreviewFileRequest(this);
        }
    }

    public PreviewFileRequest(java.lang.String filename2) {
        this.filename = filename2;
    }

    private PreviewFileRequest(com.navdy.service.library.events.file.PreviewFileRequest.Builder builder) {
        this(builder.filename);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.PreviewFileRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.filename, (java.lang.Object) ((com.navdy.service.library.events.file.PreviewFileRequest) other).filename);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.filename != null ? this.filename.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
