package com.navdy.service.library.events;

public final class NavdyEvent extends com.squareup.wire.ExtendableMessage<com.navdy.service.library.events.NavdyEvent> {
    public static final com.navdy.service.library.events.NavdyEvent.MessageType DEFAULT_TYPE = com.navdy.service.library.events.NavdyEvent.MessageType.Coordinate;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.NavdyEvent.MessageType type;

    public static final class Builder extends com.squareup.wire.ExtendableMessage.ExtendableBuilder<com.navdy.service.library.events.NavdyEvent> {
        public com.navdy.service.library.events.NavdyEvent.MessageType type;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.NavdyEvent message) {
            super(message);
            if (message != null) {
                this.type = message.type;
            }
        }

        public com.navdy.service.library.events.NavdyEvent.Builder type(com.navdy.service.library.events.NavdyEvent.MessageType type2) {
            this.type = type2;
            return this;
        }

        public <E> com.navdy.service.library.events.NavdyEvent.Builder setExtension(com.squareup.wire.Extension<com.navdy.service.library.events.NavdyEvent, E> extension, E value) {
            super.setExtension(extension, value);
            return this;
        }

        public com.navdy.service.library.events.NavdyEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.NavdyEvent(this);
        }
    }

    public enum MessageType implements com.squareup.wire.ProtoEnum {
        Coordinate(1),
        Notification(2),
        CallEvent(3),
        FileListRequest(4),
        FileListResponse(5),
        PreviewFileRequest(6),
        PreviewFileResponse(7),
        NavigationManeuverEvent(8),
        NavigationRouteRequest(9),
        NavigationRouteResponse(10),
        NavigationSessionRequest(11),
        NavigationSessionResponse(12),
        NavigationSessionStatusEvent(13),
        DismissScreen(14),
        ShowScreen(15),
        GestureEvent(16),
        PlacesSearchRequest(17),
        PlacesSearchResponse(18),
        ReadSettingsRequest(19),
        ReadSettingsResponse(20),
        UpdateSettings(21),
        ConnectionStateChange(22),
        SpeechRequest(23),
        TelephonyRequest(24),
        TelephonyResponse(25),
        NotificationEvent(26),
        DeviceInfo(27),
        PhoneEvent(28),
        VoiceAssistRequest(29),
        PhotoRequest(30),
        PhotoResponse(31),
        NotificationListRequest(32),
        NotificationListResponse(33),
        ShowCustomNotification(34),
        DateTimeConfiguration(35),
        FileTransferResponse(36),
        FileTransferRequest(37),
        FileTransferData(38),
        FileTransferStatus(39),
        Ping(40),
        NotificationsStateChange(41),
        NotificationsStateRequest(42),
        MusicTrackInfoRequest(43),
        MusicTrackInfo(44),
        MusicEvent(45),
        DialStatusRequest(46),
        DialStatusResponse(47),
        DialBondRequest(48),
        DialBondResponse(49),
        DriverProfilePreferencesRequest(50),
        DriverProfilePreferencesUpdate(51),
        TransmitLocation(52),
        DisconnectRequest(53),
        VoiceAssistResponse(54),
        GetNavigationSessionState(55),
        RouteManeuverRequest(56),
        RouteManeuverResponse(57),
        NavigationPreferencesRequest(58),
        NavigationPreferencesUpdate(59),
        DialSimulationEvent(60),
        MediaRemoteKeyEvent(61),
        ContactRequest(62),
        ContactResponse(63),
        FavoriteContactsRequest(64),
        FavoriteContactsResponse(65),
        FavoriteDestinationsRequest(66),
        FavoriteDestinationsUpdate(67),
        RecommendedDestinationsRequest(68),
        RecommendedDestinationsUpdate(69),
        ConnectionRequest(70),
        ConnectionStatus(71),
        PhoneStatusRequest(72),
        PhoneStatusResponse(73),
        LaunchAppEvent(74),
        InputPreferencesRequest(75),
        InputPreferencesUpdate(76),
        PhoneBatteryStatus(77),
        SmsMessageRequest(78),
        SmsMessageResponse(79),
        ObdStatusRequest(80),
        ObdStatusResponse(81),
        TripUpdate(82),
        TripUpdateAck(83),
        PhotoUpdatesRequest(84),
        PhotoUpdate(85),
        DisplaySpeakerPreferencesRequest(86),
        DisplaySpeakerPreferencesUpdate(87),
        NavigationRouteStatus(88),
        NavigationRouteCancelRequest(89),
        NotificationPreferencesRequest(90),
        NotificationPreferencesUpdate(91),
        StartDriveRecordingEvent(92),
        StopDriveRecordingEvent(93),
        DriveRecordingsRequest(94),
        DriveRecordingsResponse(95),
        StartDrivePlaybackEvent(96),
        StopDrivePlaybackEvent(97),
        StartDriveRecordingResponse(98),
        StartDrivePlaybackResponse(99),
        NavigationSessionRouteChange(100),
        NetworkStateChange(101),
        StopDriveRecordingResponse(102),
        NavigationSessionDeferRequest(103),
        CallStateUpdateRequest(104),
        AutoCompleteRequest(105),
        AutoCompleteResponse(106),
        GlanceEvent(107),
        NowPlayingUpdateRequest(108),
        CancelSpeechRequest(109),
        NetworkLinkReady(110),
        SuggestedDestination(111),
        CalendarEvent(112),
        CalendarEventUpdates(113),
        SpeechRequestStatus(114),
        AudioStatus(115),
        LinkPropertiesChanged(116),
        VoiceSearchRequest(117),
        VoiceSearchResponse(118),
        MusicCollectionRequest(119),
        MusicCollectionResponse(120),
        MusicCapabilitiesRequest(com.here.posclient.analytics.TrackerEvent.PositioningHybridOutdoor),
        MusicCapabilitiesResponse(122),
        MusicArtworkRequest(123),
        MusicArtworkResponse(124),
        DashboardPreferences(125),
        ClearGlances(android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PLAY),
        PlaceTypeSearchRequest(android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE),
        PlaceTypeSearchResponse(128),
        AccelerateShutdown(129),
        DestinationSelectedRequest(android.support.v4.media.TransportMediator.KEYCODE_MEDIA_RECORD),
        DestinationSelectedResponse(com.here.posclient.analytics.TrackerEvent.PositioningOfflineOutdoor),
        PhotoUpdateQuery(com.here.posclient.analytics.TrackerEvent.PositioningOfflineCommonIndoor),
        PhotoUpdateQueryResponse(com.here.posclient.analytics.TrackerEvent.PositioningOfflinePrivateIndoor),
        ResumeMusicRequest(134),
        CannedMessagesRequest(135),
        CannedMessagesUpdate(136),
        MusicCollectionSourceUpdate(137),
        PlayAudioRequest(138),
        AudioPreferencesRequest(139),
        AudioPreferencesUpdate(140);
        
        private final int value;

        private MessageType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavdyEvent(com.navdy.service.library.events.NavdyEvent.MessageType type2) {
        this.type = type2;
    }

    private NavdyEvent(com.navdy.service.library.events.NavdyEvent.Builder builder) {
        this(builder.type);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.NavdyEvent)) {
            return false;
        }
        com.navdy.service.library.events.NavdyEvent o = (com.navdy.service.library.events.NavdyEvent) other;
        if (extensionsEqual(o)) {
            return equals((java.lang.Object) this.type, (java.lang.Object) o.type);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int result2 = (extensionsHashCode() * 37) + (this.type != null ? this.type.hashCode() : 0);
        this.hashCode = result2;
        return result2;
    }
}
