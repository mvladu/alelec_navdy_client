package com.navdy.service.library.events;

public final class Frame extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_SIZE = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.FIXED32)
    public final java.lang.Integer size;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.Frame> {
        public java.lang.Integer size;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.Frame message) {
            super(message);
            if (message != null) {
                this.size = message.size;
            }
        }

        public com.navdy.service.library.events.Frame.Builder size(java.lang.Integer size2) {
            this.size = size2;
            return this;
        }

        public com.navdy.service.library.events.Frame build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.Frame(this);
        }
    }

    public Frame(java.lang.Integer size2) {
        this.size = size2;
    }

    private Frame(com.navdy.service.library.events.Frame.Builder builder) {
        this(builder.size);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.Frame)) {
            return false;
        }
        return equals((java.lang.Object) this.size, (java.lang.Object) ((com.navdy.service.library.events.Frame) other).size);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.size != null ? this.size.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
