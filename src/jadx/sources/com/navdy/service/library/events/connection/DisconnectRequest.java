package com.navdy.service.library.events.connection;

public final class DisconnectRequest extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_FORGET = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean forget;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.connection.DisconnectRequest> {
        public java.lang.Boolean forget;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.connection.DisconnectRequest message) {
            super(message);
            if (message != null) {
                this.forget = message.forget;
            }
        }

        public com.navdy.service.library.events.connection.DisconnectRequest.Builder forget(java.lang.Boolean forget2) {
            this.forget = forget2;
            return this;
        }

        public com.navdy.service.library.events.connection.DisconnectRequest build() {
            return new com.navdy.service.library.events.connection.DisconnectRequest(this);
        }
    }

    public DisconnectRequest(java.lang.Boolean forget2) {
        this.forget = forget2;
    }

    private DisconnectRequest(com.navdy.service.library.events.connection.DisconnectRequest.Builder builder) {
        this(builder.forget);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.connection.DisconnectRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.forget, (java.lang.Object) ((com.navdy.service.library.events.connection.DisconnectRequest) other).forget);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.forget != null ? this.forget.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
