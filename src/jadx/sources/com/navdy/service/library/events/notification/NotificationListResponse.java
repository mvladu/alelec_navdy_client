package com.navdy.service.library.events.notification;

public final class NotificationListResponse extends com.squareup.wire.Message {
    public static final java.util.List<java.lang.String> DEFAULT_IDS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.util.List<java.lang.String> ids;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String status_detail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.notification.NotificationListResponse> {
        public java.util.List<java.lang.String> ids;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String status_detail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.notification.NotificationListResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.status_detail = message.status_detail;
                this.ids = com.navdy.service.library.events.notification.NotificationListResponse.copyOf(message.ids);
            }
        }

        public com.navdy.service.library.events.notification.NotificationListResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationListResponse.Builder status_detail(java.lang.String status_detail2) {
            this.status_detail = status_detail2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationListResponse.Builder ids(java.util.List<java.lang.String> ids2) {
            this.ids = checkForNulls(ids2);
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationListResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.notification.NotificationListResponse(this);
        }
    }

    public NotificationListResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String status_detail2, java.util.List<java.lang.String> ids2) {
        this.status = status2;
        this.status_detail = status_detail2;
        this.ids = immutableCopyOf(ids2);
    }

    private NotificationListResponse(com.navdy.service.library.events.notification.NotificationListResponse.Builder builder) {
        this(builder.status, builder.status_detail, builder.ids);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.notification.NotificationListResponse)) {
            return false;
        }
        com.navdy.service.library.events.notification.NotificationListResponse o = (com.navdy.service.library.events.notification.NotificationListResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.status_detail, (java.lang.Object) o.status_detail) || !equals(this.ids, o.ids)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.status_detail != null) {
            i = this.status_detail.hashCode();
        }
        int result2 = ((hashCode + i) * 37) + (this.ids != null ? this.ids.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
