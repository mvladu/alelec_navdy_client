package com.navdy.service.library.events.photo;

public final class PhotoResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    public static final okio.ByteString DEFAULT_PHOTO = okio.ByteString.EMPTY;
    public static final java.lang.String DEFAULT_PHOTOCHECKSUM = "";
    public static final com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT;
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.BYTES)
    public final okio.ByteString photo;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String photoChecksum;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.photo.PhotoType photoType;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.photo.PhotoResponse> {
        public java.lang.String identifier;
        public okio.ByteString photo;
        public java.lang.String photoChecksum;
        public com.navdy.service.library.events.photo.PhotoType photoType;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.photo.PhotoResponse message) {
            super(message);
            if (message != null) {
                this.photo = message.photo;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.identifier = message.identifier;
                this.photoChecksum = message.photoChecksum;
                this.photoType = message.photoType;
            }
        }

        public com.navdy.service.library.events.photo.PhotoResponse.Builder photo(okio.ByteString photo2) {
            this.photo = photo2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoResponse.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoResponse.Builder photoChecksum(java.lang.String photoChecksum2) {
            this.photoChecksum = photoChecksum2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoResponse.Builder photoType(com.navdy.service.library.events.photo.PhotoType photoType2) {
            this.photoType = photoType2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.photo.PhotoResponse(this);
        }
    }

    public PhotoResponse(okio.ByteString photo2, com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.lang.String identifier2, java.lang.String photoChecksum2, com.navdy.service.library.events.photo.PhotoType photoType2) {
        this.photo = photo2;
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.identifier = identifier2;
        this.photoChecksum = photoChecksum2;
        this.photoType = photoType2;
    }

    private PhotoResponse(com.navdy.service.library.events.photo.PhotoResponse.Builder builder) {
        this(builder.photo, builder.status, builder.statusDetail, builder.identifier, builder.photoChecksum, builder.photoType);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.photo.PhotoResponse)) {
            return false;
        }
        com.navdy.service.library.events.photo.PhotoResponse o = (com.navdy.service.library.events.photo.PhotoResponse) other;
        if (!equals((java.lang.Object) this.photo, (java.lang.Object) o.photo) || !equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals((java.lang.Object) this.identifier, (java.lang.Object) o.identifier) || !equals((java.lang.Object) this.photoChecksum, (java.lang.Object) o.photoChecksum) || !equals((java.lang.Object) this.photoType, (java.lang.Object) o.photoType)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.photo != null ? this.photo.hashCode() : 0) * 37;
        if (this.status != null) {
            i = this.status.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.statusDetail != null) {
            i2 = this.statusDetail.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.identifier != null) {
            i3 = this.identifier.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.photoChecksum != null) {
            i4 = this.photoChecksum.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.photoType != null) {
            i5 = this.photoType.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
