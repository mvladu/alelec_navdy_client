package com.navdy.service.library.events.glances;

public enum MessageConstants implements com.squareup.wire.ProtoEnum {
    MESSAGE_FROM(0),
    MESSAGE_FROM_NUMBER(1),
    MESSAGE_BODY(2),
    MESSAGE_DOMAIN(3),
    MESSAGE_IS_SMS(4);
    
    private final int value;

    private MessageConstants(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
