package com.navdy.service.library.events.destination;

public final class Destination extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.contacts.Contact> DEFAULT_CONTACTS = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_DESTINATIONDISTANCE = "";
    public static final java.lang.Integer DEFAULT_DESTINATIONICON = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_DESTINATIONICONBKCOLOR = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_DESTINATION_SUBTITLE = "";
    public static final java.lang.String DEFAULT_DESTINATION_TITLE = "";
    public static final com.navdy.service.library.events.destination.Destination.FavoriteType DEFAULT_FAVORITE_TYPE = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
    public static final java.lang.String DEFAULT_FULL_ADDRESS = "";
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    public static final java.lang.Boolean DEFAULT_IS_RECOMMENDATION = java.lang.Boolean.valueOf(false);
    public static final java.lang.Long DEFAULT_LAST_NAVIGATED_TO = java.lang.Long.valueOf(0);
    public static final java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> DEFAULT_PHONENUMBERS = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_PLACE_ID = "";
    public static final com.navdy.service.library.events.places.PlaceType DEFAULT_PLACE_TYPE = com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
    public static final com.navdy.service.library.events.destination.Destination.SuggestionType DEFAULT_SUGGESTION_TYPE = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_NONE;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.contacts.Contact.class, tag = 18)
    public final java.util.List<com.navdy.service.library.events.contacts.Contact> contacts;
    @com.squareup.wire.ProtoField(tag = 16, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String destinationDistance;
    @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer destinationIcon;
    @com.squareup.wire.ProtoField(tag = 14, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer destinationIconBkColor;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String destination_subtitle;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String destination_title;
    @com.squareup.wire.ProtoField(tag = 2)
    public final com.navdy.service.library.events.location.LatLong display_position;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 7, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.destination.Destination.FavoriteType favorite_type;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String full_address;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 8, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean is_recommendation;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long last_navigated_to;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1)
    public final com.navdy.service.library.events.location.LatLong navigation_position;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.contacts.PhoneNumber.class, tag = 17)
    public final java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers;
    @com.squareup.wire.ProtoField(tag = 15, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String place_id;
    @com.squareup.wire.ProtoField(tag = 12, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.places.PlaceType place_type;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.destination.Destination.SuggestionType suggestion_type;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.destination.Destination> {
        public java.util.List<com.navdy.service.library.events.contacts.Contact> contacts;
        public java.lang.String destinationDistance;
        public java.lang.Integer destinationIcon;
        public java.lang.Integer destinationIconBkColor;
        public java.lang.String destination_subtitle;
        public java.lang.String destination_title;
        public com.navdy.service.library.events.location.LatLong display_position;
        public com.navdy.service.library.events.destination.Destination.FavoriteType favorite_type;
        public java.lang.String full_address;
        public java.lang.String identifier;
        public java.lang.Boolean is_recommendation;
        public java.lang.Long last_navigated_to;
        public com.navdy.service.library.events.location.LatLong navigation_position;
        public java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers;
        public java.lang.String place_id;
        public com.navdy.service.library.events.places.PlaceType place_type;
        public com.navdy.service.library.events.destination.Destination.SuggestionType suggestion_type;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.destination.Destination message) {
            super(message);
            if (message != null) {
                this.navigation_position = message.navigation_position;
                this.display_position = message.display_position;
                this.full_address = message.full_address;
                this.destination_title = message.destination_title;
                this.destination_subtitle = message.destination_subtitle;
                this.favorite_type = message.favorite_type;
                this.identifier = message.identifier;
                this.suggestion_type = message.suggestion_type;
                this.is_recommendation = message.is_recommendation;
                this.last_navigated_to = message.last_navigated_to;
                this.place_type = message.place_type;
                this.destinationIcon = message.destinationIcon;
                this.destinationIconBkColor = message.destinationIconBkColor;
                this.place_id = message.place_id;
                this.destinationDistance = message.destinationDistance;
                this.phoneNumbers = com.navdy.service.library.events.destination.Destination.copyOf(message.phoneNumbers);
                this.contacts = com.navdy.service.library.events.destination.Destination.copyOf(message.contacts);
            }
        }

        public com.navdy.service.library.events.destination.Destination.Builder navigation_position(com.navdy.service.library.events.location.LatLong navigation_position2) {
            this.navigation_position = navigation_position2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder display_position(com.navdy.service.library.events.location.LatLong display_position2) {
            this.display_position = display_position2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder full_address(java.lang.String full_address2) {
            this.full_address = full_address2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder destination_title(java.lang.String destination_title2) {
            this.destination_title = destination_title2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder destination_subtitle(java.lang.String destination_subtitle2) {
            this.destination_subtitle = destination_subtitle2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder favorite_type(com.navdy.service.library.events.destination.Destination.FavoriteType favorite_type2) {
            this.favorite_type = favorite_type2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder suggestion_type(com.navdy.service.library.events.destination.Destination.SuggestionType suggestion_type2) {
            this.suggestion_type = suggestion_type2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder is_recommendation(java.lang.Boolean is_recommendation2) {
            this.is_recommendation = is_recommendation2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder last_navigated_to(java.lang.Long last_navigated_to2) {
            this.last_navigated_to = last_navigated_to2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder place_type(com.navdy.service.library.events.places.PlaceType place_type2) {
            this.place_type = place_type2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder destinationIcon(java.lang.Integer destinationIcon2) {
            this.destinationIcon = destinationIcon2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder destinationIconBkColor(java.lang.Integer destinationIconBkColor2) {
            this.destinationIconBkColor = destinationIconBkColor2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder place_id(java.lang.String place_id2) {
            this.place_id = place_id2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder destinationDistance(java.lang.String destinationDistance2) {
            this.destinationDistance = destinationDistance2;
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder phoneNumbers(java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers2) {
            this.phoneNumbers = checkForNulls(phoneNumbers2);
            return this;
        }

        public com.navdy.service.library.events.destination.Destination.Builder contacts(java.util.List<com.navdy.service.library.events.contacts.Contact> contacts2) {
            this.contacts = checkForNulls(contacts2);
            return this;
        }

        public com.navdy.service.library.events.destination.Destination build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.destination.Destination(this);
        }
    }

    public enum FavoriteType implements com.squareup.wire.ProtoEnum {
        FAVORITE_NONE(0),
        FAVORITE_HOME(1),
        FAVORITE_WORK(2),
        FAVORITE_CONTACT(3),
        FAVORITE_CUSTOM(10);
        
        private final int value;

        private FavoriteType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum SuggestionType implements com.squareup.wire.ProtoEnum {
        SUGGESTION_NONE(0),
        SUGGESTION_CALENDAR(1),
        SUGGESTION_RECENT(2);
        
        private final int value;

        private SuggestionType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public Destination(com.navdy.service.library.events.location.LatLong navigation_position2, com.navdy.service.library.events.location.LatLong display_position2, java.lang.String full_address2, java.lang.String destination_title2, java.lang.String destination_subtitle2, com.navdy.service.library.events.destination.Destination.FavoriteType favorite_type2, java.lang.String identifier2, com.navdy.service.library.events.destination.Destination.SuggestionType suggestion_type2, java.lang.Boolean is_recommendation2, java.lang.Long last_navigated_to2, com.navdy.service.library.events.places.PlaceType place_type2, java.lang.Integer destinationIcon2, java.lang.Integer destinationIconBkColor2, java.lang.String place_id2, java.lang.String destinationDistance2, java.util.List<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers2, java.util.List<com.navdy.service.library.events.contacts.Contact> contacts2) {
        this.navigation_position = navigation_position2;
        this.display_position = display_position2;
        this.full_address = full_address2;
        this.destination_title = destination_title2;
        this.destination_subtitle = destination_subtitle2;
        this.favorite_type = favorite_type2;
        this.identifier = identifier2;
        this.suggestion_type = suggestion_type2;
        this.is_recommendation = is_recommendation2;
        this.last_navigated_to = last_navigated_to2;
        this.place_type = place_type2;
        this.destinationIcon = destinationIcon2;
        this.destinationIconBkColor = destinationIconBkColor2;
        this.place_id = place_id2;
        this.destinationDistance = destinationDistance2;
        this.phoneNumbers = immutableCopyOf(phoneNumbers2);
        this.contacts = immutableCopyOf(contacts2);
    }

    private Destination(com.navdy.service.library.events.destination.Destination.Builder builder) {
        this(builder.navigation_position, builder.display_position, builder.full_address, builder.destination_title, builder.destination_subtitle, builder.favorite_type, builder.identifier, builder.suggestion_type, builder.is_recommendation, builder.last_navigated_to, builder.place_type, builder.destinationIcon, builder.destinationIconBkColor, builder.place_id, builder.destinationDistance, builder.phoneNumbers, builder.contacts);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.destination.Destination)) {
            return false;
        }
        com.navdy.service.library.events.destination.Destination o = (com.navdy.service.library.events.destination.Destination) other;
        if (!equals((java.lang.Object) this.navigation_position, (java.lang.Object) o.navigation_position) || !equals((java.lang.Object) this.display_position, (java.lang.Object) o.display_position) || !equals((java.lang.Object) this.full_address, (java.lang.Object) o.full_address) || !equals((java.lang.Object) this.destination_title, (java.lang.Object) o.destination_title) || !equals((java.lang.Object) this.destination_subtitle, (java.lang.Object) o.destination_subtitle) || !equals((java.lang.Object) this.favorite_type, (java.lang.Object) o.favorite_type) || !equals((java.lang.Object) this.identifier, (java.lang.Object) o.identifier) || !equals((java.lang.Object) this.suggestion_type, (java.lang.Object) o.suggestion_type) || !equals((java.lang.Object) this.is_recommendation, (java.lang.Object) o.is_recommendation) || !equals((java.lang.Object) this.last_navigated_to, (java.lang.Object) o.last_navigated_to) || !equals((java.lang.Object) this.place_type, (java.lang.Object) o.place_type) || !equals((java.lang.Object) this.destinationIcon, (java.lang.Object) o.destinationIcon) || !equals((java.lang.Object) this.destinationIconBkColor, (java.lang.Object) o.destinationIconBkColor) || !equals((java.lang.Object) this.place_id, (java.lang.Object) o.place_id) || !equals((java.lang.Object) this.destinationDistance, (java.lang.Object) o.destinationDistance) || !equals(this.phoneNumbers, o.phoneNumbers) || !equals(this.contacts, o.contacts)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15 = 1;
        int i16 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.navigation_position != null ? this.navigation_position.hashCode() : 0) * 37;
        if (this.display_position != null) {
            i = this.display_position.hashCode();
        } else {
            i = 0;
        }
        int i17 = (hashCode + i) * 37;
        if (this.full_address != null) {
            i2 = this.full_address.hashCode();
        } else {
            i2 = 0;
        }
        int i18 = (i17 + i2) * 37;
        if (this.destination_title != null) {
            i3 = this.destination_title.hashCode();
        } else {
            i3 = 0;
        }
        int i19 = (i18 + i3) * 37;
        if (this.destination_subtitle != null) {
            i4 = this.destination_subtitle.hashCode();
        } else {
            i4 = 0;
        }
        int i20 = (i19 + i4) * 37;
        if (this.favorite_type != null) {
            i5 = this.favorite_type.hashCode();
        } else {
            i5 = 0;
        }
        int i21 = (i20 + i5) * 37;
        if (this.identifier != null) {
            i6 = this.identifier.hashCode();
        } else {
            i6 = 0;
        }
        int i22 = (i21 + i6) * 37;
        if (this.suggestion_type != null) {
            i7 = this.suggestion_type.hashCode();
        } else {
            i7 = 0;
        }
        int i23 = (i22 + i7) * 37;
        if (this.is_recommendation != null) {
            i8 = this.is_recommendation.hashCode();
        } else {
            i8 = 0;
        }
        int i24 = (i23 + i8) * 37;
        if (this.last_navigated_to != null) {
            i9 = this.last_navigated_to.hashCode();
        } else {
            i9 = 0;
        }
        int i25 = (i24 + i9) * 37;
        if (this.place_type != null) {
            i10 = this.place_type.hashCode();
        } else {
            i10 = 0;
        }
        int i26 = (i25 + i10) * 37;
        if (this.destinationIcon != null) {
            i11 = this.destinationIcon.hashCode();
        } else {
            i11 = 0;
        }
        int i27 = (i26 + i11) * 37;
        if (this.destinationIconBkColor != null) {
            i12 = this.destinationIconBkColor.hashCode();
        } else {
            i12 = 0;
        }
        int i28 = (i27 + i12) * 37;
        if (this.place_id != null) {
            i13 = this.place_id.hashCode();
        } else {
            i13 = 0;
        }
        int i29 = (i28 + i13) * 37;
        if (this.destinationDistance != null) {
            i16 = this.destinationDistance.hashCode();
        }
        int i30 = (i29 + i16) * 37;
        if (this.phoneNumbers != null) {
            i14 = this.phoneNumbers.hashCode();
        } else {
            i14 = 1;
        }
        int i31 = (i30 + i14) * 37;
        if (this.contacts != null) {
            i15 = this.contacts.hashCode();
        }
        int result2 = i31 + i15;
        this.hashCode = result2;
        return result2;
    }
}
