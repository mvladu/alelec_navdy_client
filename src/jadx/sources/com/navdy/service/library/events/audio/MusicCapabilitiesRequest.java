package com.navdy.service.library.events.audio;

public final class MusicCapabilitiesRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.MusicCapabilitiesRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.MusicCapabilitiesRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.audio.MusicCapabilitiesRequest build() {
            return new com.navdy.service.library.events.audio.MusicCapabilitiesRequest(this);
        }
    }

    public MusicCapabilitiesRequest() {
    }

    private MusicCapabilitiesRequest(com.navdy.service.library.events.audio.MusicCapabilitiesRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.audio.MusicCapabilitiesRequest;
    }

    public int hashCode() {
        return 0;
    }
}
