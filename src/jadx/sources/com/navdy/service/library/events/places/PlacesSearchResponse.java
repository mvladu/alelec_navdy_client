package com.navdy.service.library.events.places;

public final class PlacesSearchResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_REQUESTID = "";
    public static final java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> DEFAULT_RESULTS = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_SEARCHQUERY = "";
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String requestId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.places.PlacesSearchResult.class, tag = 4)
    public final java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> results;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String searchQuery;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.PlacesSearchResponse> {
        public java.lang.String requestId;
        public java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> results;
        public java.lang.String searchQuery;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.PlacesSearchResponse message) {
            super(message);
            if (message != null) {
                this.searchQuery = message.searchQuery;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.results = com.navdy.service.library.events.places.PlacesSearchResponse.copyOf(message.results);
                this.requestId = message.requestId;
            }
        }

        public com.navdy.service.library.events.places.PlacesSearchResponse.Builder searchQuery(java.lang.String searchQuery2) {
            this.searchQuery = searchQuery2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResponse.Builder results(java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> results2) {
            this.results = checkForNulls(results2);
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResponse.Builder requestId(java.lang.String requestId2) {
            this.requestId = requestId2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.places.PlacesSearchResponse(this);
        }
    }

    public PlacesSearchResponse(java.lang.String searchQuery2, com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> results2, java.lang.String requestId2) {
        this.searchQuery = searchQuery2;
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.results = immutableCopyOf(results2);
        this.requestId = requestId2;
    }

    private PlacesSearchResponse(com.navdy.service.library.events.places.PlacesSearchResponse.Builder builder) {
        this(builder.searchQuery, builder.status, builder.statusDetail, builder.results, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.PlacesSearchResponse)) {
            return false;
        }
        com.navdy.service.library.events.places.PlacesSearchResponse o = (com.navdy.service.library.events.places.PlacesSearchResponse) other;
        if (!equals((java.lang.Object) this.searchQuery, (java.lang.Object) o.searchQuery) || !equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals(this.results, o.results) || !equals((java.lang.Object) this.requestId, (java.lang.Object) o.requestId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.searchQuery != null ? this.searchQuery.hashCode() : 0) * 37;
        if (this.status != null) {
            i = this.status.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.statusDetail != null) {
            i2 = this.statusDetail.hashCode();
        } else {
            i2 = 0;
        }
        int hashCode2 = (((i4 + i2) * 37) + (this.results != null ? this.results.hashCode() : 1)) * 37;
        if (this.requestId != null) {
            i3 = this.requestId.hashCode();
        }
        int result2 = hashCode2 + i3;
        this.hashCode = result2;
        return result2;
    }
}
