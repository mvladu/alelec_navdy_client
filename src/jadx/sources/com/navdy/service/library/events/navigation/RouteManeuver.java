package com.navdy.service.library.events.navigation;

public final class RouteManeuver extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_CURRENTROAD = "";
    public static final java.lang.Float DEFAULT_DISTANCETOPENDINGROAD = java.lang.Float.valueOf(0.0f);
    public static final com.navdy.service.library.events.navigation.DistanceUnit DEFAULT_DISTANCETOPENDINGROADUNIT = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES;
    public static final java.lang.Integer DEFAULT_MANEUVERTIME = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_PENDINGROAD = "";
    public static final com.navdy.service.library.events.navigation.NavigationTurn DEFAULT_PENDINGTURN = com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_START;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String currentRoad;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.lang.Float distanceToPendingRoad;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.DistanceUnit distanceToPendingRoadUnit;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer maneuverTime;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String pendingRoad;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.NavigationTurn pendingTurn;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.RouteManeuver> {
        public java.lang.String currentRoad;
        public java.lang.Float distanceToPendingRoad;
        public com.navdy.service.library.events.navigation.DistanceUnit distanceToPendingRoadUnit;
        public java.lang.Integer maneuverTime;
        public java.lang.String pendingRoad;
        public com.navdy.service.library.events.navigation.NavigationTurn pendingTurn;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.RouteManeuver message) {
            super(message);
            if (message != null) {
                this.currentRoad = message.currentRoad;
                this.pendingTurn = message.pendingTurn;
                this.pendingRoad = message.pendingRoad;
                this.distanceToPendingRoad = message.distanceToPendingRoad;
                this.distanceToPendingRoadUnit = message.distanceToPendingRoadUnit;
                this.maneuverTime = message.maneuverTime;
            }
        }

        public com.navdy.service.library.events.navigation.RouteManeuver.Builder currentRoad(java.lang.String currentRoad2) {
            this.currentRoad = currentRoad2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuver.Builder pendingTurn(com.navdy.service.library.events.navigation.NavigationTurn pendingTurn2) {
            this.pendingTurn = pendingTurn2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuver.Builder pendingRoad(java.lang.String pendingRoad2) {
            this.pendingRoad = pendingRoad2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuver.Builder distanceToPendingRoad(java.lang.Float distanceToPendingRoad2) {
            this.distanceToPendingRoad = distanceToPendingRoad2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuver.Builder distanceToPendingRoadUnit(com.navdy.service.library.events.navigation.DistanceUnit distanceToPendingRoadUnit2) {
            this.distanceToPendingRoadUnit = distanceToPendingRoadUnit2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuver.Builder maneuverTime(java.lang.Integer maneuverTime2) {
            this.maneuverTime = maneuverTime2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuver build() {
            return new com.navdy.service.library.events.navigation.RouteManeuver(this);
        }
    }

    public RouteManeuver(java.lang.String currentRoad2, com.navdy.service.library.events.navigation.NavigationTurn pendingTurn2, java.lang.String pendingRoad2, java.lang.Float distanceToPendingRoad2, com.navdy.service.library.events.navigation.DistanceUnit distanceToPendingRoadUnit2, java.lang.Integer maneuverTime2) {
        this.currentRoad = currentRoad2;
        this.pendingTurn = pendingTurn2;
        this.pendingRoad = pendingRoad2;
        this.distanceToPendingRoad = distanceToPendingRoad2;
        this.distanceToPendingRoadUnit = distanceToPendingRoadUnit2;
        this.maneuverTime = maneuverTime2;
    }

    private RouteManeuver(com.navdy.service.library.events.navigation.RouteManeuver.Builder builder) {
        this(builder.currentRoad, builder.pendingTurn, builder.pendingRoad, builder.distanceToPendingRoad, builder.distanceToPendingRoadUnit, builder.maneuverTime);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.RouteManeuver)) {
            return false;
        }
        com.navdy.service.library.events.navigation.RouteManeuver o = (com.navdy.service.library.events.navigation.RouteManeuver) other;
        if (!equals((java.lang.Object) this.currentRoad, (java.lang.Object) o.currentRoad) || !equals((java.lang.Object) this.pendingTurn, (java.lang.Object) o.pendingTurn) || !equals((java.lang.Object) this.pendingRoad, (java.lang.Object) o.pendingRoad) || !equals((java.lang.Object) this.distanceToPendingRoad, (java.lang.Object) o.distanceToPendingRoad) || !equals((java.lang.Object) this.distanceToPendingRoadUnit, (java.lang.Object) o.distanceToPendingRoadUnit) || !equals((java.lang.Object) this.maneuverTime, (java.lang.Object) o.maneuverTime)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.currentRoad != null ? this.currentRoad.hashCode() : 0) * 37;
        if (this.pendingTurn != null) {
            i = this.pendingTurn.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.pendingRoad != null) {
            i2 = this.pendingRoad.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.distanceToPendingRoad != null) {
            i3 = this.distanceToPendingRoad.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.distanceToPendingRoadUnit != null) {
            i4 = this.distanceToPendingRoadUnit.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.maneuverTime != null) {
            i5 = this.maneuverTime.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
