package com.navdy.service.library.events.file;

public final class FileTransferResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_CHECKSUM = "";
    public static final java.lang.String DEFAULT_DESTINATIONFILENAME = "";
    public static final com.navdy.service.library.events.file.FileTransferError DEFAULT_ERROR = com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NO_ERROR;
    public static final com.navdy.service.library.events.file.FileType DEFAULT_FILETYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
    public static final java.lang.Integer DEFAULT_MAXCHUNKSIZE = java.lang.Integer.valueOf(0);
    public static final java.lang.Long DEFAULT_OFFSET = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_SUCCESS = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_SUPPORTSACKS = java.lang.Boolean.valueOf(false);
    public static final java.lang.Integer DEFAULT_TRANSFERID = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String checksum;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 8, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String destinationFileName;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.file.FileTransferError error;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 7, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.file.FileType fileType;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer maxChunkSize;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long offset;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean success;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean supportsAcks;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer transferId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.FileTransferResponse> {
        public java.lang.String checksum;
        public java.lang.String destinationFileName;
        public com.navdy.service.library.events.file.FileTransferError error;
        public com.navdy.service.library.events.file.FileType fileType;
        public java.lang.Integer maxChunkSize;
        public java.lang.Long offset;
        public java.lang.Boolean success;
        public java.lang.Boolean supportsAcks;
        public java.lang.Integer transferId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.FileTransferResponse message) {
            super(message);
            if (message != null) {
                this.transferId = message.transferId;
                this.offset = message.offset;
                this.checksum = message.checksum;
                this.success = message.success;
                this.maxChunkSize = message.maxChunkSize;
                this.error = message.error;
                this.fileType = message.fileType;
                this.destinationFileName = message.destinationFileName;
                this.supportsAcks = message.supportsAcks;
            }
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder transferId(java.lang.Integer transferId2) {
            this.transferId = transferId2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder offset(java.lang.Long offset2) {
            this.offset = offset2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder checksum(java.lang.String checksum2) {
            this.checksum = checksum2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder success(java.lang.Boolean success2) {
            this.success = success2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder maxChunkSize(java.lang.Integer maxChunkSize2) {
            this.maxChunkSize = maxChunkSize2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder error(com.navdy.service.library.events.file.FileTransferError error2) {
            this.error = error2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder fileType(com.navdy.service.library.events.file.FileType fileType2) {
            this.fileType = fileType2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder destinationFileName(java.lang.String destinationFileName2) {
            this.destinationFileName = destinationFileName2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse.Builder supportsAcks(java.lang.Boolean supportsAcks2) {
            this.supportsAcks = supportsAcks2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.FileTransferResponse(this);
        }
    }

    public FileTransferResponse(java.lang.Integer transferId2, java.lang.Long offset2, java.lang.String checksum2, java.lang.Boolean success2, java.lang.Integer maxChunkSize2, com.navdy.service.library.events.file.FileTransferError error2, com.navdy.service.library.events.file.FileType fileType2, java.lang.String destinationFileName2, java.lang.Boolean supportsAcks2) {
        this.transferId = transferId2;
        this.offset = offset2;
        this.checksum = checksum2;
        this.success = success2;
        this.maxChunkSize = maxChunkSize2;
        this.error = error2;
        this.fileType = fileType2;
        this.destinationFileName = destinationFileName2;
        this.supportsAcks = supportsAcks2;
    }

    private FileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse.Builder builder) {
        this(builder.transferId, builder.offset, builder.checksum, builder.success, builder.maxChunkSize, builder.error, builder.fileType, builder.destinationFileName, builder.supportsAcks);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.FileTransferResponse)) {
            return false;
        }
        com.navdy.service.library.events.file.FileTransferResponse o = (com.navdy.service.library.events.file.FileTransferResponse) other;
        if (!equals((java.lang.Object) this.transferId, (java.lang.Object) o.transferId) || !equals((java.lang.Object) this.offset, (java.lang.Object) o.offset) || !equals((java.lang.Object) this.checksum, (java.lang.Object) o.checksum) || !equals((java.lang.Object) this.success, (java.lang.Object) o.success) || !equals((java.lang.Object) this.maxChunkSize, (java.lang.Object) o.maxChunkSize) || !equals((java.lang.Object) this.error, (java.lang.Object) o.error) || !equals((java.lang.Object) this.fileType, (java.lang.Object) o.fileType) || !equals((java.lang.Object) this.destinationFileName, (java.lang.Object) o.destinationFileName) || !equals((java.lang.Object) this.supportsAcks, (java.lang.Object) o.supportsAcks)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.transferId != null ? this.transferId.hashCode() : 0) * 37;
        if (this.offset != null) {
            i = this.offset.hashCode();
        } else {
            i = 0;
        }
        int i9 = (hashCode + i) * 37;
        if (this.checksum != null) {
            i2 = this.checksum.hashCode();
        } else {
            i2 = 0;
        }
        int i10 = (i9 + i2) * 37;
        if (this.success != null) {
            i3 = this.success.hashCode();
        } else {
            i3 = 0;
        }
        int i11 = (i10 + i3) * 37;
        if (this.maxChunkSize != null) {
            i4 = this.maxChunkSize.hashCode();
        } else {
            i4 = 0;
        }
        int i12 = (i11 + i4) * 37;
        if (this.error != null) {
            i5 = this.error.hashCode();
        } else {
            i5 = 0;
        }
        int i13 = (i12 + i5) * 37;
        if (this.fileType != null) {
            i6 = this.fileType.hashCode();
        } else {
            i6 = 0;
        }
        int i14 = (i13 + i6) * 37;
        if (this.destinationFileName != null) {
            i7 = this.destinationFileName.hashCode();
        } else {
            i7 = 0;
        }
        int i15 = (i14 + i7) * 37;
        if (this.supportsAcks != null) {
            i8 = this.supportsAcks.hashCode();
        }
        int result2 = i15 + i8;
        this.hashCode = result2;
        return result2;
    }
}
