package com.navdy.service.library.events.photo;

public final class PhotoUpdateQueryResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    public static final java.lang.Boolean DEFAULT_UPDATEREQUIRED = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean updateRequired;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.photo.PhotoUpdateQueryResponse> {
        public java.lang.String identifier;
        public java.lang.Boolean updateRequired;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.photo.PhotoUpdateQueryResponse message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.updateRequired = message.updateRequired;
            }
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQueryResponse.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQueryResponse.Builder updateRequired(java.lang.Boolean updateRequired2) {
            this.updateRequired = updateRequired2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQueryResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.photo.PhotoUpdateQueryResponse(this);
        }
    }

    public PhotoUpdateQueryResponse(java.lang.String identifier2, java.lang.Boolean updateRequired2) {
        this.identifier = identifier2;
        this.updateRequired = updateRequired2;
    }

    private PhotoUpdateQueryResponse(com.navdy.service.library.events.photo.PhotoUpdateQueryResponse.Builder builder) {
        this(builder.identifier, builder.updateRequired);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.photo.PhotoUpdateQueryResponse)) {
            return false;
        }
        com.navdy.service.library.events.photo.PhotoUpdateQueryResponse o = (com.navdy.service.library.events.photo.PhotoUpdateQueryResponse) other;
        if (!equals((java.lang.Object) this.identifier, (java.lang.Object) o.identifier) || !equals((java.lang.Object) this.updateRequired, (java.lang.Object) o.updateRequired)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.identifier != null) {
            result = this.identifier.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.updateRequired != null) {
            i = this.updateRequired.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
