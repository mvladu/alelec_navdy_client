package com.navdy.service.library.events.places;

public final class PlacesSearchRequest extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_MAXRESULTS = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_REQUESTID = "";
    public static final java.lang.Integer DEFAULT_SEARCHAREA = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_SEARCHQUERY = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer maxResults;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String requestId;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer searchArea;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String searchQuery;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.PlacesSearchRequest> {
        public java.lang.Integer maxResults;
        public java.lang.String requestId;
        public java.lang.Integer searchArea;
        public java.lang.String searchQuery;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.PlacesSearchRequest message) {
            super(message);
            if (message != null) {
                this.searchQuery = message.searchQuery;
                this.searchArea = message.searchArea;
                this.maxResults = message.maxResults;
                this.requestId = message.requestId;
            }
        }

        public com.navdy.service.library.events.places.PlacesSearchRequest.Builder searchQuery(java.lang.String searchQuery2) {
            this.searchQuery = searchQuery2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchRequest.Builder searchArea(java.lang.Integer searchArea2) {
            this.searchArea = searchArea2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchRequest.Builder maxResults(java.lang.Integer maxResults2) {
            this.maxResults = maxResults2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchRequest.Builder requestId(java.lang.String requestId2) {
            this.requestId = requestId2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.places.PlacesSearchRequest(this);
        }
    }

    public PlacesSearchRequest(java.lang.String searchQuery2, java.lang.Integer searchArea2, java.lang.Integer maxResults2, java.lang.String requestId2) {
        this.searchQuery = searchQuery2;
        this.searchArea = searchArea2;
        this.maxResults = maxResults2;
        this.requestId = requestId2;
    }

    private PlacesSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest.Builder builder) {
        this(builder.searchQuery, builder.searchArea, builder.maxResults, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.PlacesSearchRequest)) {
            return false;
        }
        com.navdy.service.library.events.places.PlacesSearchRequest o = (com.navdy.service.library.events.places.PlacesSearchRequest) other;
        if (!equals((java.lang.Object) this.searchQuery, (java.lang.Object) o.searchQuery) || !equals((java.lang.Object) this.searchArea, (java.lang.Object) o.searchArea) || !equals((java.lang.Object) this.maxResults, (java.lang.Object) o.maxResults) || !equals((java.lang.Object) this.requestId, (java.lang.Object) o.requestId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.searchQuery != null ? this.searchQuery.hashCode() : 0) * 37;
        if (this.searchArea != null) {
            i = this.searchArea.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.maxResults != null) {
            i2 = this.maxResults.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.requestId != null) {
            i3 = this.requestId.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
