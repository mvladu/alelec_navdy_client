package com.navdy.service.library.events.messaging;

public final class SmsMessageResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ID = "";
    public static final java.lang.String DEFAULT_NAME = "";
    public static final java.lang.String DEFAULT_NUMBER = "";
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String id;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.messaging.SmsMessageResponse> {
        public java.lang.String id;
        public java.lang.String name;
        public java.lang.String number;
        public com.navdy.service.library.events.RequestStatus status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.messaging.SmsMessageResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.number = message.number;
                this.name = message.name;
                this.id = message.id;
            }
        }

        public com.navdy.service.library.events.messaging.SmsMessageResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageResponse.Builder number(java.lang.String number2) {
            this.number = number2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageResponse.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageResponse.Builder id(java.lang.String id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.messaging.SmsMessageResponse(this);
        }
    }

    public SmsMessageResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String number2, java.lang.String name2, java.lang.String id2) {
        this.status = status2;
        this.number = number2;
        this.name = name2;
        this.id = id2;
    }

    private SmsMessageResponse(com.navdy.service.library.events.messaging.SmsMessageResponse.Builder builder) {
        this(builder.status, builder.number, builder.name, builder.id);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.messaging.SmsMessageResponse)) {
            return false;
        }
        com.navdy.service.library.events.messaging.SmsMessageResponse o = (com.navdy.service.library.events.messaging.SmsMessageResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.number, (java.lang.Object) o.number) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.id, (java.lang.Object) o.id)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.number != null) {
            i = this.number.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.name != null) {
            i2 = this.name.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.id != null) {
            i3 = this.id.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
