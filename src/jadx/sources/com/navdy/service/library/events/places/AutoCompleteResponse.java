package com.navdy.service.library.events.places;

public final class AutoCompleteResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_PARTIALSEARCH = "";
    public static final java.util.List<java.lang.String> DEFAULT_RESULTS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String partialSearch;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.util.List<java.lang.String> results;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.AutoCompleteResponse> {
        public java.lang.String partialSearch;
        public java.util.List<java.lang.String> results;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.AutoCompleteResponse message) {
            super(message);
            if (message != null) {
                this.partialSearch = message.partialSearch;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.results = com.navdy.service.library.events.places.AutoCompleteResponse.copyOf(message.results);
            }
        }

        public com.navdy.service.library.events.places.AutoCompleteResponse.Builder partialSearch(java.lang.String partialSearch2) {
            this.partialSearch = partialSearch2;
            return this;
        }

        public com.navdy.service.library.events.places.AutoCompleteResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.places.AutoCompleteResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.places.AutoCompleteResponse.Builder results(java.util.List<java.lang.String> results2) {
            this.results = checkForNulls(results2);
            return this;
        }

        public com.navdy.service.library.events.places.AutoCompleteResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.places.AutoCompleteResponse(this);
        }
    }

    public AutoCompleteResponse(java.lang.String partialSearch2, com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.util.List<java.lang.String> results2) {
        this.partialSearch = partialSearch2;
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.results = immutableCopyOf(results2);
    }

    private AutoCompleteResponse(com.navdy.service.library.events.places.AutoCompleteResponse.Builder builder) {
        this(builder.partialSearch, builder.status, builder.statusDetail, builder.results);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.AutoCompleteResponse)) {
            return false;
        }
        com.navdy.service.library.events.places.AutoCompleteResponse o = (com.navdy.service.library.events.places.AutoCompleteResponse) other;
        if (!equals((java.lang.Object) this.partialSearch, (java.lang.Object) o.partialSearch) || !equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals(this.results, o.results)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.partialSearch != null ? this.partialSearch.hashCode() : 0) * 37;
        if (this.status != null) {
            i = this.status.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.statusDetail != null) {
            i2 = this.statusDetail.hashCode();
        }
        int result2 = ((i3 + i2) * 37) + (this.results != null ? this.results.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
