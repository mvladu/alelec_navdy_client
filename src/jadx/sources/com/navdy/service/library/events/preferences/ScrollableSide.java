package com.navdy.service.library.events.preferences;

public enum ScrollableSide implements com.squareup.wire.ProtoEnum {
    LEFT(1),
    RIGHT(2);
    
    private final int value;

    private ScrollableSide(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
