package com.navdy.service.library.events.glances;

public final class GlanceEvent extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> DEFAULT_ACTIONS = java.util.Collections.emptyList();
    public static final java.util.List<com.navdy.service.library.events.glances.KeyValue> DEFAULT_GLANCEDATA = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.glances.GlanceEvent.GlanceType DEFAULT_GLANCETYPE = com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_CALENDAR;
    public static final java.lang.String DEFAULT_ID = "";
    public static final java.lang.String DEFAULT_LANGUAGE = "";
    public static final java.lang.Long DEFAULT_POSTTIME = java.lang.Long.valueOf(0);
    public static final java.lang.String DEFAULT_PROVIDER = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(enumType = com.navdy.service.library.events.glances.GlanceEvent.GlanceActions.class, label = com.squareup.wire.Message.Label.REPEATED, tag = 6, type = com.squareup.wire.Message.Datatype.ENUM)
    public final java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> actions;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.glances.KeyValue.class, tag = 5)
    public final java.util.List<com.navdy.service.library.events.glances.KeyValue> glanceData;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.glances.GlanceEvent.GlanceType glanceType;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String id;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String language;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long postTime;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String provider;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.glances.GlanceEvent> {
        public java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> actions;
        public java.util.List<com.navdy.service.library.events.glances.KeyValue> glanceData;
        public com.navdy.service.library.events.glances.GlanceEvent.GlanceType glanceType;
        public java.lang.String id;
        public java.lang.String language;
        public java.lang.Long postTime;
        public java.lang.String provider;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.glances.GlanceEvent message) {
            super(message);
            if (message != null) {
                this.glanceType = message.glanceType;
                this.provider = message.provider;
                this.id = message.id;
                this.postTime = message.postTime;
                this.glanceData = com.navdy.service.library.events.glances.GlanceEvent.copyOf(message.glanceData);
                this.actions = com.navdy.service.library.events.glances.GlanceEvent.copyOf(message.actions);
                this.language = message.language;
            }
        }

        public com.navdy.service.library.events.glances.GlanceEvent.Builder glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType glanceType2) {
            this.glanceType = glanceType2;
            return this;
        }

        public com.navdy.service.library.events.glances.GlanceEvent.Builder provider(java.lang.String provider2) {
            this.provider = provider2;
            return this;
        }

        public com.navdy.service.library.events.glances.GlanceEvent.Builder id(java.lang.String id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.glances.GlanceEvent.Builder postTime(java.lang.Long postTime2) {
            this.postTime = postTime2;
            return this;
        }

        public com.navdy.service.library.events.glances.GlanceEvent.Builder glanceData(java.util.List<com.navdy.service.library.events.glances.KeyValue> glanceData2) {
            this.glanceData = checkForNulls(glanceData2);
            return this;
        }

        public com.navdy.service.library.events.glances.GlanceEvent.Builder actions(java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> actions2) {
            this.actions = checkForNulls(actions2);
            return this;
        }

        public com.navdy.service.library.events.glances.GlanceEvent.Builder language(java.lang.String language2) {
            this.language = language2;
            return this;
        }

        public com.navdy.service.library.events.glances.GlanceEvent build() {
            return new com.navdy.service.library.events.glances.GlanceEvent(this);
        }
    }

    public enum GlanceActions implements com.squareup.wire.ProtoEnum {
        REPLY(0);
        
        private final int value;

        private GlanceActions(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum GlanceType implements com.squareup.wire.ProtoEnum {
        GLANCE_TYPE_CALENDAR(0),
        GLANCE_TYPE_EMAIL(1),
        GLANCE_TYPE_MESSAGE(2),
        GLANCE_TYPE_SOCIAL(3),
        GLANCE_TYPE_GENERIC(4),
        GLANCE_TYPE_FUEL(5);
        
        private final int value;

        private GlanceType(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public GlanceEvent(com.navdy.service.library.events.glances.GlanceEvent.GlanceType glanceType2, java.lang.String provider2, java.lang.String id2, java.lang.Long postTime2, java.util.List<com.navdy.service.library.events.glances.KeyValue> glanceData2, java.util.List<com.navdy.service.library.events.glances.GlanceEvent.GlanceActions> actions2, java.lang.String language2) {
        this.glanceType = glanceType2;
        this.provider = provider2;
        this.id = id2;
        this.postTime = postTime2;
        this.glanceData = immutableCopyOf(glanceData2);
        this.actions = immutableCopyOf(actions2);
        this.language = language2;
    }

    private GlanceEvent(com.navdy.service.library.events.glances.GlanceEvent.Builder builder) {
        this(builder.glanceType, builder.provider, builder.id, builder.postTime, builder.glanceData, builder.actions, builder.language);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.glances.GlanceEvent)) {
            return false;
        }
        com.navdy.service.library.events.glances.GlanceEvent o = (com.navdy.service.library.events.glances.GlanceEvent) other;
        if (!equals((java.lang.Object) this.glanceType, (java.lang.Object) o.glanceType) || !equals((java.lang.Object) this.provider, (java.lang.Object) o.provider) || !equals((java.lang.Object) this.id, (java.lang.Object) o.id) || !equals((java.lang.Object) this.postTime, (java.lang.Object) o.postTime) || !equals(this.glanceData, o.glanceData) || !equals(this.actions, o.actions) || !equals((java.lang.Object) this.language, (java.lang.Object) o.language)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 1;
        int i6 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.glanceType != null ? this.glanceType.hashCode() : 0) * 37;
        if (this.provider != null) {
            i = this.provider.hashCode();
        } else {
            i = 0;
        }
        int i7 = (hashCode + i) * 37;
        if (this.id != null) {
            i2 = this.id.hashCode();
        } else {
            i2 = 0;
        }
        int i8 = (i7 + i2) * 37;
        if (this.postTime != null) {
            i3 = this.postTime.hashCode();
        } else {
            i3 = 0;
        }
        int i9 = (i8 + i3) * 37;
        if (this.glanceData != null) {
            i4 = this.glanceData.hashCode();
        } else {
            i4 = 1;
        }
        int i10 = (i9 + i4) * 37;
        if (this.actions != null) {
            i5 = this.actions.hashCode();
        }
        int i11 = (i10 + i5) * 37;
        if (this.language != null) {
            i6 = this.language.hashCode();
        }
        int result2 = i11 + i6;
        this.hashCode = result2;
        return result2;
    }
}
