package com.navdy.service.library.events.places;

public final class PlacesSearchResult extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ADDRESS = "";
    public static final java.lang.String DEFAULT_CATEGORY = "";
    public static final java.lang.Double DEFAULT_DISTANCE = java.lang.Double.valueOf(0.0d);
    public static final java.lang.String DEFAULT_LABEL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String address;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String category;
    @com.squareup.wire.ProtoField(tag = 3)
    public final com.navdy.service.library.events.location.Coordinate destinationLocation;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.DOUBLE)
    public final java.lang.Double distance;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(tag = 2)
    public final com.navdy.service.library.events.location.Coordinate navigationPosition;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.PlacesSearchResult> {
        public java.lang.String address;
        public java.lang.String category;
        public com.navdy.service.library.events.location.Coordinate destinationLocation;
        public java.lang.Double distance;
        public java.lang.String label;
        public com.navdy.service.library.events.location.Coordinate navigationPosition;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.PlacesSearchResult message) {
            super(message);
            if (message != null) {
                this.label = message.label;
                this.navigationPosition = message.navigationPosition;
                this.destinationLocation = message.destinationLocation;
                this.address = message.address;
                this.category = message.category;
                this.distance = message.distance;
            }
        }

        public com.navdy.service.library.events.places.PlacesSearchResult.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResult.Builder navigationPosition(com.navdy.service.library.events.location.Coordinate navigationPosition2) {
            this.navigationPosition = navigationPosition2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResult.Builder destinationLocation(com.navdy.service.library.events.location.Coordinate destinationLocation2) {
            this.destinationLocation = destinationLocation2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResult.Builder address(java.lang.String address2) {
            this.address = address2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResult.Builder category(java.lang.String category2) {
            this.category = category2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResult.Builder distance(java.lang.Double distance2) {
            this.distance = distance2;
            return this;
        }

        public com.navdy.service.library.events.places.PlacesSearchResult build() {
            return new com.navdy.service.library.events.places.PlacesSearchResult(this);
        }
    }

    public PlacesSearchResult(java.lang.String label2, com.navdy.service.library.events.location.Coordinate navigationPosition2, com.navdy.service.library.events.location.Coordinate destinationLocation2, java.lang.String address2, java.lang.String category2, java.lang.Double distance2) {
        this.label = label2;
        this.navigationPosition = navigationPosition2;
        this.destinationLocation = destinationLocation2;
        this.address = address2;
        this.category = category2;
        this.distance = distance2;
    }

    private PlacesSearchResult(com.navdy.service.library.events.places.PlacesSearchResult.Builder builder) {
        this(builder.label, builder.navigationPosition, builder.destinationLocation, builder.address, builder.category, builder.distance);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.PlacesSearchResult)) {
            return false;
        }
        com.navdy.service.library.events.places.PlacesSearchResult o = (com.navdy.service.library.events.places.PlacesSearchResult) other;
        if (!equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals((java.lang.Object) this.navigationPosition, (java.lang.Object) o.navigationPosition) || !equals((java.lang.Object) this.destinationLocation, (java.lang.Object) o.destinationLocation) || !equals((java.lang.Object) this.address, (java.lang.Object) o.address) || !equals((java.lang.Object) this.category, (java.lang.Object) o.category) || !equals((java.lang.Object) this.distance, (java.lang.Object) o.distance)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.label != null ? this.label.hashCode() : 0) * 37;
        if (this.navigationPosition != null) {
            i = this.navigationPosition.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.destinationLocation != null) {
            i2 = this.destinationLocation.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.address != null) {
            i3 = this.address.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.category != null) {
            i4 = this.category.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.distance != null) {
            i5 = this.distance.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
