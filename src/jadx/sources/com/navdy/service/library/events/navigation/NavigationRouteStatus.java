package com.navdy.service.library.events.navigation;

public final class NavigationRouteStatus extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_HANDLE = "";
    public static final java.lang.Integer DEFAULT_PROGRESS = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_REQUESTID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String handle;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer progress;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String requestId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationRouteStatus> {
        public java.lang.String handle;
        public java.lang.Integer progress;
        public java.lang.String requestId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationRouteStatus message) {
            super(message);
            if (message != null) {
                this.handle = message.handle;
                this.progress = message.progress;
                this.requestId = message.requestId;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationRouteStatus.Builder handle(java.lang.String handle2) {
            this.handle = handle2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteStatus.Builder progress(java.lang.Integer progress2) {
            this.progress = progress2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteStatus.Builder requestId(java.lang.String requestId2) {
            this.requestId = requestId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteStatus build() {
            return new com.navdy.service.library.events.navigation.NavigationRouteStatus(this);
        }
    }

    public NavigationRouteStatus(java.lang.String handle2, java.lang.Integer progress2, java.lang.String requestId2) {
        this.handle = handle2;
        this.progress = progress2;
        this.requestId = requestId2;
    }

    private NavigationRouteStatus(com.navdy.service.library.events.navigation.NavigationRouteStatus.Builder builder) {
        this(builder.handle, builder.progress, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationRouteStatus)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationRouteStatus o = (com.navdy.service.library.events.navigation.NavigationRouteStatus) other;
        if (!equals((java.lang.Object) this.handle, (java.lang.Object) o.handle) || !equals((java.lang.Object) this.progress, (java.lang.Object) o.progress) || !equals((java.lang.Object) this.requestId, (java.lang.Object) o.requestId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.handle != null ? this.handle.hashCode() : 0) * 37;
        if (this.progress != null) {
            i = this.progress.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.requestId != null) {
            i2 = this.requestId.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
