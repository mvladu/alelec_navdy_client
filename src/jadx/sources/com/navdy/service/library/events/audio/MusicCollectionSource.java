package com.navdy.service.library.events.audio;

public enum MusicCollectionSource implements com.squareup.wire.ProtoEnum {
    COLLECTION_SOURCE_UNKNOWN(0),
    COLLECTION_SOURCE_ANDROID_LOCAL(1),
    COLLECTION_SOURCE_IOS_MEDIA_PLAYER(2),
    COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC(3),
    COLLECTION_SOURCE_ANDROID_SPOTIFY(4);
    
    private final int value;

    private MusicCollectionSource(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
