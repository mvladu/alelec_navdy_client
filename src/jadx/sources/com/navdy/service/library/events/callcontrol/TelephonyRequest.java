package com.navdy.service.library.events.callcontrol;

public final class TelephonyRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.callcontrol.CallAction DEFAULT_ACTION = com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT;
    public static final java.lang.String DEFAULT_CALLUUID = "";
    public static final java.lang.String DEFAULT_NUMBER = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.callcontrol.CallAction action;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String callUUID;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.callcontrol.TelephonyRequest> {
        public com.navdy.service.library.events.callcontrol.CallAction action;
        public java.lang.String callUUID;
        public java.lang.String number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.callcontrol.TelephonyRequest message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.number = message.number;
                this.callUUID = message.callUUID;
            }
        }

        public com.navdy.service.library.events.callcontrol.TelephonyRequest.Builder action(com.navdy.service.library.events.callcontrol.CallAction action2) {
            this.action = action2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.TelephonyRequest.Builder number(java.lang.String number2) {
            this.number = number2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.TelephonyRequest.Builder callUUID(java.lang.String callUUID2) {
            this.callUUID = callUUID2;
            return this;
        }

        public com.navdy.service.library.events.callcontrol.TelephonyRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.callcontrol.TelephonyRequest(this);
        }
    }

    public TelephonyRequest(com.navdy.service.library.events.callcontrol.CallAction action2, java.lang.String number2, java.lang.String callUUID2) {
        this.action = action2;
        this.number = number2;
        this.callUUID = callUUID2;
    }

    private TelephonyRequest(com.navdy.service.library.events.callcontrol.TelephonyRequest.Builder builder) {
        this(builder.action, builder.number, builder.callUUID);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.callcontrol.TelephonyRequest)) {
            return false;
        }
        com.navdy.service.library.events.callcontrol.TelephonyRequest o = (com.navdy.service.library.events.callcontrol.TelephonyRequest) other;
        if (!equals((java.lang.Object) this.action, (java.lang.Object) o.action) || !equals((java.lang.Object) this.number, (java.lang.Object) o.number) || !equals((java.lang.Object) this.callUUID, (java.lang.Object) o.callUUID)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.action != null ? this.action.hashCode() : 0) * 37;
        if (this.number != null) {
            i = this.number.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.callUUID != null) {
            i2 = this.callUUID.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
