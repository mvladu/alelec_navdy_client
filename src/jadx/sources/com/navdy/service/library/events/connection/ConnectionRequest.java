package com.navdy.service.library.events.connection;

public final class ConnectionRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.connection.ConnectionRequest.Action DEFAULT_ACTION = com.navdy.service.library.events.connection.ConnectionRequest.Action.CONNECTION_START_SEARCH;
    public static final java.lang.String DEFAULT_REMOTEDEVICEID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.connection.ConnectionRequest.Action action;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String remoteDeviceId;

    public enum Action implements com.squareup.wire.ProtoEnum {
        CONNECTION_START_SEARCH(1),
        CONNECTION_STOP_SEARCH(2),
        CONNECTION_SELECT(3);
        
        private final int value;

        private Action(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.connection.ConnectionRequest> {
        public com.navdy.service.library.events.connection.ConnectionRequest.Action action;
        public java.lang.String remoteDeviceId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.connection.ConnectionRequest message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.remoteDeviceId = message.remoteDeviceId;
            }
        }

        public com.navdy.service.library.events.connection.ConnectionRequest.Builder action(com.navdy.service.library.events.connection.ConnectionRequest.Action action2) {
            this.action = action2;
            return this;
        }

        public com.navdy.service.library.events.connection.ConnectionRequest.Builder remoteDeviceId(java.lang.String remoteDeviceId2) {
            this.remoteDeviceId = remoteDeviceId2;
            return this;
        }

        public com.navdy.service.library.events.connection.ConnectionRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.connection.ConnectionRequest(this);
        }
    }

    public ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest.Action action2, java.lang.String remoteDeviceId2) {
        this.action = action2;
        this.remoteDeviceId = remoteDeviceId2;
    }

    private ConnectionRequest(com.navdy.service.library.events.connection.ConnectionRequest.Builder builder) {
        this(builder.action, builder.remoteDeviceId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.connection.ConnectionRequest)) {
            return false;
        }
        com.navdy.service.library.events.connection.ConnectionRequest o = (com.navdy.service.library.events.connection.ConnectionRequest) other;
        if (!equals((java.lang.Object) this.action, (java.lang.Object) o.action) || !equals((java.lang.Object) this.remoteDeviceId, (java.lang.Object) o.remoteDeviceId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.action != null) {
            result = this.action.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.remoteDeviceId != null) {
            i = this.remoteDeviceId.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
