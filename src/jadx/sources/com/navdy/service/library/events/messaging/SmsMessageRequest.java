package com.navdy.service.library.events.messaging;

public final class SmsMessageRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ID = "";
    public static final java.lang.String DEFAULT_MESSAGE = "";
    public static final java.lang.String DEFAULT_NAME = "";
    public static final java.lang.String DEFAULT_NUMBER = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String id;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String message;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String name;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.messaging.SmsMessageRequest> {
        public java.lang.String id;
        public java.lang.String message;
        public java.lang.String name;
        public java.lang.String number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.messaging.SmsMessageRequest message2) {
            super(message2);
            if (message2 != null) {
                this.number = message2.number;
                this.message = message2.message;
                this.name = message2.name;
                this.id = message2.id;
            }
        }

        public com.navdy.service.library.events.messaging.SmsMessageRequest.Builder number(java.lang.String number2) {
            this.number = number2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageRequest.Builder message(java.lang.String message2) {
            this.message = message2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageRequest.Builder name(java.lang.String name2) {
            this.name = name2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageRequest.Builder id(java.lang.String id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.messaging.SmsMessageRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.messaging.SmsMessageRequest(this);
        }
    }

    public SmsMessageRequest(java.lang.String number2, java.lang.String message2, java.lang.String name2, java.lang.String id2) {
        this.number = number2;
        this.message = message2;
        this.name = name2;
        this.id = id2;
    }

    private SmsMessageRequest(com.navdy.service.library.events.messaging.SmsMessageRequest.Builder builder) {
        this(builder.number, builder.message, builder.name, builder.id);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.messaging.SmsMessageRequest)) {
            return false;
        }
        com.navdy.service.library.events.messaging.SmsMessageRequest o = (com.navdy.service.library.events.messaging.SmsMessageRequest) other;
        if (!equals((java.lang.Object) this.number, (java.lang.Object) o.number) || !equals((java.lang.Object) this.message, (java.lang.Object) o.message) || !equals((java.lang.Object) this.name, (java.lang.Object) o.name) || !equals((java.lang.Object) this.id, (java.lang.Object) o.id)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.number != null ? this.number.hashCode() : 0) * 37;
        if (this.message != null) {
            i = this.message.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.name != null) {
            i2 = this.name.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.id != null) {
            i3 = this.id.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
