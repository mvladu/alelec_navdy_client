package com.navdy.service.library.events.navigation;

public final class NavigationRouteRequest extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_AUTONAVIGATE = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_CANCELCURRENT = java.lang.Boolean.valueOf(false);
    public static final com.navdy.service.library.events.destination.Destination.FavoriteType DEFAULT_DESTINATIONTYPE = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
    public static final java.lang.String DEFAULT_DESTINATION_IDENTIFIER = "";
    public static final java.lang.Boolean DEFAULT_GEOCODESTREETADDRESS = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.Boolean DEFAULT_ORIGINDISPLAY = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_REQUESTID = "";
    public static final java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> DEFAULT_ROUTEATTRIBUTES = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_STREETADDRESS = "";
    public static final java.util.List<com.navdy.service.library.events.location.Coordinate> DEFAULT_WAYPOINTS = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean autoNavigate;
    @com.squareup.wire.ProtoField(tag = 10, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean cancelCurrent;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1)
    public final com.navdy.service.library.events.location.Coordinate destination;
    @com.squareup.wire.ProtoField(tag = 12)
    public final com.navdy.service.library.events.location.Coordinate destinationDisplay;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.destination.Destination.FavoriteType destinationType;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String destination_identifier;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean geoCodeStreetAddress;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean originDisplay;
    @com.squareup.wire.ProtoField(tag = 14)
    public final com.navdy.service.library.events.destination.Destination requestDestination;
    @com.squareup.wire.ProtoField(tag = 11, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String requestId;
    @com.squareup.wire.ProtoField(enumType = com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.class, label = com.squareup.wire.Message.Label.REPEATED, tag = 13, type = com.squareup.wire.Message.Datatype.ENUM)
    public final java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> routeAttributes;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String streetAddress;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.location.Coordinate.class, tag = 3)
    public final java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationRouteRequest> {
        public java.lang.Boolean autoNavigate;
        public java.lang.Boolean cancelCurrent;
        public com.navdy.service.library.events.location.Coordinate destination;
        public com.navdy.service.library.events.location.Coordinate destinationDisplay;
        public com.navdy.service.library.events.destination.Destination.FavoriteType destinationType;
        public java.lang.String destination_identifier;
        public java.lang.Boolean geoCodeStreetAddress;
        public java.lang.String label;
        public java.lang.Boolean originDisplay;
        public com.navdy.service.library.events.destination.Destination requestDestination;
        public java.lang.String requestId;
        public java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> routeAttributes;
        public java.lang.String streetAddress;
        public java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationRouteRequest message) {
            super(message);
            if (message != null) {
                this.destination = message.destination;
                this.label = message.label;
                this.waypoints = com.navdy.service.library.events.navigation.NavigationRouteRequest.copyOf(message.waypoints);
                this.streetAddress = message.streetAddress;
                this.geoCodeStreetAddress = message.geoCodeStreetAddress;
                this.autoNavigate = message.autoNavigate;
                this.destination_identifier = message.destination_identifier;
                this.destinationType = message.destinationType;
                this.originDisplay = message.originDisplay;
                this.cancelCurrent = message.cancelCurrent;
                this.requestId = message.requestId;
                this.destinationDisplay = message.destinationDisplay;
                this.routeAttributes = com.navdy.service.library.events.navigation.NavigationRouteRequest.copyOf(message.routeAttributes);
                this.requestDestination = message.requestDestination;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder destination(com.navdy.service.library.events.location.Coordinate destination2) {
            this.destination = destination2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder waypoints(java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints2) {
            this.waypoints = checkForNulls(waypoints2);
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder streetAddress(java.lang.String streetAddress2) {
            this.streetAddress = streetAddress2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder geoCodeStreetAddress(java.lang.Boolean geoCodeStreetAddress2) {
            this.geoCodeStreetAddress = geoCodeStreetAddress2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder autoNavigate(java.lang.Boolean autoNavigate2) {
            this.autoNavigate = autoNavigate2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder destination_identifier(java.lang.String destination_identifier2) {
            this.destination_identifier = destination_identifier2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder destinationType(com.navdy.service.library.events.destination.Destination.FavoriteType destinationType2) {
            this.destinationType = destinationType2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder originDisplay(java.lang.Boolean originDisplay2) {
            this.originDisplay = originDisplay2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder cancelCurrent(java.lang.Boolean cancelCurrent2) {
            this.cancelCurrent = cancelCurrent2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder requestId(java.lang.String requestId2) {
            this.requestId = requestId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder destinationDisplay(com.navdy.service.library.events.location.Coordinate destinationDisplay2) {
            this.destinationDisplay = destinationDisplay2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder routeAttributes(java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> routeAttributes2) {
            this.routeAttributes = checkForNulls(routeAttributes2);
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder requestDestination(com.navdy.service.library.events.destination.Destination requestDestination2) {
            this.requestDestination = requestDestination2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationRouteRequest(this);
        }
    }

    public enum RouteAttribute implements com.squareup.wire.ProtoEnum {
        ROUTE_ATTRIBUTE_GAS(0),
        ROUTE_ATTRIBUTE_SAVED_ROUTE(1);
        
        private final int value;

        private RouteAttribute(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavigationRouteRequest(com.navdy.service.library.events.location.Coordinate destination2, java.lang.String label2, java.util.List<com.navdy.service.library.events.location.Coordinate> waypoints2, java.lang.String streetAddress2, java.lang.Boolean geoCodeStreetAddress2, java.lang.Boolean autoNavigate2, java.lang.String destination_identifier2, com.navdy.service.library.events.destination.Destination.FavoriteType destinationType2, java.lang.Boolean originDisplay2, java.lang.Boolean cancelCurrent2, java.lang.String requestId2, com.navdy.service.library.events.location.Coordinate destinationDisplay2, java.util.List<com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute> routeAttributes2, com.navdy.service.library.events.destination.Destination requestDestination2) {
        this.destination = destination2;
        this.label = label2;
        this.waypoints = immutableCopyOf(waypoints2);
        this.streetAddress = streetAddress2;
        this.geoCodeStreetAddress = geoCodeStreetAddress2;
        this.autoNavigate = autoNavigate2;
        this.destination_identifier = destination_identifier2;
        this.destinationType = destinationType2;
        this.originDisplay = originDisplay2;
        this.cancelCurrent = cancelCurrent2;
        this.requestId = requestId2;
        this.destinationDisplay = destinationDisplay2;
        this.routeAttributes = immutableCopyOf(routeAttributes2);
        this.requestDestination = requestDestination2;
    }

    private NavigationRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder builder) {
        this(builder.destination, builder.label, builder.waypoints, builder.streetAddress, builder.geoCodeStreetAddress, builder.autoNavigate, builder.destination_identifier, builder.destinationType, builder.originDisplay, builder.cancelCurrent, builder.requestId, builder.destinationDisplay, builder.routeAttributes, builder.requestDestination);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationRouteRequest)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest o = (com.navdy.service.library.events.navigation.NavigationRouteRequest) other;
        if (!equals((java.lang.Object) this.destination, (java.lang.Object) o.destination) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals(this.waypoints, o.waypoints) || !equals((java.lang.Object) this.streetAddress, (java.lang.Object) o.streetAddress) || !equals((java.lang.Object) this.geoCodeStreetAddress, (java.lang.Object) o.geoCodeStreetAddress) || !equals((java.lang.Object) this.autoNavigate, (java.lang.Object) o.autoNavigate) || !equals((java.lang.Object) this.destination_identifier, (java.lang.Object) o.destination_identifier) || !equals((java.lang.Object) this.destinationType, (java.lang.Object) o.destinationType) || !equals((java.lang.Object) this.originDisplay, (java.lang.Object) o.originDisplay) || !equals((java.lang.Object) this.cancelCurrent, (java.lang.Object) o.cancelCurrent) || !equals((java.lang.Object) this.requestId, (java.lang.Object) o.requestId) || !equals((java.lang.Object) this.destinationDisplay, (java.lang.Object) o.destinationDisplay) || !equals(this.routeAttributes, o.routeAttributes) || !equals((java.lang.Object) this.requestDestination, (java.lang.Object) o.requestDestination)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12 = 1;
        int i13 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.destination != null ? this.destination.hashCode() : 0) * 37;
        if (this.label != null) {
            i = this.label.hashCode();
        } else {
            i = 0;
        }
        int i14 = (hashCode + i) * 37;
        if (this.waypoints != null) {
            i2 = this.waypoints.hashCode();
        } else {
            i2 = 1;
        }
        int i15 = (i14 + i2) * 37;
        if (this.streetAddress != null) {
            i3 = this.streetAddress.hashCode();
        } else {
            i3 = 0;
        }
        int i16 = (i15 + i3) * 37;
        if (this.geoCodeStreetAddress != null) {
            i4 = this.geoCodeStreetAddress.hashCode();
        } else {
            i4 = 0;
        }
        int i17 = (i16 + i4) * 37;
        if (this.autoNavigate != null) {
            i5 = this.autoNavigate.hashCode();
        } else {
            i5 = 0;
        }
        int i18 = (i17 + i5) * 37;
        if (this.destination_identifier != null) {
            i6 = this.destination_identifier.hashCode();
        } else {
            i6 = 0;
        }
        int i19 = (i18 + i6) * 37;
        if (this.destinationType != null) {
            i7 = this.destinationType.hashCode();
        } else {
            i7 = 0;
        }
        int i20 = (i19 + i7) * 37;
        if (this.originDisplay != null) {
            i8 = this.originDisplay.hashCode();
        } else {
            i8 = 0;
        }
        int i21 = (i20 + i8) * 37;
        if (this.cancelCurrent != null) {
            i9 = this.cancelCurrent.hashCode();
        } else {
            i9 = 0;
        }
        int i22 = (i21 + i9) * 37;
        if (this.requestId != null) {
            i10 = this.requestId.hashCode();
        } else {
            i10 = 0;
        }
        int i23 = (i22 + i10) * 37;
        if (this.destinationDisplay != null) {
            i11 = this.destinationDisplay.hashCode();
        } else {
            i11 = 0;
        }
        int i24 = (i23 + i11) * 37;
        if (this.routeAttributes != null) {
            i12 = this.routeAttributes.hashCode();
        }
        int i25 = (i24 + i12) * 37;
        if (this.requestDestination != null) {
            i13 = this.requestDestination.hashCode();
        }
        int result2 = i25 + i13;
        this.hashCode = result2;
        return result2;
    }
}
