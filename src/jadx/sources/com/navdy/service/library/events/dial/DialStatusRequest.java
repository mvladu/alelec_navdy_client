package com.navdy.service.library.events.dial;

public final class DialStatusRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.dial.DialStatusRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.dial.DialStatusRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.dial.DialStatusRequest build() {
            return new com.navdy.service.library.events.dial.DialStatusRequest(this);
        }
    }

    public DialStatusRequest() {
    }

    private DialStatusRequest(com.navdy.service.library.events.dial.DialStatusRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.dial.DialStatusRequest;
    }

    public int hashCode() {
        return 0;
    }
}
