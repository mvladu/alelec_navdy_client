package com.navdy.service.library.events.callcontrol;

public final class PhoneStatusRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.callcontrol.PhoneStatusRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.callcontrol.PhoneStatusRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.callcontrol.PhoneStatusRequest build() {
            return new com.navdy.service.library.events.callcontrol.PhoneStatusRequest(this);
        }
    }

    public PhoneStatusRequest() {
    }

    private PhoneStatusRequest(com.navdy.service.library.events.callcontrol.PhoneStatusRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.callcontrol.PhoneStatusRequest;
    }

    public int hashCode() {
        return 0;
    }
}
