package com.navdy.service.library.events.file;

public final class FileTransferStatus extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_CHUNKINDEX = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.file.FileTransferError DEFAULT_ERROR = com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NO_ERROR;
    public static final java.lang.Boolean DEFAULT_SUCCESS = java.lang.Boolean.valueOf(false);
    public static final java.lang.Long DEFAULT_TOTALBYTESTRANSFERRED = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_TRANSFERCOMPLETE = java.lang.Boolean.valueOf(false);
    public static final java.lang.Integer DEFAULT_TRANSFERID = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer chunkIndex;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.file.FileTransferError error;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean success;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long totalBytesTransferred;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 6, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean transferComplete;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer transferId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.FileTransferStatus> {
        public java.lang.Integer chunkIndex;
        public com.navdy.service.library.events.file.FileTransferError error;
        public java.lang.Boolean success;
        public java.lang.Long totalBytesTransferred;
        public java.lang.Boolean transferComplete;
        public java.lang.Integer transferId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.FileTransferStatus message) {
            super(message);
            if (message != null) {
                this.transferId = message.transferId;
                this.success = message.success;
                this.totalBytesTransferred = message.totalBytesTransferred;
                this.error = message.error;
                this.chunkIndex = message.chunkIndex;
                this.transferComplete = message.transferComplete;
            }
        }

        public com.navdy.service.library.events.file.FileTransferStatus.Builder transferId(java.lang.Integer transferId2) {
            this.transferId = transferId2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferStatus.Builder success(java.lang.Boolean success2) {
            this.success = success2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferStatus.Builder totalBytesTransferred(java.lang.Long totalBytesTransferred2) {
            this.totalBytesTransferred = totalBytesTransferred2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferStatus.Builder error(com.navdy.service.library.events.file.FileTransferError error2) {
            this.error = error2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferStatus.Builder chunkIndex(java.lang.Integer chunkIndex2) {
            this.chunkIndex = chunkIndex2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferStatus.Builder transferComplete(java.lang.Boolean transferComplete2) {
            this.transferComplete = transferComplete2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferStatus build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.FileTransferStatus(this);
        }
    }

    public FileTransferStatus(java.lang.Integer transferId2, java.lang.Boolean success2, java.lang.Long totalBytesTransferred2, com.navdy.service.library.events.file.FileTransferError error2, java.lang.Integer chunkIndex2, java.lang.Boolean transferComplete2) {
        this.transferId = transferId2;
        this.success = success2;
        this.totalBytesTransferred = totalBytesTransferred2;
        this.error = error2;
        this.chunkIndex = chunkIndex2;
        this.transferComplete = transferComplete2;
    }

    private FileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus.Builder builder) {
        this(builder.transferId, builder.success, builder.totalBytesTransferred, builder.error, builder.chunkIndex, builder.transferComplete);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.FileTransferStatus)) {
            return false;
        }
        com.navdy.service.library.events.file.FileTransferStatus o = (com.navdy.service.library.events.file.FileTransferStatus) other;
        if (!equals((java.lang.Object) this.transferId, (java.lang.Object) o.transferId) || !equals((java.lang.Object) this.success, (java.lang.Object) o.success) || !equals((java.lang.Object) this.totalBytesTransferred, (java.lang.Object) o.totalBytesTransferred) || !equals((java.lang.Object) this.error, (java.lang.Object) o.error) || !equals((java.lang.Object) this.chunkIndex, (java.lang.Object) o.chunkIndex) || !equals((java.lang.Object) this.transferComplete, (java.lang.Object) o.transferComplete)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.transferId != null ? this.transferId.hashCode() : 0) * 37;
        if (this.success != null) {
            i = this.success.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.totalBytesTransferred != null) {
            i2 = this.totalBytesTransferred.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.error != null) {
            i3 = this.error.hashCode();
        } else {
            i3 = 0;
        }
        int i8 = (i7 + i3) * 37;
        if (this.chunkIndex != null) {
            i4 = this.chunkIndex.hashCode();
        } else {
            i4 = 0;
        }
        int i9 = (i8 + i4) * 37;
        if (this.transferComplete != null) {
            i5 = this.transferComplete.hashCode();
        }
        int result2 = i9 + i5;
        this.hashCode = result2;
        return result2;
    }
}
