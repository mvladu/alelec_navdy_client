package com.navdy.service.library.events.contacts;

public final class FavoriteContactsResponse extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.contacts.Contact> DEFAULT_CONTACTS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.contacts.Contact.class, tag = 3)
    public final java.util.List<com.navdy.service.library.events.contacts.Contact> contacts;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.contacts.FavoriteContactsResponse> {
        public java.util.List<com.navdy.service.library.events.contacts.Contact> contacts;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.contacts.FavoriteContactsResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.contacts = com.navdy.service.library.events.contacts.FavoriteContactsResponse.copyOf(message.contacts);
            }
        }

        public com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder contacts(java.util.List<com.navdy.service.library.events.contacts.Contact> contacts2) {
            this.contacts = checkForNulls(contacts2);
            return this;
        }

        public com.navdy.service.library.events.contacts.FavoriteContactsResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.contacts.FavoriteContactsResponse(this);
        }
    }

    public FavoriteContactsResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.util.List<com.navdy.service.library.events.contacts.Contact> contacts2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.contacts = immutableCopyOf(contacts2);
    }

    private FavoriteContactsResponse(com.navdy.service.library.events.contacts.FavoriteContactsResponse.Builder builder) {
        this(builder.status, builder.statusDetail, builder.contacts);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.contacts.FavoriteContactsResponse)) {
            return false;
        }
        com.navdy.service.library.events.contacts.FavoriteContactsResponse o = (com.navdy.service.library.events.contacts.FavoriteContactsResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals(this.contacts, o.contacts)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        }
        int result2 = ((hashCode + i) * 37) + (this.contacts != null ? this.contacts.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
