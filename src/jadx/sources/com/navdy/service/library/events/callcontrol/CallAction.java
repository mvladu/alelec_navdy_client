package com.navdy.service.library.events.callcontrol;

public enum CallAction implements com.squareup.wire.ProtoEnum {
    CALL_ACCEPT(1),
    CALL_END(2),
    CALL_DIAL(3),
    CALL_MUTE(4),
    CALL_UNMUTE(5),
    CALL_REJECT(6);
    
    private final int value;

    private CallAction(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
