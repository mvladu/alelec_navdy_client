package com.navdy.service.library.events.places;

public final class FavoriteDestinationsUpdate extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.destination.Destination> DEFAULT_DESTINATIONS = java.util.Collections.emptyList();
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.destination.Destination.class, tag = 4)
    public final java.util.List<com.navdy.service.library.events.destination.Destination> destinations;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.FavoriteDestinationsUpdate> {
        public java.util.List<com.navdy.service.library.events.destination.Destination> destinations;
        public java.lang.Long serial_number;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.FavoriteDestinationsUpdate message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.serial_number = message.serial_number;
                this.destinations = com.navdy.service.library.events.places.FavoriteDestinationsUpdate.copyOf(message.destinations);
            }
        }

        public com.navdy.service.library.events.places.FavoriteDestinationsUpdate.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.places.FavoriteDestinationsUpdate.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.places.FavoriteDestinationsUpdate.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.places.FavoriteDestinationsUpdate.Builder destinations(java.util.List<com.navdy.service.library.events.destination.Destination> destinations2) {
            this.destinations = checkForNulls(destinations2);
            return this;
        }

        public com.navdy.service.library.events.places.FavoriteDestinationsUpdate build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.places.FavoriteDestinationsUpdate(this);
        }
    }

    public FavoriteDestinationsUpdate(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.lang.Long serial_number2, java.util.List<com.navdy.service.library.events.destination.Destination> destinations2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.serial_number = serial_number2;
        this.destinations = immutableCopyOf(destinations2);
    }

    private FavoriteDestinationsUpdate(com.navdy.service.library.events.places.FavoriteDestinationsUpdate.Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.destinations);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.FavoriteDestinationsUpdate)) {
            return false;
        }
        com.navdy.service.library.events.places.FavoriteDestinationsUpdate o = (com.navdy.service.library.events.places.FavoriteDestinationsUpdate) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals(this.destinations, o.destinations)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.serial_number != null) {
            i2 = this.serial_number.hashCode();
        }
        int result2 = ((i3 + i2) * 37) + (this.destinations != null ? this.destinations.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
