package com.navdy.service.library.events.audio;

public enum Audio implements com.squareup.wire.ProtoEnum {
    DRIVE_BEHAVIOR_SPEEDING(1),
    DRIVE_BEHAVIOR_EXCESSIVE_SPEEDING(2),
    DRIVE_BEHAVIOR_HARD_ACCELERATION(3),
    DRIVE_BEHAVIOR_HARD_BRAKING(4),
    DRIVE_BEHAVIOR_HIGH_G(5);
    
    private final int value;

    private Audio(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
