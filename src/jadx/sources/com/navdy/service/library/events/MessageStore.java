package com.navdy.service.library.events;

public class MessageStore {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.events.MessageStore.class);
    private java.io.File directory;
    private com.squareup.wire.Wire wire = new com.squareup.wire.Wire((java.lang.Class<?>[]) new java.lang.Class[]{com.navdy.service.library.events.Ext_NavdyEvent.class});

    public MessageStore(java.io.File directory2) {
        this.directory = directory2;
    }

    public static <T extends com.squareup.wire.Message> T removeNulls(T message) {
        return com.navdy.service.library.events.NavdyEventUtil.applyDefaults(message);
    }

    public <T extends com.squareup.wire.Message> T readMessage(java.lang.String filename, java.lang.Class<T> type) throws java.io.IOException {
        return readMessage(filename, type, null);
    }

    public <T extends com.squareup.wire.Message> T readMessage(java.lang.String filename, java.lang.Class<T> type, com.navdy.service.library.events.NavdyEventUtil.Initializer<T> initializer) throws java.io.IOException {
        T result;
        java.io.File messageFile = new java.io.File(this.directory, filename);
        java.io.InputStream stream = null;
        try {
            if (messageFile.length() == 0) {
                result = com.navdy.service.library.events.NavdyEventUtil.getDefault(type, initializer);
            } else {
                java.io.InputStream stream2 = new java.io.FileInputStream(messageFile);
                try {
                    result = this.wire.parseFrom(stream2, type);
                    stream = stream2;
                } catch (java.lang.Exception e) {
                    e = e;
                    stream = stream2;
                    try {
                        result = com.navdy.service.library.events.NavdyEventUtil.getDefault(type, initializer);
                        sLogger.w("Failed to read message from " + filename, e);
                        com.navdy.service.library.util.IOUtils.closeStream(stream);
                        return removeNulls(result);
                    } catch (Throwable th) {
                        th = th;
                        com.navdy.service.library.util.IOUtils.closeStream(stream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    stream = stream2;
                    com.navdy.service.library.util.IOUtils.closeStream(stream);
                    throw th;
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(stream);
        } catch (java.lang.Exception e2) {
            e = e2;
            result = com.navdy.service.library.events.NavdyEventUtil.getDefault(type, initializer);
            sLogger.w("Failed to read message from " + filename, e);
            com.navdy.service.library.util.IOUtils.closeStream(stream);
            return removeNulls(result);
        }
        return removeNulls(result);
    }

    public void writeMessage(com.squareup.wire.Message message, java.lang.String filename) {
        java.lang.String tmpFilename = filename + ".new";
        java.io.File tmpFile = new java.io.File(this.directory, tmpFilename);
        try {
            byte[] bytes = message.toByteArray();
            int bytesCopied = com.navdy.service.library.util.IOUtils.copyFile(tmpFile.getAbsolutePath(), bytes);
            if (bytesCopied != bytes.length) {
                sLogger.e("failed to write messages to " + tmpFilename + "wrote: " + bytesCopied + " bytes, length:" + bytes.length);
            } else if (!tmpFile.renameTo(new java.io.File(this.directory, filename))) {
                sLogger.e("could not replace messages in " + filename);
            }
        } catch (java.lang.Exception e) {
            sLogger.e("could not write new messages to " + filename + " : ", e);
        }
    }
}
