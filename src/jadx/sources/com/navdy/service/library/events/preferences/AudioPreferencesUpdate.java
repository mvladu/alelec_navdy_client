package com.navdy.service.library.events.preferences;

public final class AudioPreferencesUpdate extends com.squareup.wire.Message {
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4)
    public final com.navdy.service.library.events.preferences.AudioPreferences preferences;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.AudioPreferencesUpdate> {
        public com.navdy.service.library.events.preferences.AudioPreferences preferences;
        public java.lang.Long serial_number;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.AudioPreferencesUpdate message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.serial_number = message.serial_number;
                this.preferences = message.preferences;
            }
        }

        public com.navdy.service.library.events.preferences.AudioPreferencesUpdate.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.preferences.AudioPreferencesUpdate.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.preferences.AudioPreferencesUpdate.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.AudioPreferencesUpdate.Builder preferences(com.navdy.service.library.events.preferences.AudioPreferences preferences2) {
            this.preferences = preferences2;
            return this;
        }

        public com.navdy.service.library.events.preferences.AudioPreferencesUpdate build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.preferences.AudioPreferencesUpdate(this);
        }
    }

    public AudioPreferencesUpdate(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.lang.Long serial_number2, com.navdy.service.library.events.preferences.AudioPreferences preferences2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.serial_number = serial_number2;
        this.preferences = preferences2;
    }

    private AudioPreferencesUpdate(com.navdy.service.library.events.preferences.AudioPreferencesUpdate.Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.preferences);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.AudioPreferencesUpdate)) {
            return false;
        }
        com.navdy.service.library.events.preferences.AudioPreferencesUpdate o = (com.navdy.service.library.events.preferences.AudioPreferencesUpdate) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals((java.lang.Object) this.serial_number, (java.lang.Object) o.serial_number) || !equals((java.lang.Object) this.preferences, (java.lang.Object) o.preferences)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.serial_number != null) {
            i2 = this.serial_number.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.preferences != null) {
            i3 = this.preferences.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
