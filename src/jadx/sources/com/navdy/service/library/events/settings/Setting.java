package com.navdy.service.library.events.settings;

public final class Setting extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_KEY = "";
    public static final java.lang.String DEFAULT_VALUE = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String key;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String value;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.settings.Setting> {
        public java.lang.String key;
        public java.lang.String value;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.settings.Setting message) {
            super(message);
            if (message != null) {
                this.key = message.key;
                this.value = message.value;
            }
        }

        public com.navdy.service.library.events.settings.Setting.Builder key(java.lang.String key2) {
            this.key = key2;
            return this;
        }

        public com.navdy.service.library.events.settings.Setting.Builder value(java.lang.String value2) {
            this.value = value2;
            return this;
        }

        public com.navdy.service.library.events.settings.Setting build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.settings.Setting(this);
        }
    }

    public Setting(java.lang.String key2, java.lang.String value2) {
        this.key = key2;
        this.value = value2;
    }

    private Setting(com.navdy.service.library.events.settings.Setting.Builder builder) {
        this(builder.key, builder.value);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.settings.Setting)) {
            return false;
        }
        com.navdy.service.library.events.settings.Setting o = (com.navdy.service.library.events.settings.Setting) other;
        if (!equals((java.lang.Object) this.key, (java.lang.Object) o.key) || !equals((java.lang.Object) this.value, (java.lang.Object) o.value)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.key != null) {
            result = this.key.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.value != null) {
            i = this.value.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
