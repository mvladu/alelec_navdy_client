package com.navdy.service.library.events.preferences;

public final class DriverProfilePreferencesRequest extends com.squareup.wire.Message {
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest> {
        public java.lang.Long serial_number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
            }
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest build() {
            return new com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest(this);
        }
    }

    public DriverProfilePreferencesRequest(java.lang.Long serial_number2) {
        this.serial_number = serial_number2;
    }

    private DriverProfilePreferencesRequest(com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest.Builder builder) {
        this(builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.serial_number, (java.lang.Object) ((com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest) other).serial_number);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.serial_number != null ? this.serial_number.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
