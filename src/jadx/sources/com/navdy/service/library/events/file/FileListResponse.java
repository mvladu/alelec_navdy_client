package com.navdy.service.library.events.file;

public final class FileListResponse extends com.squareup.wire.Message {
    public static final java.util.List<java.lang.String> DEFAULT_FILES = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.file.FileType DEFAULT_FILE_TYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.file.FileType file_type;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.util.List<java.lang.String> files;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String status_detail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.FileListResponse> {
        public com.navdy.service.library.events.file.FileType file_type;
        public java.util.List<java.lang.String> files;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String status_detail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.FileListResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.status_detail = message.status_detail;
                this.file_type = message.file_type;
                this.files = com.navdy.service.library.events.file.FileListResponse.copyOf(message.files);
            }
        }

        public com.navdy.service.library.events.file.FileListResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.file.FileListResponse.Builder status_detail(java.lang.String status_detail2) {
            this.status_detail = status_detail2;
            return this;
        }

        public com.navdy.service.library.events.file.FileListResponse.Builder file_type(com.navdy.service.library.events.file.FileType file_type2) {
            this.file_type = file_type2;
            return this;
        }

        public com.navdy.service.library.events.file.FileListResponse.Builder files(java.util.List<java.lang.String> files2) {
            this.files = checkForNulls(files2);
            return this;
        }

        public com.navdy.service.library.events.file.FileListResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.FileListResponse(this);
        }
    }

    public FileListResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String status_detail2, com.navdy.service.library.events.file.FileType file_type2, java.util.List<java.lang.String> files2) {
        this.status = status2;
        this.status_detail = status_detail2;
        this.file_type = file_type2;
        this.files = immutableCopyOf(files2);
    }

    private FileListResponse(com.navdy.service.library.events.file.FileListResponse.Builder builder) {
        this(builder.status, builder.status_detail, builder.file_type, builder.files);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.FileListResponse)) {
            return false;
        }
        com.navdy.service.library.events.file.FileListResponse o = (com.navdy.service.library.events.file.FileListResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.status_detail, (java.lang.Object) o.status_detail) || !equals((java.lang.Object) this.file_type, (java.lang.Object) o.file_type) || !equals(this.files, o.files)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.status_detail != null) {
            i = this.status_detail.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.file_type != null) {
            i2 = this.file_type.hashCode();
        }
        int result2 = ((i3 + i2) * 37) + (this.files != null ? this.files.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
