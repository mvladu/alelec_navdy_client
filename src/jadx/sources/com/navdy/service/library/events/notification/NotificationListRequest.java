package com.navdy.service.library.events.notification;

public final class NotificationListRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.notification.NotificationListRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.notification.NotificationListRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.notification.NotificationListRequest build() {
            return new com.navdy.service.library.events.notification.NotificationListRequest(this);
        }
    }

    public NotificationListRequest() {
    }

    private NotificationListRequest(com.navdy.service.library.events.notification.NotificationListRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.notification.NotificationListRequest;
    }

    public int hashCode() {
        return 0;
    }
}
