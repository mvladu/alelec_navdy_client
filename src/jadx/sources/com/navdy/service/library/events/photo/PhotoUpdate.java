package com.navdy.service.library.events.photo;

public final class PhotoUpdate extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    public static final okio.ByteString DEFAULT_PHOTO = okio.ByteString.EMPTY;
    public static final com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.BYTES)
    public final okio.ByteString photo;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.photo.PhotoType photoType;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.photo.PhotoUpdate> {
        public java.lang.String identifier;
        public okio.ByteString photo;
        public com.navdy.service.library.events.photo.PhotoType photoType;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.photo.PhotoUpdate message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.photo = message.photo;
                this.photoType = message.photoType;
            }
        }

        public com.navdy.service.library.events.photo.PhotoUpdate.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdate.Builder photo(okio.ByteString photo2) {
            this.photo = photo2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdate.Builder photoType(com.navdy.service.library.events.photo.PhotoType photoType2) {
            this.photoType = photoType2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdate build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.photo.PhotoUpdate(this);
        }
    }

    public PhotoUpdate(java.lang.String identifier2, okio.ByteString photo2, com.navdy.service.library.events.photo.PhotoType photoType2) {
        this.identifier = identifier2;
        this.photo = photo2;
        this.photoType = photoType2;
    }

    private PhotoUpdate(com.navdy.service.library.events.photo.PhotoUpdate.Builder builder) {
        this(builder.identifier, builder.photo, builder.photoType);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.photo.PhotoUpdate)) {
            return false;
        }
        com.navdy.service.library.events.photo.PhotoUpdate o = (com.navdy.service.library.events.photo.PhotoUpdate) other;
        if (!equals((java.lang.Object) this.identifier, (java.lang.Object) o.identifier) || !equals((java.lang.Object) this.photo, (java.lang.Object) o.photo) || !equals((java.lang.Object) this.photoType, (java.lang.Object) o.photoType)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.identifier != null ? this.identifier.hashCode() : 0) * 37;
        if (this.photo != null) {
            i = this.photo.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.photoType != null) {
            i2 = this.photoType.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
