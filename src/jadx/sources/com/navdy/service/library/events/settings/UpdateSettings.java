package com.navdy.service.library.events.settings;

public final class UpdateSettings extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.settings.Setting> DEFAULT_SETTINGS = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1)
    public final com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.settings.Setting.class, tag = 2)
    public final java.util.List<com.navdy.service.library.events.settings.Setting> settings;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.settings.UpdateSettings> {
        public com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration;
        public java.util.List<com.navdy.service.library.events.settings.Setting> settings;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.settings.UpdateSettings message) {
            super(message);
            if (message != null) {
                this.screenConfiguration = message.screenConfiguration;
                this.settings = com.navdy.service.library.events.settings.UpdateSettings.copyOf(message.settings);
            }
        }

        public com.navdy.service.library.events.settings.UpdateSettings.Builder screenConfiguration(com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration2) {
            this.screenConfiguration = screenConfiguration2;
            return this;
        }

        public com.navdy.service.library.events.settings.UpdateSettings.Builder settings(java.util.List<com.navdy.service.library.events.settings.Setting> settings2) {
            this.settings = checkForNulls(settings2);
            return this;
        }

        public com.navdy.service.library.events.settings.UpdateSettings build() {
            return new com.navdy.service.library.events.settings.UpdateSettings(this);
        }
    }

    public UpdateSettings(com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration2, java.util.List<com.navdy.service.library.events.settings.Setting> settings2) {
        this.screenConfiguration = screenConfiguration2;
        this.settings = immutableCopyOf(settings2);
    }

    private UpdateSettings(com.navdy.service.library.events.settings.UpdateSettings.Builder builder) {
        this(builder.screenConfiguration, builder.settings);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.settings.UpdateSettings)) {
            return false;
        }
        com.navdy.service.library.events.settings.UpdateSettings o = (com.navdy.service.library.events.settings.UpdateSettings) other;
        if (!equals((java.lang.Object) this.screenConfiguration, (java.lang.Object) o.screenConfiguration) || !equals(this.settings, o.settings)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int result2 = ((this.screenConfiguration != null ? this.screenConfiguration.hashCode() : 0) * 37) + (this.settings != null ? this.settings.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
