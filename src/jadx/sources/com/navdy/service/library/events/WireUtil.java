package com.navdy.service.library.events;

public class WireUtil {
    private static final com.navdy.service.library.events.NavdyEvent.MessageType[] sMessageTypes = com.navdy.service.library.events.NavdyEvent.MessageType.values();

    public static int getEventTypeIndex(byte[] eventData) {
        return parseEventType(eventData);
    }

    public static com.navdy.service.library.events.NavdyEvent.MessageType getEventType(byte[] eventData) {
        int eventType = parseEventType(eventData);
        if (eventType <= 0 || eventType > sMessageTypes.length) {
            return null;
        }
        return sMessageTypes[eventType - 1];
    }

    public static int parseEventType(byte[] eventData) {
        byte firstByte = eventData[0];
        int varType = firstByte & 7;
        if (com.squareup.wire.WireType.VARINT.value() != varType) {
            throw new java.lang.RuntimeException("expecting varint:" + varType);
        }
        int tag = firstByte >> 3;
        if (tag == 2) {
            return readVarint32(eventData);
        }
        throw new java.lang.RuntimeException("unexpected tag:" + tag);
    }

    public static int readVarint32(byte[] data) {
        byte tmp = data[1];
        if (tmp >= 0) {
            return tmp;
        }
        int result = tmp & android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE;
        int pos = 1 + 1;
        byte tmp2 = data[pos];
        if (tmp2 >= 0) {
            return result | (tmp2 << 7);
        }
        int result2 = result | ((tmp2 & Byte.MAX_VALUE) << 7);
        int pos2 = pos + 1;
        byte tmp3 = data[pos2];
        if (tmp3 >= 0) {
            return result2 | (tmp3 << 14);
        }
        int result3 = result2 | ((tmp3 & Byte.MAX_VALUE) << 14);
        int pos3 = pos2 + 1;
        byte tmp4 = data[pos3];
        if (tmp4 >= 0) {
            return result3 | (tmp4 << 21);
        }
        int result4 = result3 | ((tmp4 & Byte.MAX_VALUE) << 21);
        int pos4 = pos3 + 1;
        byte tmp5 = data[pos4];
        int result5 = result4 | (tmp5 << 28);
        if (tmp5 >= 0) {
            return result5;
        }
        for (int i = 0; i < 5; i++) {
            pos4++;
            if (data[pos4] >= 0) {
                return result5;
            }
        }
        throw new java.lang.RuntimeException("marlformed varint");
    }
}
