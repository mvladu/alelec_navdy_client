package com.navdy.service.library.events.connection;

public final class LinkPropertiesChanged extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_BANDWIDTHLEVEL = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer bandwidthLevel;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.connection.LinkPropertiesChanged> {
        public java.lang.Integer bandwidthLevel;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.connection.LinkPropertiesChanged message) {
            super(message);
            if (message != null) {
                this.bandwidthLevel = message.bandwidthLevel;
            }
        }

        public com.navdy.service.library.events.connection.LinkPropertiesChanged.Builder bandwidthLevel(java.lang.Integer bandwidthLevel2) {
            this.bandwidthLevel = bandwidthLevel2;
            return this;
        }

        public com.navdy.service.library.events.connection.LinkPropertiesChanged build() {
            return new com.navdy.service.library.events.connection.LinkPropertiesChanged(this);
        }
    }

    public LinkPropertiesChanged(java.lang.Integer bandwidthLevel2) {
        this.bandwidthLevel = bandwidthLevel2;
    }

    private LinkPropertiesChanged(com.navdy.service.library.events.connection.LinkPropertiesChanged.Builder builder) {
        this(builder.bandwidthLevel);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.connection.LinkPropertiesChanged)) {
            return false;
        }
        return equals((java.lang.Object) this.bandwidthLevel, (java.lang.Object) ((com.navdy.service.library.events.connection.LinkPropertiesChanged) other).bandwidthLevel);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.bandwidthLevel != null ? this.bandwidthLevel.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
