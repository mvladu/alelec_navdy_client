package com.navdy.service.library.events.navigation;

public enum NavigationTurn implements com.squareup.wire.ProtoEnum {
    NAV_TURN_START(1),
    NAV_TURN_EASY_LEFT(2),
    NAV_TURN_EASY_RIGHT(3),
    NAV_TURN_END(4),
    NAV_TURN_KEEP_LEFT(5),
    NAV_TURN_KEEP_RIGHT(6),
    NAV_TURN_LEFT(7),
    NAV_TURN_OUT_OF_ROUTE(8),
    NAV_TURN_RIGHT(9),
    NAV_TURN_SHARP_LEFT(10),
    NAV_TURN_SHARP_RIGHT(11),
    NAV_TURN_STRAIGHT(12),
    NAV_TURN_UTURN_LEFT(13),
    NAV_TURN_UTURN_RIGHT(14),
    NAV_TURN_ROUNDABOUT_SE(15),
    NAV_TURN_ROUNDABOUT_E(16),
    NAV_TURN_ROUNDABOUT_NE(17),
    NAV_TURN_ROUNDABOUT_N(18),
    NAV_TURN_ROUNDABOUT_NW(19),
    NAV_TURN_ROUNDABOUT_W(20),
    NAV_TURN_ROUNDABOUT_SW(21),
    NAV_TURN_ROUNDABOUT_S(22),
    NAV_TURN_FERRY(23),
    NAV_TURN_STATE_BOUNDARY(24),
    NAV_TURN_FOLLOW_ROUTE(25),
    NAV_TURN_EXIT_RIGHT(26),
    NAV_TURN_EXIT_LEFT(27),
    NAV_TURN_MOTORWAY(28),
    NAV_TURN_EXIT_ROUNDABOUT(29),
    NAV_TURN_UNKNOWN(30),
    NAV_TURN_MERGE_LEFT(31),
    NAV_TURN_MERGE_RIGHT(32);
    
    private final int value;

    private NavigationTurn(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
