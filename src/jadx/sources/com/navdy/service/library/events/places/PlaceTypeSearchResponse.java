package com.navdy.service.library.events.places;

public final class PlaceTypeSearchResponse extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.destination.Destination> DEFAULT_DESTINATIONS = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_REQUEST_ID = "";
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_REQUEST_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.destination.Destination.class, tag = 3)
    public final java.util.List<com.navdy.service.library.events.destination.Destination> destinations;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String request_id;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus request_status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.PlaceTypeSearchResponse> {
        public java.util.List<com.navdy.service.library.events.destination.Destination> destinations;
        public java.lang.String request_id;
        public com.navdy.service.library.events.RequestStatus request_status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.PlaceTypeSearchResponse message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.request_status = message.request_status;
                this.destinations = com.navdy.service.library.events.places.PlaceTypeSearchResponse.copyOf(message.destinations);
            }
        }

        public com.navdy.service.library.events.places.PlaceTypeSearchResponse.Builder request_id(java.lang.String request_id2) {
            this.request_id = request_id2;
            return this;
        }

        public com.navdy.service.library.events.places.PlaceTypeSearchResponse.Builder request_status(com.navdy.service.library.events.RequestStatus request_status2) {
            this.request_status = request_status2;
            return this;
        }

        public com.navdy.service.library.events.places.PlaceTypeSearchResponse.Builder destinations(java.util.List<com.navdy.service.library.events.destination.Destination> destinations2) {
            this.destinations = checkForNulls(destinations2);
            return this;
        }

        public com.navdy.service.library.events.places.PlaceTypeSearchResponse build() {
            return new com.navdy.service.library.events.places.PlaceTypeSearchResponse(this);
        }
    }

    public PlaceTypeSearchResponse(java.lang.String request_id2, com.navdy.service.library.events.RequestStatus request_status2, java.util.List<com.navdy.service.library.events.destination.Destination> destinations2) {
        this.request_id = request_id2;
        this.request_status = request_status2;
        this.destinations = immutableCopyOf(destinations2);
    }

    private PlaceTypeSearchResponse(com.navdy.service.library.events.places.PlaceTypeSearchResponse.Builder builder) {
        this(builder.request_id, builder.request_status, builder.destinations);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.PlaceTypeSearchResponse)) {
            return false;
        }
        com.navdy.service.library.events.places.PlaceTypeSearchResponse o = (com.navdy.service.library.events.places.PlaceTypeSearchResponse) other;
        if (!equals((java.lang.Object) this.request_id, (java.lang.Object) o.request_id) || !equals((java.lang.Object) this.request_status, (java.lang.Object) o.request_status) || !equals(this.destinations, o.destinations)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.request_id != null ? this.request_id.hashCode() : 0) * 37;
        if (this.request_status != null) {
            i = this.request_status.hashCode();
        }
        int result2 = ((hashCode + i) * 37) + (this.destinations != null ? this.destinations.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
