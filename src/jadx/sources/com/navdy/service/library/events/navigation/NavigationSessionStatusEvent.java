package com.navdy.service.library.events.navigation;

public final class NavigationSessionStatusEvent extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_DESTINATION_IDENTIFIER = "";
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.String DEFAULT_ROUTEID = "";
    public static final com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_SESSIONSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String destination_identifier;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(tag = 4)
    public final com.navdy.service.library.events.navigation.NavigationRouteResult route;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String routeId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.NavigationSessionState sessionState;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationSessionStatusEvent> {
        public java.lang.String destination_identifier;
        public java.lang.String label;
        public com.navdy.service.library.events.navigation.NavigationRouteResult route;
        public java.lang.String routeId;
        public com.navdy.service.library.events.navigation.NavigationSessionState sessionState;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent message) {
            super(message);
            if (message != null) {
                this.sessionState = message.sessionState;
                this.label = message.label;
                this.routeId = message.routeId;
                this.route = message.route;
                this.destination_identifier = message.destination_identifier;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent.Builder sessionState(com.navdy.service.library.events.navigation.NavigationSessionState sessionState2) {
            this.sessionState = sessionState2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent.Builder routeId(java.lang.String routeId2) {
            this.routeId = routeId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent.Builder route(com.navdy.service.library.events.navigation.NavigationRouteResult route2) {
            this.route = route2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent.Builder destination_identifier(java.lang.String destination_identifier2) {
            this.destination_identifier = destination_identifier2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionStatusEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(this);
        }
    }

    public NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState sessionState2, java.lang.String label2, java.lang.String routeId2, com.navdy.service.library.events.navigation.NavigationRouteResult route2, java.lang.String destination_identifier2) {
        this.sessionState = sessionState2;
        this.label = label2;
        this.routeId = routeId2;
        this.route = route2;
        this.destination_identifier = destination_identifier2;
    }

    private NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent.Builder builder) {
        this(builder.sessionState, builder.label, builder.routeId, builder.route, builder.destination_identifier);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationSessionStatusEvent)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationSessionStatusEvent o = (com.navdy.service.library.events.navigation.NavigationSessionStatusEvent) other;
        if (!equals((java.lang.Object) this.sessionState, (java.lang.Object) o.sessionState) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals((java.lang.Object) this.routeId, (java.lang.Object) o.routeId) || !equals((java.lang.Object) this.route, (java.lang.Object) o.route) || !equals((java.lang.Object) this.destination_identifier, (java.lang.Object) o.destination_identifier)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.sessionState != null ? this.sessionState.hashCode() : 0) * 37;
        if (this.label != null) {
            i = this.label.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.routeId != null) {
            i2 = this.routeId.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.route != null) {
            i3 = this.route.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.destination_identifier != null) {
            i4 = this.destination_identifier.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
