package com.navdy.service.library.events.navigation;

public final class NavigationRouteResponse extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_CONSIDEREDTRAFFIC = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.String DEFAULT_REQUESTID = "";
    public static final java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> DEFAULT_RESULTS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean consideredTraffic;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3)
    public final com.navdy.service.library.events.location.Coordinate destination;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String requestId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.navigation.NavigationRouteResult.class, tag = 5)
    public final java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> results;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationRouteResponse> {
        public java.lang.Boolean consideredTraffic;
        public com.navdy.service.library.events.location.Coordinate destination;
        public java.lang.String label;
        public java.lang.String requestId;
        public java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> results;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationRouteResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.destination = message.destination;
                this.label = message.label;
                this.results = com.navdy.service.library.events.navigation.NavigationRouteResponse.copyOf(message.results);
                this.consideredTraffic = message.consideredTraffic;
                this.requestId = message.requestId;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder destination(com.navdy.service.library.events.location.Coordinate destination2) {
            this.destination = destination2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder results(java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> results2) {
            this.results = checkForNulls(results2);
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder consideredTraffic(java.lang.Boolean consideredTraffic2) {
            this.consideredTraffic = consideredTraffic2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder requestId(java.lang.String requestId2) {
            this.requestId = requestId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationRouteResponse(this);
        }
    }

    public NavigationRouteResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, com.navdy.service.library.events.location.Coordinate destination2, java.lang.String label2, java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> results2, java.lang.Boolean consideredTraffic2, java.lang.String requestId2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.destination = destination2;
        this.label = label2;
        this.results = immutableCopyOf(results2);
        this.consideredTraffic = consideredTraffic2;
        this.requestId = requestId2;
    }

    private NavigationRouteResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder builder) {
        this(builder.status, builder.statusDetail, builder.destination, builder.label, builder.results, builder.consideredTraffic, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationRouteResponse)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationRouteResponse o = (com.navdy.service.library.events.navigation.NavigationRouteResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals((java.lang.Object) this.destination, (java.lang.Object) o.destination) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals(this.results, o.results) || !equals((java.lang.Object) this.consideredTraffic, (java.lang.Object) o.consideredTraffic) || !equals((java.lang.Object) this.requestId, (java.lang.Object) o.requestId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        } else {
            i = 0;
        }
        int i6 = (hashCode + i) * 37;
        if (this.destination != null) {
            i2 = this.destination.hashCode();
        } else {
            i2 = 0;
        }
        int i7 = (i6 + i2) * 37;
        if (this.label != null) {
            i3 = this.label.hashCode();
        } else {
            i3 = 0;
        }
        int hashCode2 = (((i7 + i3) * 37) + (this.results != null ? this.results.hashCode() : 1)) * 37;
        if (this.consideredTraffic != null) {
            i4 = this.consideredTraffic.hashCode();
        } else {
            i4 = 0;
        }
        int i8 = (hashCode2 + i4) * 37;
        if (this.requestId != null) {
            i5 = this.requestId.hashCode();
        }
        int result2 = i8 + i5;
        this.hashCode = result2;
        return result2;
    }
}
