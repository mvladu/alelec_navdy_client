package com.navdy.service.library.events.file;

public final class FileTransferRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_DESTINATIONFILENAME = "";
    public static final java.lang.String DEFAULT_FILEDATACHECKSUM = "";
    public static final java.lang.Long DEFAULT_FILESIZE = java.lang.Long.valueOf(0);
    public static final com.navdy.service.library.events.file.FileType DEFAULT_FILETYPE = com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA;
    public static final java.lang.Long DEFAULT_OFFSET = java.lang.Long.valueOf(0);
    public static final java.lang.Boolean DEFAULT_OVERRIDE = java.lang.Boolean.valueOf(false);
    public static final java.lang.Boolean DEFAULT_SUPPORTSACKS = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String destinationFileName;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String fileDataChecksum;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long fileSize;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.file.FileType fileType;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long offset;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean override;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean supportsAcks;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.file.FileTransferRequest> {
        public java.lang.String destinationFileName;
        public java.lang.String fileDataChecksum;
        public java.lang.Long fileSize;
        public com.navdy.service.library.events.file.FileType fileType;
        public java.lang.Long offset;
        public java.lang.Boolean override;
        public java.lang.Boolean supportsAcks;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.file.FileTransferRequest message) {
            super(message);
            if (message != null) {
                this.fileType = message.fileType;
                this.destinationFileName = message.destinationFileName;
                this.fileSize = message.fileSize;
                this.offset = message.offset;
                this.fileDataChecksum = message.fileDataChecksum;
                this.override = message.override;
                this.supportsAcks = message.supportsAcks;
            }
        }

        public com.navdy.service.library.events.file.FileTransferRequest.Builder fileType(com.navdy.service.library.events.file.FileType fileType2) {
            this.fileType = fileType2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferRequest.Builder destinationFileName(java.lang.String destinationFileName2) {
            this.destinationFileName = destinationFileName2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferRequest.Builder fileSize(java.lang.Long fileSize2) {
            this.fileSize = fileSize2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferRequest.Builder offset(java.lang.Long offset2) {
            this.offset = offset2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferRequest.Builder fileDataChecksum(java.lang.String fileDataChecksum2) {
            this.fileDataChecksum = fileDataChecksum2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferRequest.Builder override(java.lang.Boolean override2) {
            this.override = override2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferRequest.Builder supportsAcks(java.lang.Boolean supportsAcks2) {
            this.supportsAcks = supportsAcks2;
            return this;
        }

        public com.navdy.service.library.events.file.FileTransferRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.file.FileTransferRequest(this);
        }
    }

    public FileTransferRequest(com.navdy.service.library.events.file.FileType fileType2, java.lang.String destinationFileName2, java.lang.Long fileSize2, java.lang.Long offset2, java.lang.String fileDataChecksum2, java.lang.Boolean override2, java.lang.Boolean supportsAcks2) {
        this.fileType = fileType2;
        this.destinationFileName = destinationFileName2;
        this.fileSize = fileSize2;
        this.offset = offset2;
        this.fileDataChecksum = fileDataChecksum2;
        this.override = override2;
        this.supportsAcks = supportsAcks2;
    }

    private FileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest.Builder builder) {
        this(builder.fileType, builder.destinationFileName, builder.fileSize, builder.offset, builder.fileDataChecksum, builder.override, builder.supportsAcks);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.file.FileTransferRequest)) {
            return false;
        }
        com.navdy.service.library.events.file.FileTransferRequest o = (com.navdy.service.library.events.file.FileTransferRequest) other;
        if (!equals((java.lang.Object) this.fileType, (java.lang.Object) o.fileType) || !equals((java.lang.Object) this.destinationFileName, (java.lang.Object) o.destinationFileName) || !equals((java.lang.Object) this.fileSize, (java.lang.Object) o.fileSize) || !equals((java.lang.Object) this.offset, (java.lang.Object) o.offset) || !equals((java.lang.Object) this.fileDataChecksum, (java.lang.Object) o.fileDataChecksum) || !equals((java.lang.Object) this.override, (java.lang.Object) o.override) || !equals((java.lang.Object) this.supportsAcks, (java.lang.Object) o.supportsAcks)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.fileType != null ? this.fileType.hashCode() : 0) * 37;
        if (this.destinationFileName != null) {
            i = this.destinationFileName.hashCode();
        } else {
            i = 0;
        }
        int i7 = (hashCode + i) * 37;
        if (this.fileSize != null) {
            i2 = this.fileSize.hashCode();
        } else {
            i2 = 0;
        }
        int i8 = (i7 + i2) * 37;
        if (this.offset != null) {
            i3 = this.offset.hashCode();
        } else {
            i3 = 0;
        }
        int i9 = (i8 + i3) * 37;
        if (this.fileDataChecksum != null) {
            i4 = this.fileDataChecksum.hashCode();
        } else {
            i4 = 0;
        }
        int i10 = (i9 + i4) * 37;
        if (this.override != null) {
            i5 = this.override.hashCode();
        } else {
            i5 = 0;
        }
        int i11 = (i10 + i5) * 37;
        if (this.supportsAcks != null) {
            i6 = this.supportsAcks.hashCode();
        }
        int result2 = i11 + i6;
        this.hashCode = result2;
        return result2;
    }
}
