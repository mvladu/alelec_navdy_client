package com.navdy.service.library.events.file;

public enum FileType implements com.squareup.wire.ProtoEnum {
    FILE_TYPE_OTA(1),
    FILE_TYPE_LOGS(2),
    FILE_TYPE_PERF_TEST(3);
    
    private final int value;

    private FileType(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
