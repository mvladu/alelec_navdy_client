package com.navdy.service.library.events.photo;

public final class PhotoUpdateQuery extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ALBUM = "";
    public static final java.lang.String DEFAULT_AUTHOR = "";
    public static final java.lang.String DEFAULT_CHECKSUM = "";
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    public static final com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT;
    public static final java.lang.Long DEFAULT_SIZE = java.lang.Long.valueOf(0);
    public static final java.lang.String DEFAULT_TRACK = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String album;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String author;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String checksum;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.photo.PhotoType photoType;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long size;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String track;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.photo.PhotoUpdateQuery> {
        public java.lang.String album;
        public java.lang.String author;
        public java.lang.String checksum;
        public java.lang.String identifier;
        public com.navdy.service.library.events.photo.PhotoType photoType;
        public java.lang.Long size;
        public java.lang.String track;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.photo.PhotoUpdateQuery message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.photoType = message.photoType;
                this.size = message.size;
                this.checksum = message.checksum;
                this.album = message.album;
                this.track = message.track;
                this.author = message.author;
            }
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder photoType(com.navdy.service.library.events.photo.PhotoType photoType2) {
            this.photoType = photoType2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder size(java.lang.Long size2) {
            this.size = size2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder checksum(java.lang.String checksum2) {
            this.checksum = checksum2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder album(java.lang.String album2) {
            this.album = album2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder track(java.lang.String track2) {
            this.track = track2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder author(java.lang.String author2) {
            this.author = author2;
            return this;
        }

        public com.navdy.service.library.events.photo.PhotoUpdateQuery build() {
            return new com.navdy.service.library.events.photo.PhotoUpdateQuery(this);
        }
    }

    public PhotoUpdateQuery(java.lang.String identifier2, com.navdy.service.library.events.photo.PhotoType photoType2, java.lang.Long size2, java.lang.String checksum2, java.lang.String album2, java.lang.String track2, java.lang.String author2) {
        this.identifier = identifier2;
        this.photoType = photoType2;
        this.size = size2;
        this.checksum = checksum2;
        this.album = album2;
        this.track = track2;
        this.author = author2;
    }

    private PhotoUpdateQuery(com.navdy.service.library.events.photo.PhotoUpdateQuery.Builder builder) {
        this(builder.identifier, builder.photoType, builder.size, builder.checksum, builder.album, builder.track, builder.author);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.photo.PhotoUpdateQuery)) {
            return false;
        }
        com.navdy.service.library.events.photo.PhotoUpdateQuery o = (com.navdy.service.library.events.photo.PhotoUpdateQuery) other;
        if (!equals((java.lang.Object) this.identifier, (java.lang.Object) o.identifier) || !equals((java.lang.Object) this.photoType, (java.lang.Object) o.photoType) || !equals((java.lang.Object) this.size, (java.lang.Object) o.size) || !equals((java.lang.Object) this.checksum, (java.lang.Object) o.checksum) || !equals((java.lang.Object) this.album, (java.lang.Object) o.album) || !equals((java.lang.Object) this.track, (java.lang.Object) o.track) || !equals((java.lang.Object) this.author, (java.lang.Object) o.author)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.identifier != null ? this.identifier.hashCode() : 0) * 37;
        if (this.photoType != null) {
            i = this.photoType.hashCode();
        } else {
            i = 0;
        }
        int i7 = (hashCode + i) * 37;
        if (this.size != null) {
            i2 = this.size.hashCode();
        } else {
            i2 = 0;
        }
        int i8 = (i7 + i2) * 37;
        if (this.checksum != null) {
            i3 = this.checksum.hashCode();
        } else {
            i3 = 0;
        }
        int i9 = (i8 + i3) * 37;
        if (this.album != null) {
            i4 = this.album.hashCode();
        } else {
            i4 = 0;
        }
        int i10 = (i9 + i4) * 37;
        if (this.track != null) {
            i5 = this.track.hashCode();
        } else {
            i5 = 0;
        }
        int i11 = (i10 + i5) * 37;
        if (this.author != null) {
            i6 = this.author.hashCode();
        }
        int result2 = i11 + i6;
        this.hashCode = result2;
        return result2;
    }
}
