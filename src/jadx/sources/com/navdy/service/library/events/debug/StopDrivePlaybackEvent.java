package com.navdy.service.library.events.debug;

public final class StopDrivePlaybackEvent extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.debug.StopDrivePlaybackEvent> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.debug.StopDrivePlaybackEvent message) {
            super(message);
        }

        public com.navdy.service.library.events.debug.StopDrivePlaybackEvent build() {
            return new com.navdy.service.library.events.debug.StopDrivePlaybackEvent(this);
        }
    }

    public StopDrivePlaybackEvent() {
    }

    private StopDrivePlaybackEvent(com.navdy.service.library.events.debug.StopDrivePlaybackEvent.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.debug.StopDrivePlaybackEvent;
    }

    public int hashCode() {
        return 0;
    }
}
