package com.navdy.service.library.events.calendars;

public final class CalendarEvent extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_ALL_DAY = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_CALENDAR_NAME = "";
    public static final java.lang.Integer DEFAULT_COLOR = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_DISPLAY_NAME = "";
    public static final java.lang.Long DEFAULT_END_TIME = java.lang.Long.valueOf(0);
    public static final java.lang.String DEFAULT_LOCATION = "";
    public static final java.lang.Long DEFAULT_START_TIME = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean all_day;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String calendar_name;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.FIXED32)
    public final java.lang.Integer color;
    @com.squareup.wire.ProtoField(tag = 8)
    public final com.navdy.service.library.events.destination.Destination destination;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String display_name;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long end_time;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String location;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long start_time;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.calendars.CalendarEvent> {
        public java.lang.Boolean all_day;
        public java.lang.String calendar_name;
        public java.lang.Integer color;
        public com.navdy.service.library.events.destination.Destination destination;
        public java.lang.String display_name;
        public java.lang.Long end_time;
        public java.lang.String location;
        public java.lang.Long start_time;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.calendars.CalendarEvent message) {
            super(message);
            if (message != null) {
                this.display_name = message.display_name;
                this.start_time = message.start_time;
                this.end_time = message.end_time;
                this.all_day = message.all_day;
                this.color = message.color;
                this.location = message.location;
                this.calendar_name = message.calendar_name;
                this.destination = message.destination;
            }
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder display_name(java.lang.String display_name2) {
            this.display_name = display_name2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder start_time(java.lang.Long start_time2) {
            this.start_time = start_time2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder end_time(java.lang.Long end_time2) {
            this.end_time = end_time2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder all_day(java.lang.Boolean all_day2) {
            this.all_day = all_day2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder color(java.lang.Integer color2) {
            this.color = color2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder location(java.lang.String location2) {
            this.location = location2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder calendar_name(java.lang.String calendar_name2) {
            this.calendar_name = calendar_name2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent.Builder destination(com.navdy.service.library.events.destination.Destination destination2) {
            this.destination = destination2;
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEvent build() {
            return new com.navdy.service.library.events.calendars.CalendarEvent(this);
        }
    }

    public CalendarEvent(java.lang.String display_name2, java.lang.Long start_time2, java.lang.Long end_time2, java.lang.Boolean all_day2, java.lang.Integer color2, java.lang.String location2, java.lang.String calendar_name2, com.navdy.service.library.events.destination.Destination destination2) {
        this.display_name = display_name2;
        this.start_time = start_time2;
        this.end_time = end_time2;
        this.all_day = all_day2;
        this.color = color2;
        this.location = location2;
        this.calendar_name = calendar_name2;
        this.destination = destination2;
    }

    private CalendarEvent(com.navdy.service.library.events.calendars.CalendarEvent.Builder builder) {
        this(builder.display_name, builder.start_time, builder.end_time, builder.all_day, builder.color, builder.location, builder.calendar_name, builder.destination);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.calendars.CalendarEvent)) {
            return false;
        }
        com.navdy.service.library.events.calendars.CalendarEvent o = (com.navdy.service.library.events.calendars.CalendarEvent) other;
        if (!equals((java.lang.Object) this.display_name, (java.lang.Object) o.display_name) || !equals((java.lang.Object) this.start_time, (java.lang.Object) o.start_time) || !equals((java.lang.Object) this.end_time, (java.lang.Object) o.end_time) || !equals((java.lang.Object) this.all_day, (java.lang.Object) o.all_day) || !equals((java.lang.Object) this.color, (java.lang.Object) o.color) || !equals((java.lang.Object) this.location, (java.lang.Object) o.location) || !equals((java.lang.Object) this.calendar_name, (java.lang.Object) o.calendar_name) || !equals((java.lang.Object) this.destination, (java.lang.Object) o.destination)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.display_name != null ? this.display_name.hashCode() : 0) * 37;
        if (this.start_time != null) {
            i = this.start_time.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.end_time != null) {
            i2 = this.end_time.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.all_day != null) {
            i3 = this.all_day.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.color != null) {
            i4 = this.color.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.location != null) {
            i5 = this.location.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 37;
        if (this.calendar_name != null) {
            i6 = this.calendar_name.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 37;
        if (this.destination != null) {
            i7 = this.destination.hashCode();
        }
        int result2 = i13 + i7;
        this.hashCode = result2;
        return result2;
    }
}
