package com.navdy.service.library.events.places;

public final class DestinationSelectedResponse extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_REQUEST_ID = "";
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_REQUEST_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3)
    public final com.navdy.service.library.events.destination.Destination destination;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String request_id;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus request_status;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.DestinationSelectedResponse> {
        public com.navdy.service.library.events.destination.Destination destination;
        public java.lang.String request_id;
        public com.navdy.service.library.events.RequestStatus request_status;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.DestinationSelectedResponse message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.request_status = message.request_status;
                this.destination = message.destination;
            }
        }

        public com.navdy.service.library.events.places.DestinationSelectedResponse.Builder request_id(java.lang.String request_id2) {
            this.request_id = request_id2;
            return this;
        }

        public com.navdy.service.library.events.places.DestinationSelectedResponse.Builder request_status(com.navdy.service.library.events.RequestStatus request_status2) {
            this.request_status = request_status2;
            return this;
        }

        public com.navdy.service.library.events.places.DestinationSelectedResponse.Builder destination(com.navdy.service.library.events.destination.Destination destination2) {
            this.destination = destination2;
            return this;
        }

        public com.navdy.service.library.events.places.DestinationSelectedResponse build() {
            return new com.navdy.service.library.events.places.DestinationSelectedResponse(this);
        }
    }

    public DestinationSelectedResponse(java.lang.String request_id2, com.navdy.service.library.events.RequestStatus request_status2, com.navdy.service.library.events.destination.Destination destination2) {
        this.request_id = request_id2;
        this.request_status = request_status2;
        this.destination = destination2;
    }

    private DestinationSelectedResponse(com.navdy.service.library.events.places.DestinationSelectedResponse.Builder builder) {
        this(builder.request_id, builder.request_status, builder.destination);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.DestinationSelectedResponse)) {
            return false;
        }
        com.navdy.service.library.events.places.DestinationSelectedResponse o = (com.navdy.service.library.events.places.DestinationSelectedResponse) other;
        if (!equals((java.lang.Object) this.request_id, (java.lang.Object) o.request_id) || !equals((java.lang.Object) this.request_status, (java.lang.Object) o.request_status) || !equals((java.lang.Object) this.destination, (java.lang.Object) o.destination)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.request_id != null ? this.request_id.hashCode() : 0) * 37;
        if (this.request_status != null) {
            i = this.request_status.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.destination != null) {
            i2 = this.destination.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
