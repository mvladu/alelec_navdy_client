package com.navdy.service.library.events.navigation;

public final class NavigationSessionResponse extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_PENDINGSESSIONSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    public static final java.lang.String DEFAULT_ROUTEID = "";
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.navigation.NavigationSessionState pendingSessionState;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String routeId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationSessionResponse> {
        public com.navdy.service.library.events.navigation.NavigationSessionState pendingSessionState;
        public java.lang.String routeId;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationSessionResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.pendingSessionState = message.pendingSessionState;
                this.routeId = message.routeId;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationSessionResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionResponse.Builder pendingSessionState(com.navdy.service.library.events.navigation.NavigationSessionState pendingSessionState2) {
            this.pendingSessionState = pendingSessionState2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionResponse.Builder routeId(java.lang.String routeId2) {
            this.routeId = routeId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationSessionResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationSessionResponse(this);
        }
    }

    public NavigationSessionResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, com.navdy.service.library.events.navigation.NavigationSessionState pendingSessionState2, java.lang.String routeId2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.pendingSessionState = pendingSessionState2;
        this.routeId = routeId2;
    }

    private NavigationSessionResponse(com.navdy.service.library.events.navigation.NavigationSessionResponse.Builder builder) {
        this(builder.status, builder.statusDetail, builder.pendingSessionState, builder.routeId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationSessionResponse)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationSessionResponse o = (com.navdy.service.library.events.navigation.NavigationSessionResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals((java.lang.Object) this.pendingSessionState, (java.lang.Object) o.pendingSessionState) || !equals((java.lang.Object) this.routeId, (java.lang.Object) o.routeId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        } else {
            i = 0;
        }
        int i4 = (hashCode + i) * 37;
        if (this.pendingSessionState != null) {
            i2 = this.pendingSessionState.hashCode();
        } else {
            i2 = 0;
        }
        int i5 = (i4 + i2) * 37;
        if (this.routeId != null) {
            i3 = this.routeId.hashCode();
        }
        int result2 = i5 + i3;
        this.hashCode = result2;
        return result2;
    }
}
