package com.navdy.service.library.events.debug;

public final class DriveRecordingsRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.debug.DriveRecordingsRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.debug.DriveRecordingsRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.debug.DriveRecordingsRequest build() {
            return new com.navdy.service.library.events.debug.DriveRecordingsRequest(this);
        }
    }

    public DriveRecordingsRequest() {
    }

    private DriveRecordingsRequest(com.navdy.service.library.events.debug.DriveRecordingsRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.debug.DriveRecordingsRequest;
    }

    public int hashCode() {
        return 0;
    }
}
