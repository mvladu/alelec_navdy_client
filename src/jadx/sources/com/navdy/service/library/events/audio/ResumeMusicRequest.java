package com.navdy.service.library.events.audio;

public final class ResumeMusicRequest extends com.squareup.wire.Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.ResumeMusicRequest> {
        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.ResumeMusicRequest message) {
            super(message);
        }

        public com.navdy.service.library.events.audio.ResumeMusicRequest build() {
            return new com.navdy.service.library.events.audio.ResumeMusicRequest(this);
        }
    }

    public ResumeMusicRequest() {
    }

    private ResumeMusicRequest(com.navdy.service.library.events.audio.ResumeMusicRequest.Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        return other instanceof com.navdy.service.library.events.audio.ResumeMusicRequest;
    }

    public int hashCode() {
        return 0;
    }
}
