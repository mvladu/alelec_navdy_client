package com.navdy.service.library.events.audio;

public final class VoiceSearchResponse extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_CONFIDENCELEVEL = java.lang.Integer.valueOf(0);
    public static final com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError DEFAULT_ERROR = com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NOT_AVAILABLE;
    public static final java.lang.Boolean DEFAULT_LISTENINGOVERBLUETOOTH = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_LOCALE = "";
    public static final java.lang.String DEFAULT_PREFIX = "";
    public static final java.lang.String DEFAULT_RECOGNIZEDWORDS = "";
    public static final java.util.List<com.navdy.service.library.events.destination.Destination> DEFAULT_RESULTS = java.util.Collections.emptyList();
    public static final com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState DEFAULT_STATE = com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_ERROR;
    public static final java.lang.Integer DEFAULT_VOLUMELEVEL = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer confidenceLevel;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError error;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean listeningOverBluetooth;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String locale;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String prefix;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String recognizedWords;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.destination.Destination.class, tag = 7)
    public final java.util.List<com.navdy.service.library.events.destination.Destination> results;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState state;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer volumeLevel;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.VoiceSearchResponse> {
        public java.lang.Integer confidenceLevel;
        public com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError error;
        public java.lang.Boolean listeningOverBluetooth;
        public java.lang.String locale;
        public java.lang.String prefix;
        public java.lang.String recognizedWords;
        public java.util.List<com.navdy.service.library.events.destination.Destination> results;
        public com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState state;
        public java.lang.Integer volumeLevel;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.VoiceSearchResponse message) {
            super(message);
            if (message != null) {
                this.state = message.state;
                this.recognizedWords = message.recognizedWords;
                this.volumeLevel = message.volumeLevel;
                this.listeningOverBluetooth = message.listeningOverBluetooth;
                this.error = message.error;
                this.confidenceLevel = message.confidenceLevel;
                this.results = com.navdy.service.library.events.audio.VoiceSearchResponse.copyOf(message.results);
                this.prefix = message.prefix;
                this.locale = message.locale;
            }
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState state2) {
            this.state = state2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder recognizedWords(java.lang.String recognizedWords2) {
            this.recognizedWords = recognizedWords2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder volumeLevel(java.lang.Integer volumeLevel2) {
            this.volumeLevel = volumeLevel2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder listeningOverBluetooth(java.lang.Boolean listeningOverBluetooth2) {
            this.listeningOverBluetooth = listeningOverBluetooth2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError error2) {
            this.error = error2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder confidenceLevel(java.lang.Integer confidenceLevel2) {
            this.confidenceLevel = confidenceLevel2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder results(java.util.List<com.navdy.service.library.events.destination.Destination> results2) {
            this.results = checkForNulls(results2);
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder prefix(java.lang.String prefix2) {
            this.prefix = prefix2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse.Builder locale(java.lang.String locale2) {
            this.locale = locale2;
            return this;
        }

        public com.navdy.service.library.events.audio.VoiceSearchResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.audio.VoiceSearchResponse(this);
        }
    }

    public enum VoiceSearchError implements com.squareup.wire.ProtoEnum {
        NOT_AVAILABLE(0),
        NEED_PERMISSION(1),
        OFFLINE(2),
        FAILED_TO_START(3),
        NO_WORDS_RECOGNIZED(4),
        NO_RESULTS_FOUND(5),
        AMBIENT_NOISE(6),
        VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST(7);
        
        private final int value;

        private VoiceSearchError(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum VoiceSearchState implements com.squareup.wire.ProtoEnum {
        VOICE_SEARCH_ERROR(0),
        VOICE_SEARCH_STARTING(1),
        VOICE_SEARCH_LISTENING(2),
        VOICE_SEARCH_SEARCHING(3),
        VOICE_SEARCH_SUCCESS(4);
        
        private final int value;

        private VoiceSearchState(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public VoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState state2, java.lang.String recognizedWords2, java.lang.Integer volumeLevel2, java.lang.Boolean listeningOverBluetooth2, com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError error2, java.lang.Integer confidenceLevel2, java.util.List<com.navdy.service.library.events.destination.Destination> results2, java.lang.String prefix2, java.lang.String locale2) {
        this.state = state2;
        this.recognizedWords = recognizedWords2;
        this.volumeLevel = volumeLevel2;
        this.listeningOverBluetooth = listeningOverBluetooth2;
        this.error = error2;
        this.confidenceLevel = confidenceLevel2;
        this.results = immutableCopyOf(results2);
        this.prefix = prefix2;
        this.locale = locale2;
    }

    private VoiceSearchResponse(com.navdy.service.library.events.audio.VoiceSearchResponse.Builder builder) {
        this(builder.state, builder.recognizedWords, builder.volumeLevel, builder.listeningOverBluetooth, builder.error, builder.confidenceLevel, builder.results, builder.prefix, builder.locale);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.VoiceSearchResponse)) {
            return false;
        }
        com.navdy.service.library.events.audio.VoiceSearchResponse o = (com.navdy.service.library.events.audio.VoiceSearchResponse) other;
        if (!equals((java.lang.Object) this.state, (java.lang.Object) o.state) || !equals((java.lang.Object) this.recognizedWords, (java.lang.Object) o.recognizedWords) || !equals((java.lang.Object) this.volumeLevel, (java.lang.Object) o.volumeLevel) || !equals((java.lang.Object) this.listeningOverBluetooth, (java.lang.Object) o.listeningOverBluetooth) || !equals((java.lang.Object) this.error, (java.lang.Object) o.error) || !equals((java.lang.Object) this.confidenceLevel, (java.lang.Object) o.confidenceLevel) || !equals(this.results, o.results) || !equals((java.lang.Object) this.prefix, (java.lang.Object) o.prefix) || !equals((java.lang.Object) this.locale, (java.lang.Object) o.locale)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.state != null ? this.state.hashCode() : 0) * 37;
        if (this.recognizedWords != null) {
            i = this.recognizedWords.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.volumeLevel != null) {
            i2 = this.volumeLevel.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.listeningOverBluetooth != null) {
            i3 = this.listeningOverBluetooth.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.error != null) {
            i4 = this.error.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.confidenceLevel != null) {
            i5 = this.confidenceLevel.hashCode();
        } else {
            i5 = 0;
        }
        int hashCode2 = (((i11 + i5) * 37) + (this.results != null ? this.results.hashCode() : 1)) * 37;
        if (this.prefix != null) {
            i6 = this.prefix.hashCode();
        } else {
            i6 = 0;
        }
        int i12 = (hashCode2 + i6) * 37;
        if (this.locale != null) {
            i7 = this.locale.hashCode();
        }
        int result2 = i12 + i7;
        this.hashCode = result2;
        return result2;
    }
}
