package com.navdy.service.library.events.navigation;

public final class RouteManeuverRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ROUTEID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String routeId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.RouteManeuverRequest> {
        public java.lang.String routeId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.RouteManeuverRequest message) {
            super(message);
            if (message != null) {
                this.routeId = message.routeId;
            }
        }

        public com.navdy.service.library.events.navigation.RouteManeuverRequest.Builder routeId(java.lang.String routeId2) {
            this.routeId = routeId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.RouteManeuverRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.RouteManeuverRequest(this);
        }
    }

    public RouteManeuverRequest(java.lang.String routeId2) {
        this.routeId = routeId2;
    }

    private RouteManeuverRequest(com.navdy.service.library.events.navigation.RouteManeuverRequest.Builder builder) {
        this(builder.routeId);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.RouteManeuverRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.routeId, (java.lang.Object) ((com.navdy.service.library.events.navigation.RouteManeuverRequest) other).routeId);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.routeId != null ? this.routeId.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
