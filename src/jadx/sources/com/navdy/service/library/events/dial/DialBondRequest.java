package com.navdy.service.library.events.dial;

public final class DialBondRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.dial.DialBondRequest.DialAction DEFAULT_ACTION = com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_BOND;
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.dial.DialBondRequest.DialAction action;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.dial.DialBondRequest> {
        public com.navdy.service.library.events.dial.DialBondRequest.DialAction action;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.dial.DialBondRequest message) {
            super(message);
            if (message != null) {
                this.action = message.action;
            }
        }

        public com.navdy.service.library.events.dial.DialBondRequest.Builder action(com.navdy.service.library.events.dial.DialBondRequest.DialAction action2) {
            this.action = action2;
            return this;
        }

        public com.navdy.service.library.events.dial.DialBondRequest build() {
            return new com.navdy.service.library.events.dial.DialBondRequest(this);
        }
    }

    public enum DialAction implements com.squareup.wire.ProtoEnum {
        DIAL_BOND(1),
        DIAL_CLEAR_BOND(2),
        DIAL_REBOND(3);
        
        private final int value;

        private DialAction(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest.DialAction action2) {
        this.action = action2;
    }

    private DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest.Builder builder) {
        this(builder.action);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.dial.DialBondRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.action, (java.lang.Object) ((com.navdy.service.library.events.dial.DialBondRequest) other).action);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.action != null ? this.action.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
