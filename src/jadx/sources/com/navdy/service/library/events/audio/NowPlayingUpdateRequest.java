package com.navdy.service.library.events.audio;

public final class NowPlayingUpdateRequest extends com.squareup.wire.Message {
    public static final java.lang.Boolean DEFAULT_START = java.lang.Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean start;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.NowPlayingUpdateRequest> {
        public java.lang.Boolean start;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.NowPlayingUpdateRequest message) {
            super(message);
            if (message != null) {
                this.start = message.start;
            }
        }

        public com.navdy.service.library.events.audio.NowPlayingUpdateRequest.Builder start(java.lang.Boolean start2) {
            this.start = start2;
            return this;
        }

        public com.navdy.service.library.events.audio.NowPlayingUpdateRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.audio.NowPlayingUpdateRequest(this);
        }
    }

    public NowPlayingUpdateRequest(java.lang.Boolean start2) {
        this.start = start2;
    }

    private NowPlayingUpdateRequest(com.navdy.service.library.events.audio.NowPlayingUpdateRequest.Builder builder) {
        this(builder.start);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.NowPlayingUpdateRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.start, (java.lang.Object) ((com.navdy.service.library.events.audio.NowPlayingUpdateRequest) other).start);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.start != null ? this.start.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
