package com.navdy.service.library.events.debug;

public final class StartDriveRecordingEvent extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_LABEL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.debug.StartDriveRecordingEvent> {
        public java.lang.String label;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.debug.StartDriveRecordingEvent message) {
            super(message);
            if (message != null) {
                this.label = message.label;
            }
        }

        public com.navdy.service.library.events.debug.StartDriveRecordingEvent.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.debug.StartDriveRecordingEvent build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.debug.StartDriveRecordingEvent(this);
        }
    }

    public StartDriveRecordingEvent(java.lang.String label2) {
        this.label = label2;
    }

    private StartDriveRecordingEvent(com.navdy.service.library.events.debug.StartDriveRecordingEvent.Builder builder) {
        this(builder.label);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.debug.StartDriveRecordingEvent)) {
            return false;
        }
        return equals((java.lang.Object) this.label, (java.lang.Object) ((com.navdy.service.library.events.debug.StartDriveRecordingEvent) other).label);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.label != null ? this.label.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
