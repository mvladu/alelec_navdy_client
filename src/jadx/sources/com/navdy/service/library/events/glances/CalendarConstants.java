package com.navdy.service.library.events.glances;

public enum CalendarConstants implements com.squareup.wire.ProtoEnum {
    CALENDAR_TITLE(0),
    CALENDAR_TIME(1),
    CALENDAR_TIME_STR(2),
    CALENDAR_LOCATION(3);
    
    private final int value;

    private CalendarConstants(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
