package com.navdy.service.library.events.calendars;

public final class CalendarEventUpdates extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> DEFAULT_CALENDAR_EVENTS = java.util.Collections.emptyList();
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.calendars.CalendarEvent.class, tag = 1)
    public final java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> calendar_events;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.calendars.CalendarEventUpdates> {
        public java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> calendar_events;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.calendars.CalendarEventUpdates message) {
            super(message);
            if (message != null) {
                this.calendar_events = com.navdy.service.library.events.calendars.CalendarEventUpdates.copyOf(message.calendar_events);
            }
        }

        public com.navdy.service.library.events.calendars.CalendarEventUpdates.Builder calendar_events(java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> calendar_events2) {
            this.calendar_events = checkForNulls(calendar_events2);
            return this;
        }

        public com.navdy.service.library.events.calendars.CalendarEventUpdates build() {
            return new com.navdy.service.library.events.calendars.CalendarEventUpdates(this);
        }
    }

    public CalendarEventUpdates(java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> calendar_events2) {
        this.calendar_events = immutableCopyOf(calendar_events2);
    }

    private CalendarEventUpdates(com.navdy.service.library.events.calendars.CalendarEventUpdates.Builder builder) {
        this(builder.calendar_events);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.calendars.CalendarEventUpdates)) {
            return false;
        }
        return equals(this.calendar_events, ((com.navdy.service.library.events.calendars.CalendarEventUpdates) other).calendar_events);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.calendar_events != null ? this.calendar_events.hashCode() : 1;
        this.hashCode = i;
        return i;
    }
}
