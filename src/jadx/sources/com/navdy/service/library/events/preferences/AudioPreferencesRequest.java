package com.navdy.service.library.events.preferences;

public final class AudioPreferencesRequest extends com.squareup.wire.Message {
    public static final java.lang.Long DEFAULT_SERIAL_NUMBER = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.preferences.AudioPreferencesRequest> {
        public java.lang.Long serial_number;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.preferences.AudioPreferencesRequest message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
            }
        }

        public com.navdy.service.library.events.preferences.AudioPreferencesRequest.Builder serial_number(java.lang.Long serial_number2) {
            this.serial_number = serial_number2;
            return this;
        }

        public com.navdy.service.library.events.preferences.AudioPreferencesRequest build() {
            return new com.navdy.service.library.events.preferences.AudioPreferencesRequest(this);
        }
    }

    public AudioPreferencesRequest(java.lang.Long serial_number2) {
        this.serial_number = serial_number2;
    }

    private AudioPreferencesRequest(com.navdy.service.library.events.preferences.AudioPreferencesRequest.Builder builder) {
        this(builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.preferences.AudioPreferencesRequest)) {
            return false;
        }
        return equals((java.lang.Object) this.serial_number, (java.lang.Object) ((com.navdy.service.library.events.preferences.AudioPreferencesRequest) other).serial_number);
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int i = this.serial_number != null ? this.serial_number.hashCode() : 0;
        this.hashCode = i;
        return i;
    }
}
