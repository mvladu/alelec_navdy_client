package com.navdy.service.library.events.audio;

public final class PlayAudioRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.audio.Audio DEFAULT_AUDIO = com.navdy.service.library.events.audio.Audio.DRIVE_BEHAVIOR_SPEEDING;
    public static final com.navdy.service.library.events.audio.Category DEFAULT_CATEGORY = com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN;
    public static final java.lang.Long DEFAULT_ID = java.lang.Long.valueOf(0);
    public static final java.lang.String DEFAULT_LANGUAGE = "";
    public static final java.lang.Long DEFAULT_TIMESTAMP = java.lang.Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.Audio audio;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.Category category;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long id;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String language;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long timeStamp;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.PlayAudioRequest> {
        public com.navdy.service.library.events.audio.Audio audio;
        public com.navdy.service.library.events.audio.Category category;
        public java.lang.Long id;
        public java.lang.String language;
        public java.lang.Long timeStamp;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.PlayAudioRequest message) {
            super(message);
            if (message != null) {
                this.id = message.id;
                this.category = message.category;
                this.audio = message.audio;
                this.language = message.language;
                this.timeStamp = message.timeStamp;
            }
        }

        public com.navdy.service.library.events.audio.PlayAudioRequest.Builder id(java.lang.Long id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.audio.PlayAudioRequest.Builder category(com.navdy.service.library.events.audio.Category category2) {
            this.category = category2;
            return this;
        }

        public com.navdy.service.library.events.audio.PlayAudioRequest.Builder audio(com.navdy.service.library.events.audio.Audio audio2) {
            this.audio = audio2;
            return this;
        }

        public com.navdy.service.library.events.audio.PlayAudioRequest.Builder language(java.lang.String language2) {
            this.language = language2;
            return this;
        }

        public com.navdy.service.library.events.audio.PlayAudioRequest.Builder timeStamp(java.lang.Long timeStamp2) {
            this.timeStamp = timeStamp2;
            return this;
        }

        public com.navdy.service.library.events.audio.PlayAudioRequest build() {
            return new com.navdy.service.library.events.audio.PlayAudioRequest(this);
        }
    }

    public PlayAudioRequest(java.lang.Long id2, com.navdy.service.library.events.audio.Category category2, com.navdy.service.library.events.audio.Audio audio2, java.lang.String language2, java.lang.Long timeStamp2) {
        this.id = id2;
        this.category = category2;
        this.audio = audio2;
        this.language = language2;
        this.timeStamp = timeStamp2;
    }

    private PlayAudioRequest(com.navdy.service.library.events.audio.PlayAudioRequest.Builder builder) {
        this(builder.id, builder.category, builder.audio, builder.language, builder.timeStamp);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.PlayAudioRequest)) {
            return false;
        }
        com.navdy.service.library.events.audio.PlayAudioRequest o = (com.navdy.service.library.events.audio.PlayAudioRequest) other;
        if (!equals((java.lang.Object) this.id, (java.lang.Object) o.id) || !equals((java.lang.Object) this.category, (java.lang.Object) o.category) || !equals((java.lang.Object) this.audio, (java.lang.Object) o.audio) || !equals((java.lang.Object) this.language, (java.lang.Object) o.language) || !equals((java.lang.Object) this.timeStamp, (java.lang.Object) o.timeStamp)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.id != null ? this.id.hashCode() : 0) * 37;
        if (this.category != null) {
            i = this.category.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.audio != null) {
            i2 = this.audio.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.language != null) {
            i3 = this.language.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.timeStamp != null) {
            i4 = this.timeStamp.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
