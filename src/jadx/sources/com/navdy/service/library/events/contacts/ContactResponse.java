package com.navdy.service.library.events.contacts;

public final class ContactResponse extends com.squareup.wire.Message {
    public static final java.util.List<com.navdy.service.library.events.contacts.Contact> DEFAULT_CONTACTS = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_IDENTIFIER = "";
    public static final com.navdy.service.library.events.RequestStatus DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    public static final java.lang.String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.contacts.Contact.class, tag = 5)
    public final java.util.List<com.navdy.service.library.events.contacts.Contact> contacts;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String identifier;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.RequestStatus status;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.contacts.ContactResponse> {
        public java.util.List<com.navdy.service.library.events.contacts.Contact> contacts;
        public java.lang.String identifier;
        public com.navdy.service.library.events.RequestStatus status;
        public java.lang.String statusDetail;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.contacts.ContactResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.identifier = message.identifier;
                this.contacts = com.navdy.service.library.events.contacts.ContactResponse.copyOf(message.contacts);
            }
        }

        public com.navdy.service.library.events.contacts.ContactResponse.Builder status(com.navdy.service.library.events.RequestStatus status2) {
            this.status = status2;
            return this;
        }

        public com.navdy.service.library.events.contacts.ContactResponse.Builder statusDetail(java.lang.String statusDetail2) {
            this.statusDetail = statusDetail2;
            return this;
        }

        public com.navdy.service.library.events.contacts.ContactResponse.Builder identifier(java.lang.String identifier2) {
            this.identifier = identifier2;
            return this;
        }

        public com.navdy.service.library.events.contacts.ContactResponse.Builder contacts(java.util.List<com.navdy.service.library.events.contacts.Contact> contacts2) {
            this.contacts = checkForNulls(contacts2);
            return this;
        }

        public com.navdy.service.library.events.contacts.ContactResponse build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.contacts.ContactResponse(this);
        }
    }

    public ContactResponse(com.navdy.service.library.events.RequestStatus status2, java.lang.String statusDetail2, java.lang.String identifier2, java.util.List<com.navdy.service.library.events.contacts.Contact> contacts2) {
        this.status = status2;
        this.statusDetail = statusDetail2;
        this.identifier = identifier2;
        this.contacts = immutableCopyOf(contacts2);
    }

    private ContactResponse(com.navdy.service.library.events.contacts.ContactResponse.Builder builder) {
        this(builder.status, builder.statusDetail, builder.identifier, builder.contacts);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.contacts.ContactResponse)) {
            return false;
        }
        com.navdy.service.library.events.contacts.ContactResponse o = (com.navdy.service.library.events.contacts.ContactResponse) other;
        if (!equals((java.lang.Object) this.status, (java.lang.Object) o.status) || !equals((java.lang.Object) this.statusDetail, (java.lang.Object) o.statusDetail) || !equals((java.lang.Object) this.identifier, (java.lang.Object) o.identifier) || !equals(this.contacts, o.contacts)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.identifier != null) {
            i2 = this.identifier.hashCode();
        }
        int result2 = ((i3 + i2) * 37) + (this.contacts != null ? this.contacts.hashCode() : 1);
        this.hashCode = result2;
        return result2;
    }
}
