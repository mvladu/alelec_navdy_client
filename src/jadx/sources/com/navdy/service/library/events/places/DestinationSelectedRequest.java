package com.navdy.service.library.events.places;

public final class DestinationSelectedRequest extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_REQUEST_ID = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2)
    public final com.navdy.service.library.events.destination.Destination destination;
    @com.squareup.wire.ProtoField(tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String request_id;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.places.DestinationSelectedRequest> {
        public com.navdy.service.library.events.destination.Destination destination;
        public java.lang.String request_id;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.places.DestinationSelectedRequest message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.destination = message.destination;
            }
        }

        public com.navdy.service.library.events.places.DestinationSelectedRequest.Builder request_id(java.lang.String request_id2) {
            this.request_id = request_id2;
            return this;
        }

        public com.navdy.service.library.events.places.DestinationSelectedRequest.Builder destination(com.navdy.service.library.events.destination.Destination destination2) {
            this.destination = destination2;
            return this;
        }

        public com.navdy.service.library.events.places.DestinationSelectedRequest build() {
            return new com.navdy.service.library.events.places.DestinationSelectedRequest(this);
        }
    }

    public DestinationSelectedRequest(java.lang.String request_id2, com.navdy.service.library.events.destination.Destination destination2) {
        this.request_id = request_id2;
        this.destination = destination2;
    }

    private DestinationSelectedRequest(com.navdy.service.library.events.places.DestinationSelectedRequest.Builder builder) {
        this(builder.request_id, builder.destination);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.places.DestinationSelectedRequest)) {
            return false;
        }
        com.navdy.service.library.events.places.DestinationSelectedRequest o = (com.navdy.service.library.events.places.DestinationSelectedRequest) other;
        if (!equals((java.lang.Object) this.request_id, (java.lang.Object) o.request_id) || !equals((java.lang.Object) this.destination, (java.lang.Object) o.destination)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        int result2 = this.hashCode;
        if (result2 != 0) {
            return result2;
        }
        if (this.request_id != null) {
            result = this.request_id.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.destination != null) {
            i = this.destination.hashCode();
        }
        int result3 = i2 + i;
        this.hashCode = result3;
        return result3;
    }
}
