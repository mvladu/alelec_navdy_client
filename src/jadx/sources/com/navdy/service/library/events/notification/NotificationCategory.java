package com.navdy.service.library.events.notification;

public enum NotificationCategory implements com.squareup.wire.ProtoEnum {
    CATEGORY_OTHER(0),
    CATEGORY_INCOMING_CALL(1),
    CATEGORY_MISSED_CALL(2),
    CATEGORY_VOICE_MAIL(3),
    CATEGORY_SOCIAL(4),
    CATEGORY_SCHEDULE(5),
    CATEGORY_EMAIL(6),
    CATEGORY_NEWS(7),
    CATEGORY_HEALTH_AND_FITNESS(8),
    CATEGORY_BUSINESS_AND_FINANCE(9),
    CATEGORY_LOCATION(10),
    CATEGORY_ENTERTAINMENT(11);
    
    private final int value;

    private NotificationCategory(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
