package com.navdy.service.library.events.audio;

public final class SpeechRequest extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.audio.Category DEFAULT_CATEGORY = com.navdy.service.library.events.audio.Category.SPEECH_TURN_BY_TURN;
    public static final java.lang.String DEFAULT_ID = "";
    public static final java.lang.String DEFAULT_LANGUAGE = "";
    public static final java.lang.Boolean DEFAULT_SENDSTATUS = java.lang.Boolean.valueOf(false);
    public static final java.lang.String DEFAULT_WORDS = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.audio.Category category;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String id;
    @com.squareup.wire.ProtoField(tag = 5, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String language;
    @com.squareup.wire.ProtoField(tag = 4, type = com.squareup.wire.Message.Datatype.BOOL)
    public final java.lang.Boolean sendStatus;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String words;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.audio.SpeechRequest> {
        public com.navdy.service.library.events.audio.Category category;
        public java.lang.String id;
        public java.lang.String language;
        public java.lang.Boolean sendStatus;
        public java.lang.String words;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.audio.SpeechRequest message) {
            super(message);
            if (message != null) {
                this.words = message.words;
                this.category = message.category;
                this.id = message.id;
                this.sendStatus = message.sendStatus;
                this.language = message.language;
            }
        }

        public com.navdy.service.library.events.audio.SpeechRequest.Builder words(java.lang.String words2) {
            this.words = words2;
            return this;
        }

        public com.navdy.service.library.events.audio.SpeechRequest.Builder category(com.navdy.service.library.events.audio.Category category2) {
            this.category = category2;
            return this;
        }

        public com.navdy.service.library.events.audio.SpeechRequest.Builder id(java.lang.String id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.audio.SpeechRequest.Builder sendStatus(java.lang.Boolean sendStatus2) {
            this.sendStatus = sendStatus2;
            return this;
        }

        public com.navdy.service.library.events.audio.SpeechRequest.Builder language(java.lang.String language2) {
            this.language = language2;
            return this;
        }

        public com.navdy.service.library.events.audio.SpeechRequest build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.audio.SpeechRequest(this);
        }
    }

    public SpeechRequest(java.lang.String words2, com.navdy.service.library.events.audio.Category category2, java.lang.String id2, java.lang.Boolean sendStatus2, java.lang.String language2) {
        this.words = words2;
        this.category = category2;
        this.id = id2;
        this.sendStatus = sendStatus2;
        this.language = language2;
    }

    private SpeechRequest(com.navdy.service.library.events.audio.SpeechRequest.Builder builder) {
        this(builder.words, builder.category, builder.id, builder.sendStatus, builder.language);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.audio.SpeechRequest)) {
            return false;
        }
        com.navdy.service.library.events.audio.SpeechRequest o = (com.navdy.service.library.events.audio.SpeechRequest) other;
        if (!equals((java.lang.Object) this.words, (java.lang.Object) o.words) || !equals((java.lang.Object) this.category, (java.lang.Object) o.category) || !equals((java.lang.Object) this.id, (java.lang.Object) o.id) || !equals((java.lang.Object) this.sendStatus, (java.lang.Object) o.sendStatus) || !equals((java.lang.Object) this.language, (java.lang.Object) o.language)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.words != null ? this.words.hashCode() : 0) * 37;
        if (this.category != null) {
            i = this.category.hashCode();
        } else {
            i = 0;
        }
        int i5 = (hashCode + i) * 37;
        if (this.id != null) {
            i2 = this.id.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 37;
        if (this.sendStatus != null) {
            i3 = this.sendStatus.hashCode();
        } else {
            i3 = 0;
        }
        int i7 = (i6 + i3) * 37;
        if (this.language != null) {
            i4 = this.language.hashCode();
        }
        int result2 = i7 + i4;
        this.hashCode = result2;
        return result2;
    }
}
