package com.navdy.service.library.events.notification;

public final class NotificationAction extends com.squareup.wire.Message {
    public static final java.lang.Integer DEFAULT_ACTIONID = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_HANDLER = "";
    public static final java.lang.Integer DEFAULT_ICONRESOURCE = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_ICONRESOURCEFOCUSED = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_ID = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.Integer DEFAULT_LABELID = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_NOTIFICATIONID = java.lang.Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer actionId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String handler;
    @com.squareup.wire.ProtoField(tag = 7, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer iconResource;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer iconResourceFocused;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer id;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer labelId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 3, type = com.squareup.wire.Message.Datatype.INT32)
    public final java.lang.Integer notificationId;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.notification.NotificationAction> {
        public java.lang.Integer actionId;
        public java.lang.String handler;
        public java.lang.Integer iconResource;
        public java.lang.Integer iconResourceFocused;
        public java.lang.Integer id;
        public java.lang.String label;
        public java.lang.Integer labelId;
        public java.lang.Integer notificationId;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.notification.NotificationAction message) {
            super(message);
            if (message != null) {
                this.handler = message.handler;
                this.id = message.id;
                this.notificationId = message.notificationId;
                this.actionId = message.actionId;
                this.labelId = message.labelId;
                this.label = message.label;
                this.iconResource = message.iconResource;
                this.iconResourceFocused = message.iconResourceFocused;
            }
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder handler(java.lang.String handler2) {
            this.handler = handler2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder id(java.lang.Integer id2) {
            this.id = id2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder notificationId(java.lang.Integer notificationId2) {
            this.notificationId = notificationId2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder actionId(java.lang.Integer actionId2) {
            this.actionId = actionId2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder labelId(java.lang.Integer labelId2) {
            this.labelId = labelId2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder iconResource(java.lang.Integer iconResource2) {
            this.iconResource = iconResource2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction.Builder iconResourceFocused(java.lang.Integer iconResourceFocused2) {
            this.iconResourceFocused = iconResourceFocused2;
            return this;
        }

        public com.navdy.service.library.events.notification.NotificationAction build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.notification.NotificationAction(this);
        }
    }

    public NotificationAction(java.lang.String handler2, java.lang.Integer id2, java.lang.Integer notificationId2, java.lang.Integer actionId2, java.lang.Integer labelId2, java.lang.String label2, java.lang.Integer iconResource2, java.lang.Integer iconResourceFocused2) {
        this.handler = handler2;
        this.id = id2;
        this.notificationId = notificationId2;
        this.actionId = actionId2;
        this.labelId = labelId2;
        this.label = label2;
        this.iconResource = iconResource2;
        this.iconResourceFocused = iconResourceFocused2;
    }

    private NotificationAction(com.navdy.service.library.events.notification.NotificationAction.Builder builder) {
        this(builder.handler, builder.id, builder.notificationId, builder.actionId, builder.labelId, builder.label, builder.iconResource, builder.iconResourceFocused);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.notification.NotificationAction)) {
            return false;
        }
        com.navdy.service.library.events.notification.NotificationAction o = (com.navdy.service.library.events.notification.NotificationAction) other;
        if (!equals((java.lang.Object) this.handler, (java.lang.Object) o.handler) || !equals((java.lang.Object) this.id, (java.lang.Object) o.id) || !equals((java.lang.Object) this.notificationId, (java.lang.Object) o.notificationId) || !equals((java.lang.Object) this.actionId, (java.lang.Object) o.actionId) || !equals((java.lang.Object) this.labelId, (java.lang.Object) o.labelId) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals((java.lang.Object) this.iconResource, (java.lang.Object) o.iconResource) || !equals((java.lang.Object) this.iconResourceFocused, (java.lang.Object) o.iconResourceFocused)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.handler != null ? this.handler.hashCode() : 0) * 37;
        if (this.id != null) {
            i = this.id.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.notificationId != null) {
            i2 = this.notificationId.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.actionId != null) {
            i3 = this.actionId.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.labelId != null) {
            i4 = this.labelId.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.label != null) {
            i5 = this.label.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 37;
        if (this.iconResource != null) {
            i6 = this.iconResource.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 37;
        if (this.iconResourceFocused != null) {
            i7 = this.iconResourceFocused.hashCode();
        }
        int result2 = i13 + i7;
        this.hashCode = result2;
        return result2;
    }
}
