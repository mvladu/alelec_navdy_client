package com.navdy.service.library.events.navigation;

public final class NavigationRouteResult extends com.squareup.wire.Message {
    public static final java.lang.String DEFAULT_ADDRESS = "";
    public static final java.lang.Integer DEFAULT_DURATION = java.lang.Integer.valueOf(0);
    public static final java.lang.Integer DEFAULT_DURATION_TRAFFIC = java.lang.Integer.valueOf(0);
    public static final java.util.List<com.navdy.service.library.events.location.Coordinate> DEFAULT_GEOPOINTS_OBSOLETE = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_LABEL = "";
    public static final java.lang.Integer DEFAULT_LENGTH = java.lang.Integer.valueOf(0);
    public static final java.lang.String DEFAULT_ROUTEID = "";
    public static final java.util.List<java.lang.Float> DEFAULT_ROUTELATLONGS = java.util.Collections.emptyList();
    public static final java.lang.String DEFAULT_VIA = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 9, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String address;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 5, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer duration;
    @com.squareup.wire.ProtoField(tag = 6, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer duration_traffic;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REPEATED, messageType = com.navdy.service.library.events.location.Coordinate.class, tag = 3)
    public final java.util.List<com.navdy.service.library.events.location.Coordinate> geoPoints_OBSOLETE;
    @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String label;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 4, type = com.squareup.wire.Message.Datatype.UINT32)
    public final java.lang.Integer length;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String routeId;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.PACKED, tag = 7, type = com.squareup.wire.Message.Datatype.FLOAT)
    public final java.util.List<java.lang.Float> routeLatLongs;
    @com.squareup.wire.ProtoField(tag = 8, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String via;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.navigation.NavigationRouteResult> {
        public java.lang.String address;
        public java.lang.Integer duration;
        public java.lang.Integer duration_traffic;
        public java.util.List<com.navdy.service.library.events.location.Coordinate> geoPoints_OBSOLETE;
        public java.lang.String label;
        public java.lang.Integer length;
        public java.lang.String routeId;
        public java.util.List<java.lang.Float> routeLatLongs;
        public java.lang.String via;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.navigation.NavigationRouteResult message) {
            super(message);
            if (message != null) {
                this.routeId = message.routeId;
                this.label = message.label;
                this.geoPoints_OBSOLETE = com.navdy.service.library.events.navigation.NavigationRouteResult.copyOf(message.geoPoints_OBSOLETE);
                this.length = message.length;
                this.duration = message.duration;
                this.duration_traffic = message.duration_traffic;
                this.routeLatLongs = com.navdy.service.library.events.navigation.NavigationRouteResult.copyOf(message.routeLatLongs);
                this.via = message.via;
                this.address = message.address;
            }
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder routeId(java.lang.String routeId2) {
            this.routeId = routeId2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder label(java.lang.String label2) {
            this.label = label2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder geoPoints_OBSOLETE(java.util.List<com.navdy.service.library.events.location.Coordinate> geoPoints_OBSOLETE2) {
            this.geoPoints_OBSOLETE = checkForNulls(geoPoints_OBSOLETE2);
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder length(java.lang.Integer length2) {
            this.length = length2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder duration(java.lang.Integer duration2) {
            this.duration = duration2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder duration_traffic(java.lang.Integer duration_traffic2) {
            this.duration_traffic = duration_traffic2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder routeLatLongs(java.util.List<java.lang.Float> routeLatLongs2) {
            this.routeLatLongs = checkForNulls(routeLatLongs2);
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder via(java.lang.String via2) {
            this.via = via2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult.Builder address(java.lang.String address2) {
            this.address = address2;
            return this;
        }

        public com.navdy.service.library.events.navigation.NavigationRouteResult build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.navigation.NavigationRouteResult(this);
        }
    }

    public NavigationRouteResult(java.lang.String routeId2, java.lang.String label2, java.util.List<com.navdy.service.library.events.location.Coordinate> geoPoints_OBSOLETE2, java.lang.Integer length2, java.lang.Integer duration2, java.lang.Integer duration_traffic2, java.util.List<java.lang.Float> routeLatLongs2, java.lang.String via2, java.lang.String address2) {
        this.routeId = routeId2;
        this.label = label2;
        this.geoPoints_OBSOLETE = immutableCopyOf(geoPoints_OBSOLETE2);
        this.length = length2;
        this.duration = duration2;
        this.duration_traffic = duration_traffic2;
        this.routeLatLongs = immutableCopyOf(routeLatLongs2);
        this.via = via2;
        this.address = address2;
    }

    private NavigationRouteResult(com.navdy.service.library.events.navigation.NavigationRouteResult.Builder builder) {
        this(builder.routeId, builder.label, builder.geoPoints_OBSOLETE, builder.length, builder.duration, builder.duration_traffic, builder.routeLatLongs, builder.via, builder.address);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.navigation.NavigationRouteResult)) {
            return false;
        }
        com.navdy.service.library.events.navigation.NavigationRouteResult o = (com.navdy.service.library.events.navigation.NavigationRouteResult) other;
        if (!equals((java.lang.Object) this.routeId, (java.lang.Object) o.routeId) || !equals((java.lang.Object) this.label, (java.lang.Object) o.label) || !equals(this.geoPoints_OBSOLETE, o.geoPoints_OBSOLETE) || !equals((java.lang.Object) this.length, (java.lang.Object) o.length) || !equals((java.lang.Object) this.duration, (java.lang.Object) o.duration) || !equals((java.lang.Object) this.duration_traffic, (java.lang.Object) o.duration_traffic) || !equals(this.routeLatLongs, o.routeLatLongs) || !equals((java.lang.Object) this.via, (java.lang.Object) o.via) || !equals((java.lang.Object) this.address, (java.lang.Object) o.address)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 1;
        int i8 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.routeId != null ? this.routeId.hashCode() : 0) * 37;
        if (this.label != null) {
            i = this.label.hashCode();
        } else {
            i = 0;
        }
        int i9 = (hashCode + i) * 37;
        if (this.geoPoints_OBSOLETE != null) {
            i2 = this.geoPoints_OBSOLETE.hashCode();
        } else {
            i2 = 1;
        }
        int i10 = (i9 + i2) * 37;
        if (this.length != null) {
            i3 = this.length.hashCode();
        } else {
            i3 = 0;
        }
        int i11 = (i10 + i3) * 37;
        if (this.duration != null) {
            i4 = this.duration.hashCode();
        } else {
            i4 = 0;
        }
        int i12 = (i11 + i4) * 37;
        if (this.duration_traffic != null) {
            i5 = this.duration_traffic.hashCode();
        } else {
            i5 = 0;
        }
        int i13 = (i12 + i5) * 37;
        if (this.routeLatLongs != null) {
            i7 = this.routeLatLongs.hashCode();
        }
        int i14 = (i13 + i7) * 37;
        if (this.via != null) {
            i6 = this.via.hashCode();
        } else {
            i6 = 0;
        }
        int i15 = (i14 + i6) * 37;
        if (this.address != null) {
            i8 = this.address.hashCode();
        }
        int result2 = i15 + i8;
        this.hashCode = result2;
        return result2;
    }
}
