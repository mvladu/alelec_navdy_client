package com.navdy.service.library.events.settings;

public final class DateTimeConfiguration extends com.squareup.wire.Message {
    public static final com.navdy.service.library.events.settings.DateTimeConfiguration.Clock DEFAULT_FORMAT = com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_24_HOUR;
    public static final java.lang.Long DEFAULT_TIMESTAMP = java.lang.Long.valueOf(0);
    public static final java.lang.String DEFAULT_TIMEZONE = "";
    private static final long serialVersionUID = 0;
    @com.squareup.wire.ProtoField(tag = 3, type = com.squareup.wire.Message.Datatype.ENUM)
    public final com.navdy.service.library.events.settings.DateTimeConfiguration.Clock format;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.INT64)
    public final java.lang.Long timestamp;
    @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 2, type = com.squareup.wire.Message.Datatype.STRING)
    public final java.lang.String timezone;

    public static final class Builder extends com.squareup.wire.Message.Builder<com.navdy.service.library.events.settings.DateTimeConfiguration> {
        public com.navdy.service.library.events.settings.DateTimeConfiguration.Clock format;
        public java.lang.Long timestamp;
        public java.lang.String timezone;

        public Builder() {
        }

        public Builder(com.navdy.service.library.events.settings.DateTimeConfiguration message) {
            super(message);
            if (message != null) {
                this.timestamp = message.timestamp;
                this.timezone = message.timezone;
                this.format = message.format;
            }
        }

        public com.navdy.service.library.events.settings.DateTimeConfiguration.Builder timestamp(java.lang.Long timestamp2) {
            this.timestamp = timestamp2;
            return this;
        }

        public com.navdy.service.library.events.settings.DateTimeConfiguration.Builder timezone(java.lang.String timezone2) {
            this.timezone = timezone2;
            return this;
        }

        public com.navdy.service.library.events.settings.DateTimeConfiguration.Builder format(com.navdy.service.library.events.settings.DateTimeConfiguration.Clock format2) {
            this.format = format2;
            return this;
        }

        public com.navdy.service.library.events.settings.DateTimeConfiguration build() {
            checkRequiredFields();
            return new com.navdy.service.library.events.settings.DateTimeConfiguration(this);
        }
    }

    public enum Clock implements com.squareup.wire.ProtoEnum {
        CLOCK_24_HOUR(1),
        CLOCK_12_HOUR(2);
        
        private final int value;

        private Clock(int value2) {
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DateTimeConfiguration(java.lang.Long timestamp2, java.lang.String timezone2, com.navdy.service.library.events.settings.DateTimeConfiguration.Clock format2) {
        this.timestamp = timestamp2;
        this.timezone = timezone2;
        this.format = format2;
    }

    private DateTimeConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration.Builder builder) {
        this(builder.timestamp, builder.timezone, builder.format);
        setBuilder(builder);
    }

    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof com.navdy.service.library.events.settings.DateTimeConfiguration)) {
            return false;
        }
        com.navdy.service.library.events.settings.DateTimeConfiguration o = (com.navdy.service.library.events.settings.DateTimeConfiguration) other;
        if (!equals((java.lang.Object) this.timestamp, (java.lang.Object) o.timestamp) || !equals((java.lang.Object) this.timezone, (java.lang.Object) o.timezone) || !equals((java.lang.Object) this.format, (java.lang.Object) o.format)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.timestamp != null ? this.timestamp.hashCode() : 0) * 37;
        if (this.timezone != null) {
            i = this.timezone.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 37;
        if (this.format != null) {
            i2 = this.format.hashCode();
        }
        int result2 = i3 + i2;
        this.hashCode = result2;
        return result2;
    }
}
