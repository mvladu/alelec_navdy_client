package com.navdy.service.library.events.audio;

public enum MusicShuffleMode implements com.squareup.wire.ProtoEnum {
    MUSIC_SHUFFLE_MODE_UNKNOWN(1),
    MUSIC_SHUFFLE_MODE_OFF(2),
    MUSIC_SHUFFLE_MODE_SONGS(3),
    MUSIC_SHUFFLE_MODE_ALBUMS(4);
    
    private final int value;

    private MusicShuffleMode(int value2) {
        this.value = value2;
    }

    public int getValue() {
        return this.value;
    }
}
