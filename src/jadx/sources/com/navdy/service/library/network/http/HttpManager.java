package com.navdy.service.library.network.http;

public class HttpManager implements com.navdy.service.library.network.http.IHttpManager {
    protected okhttp3.OkHttpClient mHttpClient;

    public HttpManager() {
        initHttpClient();
    }

    private void initHttpClient() {
        this.mHttpClient = new okhttp3.OkHttpClient.Builder().retryOnConnectionFailure(false).build();
    }

    public okhttp3.OkHttpClient getClient() {
        return this.mHttpClient;
    }

    public okhttp3.OkHttpClient.Builder getClientCopy() {
        return this.mHttpClient.newBuilder();
    }
}
