package com.navdy.service.library.network;

public interface SocketFactory {
    com.navdy.service.library.network.SocketAdapter build() throws java.io.IOException;
}
