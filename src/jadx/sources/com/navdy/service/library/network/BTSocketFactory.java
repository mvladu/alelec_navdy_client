package com.navdy.service.library.network;

public class BTSocketFactory implements com.navdy.service.library.network.SocketFactory {
    private final java.lang.String address;
    private final boolean secure;
    private final java.util.UUID serviceUUID;

    public BTSocketFactory(java.lang.String address2, java.util.UUID serviceUUID2) {
        this(address2, serviceUUID2, true);
    }

    public BTSocketFactory(java.lang.String address2, java.util.UUID serviceUUID2, boolean secure2) {
        this.address = address2;
        this.serviceUUID = serviceUUID2;
        this.secure = secure2;
    }

    public BTSocketFactory(com.navdy.service.library.device.connection.ServiceAddress address2) {
        this(address2.getAddress(), java.util.UUID.fromString(address2.getService()));
    }

    public com.navdy.service.library.network.SocketAdapter build() throws java.io.IOException {
        android.bluetooth.BluetoothDevice device = android.bluetooth.BluetoothAdapter.getDefaultAdapter().getRemoteDevice(this.address);
        if (this.secure) {
            return new com.navdy.service.library.network.BTSocketAdapter(device.createRfcommSocketToServiceRecord(this.serviceUUID));
        }
        return new com.navdy.service.library.network.BTSocketAdapter(device.createInsecureRfcommSocketToServiceRecord(this.serviceUUID));
    }
}
