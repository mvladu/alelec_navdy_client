package com.navdy.service.library.network;

public class BTSocketAdapter implements com.navdy.service.library.network.SocketAdapter {
    private static final boolean needsPatch = (android.os.Build.VERSION.SDK_INT >= 17 && android.os.Build.VERSION.SDK_INT <= 20);
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.BTSocketAdapter.class);
    private boolean closed = false;
    private int mFd = -1;
    private android.os.ParcelFileDescriptor mPfd;
    final android.bluetooth.BluetoothSocket socket;

    public BTSocketAdapter(android.bluetooth.BluetoothSocket socket2) {
        if (socket2 == null) {
            throw new java.lang.IllegalArgumentException("Socket must not be null");
        }
        this.socket = socket2;
        if (needsPatch) {
            storeFd();
        }
    }

    public void connect() throws java.io.IOException {
        try {
            this.socket.connect();
            if (needsPatch) {
                storeFd();
            }
        } catch (java.io.IOException e) {
            if (needsPatch) {
                storeFd();
            }
            throw e;
        }
    }

    public void close() throws java.io.IOException {
        synchronized (this.socket) {
            if (!this.closed) {
                if (needsPatch) {
                    storeFd();
                }
                this.socket.close();
                if (needsPatch) {
                    cleanupFd();
                }
                this.closed = true;
            }
        }
    }

    public java.io.InputStream getInputStream() throws java.io.IOException {
        return this.socket.getInputStream();
    }

    public java.io.OutputStream getOutputStream() throws java.io.IOException {
        return this.socket.getOutputStream();
    }

    public boolean isConnected() {
        return this.socket.isConnected();
    }

    public com.navdy.service.library.device.NavdyDeviceId getRemoteDevice() {
        android.bluetooth.BluetoothDevice device = this.socket.getRemoteDevice();
        if (device != null) {
            return new com.navdy.service.library.device.NavdyDeviceId(device);
        }
        return null;
    }

    private synchronized void storeFd() {
        if (this.mPfd == null || this.mFd == -1) {
            try {
                java.lang.reflect.Field field = this.socket.getClass().getDeclaredField("mPfd");
                field.setAccessible(true);
                android.os.ParcelFileDescriptor pfd = (android.os.ParcelFileDescriptor) field.get(this.socket);
                if (pfd != null) {
                    this.mPfd = pfd;
                }
                int fd = com.navdy.service.library.util.IOUtils.getSocketFD(this.socket);
                if (fd != -1) {
                    this.mFd = fd;
                }
                sLogger.d("Stored " + this.mPfd + " fd:" + this.mFd);
            } catch (java.lang.Exception e) {
                sLogger.w("Exception storing socket internals", e);
            }
        }
        return;
    }

    private synchronized void cleanupFd() throws java.io.IOException {
        if (this.mPfd != null) {
            sLogger.d("Closing mPfd");
            this.mPfd.close();
            this.mPfd = null;
        }
        if (this.mFd != -1) {
            sLogger.d("Closing mFd:" + this.mFd);
            com.navdy.service.library.util.IOUtils.closeFD(this.mFd);
            this.mFd = -1;
        }
    }
}
