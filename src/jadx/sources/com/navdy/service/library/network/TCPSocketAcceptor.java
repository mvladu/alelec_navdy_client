package com.navdy.service.library.network;

public class TCPSocketAcceptor implements com.navdy.service.library.network.SocketAcceptor {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.TCPSocketAcceptor.class);
    private final boolean local;
    private final int port;
    private java.net.ServerSocket serverSocket;

    public TCPSocketAcceptor(int port2) {
        this(port2, false);
    }

    public TCPSocketAcceptor(int port2, boolean bindToLocalHost) {
        this.local = bindToLocalHost;
        this.port = port2;
    }

    public com.navdy.service.library.network.SocketAdapter accept() throws java.io.IOException {
        if (this.serverSocket == null) {
            if (this.local) {
                this.serverSocket = new java.net.ServerSocket(this.port, 10, java.net.InetAddress.getByAddress(new byte[]{Byte.MAX_VALUE, 0, 0, 1}));
            } else {
                this.serverSocket = new java.net.ServerSocket(this.port, 10);
            }
            sLogger.d("Socket bound to " + (this.local ? "localhost" : "all interfaces") + ", port " + this.port);
        }
        return new com.navdy.service.library.network.TCPSocketAdapter(this.serverSocket.accept());
    }

    public void close() throws java.io.IOException {
        if (this.serverSocket != null) {
            this.serverSocket.close();
            this.serverSocket = null;
        }
    }

    public com.navdy.service.library.device.connection.ConnectionInfo getRemoteConnectionInfo(com.navdy.service.library.network.SocketAdapter socket, com.navdy.service.library.device.connection.ConnectionType connectionType) {
        if (socket instanceof com.navdy.service.library.network.TCPSocketAdapter) {
            com.navdy.service.library.network.TCPSocketAdapter adapter = (com.navdy.service.library.network.TCPSocketAdapter) socket;
            com.navdy.service.library.device.NavdyDeviceId deviceId = socket.getRemoteDevice();
            if (deviceId != null) {
                return new com.navdy.service.library.device.connection.TCPConnectionInfo(deviceId, adapter.getRemoteAddress());
            }
        }
        return null;
    }
}
