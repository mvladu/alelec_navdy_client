package com.navdy.service.library.network;

public class TCPSocketFactory implements com.navdy.service.library.network.SocketFactory {
    private final java.lang.String host;
    private final int port;

    public TCPSocketFactory(java.lang.String host2, int port2) {
        this.host = host2;
        this.port = port2;
    }

    public TCPSocketFactory(com.navdy.service.library.device.connection.ServiceAddress address) {
        this(address.getAddress(), java.lang.Integer.parseInt(address.getService()));
    }

    public com.navdy.service.library.network.SocketAdapter build() throws java.io.IOException {
        return new com.navdy.service.library.network.TCPSocketAdapter(this.host, this.port);
    }
}
