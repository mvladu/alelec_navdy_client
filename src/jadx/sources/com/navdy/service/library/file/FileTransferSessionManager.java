package com.navdy.service.library.file;

public class FileTransferSessionManager implements com.navdy.service.library.file.IFileTransferManager {
    private static final int MAX_SESSIONS = 5;
    private static final int SESSION_TIMEOUT_INTERVAL = 60000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.file.FileTransferSessionManager.class);
    private android.content.Context mContext;
    private com.navdy.service.library.file.IFileTransferAuthority mFileTransferAuthority;
    private java.util.HashMap<java.lang.Integer, com.navdy.service.library.file.FileTransferSession> mFileTransferSessionsIdIndexed;
    private java.util.HashMap<java.lang.String, com.navdy.service.library.file.FileTransferSession> mFileTransferSessionsPathIndexed;
    private java.util.concurrent.atomic.AtomicInteger mTransferId = new java.util.concurrent.atomic.AtomicInteger(0);

    public FileTransferSessionManager(android.content.Context context, com.navdy.service.library.file.IFileTransferAuthority authority) {
        this.mContext = context;
        this.mFileTransferAuthority = authority;
        this.mFileTransferSessionsPathIndexed = new java.util.HashMap<>();
        this.mFileTransferSessionsIdIndexed = new java.util.HashMap<>();
    }

    public com.navdy.service.library.events.file.FileTransferResponse handleFileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest request) {
        if (request == null) {
            return null;
        }
        try {
            if (!this.mFileTransferAuthority.isFileTypeAllowed(request.fileType)) {
                return new com.navdy.service.library.events.file.FileTransferResponse.Builder().success(java.lang.Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).build();
            }
            if (request.fileType == com.navdy.service.library.events.file.FileType.FILE_TYPE_PERF_TEST) {
                com.navdy.service.library.file.FileTransferSession session = new com.navdy.service.library.file.FileTransferSession(this.mTransferId.incrementAndGet());
                com.navdy.service.library.events.file.FileTransferResponse response = session.initTestData(this.mContext, request.fileType, com.navdy.service.library.file.TransferDataSource.TEST_DATA_NAME, request.fileSize.longValue());
                if (!response.success.booleanValue()) {
                    return response;
                }
                this.mFileTransferSessionsIdIndexed.put(java.lang.Integer.valueOf(session.mTransferId), session);
                return response;
            } else if (!isPullRequest(request.fileType)) {
                java.lang.String destinationFolder = this.mFileTransferAuthority.getDirectoryForFileType(request.fileType);
                java.lang.String absolutePath = absolutePath(this.mContext, destinationFolder, request.destinationFileName);
                com.navdy.service.library.file.FileTransferSession session2 = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsPathIndexed.get(absolutePath);
                if (session2 == null && this.mFileTransferSessionsPathIndexed.size() == 5 && endSessions(true) == 0) {
                    return new com.navdy.service.library.events.file.FileTransferResponse.Builder().success(java.lang.Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build();
                }
                endFileTransferSession(session2, false);
                com.navdy.service.library.file.FileTransferSession session3 = new com.navdy.service.library.file.FileTransferSession(this.mTransferId.incrementAndGet());
                com.navdy.service.library.events.file.FileTransferResponse response2 = session3.initFileTransfer(this.mContext, request.fileType, destinationFolder, request.destinationFileName, request.fileSize.longValue(), request.offset.longValue(), request.fileDataChecksum, java.lang.Boolean.TRUE.equals(request.override));
                if (!response2.success.booleanValue()) {
                    return response2;
                }
                this.mFileTransferSessionsIdIndexed.put(java.lang.Integer.valueOf(session3.mTransferId), session3);
                this.mFileTransferSessionsPathIndexed.put(absolutePath, session3);
                return response2;
            } else {
                com.navdy.service.library.file.FileTransferSession session4 = new com.navdy.service.library.file.FileTransferSession(this.mTransferId.incrementAndGet());
                java.lang.String tempLogFolder = this.mFileTransferAuthority.getDirectoryForFileType(request.fileType);
                java.lang.String absoluteFilePath = this.mFileTransferAuthority.getFileToSend(request.fileType);
                if (android.text.TextUtils.isEmpty(absoluteFilePath)) {
                    return new com.navdy.service.library.events.file.FileTransferResponse.Builder().success(java.lang.Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                }
                java.io.File fileToSend = new java.io.File(absoluteFilePath);
                if (!fileToSend.exists()) {
                    return new com.navdy.service.library.events.file.FileTransferResponse.Builder().success(java.lang.Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                }
                com.navdy.service.library.events.file.FileTransferResponse response3 = session4.initPull(this.mContext, request.fileType, tempLogFolder, fileToSend.getName(), ((java.lang.Boolean) com.squareup.wire.Wire.get(request.supportsAcks, com.navdy.service.library.events.file.FileTransferRequest.DEFAULT_SUPPORTSACKS)).booleanValue());
                if (!response3.success.booleanValue()) {
                    return response3;
                }
                this.mFileTransferSessionsIdIndexed.put(java.lang.Integer.valueOf(session4.mTransferId), session4);
                return response3;
            }
        } catch (Throwable t) {
            sLogger.e("Exception in handle file request ", t);
            return new com.navdy.service.library.events.file.FileTransferResponse.Builder().success(java.lang.Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
        }
    }

    private int endSessions(boolean onlyInactiveSession) {
        long currentTimeStamp = java.lang.System.currentTimeMillis();
        int removed = 0;
        java.util.Iterator<java.lang.Integer> keysIterator = this.mFileTransferSessionsIdIndexed.keySet().iterator();
        while (keysIterator.hasNext()) {
            com.navdy.service.library.file.FileTransferSession session = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsIdIndexed.get(keysIterator.next());
            if (!onlyInactiveSession || (session != null && currentTimeStamp - session.getLastActivity() > com.here.odnp.util.OdnpConstants.ONE_MINUTE_IN_MS)) {
                keysIterator.remove();
                endFileTransferSession(session, false);
                removed++;
            }
        }
        return removed;
    }

    public void stop() {
        endSessions(false);
    }

    public boolean handleFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus) {
        if (fileTransferStatus == null) {
            return false;
        }
        com.navdy.service.library.file.FileTransferSession session = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsIdIndexed.get(fileTransferStatus.transferId);
        if (session != null) {
            return session.handleFileTransferStatus(fileTransferStatus);
        }
        return false;
    }

    public com.navdy.service.library.events.file.FileTransferStatus handleFileTransferData(com.navdy.service.library.events.file.FileTransferData data) {
        com.navdy.service.library.file.FileTransferSession session = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsIdIndexed.get(data.transferId);
        if (session == null) {
            return new com.navdy.service.library.events.file.FileTransferStatus.Builder().success(java.lang.Boolean.valueOf(false)).transferComplete(java.lang.Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
        }
        com.navdy.service.library.events.file.FileTransferStatus status = session.appendChunk(this.mContext, data);
        if (!status.transferComplete.booleanValue() && (status.error == null || status.error == com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NO_ERROR)) {
            return status;
        }
        endFileTransferSession(session, false);
        return status;
    }

    public com.navdy.service.library.events.file.FileTransferData getNextChunk(int transferId) throws java.lang.Throwable {
        com.navdy.service.library.file.FileTransferSession session = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsIdIndexed.get(java.lang.Integer.valueOf(transferId));
        if (session == null) {
            return null;
        }
        com.navdy.service.library.events.file.FileTransferData data = session.getNextChunk();
        if (data != null && (!data.lastChunk.booleanValue() || session.isFlowControlEnabled())) {
            return data;
        }
        endFileTransferSession(session, true);
        return data;
    }

    public com.navdy.service.library.events.file.FileType getFileType(int transferId) {
        com.navdy.service.library.file.FileTransferSession session = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsIdIndexed.get(java.lang.Integer.valueOf(transferId));
        if (session != null) {
            return session.getFileType();
        }
        return null;
    }

    private void endFileTransferSession(com.navdy.service.library.file.FileTransferSession session, boolean deleteFile) {
        java.lang.String fileName;
        java.lang.String destFolder;
        if (session != null) {
            synchronized (session) {
                fileName = session.mFileName;
                destFolder = session.mDestinationFolder;
            }
            this.mFileTransferSessionsPathIndexed.remove(absolutePath(this.mContext, destFolder, fileName));
            this.mFileTransferSessionsIdIndexed.remove(java.lang.Integer.valueOf(session.mTransferId));
            session.endSession(this.mContext, deleteFile);
        }
    }

    public void endFileTransferSession(int transferId, boolean deleteFile) {
        com.navdy.service.library.file.FileTransferSession session = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsIdIndexed.get(java.lang.Integer.valueOf(transferId));
        if (session != null) {
            endFileTransferSession(session, deleteFile);
        }
    }

    public java.lang.String absolutePathForTransferId(int transferId) {
        java.lang.String fileName;
        java.lang.String destFolder;
        com.navdy.service.library.file.FileTransferSession transferSession = (com.navdy.service.library.file.FileTransferSession) this.mFileTransferSessionsIdIndexed.get(java.lang.Integer.valueOf(transferId));
        if (transferSession == null) {
            return null;
        }
        synchronized (transferSession) {
            fileName = transferSession.mFileName;
            destFolder = transferSession.mDestinationFolder;
        }
        return absolutePath(this.mContext, destFolder, fileName);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.lang.String, code=java.lang.Object, for r3v0, types: [java.lang.Object, java.lang.String] */
    public static final java.lang.String absolutePath(android.content.Context context, java.lang.Object destinationFolder, java.lang.String destinationFileName) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        if (destinationFolder == null) {
            destinationFolder = context.getFilesDir();
        }
        return sb.append(destinationFolder).append(java.io.File.separator).append(destinationFileName).toString();
    }

    public static com.navdy.service.library.events.file.FileTransferData prepareFileTransferData(int transferId, com.navdy.service.library.file.TransferDataSource source, int chunkIndex, int chunkSize, long startOffset, long alreadyTransferred) throws java.lang.Throwable {
        try {
            long remainingBytes = source.length() - alreadyTransferred;
            int nextChunkSize = chunkSize;
            boolean lastChunk = false;
            if (remainingBytes < ((long) chunkSize)) {
                nextChunkSize = (int) remainingBytes;
                lastChunk = true;
            }
            byte[] chunk = new byte[nextChunkSize];
            source.seek(alreadyTransferred);
            source.read(chunk);
            com.navdy.service.library.events.file.FileTransferData.Builder fileTransferDataBuilder = new com.navdy.service.library.events.file.FileTransferData.Builder().transferId(java.lang.Integer.valueOf(transferId)).chunkIndex(java.lang.Integer.valueOf(chunkIndex));
            if (lastChunk) {
                fileTransferDataBuilder.fileCheckSum(source.checkSum());
            }
            return fileTransferDataBuilder.dataBytes(okio.ByteString.of(chunk)).lastChunk(java.lang.Boolean.valueOf(lastChunk)).build();
        } catch (Throwable t) {
            sLogger.e("Exception while preparing the chunk data " + t);
            throw t;
        }
    }

    public static boolean isPullRequest(com.navdy.service.library.events.file.FileType fileType) {
        switch (fileType) {
            case FILE_TYPE_LOGS:
                return true;
            default:
                return false;
        }
    }
}
