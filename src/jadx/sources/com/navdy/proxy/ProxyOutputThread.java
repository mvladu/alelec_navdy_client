package com.navdy.proxy;

public class ProxyOutputThread extends java.lang.Thread {
    private static final boolean VERBOSE_DBG = false;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.proxy.ProxyOutputThread.class);
    public android.os.Handler mHandler;
    java.io.OutputStream mOutputStream;
    com.navdy.proxy.ProxyThread mProxyThread;

    class Anon1 extends android.os.Handler {
        Anon1() {
        }

        public void handleMessage(android.os.Message msg) {
            try {
                com.navdy.proxy.ProxyOutputThread.this.mOutputStream.write((byte[]) msg.obj);
            } catch (java.io.IOException e) {
                com.navdy.proxy.ProxyOutputThread.sLogger.e("error writing to output", e);
                android.os.Looper.myLooper().quit();
            }
        }
    }

    public ProxyOutputThread(com.navdy.proxy.ProxyThread proxyThread, java.io.OutputStream outputStream) {
        setName(com.navdy.proxy.ProxyOutputThread.class.getSimpleName());
        this.mOutputStream = outputStream;
        this.mProxyThread = proxyThread;
    }

    public synchronized android.os.Handler getHandler() {
        while (this.mHandler == null) {
            try {
                wait();
            } catch (java.lang.InterruptedException e) {
                if (this.mHandler != null) {
                    break;
                }
            }
        }
        return this.mHandler;
    }

    public void run() {
        android.os.Looper.prepare();
        synchronized (this) {
            this.mHandler = new com.navdy.proxy.ProxyOutputThread.Anon1();
            notifyAll();
        }
        sLogger.i("entering loop");
        android.os.Looper.loop();
        sLogger.i("loop exited");
        this.mProxyThread.outputThreadWillFinish();
    }
}
