package com.navdy.proxy;

public class ProxyThread extends java.lang.Thread {
    private static final boolean VERBOSE_DBG = false;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.proxy.ProxyThread.class);
    java.io.InputStream mInputStream;
    com.navdy.proxy.ProxyInputThread mInputThread;
    android.os.Handler mOutputHandler;
    java.io.OutputStream mOutputStream;
    com.navdy.proxy.ProxyOutputThread mOutputThread;
    com.navdy.service.library.network.SocketAdapter mSocket;
    private long nativeStorage;

    private native void exitEventLoop();

    private native void receiveBytesNative(byte[] bArr, int i);

    private native void setupEventLoop();

    private native int startEventLoop();

    static {
        java.lang.System.loadLibrary("proxy");
    }

    public ProxyThread(com.navdy.service.library.network.SocketAdapter socket) throws java.io.IOException {
        setName(com.navdy.proxy.ProxyThread.class.getSimpleName());
        this.mSocket = socket;
        this.mInputStream = socket.getInputStream();
        this.mOutputStream = socket.getOutputStream();
        sLogger.i("initialized with input " + this.mInputStream + " and output " + this.mOutputStream);
    }

    public void run() {
        super.run();
        setupEventLoop();
        this.mInputThread = new com.navdy.proxy.ProxyInputThread(this, this.mInputStream);
        this.mOutputThread = new com.navdy.proxy.ProxyOutputThread(this, this.mOutputStream);
        this.mInputThread.start();
        this.mOutputThread.start();
        this.mOutputHandler = this.mOutputThread.getHandler();
        startEventLoop();
        com.navdy.service.library.util.IOUtils.closeStream(this.mInputStream);
        com.navdy.service.library.util.IOUtils.closeStream(this.mOutputStream);
        com.navdy.service.library.util.IOUtils.closeStream(this.mSocket);
    }

    public void receiveBytes(byte[] buffer, int bytesRead) {
        receiveBytesNative(buffer, bytesRead);
    }

    private void sendOutput(byte[] buffer) {
        android.os.Message.obtain(this.mOutputHandler, 0, buffer).sendToTarget();
    }

    public void inputThreadWillFinish() {
        exitEventLoop();
    }

    public void outputThreadWillFinish() {
        exitEventLoop();
    }
}
