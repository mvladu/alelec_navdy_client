package com.navdy.hud.app;

public interface IEventSource extends android.os.IInterface {

    public static abstract class Stub extends android.os.Binder implements com.navdy.hud.app.IEventSource {
        private static final java.lang.String DESCRIPTOR = "com.navdy.hud.app.IEventSource";
        static final int TRANSACTION_addEventListener = 1;
        static final int TRANSACTION_postEvent = 3;
        static final int TRANSACTION_postRemoteEvent = 4;
        static final int TRANSACTION_removeEventListener = 2;

        private static class Proxy implements com.navdy.hud.app.IEventSource {
            private android.os.IBinder mRemote;

            Proxy(android.os.IBinder remote) {
                this.mRemote = remote;
            }

            public android.os.IBinder asBinder() {
                return this.mRemote;
            }

            public java.lang.String getInterfaceDescriptor() {
                return com.navdy.hud.app.IEventSource.Stub.DESCRIPTOR;
            }

            public void addEventListener(com.navdy.hud.app.IEventListener listener) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.IEventSource.Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void removeEventListener(com.navdy.hud.app.IEventListener listener) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                android.os.Parcel _reply = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.IEventSource.Stub.DESCRIPTOR);
                    _data.writeStrongBinder(listener != null ? listener.asBinder() : null);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public void postEvent(byte[] bytes) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.IEventSource.Stub.DESCRIPTOR);
                    _data.writeByteArray(bytes);
                    this.mRemote.transact(3, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }

            public void postRemoteEvent(java.lang.String remoteDeviceId, byte[] bytes) throws android.os.RemoteException {
                android.os.Parcel _data = android.os.Parcel.obtain();
                try {
                    _data.writeInterfaceToken(com.navdy.hud.app.IEventSource.Stub.DESCRIPTOR);
                    _data.writeString(remoteDeviceId);
                    _data.writeByteArray(bytes);
                    this.mRemote.transact(4, _data, null, 1);
                } finally {
                    _data.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static com.navdy.hud.app.IEventSource asInterface(android.os.IBinder obj) {
            if (obj == null) {
                return null;
            }
            android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
            if (iin == null || !(iin instanceof com.navdy.hud.app.IEventSource)) {
                return new com.navdy.hud.app.IEventSource.Stub.Proxy(obj);
            }
            return (com.navdy.hud.app.IEventSource) iin;
        }

        public android.os.IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException {
            switch (code) {
                case 1:
                    data.enforceInterface(DESCRIPTOR);
                    addEventListener(com.navdy.hud.app.IEventListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface(DESCRIPTOR);
                    removeEventListener(com.navdy.hud.app.IEventListener.Stub.asInterface(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface(DESCRIPTOR);
                    postEvent(data.createByteArray());
                    return true;
                case 4:
                    data.enforceInterface(DESCRIPTOR);
                    postRemoteEvent(data.readString(), data.createByteArray());
                    return true;
                case 1598968902:
                    reply.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void addEventListener(com.navdy.hud.app.IEventListener iEventListener) throws android.os.RemoteException;

    void postEvent(byte[] bArr) throws android.os.RemoteException;

    void postRemoteEvent(java.lang.String str, byte[] bArr) throws android.os.RemoteException;

    void removeEventListener(com.navdy.hud.app.IEventListener iEventListener) throws android.os.RemoteException;
}
