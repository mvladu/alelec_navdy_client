package com.navdy.client.ota;

public interface OTAUpdateUIClient {

    public enum Error {
        NO_CONNECTIVITY,
        SERVER_ERROR
    }

    boolean isInForeground();

    void onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus downloadUpdateStatus, long j, byte b);

    void onErrorCheckingForUpdate(com.navdy.client.ota.OTAUpdateUIClient.Error error);

    void onStateChanged(com.navdy.client.ota.OTAUpdateService.State state, com.navdy.client.ota.model.UpdateInfo updateInfo);

    void onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus uploadToHUDStatus, long j, byte b);
}
