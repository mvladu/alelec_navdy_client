package com.navdy.client.ota.model;

public class UpdateInfo {
    private static final java.lang.String DEFAULT_RELEASE_NOTES_LANGUAGE_TAG = "en";
    private static final java.lang.String META_DATA_RELEASE_NOTES_TAG = "release_notes";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.ota.model.UpdateInfo.class);
    public java.lang.String description;
    public int fromVersion;
    public boolean incremental;
    public java.lang.String metaData;
    public long size;
    public java.lang.String url;
    public int version;
    public java.lang.String versionName;

    public boolean equals(java.lang.Object o) {
        if (!(o instanceof com.navdy.client.ota.model.UpdateInfo)) {
            return super.equals(o);
        }
        com.navdy.client.ota.model.UpdateInfo other = (com.navdy.client.ota.model.UpdateInfo) o;
        return other.version == this.version && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(other.versionName, this.versionName) && other.incremental == this.incremental && other.fromVersion == this.fromVersion && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(other.url, this.url);
    }

    public int hashCode() {
        return 0;
    }

    public static java.lang.String extractReleaseNotes(com.navdy.client.ota.model.UpdateInfo updateInfo) {
        sLogger.d("Parsing meta data to get release notes");
        if (updateInfo == null) {
            sLogger.d("updateInfo is null");
            return null;
        }
        java.lang.String metaData2 = updateInfo.metaData;
        if (metaData2 != null) {
            sLogger.d("Meta data node present");
            try {
                org.json.JSONObject releaseNotes = new org.json.JSONObject(metaData2).getJSONObject("release_notes");
                if (releaseNotes != null) {
                    return releaseNotes.getString(DEFAULT_RELEASE_NOTES_LANGUAGE_TAG);
                }
                sLogger.d("Release notes node not found");
            } catch (org.json.JSONException e) {
                sLogger.e("Error parsing the JSON metadata ", e);
            }
        } else {
            sLogger.d("Meta data node not present");
        }
        return null;
    }

    public static java.lang.String getFormattedVersionName(com.navdy.client.ota.model.UpdateInfo updateInfo) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(updateInfo.versionName)) {
            return updateInfo.versionName.split("-")[0];
        }
        return java.lang.String.valueOf(updateInfo.version);
    }
}
