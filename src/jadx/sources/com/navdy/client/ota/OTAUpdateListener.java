package com.navdy.client.ota;

public interface OTAUpdateListener {

    public enum CheckOTAUpdateResult {
        AVAILABLE,
        UPTODATE,
        SERVER_ERROR,
        NO_CONNECTIVITY
    }

    public enum DownloadUpdateStatus {
        STARTED,
        PAUSED,
        DOWNLOADING,
        NOT_ENOUGH_SPACE,
        NO_CONNECTIVITY,
        DOWNLOAD_FAILED,
        COMPLETED
    }

    public enum UploadToHUDStatus {
        UPLOADING,
        DEVICE_NOT_CONNECTED,
        UPLOAD_FAILED,
        COMPLETED
    }

    void onCheckForUpdateFinished(com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult checkOTAUpdateResult, com.navdy.client.ota.model.UpdateInfo updateInfo);

    void onDownloadProgress(com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus downloadUpdateStatus, int i, long j, long j2);

    void onUploadProgress(com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus uploadToHUDStatus, long j, long j2, com.navdy.service.library.events.file.FileTransferError fileTransferError);
}
