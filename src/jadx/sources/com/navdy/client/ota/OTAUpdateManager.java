package com.navdy.client.ota;

public interface OTAUpdateManager {
    void abortDownload();

    void abortUpload();

    void checkForUpdate(int i, boolean z);

    void download(com.navdy.client.ota.model.UpdateInfo updateInfo, java.io.File file, int i);

    void uploadToHUD(java.io.File file, long j, java.lang.String str);
}
