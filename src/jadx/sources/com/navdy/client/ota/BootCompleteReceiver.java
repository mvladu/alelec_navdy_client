package com.navdy.client.ota;

public class BootCompleteReceiver extends android.content.BroadcastReceiver {
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        com.navdy.client.ota.OTAUpdateService.startService(context);
    }
}
