package com.navdy.client.ota;

public interface OTAUpdateServiceInterface extends android.os.IBinder {
    void cancelDownload();

    void cancelUpload();

    boolean checkForUpdate();

    void downloadOTAUpdate();

    java.lang.String getHUDBuildVersionText();

    com.navdy.client.ota.OTAUpdateService.State getOTAUpdateState();

    com.navdy.client.ota.model.UpdateInfo getUpdateInfo();

    boolean isCheckingForUpdate();

    long lastKnownUploadSize();

    void registerUIClient(com.navdy.client.ota.OTAUpdateUIClient oTAUpdateUIClient);

    void resetUpdate();

    void setNetworkDownloadApproval(boolean z);

    void toggleAutoDownload(boolean z);

    void unregisterUIClient();
}
