package com.navdy.client.ota;

public class Config {
    long mUpdateInterval;

    public Config(android.content.res.Resources res) {
        this.mUpdateInterval = (long) res.getInteger(com.navdy.client.R.integer.ota_check_update_frequency);
    }
}
