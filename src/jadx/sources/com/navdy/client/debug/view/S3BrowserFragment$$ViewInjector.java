package com.navdy.client.debug.view;

public class S3BrowserFragment$$ViewInjector {

    /* compiled from: S3BrowserFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.view.S3BrowserFragment val$target;

        Anon1(com.navdy.client.debug.view.S3BrowserFragment s3BrowserFragment) {
            this.val$target = s3BrowserFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onRefresh(p0);
        }
    }

    /* compiled from: S3BrowserFragment$$ViewInjector */
    static class Anon2 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.view.S3BrowserFragment val$target;

        Anon2(com.navdy.client.debug.view.S3BrowserFragment s3BrowserFragment) {
            this.val$target = s3BrowserFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onBack(p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.view.S3BrowserFragment target, java.lang.Object source) {
        target.mList = (android.widget.ListView) finder.findRequiredView(source, com.navdy.client.R.id.list, "field 'mList'");
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.refresh, "field 'mRefreshButton' and method 'onRefresh'");
        target.mRefreshButton = (android.widget.Button) view;
        view.setOnClickListener(new com.navdy.client.debug.view.S3BrowserFragment$$ViewInjector.Anon1(target));
        android.view.View view2 = finder.findRequiredView(source, com.navdy.client.R.id.back, "field 'mBackButton' and method 'onBack'");
        target.mBackButton = (android.widget.Button) view2;
        view2.setOnClickListener(new com.navdy.client.debug.view.S3BrowserFragment$$ViewInjector.Anon2(target));
    }

    public static void reset(com.navdy.client.debug.view.S3BrowserFragment target) {
        target.mList = null;
        target.mRefreshButton = null;
        target.mBackButton = null;
    }
}
