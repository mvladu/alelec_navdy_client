package com.navdy.client.debug.view;

public abstract class S3BrowserFragment extends android.app.Fragment {
    private static final int REPORT_ON_DOWNLOAD_STEP = 1000;
    private static final int S3_DOWNLOAD_TIMEOUT = 300000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.view.S3BrowserFragment.class);
    private com.navdy.client.debug.view.S3BrowserFragment.ObjectAdapter mAdapter;
    protected com.navdy.client.app.framework.AppInstance mAppInstance;
    @butterknife.InjectView(2131755400)
    android.widget.Button mBackButton;
    protected com.amazonaws.services.s3.AmazonS3Client mClient;
    protected android.content.Context mContext;
    @butterknife.InjectView(2131755649)
    android.widget.ListView mList;
    private java.lang.String mPrefix = "";
    @butterknife.InjectView(2131755650)
    android.widget.Button mRefreshButton;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.amazonaws.event.ProgressListener val$downloadProgressListener;
        final /* synthetic */ java.io.File val$output;
        final /* synthetic */ java.lang.String val$s3Key;
        final /* synthetic */ com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager val$transferManager;

        /* renamed from: com.navdy.client.debug.view.S3BrowserFragment$Anon1$Anon1 reason: collision with other inner class name */
        class C0077Anon1 implements com.amazonaws.event.ProgressListener {
            private long downloadedSize = 0;
            private int progressEventCounter = 0;
            final /* synthetic */ com.amazonaws.mobileconnectors.s3.transfermanager.Download val$download;
            final /* synthetic */ long val$startTime;

            C0077Anon1(long j, com.amazonaws.mobileconnectors.s3.transfermanager.Download download) {
                this.val$startTime = j;
                this.val$download = download;
            }

            public void progressChanged(com.amazonaws.event.ProgressEvent progressEvent) {
                this.downloadedSize += progressEvent.getBytesTransferred();
                int eventCode = progressEvent.getEventCode();
                if (eventCode == 4) {
                    com.navdy.client.debug.view.S3BrowserFragment.sLogger.d("S3 file downloaded");
                } else if (eventCode == 8) {
                    com.navdy.client.debug.view.S3BrowserFragment.sLogger.d("Failed to download file from S3");
                } else {
                    int i = this.progressEventCounter + 1;
                    this.progressEventCounter = i;
                    if (i > 1000) {
                        this.progressEventCounter = 0;
                        java.lang.String message = java.lang.String.format("%s downloaded", new java.lang.Object[]{com.navdy.client.debug.util.FormatUtils.readableFileSize(this.downloadedSize)});
                        com.navdy.client.debug.view.S3BrowserFragment.sLogger.d(message);
                        if (android.os.SystemClock.elapsedRealtime() - this.val$startTime > com.navdy.client.app.ui.homescreen.CalendarUtils.EVENTS_STALENESS_LIMIT) {
                            com.navdy.client.debug.view.S3BrowserFragment.this.mAppInstance.showToast("S3 download timeout raised", false);
                            try {
                                this.val$download.abort();
                            } catch (java.io.IOException e) {
                                com.navdy.client.debug.view.S3BrowserFragment.sLogger.e("Exception while aborting S3 download", e);
                            } finally {
                                com.navdy.client.debug.view.S3BrowserFragment.Anon1.this.val$transferManager.shutdownNow(false);
                            }
                        } else {
                            com.navdy.client.debug.view.S3BrowserFragment.this.mAppInstance.showToast(message, false, 0);
                        }
                    }
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.client.debug.view.S3BrowserFragment.this.mRefreshButton.setText(com.navdy.client.R.string.refresh);
                com.navdy.client.debug.view.S3BrowserFragment.this.mBackButton.setEnabled(true);
                com.navdy.client.debug.view.S3BrowserFragment.this.mRefreshButton.setEnabled(true);
                com.navdy.client.debug.view.S3BrowserFragment.this.mBackButton.invalidate();
                com.navdy.client.debug.view.S3BrowserFragment.this.mRefreshButton.invalidate();
            }
        }

        Anon1(com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager transferManager, java.lang.String str, java.io.File file, com.amazonaws.event.ProgressListener progressListener) {
            this.val$transferManager = transferManager;
            this.val$s3Key = str;
            this.val$output = file;
            this.val$downloadProgressListener = progressListener;
        }

        public void run() {
            com.amazonaws.mobileconnectors.s3.transfermanager.Download download = this.val$transferManager.download(com.navdy.client.debug.view.S3BrowserFragment.this.getS3BucketName(), this.val$s3Key, this.val$output);
            long startTime = android.os.SystemClock.elapsedRealtime();
            download.addProgressListener(this.val$downloadProgressListener);
            download.addProgressListener((com.amazonaws.event.ProgressListener) new com.navdy.client.debug.view.S3BrowserFragment.Anon1.C0077Anon1(startTime, download));
            try {
                download.waitForCompletion();
            } catch (Throwable e) {
                com.navdy.client.debug.view.S3BrowserFragment.sLogger.d("Download interrupted ", e);
                com.navdy.client.debug.view.S3BrowserFragment.this.mAppInstance.showToast("S3 download process raised exception", false);
            } finally {
                new android.os.Handler(com.navdy.client.debug.view.S3BrowserFragment.this.mContext.getMainLooper()).post(new com.navdy.client.debug.view.S3BrowserFragment.Anon1.Anon2());
            }
        }
    }

    private class ItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        private ItemClickListener() {
        }

        /* synthetic */ ItemClickListener(com.navdy.client.debug.view.S3BrowserFragment x0, com.navdy.client.debug.view.S3BrowserFragment.Anon1 x1) {
            this();
        }

        public void onItemClick(android.widget.AdapterView<?> adapterView, android.view.View view, int pos, long id) {
            com.navdy.client.debug.view.S3BrowserFragment.MyS3Object item = (com.navdy.client.debug.view.S3BrowserFragment.MyS3Object) com.navdy.client.debug.view.S3BrowserFragment.this.mAdapter.getItem(pos);
            if (item.isFolder()) {
                com.navdy.client.debug.view.S3BrowserFragment.this.mPrefix = item.getKey();
                new com.navdy.client.debug.view.S3BrowserFragment.RefreshTask(com.navdy.client.debug.view.S3BrowserFragment.this, null).execute(new java.lang.Void[0]);
                return;
            }
            com.navdy.client.debug.view.S3BrowserFragment.this.onFileSelected(item.getKey());
        }
    }

    private static class MyS3Object {
        private final boolean isFolder;
        private final java.lang.String key;
        private final long size;

        MyS3Object(java.lang.String commonPrefix) {
            this.key = commonPrefix;
            this.isFolder = true;
            this.size = 0;
        }

        MyS3Object(com.amazonaws.services.s3.model.S3ObjectSummary summary) {
            this.key = summary.getKey();
            this.size = summary.getSize();
            this.isFolder = false;
        }

        public java.lang.String getKey() {
            return this.key;
        }

        public boolean isFolder() {
            return this.isFolder;
        }

        public long getSize() {
            return this.size;
        }
    }

    private class ObjectAdapter extends android.widget.ArrayAdapter<com.navdy.client.debug.view.S3BrowserFragment.MyS3Object> {

        private class ViewHolder {
            private final android.widget.TextView key;
            private final android.widget.TextView size;

            /* synthetic */ ViewHolder(com.navdy.client.debug.view.S3BrowserFragment.ObjectAdapter x0, android.view.View x1, com.navdy.client.debug.view.S3BrowserFragment.Anon1 x2) {
                this(x1);
            }

            private ViewHolder(android.view.View view) {
                this.key = (android.widget.TextView) view.findViewById(com.navdy.client.R.id.key);
                this.size = (android.widget.TextView) view.findViewById(com.navdy.client.R.id.size);
            }
        }

        public ObjectAdapter(android.content.Context context) {
            super(context, com.navdy.client.R.layout.list_item_bucket);
        }

        public android.view.View getView(int pos, android.view.View convertView, android.view.ViewGroup parent) {
            com.navdy.client.debug.view.S3BrowserFragment.ObjectAdapter.ViewHolder holder;
            if (convertView == null) {
                convertView = android.view.LayoutInflater.from(getContext()).inflate(com.navdy.client.R.layout.list_item_bucket, null);
                holder = new com.navdy.client.debug.view.S3BrowserFragment.ObjectAdapter.ViewHolder(this, convertView, null);
                convertView.setTag(holder);
            } else {
                holder = (com.navdy.client.debug.view.S3BrowserFragment.ObjectAdapter.ViewHolder) convertView.getTag();
            }
            com.navdy.client.debug.view.S3BrowserFragment.MyS3Object object = (com.navdy.client.debug.view.S3BrowserFragment.MyS3Object) getItem(pos);
            holder.key.setText(object.getKey());
            if (object.isFolder()) {
                holder.size.setText("");
            } else {
                holder.size.setText(com.navdy.client.debug.util.FormatUtils.readableFileSize(object.getSize()));
            }
            return convertView;
        }

        public void add(com.navdy.client.debug.view.S3BrowserFragment.MyS3Object object) {
            if (!object.getKey().equals(com.navdy.client.debug.view.S3BrowserFragment.this.mPrefix)) {
                super.add(object);
            }
        }

        public void addAll(java.util.Collection<? extends com.navdy.client.debug.view.S3BrowserFragment.MyS3Object> collection) {
            for (com.navdy.client.debug.view.S3BrowserFragment.MyS3Object obj : collection) {
                if (!obj.getKey().equals(com.navdy.client.debug.view.S3BrowserFragment.this.mPrefix)) {
                    add(obj);
                }
            }
        }
    }

    private class RefreshTask extends android.os.AsyncTask<java.lang.Void, java.lang.Void, com.amazonaws.services.s3.model.ObjectListing> {
        private RefreshTask() {
        }

        /* synthetic */ RefreshTask(com.navdy.client.debug.view.S3BrowserFragment x0, com.navdy.client.debug.view.S3BrowserFragment.Anon1 x1) {
            this();
        }

        protected void onPreExecute() {
            com.navdy.client.debug.view.S3BrowserFragment.this.mRefreshButton.setEnabled(false);
            com.navdy.client.debug.view.S3BrowserFragment.this.mRefreshButton.setText(com.navdy.client.R.string.refreshing);
        }

        /* access modifiers changed from: protected */
        public com.amazonaws.services.s3.model.ObjectListing doInBackground(java.lang.Void... params) {
            try {
                return com.navdy.client.debug.view.S3BrowserFragment.this.mClient.listObjects(new com.amazonaws.services.s3.model.ListObjectsRequest(com.navdy.client.debug.view.S3BrowserFragment.this.getS3BucketName(), com.navdy.client.debug.view.S3BrowserFragment.this.mPrefix, null, com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER, java.lang.Integer.valueOf(1000)));
            } catch (com.amazonaws.AmazonClientException e) {
                com.navdy.client.debug.view.S3BrowserFragment.sLogger.e("Exception while listing the files in the S3 Bucket :" + com.navdy.client.debug.view.S3BrowserFragment.this.getS3BucketName() + ", Prefix :" + com.navdy.client.debug.view.S3BrowserFragment.this.mPrefix, e);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(com.amazonaws.services.s3.model.ObjectListing objects) {
            if (objects == null || objects.getCommonPrefixes() == null || objects.getObjectSummaries() == null) {
                com.navdy.client.debug.view.S3BrowserFragment.sLogger.e("onPostExecute objects are null !!!");
                return;
            }
            com.navdy.client.debug.view.S3BrowserFragment.this.mAdapter.clear();
            for (java.lang.String commonPrefix : objects.getCommonPrefixes()) {
                com.navdy.client.debug.view.S3BrowserFragment.this.mAdapter.add(new com.navdy.client.debug.view.S3BrowserFragment.MyS3Object(commonPrefix));
            }
            for (com.amazonaws.services.s3.model.S3ObjectSummary summary : objects.getObjectSummaries()) {
                com.navdy.client.debug.view.S3BrowserFragment.this.mAdapter.add(new com.navdy.client.debug.view.S3BrowserFragment.MyS3Object(summary));
            }
            com.navdy.client.debug.view.S3BrowserFragment.this.mRefreshButton.setEnabled(true);
            com.navdy.client.debug.view.S3BrowserFragment.this.mRefreshButton.setText(com.navdy.client.R.string.refresh);
        }
    }

    protected abstract java.lang.String getS3BucketName();

    protected abstract void onFileSelected(java.lang.String str);

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity().getApplicationContext();
        java.lang.String AWS_ACCOUNT_ID = "";
        java.lang.String AWS_SECRET = "";
        try {
            android.content.pm.ApplicationInfo applicationInfo = this.mContext.getPackageManager().getApplicationInfo(com.navdy.client.app.NavdyApplication.getAppContext().getPackageName(), 128);
            AWS_ACCOUNT_ID = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_aws_account_id));
            AWS_SECRET = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_aws_secret));
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            sLogger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (java.lang.NullPointerException e2) {
            sLogger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
        }
        this.mClient = new com.amazonaws.services.s3.AmazonS3Client((com.amazonaws.auth.AWSCredentials) new com.amazonaws.auth.BasicAWSCredentials(AWS_ACCOUNT_ID, AWS_SECRET));
        this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
    }

    @butterknife.OnClick({2131755400})
    public void onBack(android.view.View button) {
        back();
    }

    @butterknife.OnClick({2131755650})
    public void onRefresh(android.view.View v) {
        new com.navdy.client.debug.view.S3BrowserFragment.RefreshTask(this, null).execute(new java.lang.Void[0]);
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_s3_browser, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.mAdapter = new com.navdy.client.debug.view.S3BrowserFragment.ObjectAdapter(getActivity());
        this.mList.setOnItemClickListener(new com.navdy.client.debug.view.S3BrowserFragment.ItemClickListener(this, null));
        this.mList.setAdapter(this.mAdapter);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        new com.navdy.client.debug.view.S3BrowserFragment.RefreshTask(this, null).execute(new java.lang.Void[0]);
    }

    private void back() {
        if (this.mPrefix.endsWith(com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER)) {
            this.mPrefix = this.mPrefix.substring(0, this.mPrefix.length() - 1);
        }
        int parentDir = this.mPrefix.lastIndexOf(47, this.mPrefix.length() - 1);
        if (parentDir != -1) {
            this.mPrefix = this.mPrefix.substring(0, parentDir + 1);
        } else {
            this.mPrefix = "";
        }
        new com.navdy.client.debug.view.S3BrowserFragment.RefreshTask(this, null).execute(new java.lang.Void[0]);
    }

    protected void storeS3ObjectInTemp(java.lang.String s3Key, java.io.File output, com.amazonaws.event.ProgressListener downloadProgressListener) throws java.io.IOException {
        com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager transferManager = new com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager((com.amazonaws.services.s3.AmazonS3) this.mClient);
        this.mBackButton.setEnabled(false);
        this.mRefreshButton.setEnabled(false);
        this.mRefreshButton.setText(com.navdy.client.R.string.downloading);
        this.mBackButton.invalidate();
        this.mRefreshButton.invalidate();
        new java.lang.Thread(new com.navdy.client.debug.view.S3BrowserFragment.Anon1(transferManager, s3Key, output, downloadProgressListener)).start();
    }
}
