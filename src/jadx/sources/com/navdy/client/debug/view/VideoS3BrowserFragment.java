package com.navdy.client.debug.view;

public class VideoS3BrowserFragment extends com.navdy.client.debug.view.S3BrowserFragment {
    protected static final java.util.Date URL_EXPIRATION = new java.util.Date(java.lang.System.currentTimeMillis() + com.here.odnp.util.OdnpConstants.ONE_HOUR_IN_MS);
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.view.VideoS3BrowserFragment.class);

    protected java.lang.String getS3BucketName() {
        return com.navdy.client.debug.util.S3Constants.BUCKET_NAME;
    }

    protected void onFileSelected(java.lang.String fileS3Key) {
        if (!fileS3Key.endsWith(".mp4")) {
            android.widget.Toast.makeText(getActivity(), "Please select a video file", 0).show();
            return;
        }
        java.lang.String sessionPath = new java.io.File(fileS3Key).getParent() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER;
        android.content.Intent intent = new android.content.Intent(getActivity(), com.navdy.client.debug.videoplayer.VideoPlayerActivity.class);
        java.net.URL videoUrl = this.mClient.generatePresignedUrl(getS3BucketName(), fileS3Key, URL_EXPIRATION);
        sLogger.d("Generated signed url " + videoUrl);
        intent.putExtra(com.navdy.client.debug.videoplayer.VideoPlayerActivity.VIDEO_URL_EXTRA, videoUrl);
        intent.putExtra(com.navdy.client.debug.videoplayer.VideoPlayerActivity.SESSION_PATH_EXTRA, sessionPath);
        intent.putExtra(com.navdy.client.debug.videoplayer.VideoPlayerActivity.BUCKET_NAME_EXTRA, getS3BucketName());
        startActivity(intent);
    }
}
