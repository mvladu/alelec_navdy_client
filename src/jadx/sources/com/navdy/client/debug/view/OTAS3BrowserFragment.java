package com.navdy.client.debug.view;

public class OTAS3BrowserFragment extends com.navdy.client.debug.view.S3BrowserFragment {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.view.VideoS3BrowserFragment.class);

    class Anon1 implements com.amazonaws.event.ProgressListener {
        final /* synthetic */ java.lang.String val$fileName;
        final /* synthetic */ java.io.File val$tempFile;

        Anon1(java.io.File file, java.lang.String str) {
            this.val$tempFile = file;
            this.val$fileName = str;
        }

        public void progressChanged(com.amazonaws.event.ProgressEvent progressEvent) {
            if (progressEvent.getEventCode() == 4) {
                new com.navdy.client.debug.file.RemoteFileTransferManager(com.navdy.client.debug.view.OTAS3BrowserFragment.this.mContext, this.val$tempFile, com.navdy.service.library.events.file.FileType.FILE_TYPE_OTA, this.val$fileName, (com.navdy.client.debug.file.FileTransferManager.FileTransferListener) new com.navdy.client.debug.view.OTAS3BrowserFragment.TempFileTransferListener(this.val$tempFile)).sendFile();
            }
        }
    }

    private class TempFileTransferListener implements com.navdy.client.debug.file.FileTransferManager.FileTransferListener {
        private static final int REPORT_ON_CHUNKS_STEP = 5;
        long fileSize;
        java.lang.String fileSizeString = com.navdy.client.debug.util.FormatUtils.readableFileSize(this.fileSize);
        long previousThroughPutReportSize;
        long previousThroughPutReportTime;
        long startTime;
        java.io.File transferredFile;

        TempFileTransferListener(java.io.File transferredFile2) {
            this.transferredFile = transferredFile2;
            this.fileSize = transferredFile2.length();
        }

        public void onError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
            if (!this.transferredFile.delete()) {
                com.navdy.client.debug.view.OTAS3BrowserFragment.sLogger.e("Unable to delete transferred file: " + this.transferredFile);
            }
            com.navdy.client.debug.view.OTAS3BrowserFragment.sLogger.e(error);
            com.navdy.client.debug.view.OTAS3BrowserFragment.this.mAppInstance.showToast("Error while sending file. Check log.", false);
        }

        private void showMessageAndDeleteFile(java.lang.String msg) {
            if (!this.transferredFile.delete()) {
                com.navdy.client.debug.view.OTAS3BrowserFragment.sLogger.e("Unable to delete transferred file: " + this.transferredFile);
            }
            com.navdy.client.debug.view.OTAS3BrowserFragment.this.mAppInstance.showToast(msg, false);
        }

        public void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse fileTransferResponse) {
            com.navdy.client.debug.view.OTAS3BrowserFragment.sLogger.i(java.lang.String.format("onFileTransferResponse: %s transferId: %s", new java.lang.Object[]{fileTransferResponse, fileTransferResponse.transferId}));
            if (fileTransferResponse.success.booleanValue()) {
                com.navdy.client.debug.view.OTAS3BrowserFragment.this.mAppInstance.showToast(java.lang.String.format("File transfer (%s) request acknowledged", new java.lang.Object[]{fileTransferResponse.transferId}), false, 0);
                this.startTime = java.lang.System.currentTimeMillis() / 1000;
                this.previousThroughPutReportTime = this.startTime;
                this.previousThroughPutReportSize = 0;
                return;
            }
            showMessageAndDeleteFile(java.lang.String.format("File transfer (%s) request declined with status %s", new java.lang.Object[]{fileTransferResponse.transferId, fileTransferResponse.error}));
        }

        public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus) {
            com.navdy.client.debug.view.OTAS3BrowserFragment.sLogger.i(java.lang.String.format("onFileTransferStatus: %s transferId: %s chunk: %s", new java.lang.Object[]{fileTransferStatus.error, fileTransferStatus.transferId, fileTransferStatus.chunkIndex}));
            if (fileTransferStatus.success.booleanValue()) {
                showMessageAndDeleteFile(java.lang.String.format(java.util.Locale.getDefault(), "File transfer %d fully completed (average speed: %s/s)", new java.lang.Object[]{fileTransferStatus.transferId, com.navdy.client.debug.util.FormatUtils.readableFileSize(this.fileSize / ((java.lang.System.currentTimeMillis() / 1000) - this.startTime))}));
            } else if (fileTransferStatus.error == com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NO_ERROR) {
                showMessageAndDeleteFile(java.lang.String.format(java.util.Locale.getDefault(), "Error: Status %s returned while sending chunk %d of file transfer %d", new java.lang.Object[]{fileTransferStatus.error, fileTransferStatus.chunkIndex, fileTransferStatus.transferId}));
            } else if (fileTransferStatus.chunkIndex.intValue() % 5 == 0) {
                reportCurrentTransferStatus(fileTransferStatus);
            }
        }

        private void reportCurrentTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus) {
            long sentPartSize = fileTransferStatus.totalBytesTransferred.longValue();
            long currentTime = java.lang.System.currentTimeMillis() / 1000;
            long currentThroughput = (sentPartSize - this.previousThroughPutReportSize) / (currentTime - this.previousThroughPutReportTime);
            com.navdy.client.debug.view.OTAS3BrowserFragment.this.mAppInstance.showToast(java.lang.String.format(java.util.Locale.getDefault(), "Transfer %d (chunk %d):%n%s out of %s sent.%nETA: %s (current speed: %s/s)", new java.lang.Object[]{fileTransferStatus.transferId, fileTransferStatus.chunkIndex, com.navdy.client.debug.util.FormatUtils.readableFileSize(sentPartSize), this.fileSizeString, com.navdy.client.debug.util.FormatUtils.formatDurationFromSecondsToSecondsMinutesHours((int) (((double) ((int) (currentTime - this.startTime))) * (((((double) this.fileSize) * 1.0d) / ((double) sentPartSize)) - 1.0d))), com.navdy.client.debug.util.FormatUtils.readableFileSize(currentThroughput)}), false, 0);
            this.previousThroughPutReportTime = currentTime;
            this.previousThroughPutReportSize = sentPartSize;
        }
    }

    protected java.lang.String getS3BucketName() {
        return com.navdy.client.debug.util.S3Constants.NAVDY_PROD_RELEASE_S3_BUCKET;
    }

    protected void onFileSelected(java.lang.String fileS3Key) {
        java.lang.String fileName = new java.io.File(fileS3Key).getName();
        try {
            java.io.File tempFile = java.io.File.createTempFile("fromS3_", "_" + fileName);
            try {
                storeS3ObjectInTemp(fileS3Key, tempFile, new com.navdy.client.debug.view.OTAS3BrowserFragment.Anon1(tempFile, fileName));
            } catch (java.io.IOException e) {
                tempFile.deleteOnExit();
                sLogger.e("Exception while downloading file from S3", e);
                this.mAppInstance.showToast("Exception while downloading file from S3", false);
            }
        } catch (java.io.IOException e2) {
            sLogger.e("Exception while creating temp file", e2);
            this.mAppInstance.showToast("Exception while creating temp file", false);
        }
    }
}
