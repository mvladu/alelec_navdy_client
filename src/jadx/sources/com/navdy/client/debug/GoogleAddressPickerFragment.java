package com.navdy.client.debug;

public class GoogleAddressPickerFragment extends com.navdy.client.debug.common.BaseDebugFragment implements com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener {
    private static final java.lang.String HERE_GEOCODER_ENDPOINT = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=";
    private static final java.lang.String HERE_GEOCODER_ENDPOINT2 = "&app_code=";
    private static final java.lang.String HERE_GEOCODER_ENDPOINT3 = "&gen=9&searchtext=";
    private static java.lang.String HERE_GEO_APP_ID = null;
    private static final java.lang.String HERE_GEO_APP_ID_METADATA = "HERE_GEO_APP_ID";
    private static java.lang.String HERE_GEO_APP_TOKEN = null;
    private static final java.lang.String HERE_GEO_APP_TOKEN_METADATA = "HERE_GEO_APP_TOKEN";
    private static final java.lang.String HERE_LATITUDE_STR = "Latitude";
    private static final java.lang.String HERE_LOCATION_STR = "Location";
    private static final java.lang.String HERE_LONGITUDE_STR = "Longitude";
    private static final java.lang.String HERE_NAVIGATION_STR = "NavigationPosition";
    private static final java.lang.String HERE_RESPONSE_STR = "Response";
    private static final java.lang.String HERE_RESULT_STR = "Result";
    private static final java.lang.String HERE_VIEW_STR = "View";
    private static final int ROUTE_REQUEST_TIMEOUT = 15000;
    private static final int SEARCH_RADIUS_METERS = 50000;
    private static final java.lang.String SI_IS_ROUTE_REQUEST_PENDING = "1";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.GoogleAddressPickerFragment.class);
    private com.navdy.service.library.events.navigation.NavigationRouteResult chousedRoute;
    private int currentSelectedRouteIndex = -1;
    private com.navdy.service.library.events.navigation.NavigationRouteResponse event;
    private com.google.maps.GeoApiContext geoApiContext;
    private android.os.Handler handler = new android.os.Handler();
    private android.location.Location lastLocation;
    @butterknife.InjectView(2131755569)
    android.widget.ListView listView;
    private com.navdy.client.debug.PlaceAutocompleteAdapter mAdapter;
    private android.widget.AdapterView.OnItemClickListener mAutocompleteClickListener = new com.navdy.client.debug.GoogleAddressPickerFragment.Anon4();
    @butterknife.InjectView(2131755603)
    android.widget.AutoCompleteTextView mAutocompleteView;
    protected com.google.android.gms.common.api.GoogleApiClient mGoogleApiClient;
    protected com.google.android.gms.location.places.Place mLastSelectedPlace;
    private com.google.android.gms.location.LocationRequest mLocationRequest;
    private com.google.android.gms.maps.GoogleMap mMap;
    protected com.navdy.client.debug.navigation.NavigationManager mNavigationManager;
    protected com.google.android.gms.maps.model.LatLng mSearchLocation;
    private com.google.android.gms.common.api.ResultCallback<com.google.android.gms.location.places.PlaceBuffer> mUpdatePlaceDetailsCallback = new com.navdy.client.debug.GoogleAddressPickerFragment.Anon5();
    private android.app.ProgressDialog progressDialog;
    private boolean requestedLocationUpdates;
    private com.navdy.client.debug.GoogleAddressPickerFragment.RouteDescriptionAdapter routeDescriptionAdapter;
    private boolean routeRequestSendToHud;
    private java.lang.Runnable routeRequestTimeout = new com.navdy.client.debug.GoogleAddressPickerFragment.Anon1();
    private java.util.List<com.google.android.gms.maps.model.Polyline> routes = new java.util.ArrayList();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (com.navdy.client.debug.GoogleAddressPickerFragment.this.isAlive() && com.navdy.client.debug.GoogleAddressPickerFragment.this.routeRequestSendToHud) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.logger.v("HUD route request timed out");
                com.navdy.client.debug.GoogleAddressPickerFragment.this.routeRequestSendToHud = false;
                com.navdy.client.debug.GoogleAddressPickerFragment.this.stopProgress();
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.string.timed_out, 1).show();
            }
        }
    }

    class Anon2 implements com.google.android.gms.maps.OnMapReadyCallback {
        Anon2() {
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            com.navdy.client.debug.GoogleAddressPickerFragment.this.mMap = googleMap;
            if (com.navdy.client.debug.GoogleAddressPickerFragment.this.mSearchLocation != null) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.initMap();
            }
        }
    }

    class Anon3 implements android.widget.AdapterView.OnItemClickListener {
        Anon3() {
        }

        public void onItemClick(android.widget.AdapterView<?> adapterView, android.view.View view, int i, long l) {
            com.navdy.client.debug.GoogleAddressPickerFragment.this.listView.setVisibility(View.INVISIBLE);
            com.navdy.client.debug.GoogleAddressPickerFragment.this.showRouteDetails(i);
            com.navdy.client.debug.GoogleAddressPickerFragment.this.resetRouteColor();
            com.google.android.gms.maps.model.Polyline p = (com.google.android.gms.maps.model.Polyline) com.navdy.client.debug.GoogleAddressPickerFragment.this.routes.get(i);
            p.setColor(com.navdy.client.debug.GoogleAddressPickerFragment.this.getResources().getColor(com.navdy.client.R.color.blue_warm));
            p.setZIndex(3.0f);
            p.setWidth(10.0f);
            com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.i("Number of routes : " + com.navdy.client.debug.GoogleAddressPickerFragment.this.routes.size());
        }
    }

    class Anon4 implements android.widget.AdapterView.OnItemClickListener {
        Anon4() {
        }

        public void onItemClick(android.widget.AdapterView<?> adapterView, android.view.View view, int position, long id) {
            if (com.navdy.client.debug.GoogleAddressPickerFragment.this.lastLocation == null) {
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.debug.GoogleAddressPickerFragment.this.getString(com.navdy.client.R.string.no_location), 1).show();
                return;
            }
            com.navdy.client.debug.PlaceDebugAutocomplete item = com.navdy.client.debug.GoogleAddressPickerFragment.this.mAdapter.getItem(position);
            java.lang.String placeId = java.lang.String.valueOf(item.placeId);
            com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.i("Autocomplete item selected: " + item.description);
            com.google.android.gms.location.places.Places.GeoDataApi.getPlaceById(com.navdy.client.debug.GoogleAddressPickerFragment.this.mGoogleApiClient, placeId).setResultCallback(com.navdy.client.debug.GoogleAddressPickerFragment.this.mUpdatePlaceDetailsCallback);
            ((android.view.inputmethod.InputMethodManager) com.navdy.client.debug.GoogleAddressPickerFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(com.navdy.client.debug.GoogleAddressPickerFragment.this.getActivity().getCurrentFocus().getWindowToken(), 0);
            com.navdy.client.debug.GoogleAddressPickerFragment.this.mAutocompleteView.setText("");
            com.navdy.client.debug.GoogleAddressPickerFragment.this.listView.setVisibility(View.GONE);
            com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.i("Called getPlaceById to get Place details for " + item.placeId);
            com.navdy.client.debug.GoogleAddressPickerFragment.this.startProgress(com.navdy.client.debug.GoogleAddressPickerFragment.this.getString(com.navdy.client.R.string.search_for_route));
        }
    }

    class Anon5 implements com.google.android.gms.common.api.ResultCallback<com.google.android.gms.location.places.PlaceBuffer> {

        class Anon1 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
            final /* synthetic */ com.google.android.gms.location.places.Place val$destinationPlace;
            final /* synthetic */ boolean val$isStreetAddress;
            final /* synthetic */ com.google.android.gms.location.places.PlaceBuffer val$places;

            Anon1(com.google.android.gms.location.places.Place place, boolean z, com.google.android.gms.location.places.PlaceBuffer placeBuffer) {
                this.val$destinationPlace = place;
                this.val$isStreetAddress = z;
                this.val$places = placeBuffer;
            }

            public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.searchRoutesOnHud(this.val$destinationPlace, this.val$isStreetAddress, new com.google.android.gms.maps.model.LatLng(destination.navigationLat, destination.navigationLong));
                this.val$places.release();
            }

            public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.searchRoutesOnHud(this.val$destinationPlace, this.val$isStreetAddress, null);
                this.val$places.release();
            }
        }

        Anon5() {
        }

        public void onResult(com.google.android.gms.location.places.PlaceBuffer places) {
            boolean b;
            if (!places.getStatus().isSuccess()) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.stopProgress();
                com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.e("Place query did not complete. Error: " + places.getStatus().toString());
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), places.getStatus().toString(), 1).show();
            } else if (places.getCount() == 0) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.stopProgress();
                places.release();
                com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.e("No places returned");
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.string.search_failed, 1).show();
            } else {
                com.google.android.gms.location.places.Place place = null;
                boolean exitLoop = false;
                java.util.Iterator it = places.iterator();
                while (it.hasNext()) {
                    com.google.android.gms.location.places.Place p = (com.google.android.gms.location.places.Place) it.next();
                    java.util.Iterator it2 = p.getPlaceTypes().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (((java.lang.Integer) it2.next()).intValue() == 1021) {
                                place = p;
                                exitLoop = true;
                                continue;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    if (exitLoop) {
                        break;
                    }
                }
                if (place == null) {
                    com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.w("Street address type lookup failed: place is null");
                    place = places.get(0);
                    b = false;
                } else {
                    b = true;
                }
                boolean isStreetAddress = b;
                com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.i("Place details received: " + place.getName());
                com.navdy.client.debug.GoogleAddressPickerFragment.this.mLastSelectedPlace = place;
                com.navdy.client.debug.GoogleAddressPickerFragment.this.updateMap(place);
                com.google.android.gms.location.places.Place destinationPlace = place;
                com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.v("google display coordinate=" + place.getLatLng());
                java.lang.CharSequence name = destinationPlace.getName();
                java.lang.CharSequence address = destinationPlace.getAddress();
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(new com.navdy.client.app.framework.models.Destination(name != null ? name.toString() : "", address != null ? address.toString() : ""), new com.navdy.client.debug.GoogleAddressPickerFragment.Anon5.Anon1(destinationPlace, isStreetAddress, places));
            }
        }
    }

    class Anon6 implements com.google.maps.PendingResult.Callback<com.google.maps.model.DirectionsRoute[]> {
        final /* synthetic */ com.navdy.client.debug.GoogleAddressPickerFragment.INavigationPositionCallback val$callback;

        Anon6(com.navdy.client.debug.GoogleAddressPickerFragment.INavigationPositionCallback iNavigationPositionCallback) {
            this.val$callback = iNavigationPositionCallback;
        }

        public void onResult(com.google.maps.model.DirectionsRoute[] result) {
            com.google.android.gms.maps.model.LatLng navLatLng = null;
            if (result.length == 0) {
                com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.e("no route found!");
                this.val$callback.onSuccess(null);
                return;
            }
            com.google.maps.model.DirectionsRoute route = result[0];
            if (route.legs != null && route.legs.length > 0) {
                com.google.maps.model.DirectionsLeg leg = route.legs[route.legs.length - 1];
                if (leg != null) {
                    com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.v("leg startAddress:" + leg.startAddress + " latlng:" + leg.startLocation);
                    com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.v("leg startAddress:" + leg.endAddress + " latlng:" + leg.endLocation);
                    if (leg.endLocation != null) {
                        navLatLng = new com.google.android.gms.maps.model.LatLng(leg.endLocation.lat, leg.endLocation.lng);
                    }
                }
            }
            this.val$callback.onSuccess(navLatLng);
        }

        public void onFailure(java.lang.Throwable e) {
            this.val$callback.onFailure(e);
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.debug.GoogleAddressPickerFragment.INavigationPositionCallback val$callback;
        final /* synthetic */ java.lang.String val$streetAddress;

        Anon7(java.lang.String str, com.navdy.client.debug.GoogleAddressPickerFragment.INavigationPositionCallback iNavigationPositionCallback) {
            this.val$streetAddress = str;
            this.val$callback = iNavigationPositionCallback;
        }

        public void run() {
            java.io.InputStream inputStream = null;
            try {
                java.net.URL url = new java.net.URL(com.navdy.client.debug.GoogleAddressPickerFragment.HERE_GEOCODER_ENDPOINT + com.navdy.client.debug.GoogleAddressPickerFragment.HERE_GEO_APP_ID + com.navdy.client.debug.GoogleAddressPickerFragment.HERE_GEOCODER_ENDPOINT2 + com.navdy.client.debug.GoogleAddressPickerFragment.HERE_GEO_APP_TOKEN + com.navdy.client.debug.GoogleAddressPickerFragment.HERE_GEOCODER_ENDPOINT3 + java.net.URLEncoder.encode(this.val$streetAddress, "UTF-8"));
                javax.net.ssl.HttpsURLConnection urlConnection = (javax.net.ssl.HttpsURLConnection) url.openConnection();
                int httpResponse = urlConnection.getResponseCode();
                if (httpResponse < 200 || httpResponse >= 300) {
                    this.val$callback.onFailure(new java.lang.RuntimeException("response code:" + httpResponse));
                } else {
                    inputStream = urlConnection.getInputStream();
                    org.json.JSONObject navPos = ((org.json.JSONObject) new org.json.JSONObject(com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8")).getJSONObject(com.navdy.client.debug.GoogleAddressPickerFragment.HERE_RESPONSE_STR).getJSONArray(com.navdy.client.debug.GoogleAddressPickerFragment.HERE_VIEW_STR).get(0)).getJSONArray(com.navdy.client.debug.GoogleAddressPickerFragment.HERE_RESULT_STR).getJSONObject(0).getJSONObject("Location").getJSONArray(com.navdy.client.debug.GoogleAddressPickerFragment.HERE_NAVIGATION_STR).getJSONObject(0);
                    java.lang.String latStr = navPos.getString(com.navdy.client.debug.GoogleAddressPickerFragment.HERE_LATITUDE_STR);
                    java.lang.String lonStr = navPos.getString(com.navdy.client.debug.GoogleAddressPickerFragment.HERE_LONGITUDE_STR);
                    this.val$callback.onSuccess(new com.google.android.gms.maps.model.LatLng(java.lang.Double.valueOf(latStr).doubleValue(), java.lang.Double.valueOf(lonStr).doubleValue()));
                }
            } catch (Throwable t) {
                com.navdy.client.debug.GoogleAddressPickerFragment.sLogger.e("getNavigationCoordinate", t);
                this.val$callback.onFailure(t);
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            }
        }
    }

    class Anon8 implements com.google.android.gms.maps.GoogleMap.OnMapClickListener {
        Anon8() {
        }

        public void onMapClick(com.google.android.gms.maps.model.LatLng latLng) {
            java.util.List<com.google.android.gms.maps.model.Polyline> selectedPolylines = com.navdy.client.debug.GoogleAddressPickerFragment.this.getSelectedRoute(latLng);
            if (selectedPolylines.size() > 0) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.resetRouteColor();
                com.navdy.client.debug.GoogleAddressPickerFragment.this.currentSelectedRouteIndex = com.navdy.client.debug.GoogleAddressPickerFragment.this.currentSelectedRouteIndex < selectedPolylines.size() + -1 ? com.navdy.client.debug.GoogleAddressPickerFragment.access$Anon1704(com.navdy.client.debug.GoogleAddressPickerFragment.this) : 0;
                com.google.android.gms.maps.model.Polyline p = (com.google.android.gms.maps.model.Polyline) selectedPolylines.get(com.navdy.client.debug.GoogleAddressPickerFragment.this.currentSelectedRouteIndex);
                p.setWidth(10.0f);
                p.setColor(com.navdy.client.debug.GoogleAddressPickerFragment.this.getResources().getColor(com.navdy.client.R.color.blue_warm));
            }
        }
    }

    class Anon9 implements android.content.DialogInterface.OnCancelListener {
        Anon9() {
        }

        public void onCancel(android.content.DialogInterface dialog) {
            if (com.navdy.client.debug.GoogleAddressPickerFragment.this.isAlive() && com.navdy.client.debug.GoogleAddressPickerFragment.this.routeRequestSendToHud) {
                com.navdy.client.debug.GoogleAddressPickerFragment.this.routeRequestSendToHud = false;
                com.navdy.client.debug.GoogleAddressPickerFragment.this.handler.removeCallbacks(com.navdy.client.debug.GoogleAddressPickerFragment.this.routeRequestTimeout);
            }
        }
    }

    interface INavigationPositionCallback {
        void onFailure(java.lang.Throwable th);

        void onSuccess(com.google.android.gms.maps.model.LatLng latLng);
    }

    private class RouteDescriptionAdapter extends android.widget.BaseAdapter {
        private android.content.Context context;
        private java.util.List<com.navdy.client.debug.adapter.RouteDescriptionData> data;

        class Anon1 implements android.view.View.OnClickListener {
            Anon1() {
            }

            public void onClick(android.view.View view) {
                if (com.navdy.client.debug.GoogleAddressPickerFragment.this.getActivity() instanceof com.navdy.client.debug.GoogleAddressPickerFragment.RouteListener) {
                    ((com.navdy.client.debug.GoogleAddressPickerFragment.RouteListener) com.navdy.client.debug.GoogleAddressPickerFragment.this.getActivity()).setRoute(com.navdy.client.debug.GoogleAddressPickerFragment.this.chousedRoute);
                    com.navdy.client.debug.GoogleAddressPickerFragment.this.getFragmentManager().popBackStack();
                    return;
                }
                com.navdy.client.debug.RemoteNavControlFragment navControlFragment = com.navdy.client.debug.RemoteNavControlFragment.newInstance(com.navdy.client.debug.GoogleAddressPickerFragment.this.chousedRoute, com.navdy.client.debug.GoogleAddressPickerFragment.this.event.label);
                android.app.FragmentManager fm = com.navdy.client.debug.GoogleAddressPickerFragment.this.getFragmentManager();
                if (fm != null) {
                    android.app.FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(com.navdy.client.R.id.container, navControlFragment);
                    ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            }
        }

        class ViewHolder {
            android.widget.TextView duration;
            android.widget.TextView length;
            android.widget.Button queueNavigation;
            android.widget.TextView routeDiff;
            android.widget.Button startNavigation;

            ViewHolder() {
            }
        }

        RouteDescriptionAdapter(java.util.List<com.navdy.client.debug.adapter.RouteDescriptionData> data2, android.content.Context context2) {
            this.data = data2;
            this.context = context2;
        }

        public void setData(java.util.List<com.navdy.client.debug.adapter.RouteDescriptionData> data2) {
            this.data = data2;
            notifyDataSetChanged();
        }

        public java.util.List<com.navdy.client.debug.adapter.RouteDescriptionData> getData() {
            return this.data;
        }

        public int getCount() {
            return this.data.size();
        }

        public com.navdy.client.debug.adapter.RouteDescriptionData getItem(int position) {
            return (com.navdy.client.debug.adapter.RouteDescriptionData) this.data.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public android.view.View getView(int position, android.view.View convertView, android.view.ViewGroup parent) {
            com.navdy.client.debug.GoogleAddressPickerFragment.RouteDescriptionAdapter.ViewHolder holder;
            com.navdy.client.debug.adapter.RouteDescriptionData item = getItem(position);
            if (convertView == null) {
                convertView = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(com.navdy.client.R.layout.route_description, parent, false);
                holder = new com.navdy.client.debug.GoogleAddressPickerFragment.RouteDescriptionAdapter.ViewHolder();
                holder.routeDiff = (android.widget.TextView) convertView.findViewById(com.navdy.client.R.id.brightnessTextView);
                holder.duration = (android.widget.TextView) convertView.findViewById(com.navdy.client.R.id.duration);
                holder.length = (android.widget.TextView) convertView.findViewById(com.navdy.client.R.id.textView3);
                holder.startNavigation = (android.widget.Button) convertView.findViewById(com.navdy.client.R.id.button);
                holder.queueNavigation = (android.widget.Button) convertView.findViewById(com.navdy.client.R.id.button2);
                convertView.setTag(holder);
                holder.startNavigation.setOnClickListener(new com.navdy.client.debug.GoogleAddressPickerFragment.RouteDescriptionAdapter.Anon1());
            } else {
                holder = (com.navdy.client.debug.GoogleAddressPickerFragment.RouteDescriptionAdapter.ViewHolder) convertView.getTag();
            }
            holder.routeDiff.setText(com.navdy.client.debug.util.FormatUtils.addPrefixForRouteDifference(item.diff));
            if (position == 1) {
                holder.duration.setTextColor(com.navdy.client.debug.GoogleAddressPickerFragment.this.getResources().getColor(com.navdy.client.R.color.yellow));
            } else if (position == 2) {
                holder.duration.setTextColor(android.support.v4.internal.view.SupportMenu.CATEGORY_MASK);
            } else if (position == 0) {
                holder.duration.setTextColor(-16777216);
            }
            holder.duration.setText(com.navdy.client.debug.util.FormatUtils.formatDurationFromSecondsToSecondsMinutesHours(item.duration));
            holder.length.setText(com.navdy.client.debug.util.FormatUtils.formatLengthFromMetersToMiles(item.length));
            if (getCount() == 1) {
                holder.startNavigation.setVisibility(View.VISIBLE);
                holder.queueNavigation.setVisibility(View.VISIBLE);
            } else {
                holder.startNavigation.setVisibility(View.GONE);
                holder.queueNavigation.setVisibility(View.GONE);
            }
            return convertView;
        }
    }

    interface RouteListener {
        void setRoute(com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult);
    }

    static /* synthetic */ int access$Anon1704(com.navdy.client.debug.GoogleAddressPickerFragment x0) {
        int i = x0.currentSelectedRouteIndex + 1;
        x0.currentSelectedRouteIndex = i;
        return i;
    }

    static {
        try {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            android.content.pm.ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (ai.metaData != null) {
                HERE_GEO_APP_ID = ai.metaData.getString(HERE_GEO_APP_ID_METADATA);
                HERE_GEO_APP_TOKEN = ai.metaData.getString(HERE_GEO_APP_TOKEN_METADATA);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        java.lang.String apiKey = "";
        try {
            apiKey = com.navdy.client.app.NavdyApplication.getAppContext().getPackageManager().getApplicationInfo(com.navdy.client.app.NavdyApplication.getAppContext().getPackageName(), 128).metaData.getString("com.google.android.geo.API_KEY");
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            this.logger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (java.lang.NullPointerException e2) {
            this.logger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
        }
        this.geoApiContext = new com.google.maps.GeoApiContext().setApiKey(apiKey);
        if (savedInstanceState != null) {
            this.routeRequestSendToHud = savedInstanceState.getBoolean("1", false);
        }
        this.mNavigationManager = new com.navdy.client.debug.navigation.HUDNavigationManager();
    }

    @com.squareup.otto.Subscribe
    public void onRoutingResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse response) {
        if (this.routeRequestSendToHud) {
            this.handler.removeCallbacks(this.routeRequestTimeout);
            this.routeRequestSendToHud = false;
            handleRouteResponse(response);
            return;
        }
        sLogger.w("no current route request:" + response.label + " dest:" + response.destination);
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_google_address_picker, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        if (getActivity().getActionBar() != null) {
            getActivity().getActionBar().hide();
        }
        android.content.Context context = inflater.getContext();
        this.mAutocompleteView.setOnItemClickListener(this.mAutocompleteClickListener);
        this.mAdapter = new com.navdy.client.debug.PlaceAutocompleteAdapter(context, 17367043, null, null);
        if (this.mGoogleApiClient != null && this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
        rebuildGoogleApiClient();
        this.mAdapter.setGoogleApiClient(this.mGoogleApiClient);
        this.mAutocompleteView.setAdapter(this.mAdapter);
        com.google.android.gms.maps.MapFragment mMapFragment = com.google.android.gms.maps.MapFragment.newInstance();
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(com.navdy.client.R.id.map_container, mMapFragment);
        transaction.commit();
        mMapFragment.getMapAsync(new com.navdy.client.debug.GoogleAddressPickerFragment.Anon2());
        this.listView.setOnItemClickListener(new com.navdy.client.debug.GoogleAddressPickerFragment.Anon3());
        return rootView;
    }

    public void onSaveInstanceState(android.os.Bundle outState) {
        if (this.routeRequestSendToHud) {
            outState.putBoolean("1", true);
        }
        super.onSaveInstanceState(outState);
    }

    public void onDestroy() {
        stopProgress();
        super.onDestroy();
    }

    private void resetRouteColor() {
        for (com.google.android.gms.maps.model.Polyline polyline : this.routes) {
            polyline.setColor(getResources().getColor(com.navdy.client.R.color.blue));
            polyline.setZIndex(0.0f);
            polyline.setWidth(7.0f);
        }
    }

    private java.util.List<com.google.android.gms.maps.model.Polyline> getSelectedRoute(com.google.android.gms.maps.model.LatLng latLng) {
        java.util.List<com.google.android.gms.maps.model.Polyline> result = new java.util.ArrayList<>();
        for (com.google.android.gms.maps.model.Polyline polyline : this.routes) {
            if (com.google.maps.android.PolyUtil.isLocationOnPath(latLng, polyline.getPoints(), false, 100.0d)) {
                result.add(polyline);
            }
        }
        return result;
    }

    protected void showRouteDetails(int selectedRouteIndex) {
        this.chousedRoute = (com.navdy.service.library.events.navigation.NavigationRouteResult) this.event.results.get(selectedRouteIndex);
        int i = 0;
        for (com.google.android.gms.maps.model.Polyline polyline : this.routes) {
            if (i != selectedRouteIndex) {
                polyline.setVisible(false);
            }
            i++;
        }
        this.listView.setVisibility(View.VISIBLE);
        com.navdy.client.debug.adapter.RouteDescriptionData routeDescriptionData = (com.navdy.client.debug.adapter.RouteDescriptionData) this.routeDescriptionAdapter.getData().get(selectedRouteIndex);
        java.util.List<com.navdy.client.debug.adapter.RouteDescriptionData> data = new java.util.ArrayList<>();
        data.add(routeDescriptionData);
        this.routeDescriptionAdapter.setData(data);
    }

    private void getGoogleNavigationCoordinate(com.google.android.gms.location.places.Place place, com.navdy.client.debug.GoogleAddressPickerFragment.INavigationPositionCallback callback) {
        try {
            sLogger.v("using direction api to get nav position");
            com.google.android.gms.maps.model.LatLng latLng = place.getLatLng();
            com.google.maps.DirectionsApi.newRequest(this.geoApiContext).destination(new com.google.maps.model.LatLng(latLng.latitude, latLng.longitude)).origin(new com.google.maps.model.LatLng(this.lastLocation.getLatitude(), this.lastLocation.getLongitude())).setCallback(new com.navdy.client.debug.GoogleAddressPickerFragment.Anon6(callback));
        } catch (Throwable t) {
            sLogger.e("getNavigationCoordinate", t);
            callback.onFailure(t);
        }
    }

    private void getHereNavigationCoordinate(java.lang.String streetAddress, com.navdy.client.debug.GoogleAddressPickerFragment.INavigationPositionCallback callback) {
        try {
            if (HERE_GEO_APP_ID == null || HERE_GEO_APP_TOKEN == null) {
                java.lang.String str = "no here token in manifest";
                sLogger.v(str);
                callback.onFailure(new java.lang.RuntimeException(str));
                return;
            }
            sLogger.v("using here geocoder to get nav position");
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.debug.GoogleAddressPickerFragment.Anon7(streetAddress, callback), 3);
        } catch (Throwable t) {
            sLogger.e("getNavigationCoordinate", t);
            callback.onFailure(t);
        }
    }

    private void searchRoutesOnHud(com.google.android.gms.location.places.Place place, boolean isStreetAddress, com.google.android.gms.maps.model.LatLng navigationCoordinate) {
        com.google.android.gms.maps.model.LatLng latLng;
        startProgress(getString(com.navdy.client.R.string.calculate_route));
        java.lang.String label = place.getName().toString();
        java.lang.String streetAddress = place.getAddress().toString();
        com.navdy.service.library.events.location.Coordinate display = null;
        if (navigationCoordinate != null) {
            latLng = navigationCoordinate;
            com.google.android.gms.maps.model.LatLng displayPos = place.getLatLng();
            display = new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(displayPos.latitude), java.lang.Double.valueOf(displayPos.longitude), java.lang.Float.valueOf(0.0f), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Float.valueOf(0.0f), java.lang.Long.valueOf(0), "Google");
        } else {
            latLng = place.getLatLng();
        }
        com.navdy.service.library.events.location.Coordinate destination = new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(latLng.latitude), java.lang.Double.valueOf(latLng.longitude), java.lang.Float.valueOf(0.0f), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Float.valueOf(0.0f), java.lang.Long.valueOf(0), "Google");
        if (getActivity() instanceof com.navdy.client.debug.DestinationAddressPickerFragment.DestinationListener) {
            ((com.navdy.client.debug.DestinationAddressPickerFragment.DestinationListener) getActivity()).setDestination(label, destination);
        }
        sLogger.v("sending route to HUD:" + label + "dest:" + destination + " streetAddress:" + streetAddress);
        this.mNavigationManager.startRouteRequest(destination, label, null, streetAddress, display);
        this.handler.postDelayed(this.routeRequestTimeout, 15000);
        this.routeRequestSendToHud = true;
    }

    private void updateMap(com.google.android.gms.location.places.Place place) {
        com.google.android.gms.maps.model.LatLng location = place.getLatLng();
        this.mMap.animateCamera(com.google.android.gms.maps.CameraUpdateFactory.newLatLngZoom(location, 15.0f));
        this.mMap.clear();
        this.mMap.addMarker(new com.google.android.gms.maps.model.MarkerOptions().position(location).title((java.lang.String) place.getName()));
    }

    private com.google.android.gms.maps.model.Polyline addPolyline(java.util.List<com.google.android.gms.maps.model.LatLng> points) {
        com.google.android.gms.maps.model.PolylineOptions opt = new com.google.android.gms.maps.model.PolylineOptions();
        opt.addAll(points);
        com.google.android.gms.maps.model.Polyline line = this.mMap.addPolyline(opt.width(6.0f).color(getResources().getColor(com.navdy.client.R.color.blue)));
        sLogger.i("ADD TO MAP:/....... " + this.mMap.toString());
        return line;
    }

    protected synchronized void rebuildGoogleApiClient() {
        this.mGoogleApiClient = new com.google.android.gms.common.api.GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this).addApi(com.google.android.gms.location.places.Places.GEO_DATA_API).addApi(com.google.android.gms.location.LocationServices.API).build();
        this.mGoogleApiClient.connect();
    }

    public void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult connectionResult) {
        sLogger.e("onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
        this.mAdapter.setGoogleApiClient(null);
    }

    public void onConnected(android.os.Bundle bundle) {
        this.mAdapter.setGoogleApiClient(this.mGoogleApiClient);
        sLogger.i("GoogleApiClient connected.");
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(com.navdy.client.app.NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
            this.lastLocation = com.google.android.gms.location.LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        }
        if (this.lastLocation == null) {
            sLogger.e("lastLocation was null. Now requesting location update.");
            requestLocationUpdate();
            return;
        }
        setLocationBoundsForMap();
    }

    public void onConnectionSuspended(int i) {
        this.mAdapter.setGoogleApiClient(null);
        sLogger.e("GoogleApiClient connection suspended.");
    }

    public void createLocationRequest() {
        sLogger.i("creating LocationRequest Object");
        this.mLocationRequest = com.google.android.gms.location.LocationRequest.create().setPriority(100).setInterval(10000).setFastestInterval(1000);
    }

    public void requestLocationUpdate() {
        createLocationRequest();
        sLogger.i("calling requestLocationUpdates from FusedLocationApi");
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(com.navdy.client.app.NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
            com.google.android.gms.location.LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        }
        this.requestedLocationUpdates = true;
    }

    protected void stopLocationUpdates() {
        if (this.requestedLocationUpdates) {
            com.google.android.gms.location.LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
            this.requestedLocationUpdates = false;
        }
    }

    public void onLocationChanged(android.location.Location location) {
        if (this.lastLocation == null) {
            sLogger.v("Location changed");
            this.lastLocation = location;
            setLocationBoundsForMap();
            stopLocationUpdates();
        }
    }

    public void onPause() {
        stopLocationUpdates();
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (this.mGoogleApiClient.isConnected()) {
            requestLocationUpdate();
        }
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public void setLocationBoundsForMap() {
        this.mSearchLocation = new com.google.android.gms.maps.model.LatLng(this.lastLocation.getLatitude(), this.lastLocation.getLongitude());
        sLogger.i("Setting search locus to " + this.lastLocation.getLatitude() + org.droidparts.contract.SQL.DDL.SEPARATOR + this.lastLocation.getLongitude());
        this.mAdapter.setBounds(searchAreaFor(this.mSearchLocation));
        if (this.mMap != null) {
            initMap();
        }
    }

    private void handleRouteResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse event2) {
        if (isAlive()) {
            sLogger.e(event2.status + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + event2.statusDetail);
            stopProgress();
            this.event = event2;
            if (event2.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                sLogger.e("Unable to get routes.");
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), getString(com.navdy.client.R.string.calculate_route_failure, new java.lang.Object[]{event2.statusDetail}), 1).show();
                return;
            }
            java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> results = event2.results;
            if (results == null || results.size() == 0) {
                sLogger.e("No results.");
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.string.no_route, 1).show();
                return;
            }
            java.util.List<com.navdy.client.debug.adapter.RouteDescriptionData> data = new java.util.ArrayList<>();
            int i = 0;
            this.routes.clear();
            for (com.navdy.service.library.events.navigation.NavigationRouteResult res : results) {
                this.routes.add(addPolyline(com.navdy.client.app.framework.map.MapUtils.getGmsLatLongsFromRouteResult(res)));
                int i2 = i + 1;
                data.add(new com.navdy.client.debug.adapter.RouteDescriptionData(getRouteDifference(results, i).trim(), res.duration.intValue(), res.length.intValue(), res));
                i = i2;
            }
            this.routeDescriptionAdapter = new com.navdy.client.debug.GoogleAddressPickerFragment.RouteDescriptionAdapter(data, getActivity());
            this.listView.setAdapter(this.routeDescriptionAdapter);
            this.listView.setVisibility(View.VISIBLE);
            this.chousedRoute = (com.navdy.service.library.events.navigation.NavigationRouteResult) event2.results.get(0);
        }
    }

    private java.lang.String getRouteDifference(java.util.List<com.navdy.service.library.events.navigation.NavigationRouteResult> routeResults, int routeIndex) {
        if (routeResults == null || routeResults.size() == 0) {
            return "";
        }
        if (routeResults.size() <= 1) {
            return (java.lang.String) java.util.Arrays.asList(android.text.TextUtils.split(((com.navdy.service.library.events.navigation.NavigationRouteResult) routeResults.get(0)).label, ",")).get(0);
        }
        return findFirstDifference(((com.navdy.service.library.events.navigation.NavigationRouteResult) routeResults.get(routeIndex)).label, ((com.navdy.service.library.events.navigation.NavigationRouteResult) routeResults.get(routeIndex == routeResults.size() + -1 ? 0 : routeIndex + 1)).label);
    }

    private java.lang.String findFirstDifference(java.lang.String label1, java.lang.String label2) {
        java.util.List<java.lang.String> firstLabelList = java.util.Arrays.asList(android.text.TextUtils.split(label1, ","));
        java.util.List<java.lang.String> secondLabelList = java.util.Arrays.asList(android.text.TextUtils.split(label2, ","));
        int i = 0;
        for (java.lang.String l : firstLabelList) {
            if (!l.equalsIgnoreCase((java.lang.String) secondLabelList.get(i))) {
                return l;
            }
            i++;
        }
        return (java.lang.String) firstLabelList.get(0);
    }

    private com.google.android.gms.maps.model.LatLngBounds searchAreaFor(com.google.android.gms.maps.model.LatLng location) {
        return new com.google.android.gms.maps.model.LatLngBounds.Builder().include(com.google.maps.android.SphericalUtil.computeOffset(location, 50000.0d, 0.0d)).include(com.google.maps.android.SphericalUtil.computeOffset(location, 50000.0d, 90.0d)).include(com.google.maps.android.SphericalUtil.computeOffset(location, 50000.0d, 180.0d)).include(com.google.maps.android.SphericalUtil.computeOffset(location, 50000.0d, 270.0d)).build();
    }

    private void initMap() {
        this.mMap.moveCamera(com.google.android.gms.maps.CameraUpdateFactory.newLatLngZoom(this.mSearchLocation, 15.0f));
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(com.navdy.client.app.NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
            this.mMap.setMyLocationEnabled(true);
        }
        this.mMap.setOnMapClickListener(new com.navdy.client.debug.GoogleAddressPickerFragment.Anon8());
    }

    private void startProgress(java.lang.String str) {
        if (this.progressDialog == null) {
            this.progressDialog = new android.app.ProgressDialog(getActivity());
            this.progressDialog.setIndeterminate(true);
            this.progressDialog.setOnCancelListener(new com.navdy.client.debug.GoogleAddressPickerFragment.Anon9());
        }
        this.progressDialog.setMessage(str);
        this.progressDialog.show();
    }

    private void stopProgress() {
        if (this.progressDialog != null) {
            this.progressDialog.hide();
        }
    }
}
