package com.navdy.client.debug.videoplayer;

public class VideoPlayerActivity extends android.app.Activity implements android.media.MediaPlayer.OnPreparedListener {
    public static final java.lang.String BUCKET_NAME_EXTRA = "bucket_name_extra";
    public static final java.lang.String DB_URL_EXTRA = "db_url_extra";
    public static final java.lang.String SESSION_PATH_EXTRA = "session_path_extra";
    public static final java.lang.String VIDEO_URL_EXTRA = "video_url_extra";
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.videoplayer.VideoPlayerActivity.class);
    private java.lang.String bucketName;
    private com.amazonaws.mobileconnectors.s3.transfermanager.Download download;
    @butterknife.InjectView(2131755226)
    android.widget.TextView downloadStatus;
    private com.navdy.client.debug.videoplayer.VideoPlayerActivity.DownloadTask downloadTask;
    private java.io.File localDbFile;
    private android.widget.MediaController mediaController;
    private java.lang.String sessionPath;
    private com.navdy.client.debug.videoplayer.MockLocationTransmitter.ITimeSource timeSource = new com.navdy.client.debug.videoplayer.VideoPlayerActivity.Anon1();
    private com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager transferManager;
    private boolean videoPlayerIsInitialized = false;
    @butterknife.InjectView(2131755225)
    android.widget.VideoView videoView;

    class Anon1 implements com.navdy.client.debug.videoplayer.MockLocationTransmitter.ITimeSource {
        Anon1() {
        }

        public long getCurrentTime() {
            return (long) com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.videoView.getCurrentPosition();
        }
    }

    private class DownloadTask extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.startMockLocationTransmitter();
            }
        }

        class Anon2 implements com.amazonaws.event.ProgressListener {

            class Anon1 implements java.lang.Runnable {
                Anon1() {
                }

                public void run() {
                    com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.startMockLocationTransmitter();
                }
            }

            Anon2() {
            }

            public void progressChanged(com.amazonaws.event.ProgressEvent progressEvent) {
                com.navdy.client.debug.videoplayer.VideoPlayerActivity.sLogger.d("Download event " + progressEvent);
                int eventCode = progressEvent.getEventCode();
                if (eventCode == 4) {
                    com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.runOnUiThread(new com.navdy.client.debug.videoplayer.VideoPlayerActivity.DownloadTask.Anon2.Anon1());
                } else if (eventCode == 8) {
                    com.navdy.client.debug.videoplayer.VideoPlayerActivity.sLogger.d("Failed to download db");
                }
            }
        }

        private DownloadTask() {
        }

        /* synthetic */ DownloadTask(com.navdy.client.debug.videoplayer.VideoPlayerActivity x0, com.navdy.client.debug.videoplayer.VideoPlayerActivity.Anon1 x1) {
            this();
        }

        protected void onPreExecute() {
            com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.downloadStatus.setText(java.lang.String.format("Downloading %s%s", new java.lang.Object[]{com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.sessionPath, com.navdy.client.debug.util.S3Constants.DB_FILE}));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.downloadStatus.setVisibility(View.GONE);
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            java.lang.String key = com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.sessionPath + com.navdy.client.debug.util.S3Constants.DB_FILE;
            com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.localDbFile = new java.io.File(android.os.Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_DOWNLOADS), key);
            com.navdy.client.debug.videoplayer.VideoPlayerActivity.sLogger.d("Downloading s3 object (" + key + ") to " + com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.localDbFile.getAbsolutePath());
            if (!com.navdy.service.library.util.IOUtils.createDirectory(com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.localDbFile.getParentFile())) {
                com.navdy.client.debug.videoplayer.VideoPlayerActivity.sLogger.e("Failed to create directory for db at " + com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.localDbFile.getAbsolutePath());
            } else if (com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.localDbFile.exists()) {
                com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.runOnUiThread(new com.navdy.client.debug.videoplayer.VideoPlayerActivity.DownloadTask.Anon1());
            } else {
                com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.download = com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.transferManager.download(com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.bucketName, key, com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.localDbFile);
                com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.download.addProgressListener((com.amazonaws.event.ProgressListener) new com.navdy.client.debug.videoplayer.VideoPlayerActivity.DownloadTask.Anon2());
                try {
                    com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.download.waitForCompletion();
                } catch (java.lang.InterruptedException e) {
                    com.navdy.client.debug.videoplayer.VideoPlayerActivity.sLogger.d("Download interrupted ", e);
                }
                com.navdy.client.debug.videoplayer.VideoPlayerActivity.this.downloadTask = null;
            }
            return null;
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(com.navdy.client.R.layout.activity_video_player);
        this.sessionPath = getIntent().getStringExtra(SESSION_PATH_EXTRA);
        this.bucketName = getIntent().getStringExtra(BUCKET_NAME_EXTRA);
        butterknife.ButterKnife.inject((android.app.Activity) this);
        java.lang.String AWS_ACCOUNT_ID = "";
        java.lang.String AWS_SECRET = "";
        try {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            android.os.Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            AWS_ACCOUNT_ID = bundle.getString("AWS_ACCOUNT_ID");
            AWS_SECRET = bundle.getString("AWS_SECRET");
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            sLogger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (java.lang.NullPointerException e2) {
            sLogger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
        }
        this.transferManager = new com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager((com.amazonaws.auth.AWSCredentials) new com.amazonaws.auth.BasicAWSCredentials(AWS_ACCOUNT_ID, AWS_SECRET));
    }

    private void initVideoPlayer() {
        if (!this.videoPlayerIsInitialized) {
            this.videoPlayerIsInitialized = true;
            this.mediaController = new android.widget.MediaController(this);
            this.mediaController.setAnchorView(this.videoView);
            this.videoView.setOnPreparedListener(this);
            this.videoView.setMediaController(this.mediaController);
            java.net.URL videoUrl = (java.net.URL) getIntent().getSerializableExtra(VIDEO_URL_EXTRA);
            sLogger.d("Trying to play video at " + videoUrl);
            if (videoUrl != null) {
                this.videoView.setVideoURI(android.net.Uri.parse(videoUrl.toString()));
            }
        }
    }

    protected void onResume() {
        super.onResume();
        initVideoPlayer();
        if (this.videoView != null) {
            this.videoView.start();
        }
        this.downloadTask = new com.navdy.client.debug.videoplayer.VideoPlayerActivity.DownloadTask(this, null);
        this.downloadTask.execute(new java.lang.Void[0]);
    }

    protected void onPause() {
        super.onPause();
        if (this.videoView != null) {
            this.videoView.stopPlayback();
        }
        if (this.downloadTask != null) {
            this.downloadTask.cancel(false);
            this.downloadTask = null;
        }
        startLocationTransmitter();
    }

    protected void onStop() {
        if (this.videoView != null) {
            this.videoView.cancelPendingInputEvents();
            this.videoView = null;
        }
        if (this.mediaController != null) {
            this.mediaController.hide();
            this.mediaController.removeAllViews();
            this.mediaController = null;
        }
        super.onStop();
    }

    private void startLocationTransmitter() {
        com.navdy.client.app.framework.AppInstance instance = com.navdy.client.app.framework.AppInstance.getInstance();
        com.navdy.service.library.device.RemoteDevice device = instance.getRemoteDevice();
        if (device != null) {
            instance.setLocationTransmitter(new com.navdy.client.app.framework.servicehandler.LocationTransmitter(getApplicationContext(), device));
        }
    }

    private void startMockLocationTransmitter() {
        com.navdy.client.app.framework.AppInstance instance = com.navdy.client.app.framework.AppInstance.getInstance();
        com.navdy.service.library.device.RemoteDevice device = instance.getRemoteDevice();
        if (device != null) {
            sLogger.d("Starting location transmitter with db at " + this.localDbFile);
            com.navdy.client.debug.videoplayer.MockLocationTransmitter transmitter = new com.navdy.client.debug.videoplayer.MockLocationTransmitter(this, device);
            if (transmitter.setSource(this.localDbFile.getAbsolutePath(), this.timeSource)) {
                instance.setLocationTransmitter(transmitter);
            } else {
                android.widget.Toast.makeText(this, "No location info with video", 0).show();
            }
        }
    }

    public void onPrepared(android.media.MediaPlayer mediaPlayer) {
        if (com.navdy.client.app.ui.base.BaseActivity.isEnding(this)) {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
            }
            if (this.videoView != null) {
                this.videoView.stopPlayback();
            }
        } else if (mediaPlayer != null) {
            mediaPlayer.setLooping(true);
        }
    }
}
