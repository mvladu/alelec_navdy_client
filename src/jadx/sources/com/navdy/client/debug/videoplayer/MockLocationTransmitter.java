package com.navdy.client.debug.videoplayer;

public class MockLocationTransmitter extends com.navdy.client.app.framework.servicehandler.LocationTransmitter {
    private static final int DELAY_MS = 1000;
    private static final int NETWORK_UPDATE_INTERVAL = 10000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.videoplayer.MockLocationTransmitter.class);
    private java.util.Comparator<android.location.Location> comparator = new com.navdy.client.debug.videoplayer.MockLocationTransmitter.Anon2();
    private java.util.ArrayList<android.location.Location> coordinates = new java.util.ArrayList<>();
    private android.os.Handler handler = new android.os.Handler();
    private long lastNetworkUpdate = 0;
    private java.lang.Runnable locationUpdater = new com.navdy.client.debug.videoplayer.MockLocationTransmitter.Anon1();
    private long recordingDuration;
    private long recordingEndTime;
    private long recordingStartTime;
    private com.navdy.client.debug.videoplayer.MockLocationTransmitter.ITimeSource timeSource;
    private boolean transmitting = false;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            android.location.Location location = com.navdy.client.debug.videoplayer.MockLocationTransmitter.this.getLocation();
            com.navdy.client.debug.videoplayer.MockLocationTransmitter.this.transmitLocation(location);
            long now = java.lang.System.currentTimeMillis();
            if (now - com.navdy.client.debug.videoplayer.MockLocationTransmitter.this.lastNetworkUpdate > 10000) {
                com.navdy.client.debug.videoplayer.MockLocationTransmitter.this.lastNetworkUpdate = now;
                location.setProvider("network");
                location.setAccuracy(30.0f);
                location.setAltitude(0.0d);
                location.setBearing(0.0f);
                location.setSpeed(0.0f);
                com.navdy.client.debug.videoplayer.MockLocationTransmitter.this.transmitLocation(location);
            }
            if (com.navdy.client.debug.videoplayer.MockLocationTransmitter.this.transmitting) {
                com.navdy.client.debug.videoplayer.MockLocationTransmitter.this.handler.postDelayed(this, 1000);
            }
        }
    }

    class Anon2 implements java.util.Comparator<android.location.Location> {
        Anon2() {
        }

        public int compare(android.location.Location lhs, android.location.Location rhs) {
            long lh = lhs.getTime();
            long rh = rhs.getTime();
            if (lh < rh) {
                return -1;
            }
            return lh == rh ? 0 : 1;
        }
    }

    public interface ITimeSource {
        long getCurrentTime();
    }

    public MockLocationTransmitter(android.content.Context context, com.navdy.service.library.device.RemoteDevice remoteDevice) {
        super(context, remoteDevice);
    }

    public boolean hasLocationServices() {
        return true;
    }

    protected void startLocationUpdates() {
        if (!this.transmitting) {
            this.transmitting = true;
            this.handler.postDelayed(this.locationUpdater, 1000);
        }
    }

    protected void stopLocationUpdates() {
        if (this.transmitting) {
            this.transmitting = false;
            this.handler.removeCallbacks(this.locationUpdater);
        }
    }

    public android.location.Location getLocation() {
        long mappedTime = (this.timeSource.getCurrentTime() % this.recordingDuration) + this.recordingStartTime;
        android.location.Location dummy = new android.location.Location("gps");
        dummy.setTime(mappedTime);
        int position = java.util.Collections.binarySearch(this.coordinates, dummy, this.comparator);
        if (position < 0) {
            position = (-position) - 1;
        }
        if (position >= this.coordinates.size()) {
            return dummy;
        }
        android.location.Location copy = new android.location.Location((android.location.Location) this.coordinates.get(position));
        copy.setTime(java.lang.System.currentTimeMillis());
        return copy;
    }

    private boolean hasTable(android.database.sqlite.SQLiteDatabase db, java.lang.String tableName) {
        boolean z = false;
        android.database.Cursor cursor = null;
        try {
            android.database.Cursor cursor2 = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
            if (cursor2 != null && cursor2.getCount() > 0) {
                z = true;
            }
            if (cursor2 != null) {
                cursor2.close();
            }
        } catch (java.lang.RuntimeException e) {
            sLogger.e("Failed to read table info", e);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return z;
    }

    public boolean setSource(java.lang.String path, com.navdy.client.debug.videoplayer.MockLocationTransmitter.ITimeSource timeSource2) {
        this.coordinates.clear();
        this.timeSource = timeSource2;
        sLogger.d("Reading locations from db");
        android.database.sqlite.SQLiteDatabase db = android.database.sqlite.SQLiteDatabase.openDatabase(path, null, 268435456);
        if (!hasTable(db, "location")) {
            db.close();
            return false;
        }
        android.database.Cursor cursor = null;
        try {
            android.database.Cursor cursor2 = db.rawQuery("select * from location order by time", null);
            boolean first = true;
            while (cursor2.moveToNext()) {
                int col = 0 + 1;
                android.location.Location location = new android.location.Location(cursor2.getString(0));
                int col2 = col + 1;
                location.setAccuracy(cursor2.getFloat(col));
                int col3 = col2 + 1;
                location.setAltitude(cursor2.getDouble(col2));
                int col4 = col3 + 1;
                location.setBearing(cursor2.getFloat(col3));
                int col5 = col4 + 1;
                location.setLatitude(cursor2.getDouble(col4));
                int col6 = col5 + 1;
                location.setLongitude(cursor2.getDouble(col5));
                int col7 = col6 + 1;
                location.setSpeed(cursor2.getFloat(col6));
                int i = col7 + 1;
                long time = cursor2.getLong(col7);
                location.setTime(time);
                if (first) {
                    first = false;
                    this.recordingStartTime = time;
                }
                this.recordingEndTime = time;
                this.coordinates.add(location);
                cursor2.moveToNext();
            }
            if (cursor2 != null) {
                cursor2.close();
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Failed to load locations", e);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        db.close();
        if (this.coordinates.size() <= 1) {
            return false;
        }
        this.recordingDuration = this.recordingEndTime - this.recordingStartTime;
        sLogger.d("Read " + this.coordinates.size() + " locations from db - duration:" + (((double) this.recordingDuration) / 1000.0d) + " seconds");
        return true;
    }
}
