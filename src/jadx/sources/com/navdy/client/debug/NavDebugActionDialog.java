package com.navdy.client.debug;

public class NavDebugActionDialog extends android.app.Dialog {
    private static final int COORD_TEST_ADDRESS = 0;
    private static final int COORD_TEST_NAME = 1;
    private static final int GEO_INTENT_CALENDAR = 7;
    private static final int GEO_INTENT_HANGOUT = 8;
    private static final int GOOGLE_MAPS_INTENT_ADDRESS = 4;
    private static final int GOOGLE_MAPS_INTENT_COORD = 5;
    private static final int GOOGLE_MAPS_INTENT_URL_WITH_BOTH = 6;
    private static final int GOOGLE_NAV_INTENT_ADDRESS = 2;
    private static final int GOOGLE_NAV_INTENT_COORD = 3;
    private static final int SHARE_INTENT_ASSISTANT = 10;
    private static final int SHARE_INTENT_MAPS = 9;
    private static final int SHARE_INTENT_YELP = 11;
    private static final int STREET_VIEW = 12;

    class Anon1 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ android.content.Context val$context;
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon1(com.navdy.client.app.framework.models.Destination destination, android.content.Context context) {
            this.val$destination = destination;
            this.val$context = context;
        }

        public void onClick(android.content.DialogInterface dialog, int which) {
            android.content.Intent intent = new android.content.Intent("android.intent.action.VIEW");
            try {
                java.lang.String encodedAddress = java.net.URLEncoder.encode(this.val$destination.rawAddressNotForDisplay, "UTF-8");
                switch (which) {
                    case 1:
                        intent = null;
                        com.navdy.client.debug.NavCoordTestSuiteActivity.startNavCoordTestSuiteActivity(this.val$context, this.val$destination.name);
                        android.widget.Toast.makeText(this.val$context, "Test Running. Check logs for output", 0).show();
                        break;
                    case 2:
                        intent.setData(android.net.Uri.parse("google.navigation:q=" + encodedAddress));
                        break;
                    case 3:
                        intent.setData(android.net.Uri.parse("google.navigation:q=" + this.val$destination.displayLat + "%2C" + this.val$destination.displayLong));
                        break;
                    case 4:
                        intent.setData(android.net.Uri.parse("maps.google.com:q=" + encodedAddress));
                        break;
                    case 5:
                        intent.setData(android.net.Uri.parse("maps.google.com:q=" + this.val$destination.displayLat + "%2C" + this.val$destination.displayLong));
                        break;
                    case 6:
                        intent.setData(android.net.Uri.parse("https://maps.google.com:?q=" + encodedAddress + "&ll=" + this.val$destination.displayLat + "," + this.val$destination.displayLong));
                        break;
                    case 7:
                        intent.setData(android.net.Uri.parse("geo:" + this.val$destination.displayLat + "," + this.val$destination.displayLong + "?q=" + this.val$destination.name));
                        break;
                    case 8:
                        intent.setData(android.net.Uri.parse("geo:" + this.val$destination.displayLat + "," + this.val$destination.displayLong + "?q=" + this.val$destination.displayLat + "," + this.val$destination.displayLong + "(" + this.val$destination.name + ")"));
                        break;
                    case 9:
                        intent.setAction("android.intent.action.SEND");
                        intent.setType("text/plain");
                        intent.putExtra("android.intent.extra.TEXT", this.val$destination.name + "\n" + this.val$destination.rawAddressNotForDisplay + "\n" + "\n" + "https://goo.gl/maps/SOMESHORTURL");
                        break;
                    case 10:
                        intent.setAction("android.intent.action.SEND");
                        intent.setType("text/plain");
                        intent.putExtra("android.intent.extra.TEXT", this.val$destination.name + "\n" + "\n" + this.val$destination.rawAddressNotForDisplay + "\n" + "\n" + "https://g.co/kgs/SOMESHORTURL");
                        break;
                    case 11:
                        intent.setAction("android.intent.action.SEND");
                        intent.setType("text/plain");
                        java.lang.String encodedName = this.val$destination.name.replaceAll(" ", "-");
                        try {
                            encodedName = java.net.URLEncoder.encode(this.val$destination.name, "UTF-8");
                        } catch (java.io.UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        intent.putExtra("android.intent.extra.TEXT", this.val$destination.name + "\n" + "\n" + com.navdy.client.app.ui.search.SearchConstants.YELP_MAPS_URL + encodedName + "?utm_source=ashare&utm_campaign=status_quo_sheet&ref=yelp-android");
                        break;
                    case 12:
                        intent.setData(android.net.Uri.parse("google.streetview:cbll=" + this.val$destination.displayLat + "," + this.val$destination.displayLong));
                        break;
                    default:
                        intent = null;
                        com.navdy.client.debug.NavCoordTestSuiteActivity.startNavCoordTestSuiteActivity(this.val$context, this.val$destination.rawAddressNotForDisplay);
                        android.widget.Toast.makeText(this.val$context, "Test Running. Check logs for output", 0).show();
                        break;
                }
                if (intent != null) {
                    android.widget.Toast.makeText(this.val$context, "Sending " + intent, 1).show();
                    this.val$context.startActivity(intent);
                }
            } catch (java.io.UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
    }

    public NavDebugActionDialog(@android.support.annotation.NonNull android.content.Context context) {
        super(context);
    }

    public android.support.v7.app.AlertDialog createAndShow(com.navdy.client.app.framework.models.Destination destination) {
        android.content.Context context = getContext();
        java.lang.CharSequence[] options = context.getResources().getStringArray(com.navdy.client.R.array.nav_debug_actions);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setTitle((int) com.navdy.client.R.string.actions);
        builder.setItems(options, (android.content.DialogInterface.OnClickListener) new com.navdy.client.debug.NavDebugActionDialog.Anon1(destination, context));
        return builder.show();
    }
}
