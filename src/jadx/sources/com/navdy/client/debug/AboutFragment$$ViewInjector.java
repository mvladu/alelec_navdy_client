package com.navdy.client.debug;

public class AboutFragment$$ViewInjector {

    /* compiled from: AboutFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.AboutFragment val$target;

        Anon1(com.navdy.client.debug.AboutFragment aboutFragment) {
            this.val$target = aboutFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onCheckForUpdate((android.widget.Button) p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.AboutFragment target, java.lang.Object source) {
        target.appVersionLabel = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.app_version, "field 'appVersionLabel'");
        target.updateStatusTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.update_status, "field 'updateStatusTextView'");
        target.displayInfo = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.display_info, "field 'displayInfo'");
        target.serialLabel = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.serial_label, "field 'serialLabel'");
        target.serialNumber = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.serial_no, "field 'serialNumber'");
        target.vinLabel = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.vin_label, "field 'vinLabel'");
        target.vin = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.vin, "field 'vin'");
        finder.findRequiredView(source, com.navdy.client.R.id.update_button, "method 'onCheckForUpdate'").setOnClickListener(new com.navdy.client.debug.AboutFragment$$ViewInjector.Anon1(target));
    }

    public static void reset(com.navdy.client.debug.AboutFragment target) {
        target.appVersionLabel = null;
        target.updateStatusTextView = null;
        target.displayInfo = null;
        target.serialLabel = null;
        target.serialNumber = null;
        target.vinLabel = null;
        target.vin = null;
    }
}
