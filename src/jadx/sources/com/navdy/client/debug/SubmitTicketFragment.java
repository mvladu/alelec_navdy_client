package com.navdy.client.debug;

public class SubmitTicketFragment extends com.navdy.client.app.ui.base.BaseFragment {
    public static final java.lang.String BUG = "Bug";
    public static final int CONFIRMATION_DIALOG = 2;
    private static final int CONNECTION_TIMEOUT_IN_MINUTES = 2;
    public static final java.lang.String DESCRIPTION = "description";
    public static final int ERROR_DIALOG = 1;
    public static final java.lang.String FIELDS = "fields";
    public static final java.lang.String ISSUE_TYPE = "issuetype";
    private static final java.lang.String JIRA_ATTACHMENTS = "/attachments/";
    private static final java.lang.String JIRA_ISSUE_API_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    public static final java.lang.String KEY = "key";
    public static final java.lang.String NAME = "name";
    public static final int PROGRESS_DIALOG = 3;
    public static final java.lang.String PROJECT = "project";
    private static final java.lang.String PROJECT_KEY = "NAND";
    public static final long PULL_LOGS_TIMEOUT = 15000;
    public static final java.lang.String SUMMARY = "summary";
    private static final java.lang.String TEMP_FILE_TIMESTAMP_FORMAT = "'device_log'_yyyy_dd_MM-hh_mm_ss_aa'.txt'";
    private static final java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT);
    private java.lang.String ENCODED_CRED = "";
    @butterknife.InjectView(2131756016)
    android.widget.EditText mDescription;
    private java.lang.String mDeviceLogFile;
    private java.lang.String mDisplayLogFile;
    @javax.inject.Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager = null;
    private com.navdy.service.library.network.http.services.JiraClient mJiraClient;
    @butterknife.InjectView(2131756015)
    android.widget.EditText mTitle;
    @butterknife.InjectView(2131756014)
    android.widget.Spinner spinner;

    class Anon1 implements java.lang.Runnable {

        /* renamed from: com.navdy.client.debug.SubmitTicketFragment$Anon1$Anon1 reason: collision with other inner class name */
        class C0073Anon1 implements com.navdy.client.debug.file.FileTransferManager.FileTransferListener {
            final /* synthetic */ java.lang.String val$logsFolder;

            /* renamed from: com.navdy.client.debug.SubmitTicketFragment$Anon1$Anon1$Anon1 reason: collision with other inner class name */
            class C0074Anon1 implements java.lang.Runnable {
                C0074Anon1() {
                }

                public void run() {
                    android.widget.Toast.makeText(com.navdy.client.debug.SubmitTicketFragment.this.getActivity(), "Logs collected", 1).show();
                }
            }

            C0073Anon1(java.lang.String str) {
                this.val$logsFolder = str;
            }

            public void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse response) {
                if (!response.success.booleanValue()) {
                    com.navdy.client.debug.SubmitTicketFragment.this.removeDialog();
                    com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(1, com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.error), "Error collecting display logs");
                    return;
                }
                com.navdy.client.debug.SubmitTicketFragment.this.mDisplayLogFile = this.val$logsFolder + java.io.File.separator + response.destinationFileName;
            }

            public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus status) {
                if (status.success.booleanValue() && status.transferComplete.booleanValue()) {
                    com.navdy.client.debug.SubmitTicketFragment.this.removeDialog();
                    com.navdy.client.debug.SubmitTicketFragment.this.handler.post(new com.navdy.client.debug.SubmitTicketFragment.Anon1.C0073Anon1.C0074Anon1());
                    com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(3, "", "Submitting ticket");
                    com.navdy.client.debug.SubmitTicketFragment.this.bSubmitTicket();
                }
            }

            public void onError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
                com.navdy.client.debug.SubmitTicketFragment.this.removeDialog();
                com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(1, com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.error), "Error collecting display logs " + error);
            }
        }

        Anon1() {
        }

        public void run() {
            android.content.Context applicationContext = com.navdy.client.app.NavdyApplication.getAppContext();
            java.lang.String logsFolder = com.navdy.client.app.framework.PathManager.getInstance().getLogsFolder();
            com.navdy.client.debug.SubmitTicketFragment.this.mDeviceLogFile = logsFolder + java.io.File.separator + (com.navdy.client.debug.SubmitTicketFragment.format.format(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())) + ".txt");
            try {
                com.navdy.service.library.util.LogUtils.dumpLog(applicationContext, com.navdy.client.debug.SubmitTicketFragment.this.mDeviceLogFile);
                new com.navdy.client.debug.file.RemoteFileTransferManager(applicationContext, com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS, logsFolder, 15000, (com.navdy.client.debug.file.FileTransferManager.FileTransferListener) new com.navdy.client.debug.SubmitTicketFragment.Anon1.C0073Anon1(logsFolder)).pullFile();
            } catch (Throwable th) {
                com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(1, com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.error), "Error collecting device logs");
            }
        }
    }

    class Anon2 implements android.content.DialogInterface.OnClickListener {
        Anon2() {
        }

        public void onClick(android.content.DialogInterface dialog, int which) {
            dialog.dismiss();
            com.navdy.client.debug.SubmitTicketFragment.this.getFragmentManager().popBackStack();
        }
    }

    class Anon3 implements com.navdy.service.library.network.http.services.JiraClient.ResultCallback {
        final /* synthetic */ android.content.Context val$appContext;

        class Anon1 implements com.navdy.service.library.network.http.services.JiraClient.ResultCallback {
            final /* synthetic */ java.util.ArrayList val$attachments;
            final /* synthetic */ java.lang.String val$key;

            Anon1(java.lang.String str, java.util.ArrayList arrayList) {
                this.val$key = str;
                this.val$attachments = arrayList;
            }

            public void onSuccess(java.lang.Object object) {
                com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(2, com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.success), "Issue created and log files are uploaded :" + this.val$key);
                java.util.Iterator it = this.val$attachments.iterator();
                while (it.hasNext()) {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.client.debug.SubmitTicketFragment.Anon3.this.val$appContext, ((com.navdy.service.library.network.http.services.JiraClient.Attachment) it.next()).filePath);
                }
            }

            public void onError(java.lang.Throwable t) {
                com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(2, com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.success), "Issue created " + this.val$key);
                java.util.Iterator it = this.val$attachments.iterator();
                while (it.hasNext()) {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.client.debug.SubmitTicketFragment.Anon3.this.val$appContext, ((com.navdy.service.library.network.http.services.JiraClient.Attachment) it.next()).filePath);
                }
            }
        }

        Anon3(android.content.Context context) {
            this.val$appContext = context;
        }

        public void onSuccess(java.lang.Object object) {
            java.lang.String key = (java.lang.String) object;
            com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(3, "Uploading", "Uploading files");
            java.util.ArrayList<com.navdy.service.library.network.http.services.JiraClient.Attachment> attachments = new java.util.ArrayList<>();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(com.navdy.client.debug.SubmitTicketFragment.this.mDeviceLogFile)) {
                com.navdy.service.library.network.http.services.JiraClient.Attachment attachment = new com.navdy.service.library.network.http.services.JiraClient.Attachment();
                attachment.filePath = com.navdy.client.debug.SubmitTicketFragment.this.mDeviceLogFile;
                attachment.mimeType = "text/plain";
                attachments.add(attachment);
            }
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(com.navdy.client.debug.SubmitTicketFragment.this.mDisplayLogFile)) {
                com.navdy.service.library.network.http.services.JiraClient.Attachment attachment2 = new com.navdy.service.library.network.http.services.JiraClient.Attachment();
                attachment2.filePath = com.navdy.client.debug.SubmitTicketFragment.this.mDisplayLogFile;
                attachment2.mimeType = com.navdy.service.library.network.http.HttpUtils.MIME_TYPE_ZIP;
                attachments.add(attachment2);
            }
            if (attachments == null || attachments.size() <= 0) {
                com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(2, com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.success), "Issue created " + key);
            } else {
                com.navdy.client.debug.SubmitTicketFragment.this.mJiraClient.attachFilesToTicket(key, attachments, new com.navdy.client.debug.SubmitTicketFragment.Anon3.Anon1(key, attachments));
            }
        }

        public void onError(java.lang.Throwable t) {
            com.navdy.client.debug.SubmitTicketFragment.this.showSimpleDialog(1, com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.error), com.navdy.client.debug.SubmitTicketFragment.this.getString(com.navdy.client.R.string.error_creating_ticket));
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.ENCODED_CRED = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_jira_credentials));
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.mJiraClient = new com.navdy.service.library.network.http.services.JiraClient(this.mHttpManager.getClientCopy().readTimeout(2, java.util.concurrent.TimeUnit.MINUTES).connectTimeout(2, java.util.concurrent.TimeUnit.MINUTES).build());
        this.mJiraClient.setEncodedCredentials(this.ENCODED_CRED);
    }

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View view = android.view.LayoutInflater.from(getActivity()).inflate(com.navdy.client.R.layout.submit_ticket_layout, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, view);
        android.widget.ArrayAdapter<java.lang.CharSequence> adapter = android.widget.ArrayAdapter.createFromResource(view.getContext(), com.navdy.client.R.array.jira_projects, 17367048);
        adapter.setDropDownViewResource(17367049);
        this.spinner.setAdapter(adapter);
        this.spinner.setSelection(0);
        return view;
    }

    @butterknife.OnClick({2131756017})
    void onSubmit(android.view.View view) {
        if (this.mTitle.getText().toString().equals("")) {
            showSimpleDialog(1, getString(com.navdy.client.R.string.error), getString(com.navdy.client.R.string.please_enter_title));
        } else if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.client.app.NavdyApplication.getAppContext())) {
            showSimpleDialog(1, getString(com.navdy.client.R.string.error), getString(com.navdy.client.R.string.no_connectivity));
        } else {
            showSimpleDialog(3, getString(com.navdy.client.R.string.collect_logs), getString(com.navdy.client.R.string.collecting_device_logs));
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.debug.SubmitTicketFragment.Anon1(), 1);
        }
    }

    public android.app.Dialog createDialog(int id, android.os.Bundle arguments) {
        android.content.Context context = getActivity();
        if (context == null) {
            return null;
        }
        switch (id) {
            case 2:
                android.app.AlertDialog dialog = (android.app.AlertDialog) super.createDialog(id, arguments);
                dialog.setButton(-1, "Ok", new com.navdy.client.debug.SubmitTicketFragment.Anon2());
                return dialog;
            case 3:
                android.app.ProgressDialog progressDialog = new android.app.ProgressDialog(context);
                java.lang.String title = arguments.getString("title");
                if (title != null) {
                    progressDialog.setTitle(title);
                }
                java.lang.String message = arguments.getString("message");
                if (message != null) {
                    progressDialog.setMessage(message);
                }
                progressDialog.setCancelable(false);
                return progressDialog;
            default:
                return super.createDialog(id, arguments);
        }
    }

    private void bSubmitTicket() {
        android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
        if (appContext != null) {
            java.lang.String summary = this.mTitle.getText().toString();
            java.lang.String description = this.mDescription.getText().toString();
            java.lang.StringBuilder descriptionBuilder = new java.lang.StringBuilder();
            descriptionBuilder.append("Device : ").append(android.os.Build.MODEL).append(" : ").append(android.os.Build.VERSION.RELEASE).append(" (API ").append(android.os.Build.VERSION.SDK_INT).append(")\n\n");
            android.content.SharedPreferences preferences = com.navdy.client.ota.OTAUpdateService.getSharedPreferences();
            java.lang.String lastConnectedDeviceId = preferences.getString(com.navdy.client.ota.OTAUpdateService.LAST_CONNECTED_DEVICE_ID, null);
            if (lastConnectedDeviceId != null) {
                descriptionBuilder.append("Display : ").append(preferences.getString(lastConnectedDeviceId + "_" + com.navdy.client.ota.OTAUpdateService.SW_VERSION_NAME, null)).append(" , ID :").append(lastConnectedDeviceId).append("\n\n");
            }
            android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
            descriptionBuilder.append("fullName =    ").append(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, "")).append("\n");
            descriptionBuilder.append("email =       ").append(customerPrefs.getString("email", "")).append("\n");
            descriptionBuilder.append("carYear =     ").append(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "")).append("\n");
            descriptionBuilder.append("carMake =     ").append(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "")).append("\n");
            descriptionBuilder.append("carModel =    ").append(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "")).append("\n\n");
            descriptionBuilder.append(description);
            try {
                this.mJiraClient.submitTicket(this.spinner.getSelectedItem().toString(), "Bug", summary, description, new com.navdy.client.debug.SubmitTicketFragment.Anon3(appContext));
            } catch (java.io.IOException e) {
                showSimpleDialog(1, getString(com.navdy.client.R.string.error), getString(com.navdy.client.R.string.error_creating_ticket));
            }
        }
    }
}
