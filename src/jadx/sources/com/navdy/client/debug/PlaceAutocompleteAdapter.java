package com.navdy.client.debug;

public class PlaceAutocompleteAdapter extends android.widget.ArrayAdapter<com.navdy.client.debug.PlaceDebugAutocomplete> implements android.widget.Filterable {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.PlaceAutocompleteAdapter.class);
    private com.google.android.gms.maps.model.LatLngBounds mBounds;
    private com.google.android.gms.common.api.GoogleApiClient mGoogleApiClient;
    private com.google.android.gms.location.places.AutocompleteFilter mPlaceFilter;
    private java.util.ArrayList<com.navdy.client.debug.PlaceDebugAutocomplete> mResultList;

    class Anon1 extends android.widget.Filter {
        Anon1() {
        }

        protected android.widget.Filter.FilterResults performFiltering(java.lang.CharSequence constraint) {
            android.widget.Filter.FilterResults results = new android.widget.Filter.FilterResults();
            if (constraint != null) {
                com.navdy.client.debug.PlaceAutocompleteAdapter.this.mResultList = com.navdy.client.debug.PlaceAutocompleteAdapter.this.getAutocomplete(constraint);
                if (com.navdy.client.debug.PlaceAutocompleteAdapter.this.mResultList != null) {
                    results.values = com.navdy.client.debug.PlaceAutocompleteAdapter.this.mResultList;
                    results.count = com.navdy.client.debug.PlaceAutocompleteAdapter.this.mResultList.size();
                }
            }
            return results;
        }

        protected void publishResults(java.lang.CharSequence constraint, android.widget.Filter.FilterResults results) {
            if (results == null || results.count <= 0) {
                com.navdy.client.debug.PlaceAutocompleteAdapter.this.notifyDataSetInvalidated();
            } else {
                com.navdy.client.debug.PlaceAutocompleteAdapter.this.notifyDataSetChanged();
            }
        }
    }

    public PlaceAutocompleteAdapter(android.content.Context context, int resource, com.google.android.gms.maps.model.LatLngBounds bounds, com.google.android.gms.location.places.AutocompleteFilter filter) {
        super(context, resource);
        this.mBounds = bounds;
        this.mPlaceFilter = filter;
    }

    public void setGoogleApiClient(com.google.android.gms.common.api.GoogleApiClient googleApiClient) {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            this.mGoogleApiClient = null;
        } else {
            this.mGoogleApiClient = googleApiClient;
        }
    }

    public void setBounds(com.google.android.gms.maps.model.LatLngBounds bounds) {
        this.mBounds = bounds;
    }

    public int getCount() {
        if (this.mResultList == null) {
            return 0;
        }
        return this.mResultList.size();
    }

    public com.navdy.client.debug.PlaceDebugAutocomplete getItem(int position) {
        return (com.navdy.client.debug.PlaceDebugAutocomplete) this.mResultList.get(position);
    }

    public android.widget.Filter getFilter() {
        return new com.navdy.client.debug.PlaceAutocompleteAdapter.Anon1();
    }

    private java.util.ArrayList<com.navdy.client.debug.PlaceDebugAutocomplete> getAutocomplete(java.lang.CharSequence constraint) {
        if (this.mGoogleApiClient != null) {
            sLogger.i("Starting autocomplete query for: " + constraint);
            com.google.android.gms.location.places.AutocompletePredictionBuffer autocompletePredictions = (com.google.android.gms.location.places.AutocompletePredictionBuffer) com.google.android.gms.location.places.Places.GeoDataApi.getAutocompletePredictions(this.mGoogleApiClient, constraint.toString(), this.mBounds, this.mPlaceFilter).await(60, java.util.concurrent.TimeUnit.SECONDS);
            com.google.android.gms.common.api.Status status = autocompletePredictions.getStatus();
            if (!status.isSuccess()) {
                android.widget.Toast.makeText(getContext(), "Error contacting API: " + status.toString(), 0).show();
                sLogger.e("Error getting autocomplete prediction API call: " + status.toString());
                autocompletePredictions.release();
                return null;
            }
            sLogger.i("Query completed. Received " + autocompletePredictions.getCount() + " predictions.");
            java.util.Iterator<com.google.android.gms.location.places.AutocompletePrediction> iterator = autocompletePredictions.iterator();
            java.util.ArrayList<com.navdy.client.debug.PlaceDebugAutocomplete> resultList = new java.util.ArrayList<>(autocompletePredictions.getCount());
            while (iterator.hasNext()) {
                com.google.android.gms.location.places.AutocompletePrediction prediction = (com.google.android.gms.location.places.AutocompletePrediction) iterator.next();
                resultList.add(new com.navdy.client.debug.PlaceDebugAutocomplete(prediction.getPlaceId(), prediction.getFullText(null)));
            }
            autocompletePredictions.release();
            return resultList;
        }
        sLogger.e("Google API client is not connected for autocomplete query.");
        return null;
    }
}
