package com.navdy.client.debug.gesture;

public class GestureControlFragment extends android.app.Fragment {
    private static final java.util.Map<java.lang.Integer, com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction> DIAL_BUTTONS = new com.navdy.client.debug.gesture.GestureControlFragment.Anon1();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.gesture.GestureControlFragment.class);
    private android.view.View.OnKeyListener keyListener = new com.navdy.client.debug.gesture.GestureControlFragment.Anon2();
    private com.navdy.service.library.events.input.Gesture lastGesture = com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_UP;
    protected com.navdy.client.app.framework.AppInstance mAppInstance;
    private android.widget.SeekBar.OnSeekBarChangeListener seekBarChangeListener = new com.navdy.client.debug.gesture.GestureControlFragment.Anon3();
    @butterknife.InjectView(2131755588)
    android.widget.SeekBar slider;

    static class Anon1 extends java.util.HashMap<java.lang.Integer, com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction> {
        Anon1() {
            put(java.lang.Integer.valueOf(com.navdy.client.R.id.dial_click), com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_CLICK);
            put(java.lang.Integer.valueOf(com.navdy.client.R.id.dial_long_click), com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_LONG_CLICK);
            put(java.lang.Integer.valueOf(com.navdy.client.R.id.dial_left), com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_LEFT_TURN);
            put(java.lang.Integer.valueOf(com.navdy.client.R.id.dial_right), com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction.DIAL_RIGHT_TURN);
        }
    }

    class Anon2 implements android.view.View.OnKeyListener {
        Anon2() {
        }

        public boolean onKey(android.view.View v, int keyCode, android.view.KeyEvent keyEvent) {
            com.squareup.wire.Message message = null;
            switch (keyCode) {
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                    message = new com.navdy.service.library.events.Notification(java.lang.Integer.valueOf(-1), "", "Menu select", "", java.lang.Integer.toString((keyCode - 8) + 1), "sms");
                    break;
                case 19:
                    message = new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_UP, null, null);
                    break;
                case 20:
                case 66:
                    message = new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_TO_PINCH, null, null);
                    break;
                case 21:
                    message = new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT, null, null);
                    break;
                case 22:
                    message = new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT, null, null);
                    break;
                case 29:
                    message = new com.navdy.service.library.events.Notification(java.lang.Integer.valueOf(0), "", "+1 415-867-5309", "", "ANSWER", "sms");
                    break;
                case 37:
                    message = new com.navdy.service.library.events.Notification(java.lang.Integer.valueOf(0), "", "+1 800-555-1212", "", "IGNORE", "sms");
                    break;
                case 41:
                    message = new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_MENU).build();
                    break;
                default:
                    com.navdy.client.debug.gesture.GestureControlFragment.sLogger.e("Unknown keycode: " + keyCode);
                    break;
            }
            if (keyEvent.getAction() == 0 && message != null) {
                com.navdy.client.debug.gesture.GestureControlFragment.this.sendEvent(message);
            }
            if (message != null) {
                return true;
            }
            return false;
        }
    }

    class Anon3 implements android.widget.SeekBar.OnSeekBarChangeListener {
        Anon3() {
        }

        public void onProgressChanged(android.widget.SeekBar seekBar, int progress, boolean fromUser) {
            if (com.navdy.client.debug.gesture.GestureControlFragment.this.lastGesture == com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_TO_PINCH) {
                com.navdy.client.debug.gesture.GestureControlFragment.this.lastGesture = com.navdy.service.library.events.input.Gesture.GESTURE_PINCH;
            }
            if (com.navdy.client.debug.gesture.GestureControlFragment.this.lastGesture == com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_UP) {
                com.navdy.client.debug.gesture.GestureControlFragment.this.lastGesture = com.navdy.service.library.events.input.Gesture.GESTURE_FINGER;
            }
            if (com.navdy.client.debug.gesture.GestureControlFragment.this.lastGesture == com.navdy.service.library.events.input.Gesture.GESTURE_PINCH_TO_FINGER) {
                com.navdy.client.debug.gesture.GestureControlFragment.this.lastGesture = com.navdy.service.library.events.input.Gesture.GESTURE_FINGER;
            }
            com.navdy.client.debug.gesture.GestureControlFragment.this.sendEvent(new com.navdy.service.library.events.input.GestureEvent(com.navdy.client.debug.gesture.GestureControlFragment.this.lastGesture, java.lang.Integer.valueOf(progress), java.lang.Integer.valueOf(0)));
        }

        public void onStartTrackingTouch(android.widget.SeekBar seekBar) {
        }

        public void onStopTrackingTouch(android.widget.SeekBar seekBar) {
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_gesture_control, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.slider.setMax(600);
        this.slider.setProgress(300);
        this.slider.setOnSeekBarChangeListener(this.seekBarChangeListener);
        rootView.setOnKeyListener(this.keyListener);
        return rootView;
    }

    @butterknife.OnClick({2131755587, 2131755589})
    public void onSwipeClicked(android.view.View button) {
        sendEvent(new com.navdy.service.library.events.input.GestureEvent(button.getId() == com.navdy.client.R.id.swipe_left ? com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT : com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT, java.lang.Integer.valueOf(-1), java.lang.Integer.valueOf(-1)));
    }

    @butterknife.OnClick({2131755591, 2131755590, 2131755592})
    public void onFistClicked(android.view.View button) {
        com.navdy.service.library.events.input.Gesture gesture;
        switch (button.getId()) {
            case com.navdy.client.R.id.fist_with_index /*2131755590*/:
                gesture = com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_UP;
                break;
            case com.navdy.client.R.id.fist /*2131755591*/:
                for (com.navdy.service.library.events.input.Gesture g : new com.navdy.service.library.events.input.Gesture[]{com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_TO_PINCH, com.navdy.service.library.events.input.Gesture.GESTURE_PINCH, com.navdy.service.library.events.input.Gesture.GESTURE_PINCH_TO_FINGER}) {
                    sendEvent(new com.navdy.service.library.events.input.GestureEvent(g, null, null));
                }
                gesture = com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_TO_PINCH;
                break;
            case com.navdy.client.R.id.fist_offscreen /*2131755592*/:
                gesture = com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_DOWN;
                break;
            default:
                throw new java.lang.IllegalArgumentException("Unknown button id " + button.getId());
        }
        this.lastGesture = gesture;
        sendEvent(new com.navdy.service.library.events.input.GestureEvent(gesture, null, null));
    }

    @butterknife.OnClick({2131755596, 2131755595, 2131755594, 2131755597})
    public void onDialSimulationClicked(android.view.View button) {
        sendEvent(new com.navdy.service.library.events.input.DialSimulationEvent((com.navdy.service.library.events.input.DialSimulationEvent.DialInputAction) DIAL_BUTTONS.get(java.lang.Integer.valueOf(button.getId()))));
    }

    private void sendEvent(com.squareup.wire.Message event) {
        com.navdy.service.library.device.RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(event);
        }
    }
}
