package com.navdy.client.debug.gesture;

public class GestureControlFragment$$ViewInjector {

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon1(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onSwipeClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon2 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon2(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onSwipeClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon3 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon3(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onFistClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon4 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon4(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onFistClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon5 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon5(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onFistClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon6 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon6(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onDialSimulationClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon7 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon7(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onDialSimulationClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon8 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon8(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onDialSimulationClicked(p0);
        }
    }

    /* compiled from: GestureControlFragment$$ViewInjector */
    static class Anon9 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.gesture.GestureControlFragment val$target;

        Anon9(com.navdy.client.debug.gesture.GestureControlFragment gestureControlFragment) {
            this.val$target = gestureControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onDialSimulationClicked(p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.gesture.GestureControlFragment target, java.lang.Object source) {
        target.slider = (android.widget.SeekBar) finder.findRequiredView(source, com.navdy.client.R.id.x_slider, "field 'slider'");
        finder.findRequiredView(source, com.navdy.client.R.id.swipe_left, "method 'onSwipeClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon1(target));
        finder.findRequiredView(source, com.navdy.client.R.id.swipe_right, "method 'onSwipeClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon2(target));
        finder.findRequiredView(source, com.navdy.client.R.id.fist, "method 'onFistClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon3(target));
        finder.findRequiredView(source, com.navdy.client.R.id.fist_with_index, "method 'onFistClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon4(target));
        finder.findRequiredView(source, com.navdy.client.R.id.fist_offscreen, "method 'onFistClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon5(target));
        finder.findRequiredView(source, com.navdy.client.R.id.dial_click, "method 'onDialSimulationClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon6(target));
        finder.findRequiredView(source, com.navdy.client.R.id.dial_long_click, "method 'onDialSimulationClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon7(target));
        finder.findRequiredView(source, com.navdy.client.R.id.dial_left, "method 'onDialSimulationClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon8(target));
        finder.findRequiredView(source, com.navdy.client.R.id.dial_right, "method 'onDialSimulationClicked'").setOnClickListener(new com.navdy.client.debug.gesture.GestureControlFragment$$ViewInjector.Anon9(target));
    }

    public static void reset(com.navdy.client.debug.gesture.GestureControlFragment target) {
        target.slider = null;
    }
}
