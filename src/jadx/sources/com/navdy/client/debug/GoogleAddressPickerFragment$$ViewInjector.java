package com.navdy.client.debug;

public class GoogleAddressPickerFragment$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.GoogleAddressPickerFragment target, java.lang.Object source) {
        target.mAutocompleteView = (android.widget.AutoCompleteTextView) finder.findRequiredView(source, com.navdy.client.R.id.nav_autocomplete_places, "field 'mAutocompleteView'");
        target.listView = (android.widget.ListView) finder.findRequiredView(source, com.navdy.client.R.id.listView, "field 'listView'");
    }

    public static void reset(com.navdy.client.debug.GoogleAddressPickerFragment target) {
        target.mAutocompleteView = null;
        target.listView = null;
    }
}
