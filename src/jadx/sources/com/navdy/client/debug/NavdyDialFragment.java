package com.navdy.client.debug;

public class NavdyDialFragment extends android.app.ListFragment {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.NavdyDialFragment.class);
    private com.navdy.client.app.framework.AppInstance appInstance;
    private com.navdy.service.library.events.dial.DialBondRequest.DialAction lastAction;
    private java.util.List<com.navdy.client.debug.NavdyDialFragment.ListItem> listItems;

    private enum ListItem {
        GET_DIAL_STATUS(com.navdy.client.R.string.dial_get_status),
        BOND(com.navdy.client.R.string.dial_bond),
        REBOND(com.navdy.client.R.string.dial_rebond),
        CLEAR_BOND(com.navdy.client.R.string.dial_clear_bond);
        
        private final int mResId;
        private java.lang.String mString;

        private ListItem(int item) {
            this.mResId = item;
        }

        void bindResource(android.content.res.Resources r) {
            this.mString = r.getString(this.mResId);
        }

        public java.lang.String toString() {
            return this.mString;
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        this.listItems = new java.util.ArrayList(java.util.Arrays.asList(com.navdy.client.debug.NavdyDialFragment.ListItem.values()));
        for (com.navdy.client.debug.NavdyDialFragment.ListItem item : this.listItems) {
            item.bindResource(getResources());
        }
        setListAdapter(new android.widget.ArrayAdapter(getActivity(), 17367062, 16908308, com.navdy.client.debug.NavdyDialFragment.ListItem.values()));
    }

    public void onResume() {
        super.onResume();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public void onPause() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_settings, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        return rootView;
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        com.navdy.client.debug.NavdyDialFragment.ListItem clickedItem = (com.navdy.client.debug.NavdyDialFragment.ListItem) this.listItems.get(position);
        if (!com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            this.appInstance.showToast("Not connected to Hud", true, 0);
            return;
        }
        switch (clickedItem) {
            case GET_DIAL_STATUS:
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.dial.DialStatusRequest());
                sLogger.v("sent dial status request");
                return;
            case BOND:
                this.lastAction = com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_BOND;
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.dial.DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_BOND));
                sLogger.v("sent dial bonding request");
                return;
            case REBOND:
                this.lastAction = com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_REBOND;
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.dial.DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_REBOND));
                sLogger.v("sent dial repair bonding request");
                return;
            case CLEAR_BOND:
                this.lastAction = com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_CLEAR_BOND;
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.dial.DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_CLEAR_BOND));
                sLogger.v("sent dial clear bond request");
                return;
            default:
                return;
        }
    }

    @com.squareup.otto.Subscribe
    public void onDialStatusResponse(com.navdy.service.library.events.dial.DialStatusResponse status) {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        if (!java.lang.Boolean.TRUE.equals(status.isPaired)) {
            builder.append("Dial is not paired");
        } else {
            builder.append("Dial '").append(status.name).append("' is Paired ");
            if (!java.lang.Boolean.TRUE.equals(status.isConnected)) {
                builder.append("but Not Connected");
            } else {
                builder.append("and Connected");
            }
        }
        this.appInstance.showToast(builder.toString(), true, 1);
    }

    @com.squareup.otto.Subscribe
    public void onDialBondResponse(com.navdy.service.library.events.dial.DialBondResponse response) {
        java.lang.String msg = "";
        switch (response.status) {
            case DIAL_NOT_FOUND:
                msg = "No Dial Found";
                break;
            case DIAL_ALREADY_PAIRED:
                msg = "Already paired with " + response.name;
                break;
            case DIAL_PAIRED:
                msg = "Paired with " + response.name;
                break;
            case DIAL_NOT_PAIRED:
                if (this.lastAction == null) {
                    msg = "Cannot pair";
                    break;
                } else {
                    if (this.lastAction == com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_CLEAR_BOND || this.lastAction == com.navdy.service.library.events.dial.DialBondRequest.DialAction.DIAL_REBOND) {
                        msg = "Pairing removed";
                    } else {
                        msg = "Cannot pair";
                    }
                    this.lastAction = null;
                    break;
                }
                break;
            case DIAL_OPERATION_IN_PROGRESS:
                msg = "Pairing in progresss";
                break;
            case DIAL_ERROR:
                msg = "Error occurred";
                break;
            default:
                sLogger.e("Unknown dial response status: " + response.status);
                break;
        }
        this.appInstance.showToast(msg, true, 0);
    }
}
