package com.navdy.client.debug;

public class SettingsFragment extends android.app.ListFragment {
    private com.navdy.client.debug.ConnectionStatusUpdater mConnectionStatus;
    private java.util.List<com.navdy.client.debug.SettingsFragment.ListItem> mListItems;

    private enum ListItem {
        CONNECT(com.navdy.client.R.string.settings_connect),
        SCREEN_LAYOUT(com.navdy.client.R.string.settings_adjust_screen),
        HUD_SETTINGS(com.navdy.client.R.string.hud_settings),
        ABOUT(com.navdy.client.R.string.about);
        
        private final int mResId;
        private java.lang.String mString;

        private ListItem(int item) {
            this.mResId = item;
        }

        void bindResource(android.content.res.Resources r) {
            this.mString = r.getString(this.mResId);
        }

        public java.lang.String toString() {
            return this.mString;
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mListItems = new java.util.ArrayList(java.util.Arrays.asList(com.navdy.client.debug.SettingsFragment.ListItem.values()));
        for (com.navdy.client.debug.SettingsFragment.ListItem item : this.mListItems) {
            item.bindResource(getResources());
        }
        setListAdapter(new android.widget.ArrayAdapter(getActivity(), 17367062, 16908308, com.navdy.client.debug.SettingsFragment.ListItem.values()));
    }

    public android.view.View onCreateView(@android.support.annotation.NonNull android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_settings, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.mConnectionStatus = new com.navdy.client.debug.ConnectionStatusUpdater(rootView);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        this.mConnectionStatus.onResume();
    }

    public void onPause() {
        this.mConnectionStatus.onPause();
        super.onPause();
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        switch ((com.navdy.client.debug.SettingsFragment.ListItem) this.mListItems.get(position)) {
            case CONNECT:
                android.content.Intent i = new android.content.Intent(getActivity(), com.navdy.client.app.ui.settings.BluetoothPairActivity.class);
                i.putExtra(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
                startActivity(i);
                return;
            case HUD_SETTINGS:
                com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.HudSettingsFragment.class);
                return;
            case SCREEN_LAYOUT:
                com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.ScreenConfigFragment.class);
                return;
            case ABOUT:
                com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.AboutFragment.class);
                return;
            default:
                return;
        }
    }
}
