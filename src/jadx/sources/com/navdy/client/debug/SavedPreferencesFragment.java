package com.navdy.client.debug;

public class SavedPreferencesFragment extends android.preference.PreferenceFragment {
    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(com.navdy.client.R.xml.preferences);
    }
}
