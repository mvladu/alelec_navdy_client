package com.navdy.client.debug.devicepicker;

public enum PreferredConnectionType {
    TCP(com.navdy.service.library.device.connection.ConnectionType.TCP_PROTOBUF),
    BT(com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF),
    HTTP(com.navdy.service.library.device.connection.ConnectionType.HTTP);
    
    private com.navdy.service.library.device.connection.ConnectionType type;

    private PreferredConnectionType(com.navdy.service.library.device.connection.ConnectionType type2) {
        this.type = type2;
    }

    public com.navdy.service.library.device.connection.ConnectionType getType() {
        return this.type;
    }

    public int getPriority() {
        return ordinal();
    }

    static com.navdy.client.debug.devicepicker.PreferredConnectionType fromConnectionType(com.navdy.service.library.device.connection.ConnectionType type2) {
        com.navdy.client.debug.devicepicker.PreferredConnectionType[] values;
        for (com.navdy.client.debug.devicepicker.PreferredConnectionType preferredType : values()) {
            if (preferredType.getType() == type2) {
                return preferredType;
            }
        }
        return null;
    }
}
