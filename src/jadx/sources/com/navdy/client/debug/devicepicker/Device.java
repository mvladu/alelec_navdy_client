package com.navdy.client.debug.devicepicker;

public class Device {
    protected com.navdy.service.library.device.connection.ConnectionInfo[] connectionTypes;
    protected com.navdy.service.library.device.NavdyDeviceId deviceId;
    protected int preferredType;

    public static java.lang.String prettyName(com.navdy.service.library.device.connection.ConnectionInfo connection) {
        if (connection == null) {
            return "none";
        }
        java.lang.String deviceDescription = connection.getDeviceId().getDeviceName();
        if (deviceDescription == null) {
            return connection.getAddress().getAddress();
        }
        return deviceDescription;
    }

    public Device(com.navdy.service.library.device.connection.ConnectionInfo info) {
        this(info.getDeviceId());
        add(info);
    }

    public Device(com.navdy.service.library.device.NavdyDeviceId deviceId2) {
        this.connectionTypes = new com.navdy.service.library.device.connection.ConnectionInfo[com.navdy.client.debug.devicepicker.PreferredConnectionType.values().length];
        this.preferredType = 0;
        this.deviceId = deviceId2;
    }

    public void add(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo) {
        add(connectionInfo, true);
    }

    public void add(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo, boolean updateIfFound) {
        com.navdy.client.debug.devicepicker.PreferredConnectionType type = com.navdy.client.debug.devicepicker.PreferredConnectionType.fromConnectionType(connectionInfo.getType());
        if (updateIfFound || this.connectionTypes[type.getPriority()] == null) {
            this.connectionTypes[type.getPriority()] = connectionInfo;
        }
    }

    public com.navdy.service.library.device.NavdyDeviceId getDeviceId() {
        return this.deviceId;
    }

    public com.navdy.service.library.device.connection.ConnectionInfo[] getConnectionTypes() {
        return (com.navdy.service.library.device.connection.ConnectionInfo[]) this.connectionTypes.clone();
    }

    public com.navdy.service.library.device.connection.ConnectionInfo getPreferredConnectionInfo() {
        com.navdy.service.library.device.connection.ConnectionInfo[] connectionInfoArr;
        com.navdy.service.library.device.connection.ConnectionInfo preferredInfo = this.preferredType != -1 ? this.connectionTypes[this.preferredType] : null;
        if (preferredInfo != null) {
            return preferredInfo;
        }
        for (com.navdy.service.library.device.connection.ConnectionInfo info : this.connectionTypes) {
            if (info != null) {
                return info;
            }
        }
        return null;
    }

    public int getPreferredType() {
        return this.preferredType;
    }

    public void setPreferredType(com.navdy.client.debug.devicepicker.PreferredConnectionType type) {
        this.preferredType = type.getPriority();
    }

    public java.lang.String getIpAddress() {
        com.navdy.service.library.device.connection.ConnectionInfo tcpConnectionInfo = this.connectionTypes[com.navdy.client.debug.devicepicker.PreferredConnectionType.TCP.getPriority()];
        if (tcpConnectionInfo != null) {
            return tcpConnectionInfo.getAddress().getAddress();
        }
        return null;
    }

    public java.lang.String getBluetoothAddress() {
        com.navdy.service.library.device.connection.ConnectionInfo btConnectionInfo = this.connectionTypes[com.navdy.client.debug.devicepicker.PreferredConnectionType.BT.getPriority()];
        if (btConnectionInfo != null) {
            return btConnectionInfo.getAddress().getAddress();
        }
        return null;
    }

    public java.lang.String getPrettyName() {
        java.lang.String prettyName = this.deviceId.getDeviceName();
        if (prettyName != null && !this.deviceId.equals(com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID)) {
            return prettyName;
        }
        java.lang.String prettyName2 = getIpAddress();
        if (prettyName2 != null) {
            return prettyName2;
        }
        java.lang.String prettyName3 = getBluetoothAddress();
        if (prettyName3 != null) {
            return prettyName3;
        }
        return this.deviceId.getDisplayName();
    }

    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        com.navdy.client.debug.devicepicker.Device that = (com.navdy.client.debug.devicepicker.Device) o;
        if (!this.deviceId.equals(that.deviceId)) {
            return false;
        }
        if (!this.deviceId.equals(com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID)) {
            return true;
        }
        if (this.connectionTypes.length != that.connectionTypes.length) {
            return false;
        }
        for (int i = 0; i < this.connectionTypes.length; i++) {
            com.navdy.service.library.device.connection.ConnectionInfo lhs = this.connectionTypes[i];
            com.navdy.service.library.device.connection.ConnectionInfo rhs = that.connectionTypes[i];
            if (lhs == null || rhs == null) {
                if (lhs != rhs) {
                    return false;
                }
            } else if (!lhs.equals(rhs)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int result = this.deviceId.hashCode();
        if (this.deviceId.equals(com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID)) {
            for (com.navdy.service.library.device.connection.ConnectionInfo info : this.connectionTypes) {
                if (info != null) {
                    result = (result * 31) + info.hashCode();
                }
            }
        }
        return result;
    }

    public java.lang.String toString() {
        return getPrettyName();
    }
}
