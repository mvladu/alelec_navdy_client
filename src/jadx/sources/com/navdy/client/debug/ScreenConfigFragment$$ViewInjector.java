package com.navdy.client.debug;

public class ScreenConfigFragment$$ViewInjector {

    /* compiled from: ScreenConfigFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.ScreenConfigFragment val$target;

        Anon1(com.navdy.client.debug.ScreenConfigFragment screenConfigFragment) {
            this.val$target = screenConfigFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onResetScale();
        }
    }

    /* compiled from: ScreenConfigFragment$$ViewInjector */
    static class Anon2 implements android.view.View.OnTouchListener {
        final /* synthetic */ com.navdy.client.debug.ScreenConfigFragment val$target;

        Anon2(com.navdy.client.debug.ScreenConfigFragment screenConfigFragment) {
            this.val$target = screenConfigFragment;
        }

        public boolean onTouch(android.view.View p0, android.view.MotionEvent p1) {
            return this.val$target.onTouchButton(p0, p1);
        }
    }

    /* compiled from: ScreenConfigFragment$$ViewInjector */
    static class Anon3 implements android.view.View.OnTouchListener {
        final /* synthetic */ com.navdy.client.debug.ScreenConfigFragment val$target;

        Anon3(com.navdy.client.debug.ScreenConfigFragment screenConfigFragment) {
            this.val$target = screenConfigFragment;
        }

        public boolean onTouch(android.view.View p0, android.view.MotionEvent p1) {
            return this.val$target.onTouchButton(p0, p1);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.ScreenConfigFragment target, java.lang.Object source) {
        finder.findRequiredView(source, com.navdy.client.R.id.reset_scale, "method 'onResetScale'").setOnClickListener(new com.navdy.client.debug.ScreenConfigFragment$$ViewInjector.Anon1(target));
        finder.findRequiredView(source, com.navdy.client.R.id.slide_down, "method 'onTouchButton'").setOnTouchListener(new com.navdy.client.debug.ScreenConfigFragment$$ViewInjector.Anon2(target));
        finder.findRequiredView(source, com.navdy.client.R.id.slide_up, "method 'onTouchButton'").setOnTouchListener(new com.navdy.client.debug.ScreenConfigFragment$$ViewInjector.Anon3(target));
    }

    public static void reset(com.navdy.client.debug.ScreenConfigFragment target) {
    }
}
