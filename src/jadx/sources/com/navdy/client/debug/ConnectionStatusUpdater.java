package com.navdy.client.debug;

class ConnectionStatusUpdater {
    @butterknife.InjectView(2131755562)
    com.navdy.client.debug.view.ConnectionStateIndicator mConnectionIndicator;
    @butterknife.InjectView(2131755563)
    android.widget.TextView mTextViewCurrentDevice;

    ConnectionStatusUpdater(android.view.View parentView) {
        butterknife.ButterKnife.inject((java.lang.Object) this, parentView);
    }

    public void onResume() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        updateView();
    }

    public void onPause() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
    }

    @android.annotation.SuppressLint({"SetTextI18n"})
    private void updateView() {
        com.navdy.client.app.framework.DeviceConnection deviceConnection = com.navdy.client.app.framework.DeviceConnection.getInstance();
        if (deviceConnection != null) {
            this.mConnectionIndicator.setConnectionStatus(deviceConnection.getConnectionStatus());
            this.mConnectionIndicator.setConnectionType(deviceConnection.getConnectionType());
            this.mTextViewCurrentDevice.setText("Current device: " + com.navdy.client.debug.devicepicker.Device.prettyName(deviceConnection.getConnectionInfo()));
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnectingEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectingEvent event) {
        updateView();
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnectFailureEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectionFailedEvent event) {
        updateView();
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent event) {
        updateView();
    }

    @com.squareup.otto.Subscribe
    public void onDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        updateView();
    }
}
