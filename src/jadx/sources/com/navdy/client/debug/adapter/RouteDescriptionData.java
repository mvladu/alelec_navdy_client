package com.navdy.client.debug.adapter;

public class RouteDescriptionData {
    public java.lang.String diff;
    public int duration;
    public int length;

    public RouteDescriptionData(java.lang.String diff2, int duration2, int length2, com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult) {
        this.diff = diff2;
        this.duration = duration2;
        this.length = length2;
    }
}
