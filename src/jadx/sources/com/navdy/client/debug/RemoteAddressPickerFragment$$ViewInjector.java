package com.navdy.client.debug;

public class RemoteAddressPickerFragment$$ViewInjector {

    /* compiled from: RemoteAddressPickerFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.RemoteAddressPickerFragment val$target;

        Anon1(com.navdy.client.debug.RemoteAddressPickerFragment remoteAddressPickerFragment) {
            this.val$target = remoteAddressPickerFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onSearchClicked();
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.RemoteAddressPickerFragment target, java.lang.Object source) {
        target.mEditTextDestinationQuery = (org.droidparts.widget.ClearableEditText) finder.findRequiredView(source, com.navdy.client.R.id.nav_picker_edittext_destination, "field 'mEditTextDestinationQuery'");
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.nav_picker_button_search, "field 'mButtonSearch' and method 'onSearchClicked'");
        target.mButtonSearch = (android.widget.Button) view;
        view.setOnClickListener(new com.navdy.client.debug.RemoteAddressPickerFragment$$ViewInjector.Anon1(target));
    }

    public static void reset(com.navdy.client.debug.RemoteAddressPickerFragment target) {
        target.mEditTextDestinationQuery = null;
        target.mButtonSearch = null;
    }
}
