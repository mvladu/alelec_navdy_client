package com.navdy.client.debug;

public class ScreenTestingFragment extends android.app.ListFragment {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.ScreenTestingFragment.class);
    com.navdy.client.app.framework.AppInstance mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
    protected java.util.ArrayList<com.navdy.client.debug.ScreenTestingFragment.ListItem> mListItems;

    enum ListItem {
        WELCOME("Welcome"),
        HYBRID_MAP(com.navdy.client.app.framework.glances.GlanceConstants.ACTION_MAP),
        DASH("SmartDash"),
        CONTEXT_MENU("Context menu"),
        MAIN_MENU("Main menu"),
        FAV_PLACES("Favorite Places"),
        RECOMMENDED_PLACES("Recommended Places"),
        FAV_CONTACT("Favorite ContactModel"),
        RECENT_CONTACT("Recent ContactModel"),
        SCREEN_BACK("Back"),
        SCREEN_OTA_CONFIRM("OTA Confirmation"),
        SCREEN_OPTIONS("Options"),
        SCREEN_NOTIFICATION("Show Brightness Notification"),
        SHUTDOWN("ShutDown"),
        DIAL_PAIRING("Dial Pairing");
        
        private final java.lang.String mText;

        private ListItem(java.lang.String item) {
            this.mText = item;
        }

        public java.lang.String toString() {
            return this.mText;
        }
    }

    public void onCreate(android.os.Bundle savedState) {
        super.onCreate(savedState);
        this.mListItems = new java.util.ArrayList<>(java.util.Arrays.asList(com.navdy.client.debug.ScreenTestingFragment.ListItem.values()));
        setListAdapter(new android.widget.ArrayAdapter(getActivity(), 17367062, 16908308, com.navdy.client.debug.ScreenTestingFragment.ListItem.values()));
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_test_screen, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        return rootView;
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        switch ((com.navdy.client.debug.ScreenTestingFragment.ListItem) this.mListItems.get(position)) {
            case WELCOME:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME).build());
                return;
            case HYBRID_MAP:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP).build());
                return;
            case DASH:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD).build());
                return;
            case CONTEXT_MENU:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_CONTEXT_MENU).build());
                return;
            case MAIN_MENU:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_MENU).build());
                return;
            case FAV_PLACES:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_PLACES).build());
                return;
            case RECOMMENDED_PLACES:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_RECOMMENDED_PLACES).build());
                return;
            case FAV_CONTACT:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_CONTACTS).build());
                return;
            case RECENT_CONTACT:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_RECENT_CALLS).build());
                return;
            case SCREEN_BACK:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                return;
            case SCREEN_OTA_CONFIRM:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION).build());
                return;
            case SCREEN_OPTIONS:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_OPTIONS).build());
                return;
            case SCREEN_NOTIFICATION:
                sendEvent(new com.navdy.service.library.events.notification.ShowCustomNotification("BRIGHTNESS"));
                return;
            case SHUTDOWN:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION).build());
                return;
            case DIAL_PAIRING:
                sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING).build());
                return;
            default:
                sLogger.d("unhandled: " + id);
                return;
        }
    }

    private void sendEvent(com.squareup.wire.Message event) {
        com.navdy.service.library.device.RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(event);
        }
    }
}
