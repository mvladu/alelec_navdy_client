package com.navdy.client.debug.file;

public class RemoteFileTransferManager extends com.navdy.client.debug.file.FileTransferManager {
    private static final long CHUNK_TIMEOUT = 30000;
    private static final boolean FLOW_CONTROL_ENABLED = true;
    private static final long INITIAL_REQUEST_TIMEOUT = 10000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("HUDFileTransferManager");
    private long mBytesTransferredSoFar = 0;
    private int mChunkId = -1;
    private int mChunkSize = 0;
    private volatile boolean mFileUploadCancelled = false;
    private java.io.FileOutputStream mFileWriter;
    private boolean mIsDone = false;
    private final java.lang.Object mRemoteDeviceLock = new java.lang.Object();
    private long mRequestTimeout;
    private java.util.concurrent.ExecutorService mSerialExecutor;
    private boolean mServerSupportsFlowControl = false;
    private long mStartOffset = 0;
    private int mTransferId = -1;
    private java.util.Timer timeoutTimer;

    class Anon1 extends java.util.TimerTask {
        final /* synthetic */ java.lang.String val$timeoutMessage;

        Anon1(java.lang.String str) {
            this.val$timeoutMessage = str;
        }

        public void run() {
            com.navdy.client.debug.file.RemoteFileTransferManager.this.doneWithError(null, this.val$timeoutMessage);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.file.FileTransferData val$data;

        Anon2(com.navdy.service.library.events.file.FileTransferData fileTransferData) {
            this.val$data = fileTransferData;
        }

        public void run() {
            try {
                com.navdy.client.debug.file.RemoteFileTransferManager.this.mFileWriter.write(this.val$data.dataBytes.toByteArray());
                com.navdy.client.debug.file.RemoteFileTransferManager.this.mFileWriter.flush();
                if (this.val$data.lastChunk.booleanValue()) {
                    com.navdy.service.library.util.IOUtils.fileSync(com.navdy.client.debug.file.RemoteFileTransferManager.this.mFileWriter);
                    com.navdy.service.library.util.IOUtils.closeStream(com.navdy.client.debug.file.RemoteFileTransferManager.this.mFileWriter);
                    com.navdy.service.library.events.file.FileTransferStatus.Builder builder = new com.navdy.service.library.events.file.FileTransferStatus.Builder();
                    builder.success(java.lang.Boolean.valueOf(true)).transferComplete(java.lang.Boolean.valueOf(true)).transferId(java.lang.Integer.valueOf(com.navdy.client.debug.file.RemoteFileTransferManager.this.mTransferId));
                    com.navdy.client.debug.file.RemoteFileTransferManager.this.postOnFileTransferStatus(builder.build());
                    com.navdy.client.debug.file.RemoteFileTransferManager.this.done();
                }
            } catch (java.lang.Exception e) {
                com.navdy.client.debug.file.RemoteFileTransferManager.sLogger.e("Exception while writing the data to the file");
                com.navdy.service.library.util.IOUtils.closeStream(com.navdy.client.debug.file.RemoteFileTransferManager.this.mFileWriter);
            } finally {
                com.navdy.service.library.util.IOUtils.fileSync(com.navdy.client.debug.file.RemoteFileTransferManager.this.mFileWriter);
            }
        }
    }

    public RemoteFileTransferManager(android.content.Context context, com.navdy.service.library.events.file.FileType fileType, java.lang.String destinationFolder, long requestTimeOut, com.navdy.client.debug.file.FileTransferManager.FileTransferListener fileTransferListener) {
        super(context, fileType, destinationFolder, fileTransferListener);
        this.mRequestTimeout = requestTimeOut;
        this.mSerialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public RemoteFileTransferManager(android.content.Context context, java.io.File fileToSend, com.navdy.service.library.events.file.FileType fileType, java.lang.String destinationFileName, com.navdy.client.debug.file.FileTransferManager.FileTransferListener fileTransferListener) {
        super(context, com.navdy.service.library.file.TransferDataSource.fileSource(fileToSend), fileType, destinationFileName, fileTransferListener);
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public RemoteFileTransferManager(android.content.Context context, com.navdy.service.library.events.file.FileType fileType, long dataLength, com.navdy.client.debug.file.FileTransferManager.FileTransferListener fileTransferListener) {
        super(context, com.navdy.service.library.file.TransferDataSource.testSource(dataLength), fileType, null, fileTransferListener);
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public boolean sendFile() {
        return sendFile(0);
    }

    public boolean sendFile(long offset) {
        sLogger.i(java.lang.String.format("Sending file %s (%sB) to the HUD", new java.lang.Object[]{this.mDataSource.getName(), java.lang.Long.valueOf(this.mDataSource.length())}));
        long fileLength = this.mDataSource.length();
        if (offset >= fileLength) {
            offset = fileLength;
        }
        this.mStartOffset = offset;
        boolean result = sendFileTransferRequest(offset, false);
        setTimer(INITIAL_REQUEST_TIMEOUT, "Timeout while waiting for initial file transfer response");
        return result;
    }

    public boolean pullFile() {
        com.navdy.service.library.events.file.FileTransferRequest fileTransferRequest = new com.navdy.service.library.events.file.FileTransferRequest.Builder().fileType(this.mFileType).supportsAcks(java.lang.Boolean.valueOf(true)).build();
        this.mChunkId = -1;
        this.mBytesTransferredSoFar = 0;
        setTimer(this.mRequestTimeout, "Display did not respond in time");
        if (com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            return com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) fileTransferRequest);
        }
        postOnError(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR, "Phone is not connected to the Navdy display");
        return false;
    }

    public void cancelFileUpload() {
        sLogger.d("File upload cancel called");
        synchronized (this.mRemoteDeviceLock) {
            if (!this.mFileUploadCancelled) {
                this.mFileUploadCancelled = true;
                cancelTimeoutTimer();
                done();
            }
        }
    }

    private boolean sendFileTransferRequest(long offset, boolean override) {
        return postEvent(new com.navdy.service.library.events.file.FileTransferRequest.Builder().fileType(this.mFileType).destinationFileName(this.mDestinationFileName).offset(java.lang.Long.valueOf(offset)).fileDataChecksum(this.mDataSource.checkSum(offset)).fileSize(java.lang.Long.valueOf(this.mDataSource.length())).override(java.lang.Boolean.valueOf(override)).build());
    }

    private boolean postEvent(com.squareup.wire.Message message) {
        synchronized (this.mRemoteDeviceLock) {
            if (!this.mFileUploadCancelled) {
                boolean postEvent = com.navdy.client.app.framework.DeviceConnection.postEvent(message);
                return postEvent;
            }
            sLogger.d("Not sending the event to the HUD as the file upload is cancelled");
            return false;
        }
    }

    private void done() {
        if (!this.mIsDone) {
            com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        }
        this.mIsDone = true;
    }

    private void doneWithError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
        done();
        postOnError(errorCode, error);
    }

    private void setTimer(long timeout, java.lang.String timeoutMessage) {
        try {
            buildTimerInstance();
        } catch (java.util.ConcurrentModificationException e) {
            doneWithError(null, e.getMessage());
        }
        this.timeoutTimer.schedule(new com.navdy.client.debug.file.RemoteFileTransferManager.Anon1(timeoutMessage), timeout);
    }

    private synchronized void buildTimerInstance() throws java.util.ConcurrentModificationException {
        if (this.timeoutTimer != null) {
            throw new java.util.ConcurrentModificationException("Multiple instances of timeout timer are not allowed");
        }
        this.timeoutTimer = new java.util.Timer();
    }

    private synchronized void cancelTimeoutTimer() {
        if (this.timeoutTimer != null) {
            this.timeoutTimer.cancel();
            this.timeoutTimer.purge();
            this.timeoutTimer = null;
        } else {
            sLogger.w("Timeout timer for file transfer was null while trying to cancel it");
        }
    }

    @com.squareup.otto.Subscribe
    public void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse response) {
        if (!this.mFileUploadCancelled && response.fileType == this.mFileType) {
            this.mServerSupportsFlowControl = ((java.lang.Boolean) com.squareup.wire.Wire.get(response.supportsAcks, com.navdy.service.library.events.file.FileTransferResponse.DEFAULT_SUPPORTSACKS)).booleanValue();
            if (this.mDestinationFileName != null) {
                if (response.destinationFileName != null && response.destinationFileName.equals(this.mDestinationFileName)) {
                    sLogger.d("Response received " + response.fileType + " " + response.destinationFileName);
                } else {
                    return;
                }
            }
            cancelTimeoutTimer();
            postOnFileTransferResponse(response);
            if (!response.success.booleanValue()) {
                postOnError(response.error, "File transfer response error");
            } else if (com.navdy.service.library.file.FileTransferSessionManager.isPullRequest(response.fileType)) {
                this.mTransferId = response.transferId.intValue();
                java.lang.String destinationFileName = response.destinationFileName;
                this.mChunkSize = response.maxChunkSize.intValue();
                java.io.File destinationFile = new java.io.File(this.mDestinationFolder, destinationFileName);
                if (destinationFile.exists() && !destinationFile.delete()) {
                    sLogger.e("Unable to delete destination file: " + this.mDestinationFolder + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + destinationFileName);
                }
                try {
                    if (!destinationFile.createNewFile()) {
                        postOnError(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_PERMISSION_DENIED, "Cannot create the file");
                        return;
                    }
                    this.mFileWriter = new java.io.FileOutputStream(destinationFile);
                    setTimer(30000, "Failed to receive chunks in time");
                } catch (java.lang.Exception e) {
                    postOnError(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR, "Exception while creating the file");
                }
            } else {
                this.mTransferId = response.transferId.intValue();
                long negotiatedOffset = response.offset.longValue();
                this.mChunkSize = response.maxChunkSize.intValue();
                if (negotiatedOffset != this.mStartOffset) {
                    boolean override = false;
                    if (negotiatedOffset == 0) {
                        this.mStartOffset = 0;
                    } else {
                        if (negotiatedOffset < 0 || negotiatedOffset > this.mDataSource.length()) {
                            override = true;
                        } else if (!this.mDataSource.checkSum(negotiatedOffset).equals(response.checksum)) {
                            override = true;
                        }
                        if (override) {
                            negotiatedOffset = 0;
                        }
                        this.mStartOffset = negotiatedOffset;
                    }
                    if (override) {
                        sLogger.d("Overriding the negotiated offset as the file may be corrupted");
                        sendFileTransferRequest(this.mStartOffset, true);
                        return;
                    }
                }
                this.mBytesTransferredSoFar = this.mStartOffset;
                if (this.mStartOffset <= this.mDataSource.length()) {
                    sendNextChunk();
                }
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus fileTransferStatus) {
        if (!this.mFileUploadCancelled && fileTransferStatus.transferId != null && fileTransferStatus.transferId.equals(java.lang.Integer.valueOf(this.mTransferId))) {
            sLogger.d("File transfer status received ");
            cancelTimeoutTimer();
            postOnFileTransferStatus(fileTransferStatus);
            if (!fileTransferStatus.success.booleanValue() || fileTransferStatus.chunkIndex.intValue() != this.mChunkId) {
                postOnError(fileTransferStatus.error, "File transfer status error: Invalid chunk ID");
                return;
            }
            this.mBytesTransferredSoFar = fileTransferStatus.totalBytesTransferred.longValue();
            if (fileTransferStatus.transferComplete.booleanValue()) {
                done();
                return;
            }
            if (this.mDataSource != null) {
                java.io.File file = this.mDataSource.getFile();
                if (com.navdy.service.library.file.TransferDataSource.TEST_DATA_NAME.equals(this.mDataSource.getName()) || (file != null && file.exists() && file.length() > 0)) {
                    sendNextChunk();
                    return;
                }
            }
            postOnError(fileTransferStatus.error, "File transfer status error: File empty or does not exist anymore.");
        }
    }

    protected void postOnError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
        super.postOnError(errorCode, error);
        done();
    }

    @com.squareup.otto.Subscribe
    public void onFileTransferData(com.navdy.service.library.events.file.FileTransferData data) {
        boolean illegalChunk;
        if (!this.mFileUploadCancelled && data.transferId != null && data.transferId.equals(java.lang.Integer.valueOf(this.mTransferId))) {
            sLogger.d("Transfer data received");
            cancelTimeoutTimer();
            if (data.chunkIndex.intValue() != this.mChunkId + 1) {
                illegalChunk = true;
            } else {
                illegalChunk = false;
            }
            if (illegalChunk) {
                sLogger.e("Illegal chuck received from HUD Expected :" + this.mChunkId + 1 + ", Received :" + data.chunkIndex);
            }
            this.mBytesTransferredSoFar = (!illegalChunk ? (long) data.dataBytes.size() : 0) + this.mBytesTransferredSoFar;
            this.mChunkId = data.chunkIndex.intValue();
            if (this.mServerSupportsFlowControl) {
                com.navdy.service.library.events.file.FileTransferStatus.Builder builder = new com.navdy.service.library.events.file.FileTransferStatus.Builder();
                builder.transferId(data.transferId);
                if (!illegalChunk) {
                    builder.chunkIndex(data.chunkIndex).success(java.lang.Boolean.valueOf(true)).totalBytesTransferred(java.lang.Long.valueOf(this.mBytesTransferredSoFar));
                    builder.transferComplete(data.lastChunk);
                    postEvent(builder.build());
                } else {
                    builder.success(java.lang.Boolean.valueOf(false)).transferComplete(java.lang.Boolean.valueOf(false));
                    postEvent(builder.build());
                    return;
                }
            }
            if (!illegalChunk) {
                if (!data.lastChunk.booleanValue()) {
                    setTimer(30000, "Error receiving next chunk");
                }
                this.mSerialExecutor.submit(new com.navdy.client.debug.file.RemoteFileTransferManager.Anon2(data));
            }
        }
    }

    private void sendNextChunk() {
        try {
            int i = this.mTransferId;
            com.navdy.service.library.file.TransferDataSource transferDataSource = this.mDataSource;
            int i2 = this.mChunkId + 1;
            this.mChunkId = i2;
            if (postEvent(com.navdy.service.library.file.FileTransferSessionManager.prepareFileTransferData(i, transferDataSource, i2, this.mChunkSize, this.mStartOffset, this.mBytesTransferredSoFar))) {
                setTimer(30000, "Timeout while waiting for response on sent chunk");
            } else {
                sLogger.e("sendNextChunk: cannot send the chunk");
            }
        } catch (Throwable throwable) {
            sLogger.e("Error sending the chunk", throwable);
            postOnError(null, "Error sending chunk");
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        doneWithError(null, "Disconnected from the file receiver during the transfer process");
    }
}
