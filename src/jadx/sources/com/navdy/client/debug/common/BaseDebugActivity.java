package com.navdy.client.debug.common;

public class BaseDebugActivity extends android.app.Activity implements com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider {
    protected boolean destroyed;

    protected void onDestroy() {
        this.destroyed = true;
        super.onDestroy();
    }

    public boolean isActivityDestroyed() {
        return this.destroyed || isFinishing();
    }

    public android.app.Dialog createDialog(int id, android.os.Bundle arguments) {
        android.app.Fragment topFragment = com.navdy.client.debug.util.FragmentHelper.getTopFragment(getFragmentManager());
        if (topFragment == null || !(topFragment instanceof com.navdy.client.app.ui.base.BaseFragment)) {
            return null;
        }
        return ((com.navdy.client.app.ui.base.BaseFragment) topFragment).createDialog(id, arguments);
    }
}
