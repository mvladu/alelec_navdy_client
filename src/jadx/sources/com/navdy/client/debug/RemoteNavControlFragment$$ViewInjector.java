package com.navdy.client.debug;

public class RemoteNavControlFragment$$ViewInjector {

    /* compiled from: RemoteNavControlFragment$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.RemoteNavControlFragment val$target;

        Anon1(com.navdy.client.debug.RemoteNavControlFragment remoteNavControlFragment) {
            this.val$target = remoteNavControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onStartClicked();
        }
    }

    /* compiled from: RemoteNavControlFragment$$ViewInjector */
    static class Anon2 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.RemoteNavControlFragment val$target;

        Anon2(com.navdy.client.debug.RemoteNavControlFragment remoteNavControlFragment) {
            this.val$target = remoteNavControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onSimulateClicked();
        }
    }

    /* compiled from: RemoteNavControlFragment$$ViewInjector */
    static class Anon3 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.RemoteNavControlFragment val$target;

        Anon3(com.navdy.client.debug.RemoteNavControlFragment remoteNavControlFragment) {
            this.val$target = remoteNavControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onPauseClicked();
        }
    }

    /* compiled from: RemoteNavControlFragment$$ViewInjector */
    static class Anon4 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.RemoteNavControlFragment val$target;

        Anon4(com.navdy.client.debug.RemoteNavControlFragment remoteNavControlFragment) {
            this.val$target = remoteNavControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onResumeClicked();
        }
    }

    /* compiled from: RemoteNavControlFragment$$ViewInjector */
    static class Anon5 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.RemoteNavControlFragment val$target;

        Anon5(com.navdy.client.debug.RemoteNavControlFragment remoteNavControlFragment) {
            this.val$target = remoteNavControlFragment;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onStopClicked();
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.RemoteNavControlFragment target, java.lang.Object source) {
        target.mDestinationLabelTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.navigation_destination_label, "field 'mDestinationLabelTextView'");
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.navigation_button_start, "field 'mNavigationButtonStart' and method 'onStartClicked'");
        target.mNavigationButtonStart = (android.widget.Button) view;
        view.setOnClickListener(new com.navdy.client.debug.RemoteNavControlFragment$$ViewInjector.Anon1(target));
        android.view.View view2 = finder.findRequiredView(source, com.navdy.client.R.id.navigation_button_simulate, "field 'mNavigationButtonSimulate' and method 'onSimulateClicked'");
        target.mNavigationButtonSimulate = (android.widget.Button) view2;
        view2.setOnClickListener(new com.navdy.client.debug.RemoteNavControlFragment$$ViewInjector.Anon2(target));
        android.view.View view3 = finder.findRequiredView(source, com.navdy.client.R.id.navigation_button_pause, "field 'mNavigationButtonPause' and method 'onPauseClicked'");
        target.mNavigationButtonPause = (android.widget.Button) view3;
        view3.setOnClickListener(new com.navdy.client.debug.RemoteNavControlFragment$$ViewInjector.Anon3(target));
        android.view.View view4 = finder.findRequiredView(source, com.navdy.client.R.id.navigation_button_resume, "field 'mNavigationButtonResume' and method 'onResumeClicked'");
        target.mNavigationButtonResume = (android.widget.Button) view4;
        view4.setOnClickListener(new com.navdy.client.debug.RemoteNavControlFragment$$ViewInjector.Anon4(target));
        android.view.View view5 = finder.findRequiredView(source, com.navdy.client.R.id.navigation_button_stop, "field 'mNavigationButtonStop' and method 'onStopClicked'");
        target.mNavigationButtonStop = (android.widget.Button) view5;
        view5.setOnClickListener(new com.navdy.client.debug.RemoteNavControlFragment$$ViewInjector.Anon5(target));
        target.mTextViewCurrentSimSpeed = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.test_textview_current_sim_speed, "field 'mTextViewCurrentSimSpeed'");
    }

    public static void reset(com.navdy.client.debug.RemoteNavControlFragment target) {
        target.mDestinationLabelTextView = null;
        target.mNavigationButtonStart = null;
        target.mNavigationButtonSimulate = null;
        target.mNavigationButtonPause = null;
        target.mNavigationButtonResume = null;
        target.mNavigationButtonStop = null;
        target.mTextViewCurrentSimSpeed = null;
    }
}
