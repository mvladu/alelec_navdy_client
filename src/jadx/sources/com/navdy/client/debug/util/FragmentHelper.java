package com.navdy.client.debug.util;

public class FragmentHelper {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.util.FragmentHelper.class);

    public static void pushFullScreenFragment(android.app.FragmentManager fragmentManager, java.lang.Class<? extends android.app.Fragment> fragmentClass) {
        pushFullScreenFragment(fragmentManager, fragmentClass, null);
    }

    public static void pushFullScreenFragment(android.app.FragmentManager fragmentManager, java.lang.Class<? extends android.app.Fragment> fragmentClass, android.os.Bundle args) {
        try {
            android.app.Fragment newFragment = (android.app.Fragment) fragmentClass.newInstance();
            if (args != null) {
                newFragment.setArguments(args);
            }
            newFragment.setRetainInstance(true);
            android.app.FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(com.navdy.client.R.id.container, newFragment);
            ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
        } catch (java.lang.InstantiationException e) {
            sLogger.d("Can't instantiate: " + fragmentClass.getCanonicalName(), e);
        } catch (java.lang.IllegalAccessException e2) {
            sLogger.d("Can't access: " + fragmentClass.getCanonicalName(), e2);
        }
    }

    public static android.app.Fragment getTopFragment(android.app.FragmentManager manager) {
        return manager.findFragmentById(com.navdy.client.R.id.container);
    }
}
