package com.navdy.client.debug.util;

public class S3Constants {
    public static final java.lang.String BUCKET_NAME = "gesture-data";
    public static final java.lang.String DB_FILE = "db.sqlite3";
    public static final java.lang.String LOOKUP_FOLDER_PREFIX = "lookup";
    public static final java.lang.String NAVDY_BUILDS_S3_BUCKET = "navdy-builds";
    public static final java.lang.String NAVDY_PROD_RELEASE_S3_BUCKET = "navdy-prod-release";
    public static final java.lang.String OTA_LOOKUP_FILE_NAME = "latest.json";
    public static final java.lang.String OTA_RELEASE_NOTES_FILE_NAME_FORMAT = "release_notes_%s.json";
    public static final java.lang.String S3_FILE_DELIMITER = "/";
    public static final java.lang.String S3_OTA_BETA_PREFIX = "OTA/beta";
    public static final java.lang.String S3_OTA_RELEASE_PREFIX = "OTA/release";
    public static final java.lang.String S3_OTA_STABLE_PREFIX = "OTA";
    public static final java.lang.String S3_OTA_UNSTABLE_PREFIX = "OTA/unstable";
    public static final java.lang.String S3_PROTOCOL = "s3://";

    public enum BuildSource {
        STABLE(com.navdy.client.debug.util.S3Constants.NAVDY_BUILDS_S3_BUCKET, com.navdy.client.debug.util.S3Constants.S3_OTA_STABLE_PREFIX, com.navdy.client.debug.util.S3Constants.BuildType.eng, com.navdy.client.R.string.build_source_stable),
        UNSTABLE(com.navdy.client.debug.util.S3Constants.NAVDY_BUILDS_S3_BUCKET, com.navdy.client.debug.util.S3Constants.S3_OTA_UNSTABLE_PREFIX, com.navdy.client.debug.util.S3Constants.BuildType.eng, com.navdy.client.R.string.build_source_nightly),
        BETA(com.navdy.client.debug.util.S3Constants.NAVDY_PROD_RELEASE_S3_BUCKET, com.navdy.client.debug.util.S3Constants.S3_OTA_BETA_PREFIX, com.navdy.client.debug.util.S3Constants.BuildType.user, com.navdy.client.R.string.build_source_beta),
        RELEASE(com.navdy.client.debug.util.S3Constants.NAVDY_PROD_RELEASE_S3_BUCKET, com.navdy.client.debug.util.S3Constants.S3_OTA_RELEASE_PREFIX, com.navdy.client.debug.util.S3Constants.BuildType.user, com.navdy.client.R.string.build_source_release);
        
        private java.lang.String bucket;
        private com.navdy.client.debug.util.S3Constants.BuildType buildType;
        private int labelResourceId;
        private java.lang.String prefix;

        public java.lang.String getPrefix() {
            return this.prefix;
        }

        public java.lang.String getBucket() {
            return this.bucket;
        }

        public com.navdy.client.debug.util.S3Constants.BuildType getBuildType() {
            return this.buildType;
        }

        public int getLabelResourceId() {
            return this.labelResourceId;
        }

        private BuildSource(java.lang.String bucket2, java.lang.String prefix2, com.navdy.client.debug.util.S3Constants.BuildType type, int resourceId) {
            this.prefix = prefix2;
            this.bucket = bucket2;
            this.buildType = type;
            this.labelResourceId = resourceId;
        }
    }

    public enum BuildType {
        user,
        eng
    }

    public static com.navdy.client.debug.util.S3Constants.BuildSource[] getSourcesForBuildType(com.navdy.client.debug.util.S3Constants.BuildType type) {
        switch (type) {
            case user:
                return new com.navdy.client.debug.util.S3Constants.BuildSource[]{com.navdy.client.debug.util.S3Constants.BuildSource.BETA, com.navdy.client.debug.util.S3Constants.BuildSource.RELEASE};
            case eng:
                return new com.navdy.client.debug.util.S3Constants.BuildSource[]{com.navdy.client.debug.util.S3Constants.BuildSource.UNSTABLE, com.navdy.client.debug.util.S3Constants.BuildSource.STABLE};
            default:
                return null;
        }
    }

    public static com.navdy.client.debug.util.S3Constants.BuildSource getDefaultSourceForBuildType(com.navdy.client.debug.util.S3Constants.BuildType buildType) {
        return getSourcesForBuildType(buildType)[1];
    }
}
