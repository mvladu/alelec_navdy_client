package com.navdy.client.debug.util;

public class Contacts {
    public static java.lang.String lookupNameFromPhoneNumber(android.content.Context context, java.lang.String phoneNumber) {
        android.database.Cursor c = null;
        try {
            android.net.Uri lookupUri = android.net.Uri.withAppendedPath(android.provider.ContactsContract.PhoneLookup.CONTENT_FILTER_URI, android.net.Uri.encode(phoneNumber));
            android.database.Cursor c2 = context.getContentResolver().query(lookupUri, new java.lang.String[]{"display_name"}, null, null, null);
            if (c2 == null || c2.getCount() <= 0 || !c2.moveToNext()) {
                if (c2 != null) {
                    c2.close();
                }
                return null;
            }
            java.lang.String string = c2.getString(c2.getColumnIndexOrThrow("display_name"));
            if (c2 == null) {
                return string;
            }
            c2.close();
            return string;
        } catch (java.lang.Exception e) {
            if (c != null) {
                c.close();
            }
            return null;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }
}
