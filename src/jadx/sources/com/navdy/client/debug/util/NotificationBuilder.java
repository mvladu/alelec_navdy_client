package com.navdy.client.debug.util;

public class NotificationBuilder {
    public static com.navdy.service.library.events.notification.NotificationEvent buildSmsNotification(java.lang.String contactName, java.lang.String phoneNumber, java.lang.String message, boolean cannotReplyBack) {
        java.lang.String str;
        java.lang.Integer valueOf = java.lang.Integer.valueOf(0);
        com.navdy.service.library.events.notification.NotificationCategory notificationCategory = com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_SOCIAL;
        java.lang.String str2 = contactName != null ? contactName : phoneNumber;
        java.lang.String str3 = contactName != null ? phoneNumber : "";
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(message)) {
            str = "";
        } else {
            str = message;
        }
        return new com.navdy.service.library.events.notification.NotificationEvent(valueOf, notificationCategory, str2, str3, str, null, null, null, null, null, phoneNumber, java.lang.Boolean.valueOf(cannotReplyBack), null);
    }
}
