package com.navdy.client.debug.util;

public class FormatUtils {
    private static final java.text.DecimalFormat FILE_SIZE_FORMAT = new java.text.DecimalFormat("#,##0.#");
    private static final java.lang.String SPACE = " ";

    public static java.lang.String formatLengthFromMetersToMiles(int valueInMeters) {
        return new java.text.DecimalFormat(io.fabric.sdk.android.services.common.IdManager.DEFAULT_VERSION_NAME).format(((double) valueInMeters) / 1609.344d) + " miles";
    }

    public static java.lang.String formatDurationFromSecondsToSecondsMinutesHours(int valueInSeconds) {
        java.lang.String result = "";
        int numberOfHours = valueInSeconds / 3600;
        int numberOfMinutes = (valueInSeconds - ((numberOfHours * 60) * 60)) / 60;
        int numberOfSeconds = (valueInSeconds - ((numberOfHours * 60) * 60)) - (numberOfMinutes * 60);
        if (numberOfHours > 0) {
            result = numberOfHours + " h";
        }
        if (numberOfMinutes > 0) {
            if (numberOfSeconds > 30) {
                numberOfMinutes++;
            }
            result = result + SPACE + numberOfMinutes + " min";
        }
        if (numberOfHours == 0 && numberOfMinutes == 0 && numberOfSeconds > 0) {
            result = result + SPACE + numberOfSeconds + " sec";
        }
        return result.trim();
    }

    public static java.lang.String addPrefixForRouteDifference(java.lang.String diff) {
        return "via " + diff;
    }

    public static java.lang.String readableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        int digitGroups = (int) (java.lang.Math.log10((double) size) / java.lang.Math.log10(1024.0d));
        return java.lang.String.format("%s %s", new java.lang.Object[]{FILE_SIZE_FORMAT.format(((double) size) / java.lang.Math.pow(1024.0d, (double) digitGroups)), new java.lang.String[]{"B", "KB", "MB", "GB", "TB"}[digitGroups]});
    }
}
