package com.navdy.client.debug;

public class HudSettingsFragment extends com.navdy.client.app.ui.base.BaseFragment {
    public static final java.lang.String AUTO_BRIGHTNESS = "screen.auto_brightness";
    public static final java.lang.String BRIGHTNESS = "screen.brightness";
    public static final java.lang.String GESTURE_ENGINE = "gesture.engine";
    public static final java.lang.String GESTURE_PREVIEW = "gesture.preview";
    public static final java.lang.String LED_BRIGHTNESS = "screen.led_brightness";
    public static final java.lang.String MAP_TILT = "map.tilt";
    public static final java.lang.String MAP_ZOOM = "map.zoom";
    private static final double MAX_ZOOM_LEVEL = 20.0d;
    private static final double MIN_ZOOM_LEVEL = 2.0d;
    private static final double SCALE_FACTOR = 11.11111111111111d;
    public static final java.lang.String START_VIDEO_ON_BOOT = "start_video_on_boot_preference";
    private static final int ZOOM_MAX_PROGRESS = 200;
    private static final double ZOOM_RANGE = 18.0d;
    @butterknife.InjectView(2131755609)
    android.widget.CheckBox autoBrightness;
    @butterknife.InjectView(2131755611)
    android.widget.SeekBar brightness;
    @butterknife.InjectView(2131755610)
    android.widget.TextView brightnessTextView;
    @butterknife.InjectView(2131755615)
    android.widget.CheckBox enableGesture;
    @butterknife.InjectView(2131755616)
    android.widget.CheckBox enablePreview;
    @butterknife.InjectView(2131755607)
    android.widget.CheckBox enableSpecialVoiceSearch;
    @butterknife.InjectView(2131755617)
    android.widget.CheckBox enableVideoLoop;
    private java.util.List<com.navdy.service.library.events.settings.Setting> hudSettings;
    @butterknife.InjectView(2131755614)
    android.widget.SeekBar ledBrightness;
    @butterknife.InjectView(2131755613)
    android.widget.TextView ledBrightnessTextView;
    @butterknife.InjectView(2131755621)
    android.widget.SeekBar mapTilt;
    @butterknife.InjectView(2131755619)
    android.widget.SeekBar mapZoomLevel;
    private android.content.SharedPreferences sharedPreferences;
    private android.widget.SeekBar.OnSeekBarChangeListener sliderListener = new com.navdy.client.debug.HudSettingsFragment.Anon2();
    @butterknife.InjectView(2131755620)
    android.widget.TextView tiltLevelTextView;
    @butterknife.InjectView(2131755606)
    android.widget.TextView voiceSearchLabel;
    @butterknife.InjectView(2131755618)
    android.widget.TextView zoomLevelTextView;

    class Anon2 implements android.widget.SeekBar.OnSeekBarChangeListener {
        Anon2() {
        }

        public void onProgressChanged(android.widget.SeekBar seekBar, int i, boolean b) {
            java.lang.String setting;
            java.lang.String value = java.lang.String.valueOf(i);
            switch (seekBar.getId()) {
                case com.navdy.client.R.id.brightness /*2131755611*/:
                    setting = com.navdy.client.debug.HudSettingsFragment.BRIGHTNESS;
                    com.navdy.client.debug.HudSettingsFragment.this.brightnessTextView.setText(value);
                    break;
                case com.navdy.client.R.id.ledBrightness /*2131755614*/:
                    setting = com.navdy.client.debug.HudSettingsFragment.LED_BRIGHTNESS;
                    com.navdy.client.debug.HudSettingsFragment.this.ledBrightnessTextView.setText(value);
                    break;
                case com.navdy.client.R.id.mapZoomLevel /*2131755619*/:
                    setting = com.navdy.client.debug.HudSettingsFragment.MAP_ZOOM;
                    value = java.lang.String.format(java.util.Locale.US, "%.1f", new java.lang.Object[]{java.lang.Double.valueOf((((double) i) / com.navdy.client.debug.HudSettingsFragment.SCALE_FACTOR) + com.navdy.client.debug.HudSettingsFragment.MIN_ZOOM_LEVEL)});
                    com.navdy.client.debug.HudSettingsFragment.this.zoomLevelTextView.setText(value);
                    break;
                case com.navdy.client.R.id.mapTilt /*2131755621*/:
                    setting = com.navdy.client.debug.HudSettingsFragment.MAP_TILT;
                    com.navdy.client.debug.HudSettingsFragment.this.tiltLevelTextView.setText(value);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException("Unknown slider");
            }
            com.navdy.client.debug.HudSettingsFragment.this.updateSetting(setting, value);
        }

        public void onStartTrackingTouch(android.widget.SeekBar seekBar) {
        }

        public void onStopTrackingTouch(android.widget.SeekBar seekBar) {
        }
    }

    private class SettingSpinnerListener implements android.widget.AdapterView.OnItemSelectedListener {
        private java.lang.String setting;
        private java.lang.String value;

        SettingSpinnerListener(java.lang.String setting2, java.lang.String value2) {
            this.setting = setting2;
            this.value = value2;
        }

        public void onItemSelected(android.widget.AdapterView<?> parent, android.view.View view, int position, long id) {
            java.lang.String selectedValue = parent.getSelectedItem().toString();
            if (!selectedValue.equals(this.value)) {
                this.value = selectedValue;
                com.navdy.client.debug.HudSettingsFragment.this.updateSetting(this.setting, this.value);
            }
        }

        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_hud_settings, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.mapTilt.setOnSeekBarChangeListener(this.sliderListener);
        this.mapZoomLevel.setOnSeekBarChangeListener(this.sliderListener);
        this.brightness.setOnSeekBarChangeListener(this.sliderListener);
        this.ledBrightness.setOnSeekBarChangeListener(this.sliderListener);
        this.brightness.setEnabled(false);
        this.ledBrightness.setEnabled(false);
        this.sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        return rootView;
    }

    @butterknife.OnClick({2131755609, 2131755616, 2131755615, 2131755617})
    public void onClick(android.widget.CheckBox box) {
        java.lang.String name;
        java.lang.String checkedValue = java.lang.String.valueOf(box.isChecked());
        switch (box.getId()) {
            case com.navdy.client.R.id.autoBrightness /*2131755609*/:
                name = AUTO_BRIGHTNESS;
                break;
            case com.navdy.client.R.id.enable_gesture /*2131755615*/:
                name = GESTURE_ENGINE;
                break;
            case com.navdy.client.R.id.enable_preview /*2131755616*/:
                name = GESTURE_PREVIEW;
                break;
            case com.navdy.client.R.id.enable_video_loop /*2131755617*/:
                name = START_VIDEO_ON_BOOT;
                break;
            default:
                throw new java.lang.IllegalArgumentException("invalid checkbox");
        }
        updateSetting(name, checkedValue);
    }

    @butterknife.OnClick({2131755622})
    public void onClick(android.view.View button) {
        java.util.List<com.navdy.service.library.events.settings.Setting> settings = new java.util.ArrayList<>();
        settings.add(new com.navdy.service.library.events.settings.Setting(MAP_ZOOM, "-1"));
        settings.add(new com.navdy.service.library.events.settings.Setting(MAP_TILT, "-1"));
        sendEvent(new com.navdy.service.library.events.settings.UpdateSettings((com.navdy.service.library.events.settings.ScreenConfiguration) null, settings));
        this.hudSettings = null;
        getSettings();
    }

    private void updateSetting(java.lang.String name, java.lang.String value) {
        java.util.List<com.navdy.service.library.events.settings.Setting> settings = new java.util.ArrayList<>();
        settings.add(new com.navdy.service.library.events.settings.Setting(name, value));
        sendEvent(new com.navdy.service.library.events.settings.UpdateSettings((com.navdy.service.library.events.settings.ScreenConfiguration) null, settings));
    }

    public void onResume() {
        super.onResume();
        getSettings();
    }

    private void getSettings() {
        if (this.hudSettings == null) {
            java.util.List<java.lang.String> settings = new java.util.ArrayList<>();
            settings.add(AUTO_BRIGHTNESS);
            settings.add(BRIGHTNESS);
            settings.add(LED_BRIGHTNESS);
            settings.add(GESTURE_ENGINE);
            settings.add(GESTURE_PREVIEW);
            settings.add(MAP_TILT);
            settings.add(MAP_ZOOM);
            settings.add(START_VIDEO_ON_BOOT);
            sendEvent(new com.navdy.service.library.events.settings.ReadSettingsRequest(settings));
        }
    }

    private void sendEvent(com.squareup.wire.Message event) {
        com.navdy.client.app.framework.DeviceConnection.postEvent(event);
    }

    @com.squareup.otto.Subscribe
    public void onDeviceConnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent event) {
        getSettings();
    }

    @com.squareup.otto.Subscribe
    public void onDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        this.hudSettings = null;
    }

    @com.squareup.otto.Subscribe
    public void onReadSettingsResponse(com.navdy.service.library.events.settings.ReadSettingsResponse response) {
        this.hudSettings = response.settings;
        for (com.navdy.service.library.events.settings.Setting setting : this.hudSettings) {
            java.lang.String str = setting.key;
            char c = 65535;
            switch (str.hashCode()) {
                case -1036816761:
                    if (str.equals(GESTURE_ENGINE)) {
                        c = 4;
                        break;
                    }
                    break;
                case -790890461:
                    if (str.equals(GESTURE_PREVIEW)) {
                        c = 3;
                        break;
                    }
                    break;
                case -408006893:
                    if (str.equals(BRIGHTNESS)) {
                        c = 0;
                        break;
                    }
                    break;
                case 133816335:
                    if (str.equals(MAP_TILT)) {
                        c = 6;
                        break;
                    }
                    break;
                case 134000933:
                    if (str.equals(MAP_ZOOM)) {
                        c = 5;
                        break;
                    }
                    break;
                case 285256903:
                    if (str.equals(LED_BRIGHTNESS)) {
                        c = 2;
                        break;
                    }
                    break;
                case 1278641513:
                    if (str.equals(START_VIDEO_ON_BOOT)) {
                        c = 7;
                        break;
                    }
                    break;
                case 1839978335:
                    if (str.equals(AUTO_BRIGHTNESS)) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    updateSeekBar(this.brightness, setting.value);
                    break;
                case 1:
                    this.autoBrightness.setEnabled(true);
                    this.autoBrightness.setChecked(java.lang.Boolean.parseBoolean(setting.value));
                    break;
                case 2:
                    updateSeekBar(this.ledBrightness, setting.value);
                    break;
                case 3:
                    this.enablePreview.setEnabled(true);
                    this.enablePreview.setChecked(java.lang.Boolean.parseBoolean(setting.value));
                    break;
                case 4:
                    this.enableGesture.setEnabled(true);
                    this.enableGesture.setChecked(java.lang.Boolean.parseBoolean(setting.value));
                    break;
                case 5:
                    updateSeekBar(this.mapZoomLevel, setting.value, MIN_ZOOM_LEVEL, SCALE_FACTOR);
                    break;
                case 6:
                    updateSeekBar(this.mapTilt, setting.value);
                    break;
                case 7:
                    this.enableVideoLoop.setEnabled(true);
                    this.enableVideoLoop.setChecked(java.lang.Boolean.parseBoolean(setting.value));
                    break;
            }
        }
    }

    private void setupSpinner(android.widget.Spinner spinner, java.lang.String setting, java.lang.String value, int optionsArrayId) {
        spinner.setEnabled(true);
        java.util.List<java.lang.String> optionsArray = java.util.Arrays.asList(getResources().getStringArray(optionsArrayId));
        spinner.setOnItemSelectedListener(new com.navdy.client.debug.HudSettingsFragment.SettingSpinnerListener(setting, value));
        android.widget.ArrayAdapter<java.lang.String> adapter = new android.widget.ArrayAdapter<>(getActivity(), 17367048, optionsArray);
        adapter.setDropDownViewResource(17367049);
        spinner.setAdapter(adapter);
        spinner.setSelection(optionsArray.indexOf(value));
    }

    private void updateSeekBar(android.widget.SeekBar seekBar, java.lang.String value) {
        updateSeekBar(seekBar, value, 0.0d, 1.0d);
    }

    private void updateSeekBar(android.widget.SeekBar seekBar, java.lang.String value, double offset, double scaleFactor) {
        seekBar.setEnabled(true);
        seekBar.setProgress((int) ((java.lang.Double.valueOf(value).doubleValue() - offset) * scaleFactor));
    }
}
