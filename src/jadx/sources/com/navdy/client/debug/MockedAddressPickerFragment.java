package com.navdy.client.debug;

public class MockedAddressPickerFragment extends com.navdy.client.debug.RemoteAddressPickerFragment {
    private com.navdy.client.app.framework.AppInstance appInstance;

    static {
        fragmentTitle = com.navdy.client.R.string.title_mocked_position_search;
    }

    public void onCreate(android.os.Bundle savedState) {
        super.onCreate(savedState);
        this.appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
    }

    protected void processSelectedLocation(com.navdy.service.library.events.location.Coordinate location, java.lang.String locationLabel, java.lang.String streetAddress) {
        this.appInstance.showToast("Current location mocked to " + locationLabel, false);
        getFragmentManager().popBackStack();
    }
}
