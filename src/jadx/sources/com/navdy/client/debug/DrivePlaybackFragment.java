package com.navdy.client.debug;

public class DrivePlaybackFragment extends android.app.ListFragment {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.CustomNotificationFragment.class);
    private java.util.List<java.lang.String> mPlaybackList;
    private android.widget.ArrayAdapter<java.lang.String> mPlaybackListAdapter;
    private com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener mRecordingListener = new com.navdy.client.debug.DrivePlaybackFragment.Anon1();
    private final com.navdy.client.app.framework.servicehandler.RecordingServiceManager mRecordingServiceManager = com.navdy.client.app.framework.servicehandler.RecordingServiceManager.getInstance();

    class Anon1 implements com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener {
        Anon1() {
        }

        public void onDriveRecordingsResponse(java.util.List<java.lang.String> recordings) {
            com.navdy.client.debug.DrivePlaybackFragment.this.mPlaybackList.addAll(recordings);
            com.navdy.client.debug.DrivePlaybackFragment.this.mPlaybackListAdapter.notifyDataSetChanged();
        }
    }

    class Anon2 implements android.content.DialogInterface.OnClickListener {
        Anon2() {
        }

        public void onClick(android.content.DialogInterface dialog, int id) {
            dialog.cancel();
        }
    }

    class Anon3 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ java.lang.String val$clickedItem;

        Anon3(java.lang.String str) {
            this.val$clickedItem = str;
        }

        public void onClick(android.content.DialogInterface dialog, int id) {
            com.navdy.client.debug.DrivePlaybackFragment.this.mRecordingServiceManager.sendStartDrivePlaybackEvent(this.val$clickedItem);
        }
    }

    public DrivePlaybackFragment() {
        this.mRecordingServiceManager.addListener(new java.lang.ref.WeakReference(this.mRecordingListener));
    }

    public void onCreate(android.os.Bundle savedState) {
        super.onCreate(savedState);
        this.mPlaybackList = new java.util.ArrayList();
        this.mPlaybackListAdapter = new android.widget.ArrayAdapter<>(getActivity(), 17367062, 16908308, this.mPlaybackList);
        setListAdapter(this.mPlaybackListAdapter);
        this.mRecordingServiceManager.sendDriveRecordingsRequest();
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_test_screen, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        return rootView;
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        java.lang.String clickedItem = (java.lang.String) this.mPlaybackList.get(position);
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(clickedItem).setPositiveButton("Play", new com.navdy.client.debug.DrivePlaybackFragment.Anon3(clickedItem)).setNegativeButton("Cancel", new com.navdy.client.debug.DrivePlaybackFragment.Anon2());
        alertDialogBuilder.create().show();
    }
}
