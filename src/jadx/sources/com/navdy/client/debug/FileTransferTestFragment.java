package com.navdy.client.debug;

public class FileTransferTestFragment extends android.app.Fragment implements android.view.View.OnClickListener {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.FileTransferTestFragment.class);
    private long downloadStartTime;
    private long downloadTotalBytes;
    private android.os.Handler handler = new android.os.Handler();
    @butterknife.InjectView(2131755584)
    android.widget.LinearLayout progressContainer;
    @butterknife.InjectView(2131755579)
    android.widget.RadioGroup sizeSelection;
    @butterknife.InjectView(2131755583)
    android.widget.Button transferButton;
    @butterknife.InjectView(2131755586)
    android.widget.TextView transferMessage;
    @butterknife.InjectView(2131755585)
    android.widget.ProgressBar transferProgress;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.debug.FileTransferTestFragment.this.transferProgress.setProgress((int) com.navdy.client.debug.FileTransferTestFragment.this.downloadTotalBytes);
            long elapsedMs = java.lang.System.currentTimeMillis() - com.navdy.client.debug.FileTransferTestFragment.this.downloadStartTime;
            float rate = ((float) com.navdy.client.debug.FileTransferTestFragment.this.downloadTotalBytes) / ((float) elapsedMs);
            com.navdy.client.debug.FileTransferTestFragment.this.transferMessage.setText(java.lang.String.format(java.util.Locale.getDefault(), "transferred %d bytes in %d ms, %6.2f Kb/sec.", new java.lang.Object[]{java.lang.Long.valueOf(com.navdy.client.debug.FileTransferTestFragment.this.downloadTotalBytes), java.lang.Long.valueOf(elapsedMs), java.lang.Float.valueOf(rate)}));
            com.navdy.client.debug.FileTransferTestFragment.this.transferButton.setEnabled(true);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$error;

        Anon2(java.lang.String str) {
            this.val$error = str;
        }

        public void run() {
            com.navdy.client.debug.FileTransferTestFragment.this.transferMessage.setText(com.navdy.client.debug.FileTransferTestFragment.this.getString(com.navdy.client.R.string.transfer_failed, new java.lang.Object[]{this.val$error}));
            com.navdy.client.debug.FileTransferTestFragment.this.transferButton.setEnabled(true);
        }
    }

    class Anon3 implements com.navdy.client.debug.file.FileTransferManager.FileTransferListener {
        Anon3() {
        }

        public void onFileTransferResponse(com.navdy.service.library.events.file.FileTransferResponse response) {
            if (com.navdy.client.debug.FileTransferTestFragment.this.isAdded() && !response.success.booleanValue()) {
                com.navdy.client.debug.FileTransferTestFragment.sLogger.e("error in file transfer: " + response);
            }
        }

        public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus status) {
            if (com.navdy.client.debug.FileTransferTestFragment.this.isAdded()) {
                if (!status.success.booleanValue()) {
                    com.navdy.client.debug.FileTransferTestFragment.this.recordTransferFailed(status.error.toString());
                } else if (status.transferComplete.booleanValue()) {
                    com.navdy.client.debug.FileTransferTestFragment.this.recordTransferComplete();
                } else {
                    com.navdy.client.debug.FileTransferTestFragment.this.transferProgress.setProgress(status.totalBytesTransferred.intValue());
                }
            }
        }

        public void onError(com.navdy.service.library.events.file.FileTransferError errorCode, java.lang.String error) {
            if (com.navdy.client.debug.FileTransferTestFragment.this.isAdded()) {
                com.navdy.client.debug.FileTransferTestFragment.sLogger.e("Transfer Error :" + error);
                com.navdy.client.debug.FileTransferTestFragment.this.recordTransferFailed(error);
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.debug.file.RemoteFileTransferManager val$fileTransferManager;

        Anon4(com.navdy.client.debug.file.RemoteFileTransferManager remoteFileTransferManager) {
            this.val$fileTransferManager = remoteFileTransferManager;
        }

        public void run() {
            this.val$fileTransferManager.sendFile();
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(android.net.Uri uri);
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sLogger.w("onCreate()");
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        sLogger.w("onCreateView()");
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_file_transfer_test, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.transferButton.setOnClickListener(this);
        return rootView;
    }

    private int getSelectedSize() {
        int sel = this.sizeSelection.getCheckedRadioButtonId();
        switch (sel) {
            case com.navdy.client.R.id.transfer_size1mb /*2131755580*/:
                return 1;
            case com.navdy.client.R.id.transfer_size10mb /*2131755581*/:
                return 10;
            case com.navdy.client.R.id.transfer_size100mb /*2131755582*/:
                return 100;
            default:
                sLogger.e(java.lang.String.format("unexpected file transfer size selection %x", new java.lang.Object[]{java.lang.Integer.valueOf(sel)}));
                return 1;
        }
    }

    private void recordTransferComplete() {
        this.handler.post(new com.navdy.client.debug.FileTransferTestFragment.Anon1());
    }

    private void recordTransferFailed(java.lang.String error) {
        this.handler.post(new com.navdy.client.debug.FileTransferTestFragment.Anon2(error));
    }

    public void onClick(android.view.View v) {
        this.downloadTotalBytes = ((long) (getSelectedSize() * 1024)) * 1024;
        this.transferProgress.setMax((int) this.downloadTotalBytes);
        this.transferProgress.setProgress(0);
        this.transferMessage.setText(com.navdy.client.R.string.transfer_progress);
        this.progressContainer.setVisibility(View.VISIBLE);
        this.transferButton.setEnabled(false);
        this.downloadStartTime = java.lang.System.currentTimeMillis();
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.debug.FileTransferTestFragment.Anon4(new com.navdy.client.debug.file.RemoteFileTransferManager(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.service.library.events.file.FileType.FILE_TYPE_PERF_TEST, ((long) (getSelectedSize() * 1024)) * 1024, new com.navdy.client.debug.FileTransferTestFragment.Anon3())), 2);
    }
}
