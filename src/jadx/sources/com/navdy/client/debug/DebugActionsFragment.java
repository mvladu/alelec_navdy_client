package com.navdy.client.debug;

public class DebugActionsFragment extends android.app.ListFragment {
    static final java.lang.String[] RANDOM_STREETS = {"Divisadero St.", "Embarcadero St.", "MacArthur Blvd", "El Camino Real", "Cesar Chavez St.", "287 east alviso / milpitas"};
    private static final long RETRY_INTERVAL_FOR_SUPPORT_TICKETS = java.util.concurrent.TimeUnit.MINUTES.toMillis(1);
    static final com.navdy.service.library.events.ui.Screen[] SCREENS = com.navdy.service.library.events.ui.Screen.values();
    private static final int SIM_SPEED_CHANGE_STEP = 1;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("TestActivity");
    static final android.util.SparseIntArray screenMap = new android.util.SparseIntArray();
    private static int simulationSpeed = 18;
    protected com.navdy.client.app.framework.AppInstance mAppInstance;
    protected com.navdy.client.debug.ConnectionStatusUpdater mConnectionStatus;
    protected int mLastDirectionIdx = 0;
    protected java.util.ArrayList<com.navdy.client.debug.DebugActionsFragment.ListItem> mListItems;
    @butterknife.InjectView(2131755574)
    android.widget.TextView mTextViewCurrentSimSpeed;

    private enum ListItem {
        DISCONNECT("Disconnect"),
        SCREEN_TESTING("Screen testing"),
        SEND_NAVIGATION_EVENT("Send navigation event"),
        BROWSE_VIDEOS("Browse Videos"),
        DUMMY_EVENT0(" "),
        HERE_NAVIGATION("Here Nav Test"),
        SEND_GOOGLE_NOW_EVENT("Send google now notification event"),
        DUMMY_EVENT1(" "),
        INCREASE_SIM_SPEED("Increase simulation speed +1"),
        DECREASE_SIM_SPEED("Decrease simulation speed -1"),
        MOCK_CURRENT_POSITION("Mock current GPS position"),
        RESET_CURRENT_POSITION("Reset current GPS position"),
        NOTIFICATION_TESTING("Notification Testing"),
        SHOW_OPTIONS_VIEW("Show Options View"),
        HIDE_OPTIONS_VIEW("Hide Options View"),
        SHOW_CONTEXT_MENU("Show Context Menu"),
        HIDE_CONTEXT_MENU("Hide Context Menu"),
        OTA_SEND_LARGE_FILE("Send Large File (OTA test)"),
        DRIVE_PLAYBACK("Drive Playback"),
        STOP_DRIVE_PLAYBACK("Stop Drive Playback"),
        FILE_TRANSFER_PERF_TEST("File Transfer Performance Test"),
        ENABLE_EXPERIMENTAL_FEATURE_MODE("Enable Experimental Feature Mode"),
        SEND_HEAD_UNIT_CONNECT("Send head unit connection event"),
        SEND_HEAD_UNIT_DISCONNECT("Send head unit disconnect event"),
        SET_RETRY_TIME_FOR_SUPPORT_TICKETS("Reschedule Support Ticket Alarm To 1 Min"),
        ENABLE_ZENDESK_LOGGING("Enables Zendesk SDK Logger"),
        NAV_CORD_TEST_SUITE("Navigation Coord Test Suite");
        
        private final java.lang.String mText;

        private ListItem(java.lang.String item) {
            this.mText = item;
        }

        public java.lang.String toString() {
            return this.mText;
        }
    }

    static {
        screenMap.put(com.navdy.client.R.id.dash_screen, com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD.ordinal());
        screenMap.put(com.navdy.client.R.id.hybrid_map_screen, com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP.ordinal());
        screenMap.put(com.navdy.client.R.id.context_menu_screen, com.navdy.service.library.events.ui.Screen.SCREEN_CONTEXT_MENU.ordinal());
        screenMap.put(com.navdy.client.R.id.menu_screen, com.navdy.service.library.events.ui.Screen.SCREEN_MENU.ordinal());
    }

    public void onCreate(android.os.Bundle savedState) {
        super.onCreate(savedState);
        this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        this.mListItems = new java.util.ArrayList<>(java.util.Arrays.asList(com.navdy.client.debug.DebugActionsFragment.ListItem.values()));
        setListAdapter(new android.widget.ArrayAdapter(getActivity(), 17367062, 16908308, com.navdy.client.debug.DebugActionsFragment.ListItem.values()));
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_debug_actions, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.mConnectionStatus = new com.navdy.client.debug.ConnectionStatusUpdater(rootView);
        return rootView;
    }

    public void onPause() {
        this.mConnectionStatus.onPause();
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        getListView().setChoiceMode(2);
        this.mConnectionStatus.onResume();
        refreshUI();
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        switch ((com.navdy.client.debug.DebugActionsFragment.ListItem) this.mListItems.get(position)) {
            case DISCONNECT:
                sendEvent(new com.navdy.service.library.events.connection.DisconnectRequest(java.lang.Boolean.valueOf(true)));
                return;
            case SEND_NAVIGATION_EVENT:
                sendNavigationEvent();
                return;
            case HERE_NAVIGATION:
                onHereNavigationClick();
                return;
            case SCREEN_TESTING:
                onScreenTestingClick();
                return;
            case BROWSE_VIDEOS:
                onBrowseVideosClicked();
                return;
            case INCREASE_SIM_SPEED:
                onSimSpeedChangeClick(1);
                return;
            case DECREASE_SIM_SPEED:
                onSimSpeedChangeClick(-1);
                return;
            case SEND_GOOGLE_NOW_EVENT:
                sendGoogleNowEvent();
                return;
            case NOTIFICATION_TESTING:
                onCustomNotificationClick();
                return;
            case SHOW_OPTIONS_VIEW:
                onShowOpionsView();
                return;
            case HIDE_OPTIONS_VIEW:
                onHideOptionsView();
                return;
            case SHOW_CONTEXT_MENU:
                onShowContextMenu();
                return;
            case HIDE_CONTEXT_MENU:
                onHideContextMenu();
                return;
            case OTA_SEND_LARGE_FILE:
                onOTASendLargeFile();
                return;
            case MOCK_CURRENT_POSITION:
                com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), com.navdy.client.debug.MockedAddressPickerFragment.class);
                return;
            case RESET_CURRENT_POSITION:
                return;
            case DRIVE_PLAYBACK:
                onShowDrivePlayback();
                return;
            case STOP_DRIVE_PLAYBACK:
                onStopDrivePlayback();
                return;
            case FILE_TRANSFER_PERF_TEST:
                onFileTransferTest();
                return;
            case ENABLE_EXPERIMENTAL_FEATURE_MODE:
                com.navdy.client.app.ui.settings.SettingsUtils.setFeatureMode(com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode.FEATURE_MODE_EXPERIMENTAL);
                return;
            case SEND_HEAD_UNIT_CONNECT:
                sendAudioStatusEvent(true);
                return;
            case SEND_HEAD_UNIT_DISCONNECT:
                sendAudioStatusEvent(false);
                return;
            case SET_RETRY_TIME_FOR_SUPPORT_TICKETS:
                com.navdy.client.app.framework.util.SupportTicketService.scheduleAlarmForSubmittingPendingTickets(RETRY_INTERVAL_FOR_SUPPORT_TICKETS);
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Scheduling alarm to submit tickets in 1 minutes", 0).show();
                return;
            case ENABLE_ZENDESK_LOGGING:
                com.zendesk.logger.Logger.setLoggable(true);
                android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Zendesk SDK Logger enabled", 0).show();
                return;
            case NAV_CORD_TEST_SUITE:
                startActivity(new android.content.Intent(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.debug.NavCoordTestSuiteActivity.class));
                return;
            default:
                sLogger.d("unhandled: " + id);
                return;
        }
    }

    protected void sendNotificationEvent() {
        sendEvent(getRandomNotificationEvent());
    }

    protected void sendNavigationEvent() {
        sendEvent(getRandomNavigationEvent());
    }

    protected void sendGoogleNowEvent() {
        sendEvent(getRandomGoogleNowEvent());
    }

    public void onSendDismissClick() {
        sendEvent(new com.navdy.service.library.events.ui.DismissScreen(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION));
    }

    public void onCustomNotificationClick() {
        sLogger.v("onCustomNotificationClick");
        pushFullScreenFragment(com.navdy.client.debug.CustomNotificationFragment.class);
    }

    public void onScreenTestingClick() {
        sLogger.v("onScreenTestingClick");
        pushFullScreenFragment(com.navdy.client.debug.ScreenTestingFragment.class);
    }

    public void onShowOpionsView() {
        sLogger.v("onShowOptionsView");
        sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_OPTIONS).build());
    }

    public void onHideOptionsView() {
        sLogger.v("onHideOptionsView");
        sendEvent(new com.navdy.service.library.events.ui.DismissScreen(com.navdy.service.library.events.ui.Screen.SCREEN_OPTIONS));
    }

    public void onShowContextMenu() {
        sLogger.v("onShowContextMenu");
        sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_CONTEXT_MENU).build());
    }

    public void onHideContextMenu() {
        sLogger.v("onHideContextMenu");
        sendEvent(new com.navdy.service.library.events.ui.DismissScreen(com.navdy.service.library.events.ui.Screen.SCREEN_CONTEXT_MENU));
    }

    public void sendAudioStatusEvent(boolean connected) {
        sendEvent(new com.navdy.service.library.events.audio.AudioStatus.Builder().isConnected(java.lang.Boolean.valueOf(connected)).build());
    }

    private void onOTASendLargeFile() {
        pushFullScreenFragment(com.navdy.client.debug.view.OTAS3BrowserFragment.class);
    }

    public void onBrowseVideosClicked() {
        pushFullScreenFragment(com.navdy.client.debug.view.VideoS3BrowserFragment.class);
    }

    public void onHereNavigationClick() {
        pushFullScreenFragment(com.navdy.client.debug.navdebug.NavAddressPickerFragment.class);
    }

    public void onSimSpeedChangeClick(int change) {
        changeSimulationSpeed(change);
        updateSimSpeed();
    }

    public void onShowDrivePlayback() {
        sLogger.v("onShowDrivePlayback");
        pushFullScreenFragment(com.navdy.client.debug.DrivePlaybackFragment.class);
    }

    public void onStopDrivePlayback() {
        com.navdy.client.app.framework.servicehandler.RecordingServiceManager.getInstance().sendStopDrivePlaybackEvent();
    }

    public void onFileTransferTest() {
        sLogger.v("onFileTransferTest");
        pushFullScreenFragment(com.navdy.client.debug.FileTransferTestFragment.class);
    }

    protected void pushFullScreenFragment(java.lang.Class<? extends android.app.Fragment> fragmentClass) {
        com.navdy.client.debug.util.FragmentHelper.pushFullScreenFragment(getFragmentManager(), fragmentClass);
    }

    private com.navdy.service.library.events.ui.Screen lookupScreen(int id) {
        return SCREENS[screenMap.get(id)];
    }

    @butterknife.OnClick({2131755570, 2131755573, 2131755572, 2131755571})
    public void onShowScreenClicked(android.view.View button) {
        sendEvent(new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(lookupScreen(button.getId())).build());
    }

    protected void refreshUI() {
        updateSimSpeed();
    }

    private void updateSimSpeed() {
        this.mTextViewCurrentSimSpeed.setText("Simulation speed: " + getSimulationSpeed() + " m/s " + org.droidparts.contract.SQL.DDL.SEPARATOR + ((int) (((float) getSimulationSpeed()) * 2.2369f)) + " MPH");
    }

    protected com.navdy.service.library.events.notification.NotificationEvent getRandomNotificationEvent() {
        java.lang.String phoneNumber = "408-393-6782";
        return new com.navdy.service.library.events.notification.NotificationEvent(java.lang.Integer.valueOf(1), com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_INCOMING_CALL, phoneNumber, null, "Are you on the way?", null, null, null, null, null, phoneNumber, java.lang.Boolean.valueOf(true), null);
    }

    protected com.navdy.service.library.events.notification.NotificationEvent getRandomGoogleNowEvent() {
        return new com.navdy.service.library.events.notification.NotificationEvent(java.lang.Integer.valueOf(1), com.navdy.service.library.events.notification.NotificationCategory.CATEGORY_OTHER, null, null, "AskNavdy", null, null, null, null, null, null, java.lang.Boolean.valueOf(true), null);
    }

    private void sendEvent(com.squareup.wire.Message event) {
        com.navdy.service.library.device.RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(event);
        }
    }

    protected com.navdy.service.library.events.navigation.NavigationManeuverEvent getRandomNavigationEvent() {
        int randomStreetIndex = new java.util.Random().nextInt(RANDOM_STREETS.length);
        this.mLastDirectionIdx = (this.mLastDirectionIdx + 1) % com.navdy.service.library.events.navigation.NavigationTurn.values().length;
        return new com.navdy.service.library.events.navigation.NavigationManeuverEvent("current road", com.navdy.service.library.events.navigation.NavigationTurn.values()[this.mLastDirectionIdx], "1 mi", RANDOM_STREETS[randomStreetIndex], "1:10", "10 mph", null, java.lang.Long.valueOf(0));
    }

    public static int getSimulationSpeed() {
        return simulationSpeed;
    }

    private static void changeSimulationSpeed(int diff) {
        simulationSpeed += diff;
        if (simulationSpeed <= 0) {
            sLogger.w("Trying to set simulation speed to value less than or equal to 0 (reset to 1): " + simulationSpeed);
            simulationSpeed = 1;
        }
    }
}
