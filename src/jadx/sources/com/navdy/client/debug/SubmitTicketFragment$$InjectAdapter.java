package com.navdy.client.debug;

public final class SubmitTicketFragment$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.debug.SubmitTicketFragment> implements javax.inject.Provider<com.navdy.client.debug.SubmitTicketFragment>, dagger.MembersInjector<com.navdy.client.debug.SubmitTicketFragment> {
    private dagger.internal.Binding<com.navdy.service.library.network.http.IHttpManager> mHttpManager;
    private dagger.internal.Binding<com.navdy.client.app.ui.base.BaseFragment> supertype;

    public SubmitTicketFragment$$InjectAdapter() {
        super("com.navdy.client.debug.SubmitTicketFragment", "members/com.navdy.client.debug.SubmitTicketFragment", false, com.navdy.client.debug.SubmitTicketFragment.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.client.debug.SubmitTicketFragment.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseFragment", com.navdy.client.debug.SubmitTicketFragment.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.client.debug.SubmitTicketFragment get() {
        com.navdy.client.debug.SubmitTicketFragment result = new com.navdy.client.debug.SubmitTicketFragment();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.debug.SubmitTicketFragment object) {
        object.mHttpManager = (com.navdy.service.library.network.http.IHttpManager) this.mHttpManager.get();
        this.supertype.injectMembers(object);
    }
}
