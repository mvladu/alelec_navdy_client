package com.navdy.client.debug;

public class DestinationAddressPickerFragment extends com.navdy.client.debug.RemoteAddressPickerFragment {

    public interface DestinationListener {
        void setDestination(java.lang.String str, com.navdy.service.library.events.location.Coordinate coordinate);
    }

    protected void processSelectedLocation(com.navdy.service.library.events.location.Coordinate location, java.lang.String locationLabel, java.lang.String streetAddress) {
        if (getActivity() instanceof com.navdy.client.debug.DestinationAddressPickerFragment.DestinationListener) {
            ((com.navdy.client.debug.DestinationAddressPickerFragment.DestinationListener) getActivity()).setDestination(locationLabel, location);
        }
        getFragmentManager().popBackStack();
    }
}
