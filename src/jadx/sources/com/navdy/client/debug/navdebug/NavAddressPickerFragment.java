package com.navdy.client.debug.navdebug;

public class NavAddressPickerFragment extends android.app.ListFragment implements com.here.android.mpa.common.OnEngineInitListener, com.here.android.mpa.common.PositioningManager.OnPositionChangedListener {
    private static final double MI_TO_METERS = 1690.34d;
    private static final int SEARCH_RADIUS_METERS = 169034;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.navdebug.NavAddressPickerFragment.class);
    protected com.navdy.client.app.framework.AppInstance mAppInstance;
    protected com.here.android.mpa.common.GeoPosition mCurrentGeoPosition;
    protected java.util.ArrayList<com.here.android.mpa.common.GeoCoordinate> mDestinationCoordinates;
    protected java.util.ArrayList<java.lang.String> mDestinationLabels;
    @butterknife.InjectView(2131755632)
    org.droidparts.widget.ClearableEditText mEditTextDestinationQuery;
    protected com.here.android.mpa.search.GeocodeRequest mLastGeocodeRequest;
    protected com.here.android.mpa.search.SearchRequest mLastSearchRequest;
    protected android.os.Handler mMainHandler;
    private boolean mMapEngineAvailable = false;
    @butterknife.InjectView(2131755629)
    android.widget.RadioGroup mSearchTypeRadioGroup;

    class Anon1 implements org.droidparts.widget.ClearableEditText.Listener {
        Anon1() {
        }

        public void didClearText() {
            com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showKeyboard();
        }
    }

    class Anon2 implements android.widget.RadioGroup.OnCheckedChangeListener {
        Anon2() {
        }

        public void onCheckedChanged(android.widget.RadioGroup radioGroup, int i) {
            com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.onQueryTypeCheckedChanged(radioGroup, i);
        }
    }

    class Anon3 implements com.here.android.mpa.common.OnEngineInitListener {
        final /* synthetic */ java.lang.String val$queryText;

        class Anon1 implements com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.DiscoveryResultPage> {

            /* renamed from: com.navdy.client.debug.navdebug.NavAddressPickerFragment$Anon3$Anon1$Anon1 reason: collision with other inner class name */
            class C0075Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.here.android.mpa.search.DiscoveryResultPage val$data;
                final /* synthetic */ com.here.android.mpa.search.ErrorCode val$errorCode;

                C0075Anon1(com.here.android.mpa.search.ErrorCode errorCode, com.here.android.mpa.search.DiscoveryResultPage discoveryResultPage) {
                    this.val$errorCode = errorCode;
                    this.val$data = discoveryResultPage;
                }

                public void run() {
                    if (this.val$errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError("Search error: " + this.val$errorCode.toString());
                        return;
                    }
                    java.util.List<com.here.android.mpa.search.PlaceLink> places = this.val$data.getPlaceLinks();
                    if (places == null || places.size() <= 0) {
                        java.lang.String str = "No results";
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError(str);
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.sLogger.d(str);
                        return;
                    }
                    com.navdy.client.debug.navdebug.NavAddressPickerFragment.sLogger.d("returned results: " + places.size());
                    com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.addPlaces(places);
                }
            }

            Anon1() {
            }

            public void onCompleted(com.here.android.mpa.search.DiscoveryResultPage data, com.here.android.mpa.search.ErrorCode errorCode) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mMainHandler.post(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon3.Anon1.C0075Anon1(errorCode, data));
            }
        }

        Anon3(java.lang.String str) {
            this.val$queryText = str;
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            if (com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastSearchRequest != null) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastSearchRequest.cancel();
            }
            com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastSearchRequest = new com.here.android.mpa.search.SearchRequest(this.val$queryText).setSearchCenter(com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mCurrentGeoPosition.getCoordinate());
            if (com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastSearchRequest == null) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError("Unable to search.");
                return;
            }
            com.here.android.mpa.search.ErrorCode errorCode = com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastSearchRequest.execute(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon3.Anon1());
            if (errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError("Search error: " + errorCode.toString());
            }
        }
    }

    class Anon4 implements com.here.android.mpa.common.OnEngineInitListener {
        final /* synthetic */ java.lang.String val$queryText;

        class Anon1 implements com.here.android.mpa.search.ResultListener<java.util.List<com.here.android.mpa.search.Location>> {

            /* renamed from: com.navdy.client.debug.navdebug.NavAddressPickerFragment$Anon4$Anon1$Anon1 reason: collision with other inner class name */
            class C0076Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.here.android.mpa.search.ErrorCode val$errorCode;
                final /* synthetic */ java.util.List val$locations;

                C0076Anon1(com.here.android.mpa.search.ErrorCode errorCode, java.util.List list) {
                    this.val$errorCode = errorCode;
                    this.val$locations = list;
                }

                public void run() {
                    if (this.val$errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError("Search error: " + this.val$errorCode.toString());
                    } else if (this.val$locations == null || this.val$locations.size() <= 0) {
                        java.lang.String str = "No results";
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError(str);
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.sLogger.d(str);
                    } else {
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.sLogger.d("returned results: " + this.val$locations.size());
                        com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.addDestinations(this.val$locations);
                    }
                }
            }

            Anon1() {
            }

            public void onCompleted(java.util.List<com.here.android.mpa.search.Location> locations, com.here.android.mpa.search.ErrorCode errorCode) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mMainHandler.post(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon4.Anon1.C0076Anon1(errorCode, locations));
            }
        }

        Anon4(java.lang.String str) {
            this.val$queryText = str;
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            if (com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastGeocodeRequest != null) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastGeocodeRequest.cancel();
            }
            com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastGeocodeRequest = new com.here.android.mpa.search.GeocodeRequest(this.val$queryText).setSearchArea(com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mCurrentGeoPosition.getCoordinate(), com.navdy.client.debug.navdebug.NavAddressPickerFragment.SEARCH_RADIUS_METERS);
            if (com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastGeocodeRequest == null) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError("Unable to search.");
                return;
            }
            com.here.android.mpa.search.ErrorCode errorCode = com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.mLastGeocodeRequest.execute(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon4.Anon1());
            if (errorCode != com.here.android.mpa.search.ErrorCode.NONE) {
                com.navdy.client.debug.navdebug.NavAddressPickerFragment.this.showError("Search error: " + errorCode.toString());
            }
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.here.android.mpa.common.MapEngine.getInstance().init(getActivity(), this);
        com.here.android.mpa.common.MapEngine.getInstance().init(getActivity().getApplicationContext(), this);
        this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        this.mDestinationLabels = new java.util.ArrayList<>();
        this.mDestinationCoordinates = new java.util.ArrayList<>();
        setListAdapter(new android.widget.ArrayAdapter(getActivity(), 17367043, 16908308, this.mDestinationLabels));
        this.mMainHandler = new android.os.Handler();
    }

    public void onCreateContextMenu(android.view.ContextMenu menu, android.view.View v, android.view.ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(com.navdy.client.R.menu.local_nav_actions, menu);
    }

    public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
        if (error == com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
            sLogger.e("MapEngine initialized.");
            initializePosition();
            return;
        }
        sLogger.e("MapEngine init completed with error: " + error.toString());
    }

    public void initializePosition() {
        com.here.android.mpa.common.PositioningManager positioningManager = com.here.android.mpa.common.PositioningManager.getInstance();
        positioningManager.addListener(new java.lang.ref.WeakReference(this));
        if (!positioningManager.isActive()) {
            positioningManager.start(com.here.android.mpa.common.PositioningManager.LocationMethod.GPS_NETWORK);
        }
    }

    public void onPositionUpdated(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.GeoPosition geoPosition, boolean b) {
        sLogger.d("onPositionUpdated method: " + locationMethod + " position: " + geoPosition.getCoordinate().toString());
        this.mCurrentGeoPosition = geoPosition;
        this.mMapEngineAvailable = true;
    }

    public void onPositionFixChanged(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.PositioningManager.LocationStatus locationStatus) {
        sLogger.d("onPositionFixChanged: method:" + locationMethod + " status: " + locationStatus);
    }

    protected void addDestinations(java.util.List<com.here.android.mpa.search.Location> destinationLocations) {
        this.mDestinationLabels.clear();
        this.mDestinationCoordinates.clear();
        for (com.here.android.mpa.search.Location destinationLocation : destinationLocations) {
            this.mDestinationLabels.add(getLabelForLocation(destinationLocation));
            this.mDestinationCoordinates.add(getCoordinateForLocation(destinationLocation));
        }
        ((android.widget.ArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected void addPlaces(java.util.List<com.here.android.mpa.search.PlaceLink> placeLocations) {
        this.mDestinationLabels.clear();
        this.mDestinationCoordinates.clear();
        for (com.here.android.mpa.search.PlaceLink placeLink : placeLocations) {
            this.mDestinationLabels.add(placeLink.getTitle());
            this.mDestinationCoordinates.add(placeLink.getPosition());
        }
        ((android.widget.ArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected java.lang.String getLabelForLocation(com.here.android.mpa.search.Location location) {
        java.lang.String label;
        if (location.getAddress() == null) {
            label = location.toString();
        } else {
            com.here.android.mpa.search.Address address = location.getAddress();
            java.lang.String text = address.getText();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text)) {
                return text;
            }
            java.lang.String number = address.getHouseNumber();
            java.lang.String street = address.getStreet();
            java.lang.String city = address.getCity();
            java.lang.String state = address.getState();
            java.lang.String streetAddress = java.lang.String.format("%s %s", new java.lang.Object[]{emptyIfNull(number), emptyIfNull(street)}).trim();
            java.lang.String cityState = java.lang.String.format("%s %s", new java.lang.Object[]{emptyIfNull(city), emptyIfNull(state)}).trim();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(streetAddress) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(cityState)) {
                label = java.lang.String.format("%s, %s", new java.lang.Object[]{streetAddress, cityState});
            } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(streetAddress)) {
                label = streetAddress;
            } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(cityState)) {
                label = cityState;
            } else {
                label = "(unknown address)";
            }
        }
        return label;
    }

    protected java.lang.String emptyIfNull(java.lang.String s) {
        return s == null ? "" : s;
    }

    public android.view.View onCreateView(@android.support.annotation.NonNull android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_nav_address_picker_demo, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.mEditTextDestinationQuery.setListener(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon1());
        return rootView;
    }

    public void onViewCreated(android.view.View view, android.os.Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerForContextMenu(getListView());
        this.mSearchTypeRadioGroup.setOnCheckedChangeListener(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon2());
    }

    public void onResume() {
        super.onResume();
        refreshUI();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void refreshUI() {
        if (this.mDestinationLabels.size() == 0) {
            showKeyboard();
        }
    }

    public void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
        l.showContextMenuForChild(v);
    }

    public boolean onContextItemSelected(android.view.MenuItem item) {
        com.here.android.mpa.common.GeoCoordinate destinationCoord = (com.here.android.mpa.common.GeoCoordinate) this.mDestinationCoordinates.get(((android.widget.AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
        if (destinationCoord == null) {
            showError("Unable to find coordinate.");
            return true;
        }
        hideKeyboard();
        switch (item.getItemId()) {
            case com.navdy.client.R.id.local_nav_route_info /*2131756062*/:
                startRouteBrowser(this.mCurrentGeoPosition.getCoordinate(), destinationCoord);
                return true;
            case com.navdy.client.R.id.local_nav_navigate /*2131756063*/:
                startNavigation(this.mCurrentGeoPosition.getCoordinate(), destinationCoord);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    protected com.here.android.mpa.common.GeoCoordinate getCoordinateForLocation(com.here.android.mpa.search.Location destinationLocation) {
        com.here.android.mpa.common.GeoCoordinate destinationCoord = null;
        com.here.android.mpa.search.NavigationPosition navigationPosition = null;
        if (!(destinationLocation == null || destinationLocation.getAccessPoints() == null || destinationLocation.getAccessPoints().size() <= 0)) {
            navigationPosition = (com.here.android.mpa.search.NavigationPosition) destinationLocation.getAccessPoints().get(0);
        }
        if (navigationPosition != null) {
            destinationCoord = navigationPosition.getCoordinate();
        }
        if (destinationCoord == null) {
            destinationCoord = destinationLocation.getCoordinate();
        }
        if (destinationCoord != null) {
            sLogger.d("text: " + destinationLocation.getAddress().getText() + " pos: " + destinationCoord.toString());
        } else {
            sLogger.d("couldn't build coordinate");
        }
        return destinationCoord;
    }

    public void startNavigation(com.here.android.mpa.common.GeoCoordinate start, com.here.android.mpa.common.GeoCoordinate end) {
        startActivity(com.navdy.client.debug.navdebug.NavigationDemoActivity.createIntentWithCoords(getActivity(), start, end));
    }

    public void startRouteBrowser(com.here.android.mpa.common.GeoCoordinate start, com.here.android.mpa.common.GeoCoordinate end) {
        com.navdy.client.debug.navdebug.NavRouteBrowserFragment routeBrowserFragment = com.navdy.client.debug.navdebug.NavRouteBrowserFragment.newInstance(start, end);
        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(com.navdy.client.R.id.container, routeBrowserFragment);
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }

    @butterknife.OnTextChanged({2131755632})
    public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) {
        doSearch(s.toString());
    }

    public void onQueryTypeCheckedChanged(android.widget.RadioGroup group, int checkedid) {
        doSearch(this.mEditTextDestinationQuery.getText().toString());
    }

    protected void doSearch(java.lang.String queryText) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(queryText)) {
            if (!this.mMapEngineAvailable) {
                showError("Map engine not ready.");
            } else if (this.mCurrentGeoPosition == null) {
                showError("No current position.");
            } else if (this.mSearchTypeRadioGroup.getCheckedRadioButtonId() == com.navdy.client.R.id.radioaddresses) {
                doAddressSearch(queryText);
            } else {
                doPlaceSearch(queryText);
            }
        }
    }

    protected void doPlaceSearch(java.lang.String queryText) {
        com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon3(queryText));
    }

    protected void doAddressSearch(java.lang.String queryText) {
        com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.debug.navdebug.NavAddressPickerFragment.Anon4(queryText));
    }

    protected void hideKeyboard() {
        ((android.view.inputmethod.InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.mEditTextDestinationQuery.getWindowToken(), 0);
    }

    protected void showKeyboard() {
        ((android.view.inputmethod.InputMethodManager) getActivity().getSystemService("input_method")).showSoftInput(this.mEditTextDestinationQuery, 1);
    }

    protected void showError(java.lang.String error) {
        android.widget.Toast.makeText(getActivity(), error, 0).show();
    }
}
