package com.navdy.client.debug.navdebug;

public class NavAddressPickerFragment$$ViewInjector {

    /* compiled from: NavAddressPickerFragment$$ViewInjector */
    static class Anon1 implements android.text.TextWatcher {
        final /* synthetic */ com.navdy.client.debug.navdebug.NavAddressPickerFragment val$target;

        Anon1(com.navdy.client.debug.navdebug.NavAddressPickerFragment navAddressPickerFragment) {
            this.val$target = navAddressPickerFragment;
        }

        public void onTextChanged(java.lang.CharSequence p0, int p1, int p2, int p3) {
            this.val$target.onTextChanged(p0, p1, p2, p3);
        }

        public void beforeTextChanged(java.lang.CharSequence p0, int p1, int p2, int p3) {
        }

        public void afterTextChanged(android.text.Editable p0) {
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.navdebug.NavAddressPickerFragment target, java.lang.Object source) {
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.nav_picker_edittext_destination_demo, "field 'mEditTextDestinationQuery' and method 'onTextChanged'");
        target.mEditTextDestinationQuery = (org.droidparts.widget.ClearableEditText) view;
        ((android.widget.TextView) view).addTextChangedListener(new com.navdy.client.debug.navdebug.NavAddressPickerFragment$$ViewInjector.Anon1(target));
        target.mSearchTypeRadioGroup = (android.widget.RadioGroup) finder.findRequiredView(source, com.navdy.client.R.id.search_type_radio_group, "field 'mSearchTypeRadioGroup'");
    }

    public static void reset(com.navdy.client.debug.navdebug.NavAddressPickerFragment target) {
        target.mEditTextDestinationQuery = null;
        target.mSearchTypeRadioGroup = null;
    }
}
