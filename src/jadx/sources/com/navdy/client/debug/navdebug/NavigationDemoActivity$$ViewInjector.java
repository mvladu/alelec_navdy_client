package com.navdy.client.debug.navdebug;

public class NavigationDemoActivity$$ViewInjector {

    /* compiled from: NavigationDemoActivity$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.debug.navdebug.NavigationDemoActivity val$target;

        Anon1(com.navdy.client.debug.navdebug.NavigationDemoActivity navigationDemoActivity) {
            this.val$target = navigationDemoActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onToggleClicked();
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.navdebug.NavigationDemoActivity target, java.lang.Object source) {
        target.laneInfo = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.lane_info, "field 'laneInfo'");
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.button_toggle_navigation, "field 'toggleButton' and method 'onToggleClicked'");
        target.toggleButton = (android.widget.Button) view;
        view.setOnClickListener(new com.navdy.client.debug.navdebug.NavigationDemoActivity$$ViewInjector.Anon1(target));
        target.maneuverDetailListView = (android.widget.ListView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_detail_list, "field 'maneuverDetailListView'");
    }

    public static void reset(com.navdy.client.debug.navdebug.NavigationDemoActivity target) {
        target.laneInfo = null;
        target.toggleButton = null;
        target.maneuverDetailListView = null;
    }
}
