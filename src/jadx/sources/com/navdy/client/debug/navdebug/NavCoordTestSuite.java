package com.navdy.client.debug.navdebug;

public class NavCoordTestSuite {
    private static final int ON_SCREEN_LOG_MAX_LENGTH = 5000;
    private static com.navdy.client.debug.navdebug.NavCoordTestSuite instance;
    private final java.lang.String GOOGLE_WEB_SERVICE_KEY;
    private final java.lang.String OUTPUT_FILE;
    private java.util.HashMap<com.navdy.client.debug.navdebug.NavCoordTestSuite.Step, android.util.Pair<com.navdy.service.library.events.location.Coordinate, com.navdy.service.library.events.location.Coordinate>> coords;
    private com.navdy.client.app.framework.models.Destination currentDestination;
    private int currentQueryIndex;
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.navdebug.NavCoordTestSuite.class);
    private android.os.Handler mainThreadHandler;
    private java.io.OutputStreamWriter output;
    private final java.util.ArrayList<java.lang.String> queries = new java.util.ArrayList<>();
    private boolean singleTest = false;
    private java.lang.CharSequence text;
    private android.widget.TextView textView;
    private android.widget.Toast toast;

    class Anon1 implements com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener {
        Anon1() {
        }

        public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleResults, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
            if (queryType == com.navdy.client.app.framework.search.GooglePlacesSearch.Query.TEXTSEARCH) {
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"google_text_search_results\":" + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.googleSearchResultsToJsonArray(googleResults) + ",\n");
                if (googleResults == null || googleResults.isEmpty()) {
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.runNextTest();
                    return;
                }
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  number of results: " + googleResults.size());
                java.util.ArrayList destinationListFromGoogleTextSearchResults = com.navdy.client.app.framework.search.SearchResults.getDestinationListFromGoogleTextSearchResults(googleResults);
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"google_search_result_destinations\":" + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.destinationsToJsonArray(destinationListFromGoogleTextSearchResults) + ",\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination = null;
                if (destinationListFromGoogleTextSearchResults != null && !destinationListFromGoogleTextSearchResults.isEmpty()) {
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination = (com.navdy.client.app.framework.models.Destination) destinationListFromGoogleTextSearchResults.get(0);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"selected_destination\":" + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.toJsonString() + ",\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Selected currentDestination: name[" + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.name + "]" + " address[" + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.rawAddressNotForDisplay + "]");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Display coords:    " + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.displayLat + org.droidparts.contract.SQL.DDL.SEPARATOR + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.displayLong + "\tmap: https://www.google.com/maps?sourceid=chrome&q=" + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.displayLat + "," + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.displayLong);
                    com.navdy.client.app.framework.search.GooglePlacesSearch googlePlacesSearch = new com.navdy.client.app.framework.search.GooglePlacesSearch(this);
                    if (com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.placeId != null) {
                        com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("\n==========\nGoogle Place Details (red)\n==========");
                        com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  placeId:         " + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.placeId);
                        googlePlacesSearch.runDetailsSearchWebApi(com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.placeId);
                        return;
                    }
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.doApiCalls(com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination, com.navdy.client.debug.navdebug.NavCoordTestSuite.Step.firstStep());
                }
            } else if (queryType != com.navdy.client.app.framework.search.GooglePlacesSearch.Query.DETAILS) {
            } else {
                if (com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination == null) {
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.logger.e("Received a google place details result for an unknown currentDestination. Ignoring results.");
                    return;
                }
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"google_place_details_results\":" + com.navdy.client.debug.navdebug.NavCoordTestSuite.this.googleSearchResultsToJsonArray(googleResults) + ",\n");
                if (googleResults != null && !googleResults.isEmpty()) {
                    com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destinationResult = (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) googleResults.listIterator().next();
                    com.navdy.client.app.framework.models.Destination newDestination = new com.navdy.client.app.framework.models.Destination();
                    destinationResult.toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.DETAILS, newDestination);
                    double distanceBetweenInOutCoords = com.navdy.client.app.framework.map.MapUtils.distanceBetween(com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination.getDisplayCoordinate(), newDestination.getNavigationCoordinate());
                    double distanceBetweenOutCoords = com.navdy.client.app.framework.map.MapUtils.distanceBetween(newDestination.getDisplayCoordinate(), newDestination.getNavigationCoordinate());
                    double distanceThreshold = com.navdy.client.app.framework.map.NavCoordsAddressProcessor.getDistanceThresholdFor(com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"place_details_destination\":" + newDestination.toJsonString() + ",\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"is_successful\":true,\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"display_coords\":\"" + newDestination.displayLat + org.droidparts.contract.SQL.DDL.SEPARATOR + newDestination.displayLong + "\",\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"nav_coords\":\"" + newDestination.navigationLat + org.droidparts.contract.SQL.DDL.SEPARATOR + newDestination.navigationLong + "\",\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"distance_between_out_coords\":\"" + distanceBetweenOutCoords + "\",\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"distance_between_in_n_out_coords\":\"" + distanceBetweenInOutCoords + "\",\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"distance_in_n_out_threshold\":\"" + distanceThreshold + "\",\n");
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  display_coords:  " + newDestination.displayLat + org.droidparts.contract.SQL.DDL.SEPARATOR + newDestination.displayLong + "\tmap: https://www.google.com/maps?sourceid=chrome&q=" + newDestination.displayLat + "," + newDestination.displayLong);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Address:                          " + newDestination.rawAddressNotForDisplay);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.logCoordinate("  Display coords:                   ", newDestination.displayLat, newDestination.displayLong);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.logCoordinate("  Nav coords:                       ", newDestination.navigationLat, newDestination.navigationLong);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Distance between out coords:      " + distanceBetweenOutCoords);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Distance between in & out coords: " + distanceBetweenInOutCoords);
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Distance threshold for in & out:  " + distanceThreshold);
                }
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.doApiCalls(com.navdy.client.debug.navdebug.NavCoordTestSuite.this.currentDestination, com.navdy.client.debug.navdebug.NavCoordTestSuite.Step.firstStep());
            }
        }
    }

    class Anon2 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
        final /* synthetic */ com.navdy.client.debug.navdebug.NavCoordTestSuite.Step val$step;

        Anon2(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.debug.navdebug.NavCoordTestSuite.Step step) {
            this.val$destination = destination;
            this.val$step = step;
        }

        public void onSuccess(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate displayCoords, @android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate navCoords, @android.support.annotation.Nullable com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precisionLevel) {
            double distanceBetweenInOutCoords = com.navdy.client.app.framework.map.MapUtils.distanceBetween(this.val$destination.getDisplayCoordinate(), navCoords);
            double distanceBetweenOutCoords = com.navdy.client.app.framework.map.MapUtils.distanceBetween(displayCoords, navCoords);
            double distanceThreshold = com.navdy.client.app.framework.map.NavCoordsAddressProcessor.getDistanceThresholdFor(this.val$destination);
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"is_successful\":true,\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"display_coords\":\"" + displayCoords.latitude + org.droidparts.contract.SQL.DDL.SEPARATOR + displayCoords.longitude + "\",\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"nav_coords\":\"" + navCoords.latitude + org.droidparts.contract.SQL.DDL.SEPARATOR + navCoords.longitude + "\",\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"distance_between_out_coords\":\"" + distanceBetweenOutCoords + "\",\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"distance_between_in_n_out_coords\":\"" + distanceBetweenInOutCoords + "\",\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"distance_in_n_out_threshold\":\"" + distanceThreshold + "\",\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.coords.put(this.val$step, new android.util.Pair(displayCoords, navCoords));
            if (address != null) {
                java.lang.String addressString = address.toString().replaceAll("\n", org.droidparts.contract.SQL.DDL.SEPARATOR);
                java.lang.String streetNumber = address.getHouseNumber();
                java.lang.String streetName = address.getStreet();
                java.lang.String city = address.getCity();
                java.lang.String state = address.getState();
                java.lang.String zipCode = address.getPostalCode();
                java.lang.String country = address.getCountryName();
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"address\": {\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"full\": \"" + addressString + "\",\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"streetNumber\": \"" + streetNumber + "\",\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"streetName\": \"" + streetName + "\",\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"city\": \"" + city + "\",\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"state\": \"" + state + "\",\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"zipCode\": \"" + zipCode + "\",\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("  \"country\": \"" + country + "\"\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write(" },\n");
            } else {
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write(" \"address\": {},\n");
            }
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write(" \"precisionLevel\":\"" + precisionLevel.name() + "\"\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Address:                          " + (address != null ? address.getText() : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.logCoordinate("  Display coords:                   ", displayCoords.latitude.doubleValue(), displayCoords.longitude.doubleValue());
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.logCoordinate("  Nav coords:                       ", navCoords.latitude.doubleValue(), navCoords.longitude.doubleValue());
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Distance between out coords:      " + distanceBetweenOutCoords);
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Distance between in & out coords: " + distanceBetweenInOutCoords);
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log("  Distance threshold for in & out:  " + distanceThreshold);
            goToNextStep();
        }

        public void onFailure(java.lang.String error) {
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write(" \"is_successful\":false,\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write(" \"throwable\":\"" + error + "\"\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log(" ### FAILURE ###");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.log(" ### " + error);
            goToNextStep();
        }

        private void goToNextStep() {
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.logGoogleStaticMapForThisStep(this.val$step);
            if (this.val$step.isLastStep()) {
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("}\n");
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.logGoogleStaticMapForAllSteps();
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.runNextTest();
                return;
            }
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.write("},\n");
            com.navdy.client.debug.navdebug.NavCoordTestSuite.this.doApiCalls(this.val$destination, this.val$step.nextStep());
        }
    }

    class Anon3 implements com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback val$callback;

        Anon3(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback navigationCoordinatesRetrievalCallback) {
            this.val$callback = navigationCoordinatesRetrievalCallback;
        }

        public void onComplete(@android.support.annotation.NonNull com.here.android.mpa.common.GeoCoordinate displayCoords, @android.support.annotation.Nullable com.here.android.mpa.common.GeoCoordinate navigationCoords, @android.support.annotation.Nullable com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precision) {
            this.val$callback.onSuccess(new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(displayCoords.getLatitude()), java.lang.Double.valueOf(displayCoords.getLongitude()), java.lang.Float.valueOf(0.0f), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Float.valueOf(0.0f), java.lang.Long.valueOf(0), "HERE"), new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(navigationCoords != null ? navigationCoords.getLatitude() : 0.0d), java.lang.Double.valueOf(navigationCoords != null ? navigationCoords.getLongitude() : 0.0d), java.lang.Float.valueOf(0.0f), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Float.valueOf(0.0f), java.lang.Long.valueOf(0), "HERE"), address, precision);
        }

        public void onError(@android.support.annotation.NonNull com.here.android.mpa.search.ErrorCode error) {
            this.val$callback.onFailure(error.name());
        }
    }

    class Anon4 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
        final /* synthetic */ com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback val$callback;

        Anon4(com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback navigationCoordinatesRetrievalCallback) {
            this.val$callback = navigationCoordinatesRetrievalCallback;
        }

        public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
            this.val$callback.onSuccess(destination.getDisplayCoordinate(), destination.getNavigationCoordinate(), destination.getHereAddress(), destination.precisionLevel);
        }

        public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
            this.val$callback.onFailure(error.name());
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$message;

        Anon5(java.lang.String str) {
            this.val$message = str;
        }

        public void run() {
            if (com.navdy.client.debug.navdebug.NavCoordTestSuite.this.textView != null) {
                if (com.navdy.client.debug.navdebug.NavCoordTestSuite.this.text.length() + this.val$message.length() > 5000) {
                    com.navdy.client.debug.navdebug.NavCoordTestSuite.this.text.subSequence(500, com.navdy.client.debug.navdebug.NavCoordTestSuite.this.text.length());
                }
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.text = com.navdy.client.debug.navdebug.NavCoordTestSuite.this.text + "\n" + this.val$message;
                com.navdy.client.debug.navdebug.NavCoordTestSuite.this.textView.setText(com.navdy.client.debug.navdebug.NavCoordTestSuite.this.text);
            }
        }
    }

    enum Step {
        HERE_NATIVE_PLACES_SEARCH_WITH_NAME("white"),
        HERE_WEB_PLACES_SEARCH_WITH_NAME("brown"),
        HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME("green"),
        HERE_WEB_PLACES_SEARCH_WITHOUT_NAME("purple"),
        HERE_GEOCODER_WITH_NAME("yellow"),
        HERE_GEOCODER_WITHOUT_NAME("blue"),
        GOOGLE_DIRECTIONS("orange"),
        NAVDY_PROCESS("black");
        
        java.lang.String markerColor;

        private Step(java.lang.String markerColor2) {
            this.markerColor = "red";
            this.markerColor = markerColor2;
        }

        public java.lang.String getTitle() {
            return name() + org.droidparts.contract.SQL.DDL.OPENING_BRACE + this.markerColor + ")";
        }

        public static com.navdy.client.debug.navdebug.NavCoordTestSuite.Step firstStep() {
            return values()[0];
        }

        public com.navdy.client.debug.navdebug.NavCoordTestSuite.Step nextStep() {
            if (isLastStep()) {
                return firstStep();
            }
            return values()[ordinal() + 1];
        }

        private boolean isLastStep() {
            return ordinal() + 1 >= values().length;
        }
    }

    private NavCoordTestSuite() {
        try {
            java.io.File file = new java.io.File(android.os.Environment.getExternalStorageDirectory(), "navCoordTestSuite.txt");
            if (file.exists()) {
                java.io.BufferedReader br = new java.io.BufferedReader(new java.io.FileReader(file));
                while (true) {
                    java.lang.String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    this.queries.add(line);
                }
                br.close();
            } else {
                this.queries.add("SFO");
                this.queries.add("SFO Airport Terimanal 2 - Lower Level");
                this.queries.add("Palm Springs International Airport");
                this.queries.add("Fort Funston National Park");
                this.queries.add("575 7th St, SF, CA");
                this.queries.add("Kanishka Gasto pub, Walnut creek");
                this.queries.add("7030 Devon way, Berkeley");
                this.queries.add("265 Ygnacico valled rd, Walnut creek");
                this.queries.add("Apple Store, Walnut Creek");
                this.queries.add("wrangell st elias national park");
                this.queries.add("Menlo Park Caltrain station");
                this.queries.add("Yosemite");
                this.queries.add("Elektrotehnicka skola rade serbia");
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        this.mainThreadHandler = null;
        this.textView = null;
        this.output = null;
        this.text = "";
        this.OUTPUT_FILE = "NavCoordTest.json";
        this.currentQueryIndex = 0;
        this.coords = new java.util.HashMap<>(com.navdy.client.debug.navdebug.NavCoordTestSuite.Step.values().length);
        this.GOOGLE_WEB_SERVICE_KEY = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_google_web_service));
    }

    public static com.navdy.client.debug.navdebug.NavCoordTestSuite getInstance() {
        if (instance == null) {
            instance = new com.navdy.client.debug.navdebug.NavCoordTestSuite();
        }
        return instance;
    }

    public void onResume(android.os.Handler mainThreadHandler2, android.widget.TextView textView2) {
        this.mainThreadHandler = mainThreadHandler2;
        this.textView = textView2;
        log("");
    }

    public void onPause() {
        this.textView = null;
    }

    public void showToast(java.lang.String msg) {
        if (this.toast != null) {
            this.toast.cancel();
        }
        this.toast = android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), msg, 1);
        this.toast.show();
    }

    public void run() {
        run(null);
    }

    public void run(java.lang.String query) {
        this.currentQueryIndex = 0;
        if (!com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
            this.logger.e("Unable to test APIs when offline.");
            showToast("Unable to test APIs when offline.");
            return;
        }
        showToast("Starting test suite...");
        if (this.output == null) {
            try {
                this.output = createOutputFileStreamWriter();
            } catch (java.io.IOException e) {
                this.logger.e("Unable to create or open the output file (NavCoordTest.json) for nav coord test suite.", e);
            }
        }
        write("[");
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
            this.singleTest = false;
            runTest((java.lang.String) this.queries.get(this.currentQueryIndex));
            return;
        }
        this.singleTest = true;
        runTest(query);
    }

    @android.support.annotation.NonNull
    private java.io.OutputStreamWriter createOutputFileStreamWriter() throws java.io.IOException {
        return new java.io.OutputStreamWriter(new java.io.FileOutputStream(android.os.Environment.getExternalStorageDirectory() + java.io.File.separator + "NavCoordTest.json", false));
    }

    private void runTest(java.lang.String query) {
        write("{\n  \"search_terms\":\"" + query + "\",\n");
        log(" ");
        log(" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        log(" @@ Testing: " + query);
        log(" ");
        log("\n===========\nGoogle Text Search\n===========");
        log("  search query:      " + query);
        new com.navdy.client.app.framework.search.GooglePlacesSearch(new com.navdy.client.debug.navdebug.NavCoordTestSuite.Anon1(), new android.os.Handler()).runTextSearchWebApi(query);
    }

    private void doApiCalls(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination, com.navdy.client.debug.navdebug.NavCoordTestSuite.Step step) {
        com.navdy.client.app.framework.models.Destination theDestination = new com.navdy.client.app.framework.models.Destination(destination);
        if (step == com.navdy.client.debug.navdebug.NavCoordTestSuite.Step.HERE_NATIVE_PLACES_SEARCH_WITH_NAME || step == com.navdy.client.debug.navdebug.NavCoordTestSuite.Step.HERE_WEB_PLACES_SEARCH_WITH_NAME || step == com.navdy.client.debug.navdebug.NavCoordTestSuite.Step.HERE_GEOCODER_WITH_NAME) {
            theDestination.rawAddressNotForDisplay = destination.name + org.droidparts.contract.SQL.DDL.SEPARATOR + destination.rawAddressNotForDisplay;
        }
        write("  \"" + step.name().toLowerCase() + "\":" + " {\n" + " \"currentDestination\":" + destination.toJsonString() + ",\n");
        log("\n===========\n" + step.getTitle() + "\n===========");
        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback callback = new com.navdy.client.debug.navdebug.NavCoordTestSuite.Anon2(destination, step);
        com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback hereCallback = new com.navdy.client.debug.navdebug.NavCoordTestSuite.Anon3(callback);
        switch (step) {
            case HERE_NATIVE_PLACES_SEARCH_WITH_NAME:
            case HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME:
                log("Address: " + theDestination.rawAddressNotForDisplay);
                com.navdy.client.app.framework.map.HereGeocoder.makeRequest(theDestination.rawAddressNotForDisplay, hereCallback);
                return;
            case HERE_WEB_PLACES_SEARCH_WITH_NAME:
            case HERE_WEB_PLACES_SEARCH_WITHOUT_NAME:
                log("Address: " + theDestination.rawAddressNotForDisplay);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.callWebHerePlacesSearchApi(theDestination.rawAddressNotForDisplay, hereCallback);
                return;
            case HERE_GEOCODER_WITH_NAME:
            case HERE_GEOCODER_WITHOUT_NAME:
                log("Address: " + theDestination.rawAddressNotForDisplay);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.callWebHereGeocoderApi(theDestination.rawAddressNotForDisplay, hereCallback);
                return;
            case GOOGLE_DIRECTIONS:
                log("PlaceId: " + theDestination.placeId + " Address: " + theDestination.rawAddressNotForDisplay);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.getCoordinatesFromGoogleDirectionsWebApi(theDestination, callback);
                return;
            default:
                log("PlaceId: " + theDestination.placeId + " Address: " + theDestination.rawAddressNotForDisplay);
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(theDestination, new com.navdy.client.debug.navdebug.NavCoordTestSuite.Anon4(callback));
                return;
        }
    }

    private void logGoogleStaticMapForThisStep(com.navdy.client.debug.navdebug.NavCoordTestSuite.Step step) {
        java.util.Set<com.navdy.client.debug.navdebug.NavCoordTestSuite.Step> keys = new android.support.v4.util.ArraySet<>();
        keys.add(step);
        logGoogleStaticMapForAllSteps(keys);
    }

    private void logGoogleStaticMapForAllSteps() {
        logGoogleStaticMapForAllSteps(this.coords.keySet());
    }

    private void logGoogleStaticMapForAllSteps(java.util.Set<com.navdy.client.debug.navdebug.NavCoordTestSuite.Step> keys) {
        log("Map Legend:");
        log("red: \t GOOGLE_PLACE_DETAILS");
        java.lang.String staticMapUrl = addCoordMarker("https://maps.googleapis.com/maps/api/staticmap?size=1024x768&maptype=roadmap", "red", this.currentDestination.displayLat, this.currentDestination.displayLong, this.currentDestination.navigationLat, this.currentDestination.navigationLong);
        for (com.navdy.client.debug.navdebug.NavCoordTestSuite.Step key : keys) {
            log(key.markerColor + ": \t " + key.name());
            android.util.Pair<com.navdy.service.library.events.location.Coordinate, com.navdy.service.library.events.location.Coordinate> coordinatePair = (android.util.Pair) this.coords.get(key);
            com.navdy.service.library.events.location.Coordinate disp = coordinatePair.first != null ? (com.navdy.service.library.events.location.Coordinate) coordinatePair.first : new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Float.valueOf(0.0f), java.lang.Long.valueOf(0), "");
            com.navdy.service.library.events.location.Coordinate nav = coordinatePair.second != null ? (com.navdy.service.library.events.location.Coordinate) coordinatePair.second : new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Float.valueOf(0.0f), java.lang.Long.valueOf(0), "");
            staticMapUrl = addCoordMarker(staticMapUrl, key.markerColor, disp.latitude.doubleValue(), disp.longitude.doubleValue(), nav.latitude.doubleValue(), nav.longitude.doubleValue());
        }
        log("Static map: " + (staticMapUrl + "&key=" + this.GOOGLE_WEB_SERVICE_KEY));
    }

    @android.support.annotation.NonNull
    private java.lang.String addCoordMarker(java.lang.String staticMapUrl, java.lang.String color, double dispLatitude, double dispLongitude, double navLatitude, double navLongitude) {
        if (dispLatitude == navLatitude && dispLongitude == navLongitude) {
            return addMarker(staticMapUrl, color, "B", dispLatitude, dispLongitude);
        }
        java.lang.String displayLabel = "D";
        java.lang.String navLabel = "N";
        if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(navLatitude, navLongitude)) {
            displayLabel = "C";
        }
        if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(dispLatitude, dispLongitude)) {
            navLabel = "M";
        }
        return addMarker(addMarker(staticMapUrl, color, displayLabel, dispLatitude, dispLongitude), color, navLabel, navLatitude, navLongitude);
    }

    @android.support.annotation.NonNull
    private java.lang.String addMarker(java.lang.String staticMapUrl, java.lang.String color, java.lang.String label, double latitude, double longitude) {
        return !com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(latitude, longitude) ? staticMapUrl : staticMapUrl + "&markers=color:" + color + "%7Clabel:" + label + "%7C" + latitude + "," + longitude;
    }

    private void logCoordinate(java.lang.String name, double lat, double lon) {
        java.lang.String encodedName;
        try {
            encodedName = "(" + java.net.URLEncoder.encode(name.replaceAll("[: ]", ""), "UTF-8") + ")";
        } catch (java.io.UnsupportedEncodingException e) {
            encodedName = "";
        }
        log(name + lat + org.droidparts.contract.SQL.DDL.SEPARATOR + lon + ((lat == 0.0d && lon == 0.0d) ? "" : "\tmap: " + ("https://www.google.com/maps?sourceid=chrome&q=" + lat + "," + lon + encodedName)));
    }

    private void runNextTest() {
        write("}");
        this.currentQueryIndex++;
        if (this.singleTest || this.currentQueryIndex >= this.queries.size()) {
            write("]");
            try {
                this.output.close();
                this.output = null;
            } catch (java.io.IOException e) {
                this.logger.e("Unable to close the output file (NavCoordTest.json) for nav coord test suite.", e);
            }
            logEndOfTest();
            showToast("...Test suite complete");
            return;
        }
        write(",\n");
        runTest((java.lang.String) this.queries.get(this.currentQueryIndex));
    }

    private void logEndOfTest() {
        log(" ");
        log(" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        log(" @@@@@@     Test suite finished.     @@@@@@");
        log(" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    }

    private java.lang.String googleSearchResultsToJsonArray(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleResults) {
        java.lang.StringBuilder jsonString = new java.lang.StringBuilder("[ ");
        if (googleResults == null || googleResults.isEmpty()) {
            return "[]";
        }
        for (int i = 0; i < googleResults.size(); i++) {
            jsonString.append(((com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) googleResults.get(i)).jsonString);
            if (i + 1 < googleResults.size()) {
                jsonString.append(",\n    ");
            }
        }
        jsonString.append(" ]");
        return jsonString.toString();
    }

    private java.lang.String destinationsToJsonArray(java.util.List<com.navdy.client.app.framework.models.Destination> googleSearchDestinations) {
        java.lang.StringBuilder jsonString = new java.lang.StringBuilder("[ ");
        if (googleSearchDestinations == null || googleSearchDestinations.isEmpty()) {
            return "[]";
        }
        for (int i = 0; i < googleSearchDestinations.size(); i++) {
            jsonString.append(((com.navdy.client.app.framework.models.Destination) googleSearchDestinations.get(i)).toJsonString());
            if (i + 1 < googleSearchDestinations.size()) {
                jsonString.append(",\n    ");
            }
        }
        jsonString.append(" ]");
        return jsonString.toString();
    }

    private void write(java.lang.String str) {
        try {
            if (this.output != null) {
                this.output.write(str);
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private void log(@android.support.annotation.NonNull java.lang.String message) {
        this.logger.d(message);
        this.mainThreadHandler.post(new com.navdy.client.debug.navdebug.NavCoordTestSuite.Anon5(message));
    }
}
