package com.navdy.client.debug.navdebug;

public class ManeuverDetailPanel$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.debug.navdebug.ManeuverDetailPanel target, java.lang.Object source) {
        target.mActionView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_action, "field 'mActionView'");
        target.mIconView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_icon, "field 'mIconView'");
        target.mTurnView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_turn, "field 'mTurnView'");
        target.mAngleView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_angle, "field 'mAngleView'");
        target.mTrafficDirectionView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_traffic_direction, "field 'mTrafficDirectionView'");
        target.mTransportModeView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_transport_mode, "field 'mTransportModeView'");
        target.mDistFromStartView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_distance_from_start, "field 'mDistFromStartView'");
        target.mDistFromPrevView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_distance_from_prev, "field 'mDistFromPrevView'");
        target.mDistToNextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_distance_to_next, "field 'mDistToNextView'");
        target.mInstructionView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_instruction, "field 'mInstructionView'");
        target.mCurStreetNumView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_cur_street_number, "field 'mCurStreetNumView'");
        target.mCurStreetNameView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_cur_street_name, "field 'mCurStreetNameView'");
        target.mNextStreetNumView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_next_street_number, "field 'mNextStreetNumView'");
        target.mNextStreetNameView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_next_street_name, "field 'mNextStreetNameView'");
        target.mNextStreetImageView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_next_street_image, "field 'mNextStreetImageView'");
        target.mSignpostNumberView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_signpost_number, "field 'mSignpostNumberView'");
        target.mSignpostTextView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_signpost_text, "field 'mSignpostTextView'");
        target.mSignpostIconView = (android.widget.ImageView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_signpost_icon, "field 'mSignpostIconView'");
        target.mSignpostLabelDirectionView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_signpost_label_direction, "field 'mSignpostLabelDirectionView'");
        target.mRoadElementsView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_road_elements, "field 'mRoadElementsView'");
        target.mStartTimeView = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.maneuver_row_start_time, "field 'mStartTimeView'");
    }

    public static void reset(com.navdy.client.debug.navdebug.ManeuverDetailPanel target) {
        target.mActionView = null;
        target.mIconView = null;
        target.mTurnView = null;
        target.mAngleView = null;
        target.mTrafficDirectionView = null;
        target.mTransportModeView = null;
        target.mDistFromStartView = null;
        target.mDistFromPrevView = null;
        target.mDistToNextView = null;
        target.mInstructionView = null;
        target.mCurStreetNumView = null;
        target.mCurStreetNameView = null;
        target.mNextStreetNumView = null;
        target.mNextStreetNameView = null;
        target.mNextStreetImageView = null;
        target.mSignpostNumberView = null;
        target.mSignpostTextView = null;
        target.mSignpostIconView = null;
        target.mSignpostLabelDirectionView = null;
        target.mRoadElementsView = null;
        target.mStartTimeView = null;
    }
}
