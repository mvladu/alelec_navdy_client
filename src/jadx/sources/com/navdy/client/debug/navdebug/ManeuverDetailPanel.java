package com.navdy.client.debug.navdebug;

public class ManeuverDetailPanel extends android.widget.LinearLayout {
    public static final java.lang.String NO_DATA_PLACEHOLDER = "(none)";
    @butterknife.InjectView(2131755779)
    android.widget.TextView mActionView;
    @butterknife.InjectView(2131755782)
    android.widget.TextView mAngleView;
    @butterknife.InjectView(2131755790)
    android.widget.TextView mCurStreetNameView;
    @butterknife.InjectView(2131755789)
    android.widget.TextView mCurStreetNumView;
    @butterknife.InjectView(2131755786)
    android.widget.TextView mDistFromPrevView;
    @butterknife.InjectView(2131755785)
    android.widget.TextView mDistFromStartView;
    @butterknife.InjectView(2131755787)
    android.widget.TextView mDistToNextView;
    @butterknife.InjectView(2131755780)
    android.widget.TextView mIconView;
    @butterknife.InjectView(2131755788)
    android.widget.TextView mInstructionView;
    protected com.here.android.mpa.routing.Maneuver mManeuver;
    protected boolean mMergedChildren;
    @butterknife.InjectView(2131755793)
    android.widget.ImageView mNextStreetImageView;
    @butterknife.InjectView(2131755792)
    android.widget.TextView mNextStreetNameView;
    @butterknife.InjectView(2131755791)
    android.widget.TextView mNextStreetNumView;
    @butterknife.InjectView(2131755798)
    android.widget.TextView mRoadElementsView;
    @butterknife.InjectView(2131755796)
    android.widget.ImageView mSignpostIconView;
    @butterknife.InjectView(2131755797)
    android.widget.TextView mSignpostLabelDirectionView;
    @butterknife.InjectView(2131755794)
    android.widget.TextView mSignpostNumberView;
    @butterknife.InjectView(2131755795)
    android.widget.TextView mSignpostTextView;
    @butterknife.InjectView(2131755799)
    android.widget.TextView mStartTimeView;
    @butterknife.InjectView(2131755783)
    android.widget.TextView mTrafficDirectionView;
    @butterknife.InjectView(2131755784)
    android.widget.TextView mTransportModeView;
    @butterknife.InjectView(2131755781)
    android.widget.TextView mTurnView;

    public static com.navdy.client.debug.navdebug.ManeuverDetailPanel inflate(android.view.ViewGroup parent) {
        return (com.navdy.client.debug.navdebug.ManeuverDetailPanel) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.maneuver_panel_root, parent, false);
    }

    public ManeuverDetailPanel(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        mergeChildren(context);
    }

    public ManeuverDetailPanel(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mergeChildren(context);
    }

    public com.here.android.mpa.routing.Maneuver getManeuver() {
        return this.mManeuver;
    }

    protected void mergeChildren(android.content.Context context) {
        if (!this.mMergedChildren) {
            android.view.LayoutInflater.from(context).inflate(com.navdy.client.R.layout.maneuver_panel_children, this, true);
            this.mMergedChildren = true;
            butterknife.ButterKnife.inject((java.lang.Object) this, (android.view.View) this);
            updateViews();
        }
    }

    public void setManeuver(com.here.android.mpa.routing.Maneuver maneuver) {
        this.mManeuver = maneuver;
        if (this.mMergedChildren) {
            updateViews();
        }
    }

    protected void updateViews() {
        java.lang.String action = NO_DATA_PLACEHOLDER;
        java.lang.String icon = NO_DATA_PLACEHOLDER;
        java.lang.String turn = NO_DATA_PLACEHOLDER;
        java.lang.String angle = NO_DATA_PLACEHOLDER;
        java.lang.String trafficDirection = NO_DATA_PLACEHOLDER;
        java.lang.String transportMode = NO_DATA_PLACEHOLDER;
        java.lang.String distFromStart = NO_DATA_PLACEHOLDER;
        java.lang.String distFromPrev = NO_DATA_PLACEHOLDER;
        java.lang.String distToNext = NO_DATA_PLACEHOLDER;
        java.lang.String instruction = NO_DATA_PLACEHOLDER;
        java.lang.String curStreetNum = NO_DATA_PLACEHOLDER;
        java.lang.String curStreetName = NO_DATA_PLACEHOLDER;
        java.lang.String nextStreetNum = NO_DATA_PLACEHOLDER;
        java.lang.String nextStreetName = NO_DATA_PLACEHOLDER;
        android.graphics.Bitmap nextStreetBitmap = null;
        java.lang.String signpostNumber = NO_DATA_PLACEHOLDER;
        java.lang.String signpostText = NO_DATA_PLACEHOLDER;
        int signpostFg = -16777216;
        int signpostBg = -1;
        android.graphics.Bitmap signpostIconBitmap = null;
        java.lang.String signpostLabelDirection = NO_DATA_PLACEHOLDER;
        java.lang.String roadElementsText = NO_DATA_PLACEHOLDER;
        java.lang.String startTime = NO_DATA_PLACEHOLDER;
        if (this.mManeuver != null) {
            action = defaultIfNull(this.mManeuver.getAction());
            icon = defaultIfNull(this.mManeuver.getIcon());
            turn = defaultIfNull(this.mManeuver.getTurn());
            angle = "" + this.mManeuver.getAngle();
            trafficDirection = defaultIfNull(this.mManeuver.getTrafficDirection());
            transportMode = defaultIfNull(this.mManeuver.getTransportMode());
            distFromStart = "" + this.mManeuver.getDistanceFromStart();
            distFromPrev = "" + this.mManeuver.getDistanceFromPreviousManeuver();
            distToNext = "" + this.mManeuver.getDistanceToNextManeuver();
            curStreetNum = defaultIfEmpty(this.mManeuver.getRoadNumber());
            curStreetName = defaultIfEmpty(this.mManeuver.getRoadName());
            nextStreetNum = defaultIfEmpty(this.mManeuver.getNextRoadNumber());
            nextStreetName = defaultIfEmpty(this.mManeuver.getNextRoadName());
            if (this.mManeuver.getNextRoadImage() != null && this.mManeuver.getNextRoadImage().isValid()) {
                nextStreetBitmap = this.mManeuver.getNextRoadImage().getBitmap();
            }
            if (this.mManeuver.getSignpost() != null) {
                signpostNumber = defaultIfEmpty(this.mManeuver.getSignpost().getExitNumber());
                signpostText = defaultIfEmpty(this.mManeuver.getSignpost().getExitText());
                signpostFg = this.mManeuver.getSignpost().getForegroundColor();
                signpostBg = this.mManeuver.getSignpost().getBackgroundColor();
                if (this.mManeuver.getSignpost().getExitIcon() != null && this.mManeuver.getSignpost().getExitIcon().isValid()) {
                    signpostIconBitmap = this.mManeuver.getNextRoadImage().getBitmap();
                }
                java.lang.String signpostLabelDirection2 = NO_DATA_PLACEHOLDER;
                java.util.ArrayList<java.lang.String> directionLabelList = new java.util.ArrayList<>();
                for (com.here.android.mpa.routing.Signpost.LocalizedLabel label : this.mManeuver.getSignpost().getExitDirections()) {
                    directionLabelList.add(java.lang.String.format("dir: %s name: %s text: %s", new java.lang.Object[]{defaultIfEmpty(label.getRouteDirection()), defaultIfEmpty(label.getRouteName()), defaultIfEmpty(label.getText())}));
                }
                signpostLabelDirection = android.text.TextUtils.join("\n", directionLabelList);
            }
            java.util.ArrayList<java.lang.String> roadElements = new java.util.ArrayList<>();
            for (com.here.android.mpa.common.RoadElement roadElement : this.mManeuver.getRoadElements()) {
                java.lang.String str = "roadName: %s routeName: %s%nattr: %s%navgSpeed: %f formOfWay: %s lanes: %s%nspeedLmt: %f.1 plural: %s ped: %s%nstartTime: %s";
                java.lang.Object[] objArr = new java.lang.Object[10];
                objArr[0] = defaultIfEmpty(roadElement.getRoadName());
                objArr[1] = defaultIfEmpty(roadElement.getRouteName());
                objArr[2] = android.text.TextUtils.join(" ", roadElement.getAttributes());
                objArr[3] = java.lang.Float.valueOf(roadElement.getDefaultSpeed());
                objArr[4] = defaultIfNull(roadElement.getFormOfWay());
                objArr[5] = java.lang.Integer.valueOf(roadElement.getNumberOfLanes());
                objArr[6] = java.lang.Float.valueOf(roadElement.getSpeedLimit());
                objArr[7] = roadElement.isPlural() ? defaultIfNull(roadElement.getPluralType()) : "no";
                objArr[8] = java.lang.Boolean.valueOf(roadElement.isPedestrian());
                objArr[9] = defaultIfNull(roadElement.getStartTime());
                roadElements.add(java.lang.String.format(str, objArr));
            }
            roadElementsText = android.text.TextUtils.join("\n\n", roadElements);
            startTime = defaultIfNull(this.mManeuver.getStartTime());
        }
        this.mActionView.setText(action);
        this.mIconView.setText(icon);
        this.mTurnView.setText(turn);
        this.mAngleView.setText(angle);
        this.mTrafficDirectionView.setText(trafficDirection);
        this.mTransportModeView.setText(transportMode);
        this.mDistFromStartView.setText(distFromStart);
        this.mDistFromPrevView.setText(distFromPrev);
        this.mDistToNextView.setText(distToNext);
        this.mInstructionView.setText(instruction);
        this.mCurStreetNumView.setText(curStreetNum);
        this.mCurStreetNameView.setText(curStreetName);
        this.mNextStreetNumView.setText(nextStreetNum);
        this.mNextStreetNameView.setText(nextStreetName);
        if (nextStreetBitmap != null) {
            this.mNextStreetImageView.setVisibility(View.VISIBLE);
            this.mNextStreetImageView.setImageBitmap(nextStreetBitmap);
        } else {
            this.mNextStreetImageView.setVisibility(View.GONE);
        }
        this.mSignpostNumberView.setText(signpostNumber);
        this.mSignpostTextView.setText(signpostText);
        this.mSignpostNumberView.setTextColor(signpostFg);
        this.mSignpostTextView.setTextColor(signpostFg);
        this.mSignpostNumberView.setBackgroundColor(signpostBg);
        this.mSignpostTextView.setBackgroundColor(signpostBg);
        if (signpostIconBitmap != null) {
            this.mSignpostIconView.setVisibility(View.VISIBLE);
            this.mSignpostIconView.setImageBitmap(signpostIconBitmap);
        } else {
            this.mSignpostIconView.setVisibility(View.INVISIBLE);
        }
        this.mSignpostLabelDirectionView.setText(signpostLabelDirection);
        this.mRoadElementsView.setText(roadElementsText);
        this.mStartTimeView.setText(startTime);
    }

    protected java.lang.String defaultIfNull(java.lang.Object o) {
        if (o == null) {
            return NO_DATA_PLACEHOLDER;
        }
        return o.toString();
    }

    protected java.lang.String defaultIfEmpty(java.lang.String s) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(s)) {
            return NO_DATA_PLACEHOLDER;
        }
        return s;
    }
}
