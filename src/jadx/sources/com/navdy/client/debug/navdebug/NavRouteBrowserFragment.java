package com.navdy.client.debug.navdebug;

public class NavRouteBrowserFragment extends android.app.ListFragment implements com.here.android.mpa.common.PositioningManager.OnPositionChangedListener, com.here.android.mpa.common.OnEngineInitListener {
    private static final double[] DEFAULT_DESTINATION = {49.24831d, -122.980013d};
    private static final double[] DEFAULT_START_POINT = {49.260024d, -123.006984d};
    public static final java.lang.String EXTRA_COORDS_END = "dest_coords";
    public static final java.lang.String EXTRA_COORDS_START = "start_coords";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.navdebug.NavRouteBrowserFragment.class);
    protected boolean mCalculatingRoute;
    protected com.here.android.mpa.common.GeoPosition mCurrentGeoPosition;
    private com.here.android.mpa.common.GeoCoordinate mEndCoord;
    protected boolean mEngineAvailable;
    protected boolean mEngineInitialized;
    protected com.navdy.client.debug.navdebug.ManeuverArrayAdapter mManeuverArrayAdapter;
    protected java.util.ArrayList<com.here.android.mpa.routing.Maneuver> mManeuvers;
    private com.here.android.mpa.routing.Route mRoute = null;
    private com.here.android.mpa.common.GeoCoordinate mStartCoord;

    class Anon1 implements com.here.android.mpa.routing.RouteManager.Listener {
        Anon1() {
        }

        public void onCalculateRouteFinished(com.here.android.mpa.routing.RouteManager.Error error, java.util.List<com.here.android.mpa.routing.RouteResult> results) {
            com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.mCalculatingRoute = false;
            if (error != com.here.android.mpa.routing.RouteManager.Error.NONE) {
                com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.showError("Failed: " + error.toString());
            } else if (results.size() > 0) {
                com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.setRoute(((com.here.android.mpa.routing.RouteResult) results.get(0)).getRoute());
            } else {
                com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.showError("No results.");
            }
        }

        public void onProgress(int percentage) {
            com.navdy.client.debug.navdebug.NavRouteBrowserFragment.sLogger.i("... " + percentage + " percent done ...");
        }
    }

    public static com.navdy.client.debug.navdebug.NavRouteBrowserFragment newInstance(com.here.android.mpa.common.GeoCoordinate startCoord, com.here.android.mpa.common.GeoCoordinate endCoord) {
        com.navdy.client.debug.navdebug.NavRouteBrowserFragment fragment = new com.navdy.client.debug.navdebug.NavRouteBrowserFragment();
        android.os.Bundle args = new android.os.Bundle();
        if (startCoord == null || endCoord == null) {
            sLogger.e("createIntentWithCoords: bad coordinates");
            return null;
        }
        args.putDoubleArray("start_coords", latLonArrayFromGeoCoordinate(startCoord));
        args.putDoubleArray("dest_coords", latLonArrayFromGeoCoordinate(endCoord));
        fragment.setArguments(args);
        return fragment;
    }

    public void loadArguments() {
        double[] startLatLon = getArguments().getDoubleArray("start_coords");
        double[] endLatLon = getArguments().getDoubleArray("dest_coords");
        if (startLatLon == null || startLatLon.length != 2) {
            sLogger.e("Missing valid start coordinates. Falling back.");
            startLatLon = DEFAULT_START_POINT;
        }
        if (endLatLon == null || endLatLon.length != 2) {
            sLogger.e("Missing end coordinates. Falling back.");
            endLatLon = DEFAULT_DESTINATION;
        }
        this.mStartCoord = new com.here.android.mpa.common.GeoCoordinate(startLatLon[0], startLatLon[1]);
        this.mEndCoord = new com.here.android.mpa.common.GeoCoordinate(endLatLon[0], endLatLon[1]);
    }

    public static double[] latLonArrayFromGeoCoordinate(com.here.android.mpa.common.GeoCoordinate coordinate) {
        return new double[]{coordinate.getLatitude(), coordinate.getLongitude()};
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            loadArguments();
        }
        initEngine();
        this.mManeuvers = new java.util.ArrayList<>();
        setListAdapter(new com.navdy.client.debug.navdebug.ManeuverArrayAdapter(getActivity(), this.mManeuvers));
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        return inflater.inflate(com.navdy.client.R.layout.fragment_nav_route_browser, container, false);
    }

    public void initEngine() {
        if (this.mEngineInitialized) {
            sLogger.e("Already inited.");
            return;
        }
        try {
            com.here.android.mpa.common.MapEngine.getInstance().init(getActivity(), this);
        } catch (java.lang.Exception e) {
            sLogger.e("Unable to init MapEngine");
            e.printStackTrace();
        }
    }

    public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
        if (error == com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
            sLogger.e("MapEngine initialized.");
            this.mEngineInitialized = true;
            initializePosition();
            return;
        }
        sLogger.e("MapEngine init completed with error: " + error.toString());
    }

    protected void initializePosition() {
        com.here.android.mpa.common.PositioningManager positioningManager = com.here.android.mpa.common.PositioningManager.getInstance();
        positioningManager.addListener(new java.lang.ref.WeakReference(this));
        if (!positioningManager.isActive()) {
            positioningManager.start(com.here.android.mpa.common.PositioningManager.LocationMethod.GPS_NETWORK);
        }
    }

    public void onPositionUpdated(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.GeoPosition geoPosition, boolean isMapMatched) {
        sLogger.d("onPositionUpdated method: " + locationMethod + " position: " + geoPosition.getCoordinate().toString());
        this.mCurrentGeoPosition = geoPosition;
        if (!this.mEngineAvailable) {
            this.mEngineAvailable = true;
            onEngineAvailable();
        }
    }

    public void onEngineAvailable() {
        buildRoute();
    }

    public void onPositionFixChanged(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.PositioningManager.LocationStatus locationStatus) {
        sLogger.d("onPositionFixChanged: method:" + locationMethod + " status: " + locationStatus);
    }

    public void buildRoute() {
        if (this.mRoute == null && !this.mCalculatingRoute && this.mEngineAvailable) {
            this.mCalculatingRoute = true;
            com.here.android.mpa.routing.RouteManager routeManager = new com.here.android.mpa.routing.RouteManager();
            com.here.android.mpa.routing.RoutePlan routePlan = new com.here.android.mpa.routing.RoutePlan();
            com.here.android.mpa.routing.RouteOptions routeOptions = new com.here.android.mpa.routing.RouteOptions();
            routeOptions.setTransportMode(com.here.android.mpa.routing.RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
            routePlan.setRouteOptions(routeOptions);
            routePlan.addWaypoint(this.mStartCoord);
            routePlan.addWaypoint(this.mEndCoord);
            routeManager.calculateRoute(routePlan, new com.navdy.client.debug.navdebug.NavRouteBrowserFragment.Anon1());
        }
    }

    protected void setRoute(com.here.android.mpa.routing.Route route) {
        this.mRoute = route;
        if (route.getManeuvers() == null) {
            showError("No maneuvers in route.");
            return;
        }
        this.mManeuvers.clear();
        this.mManeuvers.addAll(route.getManeuvers());
        ((com.navdy.client.debug.navdebug.ManeuverArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected void showError(java.lang.String error) {
        sLogger.e(error);
        android.widget.Toast.makeText(getActivity(), error, 0).show();
    }
}
