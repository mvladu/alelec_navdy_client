package com.navdy.client.debug;

public class NavCoordTestSuiteActivity extends android.app.Activity {
    private android.widget.EditText input;
    private android.widget.TextView logOutput;
    private com.navdy.client.debug.navdebug.NavCoordTestSuite navCoordTestSuite;

    public static void startNavCoordTestSuiteActivity(android.content.Context context, java.lang.String query) {
        android.content.Intent intent = new android.content.Intent(context, com.navdy.client.debug.NavCoordTestSuiteActivity.class);
        intent.setAction(query);
        context.startActivity(intent);
    }

    protected void onCreate(@android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.navdy.client.R.layout.debug_nav_coord_test_suite);
        this.input = (android.widget.EditText) findViewById(com.navdy.client.R.id.address_input);
        java.lang.String query = getIntent().getAction();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
            this.input.setText(query);
            this.input.setSelection(query.length());
        }
        this.navCoordTestSuite = com.navdy.client.debug.navdebug.NavCoordTestSuite.getInstance();
    }

    protected void onResume() {
        android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.logOutput = (android.widget.TextView) findViewById(com.navdy.client.R.id.log_output);
        this.navCoordTestSuite.onResume(handler, this.logOutput);
        super.onResume();
    }

    protected void onPause() {
        this.navCoordTestSuite.onPause();
        super.onPause();
    }

    public void onRunTestSuiteClick(android.view.View view) {
        com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
        this.navCoordTestSuite.run();
    }

    public void onRunSingleTestClick(android.view.View view) {
        com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
        this.navCoordTestSuite.run(this.input.getText().toString());
    }
}
