package com.navdy.client.debug;

public class AboutFragment extends android.app.Fragment {
    @butterknife.InjectView(2131755553)
    android.widget.TextView appVersionLabel;
    @butterknife.InjectView(2131755556)
    android.widget.TextView displayInfo;
    @butterknife.InjectView(2131755557)
    android.widget.TextView serialLabel;
    @butterknife.InjectView(2131755558)
    android.widget.TextView serialNumber;
    @butterknife.InjectView(2131755554)
    android.widget.TextView updateStatusTextView;
    @butterknife.InjectView(2131755560)
    android.widget.TextView vin;
    @butterknife.InjectView(2131755559)
    android.widget.TextView vinLabel;

    class Anon1 extends net.hockeyapp.android.UpdateManagerListener {
        Anon1() {
        }

        public void onNoUpdateAvailable() {
            com.navdy.client.debug.AboutFragment.this.updateStatusTextView.setVisibility(View.VISIBLE);
        }
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_about, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.appVersionLabel.setText(com.navdy.client.BuildConfig.VERSION_NAME);
        com.navdy.service.library.device.RemoteDevice device = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        int visibility = 8;
        if (device != null) {
            visibility = 0;
            com.navdy.service.library.events.DeviceInfo info = device.getDeviceInfo();
            this.serialNumber.setText(info != null ? info.deviceUuid : getResources().getString(com.navdy.client.R.string.unknown));
        }
        this.displayInfo.setVisibility(visibility);
        this.serialLabel.setVisibility(visibility);
        this.serialNumber.setVisibility(visibility);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        this.vinLabel.setVisibility(View.GONE);
        this.vin.setVisibility(View.GONE);
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.obd.ObdStatusRequest());
    }

    @butterknife.OnClick({2131755555})
    public void onCheckForUpdate(android.widget.Button button) {
        this.updateStatusTextView.setVisibility(View.INVISIBLE);
        ((com.navdy.client.debug.MainDebugActivity) getActivity()).checkForUpdates(new com.navdy.client.debug.AboutFragment.Anon1());
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onPause() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @com.squareup.otto.Subscribe
    public void onObdStatusResponse(com.navdy.service.library.events.obd.ObdStatusResponse response) {
        this.vinLabel.setVisibility(View.VISIBLE);
        this.vin.setVisibility(View.VISIBLE);
        this.vin.setText(response.vin != null ? response.vin : getResources().getString(com.navdy.client.R.string.unknown));
    }
}
