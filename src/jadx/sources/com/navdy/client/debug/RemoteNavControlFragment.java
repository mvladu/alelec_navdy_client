package com.navdy.client.debug;

public class RemoteNavControlFragment extends android.app.Fragment {
    private static final java.lang.String ARG_DESTINATION_LABEL = "destination";
    private static final java.lang.String ARG_ROUTE_ID = "routeId";
    private static final java.lang.String ARG_ROUTE_LABEL = "routeLabel";
    public static final java.lang.String EXT_TAG = "RemoteNavControlFragment";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.debug.RemoteNavControlFragment.class);
    private android.widget.Button[] mAllButtons;
    private java.lang.String mDestinationLabel;
    @butterknife.InjectView(2131755633)
    protected android.widget.TextView mDestinationLabelTextView;
    @butterknife.InjectView(2131755636)
    protected android.widget.Button mNavigationButtonPause;
    @butterknife.InjectView(2131755637)
    protected android.widget.Button mNavigationButtonResume;
    @butterknife.InjectView(2131755635)
    protected android.widget.Button mNavigationButtonSimulate;
    @butterknife.InjectView(2131755634)
    protected android.widget.Button mNavigationButtonStart;
    @butterknife.InjectView(2131755638)
    protected android.widget.Button mNavigationButtonStop;
    private com.navdy.service.library.events.navigation.NavigationSessionState mNavigationSessionState;
    private com.navdy.service.library.events.navigation.NavigationRouteResult mRouteResult;
    @butterknife.InjectView(2131755574)
    protected android.widget.TextView mTextViewCurrentSimSpeed;

    public static com.navdy.client.debug.RemoteNavControlFragment newInstance(com.navdy.service.library.events.navigation.NavigationRouteResult routeResult, java.lang.String destinationLabel) {
        com.navdy.client.debug.RemoteNavControlFragment fragment = new com.navdy.client.debug.RemoteNavControlFragment();
        android.os.Bundle args = new android.os.Bundle();
        args.putString(ARG_ROUTE_ID, routeResult.routeId);
        args.putString(ARG_ROUTE_LABEL, routeResult.label);
        args.putString(ARG_DESTINATION_LABEL, destinationLabel);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mRouteResult = new com.navdy.service.library.events.navigation.NavigationRouteResult(getArguments().getString(ARG_ROUTE_ID), getArguments().getString(ARG_ROUTE_LABEL), null, java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(0), null, null, null);
            this.mDestinationLabel = getArguments().getString(ARG_DESTINATION_LABEL);
        }
        this.mNavigationSessionState = com.navdy.client.app.framework.AppInstance.getInstance().getNavigationSessionState();
    }

    public void onPause() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.fragment_remote_nav_control, container, false);
        butterknife.ButterKnife.inject((java.lang.Object) this, rootView);
        this.mAllButtons = new android.widget.Button[]{this.mNavigationButtonStart, this.mNavigationButtonSimulate, this.mNavigationButtonPause, this.mNavigationButtonResume, this.mNavigationButtonStop};
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.mDestinationLabel)) {
            this.mDestinationLabelTextView.setText(this.mDestinationLabel);
        }
        updateNavigationSessionState(this.mNavigationSessionState);
        this.mTextViewCurrentSimSpeed.setText("Current simulation speed: " + com.navdy.client.debug.DebugActionsFragment.getSimulationSpeed() + " m/s " + org.droidparts.contract.SQL.DDL.SEPARATOR + ((int) (((float) com.navdy.client.debug.DebugActionsFragment.getSimulationSpeed()) * 2.2369f)) + " MPH");
        return rootView;
    }

    public void onResume() {
        super.onResume();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    private void updateNavigationSessionState(com.navdy.service.library.events.navigation.NavigationSessionState newState) {
        android.widget.Button[] buttonArr;
        sLogger.d("RemoteNavControlFragment: : Received new state: " + newState);
        for (android.widget.Button button : this.mAllButtons) {
            button.setEnabled(true);
            button.setVisibility(View.GONE);
        }
        switch (newState) {
            case NAV_SESSION_STOPPED:
                this.mNavigationButtonStart.setVisibility(View.VISIBLE);
                this.mNavigationButtonSimulate.setVisibility(View.VISIBLE);
                break;
            case NAV_SESSION_PAUSED:
                this.mNavigationButtonResume.setVisibility(View.VISIBLE);
                this.mNavigationButtonStop.setVisibility(View.VISIBLE);
                break;
            case NAV_SESSION_STARTED:
                this.mNavigationButtonPause.setVisibility(View.VISIBLE);
                break;
            default:
                this.mNavigationButtonStart.setVisibility(View.VISIBLE);
                this.mNavigationButtonSimulate.setVisibility(View.VISIBLE);
                this.mNavigationButtonStart.setEnabled(false);
                this.mNavigationButtonSimulate.setEnabled(false);
                break;
        }
        this.mNavigationSessionState = newState;
    }

    @butterknife.OnClick({2131755634})
    protected void onStartClicked() {
        sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, 0);
    }

    @butterknife.OnClick({2131755635})
    protected void onSimulateClicked() {
        sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED, com.navdy.client.debug.DebugActionsFragment.getSimulationSpeed());
    }

    @butterknife.OnClick({2131755636})
    protected void onPauseClicked() {
        sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED);
    }

    @butterknife.OnClick({2131755637})
    protected void onResumeClicked() {
        sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED);
    }

    @butterknife.OnClick({2131755638})
    protected void onStopClicked() {
        sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED);
    }

    private void sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState newState) {
        sendStateChangeRequest(newState, 0);
    }

    private void sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState newState, int simulationSpeed) {
        sLogger.d("RemoteNavControlFragment: Attempting to change state: " + newState);
        if (!com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            sLogger.d("RemoteNavControlFragment: : device is no longer connected");
            return;
        }
        if (com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.navigation.NavigationSessionRequest(newState, this.mDestinationLabel, this.mRouteResult.routeId, java.lang.Integer.valueOf(simulationSpeed), java.lang.Boolean.valueOf(false)))) {
            sLogger.e("RemoteNavControlFragment: State change request sent successfully.");
        } else {
            sLogger.e("RemoteNavControlFragment: State change request failed to send.");
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        updateNavigationSessionState(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent status) {
        updateNavigationSessionState(status.sessionState);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionResponse(com.navdy.service.library.events.navigation.NavigationSessionResponse response) {
        sLogger.e("RemoteNavControlFragment: Session response for pendingState: " + response.pendingSessionState + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + response.status + " - " + response.statusDetail);
    }
}
