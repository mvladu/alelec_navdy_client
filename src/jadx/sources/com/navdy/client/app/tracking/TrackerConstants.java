package com.navdy.client.app.tracking;

public class TrackerConstants {
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.tracking.TrackerConstants.class);

    public static class Attributes {
        public static final java.lang.String CAR_MAKE = "Car_Make";
        public static final java.lang.String CAR_MANUAL_ENTRY = "Vehicle_Data_Entry_Manual";
        public static final java.lang.String CAR_MODEL = "Car_Model";
        public static final java.lang.String CAR_YEAR = "Car_Year";
        public static final java.lang.String CONTACTS = "Contacts";
        static final java.lang.String CURRENT_SCREEN = "Current_Screen";
        public static final java.lang.String DURING_FLE = "During_Fle";
        public static final java.lang.String FIRST_LAUNCH_ATTRIBUTE = "First_Launch";
        public static final java.lang.String HUD_SERIAL_NUMBER = "Hud_Serial_Number";
        public static final java.lang.String HUD_VERSION_NUMBER = "Hud_Version_Number";
        public static final java.lang.String LOCATION = "Location";
        public static final java.lang.String MICROPHONE = "Microphone";
        public static final java.lang.String MOBILE_APP_VERSION = "Mobile_App_Version";
        public static final java.lang.String PHONE = "Phone";
        public static final java.lang.String SMS = "Sms";
        public static final java.lang.String STORAGE = "Storage";
        public static final java.lang.String SUCCESS_ATTRIBUTE = "Success";
        public static final java.lang.String TYPE = "Type";
        public static final java.lang.String VIN = "VIN";

        public static class DestinationAttributes {
            public static final java.lang.String FAVORITE_TYPE = "Favorite_Type";
            public static final java.lang.String NEXT_TRIP = "Next_Trip";
            public static final java.lang.String ONLINE = "Online";
            public static final java.lang.String SOURCE = "Source";
            public static final java.lang.String TYPE = "Type";

            public static class FAVORITE_TYPE_VALUES {
                private static final java.lang.String CONTACT = "Contact";
                private static final java.lang.String HOME = "Home";
                private static final java.lang.String NONE = "None";
                private static final java.lang.String OTHER = "Other";
                private static final java.lang.String WORK = "Work";

                public static java.lang.String getFavoriteTypeValue(com.navdy.client.app.framework.models.Destination destination) {
                    if (destination.favoriteType == -3) {
                        return HOME;
                    }
                    if (destination.favoriteType == -2) {
                        return WORK;
                    }
                    if (destination.favoriteType == -4) {
                        return CONTACT;
                    }
                    if (destination.isFavoriteDestination()) {
                        return OTHER;
                    }
                    return "None";
                }
            }

            public static class SOURCE_VALUES {
                public static final java.lang.String DETAILS = "Details";
                public static final java.lang.String DROP_PIN = "Drop_Pin";
                public static final java.lang.String FAVORITES_LIST = "Favorites_List";
                public static final java.lang.String SEARCH = "Search";
                public static final java.lang.String SUGGESTION_LIST = "Suggestion_List";
                public static final java.lang.String THIRD_PARTY_INTENT = "Third_Party_Intent";
            }

            public static class TYPE_VALUES {
                public static final java.lang.String CALENDAR = "Calendar";
                public static final java.lang.String DROP_PIN = "Drop_Pin";
                public static final java.lang.String FAVORITE = "Favorite";
                public static final java.lang.String INTENT = "Intent";
                public static final java.lang.String RECENT = "Recent";
                public static final java.lang.String RECOMMENDATION = "Recommendation";
                public static final java.lang.String SEARCH_AUTOCOMPLETE = "Search_Autocomplete";
                public static final java.lang.String SEARCH_CONTACT = "Search_Contact";
                public static final java.lang.String SEARCH_MAP = "Search_Map";
                public static final java.lang.String SEARCH_QUICK = "Search_Quick";
                public static final java.lang.String SEARCH_RESULT = "Search_Result";
                public static final java.lang.String UNKNOWN = "Unknown";

                public static java.lang.String getSetDestinationTypeFromSuggestion(com.navdy.client.app.framework.models.Suggestion suggestion) {
                    if (suggestion == null) {
                        return UNKNOWN;
                    }
                    if (suggestion.isRecommendation()) {
                        return RECOMMENDATION;
                    }
                    if (suggestion.getType() == com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR) {
                        return CALENDAR;
                    }
                    return RECENT;
                }
            }
        }

        public static class HudVoiceSearchAttributes {
            public static final java.lang.String NOISE_RETRY = "Noise_Retry";
        }

        public static class InstallAttributes {
            public static final java.lang.String MEDIUM_MOUNT = "Medium_Mount";
            public static final java.lang.String MOUNT_PICKER_MOUNT_TYPE = "Mount_Type";
            public static final java.lang.String MOUNT_PICKER_MOUNT_TYPE_MED_TALL = "Medium_Tall";
            public static final java.lang.String MOUNT_PICKER_MOUNT_TYPE_SHORT = "Short";
            public static final java.lang.String MOUNT_PICKER_USE_CASE = "Use_Case";
            public static final java.lang.String NB_CONFIGURATIONS = "Nb_Configurations";
            public static final java.lang.String NEW_BOX = "New_Box";
            public static final java.lang.String NEW_BOX_PLUS_MOUNTS = "New_Box_Plus_Mounts";
            public static final java.lang.String OLD_BOX = "Old_Box";
            public static final java.lang.String SELECTED_BOX = "Selected_Box";
            public static final java.lang.String SELECTED_MOUNT = "Selected_Mount";
            public static final java.lang.String SHORT_MOUNT = "Short_Mount";
            public static final java.lang.String TALL_MOUNT = "Tall_Mount";
            public static final java.lang.String VIDEO_URL = "Video_URL";
        }

        public static class MusicAttributes {
            public static final java.lang.String NUM_GPM_PLAYLISTS_INDEXED = "Num_Gpm_Playlists_Indexed";
            public static final java.lang.String NUM_GPM_TRACKS_INDEXED = "Num_Gpm_Tracks_Indexed";
        }
    }

    public static class Event {
        public static final java.lang.String BATTERY_LOW = "Battery_Low";
        public static final java.lang.String CAR_INFO_CHANGED = "Car_Info_Changed";
        public static final java.lang.String DELETING_A_FAVORITE = "Deleting_A_Favorite";
        public static final java.lang.String GLANCES_CONFIGURED = "Glances_Configured";
        public static final java.lang.String HAS_KILLED = "MapEngine_Forced_Crash";
        public static final java.lang.String HAS_PHOENIX = "MapEngine_Forced_Restart";
        public static final java.lang.String HUD_CONNECTION_ESTABLISHED = "Hud_Connection_Established";
        public static final java.lang.String HUD_CONNECTION_LOST = "Hud_Connection_Lost";
        public static final java.lang.String HUD_VOICE_SEARCH_AMBIENT_NOISE_FAILURE = "Hud_Voice_Search_ambient_noise_failure";
        public static final java.lang.String MUSIC_PLAYLIST_COMPLETE_REINDEX = "Music_Playlist_Complete_Reindex";
        public static final java.lang.String NAVIGATE_USING_FAVORITES = "Navigate_Using_Favorites";
        public static final java.lang.String NAVIGATE_USING_GOOGLE_NOW = "Navigate_Using_Google_Now";
        public static final java.lang.String NAVIGATE_USING_HUD_VOICE_SEARCH = "Navigate_Using_Hud_Voice_Search";
        public static final java.lang.String NAVIGATE_USING_SEARCH_RESULTS = "Navigate_Using_Search_Results";
        public static final java.lang.String NO_END_POINT = "No_End_Point";
        public static final java.lang.String PAIR_PHONE_TO_DISPLAY = "PairPhoneToDisplay";
        public static final java.lang.String PERMISSION_REJECTED = "Permission_Rejected";
        public static final java.lang.String SAVING_A_FAVORITE = "Saving_A_Favorite";
        public static final java.lang.String SCREEN_VIEWED = "Screen_Viewed";
        public static final java.lang.String SEARCH_FOR_LOCATION = "Search_For_Location";
        public static final java.lang.String SEARCH_USING_DESTINATION_FINDER = "Search_Using_Destination_Finder";
        public static final java.lang.String SEARCH_USING_GOOGLE_NOW = "Search_Using_Google_Now";
        public static final java.lang.String SET_DESTINATION = "Set_Destination";
        public static final java.lang.String SET_HOME = "Set_Home";
        public static final java.lang.String SET_WORK = "Set_Work";
        public static final java.lang.String SUPPORT_TICKET_CREATED = "Support_Ticket_Created";
        public static final java.lang.String SWITCHED_TO_DEFAULT = "Switched_to_default";
        public static final java.lang.String SWITCHED_TO_PLACE_SEARCH = "Switched_to_place_search";

        public static class Audio {
            public static final java.lang.String TTS_HFP_FAILURE = "TTS_HFP_FAILURE";
        }

        public static class Debug {
            public static final java.lang.String HERE_GEOCODE_COMPLETED_AFTER_TIMEOUT = "Debug_Here_Geocode_Completed_After_Timeout";
            public static final java.lang.String HERE_GEOCODE_TIMEOUT = "Debug_Here_Geocode_Timeout";
        }

        public static class Install {
            public static final java.lang.String ACTUALLY_I_ALREADY_HAVE_MED_TALL_MOUNT = "Actually_I_Already_Have_Med_Tall_Mount";
            public static final java.lang.String CONFIGURATION_AT_COMPLETION = "Configuration_At_Completion";
            public static final java.lang.String CONTINUE_WITH_UNRECOMMENDED_SHORT_MOUNT = "Continue_With_Unrecommended_Short_Mount";
            public static final java.lang.String CONTINUE_WITH_UNSUPPORTED_SHORT_MOUNT = "Continue_With_Unsupported_Short_Mount";
            public static final java.lang.String FIRST_LAUNCH_COMPLETED = "First_Launch_Completed";
            public static final java.lang.String MEDIUM_TALL_MOUNT_NOT_RECOMMENDED_CONTINUE = "Medium_Tall_Mount_Not_Recommended_Continue";
            public static final java.lang.String MOUNT_PICKER_SELECTION = "Mount_Picker_Selection";
            public static final java.lang.String PURCHASE_MOUNT_KIT = "Purchase_Mount_Kit";
            public static final java.lang.String SHORT_MOUNT_NOT_RECOMMENDED_CONTINUE = "Short_Mount_Not_Recommended_Continue";
            public static final java.lang.String USER_PICKED_MEDIUM_MOUNT = "User_Picked_Medium_Mount";
            public static final java.lang.String USER_PICKED_SHORT_MOUNT = "User_Picked_Short_Mount";
            public static final java.lang.String USER_PICKED_TALL_MOUNT = "User_Picked_Tall_Mount";
            public static final java.lang.String VIDEO_TAPPED_ON_INSTALL_COMPLETE = "Video_Tapped_On_Install_Complete";
        }

        public static class Settings {
            public static final java.lang.String AUDIO_SETTINGS_CHANGED = "Audio_Settings_Changed";
            public static final java.lang.String DIAL_SETTINGS_CHANGED = "Dial_Settings_Changed";
            public static final java.lang.String NAVIGATION_SETTINGS_CHANGED = "Navigation_Settings_Changed";
            public static final java.lang.String OTA_SETTINGS_CHANGED = "Ota_Settings_Changed";
            public static final java.lang.String PROFILE_SETTINGS_CHANGED = "Profile_Settings_Changed";
        }
    }

    public static class Screen {
        public static final java.lang.String ACTIVE_TRIP = "Active_Trip";
        public static final java.lang.String DETAILS = "Destination_Details";
        public static final java.lang.String EDIT_FAVORITES = "Edit_Favorites";
        public static final java.lang.String NOTIFICATION_GLANCES = "Notification_Glances";
        public static final java.lang.String ROUTING = "Routing";
        public static final java.lang.String SEARCH = "Search";
        public static final java.lang.String SPLASH = "Splash";

        public static class FirstLaunch {
            public static final java.lang.String CAR_INFO = "First_Launch_Car_Info";
            public static final java.lang.String EDIT_CAR_INFO = "First_Launch_Edit_Car_Info";
            public static final java.lang.String FIRST_LAUNCH = "First_Launch";
            public static final java.lang.String INSTALL_VIDEO = "First_Launch_Install_Video";
            public static final java.lang.String SCREEN_LIGHT = "First_Launch_Screen_Light";

            public static class Install {
                public static final java.lang.String BOX_PICKER = "Installation_Box_Picker";
                public static final java.lang.String BOX_VIEWER_NEW_BOX = "Installation_Box_Viewer_New_Box";
                public static final java.lang.String BOX_VIEWER_NEW_BOX_PLUS_MOUNTS = "Installation_Box_Viewer_New_Box_Plus_Mounts";
                public static final java.lang.String BOX_VIEWER_OLD_BOX = "Installation_Box_Viewer_Old_Box";
                public static final java.lang.String CABLE_PICKER = "Installation_Plugging_In";
                public static final java.lang.String DIAL = "Installation_Dial";
                public static final java.lang.String FINISHED = "Installation_Finished";
                public static final java.lang.String LENS_POSITION = "Installation_Lens_Position";
                public static final java.lang.String MEDIUM_MOUNT_RECOMMENDED = "Installation_Mount_Picker_Medium_Mount_Recommended";
                public static final java.lang.String MEDIUM_TALL_IS_TOO_HIGH = "Installation_Mount_Picker_Medium_Tall_Is_Too_High";
                public static final java.lang.String MEDIUM_TALL_MOUNT = "Installation_Medium_Tall_Mount";
                public static final java.lang.String MEDIUM_TALL_MOUNT_NOT_RECOMMENDED = "Installation_Medium_Tall_Mount_Not_Recommended";
                public static final java.lang.String MOUNT_KIT_NEEDED = "Installation_Mount_Kit_Needed";
                public static final java.lang.String MOUNT_KIT_PURCHASE = "Installation_Mount_Kit_Purchase";
                public static final java.lang.String NO_MOUNT_SUPPORTED = "Installation_Mount_Picker_No_Mount_Supported";
                public static final java.lang.String OVERVIEW = "Installation_Overview";
                public static final java.lang.String PLUG_CLA = "Installation_Plug_CLA";
                public static final java.lang.String PLUG_OBD = "Installation_Plug_OBD";
                public static final java.lang.String POWER_ON = "Installation_Power_On";
                public static final java.lang.String READY_CHECK = "Installation_Ready_Check";
                public static final java.lang.String SECURE = "Installation_Secure";
                public static final java.lang.String SHORT_IS_TOO_LOW = "Installation_Mount_Picker_Short_Is_Too_Low";
                public static final java.lang.String SHORT_MOUNT = "Installation_Short_Mount";
                public static final java.lang.String SHORT_MOUNT_NOT_RECOMMENDED = "Installation_Short_Mount_Not_Recommended";
                public static final java.lang.String TIDY_UP = "Installation_Tidy_Up";
            }

            public static class Marketing {
                public static final java.lang.String EFFORTLESS_CONTROL = "Effortless_Control";
                public static final java.lang.String GET_STARTED = "Get_Started";
                public static final java.lang.String MEET_NAVDY = "Meet_Navdy";
                public static final java.lang.String NEVER_MISS_A_TURN = "Never_Miss_A_Turn";
                public static final java.lang.String STAY_CONNECTED = "Stay_Connected";
            }
        }

        public static class Home {
            public static final java.lang.String FAVORITES = "Home_Favorites";
            public static final java.lang.String GLANCES = "Home_Glances";
            public static final java.lang.String HOME = "Home_Start";
            public static final java.lang.String TRIPS = "Home_Trips";

            public static java.lang.String tag(int index) {
                return new java.lang.String[]{HOME, FAVORITES, GLANCES, TRIPS}[index];
            }
        }

        public static class Settings {
            public static final java.lang.String AUDIO = "Settings_Audio";
            public static final java.lang.String BLUETOOTH = "Settings_Bluetooth_Pairing";
            public static final java.lang.String CONTACT = "Settings_Contact_Us";
            public static final java.lang.String DEBUG = "Settings_Debug";
            public static final java.lang.String LEGAL = "Settings_Legal";
            public static final java.lang.String MESSAGING = "Settings_Messaging";
            public static final java.lang.String NAVIGATION = "Settings_Navigation";
            public static final java.lang.String PROFILE = "Settings_Profile";
            public static final java.lang.String SUPPORT = "Settings_Support";
            public static final java.lang.String UPDATE = "Settings_Update";
        }

        public static class Web {
            public static java.lang.String tag(java.lang.String type) {
                return "Legal_" + type;
            }
        }
    }
}
