package com.navdy.client.app.tracking;

public final class SetDestinationTracker {
    private static final boolean VERBOSE = false;
    private static final com.navdy.client.app.tracking.SetDestinationTracker instance = new com.navdy.client.app.tracking.SetDestinationTracker();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.tracking.SetDestinationTracker.class);
    private java.lang.String destinationType = "";
    private java.lang.String favoriteType = "";
    private java.lang.String sourceValue = "";

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon1(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destination = destination;
        }

        public void run() {
            java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>();
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            boolean isConnected = com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected();
            boolean isOnline = com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(context);
            if (com.navdy.client.app.tracking.SetDestinationTracker.this.favoriteType.isEmpty()) {
                com.navdy.client.app.tracking.SetDestinationTracker.this.favoriteType = com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.FAVORITE_TYPE_VALUES.getFavoriteTypeValue(this.val$destination);
            }
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.NEXT_TRIP, java.lang.String.valueOf(!isConnected));
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.ONLINE, java.lang.String.valueOf(isOnline));
            attributes.put("Type", com.navdy.client.app.tracking.SetDestinationTracker.this.destinationType);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE, com.navdy.client.app.tracking.SetDestinationTracker.this.sourceValue);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.FAVORITE_TYPE, com.navdy.client.app.tracking.SetDestinationTracker.this.favoriteType);
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SET_DESTINATION, attributes);
            com.navdy.client.app.tracking.SetDestinationTracker.this.resetValues();
        }
    }

    private SetDestinationTracker() {
    }

    public static com.navdy.client.app.tracking.SetDestinationTracker getInstance() {
        return instance;
    }

    public void setDestinationType(java.lang.String destinationType2) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationType2)) {
            this.destinationType = destinationType2;
        }
    }

    public void setSourceValue(java.lang.String sourceValue2) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(sourceValue2)) {
            this.sourceValue = sourceValue2;
        }
    }

    public void setFavoriteType(java.lang.String favoriteType2) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(favoriteType2)) {
            this.favoriteType = favoriteType2;
        }
    }

    public java.lang.String getDestinationType() {
        return this.destinationType;
    }

    public java.lang.String getSourceValue() {
        return this.sourceValue;
    }

    public java.lang.String getFavoriteType() {
        return this.favoriteType;
    }

    public void resetValues() {
        this.destinationType = "";
        this.sourceValue = "";
        this.favoriteType = "";
    }

    public void tagSetDestinationEvent(com.navdy.client.app.framework.models.Destination destination) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.tracking.SetDestinationTracker.Anon1(destination), 3);
    }
}
