package com.navdy.client.app;

public class NavdyApplication extends android.support.multidex.MultiDexApplication {
    public static final boolean ENG_BUILD = false;
    private static final java.lang.String HAS_KILLED = "crashed_mapengine_kill";
    private static final java.lang.String LOG_FILENAME = "PhoneLogs";
    private static final int MAX_LOG_FILES = 12;
    private static final int MAX_LOG_SIZE = 262144;
    private static final java.lang.String NASTY_HERE_ANALYTICS_CLASS = "com.here.sdk.analytics.internal.a$Anon1";
    private static final java.lang.String TAG = com.navdy.client.app.NavdyApplication.class.getName();
    private static com.navdy.service.library.log.MemoryMapFileAppender memoryMapFileAppender = null;
    private static android.content.Context sAppContext;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(TAG);
    @javax.inject.Inject
    com.navdy.client.app.framework.util.TTSAudioRouter mAudioRouter;
    @javax.inject.Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager;
    private dagger.ObjectGraph mObjectGraph;

    class Anon1 implements java.lang.Thread.UncaughtExceptionHandler {
        final /* synthetic */ java.lang.Thread.UncaughtExceptionHandler val$defaultHandler;

        Anon1(java.lang.Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.val$defaultHandler = uncaughtExceptionHandler;
        }

        public void uncaughtException(java.lang.Thread thread, java.lang.Throwable exception) {
            com.navdy.client.app.NavdyApplication.sLogger.e("Uncaught exception - " + thread.getName() + ":" + thread.getId() + " ui thread id:" + android.os.Looper.getMainLooper().getThread().getId(), exception);
            com.navdy.client.app.NavdyApplication.sLogger.i("closing logger");
            com.navdy.service.library.log.Logger.close();
            this.val$defaultHandler.uncaughtException(thread, exception);
        }
    }

    class Anon2 implements android.app.Application.ActivityLifecycleCallbacks {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                try {
                    com.localytics.android.Localytics.openSession();
                    com.localytics.android.Localytics.upload();
                } catch (Throwable t) {
                    com.navdy.client.app.NavdyApplication.sLogger.e(t);
                }
            }
        }

        /* renamed from: com.navdy.client.app.NavdyApplication$Anon2$Anon2 reason: collision with other inner class name */
        class C0040Anon2 implements java.lang.Runnable {
            C0040Anon2() {
            }

            public void run() {
                try {
                    com.localytics.android.Localytics.closeSession();
                    com.localytics.android.Localytics.upload();
                } catch (Throwable t) {
                    com.navdy.client.app.NavdyApplication.sLogger.e(t);
                }
            }
        }

        Anon2() {
        }

        public void onActivityCreated(android.app.Activity activity, android.os.Bundle bundle) {
        }

        public void onActivityStarted(android.app.Activity activity) {
        }

        public void onActivityResumed(android.app.Activity activity) {
            com.navdy.client.app.framework.LocalyticsManager.runWhenReady(new com.navdy.client.app.NavdyApplication.Anon2.Anon1());
        }

        public void onActivityPaused(android.app.Activity activity) {
            com.navdy.client.app.framework.LocalyticsManager.runWhenReady(new com.navdy.client.app.NavdyApplication.Anon2.C0040Anon2());
        }

        public void onActivityStopped(android.app.Activity activity) {
        }

        public void onActivitySaveInstanceState(android.app.Activity activity, android.os.Bundle bundle) {
        }

        public void onActivityDestroyed(android.app.Activity activity) {
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.client.app.NavdyApplication.sLogger.v("init the TTS audio router");
            com.navdy.client.app.NavdyApplication.this.mAudioRouter.init();
            com.navdy.client.app.NavdyApplication.sLogger.v("init localytics");
            com.navdy.client.app.framework.LocalyticsManager.getInstance().initLocalytics();
            com.navdy.client.app.NavdyApplication.sLogger.v("Setting crashreporter user (again) now that the Localytics engine is initialized.");
            com.navdy.client.app.framework.util.CrashReporter.getInstance().setUser(com.navdy.client.app.tracking.Tracker.getUserId());
            com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
            com.navdy.client.app.framework.i18n.I18nManager.getInstance();
            com.navdy.client.app.NavdyApplication.this.logIfKilled();
            com.navdy.client.app.ui.settings.SettingsUtils.checkObdSettingIfBuildVersionChanged();
            if (com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_TICKETS_EXIST, false)) {
                com.navdy.client.app.framework.util.SupportTicketService.submitTicketsIfConditionsAreMet();
            }
            com.navdy.client.app.NavdyApplication.this.initSharedPrefs();
            com.navdy.client.app.service.DataCollectionService.getInstance().startService(com.navdy.client.app.NavdyApplication.sAppContext);
        }
    }

    public static class AppTaskQueue extends com.navdy.service.library.util.TaskQueue {
        public static final int CALL_SERIAL = 4;
        public static final int DESTINATIONS_PROCESSOR_SERIAL = 13;
        public static final int GENERAL_SERIAL = 12;
        public static final int GLANCE_SERIAL = 7;
        public static final int HERE_INIT_SERIAL = 11;
        public static final int IMAGE_LOADING = 6;
        public static final int MUSIC_EVENTS = 10;
        public static final int NETWORK = 3;
        public static final int NETWORK_REACHABILITY = 5;
        public static final int OTA = 2;
        public static final int ROUTE_CALCULATION = 8;
        public static final int ZENDESK_UPLOAD = 9;
    }

    public void onCreate() {
        super.onCreate();
        sAppContext = this;
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.service.library.util.SystemUtils.getProcessName(sAppContext, android.os.Process.myPid()), getString(com.navdy.client.R.string.map_service_process_name))) {
            initApp();
            sLogger.v("Starting app on " + android.os.Build.BRAND + "," + android.os.Build.HARDWARE + "," + android.os.Build.MODEL);
            try {
                registerActivityLifecycleCallbacks(new com.localytics.android.LocalyticsActivityLifecycleCallbacks(this));
            } catch (java.lang.Exception e) {
                sLogger.e("Unable to register for LocalyticsActivityLifecycleCallbacks.", e);
            }
        }
    }

    public void onTrimMemory(int level) {
        if (level == 80) {
            com.navdy.client.app.service.DataCollectionService.getInstance().handleLowMemory();
        }
        super.onTrimMemory(level);
    }

    protected void attachBaseContext(android.content.Context base) {
        super.attachBaseContext(base);
        long l1 = android.os.SystemClock.elapsedRealtime();
        android.support.multidex.MultiDex.install(this);
        android.util.Log.v(TAG, "Time to install Multidex: " + (android.os.SystemClock.elapsedRealtime() - l1));
    }

    public void registerActivityLifecycleCallbacks(android.app.Application.ActivityLifecycleCallbacks callback) {
        java.lang.String className = "";
        if (callback != null) {
            className = callback.getClass().getName();
        }
        sLogger.d("Activity Callback " + className);
        if (NASTY_HERE_ANALYTICS_CLASS.equals(className)) {
            sLogger.d("Preventing " + className + " as an activity callback");
        } else {
            super.registerActivityLifecycleCallbacks(callback);
        }
    }

    public static android.content.Context getAppContext() {
        return sAppContext;
    }

    @android.annotation.SuppressLint({"CommitPrefEdits"})
    public static void kill() {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putInt(HAS_KILLED, 1).commit();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private static void initLogger() {
        java.io.File internalStorageFolder = getAppContext().getFilesDir();
        java.lang.String internalStorageFolderPath = "";
        if (internalStorageFolder != null && internalStorageFolder.exists()) {
            internalStorageFolderPath = internalStorageFolder.getPath();
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(internalStorageFolderPath)) {
            memoryMapFileAppender = new com.navdy.service.library.log.MemoryMapFileAppender(sAppContext, internalStorageFolderPath, LOG_FILENAME, android.support.v4.media.session.PlaybackStateCompat.ACTION_SET_REPEAT_MODE, 12, false);
            com.navdy.service.library.log.Logger.init(new com.navdy.service.library.log.LogAppender[]{new com.navdy.service.library.log.LogcatAppender(), memoryMapFileAppender});
            return;
        }
        com.navdy.service.library.log.Logger.init(new com.navdy.service.library.log.LogAppender[]{new com.navdy.service.library.log.LogcatAppender()});
    }

    @android.support.annotation.NonNull
    public static java.util.ArrayList<java.io.File> getLogFiles() {
        if (memoryMapFileAppender != null) {
            return memoryMapFileAppender.getLogFiles();
        }
        return new java.util.ArrayList<>(0);
    }

    private void installCrashHandler() {
        java.lang.Thread.setDefaultUncaughtExceptionHandler(new com.navdy.client.app.NavdyApplication.Anon1(java.lang.Thread.getDefaultUncaughtExceptionHandler()));
    }

    private void initTaskManager() {
        sLogger.v(":initializing taskMgr");
        com.navdy.service.library.task.TaskManager taskManager = com.navdy.service.library.task.TaskManager.getInstance();
        taskManager.addTaskQueue(1, 3);
        taskManager.addTaskQueue(2, 1);
        taskManager.addTaskQueue(3, 3);
        taskManager.addTaskQueue(4, 1);
        taskManager.addTaskQueue(5, 1);
        taskManager.addTaskQueue(6, 3);
        taskManager.addTaskQueue(7, 1);
        taskManager.addTaskQueue(8, 1);
        taskManager.addTaskQueue(9, 1);
        taskManager.addTaskQueue(10, 1);
        taskManager.addTaskQueue(11, 1);
        taskManager.addTaskQueue(12, 1);
        taskManager.addTaskQueue(13, 1);
        taskManager.init();
    }

    private void registerActivityLifecycleCallbacks() {
        registerActivityLifecycleCallbacks(new com.navdy.client.app.NavdyApplication.Anon2());
    }

    private void initApp() {
        com.navdy.client.app.framework.util.IMMLeaks.fixFocusedViewLeak(this);
        this.mObjectGraph = dagger.ObjectGraph.create(new com.navdy.client.app.framework.ProdModule(this));
        this.mObjectGraph.inject(this);
        android.util.Log.v(TAG, "Starting the HttpManager");
        android.util.Log.v(TAG, "install crash handler");
        installCrashHandler();
        android.util.Log.v(TAG, "init logger");
        initLogger();
        java.lang.String userId = com.navdy.client.app.tracking.Tracker.getUserId();
        if (!isDeveloperBuild()) {
            sLogger.v("register crashlytics");
            com.navdy.client.app.framework.util.CrashReporter.getInstance().installCrashHandler(getAppContext(), userId);
        }
        sLogger.v("init taskmgr");
        initTaskManager();
        registerActivityLifecycleCallbacks();
        long time = android.os.SystemClock.elapsedRealtime();
        sLogger.v("init AppInstance");
        com.navdy.client.app.framework.AppInstance.getInstance().initializeApp();
        sLogger.d("It took " + (android.os.SystemClock.elapsedRealtime() - time) + "ms. to initialize App.");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.NavdyApplication.Anon3(), 4);
    }

    private void logIfKilled() {
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (sharedPreferences.getInt(HAS_KILLED, 0) > 0) {
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.HAS_KILLED);
            sharedPreferences.edit().putInt(HAS_KILLED, 0).apply();
        }
    }

    private void initSharedPrefs() {
        int prefVersion = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getInt(com.navdy.client.app.ui.settings.SettingsConstants.SHARED_PREF_VERSION, 0);
        if (prefVersion > 1) {
            com.navdy.client.app.ui.settings.SettingsUtils.onSharedPrefsDowngrade(prefVersion, 1);
        } else if (prefVersion < 1) {
            com.navdy.client.app.ui.settings.SettingsUtils.onSharedPrefsUpgrade(prefVersion, 1);
        }
    }

    public java.lang.Object getSystemService(@android.support.annotation.NonNull java.lang.String name) {
        if (com.navdy.client.app.framework.Injector.matchesService(name)) {
            return this.mObjectGraph;
        }
        return super.getSystemService(name);
    }

    public static boolean isDeveloperBuild() {
        return false;
    }
}
