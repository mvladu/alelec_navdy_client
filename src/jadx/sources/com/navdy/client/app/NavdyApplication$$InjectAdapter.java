package com.navdy.client.app;

public final class NavdyApplication$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.NavdyApplication> implements javax.inject.Provider<com.navdy.client.app.NavdyApplication>, dagger.MembersInjector<com.navdy.client.app.NavdyApplication> {
    private dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> mAudioRouter;
    private dagger.internal.Binding<com.navdy.service.library.network.http.IHttpManager> mHttpManager;

    public NavdyApplication$$InjectAdapter() {
        super("com.navdy.client.app.NavdyApplication", "members/com.navdy.client.app.NavdyApplication", false, com.navdy.client.app.NavdyApplication.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.client.app.NavdyApplication.class, getClass().getClassLoader());
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", com.navdy.client.app.NavdyApplication.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
        injectMembersBindings.add(this.mAudioRouter);
    }

    public com.navdy.client.app.NavdyApplication get() {
        com.navdy.client.app.NavdyApplication result = new com.navdy.client.app.NavdyApplication();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.NavdyApplication object) {
        object.mHttpManager = (com.navdy.service.library.network.http.IHttpManager) this.mHttpManager.get();
        object.mAudioRouter = (com.navdy.client.app.framework.util.TTSAudioRouter) this.mAudioRouter.get();
    }
}
