package com.navdy.client.app.ui.settings;

public final class AudioSettingsActivity$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.ui.settings.AudioSettingsActivity> implements javax.inject.Provider<com.navdy.client.app.ui.settings.AudioSettingsActivity>, dagger.MembersInjector<com.navdy.client.app.ui.settings.AudioSettingsActivity> {
    private dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> mAudioRouter;
    private dagger.internal.Binding<android.content.SharedPreferences> mSharedPrefs;
    private dagger.internal.Binding<com.navdy.client.app.ui.base.BaseEditActivity> supertype;

    public AudioSettingsActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.settings.AudioSettingsActivity", "members/com.navdy.client.app.ui.settings.AudioSettingsActivity", false, com.navdy.client.app.ui.settings.AudioSettingsActivity.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mSharedPrefs = linker.requestBinding("android.content.SharedPreferences", com.navdy.client.app.ui.settings.AudioSettingsActivity.class, getClass().getClassLoader());
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", com.navdy.client.app.ui.settings.AudioSettingsActivity.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseEditActivity", com.navdy.client.app.ui.settings.AudioSettingsActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mSharedPrefs);
        injectMembersBindings.add(this.mAudioRouter);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.client.app.ui.settings.AudioSettingsActivity get() {
        com.navdy.client.app.ui.settings.AudioSettingsActivity result = new com.navdy.client.app.ui.settings.AudioSettingsActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.ui.settings.AudioSettingsActivity object) {
        object.mSharedPrefs = (android.content.SharedPreferences) this.mSharedPrefs.get();
        object.mAudioRouter = (com.navdy.client.app.framework.util.TTSAudioRouter) this.mAudioRouter.get();
        this.supertype.injectMembers(object);
    }
}
