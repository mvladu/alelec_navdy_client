package com.navdy.client.app.ui.firstlaunch;

public class AppSetupScreen implements android.os.Parcelable {
    public static final android.os.Parcelable.Creator<com.navdy.client.app.ui.firstlaunch.AppSetupScreen> CREATOR = new com.navdy.client.app.ui.firstlaunch.AppSetupScreen.Anon1();
    public int blueTextRes;
    public int buttonFailRes;
    public int buttonRes;
    public int descriptionFailRes;
    public int descriptionRes;
    public int hudRes;
    public int hudResFail;
    public boolean isMandatory;
    public com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType screenType = com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.UNKNOWN;
    public int titleFailRes;
    public int titleRes;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.client.app.ui.firstlaunch.AppSetupScreen> {
        Anon1() {
        }

        public com.navdy.client.app.ui.firstlaunch.AppSetupScreen createFromParcel(android.os.Parcel in) {
            return new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(in);
        }

        public com.navdy.client.app.ui.firstlaunch.AppSetupScreen[] newArray(int size) {
            return new com.navdy.client.app.ui.firstlaunch.AppSetupScreen[size];
        }
    }

    public enum ScreenType {
        UNKNOWN(-1),
        PROFILE(0),
        BLUETOOTH(1),
        BLUETOOTH_SUCCESS(2),
        NOTIFICATIONS(3),
        ACCESS_FINE_LOCATION(4),
        USE_MICROPHONE(5),
        READ_CONTACTS(6),
        RECEIVE_SMS(7),
        CALL_PHONE(8),
        READ_CALENDAR(9),
        WRITE_EXTERNAL_STORAGE(10),
        END(11);
        
        int type;

        private ScreenType(int type2) {
            this.type = type2;
        }

        public static com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType fromValue(int type2) {
            switch (type2) {
                case 0:
                    return PROFILE;
                case 1:
                    return BLUETOOTH;
                case 2:
                    return BLUETOOTH_SUCCESS;
                case 3:
                    return NOTIFICATIONS;
                case 4:
                    return ACCESS_FINE_LOCATION;
                case 5:
                    return READ_CONTACTS;
                case 6:
                    return RECEIVE_SMS;
                case 7:
                    return CALL_PHONE;
                case 8:
                    return READ_CALENDAR;
                case 9:
                    return WRITE_EXTERNAL_STORAGE;
                case 10:
                    return END;
                default:
                    return UNKNOWN;
            }
        }

        public int getValue() {
            return this.type;
        }
    }

    public AppSetupScreen() {
    }

    public AppSetupScreen(int hudRes2, int hudResFail2, int titleRes2, int descriptionRes2, int blueTextRes2, int buttonRes2, int titleFailRes2, int descriptionFailRes2, int buttonFailRes2, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType screenType2, boolean isMandatory2) {
        this.hudRes = hudRes2;
        this.hudResFail = hudResFail2;
        this.titleRes = titleRes2;
        this.descriptionRes = descriptionRes2;
        this.blueTextRes = blueTextRes2;
        this.buttonRes = buttonRes2;
        this.titleFailRes = titleFailRes2;
        this.descriptionFailRes = descriptionFailRes2;
        this.buttonFailRes = buttonFailRes2;
        this.screenType = screenType2;
        this.isMandatory = isMandatory2;
    }

    public AppSetupScreen(int hudRes2, int hudResFail2, int titleRes2, int descriptionRes2, int buttonRes2, int titleFailRes2, int descriptionFailRes2, int buttonFailRes2, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType screenType2, boolean isMandatory2) {
        this.hudRes = hudRes2;
        this.hudResFail = hudResFail2;
        this.titleRes = titleRes2;
        this.descriptionRes = descriptionRes2;
        this.buttonRes = buttonRes2;
        this.titleFailRes = titleFailRes2;
        this.descriptionFailRes = descriptionFailRes2;
        this.buttonFailRes = buttonFailRes2;
        this.screenType = screenType2;
        this.isMandatory = isMandatory2;
    }

    public AppSetupScreen(int hudRes2, int titleRes2, int descriptionRes2, int buttonRes2, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType screenType2, boolean isMandatory2) {
        this.hudRes = hudRes2;
        this.titleRes = titleRes2;
        this.descriptionRes = descriptionRes2;
        this.buttonRes = buttonRes2;
        this.screenType = screenType2;
        this.isMandatory = isMandatory2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeInt(this.hudRes);
        dest.writeInt(this.hudResFail);
        dest.writeInt(this.titleRes);
        dest.writeInt(this.descriptionRes);
        dest.writeInt(this.blueTextRes);
        dest.writeInt(this.buttonRes);
        dest.writeInt(this.titleFailRes);
        dest.writeInt(this.descriptionFailRes);
        dest.writeInt(this.buttonFailRes);
        dest.writeInt(this.screenType.getValue());
        dest.writeInt(this.isMandatory ? 1 : 0);
    }

    protected AppSetupScreen(android.os.Parcel in) {
        this.hudRes = in.readInt();
        this.hudResFail = in.readInt();
        this.titleRes = in.readInt();
        this.descriptionRes = in.readInt();
        this.blueTextRes = in.readInt();
        this.buttonRes = in.readInt();
        this.titleFailRes = in.readInt();
        this.descriptionFailRes = in.readInt();
        this.buttonFailRes = in.readInt();
        this.screenType = com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.fromValue(in.readInt());
        this.isMandatory = in.readInt() != 0;
    }
}
