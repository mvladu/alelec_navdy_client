package com.navdy.client.app.ui.routing;

public final class RoutingActivity$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.ui.routing.RoutingActivity> implements javax.inject.Provider<com.navdy.client.app.ui.routing.RoutingActivity>, dagger.MembersInjector<com.navdy.client.app.ui.routing.RoutingActivity> {
    private dagger.internal.Binding<com.navdy.client.app.framework.servicehandler.NetworkStatusManager> networkStatusManager;
    private dagger.internal.Binding<com.navdy.client.app.ui.base.BaseToolbarActivity> supertype;

    public RoutingActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.routing.RoutingActivity", "members/com.navdy.client.app.ui.routing.RoutingActivity", false, com.navdy.client.app.ui.routing.RoutingActivity.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.networkStatusManager = linker.requestBinding("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", com.navdy.client.app.ui.routing.RoutingActivity.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseToolbarActivity", com.navdy.client.app.ui.routing.RoutingActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.networkStatusManager);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.client.app.ui.routing.RoutingActivity get() {
        com.navdy.client.app.ui.routing.RoutingActivity result = new com.navdy.client.app.ui.routing.RoutingActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.ui.routing.RoutingActivity object) {
        object.networkStatusManager = (com.navdy.client.app.framework.servicehandler.NetworkStatusManager) this.networkStatusManager.get();
        this.supertype.injectMembers(object);
    }
}
