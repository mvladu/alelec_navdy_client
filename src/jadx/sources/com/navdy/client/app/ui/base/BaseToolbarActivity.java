package com.navdy.client.app.ui.base;

public abstract class BaseToolbarActivity extends com.navdy.client.app.ui.base.BaseActivity {

    public class ToolbarBuilder {
        java.lang.String title = "";

        public ToolbarBuilder() {
        }

        public com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder title(java.lang.String title2) {
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(title2)) {
                this.title = title2;
            }
            return this;
        }

        public com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder title(@android.support.annotation.StringRes int title2) {
            java.lang.String titleString = "";
            if (title2 > 0) {
                titleString = com.navdy.client.app.ui.base.BaseToolbarActivity.this.getResources().getString(title2);
            }
            return title(titleString);
        }

        public android.support.v7.widget.Toolbar build() {
            com.navdy.client.app.ui.base.BaseToolbarActivity.this.setTitle(this.title);
            return com.navdy.client.app.ui.base.BaseToolbarActivity.this.initActionBar(com.navdy.client.R.id.my_toolbar);
        }
    }

    private android.support.v7.widget.Toolbar initActionBar(@android.support.annotation.IdRes int toolbarResId) {
        android.support.v7.widget.Toolbar myToolbar = (android.support.v7.widget.Toolbar) findViewById(toolbarResId);
        setSupportActionBar(myToolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        return myToolbar;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
