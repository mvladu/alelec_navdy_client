package com.navdy.client.app.ui.firstlaunch;

public class InstallLensCheckFragment extends com.navdy.client.app.ui.firstlaunch.InstallCardFragment {
    public static final int ANIMATION_DURATION = 2500;
    private static final int[] MEDIUM_TALL_MOUNT_IMAGES = {com.navdy.client.R.drawable.img_installation_lens_medium_tall_high, com.navdy.client.R.drawable.img_installation_lens_medium_tall_success, com.navdy.client.R.drawable.img_installation_lens_medium_tall_low};
    private static final int[] SHORT_MOUNT_2016_IMAGES = {com.navdy.client.R.drawable.img_installation_lens_2016_short_high, com.navdy.client.R.drawable.img_installation_lens_2016_short_success, com.navdy.client.R.drawable.img_installation_lens_2016_short_low};
    private static final int[] SHORT_MOUNT_2017_IMAGES = {com.navdy.client.R.drawable.img_installation_lens_2017_short_high, com.navdy.client.R.drawable.img_installation_lens_2017_short_success, com.navdy.client.R.drawable.img_installation_lens_2017_short_low};
    protected static final boolean VERBOSE = false;
    protected static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.class);
    com.navdy.client.app.framework.util.ImageCache imageCache = new com.navdy.client.app.framework.util.ImageCache();
    private java.util.concurrent.atomic.AtomicBoolean isAnimating = new java.util.concurrent.atomic.AtomicBoolean(false);
    private com.navdy.client.app.framework.models.MountInfo.MountType mountType;
    com.navdy.client.app.ui.VerticalViewPager mountViewPager;
    private com.navdy.client.app.ui.ImageResourcePagerAdaper mountViewPagerAdapter;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            int position = com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.this.mountViewPager.getCurrentItem() + 1;
            if (position >= com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.this.mountViewPagerAdapter.getCount()) {
                position = 0;
            }
            com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.this.mountViewPager.setCurrentItem(position);
            com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.this.handler.postDelayed(this, 2500);
        }
    }

    private class LensCheckPageListener implements android.support.v4.view.ViewPager.OnPageChangeListener {
        final android.view.View[] dots;

        private LensCheckPageListener() {
            this.dots = new android.view.View[]{com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.this.rootView.findViewById(com.navdy.client.R.id.lens_too_high_dot), com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.this.rootView.findViewById(com.navdy.client.R.id.lens_ok_dot), com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.this.rootView.findViewById(com.navdy.client.R.id.lens_too_low_dot)};
        }

        /* synthetic */ LensCheckPageListener(com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment x0, com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.Anon1 x1) {
            this();
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            int i = 0;
            while (i < this.dots.length) {
                if (this.dots[i] != null) {
                    this.dots[i].setVisibility(position == i ? 0 : 4);
                }
                i++;
            }
        }

        public void onPageScrollStateChanged(int state) {
        }
    }

    public InstallLensCheckFragment() {
        setLayoutId(com.navdy.client.R.layout.fle_install_lens_check);
    }

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, @android.support.annotation.Nullable android.view.ViewGroup container, @android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        this.rootView = inflater.inflate(com.navdy.client.R.layout.fle_install_lens_check, container, false);
        this.mountViewPager = (com.navdy.client.app.ui.VerticalViewPager) this.rootView.findViewById(com.navdy.client.R.id.mount_illustration_view_pager);
        updateMountType();
        return this.rootView;
    }

    public void setMountType(com.navdy.client.app.framework.models.MountInfo.MountType type) {
        this.mountType = type;
    }

    public void updateMountType() {
        if (this.rootView != null) {
            android.widget.TextView textOk = (android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.lens_ok_title);
            android.widget.TextView textTooLow = (android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.lens_too_low_title);
            ((android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.lens_too_high_title)).setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.lens_too_high));
            textOk.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.lens_ok));
            textTooLow.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.lens_too_low));
            if (this.mountViewPager != null) {
                if (this.mountType != com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
                    this.mountViewPagerAdapter = new com.navdy.client.app.ui.ImageResourcePagerAdaper(getContext(), MEDIUM_TALL_MOUNT_IMAGES, this.imageCache);
                } else if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box"), "Old_Box")) {
                    this.mountViewPagerAdapter = new com.navdy.client.app.ui.ImageResourcePagerAdaper(getContext(), SHORT_MOUNT_2016_IMAGES, this.imageCache);
                } else {
                    this.mountViewPagerAdapter = new com.navdy.client.app.ui.ImageResourcePagerAdaper(getContext(), SHORT_MOUNT_2017_IMAGES, this.imageCache);
                }
                this.mountViewPager.setAdapter(this.mountViewPagerAdapter);
                this.mountViewPager.addOnPageChangeListener(new com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.LensCheckPageListener(this, null));
                this.mountViewPager.setCurrentItem(1);
            }
            animate();
        }
    }

    private void animate() {
        if (this.mountViewPager != null && this.mountViewPagerAdapter != null && !this.isAnimating.get()) {
            this.isAnimating.set(true);
            this.handler.postDelayed(new com.navdy.client.app.ui.firstlaunch.InstallLensCheckFragment.Anon1(), 2500);
        }
    }

    public void onPause() {
        this.handler.removeCallbacksAndMessages(null);
        this.isAnimating.set(false);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        animate();
    }

    public void onDestroy() {
        this.imageCache.clearCache();
        super.onDestroy();
    }
}
