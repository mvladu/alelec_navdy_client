package com.navdy.client.app.ui;

public class ImageResourcePagerAdaper extends android.support.v4.view.PagerAdapter {
    private com.navdy.client.app.framework.util.ImageCache imageCache = new com.navdy.client.app.framework.util.ImageCache();
    protected int[] imageResourceIds = new int[0];
    protected android.view.LayoutInflater mLayoutInflater;

    public ImageResourcePagerAdaper(android.content.Context context, int[] imageResourceIds2, com.navdy.client.app.framework.util.ImageCache imageCache2) {
        this.imageResourceIds = (int[]) imageResourceIds2.clone();
        this.imageCache = imageCache2;
        this.mLayoutInflater = (android.view.LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.imageResourceIds.length;
    }

    public boolean isViewFromObject(android.view.View view, java.lang.Object object) {
        return view == object;
    }

    public java.lang.Object instantiateItem(android.view.ViewGroup container, int position) {
        android.widget.ImageView imageView = (android.widget.ImageView) this.mLayoutInflater.inflate(com.navdy.client.R.layout.full_screen_image, container, false);
        if (imageView == null) {
            return null;
        }
        com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, this.imageResourceIds[position], this.imageCache);
        container.addView(imageView);
        return imageView;
    }

    public void destroyItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        container.removeView((android.widget.ImageView) object);
    }
}
