package com.navdy.client.app.ui.routing;

public class RoutingActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener {
    private static final int HERE_MAP_PADDING = com.navdy.client.app.framework.map.MapUtils.hereMapSidePadding;
    private static final int HERE_MAP_TOP_PADDING = (com.navdy.client.app.NavdyApplication.getAppContext().getResources().getDimensionPixelSize(com.navdy.client.R.dimen.search_bar_margin) + com.navdy.client.app.framework.map.MapUtils.hereMapTopDownPadding);
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.routing.RoutingActivity.class);
    private android.widget.RelativeLayout activeTripCardFrameLayoutContainer;
    private boolean canRetry;
    private com.here.android.mpa.mapping.MapMarker destinationMarker;
    private com.navdy.client.app.ui.customviews.FluctuatorAnimatorView fluctuatorAnimatorView;
    private com.navdy.client.app.ui.customviews.Gauge gauge;
    private com.navdy.client.app.ui.base.BaseHereMapFragment hereMapFragment;
    private final com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    @javax.inject.Inject
    com.navdy.client.app.framework.servicehandler.NetworkStatusManager networkStatusManager = null;
    private android.widget.RelativeLayout offlineBanner;
    private android.widget.TextView routeName;

    class Anon1 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon1(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destination = destination;
        }

        public void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.navdy.client.app.ui.routing.RoutingActivity.this.destinationMarker = this.val$destination.getHereMapMarker();
            if (com.navdy.client.app.ui.routing.RoutingActivity.this.destinationMarker != null) {
                hereMap.addMapObject(com.navdy.client.app.ui.routing.RoutingActivity.this.destinationMarker);
            }
            com.navdy.client.app.ui.routing.RoutingActivity.this.hereMapFragment.centerOnUserLocationAndDestination(this.val$destination, com.here.android.mpa.mapping.Map.Animation.LINEAR);
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon2 extends com.here.android.mpa.mapping.MapGesture.OnGestureListener.OnGestureListenerAdapter {
        Anon2() {
        }

        public boolean onTapEvent(android.graphics.PointF pointF) {
            com.navdy.client.app.ui.routing.RoutingActivity.this.retryRoute(null);
            return super.onTapEvent(pointF);
        }
    }

    public static void startRoutingActivity(android.app.Activity activity) {
        if (activity != null) {
            activity.startActivity(new android.content.Intent(activity.getApplicationContext(), com.navdy.client.app.ui.routing.RoutingActivity.class));
        } else {
            logger.e("could not start routing activity, origin activity is null");
        }
    }

    public void retryRoute(android.view.View view) {
        if (this.canRetry) {
            this.canRetry = false;
            this.navdyRouteHandler.retryRoute();
        }
    }

    @com.squareup.otto.Subscribe
    public void appInstanceDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent deviceDisconnectedEvent) {
        logger.v("App Disconnected");
        this.handler.removeCallbacks(null);
        finish();
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        logger.v("onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.routing_activity);
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.activeTripCardFrameLayoutContainer = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.trip_overview_estimates);
        this.gauge = (com.navdy.client.app.ui.customviews.Gauge) findViewById(com.navdy.client.R.id.gauge);
        this.fluctuatorAnimatorView = (com.navdy.client.app.ui.customviews.FluctuatorAnimatorView) findViewById(com.navdy.client.R.id.fluctuator);
        this.offlineBanner = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.offline_banner);
        this.routeName = (android.widget.TextView) findViewById(com.navdy.client.R.id.route_name);
        initUi();
    }

    public void onResume() {
        logger.v("onResume");
        super.onResume();
        this.hereMapFragment.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.ROUTING);
        this.navdyRouteHandler.addListener(this);
        updateOfflineBannerVisibility();
    }

    public void onPause() {
        logger.v("onPause");
        this.navdyRouteHandler.removeListener(this);
        super.onPause();
    }

    protected void onDestroy() {
        logger.v("onDestroy");
        this.handler.removeCallbacksAndMessages(null);
        this.fluctuatorAnimatorView.stop();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed() {
        this.navdyRouteHandler.stopRouting();
        super.onBackPressed();
    }

    public void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        finish();
    }

    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.Nullable com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
        finish();
    }

    public void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        logger.v("onRouteCalculating");
        this.routeName.setText(com.navdy.client.R.string.calculating_dot_dot_dot);
        this.gauge.setVisibility(View.INVISIBLE);
        this.fluctuatorAnimatorView.setVisibility(View.VISIBLE);
        this.fluctuatorAnimatorView.start();
        this.fluctuatorAnimatorView.animate();
        this.hereMapFragment.whenReady(new com.navdy.client.app.ui.routing.RoutingActivity.Anon1(destination));
    }

    public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        if (error == com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE) {
            finish();
            return;
        }
        this.fluctuatorAnimatorView.stop();
        this.fluctuatorAnimatorView.setVisibility(View.INVISIBLE);
        this.routeName.setText(com.navdy.client.R.string.error_please_retry_route);
        this.canRetry = true;
    }

    public void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        finish();
    }

    public void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo progress) {
        finish();
    }

    public void onReroute() {
        finish();
    }

    public void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    public void onStopRoute() {
        finish();
    }

    private void initUi() {
        setUpHereMap();
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.calculating_route).build();
        this.activeTripCardFrameLayoutContainer.setVisibility(View.INVISIBLE);
    }

    private void setUpHereMap() {
        this.hereMapFragment = (com.navdy.client.app.ui.base.BaseHereMapFragment) getFragmentManager().findFragmentById(com.navdy.client.R.id.map_fragment);
        if (this.hereMapFragment == null) {
            logger.e("Calling setUpHereMap with a null hereMapFragment");
            return;
        }
        this.hereMapFragment.hide();
        this.hereMapFragment.setUsableArea(HERE_MAP_TOP_PADDING, HERE_MAP_PADDING, HERE_MAP_PADDING, HERE_MAP_PADDING);
        this.hereMapFragment.centerOnUserLocation();
        this.hereMapFragment.setMapGesture(new com.navdy.client.app.ui.routing.RoutingActivity.Anon2());
        this.hereMapFragment.show();
    }

    @com.squareup.otto.Subscribe
    public void handleReachabilityStateChange(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        if (this.offlineBanner != null) {
            this.offlineBanner.setVisibility(com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() ? 8 : 0);
        }
    }

    public void onRefreshConnectivityClick(android.view.View view) {
        com.navdy.client.app.framework.AppInstance.getInstance().checkForNetwork();
    }
}
