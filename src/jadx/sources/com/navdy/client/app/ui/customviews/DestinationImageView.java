package com.navdy.client.app.ui.customviews;

public class DestinationImageView extends android.widget.ImageView {
    private static final java.lang.String EMPTY = "";
    private static final java.lang.String SPACE = " ";
    private static int greyColor;
    private static int largeStyleTextSize;
    private static int smallStyleTextSize;
    private static android.graphics.Typeface typefaceLarge;
    private static android.graphics.Typeface typefaceSmall;
    private static boolean valuesSet;
    private static int whiteColor;
    private com.navdy.client.app.framework.util.ContactImageHelper contactImageHelper;
    private java.lang.String initials;
    private android.graphics.Paint paint;
    private com.navdy.client.app.ui.customviews.DestinationImageView.Style style;

    public enum Style {
        INITIALS,
        DEFAULT
    }

    public DestinationImageView(android.content.Context context) {
        this(context, null, 0);
    }

    public DestinationImageView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DestinationImageView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.style = com.navdy.client.app.ui.customviews.DestinationImageView.Style.DEFAULT;
        init();
    }

    private void init() {
        this.contactImageHelper = com.navdy.client.app.framework.util.ContactImageHelper.getInstance();
        if (!valuesSet) {
            valuesSet = true;
            android.content.res.Resources resources = getResources();
            greyColor = android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.material_grey_800);
            whiteColor = android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.white);
            smallStyleTextSize = (int) resources.getDimension(com.navdy.client.R.dimen.contact_image_small_text);
            largeStyleTextSize = (int) resources.getDimension(com.navdy.client.R.dimen.contact_image_large_text);
            typefaceSmall = android.graphics.Typeface.create("sans-serif-medium", 0);
            typefaceLarge = android.graphics.Typeface.create("sans-serif", 1);
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setInitials(java.lang.String initials2, com.navdy.client.app.ui.customviews.DestinationImageView.Style style2) {
        this.initials = initials2;
        this.style = style2;
        invalidate();
    }

    public void setImage(int resourceId, java.lang.String initials2, com.navdy.client.app.ui.customviews.DestinationImageView.Style style2) {
        setImageResource(resourceId);
        setInitials(initials2, style2);
    }

    public void setImage(android.graphics.Bitmap bitmap) {
        clearInitials();
        setImageBitmap(createCircleBitmap(bitmap));
    }

    public void setImage(java.lang.String name) {
        setImage(this.contactImageHelper.getResourceId(this.contactImageHelper.getContactImageIndex(name)), name);
    }

    public void setImage(int resourceId, java.lang.String name) {
        setImage(resourceId, getInitials(name), com.navdy.client.app.ui.customviews.DestinationImageView.Style.INITIALS);
    }

    public static synchronized java.lang.String getInitials(java.lang.String name) {
        java.lang.String str;
        synchronized (com.navdy.client.app.ui.customviews.DestinationImageView.class) {
            java.lang.StringBuilder builder = new java.lang.StringBuilder();
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(name)) {
                str = "";
            } else {
                java.lang.String name2 = name.trim();
                int index = name2.indexOf(SPACE);
                if (index > 0) {
                    java.lang.String first = name2.substring(0, index).trim();
                    java.lang.String last = name2.substring(index + 1).trim();
                    builder.setLength(0);
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(first)) {
                        builder.append(first.charAt(0));
                    }
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(last)) {
                        builder.append(last.charAt(0));
                    }
                    str = builder.toString().toUpperCase();
                } else if (name2.length() > 0) {
                    str = java.lang.String.valueOf(name2.charAt(0)).toUpperCase();
                } else {
                    str = "";
                }
            }
        }
        return str;
    }

    public void clearInitials() {
        this.initials = "";
        this.style = com.navdy.client.app.ui.customviews.DestinationImageView.Style.DEFAULT;
    }

    public static android.graphics.Bitmap createCircleBitmap(android.graphics.Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        int width = context.getResources().getDimensionPixelSize(com.navdy.client.R.dimen.contact_image_width);
        int height = context.getResources().getDimensionPixelSize(com.navdy.client.R.dimen.contact_image_height);
        android.graphics.Bitmap bitmap2 = android.graphics.Bitmap.createScaledBitmap(bitmap, width, height, false);
        android.graphics.Bitmap newBitmap = android.graphics.Bitmap.createBitmap(width, height, android.graphics.Bitmap.Config.ARGB_8888);
        android.graphics.Path path = new android.graphics.Path();
        path.addCircle(((float) width) / 2.0f, ((float) height) / 2.0f, java.lang.Math.min((float) width, ((float) height) / 2.0f), android.graphics.Path.Direction.CCW);
        android.graphics.Canvas canvas = new android.graphics.Canvas(newBitmap);
        canvas.clipPath(path);
        canvas.drawBitmap(bitmap2, 0.0f, 0.0f, null);
        return newBitmap;
    }

    protected void onDraw(android.graphics.Canvas canvas) {
        super.onDraw(canvas);
        if (this.style != com.navdy.client.app.ui.customviews.DestinationImageView.Style.DEFAULT) {
            int width = getWidth();
            int height = getHeight();
            if (this.initials != null) {
                this.paint.setColor(whiteColor);
                this.paint.setTextSize((float) largeStyleTextSize);
                this.paint.setTypeface(typefaceLarge);
                this.paint.setTextAlign(android.graphics.Paint.Align.CENTER);
                canvas.drawText(this.initials, ((float) width) / 2.0f, (((float) height) / 2.0f) - ((this.paint.descent() + this.paint.ascent()) / 2.0f), this.paint);
            }
        }
    }
}
