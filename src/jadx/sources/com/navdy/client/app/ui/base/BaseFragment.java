package com.navdy.client.app.ui.base;

public class BaseFragment extends android.app.Fragment implements com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider {
    private static final java.lang.String FRAGMENT_DIALOG = "DIALOG";
    protected com.navdy.client.app.ui.base.BaseActivity baseActivity;
    protected final android.os.Handler handler = new android.os.Handler();
    protected final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private volatile boolean mIsInForeground = false;
    private android.app.ProgressDialog progressDialog = null;

    public void onCreate(android.os.Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
    }

    public void onPause() {
        this.logger.v("::onPause");
        super.onPause();
        this.mIsInForeground = false;
        hideProgressDialog();
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
        this.mIsInForeground = true;
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public void onAttach(android.app.Activity activity) {
        this.logger.v("::onAttach");
        super.onAttach(activity);
        if (activity instanceof com.navdy.client.app.ui.base.BaseActivity) {
            this.baseActivity = (com.navdy.client.app.ui.base.BaseActivity) activity;
        }
    }

    public void onDetach() {
        this.logger.v("::onDetach");
        this.baseActivity = null;
        super.onDetach();
    }

    public void onDestroy() {
        this.logger.v("::onDestroy");
        dismissProgressDialog();
        super.onDestroy();
    }

    public boolean isAlive() {
        return this.baseActivity != null && !this.baseActivity.isActivityDestroyed();
    }

    public void showSimpleDialog(int id, java.lang.String title, java.lang.String message) {
        removeDialog();
        android.app.FragmentManager manager = getFragmentManager();
        if (manager != null) {
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putString("title", title);
            bundle.putString("message", message);
            com.navdy.client.app.ui.SimpleDialogFragment.newInstance(id, bundle).show(manager, FRAGMENT_DIALOG);
        }
    }

    public void removeDialog() {
        if (isInForeground()) {
            android.app.FragmentManager manager = getFragmentManager();
            if (manager != null) {
                android.app.Fragment fragment = manager.findFragmentByTag(FRAGMENT_DIALOG);
                if (fragment != null) {
                    getFragmentManager().beginTransaction().remove(fragment).commit();
                }
            }
        }
    }

    public android.app.Dialog createDialog(int id, android.os.Bundle arguments) {
        if (arguments == null) {
            return null;
        }
        if (arguments.get("title") == null && arguments.get("message") == null) {
            return null;
        }
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setCancelable(true);
        java.lang.String title = arguments.getString("title");
        if (title != null) {
            builder.setTitle(title);
        }
        java.lang.String message = arguments.getString("message");
        if (message != null) {
            builder.setMessage(message);
        }
        builder.setPositiveButton(getString(com.navdy.client.R.string.ok), null);
        return builder.create();
    }

    public void showProgressDialog() {
        this.progressDialog = com.navdy.client.app.ui.base.BaseActivity.showProgressDialog(getActivity(), this.progressDialog);
    }

    public void hideProgressDialog() {
        com.navdy.client.app.ui.base.BaseActivity.hideProgressDialog(getActivity(), this.progressDialog);
    }

    public void dismissProgressDialog() {
        com.navdy.client.app.ui.base.BaseActivity.dismissProgressDialog(getActivity(), this.progressDialog);
    }
}
