package com.navdy.client.app.ui.firstlaunch;

public class InstallIntroActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {

    class Anon1 implements android.view.View.OnClickListener {
        Anon1() {
        }

        public void onClick(android.view.View v) {
            android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (sharedPreferences != null) {
                sharedPreferences.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HAS_SKIPPED_INSTALL, true).apply();
            }
            com.navdy.client.app.ui.firstlaunch.InstallIntroActivity.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.firstlaunch.InstallIntroActivity.this.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallCompleteActivity.class));
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_install_intro);
        if (getIntent().getBooleanExtra(com.navdy.client.app.ui.settings.SettingsConstants.EXTRA_WAS_SKIPPED, false) && com.navdy.client.app.ui.firstlaunch.InstallActivity.userHasFinishedInstall() && !com.navdy.client.app.ui.firstlaunch.AppSetupActivity.userHasFinishedAppSetup()) {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.goToAppSetup(this);
        }
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.installation).build();
        android.widget.TextView alreadyInstalled = (android.widget.TextView) findViewById(com.navdy.client.R.id.already_installed);
        if (alreadyInstalled != null) {
            alreadyInstalled.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.already_installed_skip_to_app_setup));
            alreadyInstalled.setOnClickListener(new com.navdy.client.app.ui.firstlaunch.InstallIntroActivity.Anon1());
        }
    }

    public void onNextClick(android.view.View v) {
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (sharedPreferences != null) {
            sharedPreferences.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HAS_SKIPPED_INSTALL, false).apply();
        }
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.BoxPickerActivity.class));
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.READY_CHECK);
    }
}
