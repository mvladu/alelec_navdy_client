package com.navdy.client.app.ui.settings;

public class CalendarTitleViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    private android.widget.TextView name = null;

    public CalendarTitleViewHolder(android.view.View v) {
        super(v);
        if (v instanceof android.widget.TextView) {
            this.name = (android.widget.TextView) v;
        } else {
            this.name = (android.widget.TextView) v.findViewById(com.navdy.client.R.id.calendar_name);
        }
    }

    public void setName(java.lang.String name2) {
        if (this.name != null) {
            this.name.setText(name2);
        }
    }
}
