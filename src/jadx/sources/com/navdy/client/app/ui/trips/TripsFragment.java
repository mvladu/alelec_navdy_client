package com.navdy.client.app.ui.trips;

public class TripsFragment extends com.navdy.client.app.ui.base.BaseSupportFragment {
    private android.widget.CursorAdapter cursorAdaptor;

    class Anon1 extends android.widget.CursorAdapter {
        Anon1(android.content.Context x0, android.database.Cursor x1, boolean x2) {
            super(x0, x1, x2);
        }

        public android.view.View newView(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup parent) {
            return android.view.LayoutInflater.from(context).inflate(17367044, parent, false);
        }

        public void bindView(android.view.View view, android.content.Context context, android.database.Cursor cursor) {
            android.widget.TextView text1 = (android.widget.TextView) view.findViewById(16908308);
            android.widget.TextView text2 = (android.widget.TextView) view.findViewById(16908309);
            text1.setTypeface(null, 1);
            com.navdy.client.app.framework.models.Trip trip = com.navdy.client.app.providers.NavdyContentProvider.getTripsItemAt(cursor, cursor.getPosition());
            com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(trip.destinationId);
            if (destination != null) {
                java.lang.String str = "%s%n(%s)";
                java.lang.Object[] objArr = new java.lang.Object[2];
                objArr[0] = destination.isFavoriteDestination() ? destination.getFavoriteLabel() : destination.name;
                objArr[1] = destination.getAddressForDisplay();
                text1.setText(java.lang.String.format(str, objArr));
            } else {
                text1.setText(java.lang.String.format("Unable to find destination: %s", new java.lang.Object[]{java.lang.Integer.valueOf(trip.destinationId)}));
            }
            long startTime = trip.startTime;
            text2.setText(java.lang.String.format(java.util.Locale.getDefault(), "Start: %s (odo: %d)%n         %d + %d%n          lat: %f long: %f%nend: %s (odo: %d)%n          lat: %f long: %f%narrived: %d | trip number: %d | id: %d", new java.lang.Object[]{com.navdy.client.app.ui.trips.TripsFragment.this.getDate(startTime), java.lang.Integer.valueOf(trip.startOdometer), java.lang.Long.valueOf(startTime), java.lang.Integer.valueOf(trip.offset), java.lang.Double.valueOf(trip.startLat), java.lang.Double.valueOf(trip.startLong), com.navdy.client.app.ui.trips.TripsFragment.this.getDate(trip.endTime), java.lang.Integer.valueOf(trip.endOdometer), java.lang.Double.valueOf(trip.endLat), java.lang.Double.valueOf(trip.endLong), java.lang.Long.valueOf(trip.arrivedAtDestination), java.lang.Long.valueOf(trip.tripNumber), java.lang.Integer.valueOf(trip.id)}));
        }
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.hs_fragment_trips, container, false);
        this.cursorAdaptor = new com.navdy.client.app.ui.trips.TripsFragment.Anon1(getContext(), com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor(), true);
        ((android.widget.ListView) rootView.findViewById(com.navdy.client.R.id.list)).setAdapter(this.cursorAdaptor);
        return rootView;
    }

    public java.lang.String getDate(long milliseconds) {
        if (milliseconds > 0) {
            return new java.util.Date(milliseconds).toString();
        }
        return "never";
    }

    public void onDestroy() {
        this.cursorAdaptor.changeCursor(null);
        super.onDestroy();
    }
}
