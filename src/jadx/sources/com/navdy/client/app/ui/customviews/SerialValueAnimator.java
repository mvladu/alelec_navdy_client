package com.navdy.client.app.ui.customviews;

public class SerialValueAnimator {
    private com.navdy.client.app.ui.customviews.DefaultAnimationListener animationListener = new com.navdy.client.app.ui.customviews.SerialValueAnimator.Anon1();
    private com.navdy.client.app.ui.customviews.SerialValueAnimator.SerialValueAnimatorAdapter mAdapter;
    private boolean mAnimationRunning;
    private android.animation.ValueAnimator mAnimator;
    private int mDuration;
    private int mReverseDuration = 100;
    private java.util.Queue<java.lang.Float> mValueQueue = new java.util.LinkedList();

    class Anon1 extends com.navdy.client.app.ui.customviews.DefaultAnimationListener {
        Anon1() {
        }

        public void onAnimationEnd(android.animation.Animator animation) {
            if (com.navdy.client.app.ui.customviews.SerialValueAnimator.this.mValueQueue.size() == 0) {
                com.navdy.client.app.ui.customviews.SerialValueAnimator.this.mAnimationRunning = false;
                com.navdy.client.app.ui.customviews.SerialValueAnimator.this.mAnimator = null;
                return;
            }
            com.navdy.client.app.ui.customviews.SerialValueAnimator.this.animate(((java.lang.Float) com.navdy.client.app.ui.customviews.SerialValueAnimator.this.mValueQueue.remove()).floatValue());
        }
    }

    class Anon2 implements android.animation.ValueAnimator.AnimatorUpdateListener {
        Anon2() {
        }

        public void onAnimationUpdate(android.animation.ValueAnimator animation) {
            com.navdy.client.app.ui.customviews.SerialValueAnimator.this.mAdapter.setValue(((java.lang.Float) animation.getAnimatedValue()).floatValue());
        }
    }

    public interface SerialValueAnimatorAdapter {
        float getValue();

        void setValue(float f);
    }

    public SerialValueAnimator(com.navdy.client.app.ui.customviews.SerialValueAnimator.SerialValueAnimatorAdapter adapter, int animationDuration) {
        this.mAdapter = adapter;
        this.mDuration = animationDuration;
        if (adapter == null) {
            throw new java.lang.IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }

    public SerialValueAnimator(com.navdy.client.app.ui.customviews.SerialValueAnimator.SerialValueAnimatorAdapter adapter, int animationDuration, int reverseAnimationDuration) {
        this.mAdapter = adapter;
        this.mDuration = animationDuration;
        this.mReverseDuration = reverseAnimationDuration;
        if (adapter == null) {
            throw new java.lang.IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }

    public void setValue(float val) {
        if (this.mAdapter.getValue() == val) {
            this.mAdapter.setValue(val);
        } else if (this.mAnimationRunning) {
            this.mValueQueue.add(java.lang.Float.valueOf(val));
        } else {
            this.mAnimationRunning = true;
            animate(val);
        }
    }

    public void setAnimationSpeeds(int forwardAnimationDuration, int reverseAnimationDuration) {
        this.mDuration = forwardAnimationDuration;
        this.mReverseDuration = reverseAnimationDuration;
    }

    private void animate(float value) {
        this.mAnimator = android.animation.ValueAnimator.ofFloat(new float[]{this.mAdapter.getValue(), value});
        if (this.mAdapter.getValue() > value) {
            this.mAnimator.setDuration((long) this.mReverseDuration);
        } else {
            this.mAnimator.setDuration((long) this.mDuration);
        }
        this.mAnimator.setInterpolator(new android.view.animation.LinearInterpolator());
        this.mAnimator.addUpdateListener(new com.navdy.client.app.ui.customviews.SerialValueAnimator.Anon2());
        this.mAnimator.addListener(this.animationListener);
        this.mAnimator.start();
    }

    public void release() {
        this.mValueQueue.clear();
        if (this.mAnimator != null) {
            this.mAnimator.removeAllListeners();
            this.mAnimator.removeAllUpdateListeners();
            this.mAnimator.cancel();
        }
        this.mAnimator = null;
        this.mAnimationRunning = false;
    }
}
