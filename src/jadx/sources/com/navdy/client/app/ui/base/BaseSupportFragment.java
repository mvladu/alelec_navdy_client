package com.navdy.client.app.ui.base;

public class BaseSupportFragment extends android.support.v4.app.Fragment implements com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider {
    protected com.navdy.client.app.ui.base.BaseActivity baseActivity;
    protected final android.os.Handler handler = new android.os.Handler();
    protected final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private volatile boolean mIsInForeground = false;
    private android.app.ProgressDialog progressDialog = null;

    public void onCreate(android.os.Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
    }

    public void onPause() {
        this.logger.v("::onPause");
        super.onPause();
        this.mIsInForeground = false;
        hideProgressDialog();
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
        this.mIsInForeground = true;
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public void onAttach(android.app.Activity activity) {
        this.logger.v("::onAttach");
        super.onAttach(activity);
        if (activity instanceof com.navdy.client.app.ui.base.BaseActivity) {
            this.baseActivity = (com.navdy.client.app.ui.base.BaseActivity) activity;
        }
    }

    public void onDetach() {
        this.logger.v("::onDetach");
        this.baseActivity = null;
        super.onDetach();
    }

    public void onDestroy() {
        this.logger.v("::onDestroy");
        dismissProgressDialog();
        super.onDestroy();
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public boolean isAlive() {
        return this.baseActivity != null && !this.baseActivity.isActivityDestroyed();
    }

    @android.support.annotation.NonNull
    protected android.graphics.Point getScreenSize() {
        return com.navdy.client.app.ui.base.BaseActivity.getScreenSize(getActivity());
    }

    public android.app.Dialog createDialog(int id, android.os.Bundle arguments) {
        return null;
    }

    public void showProgressDialog() {
        this.progressDialog = com.navdy.client.app.ui.base.BaseActivity.showProgressDialog(getActivity(), this.progressDialog);
    }

    public void hideProgressDialog() {
        com.navdy.client.app.ui.base.BaseActivity.hideProgressDialog(getActivity(), this.progressDialog);
    }

    public void dismissProgressDialog() {
        com.navdy.client.app.ui.base.BaseActivity.dismissProgressDialog(getActivity(), this.progressDialog);
    }

    public void showThisFragment(@org.jetbrains.annotations.Nullable android.app.Fragment fragment) {
        showThisFragment(fragment, getActivity());
    }

    public void hideThisFragment(@org.jetbrains.annotations.Nullable android.app.Fragment fragment) {
        hideThisFragment(fragment, getActivity());
    }

    public void hideThisFragmentImmediately(@org.jetbrains.annotations.Nullable android.app.Fragment fragment) {
        hideThisFragmentImmediately(fragment, getActivity());
    }

    public static void showThisFragment(@org.jetbrains.annotations.Nullable android.app.Fragment fragment, @org.jetbrains.annotations.Nullable android.support.v4.app.FragmentActivity fragmentActivity) {
        if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(fragmentActivity) && !isEnding(fragment) && !fragment.isVisible()) {
            fragmentActivity.getFragmentManager().beginTransaction().setCustomAnimations(17498112, 17498113).show(fragment).commit();
        }
    }

    public static void hideThisFragment(@org.jetbrains.annotations.Nullable android.app.Fragment fragment, @org.jetbrains.annotations.Nullable android.support.v4.app.FragmentActivity fragmentActivity) {
        if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(fragmentActivity) && !isEnding(fragment) && !fragment.isHidden()) {
            fragmentActivity.getFragmentManager().beginTransaction().setCustomAnimations(17498112, 17498113).hide(fragment).commitAllowingStateLoss();
        }
    }

    private void hideThisFragmentImmediately(android.app.Fragment fragment, android.support.v4.app.FragmentActivity fragmentActivity) {
        if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(fragmentActivity) && !isEnding(fragment) && !fragment.isHidden()) {
            fragmentActivity.getFragmentManager().beginTransaction().hide(fragment).commitAllowingStateLoss();
        }
    }

    public static boolean isEnding(@org.jetbrains.annotations.Nullable android.app.Fragment fragment) {
        return fragment == null || fragment.isRemoving() || fragment.isDetached();
    }

    public static boolean isEnding(@org.jetbrains.annotations.Nullable android.support.v4.app.Fragment fragment) {
        return fragment == null || fragment.isRemoving() || fragment.isDetached();
    }
}
