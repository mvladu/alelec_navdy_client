package com.navdy.client.app.ui.favorites;

public class FavoriteViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    protected android.widget.ImageView edit;
    protected android.widget.TextView firstLine;
    protected android.widget.ImageView icon;
    protected android.view.View row;
    protected android.widget.TextView secondLine;

    public FavoriteViewHolder(android.view.View itemView) {
        super(itemView);
        this.row = itemView.findViewById(com.navdy.client.R.id.card_row);
        this.icon = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.icon);
        this.firstLine = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.card_first_line);
        this.secondLine = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.card_second_line);
        this.edit = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.edit);
    }
}
