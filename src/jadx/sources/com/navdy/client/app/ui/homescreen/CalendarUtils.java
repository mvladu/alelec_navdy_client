package com.navdy.client.app.ui.homescreen;

public class CalendarUtils {
    public static final long EVENTS_STALENESS_LIMIT = 300000;
    private static final int MAX_CALENDAR_EVENTS = 20;
    private static final long TIME_LIMIT_AFTER_NOW = 172800000;
    private static final long TIME_LIMIT_BEFORE_NOW = 1800000;
    private static java.util.List<com.navdy.client.app.framework.models.CalendarEvent> cachedEvents = new java.util.ArrayList();
    private static long lastReadTime = 0;
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.CalendarUtils.class);

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$remoteDevice;

        /* renamed from: com.navdy.client.app.ui.homescreen.CalendarUtils$Anon1$Anon1 reason: collision with other inner class name */
        class C0063Anon1 implements com.navdy.client.app.ui.homescreen.CalendarUtils.CalendarEventReader {
            C0063Anon1() {
            }

            public void onCalendarEventsRead(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> calendarEvents) {
                java.util.List<com.navdy.service.library.events.calendars.CalendarEvent> calendarEventProtoObjects = new java.util.ArrayList<>();
                if (!calendarEvents.isEmpty()) {
                    int i = 0;
                    while (i < calendarEvents.size() && i < 20) {
                        com.navdy.client.app.framework.models.CalendarEvent calendarEvent = (com.navdy.client.app.framework.models.CalendarEvent) calendarEvents.get(i);
                        com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(new com.navdy.client.app.framework.models.Destination(calendarEvent.displayName, calendarEvent.location));
                        if (destination != null) {
                            calendarEvent.setDestination(destination.toProtobufDestinationObject());
                        }
                        calendarEventProtoObjects.add(calendarEvent.toProtobufObject());
                        i++;
                    }
                    com.navdy.client.app.ui.homescreen.CalendarUtils.Anon1.this.val$remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.calendars.CalendarEventUpdates((java.util.List) calendarEventProtoObjects));
                }
            }
        }

        Anon1(com.navdy.service.library.device.RemoteDevice remoteDevice) {
            this.val$remoteDevice = remoteDevice;
        }

        public void run() {
            com.navdy.client.app.ui.homescreen.CalendarUtils.logger.d("sendCalendarsToHud");
            com.navdy.client.app.ui.homescreen.CalendarUtils.forceCalendarRefresh();
            com.navdy.client.app.ui.homescreen.CalendarUtils.readCalendarEvent(new com.navdy.client.app.ui.homescreen.CalendarUtils.Anon1.C0063Anon1(), false);
        }
    }

    static class Anon2 implements com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.NavigableCoordinateWorkerFinishCallback {
        final /* synthetic */ com.navdy.client.app.ui.homescreen.CalendarUtils.CalendarEventReader val$callback;

        Anon2(com.navdy.client.app.ui.homescreen.CalendarUtils.CalendarEventReader calendarEventReader) {
            this.val$callback = calendarEventReader;
        }

        public void onFinish(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> results) {
            java.util.Iterator<com.navdy.client.app.framework.models.CalendarEvent> iterator = results.iterator();
            while (iterator.hasNext()) {
                com.navdy.client.app.framework.models.CalendarEvent event = (com.navdy.client.app.framework.models.CalendarEvent) iterator.next();
                if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(event.latLng)) {
                    iterator.remove();
                    results.remove(event);
                }
            }
            com.navdy.client.app.ui.homescreen.CalendarUtils.logger.i("Here found coordinates for " + results.size() + " Calendar Event(s)");
            this.val$callback.onCalendarEventsRead(results);
            com.navdy.client.app.ui.homescreen.CalendarUtils.setCachedEvents(results);
        }
    }

    public interface CalendarEventReader {
        void onCalendarEventsRead(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> list);
    }

    private static synchronized void setCachedEvents(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> cachedEvents2) {
        synchronized (com.navdy.client.app.ui.homescreen.CalendarUtils.class) {
            lastReadTime = java.lang.System.currentTimeMillis();
            cachedEvents = cachedEvents2;
        }
    }

    public static void sendCalendarsToHud() {
        sendCalendarsToHud(com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice());
    }

    public static void sendCalendarsToHud(com.navdy.service.library.device.RemoteDevice remoteDevice) {
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            logger.i("Not sending calendar events because not connected to HUD");
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.homescreen.CalendarUtils.Anon1(remoteDevice), 1);
        }
    }

    private static java.lang.String getSelectionForCalendarInstances(boolean mustHaveLocation) {
        long now = java.lang.System.currentTimeMillis();
        java.lang.String selection = "";
        if (mustHaveLocation) {
            selection = "eventLocation <> '' AND allDay = 0 AND ";
        }
        return selection + "begin > " + (now - 1800000) + org.droidparts.contract.SQL.AND + "begin" + " < " + (TIME_LIMIT_AFTER_NOW + now) + org.droidparts.contract.SQL.AND + "end" + " > " + now + org.droidparts.contract.SQL.AND + "selfAttendeeStatus" + " <> " + 2;
    }

    public static synchronized void forceCalendarRefresh() {
        synchronized (com.navdy.client.app.ui.homescreen.CalendarUtils.class) {
            lastReadTime = 0;
        }
    }

    public static synchronized void readCalendarEvent(com.navdy.client.app.ui.homescreen.CalendarUtils.CalendarEventReader callback, boolean buildForSuggestions) {
        synchronized (com.navdy.client.app.ui.homescreen.CalendarUtils.class) {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (callback == null) {
                logger.e("Called readCalendarEvent with a null callback! This should never happen.");
            } else {
                java.util.ArrayList<com.navdy.client.app.framework.models.CalendarEvent> events = new java.util.ArrayList<>();
                if (android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.READ_CALENDAR") != 0) {
                    logger.v("Calendar Access not permitted.");
                    callback.onCalendarEventsRead(events);
                } else {
                    long now = java.lang.System.currentTimeMillis();
                    if (now - lastReadTime < EVENTS_STALENESS_LIMIT) {
                        logger.v("It hasn't been long enough. Ignoring request");
                        callback.onCalendarEventsRead(cachedEvents);
                    } else {
                        android.database.Cursor cursor = null;
                        try {
                            android.net.Uri.Builder builder = android.provider.CalendarContract.Instances.CONTENT_URI.buildUpon();
                            android.content.ContentUris.appendId(builder, now - 1800000);
                            android.content.ContentUris.appendId(builder, TIME_LIMIT_AFTER_NOW + now);
                            android.net.Uri contentUri = builder.build();
                            android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
                            java.lang.String selection = getSelectionForCalendarInstances(buildForSuggestions);
                            logger.i("buildForSuggestions: " + buildForSuggestions + ", selection: " + selection);
                            cursor = appContext.getContentResolver().query(contentUri, new java.lang.String[]{"title", "begin", "end", "allDay", "calendar_color", "eventLocation", "visible", "calendar_id"}, selection, null, "begin ASC");
                            if (cursor == null || !cursor.moveToFirst()) {
                                callback.onCalendarEventsRead(events);
                                setCachedEvents(events);
                            } else {
                                logger.d("Found " + cursor.getCount() + " Calendar Event(s) in the DB that matches the timebox criteria");
                                do {
                                    int displayNameColumnIndex = cursor.getColumnIndex("title");
                                    int startTimeColumnIndex = cursor.getColumnIndex("begin");
                                    int endTimeColumnIndex = cursor.getColumnIndex("end");
                                    int allDayColumnIndex = cursor.getColumnIndex("allDay");
                                    int colorColumnIndex = cursor.getColumnIndex("calendar_color");
                                    int locationColumnIndex = cursor.getColumnIndex("eventLocation");
                                    int visibleColumnIndex = cursor.getColumnIndex("visible");
                                    int calendarIdColumnIndex = cursor.getColumnIndex("calendar_id");
                                    java.lang.String displayNameString = cursor.getString(displayNameColumnIndex);
                                    java.lang.Long startDateAndTime = parseLongSafely(cursor.getString(startTimeColumnIndex));
                                    java.lang.Long endDateAndTime = parseLongSafely(cursor.getString(endTimeColumnIndex));
                                    boolean allDay = cursor.getInt(allDayColumnIndex) > 0;
                                    int colorARGB = cursor.getInt(colorColumnIndex);
                                    java.lang.String locationString = cursor.getString(locationColumnIndex);
                                    boolean visible = cursor.getInt(visibleColumnIndex) > 0;
                                    long calendarId = cursor.getLong(calendarIdColumnIndex);
                                    com.navdy.client.app.framework.models.CalendarEvent calendarEvent = new com.navdy.client.app.framework.models.CalendarEvent(displayNameString, calendarId, startDateAndTime.longValue(), endDateAndTime.longValue(), locationString, (long) colorARGB, allDay);
                                    if (!sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDAR_PREFIX + calendarId, visible)) {
                                        logger.d("Ignoring calendar event: " + calendarEvent.toString());
                                    } else {
                                        logger.d("Adding calendar event: " + calendarEvent.toString());
                                        events.add(calendarEvent);
                                    }
                                } while (cursor.moveToNext());
                                if (buildForSuggestions) {
                                    new com.navdy.client.app.framework.map.HereNavigableCoordinateWorker(events, new com.navdy.client.app.ui.homescreen.CalendarUtils.Anon2(callback));
                                } else {
                                    callback.onCalendarEventsRead(events);
                                }
                            }
                        } finally {
                            com.navdy.service.library.util.IOUtils.closeStream(cursor);
                        }
                    }
                }
            }
        }
    }

    public static java.util.ArrayList<com.navdy.client.app.framework.models.Calendar> listCalendars(android.content.Context context) {
        boolean visible;
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.READ_CALENDAR") != 0) {
            return null;
        }
        android.database.Cursor cur = null;
        try {
            cur = context.getContentResolver().query(android.provider.CalendarContract.Calendars.CONTENT_URI, new java.lang.String[]{"_id", "calendar_displayName", "calendar_color", "account_name", "account_type", "ownerAccount", "visible"}, null, null, "account_type ASC, visible DESC");
            if (cur == null) {
                return null;
            }
            java.util.ArrayList<com.navdy.client.app.framework.models.Calendar> list = new java.util.ArrayList<>();
            list.add(new com.navdy.client.app.framework.models.Calendar(com.navdy.client.app.framework.models.Calendar.CalendarListItemType.GLOBAL_SWITCH));
            java.lang.String lastAccountType = null;
            while (cur.moveToNext()) {
                long id = cur.getLong(cur.getColumnIndex("_id"));
                java.lang.String calendarDisplayName = cur.getString(cur.getColumnIndex("calendar_displayName"));
                int calendarColor = cur.getInt(cur.getColumnIndex("calendar_color"));
                java.lang.String accountName = cur.getString(cur.getColumnIndex("account_name"));
                java.lang.String accountType = cur.getString(cur.getColumnIndex("account_type"));
                java.lang.String ownerAccount = cur.getString(cur.getColumnIndex("ownerAccount"));
                if (cur.getInt(cur.getColumnIndex("visible")) == 1) {
                    visible = true;
                } else {
                    visible = false;
                }
                if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(accountType, lastAccountType)) {
                    if ("com.google".equals(accountType)) {
                        list.add(new com.navdy.client.app.framework.models.Calendar(com.navdy.client.app.framework.models.Calendar.CalendarListItemType.TITLE, context.getString(com.navdy.client.R.string.google_calendar)));
                    } else {
                        list.add(new com.navdy.client.app.framework.models.Calendar(com.navdy.client.app.framework.models.Calendar.CalendarListItemType.TITLE, accountType));
                    }
                    lastAccountType = accountType;
                }
                list.add(new com.navdy.client.app.framework.models.Calendar(com.navdy.client.app.framework.models.Calendar.CalendarListItemType.CALENDAR, id, calendarDisplayName, calendarColor, accountName, accountType, ownerAccount, visible));
            }
            com.navdy.service.library.util.IOUtils.closeStream(cur);
            return list;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cur);
        }
    }

    private static java.lang.Long parseLongSafely(java.lang.String n) {
        java.lang.Long ret = java.lang.Long.valueOf(0);
        if (n == null) {
            return ret;
        }
        try {
            return java.lang.Long.valueOf(java.lang.Long.parseLong(n));
        } catch (java.lang.Exception e) {
            logger.e("Can't parse a long out of this string: " + n);
            return ret;
        }
    }

    public static java.lang.String getDateAndTime(long milliSeconds) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (milliSeconds == 0) {
            return context.getString(com.navdy.client.R.string.unknown);
        }
        return java.text.SimpleDateFormat.getDateTimeInstance().format(new java.util.Date(milliSeconds));
    }

    public static java.lang.String getTime(long milliSeconds) {
        return android.text.format.DateFormat.getTimeFormat(com.navdy.client.app.NavdyApplication.getAppContext()).format(new java.util.Date(milliSeconds));
    }

    /* JADX INFO: finally extract failed */
    public static void convertAllClendarNameSharedPrefsToCalendarIds() {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.READ_CALENDAR") != 0) {
            removeAllSelectedCalendarsFromSharedPrefs();
            return;
        }
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        java.util.Set<java.lang.String> keySet = sharedPreferences.getAll().keySet();
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        for (java.lang.String key : keySet) {
            if (key.startsWith(com.navdy.client.app.ui.settings.SettingsConstants.CALENDAR_PREFIX)) {
                boolean value = sharedPreferences.getBoolean(key, true);
                java.lang.String calendarName = key.substring(com.navdy.client.app.ui.settings.SettingsConstants.CALENDAR_PREFIX.length());
                try {
                    android.database.Cursor cur = context.getContentResolver().query(android.provider.CalendarContract.Calendars.CONTENT_URI, new java.lang.String[]{"_id"}, "name=?", new java.lang.String[]{calendarName}, null);
                    while (cur != null && cur.moveToNext()) {
                        long calendarId = cur.getLong(cur.getColumnIndex("_id"));
                        if (calendarId > 0) {
                            editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDAR_PREFIX + calendarId, value);
                        }
                    }
                    com.navdy.service.library.util.IOUtils.closeStream(cur);
                    editor.remove(key);
                } catch (Throwable th) {
                    com.navdy.service.library.util.IOUtils.closeStream(null);
                    throw th;
                }
            }
        }
        editor.apply();
    }

    private static void removeAllSelectedCalendarsFromSharedPrefs() {
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        java.util.Set<java.lang.String> keySet = sharedPreferences.getAll().keySet();
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        for (java.lang.String key : keySet) {
            if (key.startsWith(com.navdy.client.app.ui.settings.SettingsConstants.CALENDAR_PREFIX)) {
                editor.remove(key);
            }
        }
        editor.apply();
    }
}
