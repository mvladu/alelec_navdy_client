package com.navdy.client.app.ui.glances;

public class GlancesFragment extends com.navdy.client.app.ui.base.BaseSupportFragment {
    private static final float DISABLED_ALPHA_LEVEL = 0.5f;
    public static final boolean VERBOSE = false;
    private android.widget.Switch allowGlances;
    private android.view.View.OnClickListener enableGlancesLearnMoreOnClickListener = new com.navdy.client.app.ui.glances.GlancesFragment.Anon5();
    private boolean glancesAreEnabled = false;
    private boolean glancesAreReadAloud = true;
    private android.widget.CompoundButton.OnCheckedChangeListener glancesOnCheckedChangeListener = new com.navdy.client.app.ui.glances.GlancesFragment.Anon3();
    private boolean glancesShowContent = false;
    private android.widget.RadioButton readAloud;
    private android.widget.RadioButton readAloudAndShowContent;
    private android.widget.LinearLayout readAloudAndShowContentLayout;
    private android.view.View.OnClickListener readAloudAndShowContentOnClickListener = new com.navdy.client.app.ui.glances.GlancesFragment.Anon4();
    private android.widget.LinearLayout readAloudLayout;
    private android.view.View rootView;
    private android.widget.RadioButton showContent;
    private android.widget.LinearLayout showContentLayout;
    private android.view.View.OnClickListener testGlanceButtonOnClickListener = new com.navdy.client.app.ui.glances.GlancesFragment.Anon6();

    class Anon1 implements android.view.View.OnClickListener {
        Anon1() {
        }

        public void onClick(android.view.View v) {
            if (com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreEnabled) {
                com.navdy.client.app.ui.glances.GlancesFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.glances.GlancesFragment.this.getContext(), com.navdy.client.app.ui.glances.AppNotificationsActivity.class));
            }
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        final /* synthetic */ java.lang.String val$pkg;

        Anon2(java.lang.String str) {
            this.val$pkg = str;
        }

        public void onClick(android.view.View v) {
            if (v != null && (v instanceof android.widget.Switch)) {
                com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(this.val$pkg, ((android.widget.Switch) v).isChecked());
                if (com.navdy.client.app.ui.glances.GlanceUtils.isDrivingGlance(this.val$pkg)) {
                    com.navdy.client.app.ui.glances.GlancesFragment.this.increaseSerialAndSendAllGlancesPreferencesToHud();
                }
            }
        }
    }

    class Anon3 implements android.widget.CompoundButton.OnCheckedChangeListener {
        Anon3() {
        }

        public void onCheckedChanged(android.widget.CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                com.navdy.client.app.ui.glances.GlancesFragment.this.showEnableGlancesDialog();
            } else {
                com.navdy.client.app.ui.glances.GlancesFragment.this.setGlances(java.lang.Boolean.valueOf(false));
            }
        }
    }

    class Anon4 implements android.view.View.OnClickListener {

        class Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
            Anon1() {
            }

            /* access modifiers changed from: protected */
            public java.lang.Void doInBackground(java.lang.Void... params) {
                com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_READ_ALOUD, com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreReadAloud);
                com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_SHOW_CONTENT, com.navdy.client.app.ui.glances.GlancesFragment.this.glancesShowContent);
                com.navdy.client.app.ui.glances.GlancesFragment.this.increaseSerialAndSendAllGlancesPreferencesToHud();
                return null;
            }
        }

        Anon4() {
        }

        public void onClick(android.view.View v) {
            boolean z;
            boolean z2;
            boolean z3 = true;
            switch (v.getId()) {
                case com.navdy.client.R.id.show_content_layout /*2131755718*/:
                case com.navdy.client.R.id.show_content /*2131755720*/:
                    com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreReadAloud = false;
                    com.navdy.client.app.ui.glances.GlancesFragment.this.glancesShowContent = true;
                    break;
                case com.navdy.client.R.id.read_aloud_and_show_content_layout /*2131755721*/:
                case com.navdy.client.R.id.read_aloud_and_show_content /*2131755723*/:
                    com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreReadAloud = true;
                    com.navdy.client.app.ui.glances.GlancesFragment.this.glancesShowContent = true;
                    break;
                default:
                    com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreReadAloud = true;
                    com.navdy.client.app.ui.glances.GlancesFragment.this.glancesShowContent = false;
                    break;
            }
            android.widget.RadioButton access$Anon500 = com.navdy.client.app.ui.glances.GlancesFragment.this.readAloud;
            if (!com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreReadAloud || com.navdy.client.app.ui.glances.GlancesFragment.this.glancesShowContent) {
                z = false;
            } else {
                z = true;
            }
            access$Anon500.setChecked(z);
            android.widget.RadioButton access$Anon600 = com.navdy.client.app.ui.glances.GlancesFragment.this.showContent;
            if (com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreReadAloud || !com.navdy.client.app.ui.glances.GlancesFragment.this.glancesShowContent) {
                z2 = false;
            } else {
                z2 = true;
            }
            access$Anon600.setChecked(z2);
            android.widget.RadioButton access$Anon700 = com.navdy.client.app.ui.glances.GlancesFragment.this.readAloudAndShowContent;
            if (!com.navdy.client.app.ui.glances.GlancesFragment.this.glancesAreReadAloud || !com.navdy.client.app.ui.glances.GlancesFragment.this.glancesShowContent) {
                z3 = false;
            }
            access$Anon700.setChecked(z3);
            new com.navdy.client.app.ui.glances.GlancesFragment.Anon4.Anon1().execute(new java.lang.Void[0]);
        }
    }

    class Anon5 implements android.view.View.OnClickListener {
        Anon5() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.glances.GlancesFragment.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.glances.GlancesFragment.this.getContext(), com.navdy.client.app.ui.homescreen.GlanceDialogActivity.class));
        }
    }

    class Anon6 implements android.view.View.OnClickListener {
        Anon6() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.glances.GlanceUtils.sendTestGlance((com.navdy.client.app.ui.base.BaseActivity) com.navdy.client.app.ui.glances.GlancesFragment.this.getActivity(), com.navdy.client.app.ui.glances.GlancesFragment.this.logger);
        }
    }

    class Anon7 implements android.content.DialogInterface.OnClickListener {
        Anon7() {
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            if (i == -1) {
                com.navdy.client.app.ui.glances.GlancesFragment.this.setGlances(java.lang.Boolean.valueOf(true));
            } else if (com.navdy.client.app.ui.glances.GlancesFragment.this.allowGlances != null) {
                com.navdy.client.app.ui.glances.GlancesFragment.this.setGlances(java.lang.Boolean.valueOf(false));
                com.navdy.client.app.ui.glances.GlancesFragment.this.allowGlances.setChecked(false);
            }
        }
    }

    class Anon8 implements android.content.DialogInterface.OnCancelListener {
        Anon8() {
        }

        public void onCancel(android.content.DialogInterface dialogInterface) {
            com.navdy.client.app.ui.glances.GlancesFragment.this.logger.v("onCancel");
            com.navdy.client.app.ui.glances.GlancesFragment.this.setGlances(java.lang.Boolean.valueOf(false));
            com.navdy.client.app.ui.glances.GlancesFragment.this.allowGlances.setChecked(false);
        }
    }

    class Anon9 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ java.lang.Boolean val$glancesAreEnabled;

        Anon9(java.lang.Boolean bool) {
            this.val$glancesAreEnabled = bool;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, this.val$glancesAreEnabled.booleanValue());
            com.navdy.client.app.ui.glances.GlancesFragment.this.increaseSerialAndSendAllGlancesPreferencesToHud();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            com.navdy.client.app.ui.glances.GlancesFragment.this.initAllSwitches();
        }
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        this.rootView = inflater.inflate(com.navdy.client.R.layout.hs_fragment_glances, container, false);
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (sharedPrefs != null) {
            this.glancesAreEnabled = sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false);
            this.glancesAreReadAloud = sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_READ_ALOUD, true);
            this.glancesShowContent = sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_SHOW_CONTENT, false);
        }
        this.allowGlances = (android.widget.Switch) this.rootView.findViewById(com.navdy.client.R.id.allow_glances_switch);
        this.allowGlances.setChecked(this.glancesAreEnabled);
        this.allowGlances.setOnCheckedChangeListener(this.glancesOnCheckedChangeListener);
        this.readAloudLayout = (android.widget.LinearLayout) this.rootView.findViewById(com.navdy.client.R.id.read_aloud_layout);
        this.readAloud = (android.widget.RadioButton) this.rootView.findViewById(com.navdy.client.R.id.read_aloud);
        this.readAloudLayout.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.readAloud.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.showContentLayout = (android.widget.LinearLayout) this.rootView.findViewById(com.navdy.client.R.id.show_content_layout);
        this.showContent = (android.widget.RadioButton) this.rootView.findViewById(com.navdy.client.R.id.show_content);
        this.showContentLayout.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.showContent.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.readAloudAndShowContentLayout = (android.widget.LinearLayout) this.rootView.findViewById(com.navdy.client.R.id.read_aloud_and_show_content_layout);
        this.readAloudAndShowContent = (android.widget.RadioButton) this.rootView.findViewById(com.navdy.client.R.id.read_aloud_and_show_content);
        this.readAloudAndShowContentLayout.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.readAloudAndShowContent.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        android.widget.TextView glancesSwitchDescription = (android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.glances_switch_desc);
        glancesSwitchDescription.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.allow_glances_description));
        glancesSwitchDescription.setOnClickListener(this.enableGlancesLearnMoreOnClickListener);
        ((android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.test_glance)).setOnClickListener(this.testGlanceButtonOnClickListener);
        initAllSwitches();
        android.widget.RelativeLayout notificationGlances = (android.widget.RelativeLayout) this.rootView.findViewById(com.navdy.client.R.id.notification_glances);
        if (notificationGlances != null) {
            notificationGlances.setOnClickListener(new com.navdy.client.app.ui.glances.GlancesFragment.Anon1());
        }
        return this.rootView;
    }

    public void highlightGlanceSwitch() {
        if (this.rootView != null && this.baseActivity != null) {
            this.rootView.scrollTo(0, 0);
            startHighlightAnimation(this.rootView.findViewById(com.navdy.client.R.id.allow_glances_switch));
        }
    }

    private void startHighlightAnimation(android.view.View glances) {
        if (glances != null) {
            android.view.animation.AlphaAnimation alphaAnimation = new android.view.animation.AlphaAnimation(0.1f, 1.0f);
            alphaAnimation.setDuration(250);
            alphaAnimation.setRepeatCount(6);
            alphaAnimation.setRepeatMode(2);
            glances.startAnimation(alphaAnimation);
        }
    }

    private void initAllSwitches() {
        boolean z;
        float f;
        boolean z2;
        float f2;
        float f3;
        float f4;
        boolean z3 = true;
        float f5 = 1.0f;
        android.widget.RadioButton radioButton = this.readAloud;
        if (!this.glancesAreReadAloud || this.glancesShowContent) {
            z = false;
        } else {
            z = true;
        }
        radioButton.setChecked(z);
        this.readAloud.setEnabled(this.glancesAreEnabled);
        android.widget.LinearLayout linearLayout = this.readAloudLayout;
        if (this.glancesAreEnabled) {
            f = 1.0f;
        } else {
            f = 0.5f;
        }
        linearLayout.setAlpha(f);
        android.widget.RadioButton radioButton2 = this.showContent;
        if (this.glancesAreReadAloud || !this.glancesShowContent) {
            z2 = false;
        } else {
            z2 = true;
        }
        radioButton2.setChecked(z2);
        this.showContent.setEnabled(this.glancesAreEnabled);
        android.widget.LinearLayout linearLayout2 = this.showContentLayout;
        if (this.glancesAreEnabled) {
            f2 = 1.0f;
        } else {
            f2 = 0.5f;
        }
        linearLayout2.setAlpha(f2);
        android.widget.RadioButton radioButton3 = this.readAloudAndShowContent;
        if (!this.glancesAreReadAloud || !this.glancesShowContent) {
            z3 = false;
        }
        radioButton3.setChecked(z3);
        this.readAloudAndShowContent.setEnabled(this.glancesAreEnabled);
        android.widget.LinearLayout linearLayout3 = this.readAloudAndShowContentLayout;
        if (this.glancesAreEnabled) {
            f3 = 1.0f;
        } else {
            f3 = 0.5f;
        }
        linearLayout3.setAlpha(f3);
        initSwitch(com.navdy.client.R.id.allow_calls_switch, com.navdy.client.app.framework.glances.GlanceConstants.PHONE_PACKAGE);
        initSwitch(com.navdy.client.R.id.allow_sms_switch, com.navdy.client.app.framework.glances.GlanceConstants.SMS_PACKAGE);
        initSwitch(com.navdy.client.R.id.allow_fuel_switch, com.navdy.client.app.framework.glances.GlanceConstants.FUEL_PACKAGE);
        initSwitch(com.navdy.client.R.id.allow_music_switch, com.navdy.client.app.framework.glances.GlanceConstants.MUSIC_PACKAGE);
        initSwitch(com.navdy.client.R.id.allow_traffic_switch, com.navdy.client.app.framework.glances.GlanceConstants.TRAFFIC_PACKAGE);
        initSwitch(com.navdy.client.R.id.allow_gmail_switch, com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_MAIL);
        initSwitch(com.navdy.client.R.id.allow_google_calendar_switch, com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_CALENDAR);
        initSwitch(com.navdy.client.R.id.allow_hangouts_switch, com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_HANGOUTS);
        initSwitch(com.navdy.client.R.id.allow_slack_switch, com.navdy.client.app.framework.glances.GlanceConstants.SLACK);
        initSwitch(com.navdy.client.R.id.allow_whatsapp_switch, com.navdy.client.app.framework.glances.GlanceConstants.WHATS_APP);
        initSwitch(com.navdy.client.R.id.allow_facebook_messenger_switch, com.navdy.client.app.framework.glances.GlanceConstants.FACEBOOK_MESSENGER);
        initSwitch(com.navdy.client.R.id.allow_facebook_switch, com.navdy.client.app.framework.glances.GlanceConstants.FACEBOOK);
        initSwitch(com.navdy.client.R.id.allow_twitter_switch, com.navdy.client.app.framework.glances.GlanceConstants.TWITTER);
        android.widget.RelativeLayout notificationGlances = (android.widget.RelativeLayout) this.rootView.findViewById(com.navdy.client.R.id.notification_glances);
        if (notificationGlances != null) {
            if (this.glancesAreEnabled) {
                f4 = 1.0f;
            } else {
                f4 = 0.5f;
            }
            notificationGlances.setAlpha(f4);
        }
        android.widget.TextView otherGlances = (android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.other_glances);
        if (otherGlances != null) {
            if (!this.glancesAreEnabled) {
                f5 = 0.5f;
            }
            otherGlances.setAlpha(f5);
        }
    }

    private void initSwitch(int switchId, java.lang.String appPackage) {
        android.widget.Switch aSwitch = (android.widget.Switch) this.rootView.findViewById(switchId);
        aSwitch.setChecked(com.navdy.client.app.ui.glances.GlanceUtils.isThisGlanceEnabledInItself(appPackage));
        aSwitch.setEnabled(this.glancesAreEnabled);
        android.view.ViewParent parent = aSwitch.getParent();
        if (parent != null && (parent instanceof android.view.View)) {
            ((android.view.View) parent).setAlpha(this.glancesAreEnabled ? 1.0f : DISABLED_ALPHA_LEVEL);
        }
        aSwitch.setOnClickListener(getGlanceSwitchClickListener(appPackage));
    }

    private android.view.View.OnClickListener getGlanceSwitchClickListener(java.lang.String pkg) {
        return new com.navdy.client.app.ui.glances.GlancesFragment.Anon2(pkg);
    }

    private void increaseSerialAndSendAllGlancesPreferencesToHud() {
        com.navdy.client.app.ui.settings.SettingsUtils.sendGlancesSettingsToTheHud(com.navdy.client.app.ui.glances.GlanceUtils.buildGlancesPreferences(com.navdy.client.app.ui.settings.SettingsUtils.incrementSerialNumber(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_SERIAL_NUM), this.glancesAreEnabled, this.glancesAreReadAloud, this.glancesShowContent));
    }

    private void showEnableGlancesDialog() {
        com.navdy.client.app.ui.homescreen.HomescreenActivity homescreenActivity = (com.navdy.client.app.ui.homescreen.HomescreenActivity) getActivity();
        if (homescreenActivity != null) {
            homescreenActivity.showQuestionDialog(com.navdy.client.R.string.warning, com.navdy.client.R.string.glances_warning, com.navdy.client.R.string.enable_glances, com.navdy.client.R.string.cancel, new com.navdy.client.app.ui.glances.GlancesFragment.Anon7(), new com.navdy.client.app.ui.glances.GlancesFragment.Anon8());
        }
    }

    public void setGlances(java.lang.Boolean glancesAreEnabled2) {
        this.glancesAreEnabled = glancesAreEnabled2.booleanValue();
        if (glancesAreEnabled2.booleanValue()) {
            android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (!sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, false)) {
                sharedPreferences.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, true).apply();
                android.support.v4.app.FragmentActivity activity = getActivity();
                if (activity instanceof com.navdy.client.app.ui.homescreen.HomescreenActivity) {
                    ((com.navdy.client.app.ui.homescreen.HomescreenActivity) activity).rebuildSuggestions();
                }
            }
        }
        new com.navdy.client.app.ui.glances.GlancesFragment.Anon9(glancesAreEnabled2).execute(new java.lang.Void[0]);
    }
}
