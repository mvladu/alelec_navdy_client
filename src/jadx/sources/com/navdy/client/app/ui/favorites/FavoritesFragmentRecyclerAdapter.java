package com.navdy.client.app.ui.favorites;

public class FavoritesFragmentRecyclerAdapter extends com.navdy.client.app.ui.RecyclerViewCursorAdapter<android.support.v7.widget.RecyclerView.ViewHolder> implements com.navdy.client.app.ui.favorites.ItemTouchHelperAdapter {
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.class);
    private static int maxShortcutCountPerActivity = ((android.content.pm.ShortcutManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService(android.content.pm.ShortcutManager.class)).getMaxShortcutCountPerActivity();
    private android.view.View.OnClickListener listener;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            if (android.os.Build.VERSION.SDK_INT >= 25) {
                android.content.pm.ShortcutManager shortcutManager = (android.content.pm.ShortcutManager) com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.context.getSystemService(android.content.pm.ShortcutManager.class);
                com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.logger.d("favoritesAdapter.getItemCount() = " + com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.getItemCount());
                int itemCount = com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.getItemCount();
                java.util.ArrayList<android.content.pm.ShortcutInfo> shortcuts = new java.util.ArrayList<>(itemCount);
                com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.logger.d("max shortcut count is " + com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.maxShortcutCountPerActivity);
                int i = 0;
                while (i < itemCount && i < com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.maxShortcutCountPerActivity) {
                    com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.getItem(i);
                    if (destination != null) {
                        android.content.Intent intent = new android.content.Intent(com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.context, com.navdy.client.app.ui.homescreen.HomescreenActivity.class);
                        intent.putExtra(com.navdy.client.app.ui.homescreen.HomescreenActivity.EXTRA_DESTINATION, destination.id);
                        intent.setAction("android.intent.action.VIEW");
                        java.lang.String favoriteLabel = destination.getFavoriteLabel().trim();
                        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(favoriteLabel)) {
                            favoriteLabel = (java.lang.String) destination.getTitleAndSubtitle().first;
                        }
                        shortcuts.add(new android.content.pm.ShortcutInfo.Builder(com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.context, java.lang.String.valueOf(destination.id)).setShortLabel(favoriteLabel).setLongLabel(com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.context.getString(com.navdy.client.R.string.navigate_to_x, new java.lang.Object[]{favoriteLabel})).setIcon(android.graphics.drawable.Icon.createWithResource(com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.this.context, destination.getBadgeAsset())).setIntent(intent).build());
                    }
                    i++;
                }
                com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.logger.d("shortcuts.size() = " + shortcuts.size());
                shortcutManager.setDynamicShortcuts(shortcuts);
            }
        }
    }

    static class ItemTouchHelperCallback extends android.support.v7.widget.helper.ItemTouchHelper.Callback {
        private final com.navdy.client.app.ui.favorites.ItemTouchHelperAdapter mAdapter;

        ItemTouchHelperCallback(com.navdy.client.app.ui.favorites.ItemTouchHelperAdapter adapter) {
            this.mAdapter = adapter;
        }

        public int getMovementFlags(android.support.v7.widget.RecyclerView recyclerView, android.support.v7.widget.RecyclerView.ViewHolder viewHolder) {
            return makeMovementFlags(3, 48);
        }

        public boolean onMove(android.support.v7.widget.RecyclerView recyclerView, android.support.v7.widget.RecyclerView.ViewHolder viewHolder, android.support.v7.widget.RecyclerView.ViewHolder target) {
            this.mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }

        public void onMoved(android.support.v7.widget.RecyclerView recyclerView, android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int fromPos, android.support.v7.widget.RecyclerView.ViewHolder target, int toPos, int x, int y) {
            super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
            this.mAdapter.setupDynamicShortcuts();
        }

        public void onSwiped(android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int direction) {
        }

        public boolean isItemViewSwipeEnabled() {
            return false;
        }

        public boolean isLongPressDragEnabled() {
            return true;
        }
    }

    static {
        if (android.os.Build.VERSION.SDK_INT >= 25) {
        }
    }

    FavoritesFragmentRecyclerAdapter(android.content.Context context) {
        super(context, false, true);
    }

    public android.database.Cursor getNewCursor() {
        return com.navdy.client.app.providers.NavdyContentProvider.getFavoritesCursor();
    }

    public long getItemId(int position) {
        if (!this.dataIsValid || this.cursor == null || !this.cursor.moveToPosition(position)) {
            return 0;
        }
        return this.cursor.getLong(this.cursor.getColumnIndex("_id"));
    }

    public com.navdy.client.app.ui.favorites.FavoriteViewHolder onCreateNormalViewHolder(android.view.ViewGroup parent) {
        return new com.navdy.client.app.ui.favorites.FavoriteViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.favorites_item_layout, parent, false));
    }

    public android.support.v7.widget.RecyclerView.ViewHolder onCreateHeaderViewHolder(android.view.ViewGroup parent) {
        return null;
    }

    public com.navdy.client.app.ui.favorites.PaddingFooterViewHolder onCreateFooterViewHolder(android.view.ViewGroup parent) {
        return new com.navdy.client.app.ui.favorites.PaddingFooterViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.padding_footer_layout, parent, false));
    }

    @android.support.annotation.UiThread
    public void onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof com.navdy.client.app.ui.favorites.FavoriteViewHolder) {
            com.navdy.client.app.ui.favorites.FavoriteViewHolder favoriteViewHolder = (com.navdy.client.app.ui.favorites.FavoriteViewHolder) viewHolder;
            if (!this.dataIsValid) {
                throw new java.lang.IllegalStateException("this should only be called when the cursor is valid");
            } else if (this.cursor == null || !this.cursor.moveToPosition(position)) {
                throw new java.lang.IllegalStateException("couldn't move cursor to position " + position + " cursor = " + this.cursor);
            } else {
                try {
                    com.navdy.client.app.framework.models.Destination destination = getItem(position);
                    android.util.Pair<java.lang.String, java.lang.String> titleAndSubtitle = destination.getTitleAndSubtitle();
                    if (favoriteViewHolder.firstLine != null) {
                        favoriteViewHolder.firstLine.setText((java.lang.CharSequence) titleAndSubtitle.first);
                    }
                    if (favoriteViewHolder.secondLine != null) {
                        favoriteViewHolder.secondLine.setText((java.lang.CharSequence) titleAndSubtitle.second);
                    }
                    if (favoriteViewHolder.icon != null) {
                        favoriteViewHolder.icon.setImageResource(destination.getBadgeAsset());
                    }
                    if (favoriteViewHolder.edit != null) {
                        favoriteViewHolder.edit.setTag(java.lang.Integer.valueOf(position));
                        favoriteViewHolder.edit.setOnClickListener(this.listener);
                    }
                    if (favoriteViewHolder.row != null) {
                        favoriteViewHolder.row.setTag(java.lang.Integer.valueOf(position));
                        favoriteViewHolder.row.setOnClickListener(this.listener);
                    }
                } catch (android.database.CursorIndexOutOfBoundsException e) {
                    logger.e("Error happened while reading the destination cursor: ", e);
                }
            }
        }
    }

    void setClickListener(android.view.View.OnClickListener onClickListener) {
        this.listener = onClickListener;
    }

    public com.navdy.client.app.framework.models.Destination getItem(int position) {
        return com.navdy.client.app.providers.NavdyContentProvider.getDestinationItemAt(this.cursor, position);
    }

    public void onItemMove(int fromPosition, int toPosition) {
        com.navdy.client.app.framework.models.Destination fromDestination = getItem(fromPosition);
        com.navdy.client.app.framework.models.Destination toDestination = getItem(toPosition);
        if (fromDestination != null && toDestination != null) {
            fromDestination.setFavoriteOrder(toPosition);
            toDestination.setFavoriteOrder(fromPosition);
            fromDestination.updateOnlyFavoriteFieldsInDb(false);
            toDestination.updateOnlyFavoriteFieldsInDb();
            notifyDataSetChanged();
        }
    }

    public void setupDynamicShortcuts() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.Anon1(), 1);
    }

    public void onItemDismiss(int position) {
    }
}
