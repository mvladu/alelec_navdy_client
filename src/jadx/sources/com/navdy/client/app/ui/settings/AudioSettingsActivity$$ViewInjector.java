package com.navdy.client.app.ui.settings;

public class AudioSettingsActivity$$ViewInjector {

    /* compiled from: AudioSettingsActivity$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioSettingsActivity val$target;

        Anon1(com.navdy.client.app.ui.settings.AudioSettingsActivity audioSettingsActivity) {
            this.val$target = audioSettingsActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    /* compiled from: AudioSettingsActivity$$ViewInjector */
    static class Anon2 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioSettingsActivity val$target;

        Anon2(com.navdy.client.app.ui.settings.AudioSettingsActivity audioSettingsActivity) {
            this.val$target = audioSettingsActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    /* compiled from: AudioSettingsActivity$$ViewInjector */
    static class Anon3 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioSettingsActivity val$target;

        Anon3(com.navdy.client.app.ui.settings.AudioSettingsActivity audioSettingsActivity) {
            this.val$target = audioSettingsActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    /* compiled from: AudioSettingsActivity$$ViewInjector */
    static class Anon4 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioSettingsActivity val$target;

        Anon4(com.navdy.client.app.ui.settings.AudioSettingsActivity audioSettingsActivity) {
            this.val$target = audioSettingsActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    /* compiled from: AudioSettingsActivity$$ViewInjector */
    static class Anon5 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioSettingsActivity val$target;

        Anon5(com.navdy.client.app.ui.settings.AudioSettingsActivity audioSettingsActivity) {
            this.val$target = audioSettingsActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    /* compiled from: AudioSettingsActivity$$ViewInjector */
    static class Anon6 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioSettingsActivity val$target;

        Anon6(com.navdy.client.app.ui.settings.AudioSettingsActivity audioSettingsActivity) {
            this.val$target = audioSettingsActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.app.ui.settings.AudioSettingsActivity target, java.lang.Object source) {
        target.mWelcomeMessageSwitch = (android.widget.Switch) finder.findRequiredView(source, com.navdy.client.R.id.welcome_message, "field 'mWelcomeMessageSwitch'");
        target.mSppedLimitWarnings = (android.widget.Switch) finder.findRequiredView(source, com.navdy.client.R.id.speed_warnings, "field 'mSppedLimitWarnings'");
        target.mCameraWarnings = (android.widget.Switch) finder.findRequiredView(source, com.navdy.client.R.id.camera_warnings, "field 'mCameraWarnings'");
        target.mTurnByTurnNavigation = (android.widget.Switch) finder.findRequiredView(source, com.navdy.client.R.id.turn_by_turn_instructions, "field 'mTurnByTurnNavigation'");
        android.view.View view = finder.findRequiredView(source, com.navdy.client.R.id.speaker, "field 'mSpeaker' and method 'onClick'");
        target.mSpeaker = (android.widget.RadioButton) view;
        view.setOnClickListener(new com.navdy.client.app.ui.settings.AudioSettingsActivity$$ViewInjector.Anon1(target));
        android.view.View view2 = finder.findRequiredView(source, com.navdy.client.R.id.bluetooth, "field 'mBluetooth' and method 'onClick'");
        target.mBluetooth = (android.widget.RadioButton) view2;
        view2.setOnClickListener(new com.navdy.client.app.ui.settings.AudioSettingsActivity$$ViewInjector.Anon2(target));
        target.mSmartBluetooth = (android.widget.RadioButton) finder.findRequiredView(source, com.navdy.client.R.id.smart_bluetooth, "field 'mSmartBluetooth'");
        target.mTxtVoiceName = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.txt_voice_name, "field 'mTxtVoiceName'");
        android.view.View view3 = finder.findRequiredView(source, com.navdy.client.R.id.preference_voice, "field 'mVoicePreference' and method 'onClick'");
        target.mVoicePreference = view3;
        view3.setOnClickListener(new com.navdy.client.app.ui.settings.AudioSettingsActivity$$ViewInjector.Anon3(target));
        android.view.View view4 = finder.findRequiredView(source, com.navdy.client.R.id.preference_speech_delay, "field 'mSpeechDelayPreference' and method 'onClick'");
        target.mSpeechDelayPreference = view4;
        view4.setOnClickListener(new com.navdy.client.app.ui.settings.AudioSettingsActivity$$ViewInjector.Anon4(target));
        target.tvSpeechLevel = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.speech_level, "field 'tvSpeechLevel'");
        target.mMainDescription = (android.widget.TextView) finder.findRequiredView(source, com.navdy.client.R.id.audio_settings_description, "field 'mMainDescription'");
        target.speechLevelSettings = (com.navdy.client.app.ui.customviews.MultipleChoiceLayout) finder.findRequiredView(source, com.navdy.client.R.id.audio_settings_speech_level, "field 'speechLevelSettings'");
        target.mDriveScoreWarningsSwitch = (android.widget.Switch) finder.findRequiredView(source, com.navdy.client.R.id.drive_score_warnings, "field 'mDriveScoreWarningsSwitch'");
        finder.findRequiredView(source, com.navdy.client.R.id.smart_bluetooth_container, "method 'onClick'").setOnClickListener(new com.navdy.client.app.ui.settings.AudioSettingsActivity$$ViewInjector.Anon5(target));
        finder.findRequiredView(source, com.navdy.client.R.id.btn_test_audio, "method 'onClick'").setOnClickListener(new com.navdy.client.app.ui.settings.AudioSettingsActivity$$ViewInjector.Anon6(target));
    }

    public static void reset(com.navdy.client.app.ui.settings.AudioSettingsActivity target) {
        target.mWelcomeMessageSwitch = null;
        target.mSppedLimitWarnings = null;
        target.mCameraWarnings = null;
        target.mTurnByTurnNavigation = null;
        target.mSpeaker = null;
        target.mBluetooth = null;
        target.mSmartBluetooth = null;
        target.mTxtVoiceName = null;
        target.mVoicePreference = null;
        target.mSpeechDelayPreference = null;
        target.tvSpeechLevel = null;
        target.mMainDescription = null;
        target.speechLevelSettings = null;
        target.mDriveScoreWarningsSwitch = null;
    }
}
