package com.navdy.client.app.ui;

public class MainActivity extends com.navdy.client.app.ui.base.BaseActivity {
    private static final int SPLASH_TIMEOUT = 1000;
    private com.navdy.client.app.framework.AppInstance appInstance;
    private java.lang.Runnable launchRunnable = new com.navdy.client.app.ui.MainActivity.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            android.content.Intent i;
            if (!com.navdy.client.app.ui.MainActivity.this.isActivityDestroyed()) {
                if (com.navdy.client.app.ui.firstlaunch.AppSetupActivity.userHasFinishedAppSetup()) {
                    i = new android.content.Intent(com.navdy.client.app.ui.MainActivity.this, com.navdy.client.app.ui.homescreen.HomescreenActivity.class);
                    i.setFlags(67108864);
                } else {
                    i = new android.content.Intent(com.navdy.client.app.ui.MainActivity.this, com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.class);
                }
                com.navdy.client.app.ui.MainActivity.this.startActivity(i);
                com.navdy.client.app.ui.MainActivity.this.finish();
            }
        }
    }

    class Anon2 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ long val$timeBeforeGmsCheck;

        Anon2(long j) {
            this.val$timeBeforeGmsCheck = j;
        }

        public void onClick(android.content.DialogInterface dialogInterface, int whichButton) {
            if (whichButton == -1) {
                com.navdy.client.app.ui.MainActivity.this.openMarketAppFor("com.google.android.gms");
                com.navdy.client.app.ui.MainActivity.this.finish();
                return;
            }
            com.navdy.client.app.ui.MainActivity.this.initAndStartTheApp(this.val$timeBeforeGmsCheck);
        }
    }

    class Anon3 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ long val$timeBeforeGmsCheck;

        Anon3(long j) {
            this.val$timeBeforeGmsCheck = j;
        }

        public void onClick(android.content.DialogInterface dialogInterface, int whichButton) {
            if (whichButton == -1) {
                com.navdy.client.app.ui.MainActivity.this.openMarketAppFor("com.google.android.gms");
                com.navdy.client.app.ui.MainActivity.this.finish();
                return;
            }
            com.navdy.client.app.ui.MainActivity.this.initAndStartTheApp(this.val$timeBeforeGmsCheck);
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ int val$finalRemainingTime;

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                if (!com.navdy.client.app.ui.MainActivity.this.isActivityDestroyed()) {
                    android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "App Instance error", 0).show();
                    com.navdy.client.app.ui.MainActivity.this.finish();
                }
            }
        }

        Anon4(int i) {
            this.val$finalRemainingTime = i;
        }

        public void run() {
            try {
                com.navdy.client.app.ui.MainActivity.this.appInstance.initializeApp();
                com.navdy.client.app.ui.MainActivity.this.handler.postDelayed(com.navdy.client.app.ui.MainActivity.this.launchRunnable, (long) this.val$finalRemainingTime);
            } catch (Throwable t) {
                com.navdy.client.app.ui.MainActivity.this.logger.e(t);
                com.navdy.client.app.ui.MainActivity.this.handler.post(new com.navdy.client.app.ui.MainActivity.Anon4.Anon1());
            }
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.main_lyt);
        android.widget.ImageView imageView = (android.widget.ImageView) findViewById(com.navdy.client.R.id.navdy_logo);
        if (imageView != null) {
            com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, com.navdy.client.R.drawable.image_launch_background, null);
        }
        this.appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        long timeBeforeGmsCheck = new java.util.Date().getTime();
        switch (com.navdy.client.app.framework.util.GmsUtils.checkIfMissingOrOutOfDate()) {
            case MISSING:
                showQuestionDialog(com.navdy.client.R.string.no_google_play_services, com.navdy.client.R.string.dude_you_need_google_play_services, com.navdy.client.R.string.install_now, com.navdy.client.R.string.later, new com.navdy.client.app.ui.MainActivity.Anon2(timeBeforeGmsCheck), null);
                return;
            case OUT_OF_DATE:
                showQuestionDialog(com.navdy.client.R.string.outdated_gms_title, com.navdy.client.R.string.outdated_gms_message, com.navdy.client.R.string.update_now, com.navdy.client.R.string.later, new com.navdy.client.app.ui.MainActivity.Anon3(timeBeforeGmsCheck), null);
                return;
            case UP_TO_DATE:
                initAndStartTheApp(timeBeforeGmsCheck);
                return;
            default:
                return;
        }
    }

    private void initAndStartTheApp(long timeBeforeGmsCheck) {
        int remainingTime = 1000 - ((int) (new java.util.Date().getTime() - timeBeforeGmsCheck));
        if (remainingTime < 0) {
            remainingTime = 0;
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.MainActivity.Anon4(remainingTime), 1);
    }

    protected void onDestroy() {
        this.handler.removeCallbacks(this.launchRunnable);
        super.onDestroy();
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.SPLASH);
    }

    protected void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
        com.localytics.android.Localytics.onNewIntent(this, intent);
    }
}
