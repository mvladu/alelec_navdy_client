package com.navdy.client.app.ui;

public class PhotoUtils {
    public static final int COMPRESS_PHOTO_QUALITY = 85;
    public static final int PICK_PHOTO_FROM_GALLERY = 2;
    public static final int TAKE_PHOTO = 1;
    public static final java.lang.String TEMP_PHOTO_FILE = "temp_photo.jpg";

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.ui.base.BaseActivity val$activity;
        final /* synthetic */ java.lang.String val$finalName;
        final /* synthetic */ com.navdy.service.library.log.Logger val$logger;

        /* renamed from: com.navdy.client.app.ui.PhotoUtils$Anon1$Anon1 reason: collision with other inner class name */
        class C0052Anon1 implements java.lang.Runnable {
            C0052Anon1() {
            }

            public void run() {
                android.content.Intent intent = new android.content.Intent("android.media.action.IMAGE_CAPTURE");
                java.io.File file = com.navdy.service.library.util.IOUtils.getTempFile(com.navdy.client.app.ui.PhotoUtils.Anon1.this.val$logger, com.navdy.client.app.ui.PhotoUtils.Anon1.this.val$finalName);
                if (file != null) {
                    try {
                        intent.putExtra("output", getUri(file));
                        intent.putExtra("return-data", true);
                        com.navdy.client.app.ui.PhotoUtils.Anon1.this.val$logger.d("filename: " + com.navdy.client.app.ui.PhotoUtils.Anon1.this.val$finalName);
                        try {
                            com.navdy.client.app.ui.PhotoUtils.Anon1.this.val$activity.startActivityForResult(intent, 1);
                        } catch (android.content.ActivityNotFoundException e) {
                            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.no_camera_found, new java.lang.Object[0]);
                        }
                    } catch (Throwable t) {
                        com.navdy.client.app.ui.PhotoUtils.Anon1.this.val$logger.e("Unable to get a temp file", t);
                        com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.storage_permission_not_granted, new java.lang.Object[0]);
                    }
                } else {
                    com.navdy.client.app.ui.PhotoUtils.Anon1.this.val$logger.e("PhotoUtils:: temporary file is null");
                }
            }

            public android.net.Uri getUri(java.io.File file) {
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    return android.support.v4.content.FileProvider.getUriForFile(com.navdy.client.app.NavdyApplication.getAppContext(), "com.navdy.client.provider", file);
                }
                return android.net.Uri.fromFile(file);
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.storage_permission_not_granted, new java.lang.Object[0]);
            }
        }

        Anon1(com.navdy.client.app.ui.base.BaseActivity baseActivity, com.navdy.service.library.log.Logger logger, java.lang.String str) {
            this.val$activity = baseActivity;
            this.val$logger = logger;
            this.val$finalName = str;
        }

        public void run() {
            this.val$activity.requestStoragePermission(new com.navdy.client.app.ui.PhotoUtils.Anon1.C0052Anon1(), new com.navdy.client.app.ui.PhotoUtils.Anon1.Anon2());
        }
    }

    static class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.camera_permission_not_granted, new java.lang.Object[0]);
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.ui.base.BaseActivity val$activity;

        Anon3(com.navdy.client.app.ui.base.BaseActivity baseActivity) {
            this.val$activity = baseActivity;
        }

        public void run() {
            android.content.Intent getIntent = new android.content.Intent("android.intent.action.GET_CONTENT");
            getIntent.setType("image/*");
            this.val$activity.startActivityForResult(android.content.Intent.createChooser(getIntent, "Attach an image"), 2);
        }
    }

    static class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.storage_permission_not_granted, new java.lang.Object[0]);
        }
    }

    public static void takePhoto(com.navdy.client.app.ui.base.BaseActivity activity, com.navdy.service.library.log.Logger logger, java.lang.String filename) {
        if (!com.navdy.service.library.util.IOUtils.isExternalStorageReadable()) {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.external_storage_not_mounted, new java.lang.Object[0]);
            return;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(filename)) {
            filename = com.navdy.service.library.util.IOUtils.getTempFilename();
        }
        activity.requestCameraPermission(new com.navdy.client.app.ui.PhotoUtils.Anon1(activity, logger, filename), new com.navdy.client.app.ui.PhotoUtils.Anon2());
    }

    public static void getPhotoFromGallery(com.navdy.client.app.ui.base.BaseActivity activity) {
        if (activity != null) {
            activity.requestStoragePermission(new com.navdy.client.app.ui.PhotoUtils.Anon3(activity), new com.navdy.client.app.ui.PhotoUtils.Anon4());
        }
    }
}
