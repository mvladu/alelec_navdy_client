package com.navdy.client.app.ui.customviews;

public class ActiveTripCardView extends com.navdy.client.app.ui.customviews.TripCardView {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.customviews.ActiveTripCardView.class);
    private final android.content.Context context;

    class Anon1 implements android.view.View.OnClickListener {
        Anon1() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.customviews.ActiveTripCardView.logger.v("onClick, start active trip activity");
            com.navdy.client.app.ui.customviews.ActiveTripCardView.this.handleOnClick();
        }
    }

    public ActiveTripCardView(android.content.Context context2) {
        this(context2, null, 0);
    }

    public ActiveTripCardView(android.content.Context context2, android.util.AttributeSet attrs) {
        this(context2, attrs, 0);
    }

    public ActiveTripCardView(android.content.Context context2, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context2, attrs, defStyleAttr);
        this.context = context2;
        setOnClickListener(new com.navdy.client.app.ui.customviews.ActiveTripCardView.Anon1());
    }

    public void handleOnClick() {
        com.navdy.client.app.ui.routing.ActiveTripActivity.startActiveTripActivity(this.context);
    }

    protected java.lang.String appendPrefix(java.lang.String name) {
        return getResources().getString(com.navdy.client.R.string.active_trip_prefix, new java.lang.Object[]{name});
    }
}
