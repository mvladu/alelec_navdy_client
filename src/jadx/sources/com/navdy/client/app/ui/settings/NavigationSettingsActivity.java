package com.navdy.client.app.ui.settings;

public class NavigationSettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity {
    private android.widget.Switch autoRecalc;
    private android.widget.Switch autoTrains;
    private android.widget.Switch ferries;
    private android.widget.Switch highways;
    private android.widget.RadioButton routeCalculationFastest;
    private android.widget.RadioButton routeCalculationShortest;
    private android.content.SharedPreferences sharedPrefs;
    private android.widget.Switch tollRoads;
    private android.widget.Switch tunnels;
    private android.widget.Switch unpavedRoads;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_navigation);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_navigation).build();
        this.routeCalculationFastest = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.route_calculation_fastest);
        this.routeCalculationShortest = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.route_calculation_shortest);
        this.autoRecalc = (android.widget.Switch) findViewById(com.navdy.client.R.id.auto_recalc);
        this.highways = (android.widget.Switch) findViewById(com.navdy.client.R.id.highways);
        this.tollRoads = (android.widget.Switch) findViewById(com.navdy.client.R.id.toll_roads);
        this.ferries = (android.widget.Switch) findViewById(com.navdy.client.R.id.ferries);
        this.tunnels = (android.widget.Switch) findViewById(com.navdy.client.R.id.tunnels);
        this.unpavedRoads = (android.widget.Switch) findViewById(com.navdy.client.R.id.unpaved_roads);
        this.autoTrains = (android.widget.Switch) findViewById(com.navdy.client.R.id.auto_trains);
        this.sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        boolean calculateShortestRouteIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_ROUTE_CALCULATION, false);
        boolean autoRecalcIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_RECALC, false);
        boolean highwaysIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_HIGHWAYS, true);
        boolean tollRoadsIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TOLL_ROADS, true);
        boolean ferriesIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_FERRIES, true);
        boolean tunnelsIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TUNNELS, true);
        boolean unpavedRoadsIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_UNPAVED_ROADS, true);
        boolean autoTrainsIsOn = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_TRAINS, true);
        initCompoundButton(this.routeCalculationFastest, !calculateShortestRouteIsOn);
        initCompoundButton(this.routeCalculationShortest, calculateShortestRouteIsOn);
        initCompoundButton(this.autoRecalc, autoRecalcIsOn);
        initCompoundButton(this.highways, highwaysIsOn);
        initCompoundButton(this.tollRoads, tollRoadsIsOn);
        initCompoundButton(this.ferries, ferriesIsOn);
        initCompoundButton(this.tunnels, tunnelsIsOn);
        initCompoundButton(this.unpavedRoads, unpavedRoadsIsOn);
        initCompoundButton(this.autoTrains, autoTrainsIsOn);
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.NAVIGATION);
    }

    public void onAutoRecalcClick(android.view.View view) {
        if (this.autoRecalc != null && this.autoRecalc.isEnabled()) {
            this.autoRecalc.performClick();
        }
    }

    protected void saveChanges() {
        if (this.sharedPrefs != null) {
            long serialNumber = this.sharedPrefs.getLong("nav_serial_number", 0) + 1;
            this.sharedPrefs.edit().putLong("nav_serial_number", serialNumber).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_ROUTE_CALCULATION, this.routeCalculationShortest.isChecked()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_RECALC, this.autoRecalc.isChecked()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_HIGHWAYS, this.highways.isChecked()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TOLL_ROADS, this.tollRoads.isChecked()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_FERRIES, this.ferries.isChecked()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TUNNELS, this.tunnels.isChecked()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_UNPAVED_ROADS, this.unpavedRoads.isChecked()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_TRAINS, this.autoTrains.isChecked()).apply();
            if (com.navdy.client.app.ui.settings.SettingsUtils.sendNavSettingsToTheHud(com.navdy.client.app.ui.settings.SettingsUtils.buildNavigationPreferences(serialNumber, this.routeCalculationShortest.isChecked(), this.autoRecalc.isChecked(), this.highways.isChecked(), this.tollRoads.isChecked(), this.ferries.isChecked(), this.tunnels.isChecked(), this.unpavedRoads.isChecked(), this.autoTrains.isChecked(), this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SPEED_WARNINGS, false), this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_CAMERA_WARNINGS, true), this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, true)))) {
                showShortToast(com.navdy.client.R.string.settings_navigation_succeeded, new java.lang.Object[0]);
                com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Settings.NAVIGATION_SETTINGS_CHANGED);
                return;
            }
            showShortToast(com.navdy.client.R.string.settings_need_to_be_connected_to_hud, new java.lang.Object[0]);
            return;
        }
        showShortToast(com.navdy.client.R.string.settings_navigation_failed, new java.lang.Object[0]);
    }
}
