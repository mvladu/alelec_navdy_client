package com.navdy.client.app.ui.settings;

public class ProfileSettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity {
    public static final int PICK_AN_IMAGE = 0;
    public static final int REMOVE_PICTURE = 3;
    public static final int TAKE_A_PICTURE = 1;
    protected final android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
    /* access modifiers changed from: protected */
    public final android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
    /* access modifiers changed from: protected */
    public java.lang.String email = null;
    protected android.widget.EditText emailInput;
    protected android.support.design.widget.TextInputLayout emailLabel;
    protected final android.content.res.ColorStateList greyColorStateList = android.content.res.ColorStateList.valueOf(android.support.v4.content.ContextCompat.getColor(this.appContext, com.navdy.client.R.color.grey));
    /* access modifiers changed from: protected */
    public java.lang.String name = null;
    protected android.widget.EditText nameInput;
    protected android.support.design.widget.TextInputLayout nameLabel;
    protected com.navdy.client.app.ui.customviews.DestinationImageView photo;
    protected android.widget.LinearLayout photoHint;
    protected com.navdy.client.app.ui.firstlaunch.AppSetupProfileFragment profileFragment;
    protected final android.content.res.ColorStateList redColorStateList = android.content.res.ColorStateList.valueOf(android.support.v4.content.ContextCompat.getColor(this.appContext, com.navdy.client.R.color.red));
    /* access modifiers changed from: protected */
    public android.graphics.Bitmap userPhoto = null;

    class Anon1 implements android.text.TextWatcher {
        Anon1() {
        }

        public void beforeTextChanged(java.lang.CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(android.text.Editable text) {
            com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.updatePhoto(text.toString());
        }
    }

    class Anon2 implements android.view.View.OnFocusChangeListener {
        Anon2() {
        }

        public void onFocusChange(android.view.View v, boolean hasFocus) {
            if (!hasFocus) {
                com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.validateName();
            } else {
                com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.nameInput.setSelection(0, com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.nameInput.getText().length());
            }
        }
    }

    class Anon3 implements android.view.View.OnFocusChangeListener {
        Anon3() {
        }

        public void onFocusChange(android.view.View v, boolean hasFocus) {
            if (!hasFocus) {
                com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.validateEmail();
            } else {
                com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.emailInput.setSelection(0, com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.emailInput.getText().length());
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ android.net.Uri val$destination;
        final /* synthetic */ android.net.Uri val$source;

        Anon4(android.net.Uri uri, android.net.Uri uri2) {
            this.val$source = uri;
            this.val$destination = uri2;
        }

        public void run() {
            com.soundcloud.android.crop.Crop.of(this.val$source, this.val$destination).asSquare().start(com.navdy.client.app.ui.settings.ProfileSettingsActivity.this);
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.storage_permission_not_granted, new java.lang.Object[0]);
        }
    }

    class Anon6 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ android.content.Intent val$responseIntent;

        Anon6(android.content.Intent intent) {
            this.val$responseIntent = intent;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.showProgressDialog();
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void[] params) {
            android.graphics.Bitmap photoBitmap;
            android.net.Uri output = com.soundcloud.android.crop.Crop.getOutput(this.val$responseIntent);
            android.content.res.Resources resources = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
            int width = resources.getDimensionPixelSize(com.navdy.client.R.dimen.contact_image_width);
            int height = resources.getDimensionPixelSize(com.navdy.client.R.dimen.contact_image_height);
            if (output != null) {
                photoBitmap = com.navdy.client.app.framework.util.ImageUtils.getScaledBitmap(new java.io.File(output.getPath()), width, height);
            } else {
                photoBitmap = null;
            }
            if (photoBitmap != null) {
                com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.userPhoto = photoBitmap;
                com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.somethingChanged = true;
                com.navdy.client.app.tracking.Tracker.saveUserPhotoToInternalStorage(photoBitmap);
                com.navdy.client.app.ui.settings.SettingsUtils.incrementDriverProfileSerial();
                com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
                if (!new java.io.File(output.getPath()).delete()) {
                    com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.logger.w("Unable to delete temporary photo file.");
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            java.lang.String fullName;
            super.onPostExecute(aVoid);
            com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.hideProgressDialog();
            if (com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.nameInput != null) {
                fullName = com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.nameInput.getText().toString();
            } else {
                fullName = "";
            }
            com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.updatePhoto(com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.userPhoto, fullName);
        }
    }

    class Anon7 implements android.content.DialogInterface.OnClickListener {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.soundcloud.android.crop.Crop.pickImage(com.navdy.client.app.ui.settings.ProfileSettingsActivity.this);
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.storage_permission_not_granted, new java.lang.Object[0]);
            }
        }

        Anon7() {
        }

        public void onClick(android.content.DialogInterface dialog, int which) {
            switch (which) {
                case 0:
                    com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.requestStoragePermission(new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon7.Anon1(), new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon7.Anon2());
                    return;
                case 1:
                    com.navdy.client.app.ui.PhotoUtils.takePhoto(com.navdy.client.app.ui.settings.ProfileSettingsActivity.this, com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.logger, com.navdy.client.app.ui.PhotoUtils.TEMP_PHOTO_FILE);
                    return;
                default:
                    com.navdy.client.app.tracking.Tracker.eraseUserProfilePhoto();
                    if (com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.profileFragment != null) {
                        com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.photo.setImage(com.navdy.client.R.drawable.icon_user_bg_4, com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.profileFragment.getNameText());
                        com.navdy.client.app.ui.settings.ProfileSettingsActivity.this.somethingChanged = true;
                        return;
                    }
                    return;
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(this instanceof com.navdy.client.app.ui.firstlaunch.AppSetupActivity)) {
            setContentView((int) com.navdy.client.R.layout.settings_profile);
            new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_profile).build();
            doProfileOnCreate((com.navdy.client.app.ui.firstlaunch.AppSetupProfileFragment) getSupportFragmentManager().findFragmentById(com.navdy.client.R.id.profile_card));
        }
    }

    public void doProfileOnCreate(com.navdy.client.app.ui.firstlaunch.AppSetupProfileFragment profileFragment2) {
        this.profileFragment = profileFragment2;
        if (profileFragment2 != null) {
            android.view.View rootView = profileFragment2.getRootView();
            this.nameInput = (android.widget.EditText) rootView.findViewById(com.navdy.client.R.id.enter_name);
            this.emailInput = (android.widget.EditText) rootView.findViewById(com.navdy.client.R.id.enter_email);
            this.nameLabel = (android.support.design.widget.TextInputLayout) rootView.findViewById(com.navdy.client.R.id.enter_name_til);
            this.emailLabel = (android.support.design.widget.TextInputLayout) rootView.findViewById(com.navdy.client.R.id.enter_email_til);
            if (this.nameInput != null) {
                android.support.v4.view.ViewCompat.setBackgroundTintList(this.nameInput, this.greyColorStateList);
                this.nameInput.addTextChangedListener(new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon1());
                this.nameInput.setOnFocusChangeListener(new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon2());
            }
            if (this.emailInput != null) {
                android.support.v4.view.ViewCompat.setBackgroundTintList(this.emailInput, this.greyColorStateList);
                this.emailInput.setOnFocusChangeListener(new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon3());
            }
        }
    }

    protected void onPause() {
        validateAndSaveName(true);
        validateAndSaveEmail(true);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.photo = (com.navdy.client.app.ui.customviews.DestinationImageView) findViewById(com.navdy.client.R.id.photo);
        this.photoHint = (android.widget.LinearLayout) findViewById(com.navdy.client.R.id.photo_hint);
        if (this.userPhoto == null) {
            this.userPhoto = com.navdy.client.app.tracking.Tracker.getUserProfilePhoto();
        }
        this.name = this.customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, "");
        this.email = this.customerPrefs.getString("email", "");
        updatePhoto(this.userPhoto, this.name);
        if (!(this.nameInput == null || this.name == null)) {
            this.nameInput.setText(this.name);
            this.nameInput.setSelection(this.name.length());
            validateName();
        }
        if (!(this.emailInput == null || this.email == null)) {
            this.emailInput.setText(this.email);
            this.emailInput.setSelection(this.email.length());
            validateEmail();
        }
        this.somethingChanged = false;
        com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.PROFILE);
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, android.content.Intent responseIntent) {
        android.net.Uri source;
        if (resultCode == -1) {
            if (requestCode == 1 || requestCode == 9162) {
                android.net.Uri destination = android.net.Uri.fromFile(com.navdy.service.library.util.IOUtils.getTempFile(this.logger, com.navdy.client.app.ui.PhotoUtils.TEMP_PHOTO_FILE));
                if (requestCode == 9162) {
                    source = responseIntent.getData();
                } else {
                    source = destination;
                }
                requestStoragePermission(new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon4(source, destination), new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon5());
            } else if (requestCode == 6709) {
                new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon6(responseIntent).execute(new java.lang.Void[0]);
            }
        }
        super.onActivityResult(requestCode, resultCode, responseIntent);
    }

    public void onChangePhoto(android.view.View view) {
        java.lang.CharSequence[] options = getResources().getStringArray(com.navdy.client.R.array.settings_profile_edit_picture);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle((int) com.navdy.client.R.string.settings_profile_change_photo_btn);
        builder.setItems(options, (android.content.DialogInterface.OnClickListener) new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon7());
        builder.show();
    }

    public void onPrivacyPolicyClick(android.view.View view) {
        android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
        browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onNextProfileStepClick(android.view.View view) {
        if (saveProfileInfo()) {
            onProfileInfoSet();
        }
    }

    public void onProfileInfoSet() {
        finish();
    }

    private void updatePhoto(java.lang.String fullName) {
        if (this.photo != null && this.userPhoto == null) {
            this.photo.setImage(com.navdy.client.R.drawable.icon_user_bg_4, fullName);
        }
    }

    private void updatePhoto(android.graphics.Bitmap userPhoto2, java.lang.String fullName) {
        if (this.photo != null) {
            android.widget.TextView hintText = (android.widget.TextView) findViewById(com.navdy.client.R.id.photo_hint_text);
            if (userPhoto2 != null) {
                this.photo.setImage(userPhoto2);
                if (hintText != null) {
                    hintText.setText(com.navdy.client.R.string.edit_photo);
                    return;
                }
                return;
            }
            this.photo.setImage(com.navdy.client.R.drawable.icon_user_bg_4, fullName);
            if (hintText != null) {
                hintText.setText(com.navdy.client.R.string.add_photo);
            }
        }
    }

    protected void saveChanges() {
        saveProfileInfo();
    }

    private boolean saveProfileInfo() {
        boolean isOnMainThread = false;
        boolean success = validateAndSaveEmail(validateAndSaveName(true));
        if (success) {
            showShortToast(com.navdy.client.R.string.settings_profile_edit_succeeded, new java.lang.Object[0]);
            com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
            com.navdy.client.app.tracking.Tracker.registerUser(this.email, this.name);
            if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
                isOnMainThread = true;
            }
            if (isOnMainThread) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.ProfileSettingsActivity.Anon8(), 1);
            } else {
                com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }
        return success;
    }

    private boolean validateName() {
        if (com.navdy.client.app.framework.util.StringUtils.isValidName(this.nameInput.getText().toString())) {
            this.nameLabel.setHintTextAppearance(com.navdy.client.R.style.blue_text);
            android.support.v4.view.ViewCompat.setBackgroundTintList(this.nameInput, this.greyColorStateList);
            return true;
        }
        this.nameLabel.setHintTextAppearance(com.navdy.client.R.style.red_text_button);
        android.support.v4.view.ViewCompat.setBackgroundTintList(this.nameInput, this.redColorStateList);
        return false;
    }

    private boolean validateEmail() {
        java.lang.String newEmail = this.emailInput.getText().toString();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newEmail) && newEmail.matches(".*[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000].*")) {
            newEmail = newEmail.replaceAll(com.navdy.client.app.framework.util.StringUtils.WHITESPACE_REGEXP, "");
            this.emailInput.setText(newEmail);
        }
        if (com.navdy.client.app.framework.util.StringUtils.isValidEmail(newEmail)) {
            this.emailLabel.setHintTextAppearance(com.navdy.client.R.style.blue_text);
            android.support.v4.view.ViewCompat.setBackgroundTintList(this.emailInput, this.greyColorStateList);
            return true;
        }
        this.emailLabel.setHintTextAppearance(com.navdy.client.R.style.red_text_button);
        android.support.v4.view.ViewCompat.setBackgroundTintList(this.emailInput, this.redColorStateList);
        return false;
    }

    protected boolean validateAndSaveName(boolean success) {
        if (this.nameInput == null) {
            return success;
        }
        java.lang.String newName = this.nameInput.getText().toString();
        boolean differentFromBefore = isDifferentFromBefore(newName, this.name);
        if (differentFromBefore) {
            this.somethingChanged = true;
        }
        if (validateName()) {
            this.name = newName;
            if (this.customerPrefs == null) {
                return success;
            }
            this.customerPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.FULL_NAME, this.name).apply();
            return success;
        }
        if (differentFromBefore) {
            showLongToast(com.navdy.client.R.string.settings_profile_edit_invalid_name, new java.lang.Object[0]);
        }
        return false;
    }

    protected boolean validateAndSaveEmail(boolean success) {
        if (this.emailInput == null) {
            return success;
        }
        java.lang.String newEmail = this.emailInput.getText().toString();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newEmail) && newEmail.matches(".*[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000].*")) {
            newEmail = newEmail.replaceAll(com.navdy.client.app.framework.util.StringUtils.WHITESPACE_REGEXP, "");
            this.emailInput.setText(newEmail);
        }
        boolean differentFromBefore = isDifferentFromBefore(newEmail, this.email);
        if (differentFromBefore) {
            this.somethingChanged = true;
        }
        if (validateEmail()) {
            this.email = newEmail;
            if (this.customerPrefs == null) {
                return success;
            }
            this.customerPrefs.edit().putString("email", this.email).apply();
            return success;
        }
        if (differentFromBefore) {
            showLongToast(com.navdy.client.R.string.settings_profile_edit_invalid_email, new java.lang.Object[0]);
        }
        return false;
    }

    public boolean isDifferentFromBefore(java.lang.String newString, java.lang.String oldString) {
        return com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newString) != com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(oldString) || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(newString, oldString);
    }
}
