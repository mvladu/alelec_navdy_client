package com.navdy.client.app.ui.settings;

public class AudioSettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity implements android.widget.CompoundButton.OnCheckedChangeListener {
    public static final int DIALOG_VOICE_SELECTION = 1;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.settings.AudioSettingsActivity.class);
    com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput audioOutput = com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput.BEST_AVAILABLE;
    int audioTestIndex = 0;
    java.lang.String[] audioTests;
    @javax.inject.Inject
    com.navdy.client.app.framework.util.TTSAudioRouter mAudioRouter;
    @butterknife.InjectView(2131755896)
    android.widget.RadioButton mBluetooth;
    @butterknife.InjectView(2131755910)
    android.widget.Switch mCameraWarnings;
    boolean mCameraWarningsIsOn;
    boolean mDriveScoreWarningsIsOn;
    @butterknife.InjectView(2131755911)
    android.widget.Switch mDriveScoreWarningsSwitch;
    android.speech.tts.Voice mInitialVoice;
    @butterknife.InjectView(2131755892)
    android.widget.TextView mMainDescription;
    java.lang.String mSelectedVoice;
    @javax.inject.Inject
    android.content.SharedPreferences mSharedPrefs;
    @butterknife.InjectView(2131755895)
    android.widget.RadioButton mSmartBluetooth;
    @butterknife.InjectView(2131755897)
    android.widget.RadioButton mSpeaker;
    @butterknife.InjectView(2131755903)
    android.view.View mSpeechDelayPreference;
    boolean mSpeedWarningsIsOn;
    @butterknife.InjectView(2131755909)
    android.widget.Switch mSppedLimitWarnings;
    boolean mTbtInstructionsIsOn;
    float mTtsLevel;
    @butterknife.InjectView(2131755907)
    android.widget.Switch mTurnByTurnNavigation;
    @butterknife.InjectView(2131755902)
    android.widget.TextView mTxtVoiceName;
    @butterknife.InjectView(2131755900)
    android.view.View mVoicePreference;
    boolean mWelcomeMessage;
    @butterknife.InjectView(2131755908)
    android.widget.Switch mWelcomeMessageSwitch;
    @butterknife.InjectView(2131755899)
    com.navdy.client.app.ui.customviews.MultipleChoiceLayout speechLevelSettings;
    @butterknife.InjectView(2131755905)
    android.widget.TextView tvSpeechLevel;

    class Anon1 implements com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceListener {
        Anon1() {
        }

        public void onChoiceSelected(java.lang.String text, int index) {
            switch (index) {
                case 0:
                    com.navdy.client.app.ui.settings.AudioSettingsActivity.this.setTtsLevel(0.3f);
                    break;
                case 1:
                    com.navdy.client.app.ui.settings.AudioSettingsActivity.this.setTtsLevel(0.6f);
                    break;
                case 2:
                    com.navdy.client.app.ui.settings.AudioSettingsActivity.this.setTtsLevel(1.0f);
                    break;
            }
            com.navdy.client.app.ui.settings.AudioSettingsActivity.this.playTestAudio();
        }
    }

    class Anon2 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel[] val$resultantArray;

        Anon2(com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel[] voiceLabelArr) {
            this.val$resultantArray = voiceLabelArr;
        }

        public void onClick(android.content.DialogInterface dialog, int which) {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                com.navdy.client.app.ui.settings.AudioSettingsActivity.this.somethingChanged = true;
                com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel voiceLabel = this.val$resultantArray[which];
                if (voiceLabel != null) {
                    com.navdy.client.app.ui.settings.AudioSettingsActivity.sLogger.d("Selected voice :" + voiceLabel.label + " " + voiceLabel.voice.getName());
                    com.navdy.client.app.ui.settings.AudioSettingsActivity.this.mAudioRouter.getTextToSpeech().setVoice(voiceLabel.voice);
                    com.navdy.client.app.ui.settings.AudioSettingsActivity.this.mTxtVoiceName.setText(com.navdy.client.app.ui.settings.AudioSettingsActivity.this.mAudioRouter.getTextToSpeech().getVoice() != null ? voiceLabel.label : "");
                    com.navdy.client.app.ui.settings.AudioSettingsActivity.this.mSelectedVoice = voiceLabel.voice.getName();
                    return;
                }
                com.navdy.client.app.ui.settings.AudioSettingsActivity.sLogger.e("Selected voice does not exists , total :" + this.val$resultantArray.length + ", Index :" + which);
            }
        }
    }

    static class VoiceLabel {
        java.lang.String label;
        android.speech.tts.Voice voice;

        VoiceLabel() {
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_audio);
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder toolbarBuilder = new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder();
        toolbarBuilder.title((int) com.navdy.client.R.string.menu_audio).build();
        butterknife.ButterKnife.inject((android.app.Activity) this);
        this.audioTests = getResources().getStringArray(com.navdy.client.R.array.test_audio);
        this.audioTestIndex = new java.util.Random().nextInt(this.audioTests.length);
        if (this.mSharedPrefs.contains(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PHONE_VOLUME)) {
            int phoneVolumeLevel = this.mSharedPrefs.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PHONE_VOLUME, 75);
            this.mSharedPrefs.edit().remove(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PHONE_VOLUME).apply();
            if (!this.mSharedPrefs.contains(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME)) {
                if (phoneVolumeLevel <= 50) {
                    this.mSharedPrefs.edit().putFloat(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME, 0.3f).apply();
                } else if (phoneVolumeLevel <= 75) {
                    this.mSharedPrefs.edit().putFloat(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME, 0.6f).apply();
                } else {
                    this.mSharedPrefs.edit().putFloat(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME, 1.0f).apply();
                }
            }
        }
        this.mTtsLevel = this.mSharedPrefs.getFloat(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME, 0.6f);
        this.mWelcomeMessage = this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, false);
        this.mTbtInstructionsIsOn = this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, true);
        this.mSpeedWarningsIsOn = this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SPEED_WARNINGS, false);
        this.mCameraWarningsIsOn = this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_CAMERA_WARNINGS, true);
        this.mDriveScoreWarningsIsOn = this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_DRIVE_SCORE_WARNINGS, true);
        try {
            this.mSelectedVoice = this.mSharedPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_VOICE, null);
        } catch (java.lang.ClassCastException e) {
            sLogger.e("Class cast exception while getting the saved voice");
            this.mSharedPrefs.edit().remove(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_VOICE).apply();
        }
        this.audioOutput = com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput.values()[this.mSharedPrefs.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_OUTPUT_PREFERENCE, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_OUTPUT_PREFERENCE_DEFAULT)];
        boolean voiceAvailable = false;
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            voiceAvailable = true;
            try {
                android.speech.tts.TextToSpeech textToSpeech = this.mAudioRouter.getTextToSpeech();
                this.mInitialVoice = textToSpeech.getVoice();
                if (this.mInitialVoice == null) {
                    sLogger.e("TTS getVoice() returned null, trying to select default voice");
                    this.mInitialVoice = textToSpeech.getDefaultVoice();
                    if (this.mInitialVoice != null) {
                        sLogger.d("Default voice : " + this.mInitialVoice);
                        textToSpeech.setVoice(this.mInitialVoice);
                        this.mSelectedVoice = this.mInitialVoice.getName();
                    } else {
                        sLogger.d("Default voice is also null");
                    }
                }
                java.util.Set<android.speech.tts.Voice> voicesSet = textToSpeech.getVoices();
                if (voicesSet == null) {
                    sLogger.d("List of voices available is null. Not giving an option to select the voice");
                    voiceAvailable = false;
                } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.mSelectedVoice)) {
                    sLogger.d("User selected voice " + this.mSelectedVoice);
                    if (this.mInitialVoice == null || !this.mSelectedVoice.equals(this.mInitialVoice.getName())) {
                        java.util.Iterator<android.speech.tts.Voice> voicesIterator = voicesSet.iterator();
                        boolean found = false;
                        while (true) {
                            if (!voicesIterator.hasNext()) {
                                break;
                            }
                            android.speech.tts.Voice voice = (android.speech.tts.Voice) voicesIterator.next();
                            if (this.mSelectedVoice.equals(voice.getName())) {
                                textToSpeech.setVoice(voice);
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            sLogger.d("Could not find the voice that user selected in the list of voices");
                            if (this.mInitialVoice != null) {
                                sLogger.d("Falling back to the voice currently configured");
                                this.mSelectedVoice = this.mInitialVoice.getName();
                            } else {
                                sLogger.d("No voice currently configured on the TTS");
                                this.mSelectedVoice = null;
                            }
                        }
                    } else {
                        sLogger.d("User selected voice is same as TTS voice currently configured");
                    }
                } else {
                    sLogger.d("User selected voice is empty");
                    if (this.mInitialVoice != null) {
                        this.mSelectedVoice = this.mInitialVoice.getName();
                    }
                }
            } catch (Throwable e2) {
                sLogger.e("Exception while getting the voice ", e2);
                voiceAvailable = false;
            }
        }
        sLogger.d("Initializing the Audio preferences");
        sLogger.d("Welcome Message: " + this.mWelcomeMessage);
        sLogger.d("TTS Volume : " + this.mTtsLevel);
        sLogger.d("TBT : " + this.mTbtInstructionsIsOn);
        sLogger.d("Speed warning : " + this.mSpeedWarningsIsOn);
        sLogger.d("Camera warning : " + this.mCameraWarningsIsOn);
        sLogger.d("Drive Score warning : " + this.mDriveScoreWarningsIsOn);
        sLogger.d("Output Preference : " + this.audioOutput.name());
        sLogger.d("Saved Voice : " + this.mSelectedVoice);
        this.mMainDescription.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.settings_audio_title));
        if (this.mTurnByTurnNavigation != null) {
            this.mTurnByTurnNavigation.setChecked(this.mTbtInstructionsIsOn);
            this.mTurnByTurnNavigation.setOnCheckedChangeListener(this);
        }
        boolean ttsIsLow = this.mTtsLevel <= 0.3f;
        boolean ttsIsLoud = this.mTtsLevel >= 1.0f;
        com.navdy.client.app.ui.customviews.MultipleChoiceLayout multipleChoiceLayout = this.speechLevelSettings;
        int i = ttsIsLow ? 0 : ttsIsLoud ? 2 : 1;
        multipleChoiceLayout.setSelectedIndex(i);
        com.navdy.client.app.ui.customviews.MultipleChoiceLayout multipleChoiceLayout2 = this.speechLevelSettings;
        com.navdy.client.app.ui.settings.AudioSettingsActivity.Anon1 anon1 = new com.navdy.client.app.ui.settings.AudioSettingsActivity.Anon1();
        multipleChoiceLayout2.setChoiceListener(anon1);
        this.mWelcomeMessageSwitch.setChecked(this.mWelcomeMessage);
        this.mWelcomeMessageSwitch.setOnCheckedChangeListener(this);
        this.mSppedLimitWarnings.setChecked(this.mSpeedWarningsIsOn);
        this.mSppedLimitWarnings.setOnCheckedChangeListener(this);
        this.mCameraWarnings.setChecked(this.mCameraWarningsIsOn);
        this.mCameraWarnings.setOnCheckedChangeListener(this);
        this.mDriveScoreWarningsSwitch.setChecked(this.mDriveScoreWarningsIsOn);
        this.mDriveScoreWarningsSwitch.setOnCheckedChangeListener(this);
        if (android.os.Build.VERSION.SDK_INT < 21 || !voiceAvailable) {
            this.mVoicePreference.setVisibility(View.GONE);
        } else {
            this.mTxtVoiceName.setText(getDisplayName(this.mAudioRouter.getTextToSpeech().getVoice()));
        }
        if (this.audioOutput == com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput.BEST_AVAILABLE) {
            this.mSpeechDelayPreference.setVisibility(View.VISIBLE);
        } else {
            this.mSpeechDelayPreference.setVisibility(View.GONE);
        }
        setAudioOutputAndUpdateUI(this.audioOutput);
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.AUDIO);
        this.tvSpeechLevel.setText(java.lang.Integer.toString(this.mSharedPrefs.getInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1) + 1));
    }

    private void sendPreferencesToDisplay() {
        boolean success = com.navdy.client.app.ui.settings.SettingsUtils.sendNavSettingsToTheHud(com.navdy.client.app.ui.settings.SettingsUtils.buildNavigationPreferences(com.navdy.client.app.ui.settings.SettingsUtils.incrementSerialNumber("nav_serial_number"), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_ROUTE_CALCULATION, false), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_RECALC, false), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_HIGHWAYS, true), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TOLL_ROADS, true), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_FERRIES, true), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TUNNELS, true), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_UNPAVED_ROADS, true), this.mSharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_TRAINS, true), this.mSppedLimitWarnings.isChecked(), this.mCameraWarnings.isChecked(), this.mTurnByTurnNavigation.isChecked()));
        com.navdy.client.app.ui.settings.SettingsUtils.sendAudioPreferencesToDisplay(true);
        if (success) {
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Settings.AUDIO_SETTINGS_CHANGED);
        } else {
            showShortToast(com.navdy.client.R.string.settings_need_to_be_connected_to_hud, new java.lang.Object[0]);
        }
    }

    public void onCheckedChanged(android.widget.CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case com.navdy.client.R.id.turn_by_turn_instructions /*2131755907*/:
                this.mTbtInstructionsIsOn = isChecked;
                break;
            case com.navdy.client.R.id.welcome_message /*2131755908*/:
                this.mWelcomeMessage = isChecked;
                break;
            case com.navdy.client.R.id.speed_warnings /*2131755909*/:
                this.mSpeedWarningsIsOn = isChecked;
                break;
            case com.navdy.client.R.id.camera_warnings /*2131755910*/:
                this.mCameraWarningsIsOn = isChecked;
                break;
            case com.navdy.client.R.id.drive_score_warnings /*2131755911*/:
                this.mDriveScoreWarningsIsOn = isChecked;
                break;
        }
        this.somethingChanged = true;
    }

    protected void saveChanges() {
        this.mSharedPrefs.edit().putFloat(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TTS_VOLUME, this.mTtsLevel).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, this.mWelcomeMessage).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, this.mTbtInstructionsIsOn).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SPEED_WARNINGS, this.mSpeedWarningsIsOn).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_CAMERA_WARNINGS, this.mCameraWarningsIsOn).putString(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_VOICE, this.mSelectedVoice).putInt(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_OUTPUT_PREFERENCE, this.audioOutput.ordinal()).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_DRIVE_SCORE_WARNINGS, this.mDriveScoreWarningsIsOn).apply();
        sendPreferencesToDisplay();
    }

    @butterknife.OnClick({2131755894, 2131755896, 2131755897, 2131755893, 2131755900, 2131755903})
    public void onClick(android.view.View v) {
        switch (v.getId()) {
            case com.navdy.client.R.id.btn_test_audio /*2131755893*/:
                playTestAudio();
                return;
            case com.navdy.client.R.id.smart_bluetooth_container /*2131755894*/:
                setAudioOutputAndUpdateUI(com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput.BEST_AVAILABLE);
                this.mSpeechDelayPreference.setVisibility(View.VISIBLE);
                playTestAudio();
                return;
            case com.navdy.client.R.id.bluetooth /*2131755896*/:
                setAudioOutputAndUpdateUI(com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput.BLUETOOTH_MEDIA);
                this.mSpeechDelayPreference.setVisibility(View.GONE);
                playTestAudio();
                return;
            case com.navdy.client.R.id.speaker /*2131755897*/:
                setAudioOutputAndUpdateUI(com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput.PHONE_SPEAKER);
                this.mSpeechDelayPreference.setVisibility(View.GONE);
                playTestAudio();
                return;
            case com.navdy.client.R.id.preference_voice /*2131755900*/:
                showSimpleDialog(1, (java.lang.String) null, (java.lang.String) null);
                return;
            case com.navdy.client.R.id.preference_speech_delay /*2131755903*/:
                if (com.navdy.client.app.framework.util.TTSAudioRouter.isHFPSpeakerConnected()) {
                    com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.start(this);
                    return;
                } else {
                    com.navdy.client.app.ui.settings.AudioDialogActivity.showHFPNotConnected(this);
                    return;
                }
            default:
                return;
        }
    }

    private void playTestAudio() {
        if (this.mAudioRouter.isTTSAvailable() && this.audioTests != null && this.audioTests.length > 0) {
            this.mAudioRouter.processTTSRequest(this.audioTests[this.audioTestIndex], null, false);
            this.audioTestIndex++;
            if (this.audioTestIndex >= this.audioTests.length) {
                this.audioTestIndex = 0;
            }
        }
        com.navdy.client.app.ui.settings.AudioDialogActivity.startAudioStatusActivity(this);
    }

    private void setAudioOutputAndUpdateUI(com.navdy.client.app.framework.util.TTSAudioRouter.AudioOutput audioOutput2) {
        switch (audioOutput2) {
            case BEST_AVAILABLE:
                this.mSmartBluetooth.setChecked(true);
                this.mBluetooth.setChecked(false);
                this.mSpeaker.setChecked(false);
                break;
            case BLUETOOTH_MEDIA:
                this.mBluetooth.setChecked(true);
                this.mSmartBluetooth.setChecked(false);
                this.mSpeaker.setChecked(false);
                break;
            case PHONE_SPEAKER:
                this.mSpeaker.setChecked(true);
                this.mBluetooth.setChecked(false);
                this.mSmartBluetooth.setChecked(false);
                break;
        }
        this.audioOutput = audioOutput2;
        this.somethingChanged = true;
        this.mAudioRouter.setAudioOutput(audioOutput2, true);
    }

    public void setTtsLevel(float ttsLevel) {
        this.mTtsLevel = ttsLevel;
        this.mAudioRouter.setTtsVolume(this.mTtsLevel);
        this.somethingChanged = true;
    }

    public android.app.Dialog createDialog(int id, android.os.Bundle arguments) {
        switch (id) {
            case 1:
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    java.util.Set<android.speech.tts.Voice> voices = this.mAudioRouter.getTextToSpeech().getVoices();
                    if (voices != null && voices.size() > 0) {
                        java.lang.Object[] voicesArray = voices.toArray();
                        java.util.ArrayList<com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel> voiceLabelsList = new java.util.ArrayList<>();
                        int length = voicesArray.length;
                        for (int i = 0; i < length; i++) {
                            android.speech.tts.Voice voiceObj = (android.speech.tts.Voice) voicesArray[i];
                            com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel voiceLabel = new com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel();
                            voiceLabel.voice = voiceObj;
                            voiceLabel.label = getDisplayName(voiceObj);
                            voiceLabelsList.add(voiceLabel);
                        }
                        com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel[] resultantArray = new com.navdy.client.app.ui.settings.AudioSettingsActivity.VoiceLabel[voiceLabelsList.size()];
                        voiceLabelsList.toArray(resultantArray);
                        java.lang.CharSequence[] labels = new java.lang.CharSequence[resultantArray.length];
                        int length2 = resultantArray.length;
                        int i2 = 0;
                        int i3 = 0;
                        while (i2 < length2) {
                            int i4 = i3 + 1;
                            labels[i3] = resultantArray[i2].label;
                            i2++;
                            i3 = i4;
                        }
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                        builder.setItems(labels, new com.navdy.client.app.ui.settings.AudioSettingsActivity.Anon2(resultantArray));
                        android.app.AlertDialog dialog = builder.create();
                        dialog.setTitle(getString(com.navdy.client.R.string.select_voice));
                        return dialog;
                    }
                }
                break;
        }
        return super.createDialog(id, arguments);
    }

    public void onDescriptionClick(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.AudioDialogActivity.class));
    }

    private static java.lang.String getDisplayName(java.lang.Object voiceObj) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            return null;
        }
        android.speech.tts.Voice voice = (android.speech.tts.Voice) voiceObj;
        if (voice == null) {
            return "";
        }
        java.lang.String displayName = voice.getLocale().getDisplayName();
        java.lang.String uniqueName = voice.getName();
        int index = uniqueName.indexOf("#");
        if (index <= 0 || index >= uniqueName.length() - 2) {
            return displayName + org.droidparts.contract.SQL.DDL.OPENING_BRACE + uniqueName + ")";
        }
        return displayName + " " + uniqueName.substring(index + 1, uniqueName.length());
    }

    protected void discardChanges() {
        super.discardChanges();
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.mAudioRouter.getTextToSpeech().setVoice(this.mInitialVoice);
        }
        this.mAudioRouter.resetToUserPreference();
    }
}
