package com.navdy.client.app.ui.firstlaunch;

public class MarketingFlowActivity extends com.navdy.client.app.ui.base.BaseActivity {
    @android.support.annotation.DrawableRes
    private static final int[] BG_IMAGES = {com.navdy.client.R.drawable.img_meet_navdy_road, com.navdy.client.R.drawable.img_never_miss_turn_road, com.navdy.client.R.drawable.img_stay_connected, com.navdy.client.R.drawable.img_be_in_control_road};
    @android.support.annotation.StringRes
    private static final int[] DESC_TEXTS = {com.navdy.client.R.string.marketing_meet_navdy_desc, com.navdy.client.R.string.marketing_never_miss_turn_desc, com.navdy.client.R.string.marketing_stay_connected_desc, com.navdy.client.R.string.marketing_effortless_control_desc, com.navdy.client.R.string.marketing_get_started_desc};
    public static final int HAND_APPEARANCE_INDEX = 3;
    @android.support.annotation.DrawableRes
    private static final int[] HUD_UI_IMAGES = {com.navdy.client.R.drawable.img_fle_marketing_meet_navdy_ui, com.navdy.client.R.drawable.img_fle_marketing_never_miss_ui, com.navdy.client.R.drawable.img_fle_marketing_stay_connected_ui, com.navdy.client.R.drawable.img_fle_marketing_effortless_control_ui};
    /* access modifiers changed from: private */
    @android.support.annotation.DrawableRes
    public static final int[] PAGINATIONS = {com.navdy.client.R.drawable.pagination_1, com.navdy.client.R.drawable.pagination_2, com.navdy.client.R.drawable.pagination_3, com.navdy.client.R.drawable.pagination_4, com.navdy.client.R.drawable.pagination_5};
    /* access modifiers changed from: private */
    @android.support.annotation.StringRes
    public static final int[] TITLE_TEXTS = {com.navdy.client.R.string.marketing_meet_navdy, com.navdy.client.R.string.marketing_never_miss_turn, com.navdy.client.R.string.marketing_stay_connected, com.navdy.client.R.string.marketing_effortless_control, com.navdy.client.R.string.marketing_get_started};
    private int hiddenButtonClickCount = 0;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.CarMdUtils.buildCarList();
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        Anon2() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.this.onTextViewPagerClick(v);
        }
    }

    class Anon3 implements android.support.v4.view.ViewPager.OnPageChangeListener {
        boolean gotHandY = false;
        float handPivotY = 0.0f;
        final /* synthetic */ android.support.v4.view.ViewPager val$bgPager;
        final /* synthetic */ android.widget.TextView val$buyOne;
        final /* synthetic */ android.widget.ImageView val$hand;
        final /* synthetic */ android.support.v4.view.ViewPager val$hudUiPager;
        final /* synthetic */ android.widget.ImageView val$logo;
        final /* synthetic */ android.widget.ImageView val$pagination;
        final /* synthetic */ android.widget.Button val$start;
        final /* synthetic */ android.support.v4.view.ViewPager val$textPager;
        final /* synthetic */ float val$totalTranslationY;

        Anon3(android.widget.ImageView imageView, android.support.v4.view.ViewPager viewPager, android.support.v4.view.ViewPager viewPager2, float f, android.widget.ImageView imageView2, android.widget.TextView textView, android.widget.Button button, android.support.v4.view.ViewPager viewPager3, android.widget.ImageView imageView3) {
            this.val$hand = imageView;
            this.val$textPager = viewPager;
            this.val$bgPager = viewPager2;
            this.val$totalTranslationY = f;
            this.val$logo = imageView2;
            this.val$buyOne = textView;
            this.val$start = button;
            this.val$hudUiPager = viewPager3;
            this.val$pagination = imageView3;
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (!this.gotHandY) {
                this.gotHandY = true;
                this.handPivotY = this.val$hand.getPivotY();
            }
            this.val$hand.setPivotY(this.handPivotY + ((float) this.val$hand.getHeight()));
            int scrollX = this.val$textPager.getScrollX();
            this.val$bgPager.setScrollX(scrollX);
            if (position >= 2 && position <= 4) {
                this.val$hand.setRotation((-90.0f * (((((float) scrollX) - ((float) (this.val$textPager.getWidth() * 3))) * 100.0f) / ((float) this.val$textPager.getWidth()))) / 100.0f);
            }
            if (position >= com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.TITLE_TEXTS.length - 2) {
                float percentage = ((((float) scrollX) - ((float) (this.val$textPager.getWidth() * (com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.TITLE_TEXTS.length - 2)))) * 100.0f) / ((float) this.val$textPager.getWidth());
                float translation = (this.val$totalTranslationY * percentage) / 100.0f;
                this.val$logo.setTranslationY(translation);
                this.val$buyOne.setTranslationY((-this.val$totalTranslationY) + translation);
                this.val$start.setTranslationY((percentage * ((float) com.navdy.client.app.ui.UiUtils.convertDpToPx(80.0f))) / -100.0f);
            }
        }

        public void onPageSelected(int position) {
            this.val$hudUiPager.setCurrentItem(position);
            this.val$pagination.setImageResource(com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.PAGINATIONS[position]);
            if (position == 3) {
                this.val$hand.setVisibility(View.VISIBLE);
            } else {
                this.val$hand.setVisibility(View.GONE);
            }
            if (position >= com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.TITLE_TEXTS.length - 2) {
                this.val$start.setVisibility(View.VISIBLE);
            } else {
                this.val$start.setVisibility(View.GONE);
            }
            switch (position) {
                case 1:
                    com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Marketing.NEVER_MISS_A_TURN);
                    return;
                case 2:
                    com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Marketing.STAY_CONNECTED);
                    return;
                case 3:
                    com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Marketing.EFFORTLESS_CONTROL);
                    return;
                case 4:
                    com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Marketing.GET_STARTED);
                    return;
                default:
                    com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Marketing.MEET_NAVDY);
                    return;
            }
        }

        public void onPageScrollStateChanged(int state) {
            if (state == 0) {
                this.val$bgPager.setCurrentItem(this.val$textPager.getCurrentItem());
            }
        }
    }

    protected void onStart() {
        super.onStart();
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.Anon1(), 1);
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_marketing_flow);
        if (userHasFinishedMarketing()) {
            callInstallActivity(true);
        }
        android.support.v4.view.ViewPager bgPager = (android.support.v4.view.ViewPager) findViewById(com.navdy.client.R.id.bg_view_pager);
        android.support.v4.view.ViewPager hudUiPager = (android.support.v4.view.ViewPager) findViewById(com.navdy.client.R.id.hud_ui_view_pager);
        android.support.v4.view.ViewPager textPager = (android.support.v4.view.ViewPager) findViewById(com.navdy.client.R.id.text_view_pager);
        android.widget.ImageView pagination = (android.widget.ImageView) findViewById(com.navdy.client.R.id.pagination);
        android.widget.ImageView hand = (android.widget.ImageView) findViewById(com.navdy.client.R.id.hand);
        android.widget.TextView buyOne = (android.widget.TextView) findViewById(com.navdy.client.R.id.buy_one);
        android.widget.ImageView logo = (android.widget.ImageView) findViewById(com.navdy.client.R.id.navdy_logo);
        android.widget.Button start = (android.widget.Button) findViewById(com.navdy.client.R.id.start_install);
        if (bgPager != null && hudUiPager != null && textPager != null && pagination != null && hand != null && buyOne != null && logo != null && start != null) {
            hand.setTranslationX(((float) hand.getWidth()) / 2.0f);
            hand.setTranslationY(((float) hand.getHeight()) * 0.75f);
            float totalTranslationY = (float) com.navdy.client.app.ui.UiUtils.convertDpToPx(135.0f);
            buyOne.setTranslationY(-totalTranslationY);
            android.content.Context appContext = getApplicationContext();
            bgPager.setAdapter(new com.navdy.client.app.ui.firstlaunch.MarketingFlowImagePagerAdaper(appContext, BG_IMAGES, this.imageCache));
            hudUiPager.setAdapter(new com.navdy.client.app.ui.firstlaunch.MarketingFlowImagePagerAdaper(appContext, HUD_UI_IMAGES, this.imageCache));
            com.navdy.client.app.ui.firstlaunch.MarketingFlowTextPagerAdaper textPagerAdapter = new com.navdy.client.app.ui.firstlaunch.MarketingFlowTextPagerAdaper(appContext, TITLE_TEXTS, DESC_TEXTS, new com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.Anon2());
            buyOne.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.buy_navdy_at_navdycom));
            textPager.setAdapter(textPagerAdapter);
            textPager.addOnPageChangeListener(new com.navdy.client.app.ui.firstlaunch.MarketingFlowActivity.Anon3(hand, textPager, bgPager, totalTranslationY, logo, buyOne, start, hudUiPager, pagination));
            com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Marketing.MEET_NAVDY);
        }
    }

    public void onTextViewPagerClick(android.view.View view) {
        android.support.v4.view.ViewPager textPager = (android.support.v4.view.ViewPager) findViewById(com.navdy.client.R.id.text_view_pager);
        if (textPager != null) {
            int nextItem = textPager.getCurrentItem() + 1;
            android.support.v4.view.PagerAdapter adapter = textPager.getAdapter();
            if (adapter != null && nextItem < adapter.getCount()) {
                textPager.setCurrentItem(nextItem);
            }
        }
    }

    public void onBuyOneClick(android.view.View view) {
        openBrowserFor(android.net.Uri.parse(getString(com.navdy.client.R.string.purchase_link)));
    }

    public void onStartInstallClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FINISHED_MARKETING, true).apply();
        callInstallActivity(false);
    }

    public void callInstallActivity(boolean wasSkiped) {
        android.content.Intent installIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallIntroActivity.class);
        installIntent.putExtra(com.navdy.client.app.ui.settings.SettingsConstants.EXTRA_WAS_SKIPPED, wasSkiped);
        startActivity(installIntent);
    }

    public static boolean userHasFinishedMarketing() {
        return com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FINISHED_MARKETING, false);
    }

    public void onNavdyLogoClick(android.view.View view) {
    }
}
