package com.navdy.client.app.ui.settings;

public class ReplyViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    public final android.widget.ImageView more;
    public final android.widget.TextView text;

    ReplyViewHolder(android.view.View itemView) {
        super(itemView);
        this.text = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.text);
        this.more = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.more);
    }
}
