package com.navdy.client.app.ui.details;

public class DetailsActivity extends com.navdy.client.app.ui.base.BaseActivity implements com.google.android.gms.location.LocationListener, com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {
    private static final java.lang.String EXTRA_LOCATION = "location";
    public static final java.lang.String EXTRA_UPDATED_DESTINATION = "updated_destination";
    public static final int FINISH_PARENT_CODE = 1;
    private android.widget.ImageButton addToFavoritesButton;
    private android.widget.TextView address;
    private android.widget.ImageView badge;
    private com.navdy.client.app.framework.models.Destination destination;
    private com.navdy.client.app.ui.customviews.UnitSystemTextView distance;
    private android.widget.ImageView editFavoriteButton;
    private android.widget.ImageView fab;
    private com.google.android.gms.maps.GoogleMap googleMap;
    private com.navdy.client.app.ui.base.BaseGoogleMapFragment googleMapFragment;
    private boolean hasUpdatedDestination = false;
    private boolean isActivityForResult = false;
    private android.location.Location lastLocation;
    private com.google.android.gms.common.api.GoogleApiClient mGoogleApiClient;
    private com.google.android.gms.location.LocationRequest mLocationRequest;
    private android.support.v7.widget.Toolbar myToolbar;
    private final com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    private android.widget.TextView openHoursFirstLine;
    private android.widget.TextView openHoursSecondLine;
    private android.widget.TextView phone;
    private boolean requestedLocationUpdates;
    private android.widget.TextView title;
    private android.widget.TextView website;

    class Anon1 implements android.view.View.OnClickListener {

        /* renamed from: com.navdy.client.app.ui.details.DetailsActivity$Anon1$Anon1 reason: collision with other inner class name */
        class C0054Anon1 implements com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback {

            /* renamed from: com.navdy.client.app.ui.details.DetailsActivity$Anon1$Anon1$Anon1 reason: collision with other inner class name */
            class C0055Anon1 implements java.lang.Runnable {
                final /* synthetic */ int val$nbRows;

                C0055Anon1(int i) {
                    this.val$nbRows = i;
                }

                public void run() {
                    com.navdy.client.app.ui.details.DetailsActivity.this.hideProgressDialog();
                    if (this.val$nbRows > 0) {
                        com.navdy.client.app.ui.details.DetailsActivity.this.updateDestinationType();
                        com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.toast_favorite_edit_saved, new java.lang.Object[0]);
                        return;
                    }
                    com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.toast_favorite_edit_error, new java.lang.Object[0]);
                }
            }

            C0054Anon1() {
            }

            public void onQueryCompleted(int nbRows, @android.support.annotation.Nullable android.net.Uri uri) {
                com.navdy.client.app.ui.details.DetailsActivity.this.logger.d("saveDestinationAsFavorite");
                com.navdy.client.app.ui.details.DetailsActivity.this.runOnUiThread(new com.navdy.client.app.ui.details.DetailsActivity.Anon1.C0054Anon1.C0055Anon1(nbRows));
            }
        }

        Anon1() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.details.DetailsActivity.this.hasUpdatedDestination = true;
            if (com.navdy.client.app.ui.details.DetailsActivity.this.destination.isFavoriteDestination()) {
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.startFavoriteEditActivity(com.navdy.client.app.ui.details.DetailsActivity.this, com.navdy.client.app.ui.details.DetailsActivity.this.destination);
                return;
            }
            com.navdy.client.app.ui.details.DetailsActivity.this.showProgressDialog();
            com.navdy.client.app.ui.details.DetailsActivity.this.destination.saveDestinationAsFavoritesAsync(-1, new com.navdy.client.app.ui.details.DetailsActivity.Anon1.C0054Anon1());
        }
    }

    class Anon2 implements android.view.View.OnClickListener {

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.client.app.tracking.SetDestinationTracker.getInstance().tagSetDestinationEvent(com.navdy.client.app.ui.details.DetailsActivity.this.destination);
                com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().requestNewRoute(com.navdy.client.app.ui.details.DetailsActivity.this.destination);
                com.navdy.client.app.ui.details.DetailsActivity.this.finishThisAndParent();
            }
        }

        Anon2() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.details.DetailsActivity.this.showRequestNewRouteDialog(new com.navdy.client.app.ui.details.DetailsActivity.Anon2.Anon1());
        }
    }

    class Anon4 implements com.google.android.gms.maps.OnMapReadyCallback {
        Anon4() {
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap gMap) {
            com.navdy.client.app.ui.details.DetailsActivity.this.googleMap = gMap;
            com.navdy.client.app.ui.details.DetailsActivity.this.addMarkerAndCenterMap();
        }
    }

    class Anon5 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
        Anon5() {
        }

        public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
            com.navdy.client.app.ui.details.DetailsActivity.this.destination = destination;
            com.navdy.client.app.ui.details.DetailsActivity.this.addMarkerAndCenterMap();
            com.navdy.client.app.ui.details.DetailsActivity.this.badge.setImageResource(destination.getBadgeAsset());
            com.navdy.client.app.ui.details.DetailsActivity.this.initFavoriteButton();
            com.navdy.client.app.ui.details.DetailsActivity.this.hideProgressDialog();
        }

        public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
            com.navdy.client.app.ui.details.DetailsActivity.this.destination = destination;
            if (!destination.hasOneValidSetOfCoordinates()) {
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.invalid_address, new java.lang.Object[0]);
                com.navdy.client.app.ui.details.DetailsActivity.this.finish();
            }
            if (com.navdy.client.app.ui.details.DetailsActivity.this.googleMap != null && com.navdy.client.app.ui.details.DetailsActivity.this.googleMapFragment != null) {
                if (destination.hasValidDisplayCoordinates()) {
                    android.location.Location location = new android.location.Location("");
                    location.setLatitude(destination.displayLat);
                    location.setLongitude(destination.displayLong);
                    com.navdy.client.app.ui.details.DetailsActivity.this.googleMapFragment.moveMap(location, 14.0f, false);
                } else if (destination.hasValidNavCoordinates()) {
                    android.location.Location location2 = new android.location.Location("");
                    location2.setLatitude(destination.navigationLat);
                    location2.setLongitude(destination.navigationLong);
                    com.navdy.client.app.ui.details.DetailsActivity.this.googleMapFragment.moveMap(location2, 14.0f, false);
                } else {
                    com.navdy.client.app.ui.details.DetailsActivity.this.googleMapFragment.moveMap(com.navdy.client.app.ui.details.DetailsActivity.this.navdyLocationManager.getSmartStartLocation(), 14.0f, false);
                }
            }
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            com.navdy.client.app.ui.details.DetailsActivity.this.buildAndConnectGoogleApiClient();
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.cant_calculate_distances_without_location_permission, new java.lang.Object[0]);
        }
    }

    class Anon8 extends android.os.AsyncTask<java.lang.Object, java.lang.Object, com.navdy.client.app.framework.models.Destination> {
        Anon8() {
        }

        /* access modifiers changed from: protected */
        public com.navdy.client.app.framework.models.Destination doInBackground(java.lang.Object... params) {
            if (com.navdy.client.app.ui.details.DetailsActivity.this.destination != null) {
                return com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(com.navdy.client.app.ui.details.DetailsActivity.this.destination.id);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(com.navdy.client.app.framework.models.Destination d) {
            if (d != null) {
                com.navdy.client.app.ui.details.DetailsActivity.this.destination = d;
            }
            com.navdy.client.app.ui.details.DetailsActivity.this.fillTextViews();
            com.navdy.client.app.ui.details.DetailsActivity.this.updateDestinationType();
        }
    }

    class Anon9 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$placeId;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destinationFromDb;

            Anon1(com.navdy.client.app.framework.models.Destination destination) {
                this.val$destinationFromDb = destination;
            }

            public void run() {
                com.navdy.client.app.ui.details.DetailsActivity.this.destination = this.val$destinationFromDb;
                com.navdy.client.app.ui.details.DetailsActivity.this.initFavoriteButton();
                com.navdy.client.app.ui.details.DetailsActivity.this.updateDestinationType();
            }
        }

        Anon9(java.lang.String str) {
            this.val$placeId = str;
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination destinationFromDb = com.navdy.client.app.providers.NavdyContentProvider.getThisDestinationWithPlaceId(this.val$placeId);
            if (destinationFromDb != null && destinationFromDb.isFavoriteDestination()) {
                com.navdy.client.app.ui.details.DetailsActivity.this.runOnUiThread(new com.navdy.client.app.ui.details.DetailsActivity.Anon9.Anon1(destinationFromDb));
            }
        }
    }

    public static void startDetailsActivity(com.navdy.client.app.framework.models.Destination destination2, android.content.Context context) {
        if (context == null || !(context instanceof android.app.Activity)) {
            sLogger.w("startDetailsActivity, invalid context");
            return;
        }
        android.content.Intent i = new android.content.Intent(context, com.navdy.client.app.ui.details.DetailsActivity.class);
        i.putExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT, destination2);
        context.startActivity(i);
    }

    public static void startDetailsActivityForResult(com.navdy.client.app.framework.models.Destination destination2, android.content.Context context) {
        if (context == null || !(context instanceof android.app.Activity)) {
            sLogger.w("startDetailsActivity, invalid context");
            return;
        }
        android.content.Intent i = new android.content.Intent(context, com.navdy.client.app.ui.details.DetailsActivity.class);
        i.putExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT, destination2);
        ((android.app.Activity) context).startActivityForResult(i, 5);
    }

    private void initFavoriteButton() {
        if (this.destination.isFavoriteDestination()) {
            if (this.addToFavoritesButton != null) {
                this.addToFavoritesButton.setVisibility(View.INVISIBLE);
            }
            if (this.editFavoriteButton != null) {
                this.editFavoriteButton.setVisibility(View.VISIBLE);
            }
        } else {
            if (this.addToFavoritesButton != null) {
                this.addToFavoritesButton.setVisibility(View.VISIBLE);
            }
            if (this.editFavoriteButton != null) {
                this.editFavoriteButton.setVisibility(View.GONE);
            }
        }
        android.view.View.OnClickListener favoriteClickListener = new com.navdy.client.app.ui.details.DetailsActivity.Anon1();
        if (this.addToFavoritesButton != null) {
            this.addToFavoritesButton.setOnClickListener(favoriteClickListener);
        }
        if (this.editFavoriteButton != null) {
            this.editFavoriteButton.setOnClickListener(favoriteClickListener);
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.details_activity);
        android.content.Intent i = getIntent();
        if (i != null) {
            if (getCallingActivity() != null) {
                this.isActivityForResult = true;
            }
            this.destination = (com.navdy.client.app.framework.models.Destination) i.getParcelableExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT);
            this.lastLocation = (android.location.Location) i.getParcelableExtra(EXTRA_LOCATION);
            if (i.hasExtra("Type")) {
                i.getStringExtra("Type");
            }
            this.logger.v("lastLocation coordinates: " + (this.lastLocation != null ? this.lastLocation.getLatitude() + "," + this.lastLocation.getLongitude() : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
        }
        this.myToolbar = (android.support.v7.widget.Toolbar) findViewById(com.navdy.client.R.id.my_toolbar);
        setSupportActionBar(this.myToolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            setTitle(this.destination.name);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        this.googleMapFragment = (com.navdy.client.app.ui.base.BaseGoogleMapFragment) getFragmentManager().findFragmentById(com.navdy.client.R.id.place_details_map_view);
        this.badge = (android.widget.ImageView) findViewById(com.navdy.client.R.id.badge);
        this.title = (android.widget.TextView) findViewById(com.navdy.client.R.id.details_place_title);
        this.address = (android.widget.TextView) findViewById(com.navdy.client.R.id.place_details_address);
        this.distance = (com.navdy.client.app.ui.customviews.UnitSystemTextView) findViewById(com.navdy.client.R.id.place_details_distance_first_line);
        this.phone = (android.widget.TextView) findViewById(com.navdy.client.R.id.place_details_contact_first_line);
        this.website = (android.widget.TextView) findViewById(com.navdy.client.R.id.place_details_contact_second_line);
        this.openHoursFirstLine = (android.widget.TextView) findViewById(com.navdy.client.R.id.place_details_hours_first_line);
        this.openHoursSecondLine = (android.widget.TextView) findViewById(com.navdy.client.R.id.place_details_hours_second_line);
        fillTextViews();
        updateDestinationType();
        this.fab = (android.widget.ImageView) findViewById(com.navdy.client.R.id.fab);
        this.fab.setOnClickListener(new com.navdy.client.app.ui.details.DetailsActivity.Anon2());
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new com.navdy.client.app.ui.details.DetailsActivity.Anon4());
        }
        if (this.destination.isContact()) {
            showPhoneNumbers();
        }
        com.navdy.client.app.framework.search.GooglePlacesSearch googlePlacesSearch = new com.navdy.client.app.framework.search.GooglePlacesSearch(this, this.handler);
        if (this.destination.placeId != null) {
            googlePlacesSearch.runDetailsSearchWebApi(this.destination.placeId);
            updateFavoriteButtonIfAlreadyFavorited(this.destination.placeId);
        } else {
            if (!this.destination.hasOneValidSetOfCoordinates()) {
                if (this.addToFavoritesButton != null) {
                    this.addToFavoritesButton.setVisibility(View.INVISIBLE);
                }
                if (this.editFavoriteButton != null) {
                    this.editFavoriteButton.setVisibility(View.GONE);
                }
                showProgressDialog();
            }
            this.logger.v("Getting location coordinates for: " + this.destination.getAddressForDisplay());
            com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(this.destination, new com.navdy.client.app.ui.details.DetailsActivity.Anon5());
        }
        requestLocationPermission(new com.navdy.client.app.ui.details.DetailsActivity.Anon6(), new com.navdy.client.app.ui.details.DetailsActivity.Anon7());
        android.location.Location currentLocation = this.navdyLocationManager.getSmartStartLocation();
        if (currentLocation != null) {
            this.lastLocation = currentLocation;
        }
        com.navdy.client.app.tracking.SetDestinationTracker.getInstance().setSourceValue(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES.DETAILS);
    }

    protected void onResume() {
        super.onResume();
        updateOfflineBannerVisibility();
        new com.navdy.client.app.ui.details.DetailsActivity.Anon8().execute(new java.lang.Object[0]);
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.DETAILS);
    }

    public void finish() {
        if (this.isActivityForResult && this.hasUpdatedDestination) {
            this.logger.d("Destination was updated. Suggestions will be rebuilt");
            android.content.Intent i = new android.content.Intent();
            i.putExtra(EXTRA_UPDATED_DESTINATION, true);
            setResult(-1, i);
        }
        super.finish();
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        updateConnectionIndicator();
        return true;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onConnected(android.os.Bundle bundle) {
        this.logger.v("onConnected");
        try {
            this.lastLocation = com.google.android.gms.location.LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        } catch (java.lang.SecurityException securityException) {
            this.logger.e("Security Exception: ", securityException);
        }
        if (this.lastLocation == null) {
            this.logger.e("lastLocation was null. Now requesting location update.");
            requestLocationUpdate();
        }
    }

    public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
        this.logger.v("OnGoogleSearchResult");
        if (destinations != null && !destinations.isEmpty()) {
            displayGooglePlaceDetailData((com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) destinations.listIterator().next());
        }
    }

    public void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult connectionResult) {
        this.logger.e("onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    public void onConnectionSuspended(int i) {
        this.logger.e("GoogleApiClient connection suspended.");
    }

    public void onLocationChanged(android.location.Location location) {
        if (this.lastLocation == null) {
            this.logger.v("Location changed");
            this.lastLocation = location;
            stopLocationUpdates();
        }
    }

    @com.squareup.otto.Subscribe
    public void appInstanceDeviceConnectionEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent deviceConnectedEvent) {
        this.logger.v("appInstanceDeviceConnectionEvent");
        updateConnectionIndicator();
    }

    @com.squareup.otto.Subscribe
    public void appInstanceDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent deviceDisconnectedEvent) {
        this.logger.v("appInstanceDeviceDisconnectedEvent");
        updateConnectionIndicator();
    }

    private void updateDestinationType() {
        addMarkerAndCenterMap();
        this.badge.setImageResource(this.destination.getBadgeAsset());
        this.addToFavoritesButton = (android.widget.ImageButton) findViewById(com.navdy.client.R.id.add_to_favorites_button);
        this.editFavoriteButton = (android.widget.ImageView) findViewById(com.navdy.client.R.id.edit_favorites_button);
        initFavoriteButton();
    }

    private void addMarkerAndCenterMap() {
        if (this.googleMap != null) {
            this.googleMap.clear();
        }
        com.google.android.gms.maps.model.LatLng destinationLatLng = null;
        if (this.destination.hasValidDisplayCoordinates()) {
            destinationLatLng = new com.google.android.gms.maps.model.LatLng(this.destination.displayLat, this.destination.displayLong);
        } else if (this.destination.hasValidNavCoordinates()) {
            destinationLatLng = new com.google.android.gms.maps.model.LatLng(this.destination.navigationLat, this.destination.navigationLong);
        }
        if (destinationLatLng != null && this.googleMap != null) {
            com.google.android.gms.maps.model.MarkerOptions markerOptions = new com.google.android.gms.maps.model.MarkerOptions();
            markerOptions.title(this.destination.name);
            markerOptions.draggable(false);
            markerOptions.position(destinationLatLng);
            markerOptions.icon(com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource(this.destination.getPinAsset()));
            this.googleMap.addMarker(markerOptions);
            android.location.Location location = new android.location.Location("");
            location.setLatitude(destinationLatLng.latitude);
            location.setLongitude(destinationLatLng.longitude);
            if (this.googleMapFragment != null) {
                this.googleMapFragment.moveMap(location, 14.0f, false);
            }
        }
    }

    private void updateConnectionIndicator() {
        android.content.Context applicationContext = getApplicationContext();
        if (com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected()) {
            this.myToolbar.getMenu().removeItem(0);
            this.fab.setImageResource(com.navdy.client.R.drawable.button_start);
            return;
        }
        android.content.Intent intent = new android.content.Intent(applicationContext, com.navdy.client.app.ui.settings.BluetoothPairActivity.class);
        intent.putExtra(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
        this.myToolbar.getMenu().add(0, 0, 0, com.navdy.client.R.string.navdy_not_connected).setIcon(com.navdy.client.R.drawable.icon_display_disconnected).setIntent(intent).setShowAsAction(1);
        this.fab.setImageResource(com.navdy.client.R.drawable.button_start_offline);
    }

    private void fillTextViews() {
        java.util.ArrayList<java.lang.String> addressLines = this.destination.getAddressLines();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.destination.name)) {
            if (this.title != null) {
                this.title.setText(this.destination.name);
            }
            if (this.address != null) {
                this.address.setText(com.navdy.client.app.framework.util.StringUtils.join(addressLines, "\n"));
                return;
            }
            return;
        }
        if (this.title != null) {
            this.title.setText((java.lang.CharSequence) addressLines.remove(0));
        }
        if (this.address != null) {
            this.address.setText(com.navdy.client.app.framework.util.StringUtils.join(addressLines, "\n"));
        }
    }

    private void buildAndConnectGoogleApiClient() {
        this.mGoogleApiClient = new com.google.android.gms.common.api.GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(com.google.android.gms.location.places.Places.GEO_DATA_API).addApi(com.google.android.gms.location.places.Places.PLACE_DETECTION_API).addApi(com.google.android.gms.location.LocationServices.API).build();
        this.mGoogleApiClient.connect();
    }

    private void showPhoneNumbers() {
        java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> contacts = this.destination.getContacts();
        java.util.ArrayList<java.lang.String> phoneNumbers = new java.util.ArrayList<>();
        java.util.Iterator it = contacts.iterator();
        while (it.hasNext()) {
            com.navdy.service.library.events.contacts.Contact contact = (com.navdy.service.library.events.contacts.Contact) it.next();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(contact.number)) {
                phoneNumbers.add(getString(com.navdy.client.R.string.phone_number_with_type, new java.lang.Object[]{contact.number, getLocalizedNumberTypeString(contact.numberType, contact.label)}));
            }
        }
        if (phoneNumbers.size() > 0) {
            this.phone.setText(com.navdy.client.app.framework.util.StringUtils.join(phoneNumbers, "\n"));
            this.phone.setVisibility(View.VISIBLE);
            android.widget.TextView infoTitle = (android.widget.TextView) findViewById(com.navdy.client.R.id.contact_title);
            if (infoTitle != null) {
                infoTitle.setVisibility(View.VISIBLE);
            }
        }
    }

    @android.support.annotation.NonNull
    private java.lang.String getLocalizedNumberTypeString(com.navdy.service.library.events.contacts.PhoneNumberType numberType, @android.support.annotation.Nullable java.lang.String label) {
        switch (numberType) {
            case PHONE_NUMBER_HOME:
                return getString(com.navdy.client.R.string.home);
            case PHONE_NUMBER_WORK:
                return getString(com.navdy.client.R.string.work);
            case PHONE_NUMBER_MOBILE:
                return getString(com.navdy.client.R.string.mobile);
            default:
                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(label)) {
                    return getString(com.navdy.client.R.string.other);
                }
                return label;
        }
    }

    private void displayGooglePlaceDetailData(com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destinationResult) {
        this.logger.v("destinationResult: " + destinationResult);
        destinationResult.toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.DETAILS, this.destination);
        this.destination.persistPlaceDetailInfoAndUpdateListsAsync();
        fillTextViews();
        addMarkerAndCenterMap();
        this.badge.setImageResource(this.destination.getBadgeAsset());
        if (!(destinationResult.lat == null || destinationResult.lng == null || this.lastLocation == null)) {
            android.location.Location destinationLocation = new android.location.Location("");
            double destinationLat = java.lang.Double.parseDouble(destinationResult.lat);
            double destinationLng = java.lang.Double.parseDouble(destinationResult.lng);
            destinationLocation.setLatitude(destinationLat);
            destinationLocation.setLongitude(destinationLng);
            this.logger.v("Destination Location lat long " + destinationLocation.getLatitude() + " " + destinationLocation.getLongitude());
            java.lang.Float floatDistanceMeters = java.lang.Float.valueOf(this.lastLocation.distanceTo(destinationLocation));
            this.distance.setDistance((double) floatDistanceMeters.floatValue());
            this.distance.setVisibility(View.VISIBLE);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationResult.phone)) {
            this.phone.setText(destinationResult.phone);
            this.phone.setVisibility(View.VISIBLE);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationResult.url)) {
            this.website.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml("<a href=\"" + destinationResult.url + "\">" + getString(com.navdy.client.R.string.website) + "</a>"));
            this.website.setVisibility(View.VISIBLE);
            this.website.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        }
        if (destinationResult.open_hours != null) {
            this.logger.v("destinationResult is not null");
            if (!destinationResult.open_hours.isEmpty()) {
                java.lang.String openHours = (java.lang.String) destinationResult.open_hours.get(getDayFromCalendarOrdinal(java.util.Calendar.getInstance().get(7)));
                java.lang.String openHours2 = openHours.substring(openHours.indexOf(58));
                if (destinationResult.open_now) {
                    this.openHoursFirstLine.setText(com.navdy.client.R.string.open_now);
                } else {
                    this.openHoursFirstLine.setText(com.navdy.client.R.string.today);
                }
                this.openHoursFirstLine.setVisibility(View.VISIBLE);
                this.openHoursSecondLine.setText(openHours2);
                this.openHoursSecondLine.setVisibility(View.VISIBLE);
            }
        }
        java.lang.String typeString = null;
        if (destinationResult.types != null && destinationResult.types.length > 0) {
            android.widget.TextView type = (android.widget.TextView) findViewById(com.navdy.client.R.id.place_details_place_type);
            if (type != null) {
                try {
                    org.json.JSONArray array = new org.json.JSONArray(destinationResult.types[0]);
                    if (array.length() > 0) {
                        typeString = (java.lang.String) array.get(0);
                    }
                } catch (org.json.JSONException e) {
                    e.printStackTrace();
                }
                if (isValidPlaceType(typeString)) {
                    int stringId = getResources().getIdentifier("place_type_" + typeString, "string", com.navdy.client.BuildConfig.APPLICATION_ID);
                    if (stringId > 0) {
                        type.setText(stringId);
                        type.setVisibility(View.VISIBLE);
                    } else {
                        this.logger.v("Unable to find a type string for type: " + typeString);
                    }
                }
            }
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationResult.price_level)) {
            android.widget.TextView cost = (android.widget.TextView) findViewById(com.navdy.client.R.id.place_details_cost);
            if (cost != null) {
                cost.setText(com.navdy.client.app.ui.search.SearchRecyclerAdapter.getPriceString(destinationResult.price_level));
                cost.setVisibility(View.VISIBLE);
            }
        }
        if (destinationResult.open_hours != null || isValidPlaceType(typeString) || !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationResult.price_level) || !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationResult.phone) || !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationResult.url)) {
            android.widget.TextView infoTitle = (android.widget.TextView) findViewById(com.navdy.client.R.id.contact_title);
            if (infoTitle != null) {
                infoTitle.setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean isValidPlaceType(java.lang.String typeString) {
        return typeString != null && !"street_address".equals(typeString);
    }

    private int getDayFromCalendarOrdinal(int day) {
        switch (day) {
            case 1:
                return 6;
            case 3:
                return 1;
            case 4:
                return 2;
            case 5:
                return 3;
            case 6:
                return 4;
            case 7:
                return 5;
            default:
                return 0;
        }
    }

    private void requestLocationUpdate() {
        createLocationRequest();
        this.logger.i("calling requestLocationUpdates from FusedLocationApi");
        try {
            com.google.android.gms.location.LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        } catch (java.lang.SecurityException se) {
            sLogger.e((java.lang.Throwable) se);
        }
    }

    private void createLocationRequest() {
        this.logger.i("creating LocationRequest Object");
        this.mLocationRequest = com.google.android.gms.location.LocationRequest.create().setPriority(100).setInterval(10000).setFastestInterval(1000);
    }

    private void stopLocationUpdates() {
        if (this.requestedLocationUpdates) {
            com.google.android.gms.location.LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
            this.requestedLocationUpdates = false;
        }
    }

    private void finishThisAndParent() {
        setResult(1);
        finish();
    }

    private void updateFavoriteButtonIfAlreadyFavorited(java.lang.String placeId) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.details.DetailsActivity.Anon9(placeId), 1);
    }

    @com.squareup.otto.Subscribe
    public void handleReachabilityStateChange(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        android.view.View offlineBanner = findViewById(com.navdy.client.R.id.offline_banner);
        if (offlineBanner != null) {
            offlineBanner.setVisibility(com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() ? 8 : 0);
        }
    }

    public void onRefreshConnectivityClick(android.view.View view) {
        com.navdy.client.app.framework.AppInstance.getInstance().checkForNetwork();
    }
}
