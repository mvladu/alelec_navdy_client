package com.navdy.client.app.ui.settings;

public class LegalSettingsActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_legal);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_legal).build();
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.LEGAL);
    }

    public void onAttributionClick(android.view.View view) {
        android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
        browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.ATTRIBUTION);
        startActivity(browserIntent);
    }

    public void onPrivacyPolicyClick(android.view.View view) {
        android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
        browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    public void onTermsClick(android.view.View view) {
        android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
        browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.TERMS);
        startActivity(browserIntent);
    }
}
