package com.navdy.client.app.ui.settings;

public class CalendarViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    private android.widget.TextView calendarName;
    private android.widget.TextView calendarOwner;
    private android.widget.ImageView checkMark;
    private android.view.View color;
    private android.view.View rootView;
    private final android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.client.app.framework.models.Calendar val$calendar;

        Anon1(com.navdy.client.app.framework.models.Calendar calendar) {
            this.val$calendar = calendar;
        }

        public void onClick(android.view.View v) {
            boolean enabled = true;
            int i = 0;
            if (com.navdy.client.app.ui.settings.CalendarViewHolder.this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDARS_ENABLED, true)) {
                java.lang.String sharedPrefKey = com.navdy.client.app.ui.settings.SettingsConstants.CALENDAR_PREFIX + this.val$calendar.id;
                if (com.navdy.client.app.ui.settings.CalendarViewHolder.this.sharedPrefs.getBoolean(sharedPrefKey, this.val$calendar.visible)) {
                    enabled = false;
                }
                com.navdy.client.app.ui.settings.CalendarViewHolder.this.sharedPrefs.edit().putBoolean(sharedPrefKey, enabled).apply();
                android.widget.ImageView access$Anon100 = com.navdy.client.app.ui.settings.CalendarViewHolder.this.checkMark;
                if (!enabled) {
                    i = 8;
                }
                access$Anon100.setVisibility(i);
            }
        }
    }

    CalendarViewHolder(android.view.View v) {
        super(v);
        this.rootView = v;
        this.color = v.findViewById(com.navdy.client.R.id.color);
        this.calendarName = (android.widget.TextView) v.findViewById(com.navdy.client.R.id.calendar_name);
        this.calendarOwner = (android.widget.TextView) v.findViewById(com.navdy.client.R.id.calendar_owner);
        this.checkMark = (android.widget.ImageView) v.findViewById(com.navdy.client.R.id.checkmark);
    }

    public void setCalendar(com.navdy.client.app.framework.models.Calendar calendar) {
        int i = 0;
        if (this.color != null && this.calendarName != null && this.calendarOwner != null && this.checkMark != null) {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            ((android.graphics.drawable.GradientDrawable) this.color.getBackground()).setColor(calendar.calendarColor);
            boolean globalSwitchIsEnabled = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDARS_ENABLED, true);
            this.calendarName.setText(calendar.displayName);
            this.calendarOwner.setText(context.getString(com.navdy.client.R.string.shared_by, new java.lang.Object[]{calendar.accountName}));
            boolean enabled = isEnabled(calendar);
            android.widget.ImageView imageView = this.checkMark;
            if (!enabled) {
                i = 8;
            }
            imageView.setVisibility(i);
            this.rootView.setAlpha(globalSwitchIsEnabled ? 1.0f : 0.3f);
            this.rootView.setOnClickListener(new com.navdy.client.app.ui.settings.CalendarViewHolder.Anon1(calendar));
        }
    }

    private boolean isEnabled(com.navdy.client.app.framework.models.Calendar calendar) {
        return this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDAR_PREFIX + calendar.id, calendar.visible);
    }
}
