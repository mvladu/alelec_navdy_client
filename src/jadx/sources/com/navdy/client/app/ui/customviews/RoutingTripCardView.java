package com.navdy.client.app.ui.customviews;

public class RoutingTripCardView extends com.navdy.client.app.ui.customviews.TripCardView {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.customviews.RoutingTripCardView.class);

    public RoutingTripCardView(android.content.Context context) {
        this(context, null, 0);
    }

    public RoutingTripCardView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoutingTripCardView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected java.lang.String appendPrefix(java.lang.String name) {
        return name;
    }

    public void setNameAddressAndIcon(com.navdy.client.app.framework.models.Destination destination) {
        if (destination == null) {
            logger.d("Destination object is null");
            return;
        }
        if (this.icon != null) {
            this.icon.setImageResource(destination.getBadgeAssetForActiveTrip());
        }
        com.navdy.client.app.framework.models.Destination.MultilineAddress mla = destination.getMultilineAddress();
        if (this.tripName != null) {
            this.tripName.setText(appendPrefix(mla.title));
        }
        if (this.tripAddressFirstLine != null) {
            this.tripAddressFirstLine.setText(mla.addressFirstLine);
        }
        if (this.tripAddressSecondLine != null) {
            this.tripAddressSecondLine.setText(mla.addressSecondLine);
        }
    }
}
