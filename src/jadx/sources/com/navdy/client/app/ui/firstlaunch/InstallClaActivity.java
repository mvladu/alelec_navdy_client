package com.navdy.client.app.ui.firstlaunch;

public class InstallClaActivity extends com.navdy.client.app.ui.base.BaseActivity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_install_cla);
        loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.image_12_v_fpo);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.PLUG_CLA);
    }

    public void onNextClick(android.view.View view) {
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
        intent.putExtra("extra_step", com.navdy.client.R.layout.fle_install_tidying_up);
        startActivity(intent);
    }

    public void onBackClick(android.view.View view) {
        finish();
    }
}
