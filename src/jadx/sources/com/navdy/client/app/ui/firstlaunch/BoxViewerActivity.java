package com.navdy.client.app.ui.firstlaunch;

public class BoxViewerActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    public static final java.lang.String EXTRA_BOX = "extra_box";
    private java.lang.String box;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_box_viewer);
        android.view.View next = findViewById(com.navdy.client.R.id.next);
        this.box = getIntent().getStringExtra(EXTRA_BOX);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.box) && next != null) {
            next.performClick();
        }
    }

    public void onNextClick(android.view.View view) {
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (sharedPreferences != null) {
            java.lang.String lastConfig = sharedPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_CONFIG, "");
            java.lang.String currentConfig = getCurrentConfig();
            if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(lastConfig, currentConfig)) {
                sharedPreferences.edit().putInt(com.navdy.client.app.ui.settings.SettingsConstants.NB_CONFIG, sharedPreferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.NB_CONFIG, 0) + 1).putString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_CONFIG, currentConfig).apply();
            }
            sharedPreferences.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HAS_SKIPPED_INSTALL, false).apply();
        }
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.class);
        intent.putExtra("extra_next_step", "installation_flow");
        startActivity(intent);
        finish();
    }

    private java.lang.String getCurrentConfig() {
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        java.lang.String carYear = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String carMake = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String carModel = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        return carYear + carMake + carModel + this.box + com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter.getCurrentMountType(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences()).getValue();
    }

    protected void onResume() {
        hideSystemUI();
        android.widget.TextView title = (android.widget.TextView) findViewById(com.navdy.client.R.id.title);
        android.widget.TextView subtitle = (android.widget.TextView) findViewById(com.navdy.client.R.id.subtitle);
        java.lang.String str = this.box;
        char c = 65535;
        switch (str.hashCode()) {
            case -784816532:
                if (str.equals("New_Box")) {
                    c = 0;
                    break;
                }
                break;
            case 285544307:
                if (str.equals("Old_Box")) {
                    c = 2;
                    break;
                }
                break;
            case 1554803820:
                if (str.equals("New_Box_Plus_Mounts")) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                loadImage(com.navdy.client.R.id.box_photo, com.navdy.client.R.drawable.image_navdy_box);
                if (title != null) {
                    title.setText(com.navdy.client.R.string.new_box_title);
                }
                if (subtitle != null) {
                    subtitle.setText(com.navdy.client.R.string.new_box_long_description);
                }
                com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.BOX_VIEWER_NEW_BOX);
                break;
            case 1:
                loadImage(com.navdy.client.R.id.box_photo, com.navdy.client.R.drawable.image_navdy_plus_kit);
                if (title != null) {
                    title.setText(com.navdy.client.R.string.new_box_plus_mounts_title);
                }
                if (subtitle != null) {
                    subtitle.setText(com.navdy.client.R.string.new_box_plus_mounts_long_description);
                }
                com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.BOX_VIEWER_NEW_BOX_PLUS_MOUNTS);
                break;
            case 2:
                loadImage(com.navdy.client.R.id.box_photo, com.navdy.client.R.drawable.image_navdy_oversized);
                if (title != null) {
                    title.setText(com.navdy.client.R.string.old_box_title);
                }
                if (subtitle != null) {
                    subtitle.setText(com.navdy.client.R.string.old_box_long_description);
                }
                com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.BOX_VIEWER_OLD_BOX);
                break;
            default:
                this.logger.e("Invalid box type: " + this.box);
                finish();
                break;
        }
        super.onResume();
    }

    public void onBackClick(android.view.View view) {
        finish();
    }
}
