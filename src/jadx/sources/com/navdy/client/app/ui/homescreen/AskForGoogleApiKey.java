package com.navdy.client.app.ui.homescreen;

public class AskForGoogleApiKey extends com.navdy.client.app.ui.base.BaseActivity {
    public static final java.lang.String EXTRA_TYPE = "EXTRA_TYPE";
    public static final java.lang.String TYPE_MAPS = "TYPE_MAPS";
    public static final java.lang.String TYPE_SERVICES = "TYPE_SERVICES";
    private boolean isMaps = true;

    protected void onResume() {
        super.onResume();
        setContentView((int) com.navdy.client.R.layout.ask_activity);
        android.widget.TextView title = (android.widget.TextView) findViewById(com.navdy.client.R.id.title);
        android.widget.TextView description = (android.widget.TextView) findViewById(com.navdy.client.R.id.description);
        android.widget.EditText input = (android.widget.EditText) findViewById(com.navdy.client.R.id.input);
        android.content.Intent callingIntent = getIntent();
        if (!callingIntent.hasExtra(EXTRA_TYPE)) {
            return;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(callingIntent.getStringExtra(EXTRA_TYPE), TYPE_MAPS)) {
            title.setText(com.navdy.client.R.string.google_maps_api_title);
            description.setText(com.navdy.client.R.string.google_maps_api_desc);
            input.setHint(com.navdy.client.R.string.google_maps_api_input);
            this.isMaps = true;
            return;
        }
        title.setText(com.navdy.client.R.string.google_services_api_title);
        description.setText(com.navdy.client.R.string.google_services_api_desc);
        input.setHint(com.navdy.client.R.string.google_services_api_input);
        this.isMaps = false;
    }

    public void onGetKeyClicked(android.view.View view) {
        java.lang.String url = "https://developers.google.com/places/web-service/get-api-key";
        if (this.isMaps) {
            url = "https://developers.google.com/maps/documentation/android-api/signup";
        }
        startActivity(new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse(url)));
    }

    public void onUseThisKeyClicked(android.view.View view) {
        java.lang.String key = java.lang.String.valueOf(((android.widget.EditText) findViewById(com.navdy.client.R.id.input)).getText());
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(key) || com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID.equals(key)) {
            showShortToast(com.navdy.client.R.string.invalid_key, new java.lang.Object[0]);
        } else {
            android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (this.isMaps) {
                sharedPreferences.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.GOOGLE_MAPS_API_KEY, key).apply();
            } else {
                sharedPreferences.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.GOOGLE_SERVICES_API_KEY, key).apply();
            }
        }
        finish();
    }
}
