package com.navdy.client.app.ui.settings;

public class FeatureVideosActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    private android.support.v7.widget.LinearLayoutManager layoutManager;
    private com.navdy.client.app.ui.settings.FeatureVideosAdapter mFeatureAdapter;
    private android.support.v7.widget.RecyclerView mFeatureRecycler;

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_feature_videos_activity);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.feature_videos).build();
        this.mFeatureRecycler = (android.support.v7.widget.RecyclerView) findViewById(com.navdy.client.R.id.features_recycler_view);
        this.mFeatureAdapter = new com.navdy.client.app.ui.settings.FeatureVideosAdapter(this);
        this.layoutManager = new android.support.v7.widget.LinearLayoutManager(this);
        this.layoutManager.setOrientation(1);
        this.mFeatureRecycler.setLayoutManager(this.layoutManager);
        this.mFeatureRecycler.setAdapter(this.mFeatureAdapter);
    }

    protected void onResume() {
        super.onResume();
        if (this.mFeatureAdapter != null) {
            this.mFeatureAdapter.notifyDataSetChanged();
        }
    }
}
