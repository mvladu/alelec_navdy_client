package com.navdy.client.app.ui.search;

public final class SearchActivity$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.ui.search.SearchActivity> implements javax.inject.Provider<com.navdy.client.app.ui.search.SearchActivity>, dagger.MembersInjector<com.navdy.client.app.ui.search.SearchActivity> {
    private dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> mAudioRouter;
    private dagger.internal.Binding<com.navdy.client.app.ui.base.BaseActivity> supertype;

    public SearchActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.search.SearchActivity", "members/com.navdy.client.app.ui.search.SearchActivity", false, com.navdy.client.app.ui.search.SearchActivity.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", com.navdy.client.app.ui.search.SearchActivity.class, getClass().getClassLoader());
        dagger.internal.Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseActivity", com.navdy.client.app.ui.search.SearchActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mAudioRouter);
        injectMembersBindings.add(this.supertype);
    }

    public com.navdy.client.app.ui.search.SearchActivity get() {
        com.navdy.client.app.ui.search.SearchActivity result = new com.navdy.client.app.ui.search.SearchActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.ui.search.SearchActivity object) {
        object.mAudioRouter = (com.navdy.client.app.framework.util.TTSAudioRouter) this.mAudioRouter.get();
        this.supertype.injectMembers(object);
    }
}
