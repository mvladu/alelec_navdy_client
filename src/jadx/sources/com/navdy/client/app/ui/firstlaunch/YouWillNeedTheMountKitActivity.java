package com.navdy.client.app.ui.firstlaunch;

public class YouWillNeedTheMountKitActivity extends com.navdy.client.app.ui.base.BaseActivity {
    public static final java.lang.String MOUNT_UPSELL_URL = "https://shop.navdy.com/products/mount-kit?utm_source=navdy-app&utm_medium=upsell";
    private com.navdy.client.app.framework.models.MountInfo mountInfo;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_you_will_need_the_mount_kit);
        java.lang.String modelString = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        android.widget.TextView description = (android.widget.TextView) findViewById(com.navdy.client.R.id.you_will_need_the_mount_kit_description);
        if (description != null) {
            description.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml(com.navdy.client.R.string.you_will_need_the_mount_kit_description, modelString));
        }
        loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.asset_install_mount_kit);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.MOUNT_KIT_NEEDED);
        this.mountInfo = com.navdy.client.app.ui.settings.SettingsUtils.getMountInfo();
    }

    public void onDescriptionClick(android.view.View view) {
        android.content.Intent intent;
        if (!this.mountInfo.shortSupported) {
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.CONTINUE_WITH_UNSUPPORTED_SHORT_MOUNT);
            intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.MountNotRecommendedActivity.class);
            intent.putExtra("extra_mount", com.navdy.client.app.framework.models.MountInfo.MountType.SHORT.getValue());
        } else {
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.CONTINUE_WITH_UNRECOMMENDED_SHORT_MOUNT);
            intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
            intent.putExtra("extra_mount", com.navdy.client.app.framework.models.MountInfo.MountType.SHORT.getValue());
        }
        startActivity(intent);
    }

    public void onUserHasMediumMountClick(android.view.View view) {
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.ACTUALLY_I_ALREADY_HAVE_MED_TALL_MOUNT);
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "New_Box_Plus_Mounts").apply();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
        intent.putExtra("extra_mount", com.navdy.client.app.framework.models.MountInfo.MountType.TALL.getValue());
        startActivity(intent);
    }

    public void onPurchaseClick(android.view.View view) {
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.MOUNT_KIT_PURCHASE);
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.PURCHASE_MOUNT_KIT);
        openBrowserFor(android.net.Uri.parse(MOUNT_UPSELL_URL));
    }

    public void onBackClick(android.view.View view) {
        onBackPressed();
    }
}
