package com.navdy.client.app.ui.glances;

public class GlanceUtils {
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.glances.GlanceUtils.class);

    public static boolean isDrivingGlance(java.lang.String pkg) {
        return com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(pkg, com.navdy.client.app.framework.glances.GlanceConstants.Group.DRIVING_GLANCES);
    }

    public static boolean isWhiteListedApp(java.lang.String pkg) {
        return com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(pkg, com.navdy.client.app.framework.glances.GlanceConstants.Group.WHITE_LIST);
    }

    public static boolean isCallGlancesEnabled() {
        android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        java.lang.String dialerPackage = com.navdy.client.app.ui.settings.SettingsUtils.getDialerPackage(appContext.getPackageManager());
        if (!sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false) || !sharedPrefs.getBoolean(dialerPackage, true)) {
            return false;
        }
        return true;
    }

    public static boolean isSmsGlancesEnabled() {
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        java.lang.String smsPackage = com.navdy.client.app.ui.settings.SettingsUtils.getSmsPackage();
        if (!sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false) || !sharedPrefs.getBoolean(smsPackage, true)) {
            return false;
        }
        return true;
    }

    public static boolean isThisGlanceEnabledAsWellAsGlobal(java.lang.String pkg) {
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        boolean defaultVal = getDefaultValueFor(pkg);
        logger.d("glances are globally turned " + (sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false) ? "on" : "off") + " and are " + (sharedPrefs.getBoolean(pkg, defaultVal) ? "on" : "off") + " for " + pkg + " default value for this package is " + defaultVal);
        if (!sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false) || !sharedPrefs.getBoolean(pkg, defaultVal)) {
            return false;
        }
        return true;
    }

    public static boolean isThisGlanceEnabledInItself(java.lang.String pkg) {
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        boolean defaultVal = getDefaultValueFor(pkg);
        logger.d("glances are " + (sharedPrefs.getBoolean(pkg, defaultVal) ? "on" : "off") + " for " + pkg + " default value for this package is " + defaultVal);
        return sharedPrefs.getBoolean(pkg, defaultVal);
    }

    public static void saveGlancesConfigurationChanges(java.lang.String key, boolean value) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(key, value).apply();
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.GLANCES_CONFIGURED);
    }

    public static com.navdy.service.library.events.preferences.NotificationPreferences buildGlancesPreferences(long serialNumber, boolean glancesAreEnabled, boolean readAloud, boolean showContent) {
        java.util.ArrayList<com.navdy.service.library.events.notification.NotificationSetting> settings = new java.util.ArrayList<>();
        settings.add(new com.navdy.service.library.events.notification.NotificationSetting.Builder().app(com.navdy.client.app.framework.glances.GlanceConstants.PHONE_PACKAGE).enabled(java.lang.Boolean.valueOf(isThisGlanceEnabledInItself(com.navdy.client.app.framework.glances.GlanceConstants.PHONE_PACKAGE))).build());
        settings.add(new com.navdy.service.library.events.notification.NotificationSetting.Builder().app(com.navdy.client.app.framework.glances.GlanceConstants.SMS_PACKAGE).enabled(java.lang.Boolean.valueOf(isThisGlanceEnabledInItself(com.navdy.client.app.framework.glances.GlanceConstants.SMS_PACKAGE))).build());
        settings.add(new com.navdy.service.library.events.notification.NotificationSetting.Builder().app(com.navdy.client.app.framework.glances.GlanceConstants.FUEL_PACKAGE).enabled(java.lang.Boolean.valueOf(isThisGlanceEnabledInItself(com.navdy.client.app.framework.glances.GlanceConstants.FUEL_PACKAGE))).build());
        settings.add(new com.navdy.service.library.events.notification.NotificationSetting.Builder().app(com.navdy.client.app.framework.glances.GlanceConstants.MUSIC_PACKAGE).enabled(java.lang.Boolean.valueOf(isThisGlanceEnabledInItself(com.navdy.client.app.framework.glances.GlanceConstants.MUSIC_PACKAGE))).build());
        settings.add(new com.navdy.service.library.events.notification.NotificationSetting.Builder().app(com.navdy.client.app.framework.glances.GlanceConstants.TRAFFIC_PACKAGE).enabled(java.lang.Boolean.valueOf(isThisGlanceEnabledInItself(com.navdy.client.app.framework.glances.GlanceConstants.TRAFFIC_PACKAGE))).build());
        return buildGlancesPreferences(serialNumber, glancesAreEnabled, readAloud, showContent, settings);
    }

    public static com.navdy.service.library.events.preferences.NotificationPreferences buildGlancesPreferences(long serialNumber, boolean glancesAreEnabled, boolean readAloud, boolean showContent, java.util.List<com.navdy.service.library.events.notification.NotificationSetting> settings) {
        com.navdy.service.library.events.preferences.NotificationPreferences.Builder notifPrefBldr = new com.navdy.service.library.events.preferences.NotificationPreferences.Builder();
        notifPrefBldr.serial_number = java.lang.Long.valueOf(serialNumber);
        notifPrefBldr.enabled = java.lang.Boolean.valueOf(glancesAreEnabled);
        notifPrefBldr.readAloud = java.lang.Boolean.valueOf(readAloud);
        notifPrefBldr.showContent = java.lang.Boolean.valueOf(showContent);
        notifPrefBldr.settings = settings;
        return notifPrefBldr.build();
    }

    public static boolean sendGlancesSettingsToTheHudBasedOnSharedPrefValue() {
        android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        return com.navdy.client.app.ui.settings.SettingsUtils.sendGlancesSettingsToTheHud(buildGlancesPreferences(sharedPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_SERIAL_NUM, 0), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_READ_ALOUD, true), sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_SHOW_CONTENT, false)));
    }

    public static boolean getDefaultValueFor(java.lang.String pkg) {
        if (isDrivingGlance(pkg)) {
            return true;
        }
        if (isWhiteListedApp(pkg)) {
        }
        return false;
    }

    public static void sendTestGlance(com.navdy.client.app.ui.base.BaseActivity activity) {
        sendTestGlance(activity, null);
    }

    public static void sendTestGlance(com.navdy.client.app.ui.base.BaseActivity activity, com.navdy.service.library.log.Logger logger2) {
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        com.navdy.client.app.framework.AppInstance appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        java.util.ArrayList<java.lang.String> glanceMessageArray = new java.util.ArrayList<>();
        java.lang.String[] glanceMessages = activity.getResources().getStringArray(com.navdy.client.R.array.glance_messages);
        java.lang.String title = context.getString(com.navdy.client.R.string.title_activity_main);
        java.lang.String firstMessage = context.getString(com.navdy.client.R.string.glance_test_message_one);
        glanceMessageArray.add(firstMessage);
        glanceMessageArray.addAll(java.util.Arrays.asList(glanceMessages));
        if (!appInstance.isDeviceConnected() || appInstance.getRemoteDevice() == null || glanceMessageArray.isEmpty()) {
            if (logger2 != null) {
                logger2.v("Device is disconnected. Cannot send test glance");
            }
            if (!activity.isFinishing()) {
                activity.showSimpleDialog(0, activity.getString(com.navdy.client.R.string.navdy_display_disconnected), activity.getString(com.navdy.client.R.string.glance_failure_explanation));
                return;
            }
            return;
        }
        if (logger2 != null) {
            logger2.v("Device is connected. Sending test glance");
        }
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), title));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), firstMessage));
        java.util.Iterator it = glanceMessageArray.iterator();
        while (it.hasNext()) {
            java.lang.String glanceMessage = (java.lang.String) it.next();
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_TITLE.name(), title));
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), glanceMessage));
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MAIN_ICON.name(), com.navdy.service.library.events.glances.GlanceIconConstants.GLANCE_ICON_NAVDY_MAIN.name()));
            data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_SIDE_ICON.name(), com.navdy.service.library.events.glances.GlanceIconConstants.GLANCE_ICON_MESSAGE_SIDE_BLUE.name()));
            appInstance.getRemoteDevice().postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).id(java.util.UUID.randomUUID().toString()).postTime(java.lang.Long.valueOf(java.lang.System.currentTimeMillis())).provider("Navdy").glanceData(data).build());
        }
    }
}
