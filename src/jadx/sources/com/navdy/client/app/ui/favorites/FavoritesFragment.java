package com.navdy.client.app.ui.favorites;

public class FavoritesFragment extends com.navdy.client.app.ui.base.BaseSupportFragment {
    public com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter favoritesAdapter;
    private com.navdy.client.app.framework.util.ImageCache imageCache = new com.navdy.client.app.framework.util.ImageCache();
    private final com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    public android.support.v7.widget.RecyclerView recyclerView;
    public android.widget.RelativeLayout splash;

    class Anon1 implements android.view.View.OnClickListener {

        /* renamed from: com.navdy.client.app.ui.favorites.FavoritesFragment$Anon1$Anon1 reason: collision with other inner class name */
        class C0057Anon1 implements java.lang.Runnable {
            final /* synthetic */ android.support.v4.app.FragmentActivity val$activity;
            final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
            final /* synthetic */ com.navdy.client.app.tracking.SetDestinationTracker val$setDestinationTracker;

            C0057Anon1(com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker, com.navdy.client.app.framework.models.Destination destination, android.support.v4.app.FragmentActivity fragmentActivity) {
                this.val$setDestinationTracker = setDestinationTracker;
                this.val$destination = destination;
                this.val$activity = fragmentActivity;
            }

            public void run() {
                this.val$setDestinationTracker.tagSetDestinationEvent(this.val$destination);
                com.navdy.client.app.ui.favorites.FavoritesFragment.this.navdyRouteHandler.requestNewRoute(this.val$destination);
                if (!com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected() && (this.val$activity instanceof com.navdy.client.app.ui.homescreen.HomescreenActivity)) {
                    this.val$activity.onBackPressed();
                }
            }
        }

        Anon1() {
        }

        public void onClick(android.view.View v) {
            int position = ((java.lang.Integer) v.getTag()).intValue();
            android.support.v4.app.FragmentActivity activity = com.navdy.client.app.ui.favorites.FavoritesFragment.this.getActivity();
            if (v.getId() != com.navdy.client.R.id.edit) {
                com.navdy.client.app.ui.favorites.FavoritesFragment.this.logger.v("position clicked: " + position);
                com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.NAVIGATE_USING_FAVORITES);
                if (com.navdy.client.app.ui.favorites.FavoritesFragment.this.favoritesAdapter == null) {
                    com.navdy.client.app.ui.favorites.FavoritesFragment.this.logger.e("Can't handle click at position " + position + " without a valid adapter");
                    return;
                }
                com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.ui.favorites.FavoritesFragment.this.favoritesAdapter.getItem(position);
                com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker = com.navdy.client.app.tracking.SetDestinationTracker.getInstance();
                setDestinationTracker.setSourceValue(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES.FAVORITES_LIST);
                setDestinationTracker.setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.FAVORITE);
                setDestinationTracker.setFavoriteType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.FAVORITE_TYPE_VALUES.getFavoriteTypeValue(destination));
                ((com.navdy.client.app.ui.base.BaseActivity) activity).showRequestNewRouteDialog(new com.navdy.client.app.ui.favorites.FavoritesFragment.Anon1.C0057Anon1(setDestinationTracker, destination, activity));
            } else if (com.navdy.client.app.ui.favorites.FavoritesFragment.this.favoritesAdapter != null) {
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.startFavoriteEditActivity(activity, com.navdy.client.app.ui.favorites.FavoritesFragment.this.favoritesAdapter.getItem(position));
            }
        }
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        android.view.View rootView = inflater.inflate(com.navdy.client.R.layout.hs_fragment_favorites, container, false);
        this.recyclerView = (android.support.v7.widget.RecyclerView) rootView.findViewById(com.navdy.client.R.id.favorites_fragment_recycler_view);
        android.content.Context context = getContext();
        android.support.v7.widget.LinearLayoutManager layoutManager = new android.support.v7.widget.LinearLayoutManager(context);
        layoutManager.setOrientation(1);
        this.recyclerView.setLayoutManager(layoutManager);
        this.favoritesAdapter = new com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter(context);
        this.recyclerView.setAdapter(this.favoritesAdapter);
        this.favoritesAdapter.setClickListener(new com.navdy.client.app.ui.favorites.FavoritesFragment.Anon1());
        new android.support.v7.widget.helper.ItemTouchHelper(new com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.ItemTouchHelperCallback(this.favoritesAdapter)).attachToRecyclerView(this.recyclerView);
        this.splash = (android.widget.RelativeLayout) rootView.findViewById(com.navdy.client.R.id.splash);
        android.widget.ImageView illustration = (android.widget.ImageView) rootView.findViewById(com.navdy.client.R.id.illustration);
        if (illustration != null) {
            com.navdy.client.app.framework.util.ImageUtils.loadImage(illustration, com.navdy.client.R.drawable.image_favorites, this.imageCache);
        }
        return rootView;
    }

    public void onResume() {
        super.onResume();
        showOrHideSplashScreen();
        if (this.favoritesAdapter != null) {
            this.favoritesAdapter.setupDynamicShortcuts();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.favoritesAdapter != null) {
            this.favoritesAdapter.close();
        }
        this.imageCache.clearCache();
    }

    public void showOrHideSplashScreen() {
        if (!isEnding((android.support.v4.app.Fragment) this)) {
            int itemCount = 0;
            if (this.favoritesAdapter != null) {
                itemCount = this.favoritesAdapter.getItemCount();
            }
            if (this.recyclerView != null && this.splash != null) {
                if (itemCount > 0) {
                    this.recyclerView.setVisibility(View.VISIBLE);
                    this.splash.setVisibility(View.GONE);
                    return;
                }
                this.recyclerView.setVisibility(View.GONE);
                this.splash.setVisibility(View.VISIBLE);
            }
        }
    }
}
