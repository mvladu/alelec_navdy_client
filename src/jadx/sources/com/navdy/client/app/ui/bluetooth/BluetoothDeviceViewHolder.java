package com.navdy.client.app.ui.bluetooth;

class BluetoothDeviceViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    android.widget.TextView deviceMainTitle;
    protected android.view.View row;

    BluetoothDeviceViewHolder(android.view.View itemView) {
        super(itemView);
        this.row = itemView.findViewById(com.navdy.client.R.id.bluetooth_device_list_row);
        this.deviceMainTitle = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.device_main_title);
    }
}
