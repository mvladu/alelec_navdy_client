package com.navdy.client.app.ui.homescreen;

class SectionHeaderViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
    android.widget.ImageView chevron;
    android.widget.TextView title;

    SectionHeaderViewHolder(android.view.View itemView) {
        super(itemView);
        this.title = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.title);
        this.chevron = (android.widget.ImageView) itemView.findViewById(com.navdy.client.R.id.chevron);
    }
}
