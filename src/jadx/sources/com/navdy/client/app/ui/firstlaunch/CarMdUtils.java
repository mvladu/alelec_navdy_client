package com.navdy.client.app.ui.firstlaunch;

class CarMdUtils {
    private static final long CACHE_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(5);
    private static final java.lang.String CRIME_PHOTO_BUCKET = "navdy-prod-release";
    private static final java.lang.String CRIME_PHOTO_KEY_BASE = "obd-locations/";
    private static final java.lang.String FILE_EXTRENSION = ".jpg";
    private static final int NB_CELLS_PER_LINE = 6;
    private static volatile java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>>> cachedYearMap;
    private static android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.firstlaunch.CarMdUtils.class);

    static class Anon1 implements com.amazonaws.mobileconnectors.s3.transferutility.TransferListener {
        final /* synthetic */ java.lang.Runnable val$callback;
        final /* synthetic */ java.lang.String val$key;

        Anon1(java.lang.Runnable runnable, java.lang.String str) {
            this.val$callback = runnable;
            this.val$key = str;
        }

        public void onStateChanged(int id, com.amazonaws.mobileconnectors.s3.transferutility.TransferState state) {
            com.navdy.client.app.ui.firstlaunch.CarMdUtils.logger.d("Trying to download OBD image. State: " + state.name());
            if (this.val$callback == null) {
                return;
            }
            if (state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.COMPLETED || state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.FAILED || state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.PAUSED || state == com.amazonaws.mobileconnectors.s3.transferutility.TransferState.WAITING_FOR_NETWORK) {
                this.val$callback.run();
            }
        }

        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        }

        public void onError(int id, java.lang.Exception ex) {
            com.navdy.client.app.ui.firstlaunch.CarMdUtils.logger.e("Error while trying to download OBD image: " + this.val$key, ex);
        }
    }

    static class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.ui.firstlaunch.CarMdUtils.clearCache();
            java.lang.System.gc();
        }
    }

    static class ObdLocation {
        java.lang.String accessNote = "";
        public int location = com.navdy.client.app.ui.firstlaunch.CarInfoActivity.DEFAULT_OBD_LOCATION;
        public java.lang.String note = "";

        ObdLocation(int location2, java.lang.String accessNote2, java.lang.String note2) {
            this.location = location2;
            this.accessNote = accessNote2;
            this.note = note2;
        }
    }

    CarMdUtils() {
    }

    private static java.lang.String getCrimePhotoKey(java.lang.String year, java.lang.String make, java.lang.String model) {
        return CRIME_PHOTO_KEY_BASE + getVehicleName(year, make, model) + FILE_EXTRENSION;
    }

    private static java.lang.String getVehicleName(java.lang.String year, java.lang.String make, java.lang.String model) {
        return (make.trim() + "-" + model.trim() + "-" + year.trim()).replace("\\s", "-");
    }

    static void downloadObdCrimePhoto(java.lang.String year, java.lang.String make, java.lang.String model, java.lang.Runnable callback) {
        java.lang.String key = getCrimePhotoKey(year, make, model);
        new com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility(com.navdy.client.ota.impl.OTAUpdateManagerImpl.createS3Client(), com.navdy.client.app.NavdyApplication.getAppContext()).download("navdy-prod-release", key, new java.io.File(com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir() + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.OBD_IMAGE_FILE_NAME)).setTransferListener(new com.navdy.client.app.ui.firstlaunch.CarMdUtils.Anon1(callback, key));
    }

    @android.support.annotation.WorkerThread
    static synchronized java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>>> buildCarList() {
        java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>>> yearMap;
        int location;
        synchronized (com.navdy.client.app.ui.firstlaunch.CarMdUtils.class) {
            com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
            if (cachedYearMap != null) {
                resetClearingTimer();
                yearMap = cachedYearMap;
            } else {
                java.io.InputStream inputStream = null;
                java.util.Scanner sc = null;
                yearMap = new java.util.HashMap<>();
                try {
                    inputStream = com.navdy.client.app.NavdyApplication.getAppContext().getResources().openRawResource(com.navdy.client.R.raw.vehicles);
                    java.util.Scanner scanner = new java.util.Scanner(inputStream, "UTF-8");
                    try {
                        if (scanner.hasNextLine()) {
                            scanner.nextLine();
                        }
                        while (scanner.hasNextLine()) {
                            java.lang.String line = scanner.nextLine();
                            if (line.contains("\"\"\"Driver Side - Left of Steering Wheel, above Hood Release\"\"\"")) {
                                line = line.replace("\"\"\"Driver Side - Left of Steering Wheel, above Hood Release\"\"\"", "Driver Side - Left of Steering Wheel above Hood Release");
                            }
                            java.lang.String[] cells = line.split(",", -1);
                            if (cells.length < 6) {
                                java.lang.RuntimeException runtimeException = new java.lang.RuntimeException("Unable to parse the obd car data! Not enough cells in this line:" + line);
                                throw runtimeException;
                            }
                            java.lang.String make = cells[0];
                            java.lang.String model = cells[1];
                            java.lang.String year = cells[2];
                            java.lang.String position = cells[3];
                            java.lang.String accessNote = cells[4];
                            java.lang.String note = cells[5];
                            location = java.lang.Integer.parseInt(position) - 1;
                            com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation obdLocation = new com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation(location, accessNote, note);
                            java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation> modelMap = new java.util.HashMap<>();
                            java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>> makeMap = (java.util.HashMap) yearMap.get(year);
                            if (makeMap == null) {
                                makeMap = new java.util.HashMap<>();
                            } else {
                                modelMap = (java.util.HashMap) makeMap.get(make);
                                if (modelMap == null) {
                                    modelMap = new java.util.HashMap<>();
                                } else if (((com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation) modelMap.get(model)) != null) {
                                    java.lang.RuntimeException runtimeException2 = new java.lang.RuntimeException("Duplicates in the list of obd locations for: year[" + year + "]" + " make[" + make + "]" + " model[" + model + "]" + " location[" + location + "]" + " accessNote[" + accessNote + "]" + " note[" + note + "]");
                                    throw runtimeException2;
                                }
                            }
                            modelMap.put(model, obdLocation);
                            makeMap.put(make, modelMap);
                            yearMap.put(year, makeMap);
                        }
                        java.io.IOException ioException = scanner.ioException();
                        if (ioException != null) {
                            java.lang.RuntimeException runtimeException3 = new java.lang.RuntimeException("Unable to parse the obd car data!", ioException);
                            throw runtimeException3;
                        }
                        com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                        com.navdy.service.library.util.IOUtils.closeStream(scanner);
                        cachedYearMap = yearMap;
                        resetClearingTimer();
                    } catch (java.lang.Exception e) {
                        location = com.navdy.client.app.ui.firstlaunch.CarInfoActivity.DEFAULT_OBD_LOCATION;
                    } catch (Throwable th) {
                        th = th;
                        sc = scanner;
                        com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                        com.navdy.service.library.util.IOUtils.closeStream(sc);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(sc);
                    throw th;
                }
            }
        }
        return yearMap;
    }

    static java.lang.String getLocalizedAccessNote(java.lang.String original) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(original)) {
            return original;
        }
        android.content.res.Resources res = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
        char c = 65535;
        switch (original.hashCode()) {
            case -1624682019:
                if (original.equals("uncovered")) {
                    c = 1;
                    break;
                }
                break;
            case 958484118:
                if (original.equals("covered")) {
                    c = 0;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return res.getString(com.navdy.client.R.string.covered);
            case 1:
                return res.getString(com.navdy.client.R.string.uncovered);
            default:
                return original;
        }
    }

    static java.lang.String getLocalizedNote(java.lang.String original) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(original)) {
            return original;
        }
        android.content.res.Resources res = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
        char c = 65535;
        switch (original.hashCode()) {
            case -1892693360:
                if (original.equals("Center Console  -  Below Radio & A/C Controls")) {
                    c = 3;
                    break;
                }
                break;
            case -1829801416:
                if (original.equals("Center Console - Above Climate Control")) {
                    c = 4;
                    break;
                }
                break;
            case -1457707482:
                if (original.equals("Driver Side - Left of Steering Wheel above Hood Release")) {
                    c = 0;
                    break;
                }
                break;
            case -1246714626:
                if (original.equals("Center Compartment - Next to Hand Brake")) {
                    c = 1;
                    break;
                }
                break;
            case -1122499876:
                if (original.equals("Center Compartment - Under Armrest")) {
                    c = 2;
                    break;
                }
                break;
            case -862795881:
                if (original.equals("Driver Side - Kick Panel Behind Fuse Box Cover")) {
                    c = 10;
                    break;
                }
                break;
            case -725524244:
                if (original.equals("Center Console - Right Side Of Console")) {
                    c = 8;
                    break;
                }
                break;
            case -330998842:
                if (original.equals("Center Console - Behind Coin Tray")) {
                    c = 6;
                    break;
                }
                break;
            case 338861089:
                if (original.equals("Driver Side - Left Side of Center Console")) {
                    c = 11;
                    break;
                }
                break;
            case 529269830:
                if (original.equals("Driver Side - Under Lower Left Side of Dashboard")) {
                    c = 13;
                    break;
                }
                break;
            case 754693895:
                if (original.equals("Rear Center Console -  Left of Ashtray")) {
                    c = 17;
                    break;
                }
                break;
            case 1113299006:
                if (original.equals("Driver Side - Under Steering Wheel Column")) {
                    c = 15;
                    break;
                }
                break;
            case 1269865364:
                if (original.equals("Driver Side - Right Side of Steering Wheel")) {
                    c = 12;
                    break;
                }
                break;
            case 1737347906:
                if (original.equals("Center Console - Behind Fuse Box Cover")) {
                    c = 7;
                    break;
                }
                break;
            case 1780203301:
                if (original.equals("Driver Side - Under Lower Right Side of Dashboard")) {
                    c = 14;
                    break;
                }
                break;
            case 1892158640:
                if (original.equals("Center Console - Right Side of Radio")) {
                    c = 9;
                    break;
                }
                break;
            case 2067239503:
                if (original.equals("Passenger Side - Under Lower Left Side of Glove Compartment")) {
                    c = 16;
                    break;
                }
                break;
            case 2110562093:
                if (original.equals("Center Console - Behind Ashtray")) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                return res.getString(com.navdy.client.R.string.driver_side__left_of_steering_wheel_above_hood_release);
            case 1:
                return res.getString(com.navdy.client.R.string.center_compartment__next_to_hand_brake);
            case 2:
                return res.getString(com.navdy.client.R.string.center_compartment__under_armrest);
            case 3:
                return res.getString(com.navdy.client.R.string.center_console____below_radio__ac_controls);
            case 4:
                return res.getString(com.navdy.client.R.string.center_console__above_climate_control);
            case 5:
                return res.getString(com.navdy.client.R.string.center_console__behind_ashtray);
            case 6:
                return res.getString(com.navdy.client.R.string.center_console__behind_coin_tray);
            case 7:
                return res.getString(com.navdy.client.R.string.center_console__behind_fuse_box_cover);
            case 8:
                return res.getString(com.navdy.client.R.string.center_console__right_side_of_console);
            case 9:
                return res.getString(com.navdy.client.R.string.center_console__right_side_of_radio);
            case 10:
                return res.getString(com.navdy.client.R.string.driver_side__kick_panel_behind_fuse_box_cover);
            case 11:
                return res.getString(com.navdy.client.R.string.driver_side__left_side_of_center_console);
            case 12:
                return res.getString(com.navdy.client.R.string.driver_side__right_side_of_steering_wheel);
            case 13:
                return res.getString(com.navdy.client.R.string.driver_side__under_lower_left_side_of_dashboard);
            case 14:
                return res.getString(com.navdy.client.R.string.driver_side__under_lower_right_side_of_dashboard);
            case 15:
                return res.getString(com.navdy.client.R.string.driver_side__under_steering_wheel_column);
            case 16:
                return res.getString(com.navdy.client.R.string.passenger_side__under_lower_left_side_of_glove_compartment);
            case 17:
                return res.getString(com.navdy.client.R.string.rear_center_console___left_of_ashtray);
            default:
                throw new java.lang.RuntimeException("Unknown location: " + original);
        }
    }

    private static synchronized void resetClearingTimer() {
        synchronized (com.navdy.client.app.ui.firstlaunch.CarMdUtils.class) {
            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(new com.navdy.client.app.ui.firstlaunch.CarMdUtils.Anon2(), CACHE_TIMEOUT);
        }
    }

    private static synchronized void clearCache() {
        synchronized (com.navdy.client.app.ui.firstlaunch.CarMdUtils.class) {
            cachedYearMap = null;
        }
    }

    static synchronized boolean hasCachedCarList() {
        boolean z;
        synchronized (com.navdy.client.app.ui.firstlaunch.CarMdUtils.class) {
            z = cachedYearMap != null;
        }
        return z;
    }

    @android.support.annotation.Nullable
    static com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation getObdLocation(@android.support.annotation.Nullable java.lang.String year, @android.support.annotation.Nullable java.lang.String make, @android.support.annotation.Nullable java.lang.String model, @android.support.annotation.Nullable java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>>> yearMap) {
        if (make == null || year == null || model == null || yearMap == null || yearMap.size() == 0) {
            return null;
        }
        java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation>> makeMap = (java.util.HashMap) yearMap.get(year);
        if (makeMap == null) {
            return null;
        }
        java.util.HashMap<java.lang.String, com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation> modelMap = (java.util.HashMap) makeMap.get(make);
        if (modelMap != null) {
            return (com.navdy.client.app.ui.firstlaunch.CarMdUtils.ObdLocation) modelMap.get(model);
        }
        return null;
    }
}
