package com.navdy.client.app.ui.bluetooth;

class BluetoothDialogRecyclerAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.navdy.client.app.ui.bluetooth.BluetoothDeviceViewHolder> {
    private java.util.List<java.lang.String> bluetoothDevices = new java.util.ArrayList();
    private android.view.View.OnClickListener listener;

    BluetoothDialogRecyclerAdapter() {
    }

    void setClickListener(android.view.View.OnClickListener customItemClickListener) {
        this.listener = customItemClickListener;
    }

    public int getItemCount() {
        return this.bluetoothDevices.size();
    }

    public com.navdy.client.app.ui.bluetooth.BluetoothDeviceViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        return new com.navdy.client.app.ui.bluetooth.BluetoothDeviceViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.fl_bluetooth_recyclerview_item, parent, false));
    }

    public void onBindViewHolder(com.navdy.client.app.ui.bluetooth.BluetoothDeviceViewHolder bluetoothDeviceViewHolder, int position) {
        if (!this.bluetoothDevices.isEmpty()) {
            bluetoothDeviceViewHolder.deviceMainTitle.setText((java.lang.String) this.bluetoothDevices.get(position));
            bluetoothDeviceViewHolder.row.setOnClickListener(this.listener);
        }
    }

    void addDevice(com.navdy.client.debug.devicepicker.Device newDevice) {
        this.bluetoothDevices.add(newDevice.getPrettyName());
        notifyItemInserted(this.bluetoothDevices.size() - 1);
    }

    public void clear() {
        this.bluetoothDevices.clear();
        notifyDataSetChanged();
    }

    public boolean contains(java.lang.String name) {
        return this.bluetoothDevices.contains(name);
    }
}
