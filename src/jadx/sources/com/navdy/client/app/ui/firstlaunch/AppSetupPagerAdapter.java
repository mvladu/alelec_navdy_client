package com.navdy.client.app.ui.firstlaunch;

public class AppSetupPagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter {
    static final int BT_SCREEN_INDEX = 1;
    private static final int CACHE_SIZE = 20971520;
    private static final int FADE_DURATION = 200;
    private static final int IMAGE_MAX_SIZE = 4194304;
    public static final int PHONE_SCREEN_INDEX = 8;
    private static final int PROFILE_SCREEN_INDEX = 0;
    private static final com.navdy.client.app.ui.firstlaunch.AppSetupScreen[] screens = {new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_profile_empty, com.navdy.client.R.string.fle_app_setup_profile_title, com.navdy.client.R.string.fle_app_setup_profile_desc, com.navdy.client.R.string.fle_app_setup_profile_button, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.PROFILE, true), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_bluetooth, com.navdy.client.R.drawable.image_navdy_bluetooth_error, com.navdy.client.R.string.fle_app_setup_pair_with_display_title, com.navdy.client.R.string.fle_app_setup_pair_with_display_desc, com.navdy.client.R.string.contact_support, com.navdy.client.R.string.fle_app_setup_pair_with_display_button, com.navdy.client.R.string.fle_app_setup_pair_with_display_title_fail, com.navdy.client.R.string.fle_app_setup_pair_with_display_desc_fail, com.navdy.client.R.string.fle_app_setup_pair_with_display_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH, true), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_connected, com.navdy.client.R.string.fle_app_setup_pair_with_display_title_success, com.navdy.client.R.string.fle_app_setup_pair_with_display_desc_success, com.navdy.client.R.string.fle_app_setup_pair_with_display_button_success, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH_SUCCESS, true), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_glances, com.navdy.client.R.drawable.image_navdy_phone_notification_access, com.navdy.client.R.string.fle_app_setup_glances_title, com.navdy.client.R.string.fle_app_setup_glances_desc, 0, com.navdy.client.R.string.fle_app_setup_glances_button, com.navdy.client.R.string.fle_app_setup_glances_title_fail, com.navdy.client.R.string.fle_app_setup_glances_desc_fail, com.navdy.client.R.string.fle_app_setup_glances_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.NOTIFICATIONS, true), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_location, com.navdy.client.R.drawable.image_navdy_phone_settings, com.navdy.client.R.string.fle_app_setup_location_title, com.navdy.client.R.string.fle_app_setup_location_desc, com.navdy.client.R.string.fle_app_setup_location_button, com.navdy.client.R.string.fle_app_setup_location_title_fail, com.navdy.client.R.string.fle_app_setup_location_desc_fail, com.navdy.client.R.string.fle_app_setup_location_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.ACCESS_FINE_LOCATION, true), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_microphone, com.navdy.client.R.drawable.image_navdy_phone_settings, com.navdy.client.R.string.fle_app_setup_microphone_title, com.navdy.client.R.string.fle_app_setup_microphone_desc, 0, com.navdy.client.R.string.fle_app_setup_microphone_button, com.navdy.client.R.string.fle_app_setup_microphone_title_fail, com.navdy.client.R.string.fle_app_setup_microphone_desc_fail, com.navdy.client.R.string.fle_app_setup_microphone_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.USE_MICROPHONE, false), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_contacts, com.navdy.client.R.drawable.image_navdy_phone_settings, com.navdy.client.R.string.fle_app_setup_contacts_title, com.navdy.client.R.string.fle_app_setup_contacts_desc, 0, com.navdy.client.R.string.fle_app_setup_contacts_button, com.navdy.client.R.string.fle_app_setup_contacts_title_fail, com.navdy.client.R.string.fle_app_setup_contacts_desc_fail, com.navdy.client.R.string.fle_app_setup_contacts_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.READ_CONTACTS, false), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_text, com.navdy.client.R.drawable.image_navdy_phone_settings, com.navdy.client.R.string.fle_app_setup_messaging_title, com.navdy.client.R.string.fle_app_setup_messaging_desc, 0, com.navdy.client.R.string.fle_app_setup_messaging_button, com.navdy.client.R.string.fle_app_setup_messaging_title_fail, com.navdy.client.R.string.fle_app_setup_messaging_desc_fail, com.navdy.client.R.string.fle_app_setup_messaging_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.RECEIVE_SMS, false), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_phone, com.navdy.client.R.drawable.image_navdy_phone_settings, com.navdy.client.R.string.fle_app_setup_make_calls_title, com.navdy.client.R.string.fle_app_setup_make_calls_desc, 0, com.navdy.client.R.string.fle_app_setup_make_calls_button, com.navdy.client.R.string.fle_app_setup_make_calls_title_fail, com.navdy.client.R.string.fle_app_setup_make_calls_desc_fail, com.navdy.client.R.string.fle_app_setup_make_calls_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.CALL_PHONE, false), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_calendar, com.navdy.client.R.drawable.image_navdy_phone_settings, com.navdy.client.R.string.fle_app_setup_calendar_title, com.navdy.client.R.string.fle_app_setup_calendar_desc, 0, com.navdy.client.R.string.fle_app_setup_calendar_button, com.navdy.client.R.string.fle_app_setup_calendar_title_fail, com.navdy.client.R.string.fle_app_setup_calendar_desc_fail, com.navdy.client.R.string.fle_app_setup_calendar_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.READ_CALENDAR, false), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_software, com.navdy.client.R.drawable.image_navdy_phone_settings, com.navdy.client.R.string.fle_app_setup_storage_title, com.navdy.client.R.string.fle_app_setup_storage_desc, com.navdy.client.R.string.fle_app_setup_storage_button, com.navdy.client.R.string.fle_app_setup_storage_title_fail, com.navdy.client.R.string.fle_app_setup_storage_desc_fail, com.navdy.client.R.string.fle_app_setup_storage_button_fail, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.WRITE_EXTERNAL_STORAGE, true), new com.navdy.client.app.ui.firstlaunch.AppSetupScreen(com.navdy.client.R.drawable.image_navdy_finished, com.navdy.client.R.string.fle_app_setup_end_title, com.navdy.client.R.string.fle_app_setup_end_desc, com.navdy.client.R.string.fle_app_setup_end_button, com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.END, false)};
    private android.util.SparseArray<android.support.v4.app.Fragment> cache = new android.util.SparseArray<>(screens.length);
    private com.navdy.client.app.framework.util.ImageCache imageCache = new com.navdy.client.app.framework.util.ImageCache(CACHE_SIZE, 4194304);
    private final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.class);
    private boolean pretendBtConnected = false;
    private int screenCount;

    class Anon1 implements android.view.animation.Animation.AnimationListener {
        final /* synthetic */ android.view.animation.Animation val$fadeIn;
        final /* synthetic */ android.widget.ImageView val$hud;
        final /* synthetic */ int val$hudRes;

        Anon1(android.widget.ImageView imageView, int i, android.view.animation.Animation animation) {
            this.val$hud = imageView;
            this.val$hudRes = i;
            this.val$fadeIn = animation;
        }

        public void onAnimationStart(android.view.animation.Animation animation) {
        }

        public void onAnimationEnd(android.view.animation.Animation animation) {
            com.navdy.client.app.framework.util.ImageUtils.loadImage(this.val$hud, this.val$hudRes, com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.this.imageCache);
            this.val$hud.startAnimation(this.val$fadeIn);
        }

        public void onAnimationRepeat(android.view.animation.Animation animation) {
        }
    }

    AppSetupPagerAdapter(android.support.v4.app.FragmentManager fm, com.navdy.client.app.framework.util.ImageCache imageCache2) {
        super(fm);
        recalculateScreenCount();
        this.imageCache = imageCache2;
    }

    public android.support.v4.app.Fragment getItem(int position) {
        android.support.v4.app.Fragment screen = (android.support.v4.app.Fragment) this.cache.get(position);
        if (screen != null) {
            return screen;
        }
        if (position == 0) {
            com.navdy.client.app.ui.firstlaunch.AppSetupProfileFragment fragment = new com.navdy.client.app.ui.firstlaunch.AppSetupProfileFragment();
            this.cache.put(position, fragment);
            return fragment;
        }
        com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment fragment2 = new com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment();
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putParcelable("screen", screens[position]);
        fragment2.setArguments(bundle);
        this.cache.put(position, fragment2);
        return fragment2;
    }

    public void destroyItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        super.destroyItem(container, position, object);
        this.cache.remove(position);
    }

    public void destroyItem(android.view.View container, int position, java.lang.Object object) {
        super.destroyItem(container, position, object);
        this.cache.remove(position);
    }

    void clearCache() {
        this.cache.clear();
        this.imageCache.clearCache();
    }

    public int getCount() {
        return this.screenCount;
    }

    public void recalculateScreenCount() {
        com.navdy.client.app.ui.firstlaunch.AppSetupScreen[] appSetupScreenArr;
        boolean deviceIsConnected;
        int count = 1;
        for (com.navdy.client.app.ui.firstlaunch.AppSetupScreen screen : screens) {
            if (screen.screenType != com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
                if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.END || !weHavePermissionForThisScreen(screen)) {
                    break;
                }
            } else {
                com.navdy.client.app.framework.AppInstance appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
                if (this.pretendBtConnected || appInstance.isDeviceConnected()) {
                    deviceIsConnected = true;
                } else {
                    deviceIsConnected = false;
                }
                if (!deviceIsConnected) {
                    break;
                }
            }
            count++;
        }
        this.screenCount = count;
        notifyDataSetChanged();
    }

    void updateIllustration(java.lang.ref.WeakReference<android.widget.ImageView> hudRef, int position, boolean showingFail) {
        int hudRes;
        if (hudRef == null) {
            this.logger.e("Trying to call updateIllustration with null layout elements");
            return;
        }
        android.widget.ImageView hud = (android.widget.ImageView) hudRef.get();
        if (hud != null) {
            com.navdy.client.app.ui.firstlaunch.AppSetupScreen screen = screens[position];
            if (screen != null) {
                if (showingFail) {
                    hudRes = screen.hudResFail;
                } else {
                    hudRes = screen.hudRes;
                }
                android.view.animation.Animation fadeOut = new android.view.animation.AlphaAnimation(1.0f, 0.0f);
                fadeOut.setInterpolator(new android.view.animation.AccelerateInterpolator());
                fadeOut.setDuration(200);
                android.view.animation.Animation fadeIn = new android.view.animation.AlphaAnimation(0.0f, 1.0f);
                fadeIn.setInterpolator(new android.view.animation.DecelerateInterpolator());
                fadeIn.setDuration(200);
                fadeOut.setAnimationListener(new com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.Anon1(hud, hudRes, fadeIn));
                hud.clearAnimation();
                hud.setAlpha(1.0f);
                hud.startAnimation(fadeOut);
            }
        }
    }

    public static int getScreensCount() {
        return screens.length;
    }

    public static com.navdy.client.app.ui.firstlaunch.AppSetupScreen getScreen(int position) {
        if (position < 0 || position >= screens.length) {
            return null;
        }
        return screens[position];
    }

    public static boolean weHavePermissionForThisScreen(com.navdy.client.app.ui.firstlaunch.AppSetupScreen screen) {
        if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.PROFILE) {
            return com.navdy.client.app.tracking.Tracker.isUserRegistered();
        }
        if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.NOTIFICATIONS) {
            return com.navdy.client.app.ui.base.BaseActivity.weHaveNotificationPermission();
        }
        if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.ACCESS_FINE_LOCATION) {
            if (com.navdy.client.app.ui.base.BaseActivity.weHaveLocationPermission() || (!screen.isMandatory && com.navdy.client.app.ui.base.BaseActivity.alreadyAskedForLocationPermission())) {
                return true;
            }
            return false;
        } else if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.USE_MICROPHONE) {
            if (com.navdy.client.app.ui.base.BaseActivity.weHaveMicrophonePermission() || (!screen.isMandatory && com.navdy.client.app.ui.base.BaseActivity.alreadyAskedForMicrophonePermission())) {
                return true;
            }
            return false;
        } else if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.READ_CONTACTS) {
            if (com.navdy.client.app.ui.base.BaseActivity.weHaveContactsPermission() || (!screen.isMandatory && com.navdy.client.app.ui.base.BaseActivity.alreadyAskedForContactsPermission())) {
                return true;
            }
            return false;
        } else if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.RECEIVE_SMS) {
            if (com.navdy.client.app.ui.base.BaseActivity.weHaveSmsPermission() || (!screen.isMandatory && com.navdy.client.app.ui.base.BaseActivity.alreadyAskedForSmsPermission())) {
                return true;
            }
            return false;
        } else if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.CALL_PHONE) {
            if (com.navdy.client.app.ui.base.BaseActivity.weHavePhonePermission() || (!screen.isMandatory && com.navdy.client.app.ui.base.BaseActivity.alreadyAskedForPhonePermission())) {
                return true;
            }
            return false;
        } else if (screen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.READ_CALENDAR) {
            if (com.navdy.client.app.ui.base.BaseActivity.weHaveCalendarPermission() || (!screen.isMandatory && com.navdy.client.app.ui.base.BaseActivity.alreadyAskedForCalendarPermission())) {
                return true;
            }
            return false;
        } else if (screen.screenType != com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.WRITE_EXTERNAL_STORAGE) {
            return true;
        } else {
            if (com.navdy.client.app.ui.base.BaseActivity.weHaveStoragePermission() || (!screen.isMandatory && com.navdy.client.app.ui.base.BaseActivity.alreadyAskedForStoragePermission())) {
                return true;
            }
            return false;
        }
    }

    void pretendBtConnected() {
        this.pretendBtConnected = true;
    }
}
