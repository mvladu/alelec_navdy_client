package com.navdy.client.app.ui.base;

public class BaseGoogleMapFragment extends com.google.android.gms.maps.MapFragment {
    private static final boolean AUTO_CENTER_DEFAULT = false;
    private static final boolean CAR_LOCATION_ENABLED_DEFAULT = true;
    private static final boolean CENTER_MAP_ON_LAST_KNOWN_LOCATION_ENABLED_DEFAULT = true;
    private static final float CENTER_MAP_ZOOM = 16.0f;
    public static final float KEEP_ZOOM = -1.0f;
    private static final boolean MAP_TOOLBAR_ENABLED_DEFAULT = false;
    private static final double MIN_DISTANCE_CENTER_MAP_BOTH = 50.0d;
    private static final boolean MY_LOCATION_BUTTON_ENABLED_DEFAULT = false;
    private static final boolean MY_LOCATION_ENABLED_DEFAULT = true;
    private static final boolean SCROLL_GESTURES_ENABLED_DEFAULT = true;
    private static final boolean TRAFFIC_ENABLED_DEFAULT = true;
    public static final float ZOOM_DEFAULT = 14.0f;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.base.BaseGoogleMapFragment.class);
    private final java.util.Queue<com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentAttached> attachCompleteListeners = new java.util.LinkedList();
    private boolean autoCenter;
    private final com.navdy.client.app.framework.util.BusProvider bus = com.navdy.client.app.framework.util.BusProvider.getInstance();
    private boolean carLocationEnabled;
    private com.google.android.gms.maps.model.Marker carMarker;
    private boolean centerMapOnLastKnownLocation;
    java.util.concurrent.atomic.AtomicBoolean isAttached = new java.util.concurrent.atomic.AtomicBoolean(false);
    private int mapPaddingBottom;
    private int mapPaddingLeft;
    private int mapPaddingRight;
    private int mapPaddingTop;
    private boolean mapToolbarEnabled;
    private boolean myLocationButtonEnabled;
    private boolean myLocationEnabled;
    private final com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener navdyLocationListener = new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon1();
    private final com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    private final java.util.Queue<com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentReady> readyCompleteListeners = new java.util.LinkedList();
    private boolean scrollGesturesEnabled;
    private boolean trafficEnabled;

    class Anon1 implements com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener {

        /* renamed from: com.navdy.client.app.ui.base.BaseGoogleMapFragment$Anon1$Anon1 reason: collision with other inner class name */
        class C0053Anon1 implements com.google.android.gms.maps.OnMapReadyCallback {
            C0053Anon1() {
            }

            public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
                try {
                    if (com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.autoCenter) {
                        com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.centerOnLastKnownLocation(true);
                    }
                } catch (java.lang.Exception e) {
                    com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.e((java.lang.Throwable) e);
                }
            }
        }

        Anon1() {
        }

        public void onPhoneLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate phoneLocation) {
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon1.C0053Anon1());
        }

        public void onCarLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate carLocation) {
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.updateCarMarker(carLocation);
        }
    }

    class Anon10 implements com.google.android.gms.maps.OnMapReadyCallback {
        final /* synthetic */ com.navdy.service.library.events.location.Coordinate val$carLocation;

        Anon10(com.navdy.service.library.events.location.Coordinate coordinate) {
            this.val$carLocation = coordinate;
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            com.google.android.gms.maps.model.LatLng carLatLng = new com.google.android.gms.maps.model.LatLng(this.val$carLocation.latitude.doubleValue(), this.val$carLocation.longitude.doubleValue());
            if (com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.carMarker != null) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.carMarker.remove();
            }
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.carMarker = googleMap.addMarker(new com.google.android.gms.maps.model.MarkerOptions().position(carLatLng).title(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.car_location)).icon(com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource(com.navdy.client.R.drawable.icon_pin_car)));
            if (com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.autoCenter) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.centerOnLastKnownLocation(true);
            }
        }
    }

    class Anon2 implements com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentReady {
        final /* synthetic */ com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnShowMapListener val$onShowMapListener;

        class Anon1 implements com.google.android.gms.maps.OnMapReadyCallback {
            Anon1() {
            }

            public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
                if (com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.centerMapOnLastKnownLocation) {
                    com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.centerOnLastKnownLocation(false);
                    com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.fadeIn();
                    if (com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon2.this.val$onShowMapListener != null) {
                        com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon2.this.val$onShowMapListener.onShow();
                        return;
                    }
                    return;
                }
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.v("do not centerOnLastKnownLocation");
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.fadeIn();
                if (com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon2.this.val$onShowMapListener != null) {
                    com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon2.this.val$onShowMapListener.onShow();
                }
            }
        }

        Anon2(com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnShowMapListener onShowMapListener) {
            this.val$onShowMapListener = onShowMapListener;
        }

        public void onReady() {
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon2.Anon1());
        }
    }

    class Anon3 implements com.google.android.gms.maps.OnMapReadyCallback {
        final /* synthetic */ boolean val$animate;
        final /* synthetic */ com.google.android.gms.maps.model.LatLngBounds val$latLngBounds;

        class Anon1 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
            final /* synthetic */ android.view.View val$container;

            Anon1(android.view.View view) {
                this.val$container = view;
            }

            public void onGlobalLayout() {
                int height = this.val$container.getHeight();
                int width = this.val$container.getWidth();
                if (height == 0 || width == 0) {
                    com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.e("zoomTo, googleMap has no dimensions");
                } else {
                    int padding = 0;
                    int additionalTopDownPadding = (int) (0.15d * ((double) height));
                    boolean additionalTopDownPaddingFitsSides = additionalTopDownPadding < ((width - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingRight) - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingLeft) * 2;
                    if (!(additionalTopDownPadding < ((height - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingTop) - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingBottom) * 2) || !additionalTopDownPaddingFitsSides) {
                        int additionalSidePadding = (int) (0.1d * ((double) width));
                        boolean additionalSidePaddingFitsSides = additionalSidePadding < ((width - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingRight) - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingLeft) * 2;
                        if ((additionalSidePadding < ((height - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingTop) - com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingBottom) * 2) && additionalSidePaddingFitsSides) {
                            padding = additionalSidePadding;
                        }
                    } else {
                        padding = additionalTopDownPadding;
                    }
                    com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.moveMapInternal(com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds(com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon3.this.val$latLngBounds, padding), com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon3.this.val$animate);
                }
                this.val$container.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }

        Anon3(com.google.android.gms.maps.model.LatLngBounds latLngBounds, boolean z) {
            this.val$latLngBounds = latLngBounds;
            this.val$animate = z;
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.v("zoomTo, creating new CameraUpdate, map ready? " + (googleMap != null));
            android.view.View container = com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getView();
            if (container == null || !container.getViewTreeObserver().isAlive()) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.e("zoomTo, view is null or its viewtreeobserver not alive");
            } else {
                container.getViewTreeObserver().addOnGlobalLayoutListener(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon3.Anon1(container));
            }
        }
    }

    class Anon4 implements com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentAttached {
        Anon4() {
        }

        public void onAttached() {
            if (!com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.isVisible()) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.v("hide, already hidden, no-op");
                return;
            }
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getFragmentManager().beginTransaction().hide(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this).commitAllowingStateLoss();
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getFragmentManager().executePendingTransactions();
        }
    }

    class Anon5 implements com.google.android.gms.maps.OnMapReadyCallback {
        final /* synthetic */ com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent val$event;

        Anon5(com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent limitCellDataEvent) {
            this.val$event = limitCellDataEvent;
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            googleMap.setTrafficEnabled(!this.val$event.hasLimitedCellData);
        }
    }

    class Anon6 implements com.google.android.gms.maps.OnMapReadyCallback {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.google.android.gms.maps.GoogleMap val$googleMap;

            Anon1(com.google.android.gms.maps.GoogleMap googleMap) {
                this.val$googleMap = googleMap;
            }

            public void run() {
                this.val$googleMap.setMyLocationEnabled(true);
            }
        }

        Anon6() {
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            try {
                com.google.android.gms.maps.MapsInitializer.initialize(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getContext());
                com.google.android.gms.maps.UiSettings uiSettings = googleMap.getUiSettings();
                uiSettings.setScrollGesturesEnabled(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.scrollGesturesEnabled);
                uiSettings.setMyLocationButtonEnabled(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.myLocationButtonEnabled);
                uiSettings.setMapToolbarEnabled(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapToolbarEnabled);
                googleMap.setTrafficEnabled(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.trafficEnabled && !com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData());
                if (com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.myLocationEnabled) {
                    com.navdy.client.app.ui.base.BaseActivity.requestLocationPermission(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon6.Anon1(googleMap), null, com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getActivity());
                }
            } catch (java.lang.Exception e) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.e((java.lang.Throwable) e);
            }
        }
    }

    class Anon7 implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        final /* synthetic */ android.view.View val$finalView;

        class Anon1 implements com.google.android.gms.maps.OnMapReadyCallback {
            Anon1() {
            }

            public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
                googleMap.setPadding(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingLeft, com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingTop, com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingRight, com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingBottom);
            }
        }

        Anon7(android.view.View view) {
            this.val$finalView = view;
        }

        public void onGlobalLayout() {
            boolean paddingFitsSides;
            boolean paddingFitsTopDown;
            int width = this.val$finalView.getWidth();
            int height = this.val$finalView.getHeight();
            if (((double) width) > ((double) (com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingLeft + com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingRight)) * 1.5d) {
                paddingFitsSides = true;
            } else {
                paddingFitsSides = false;
            }
            if (((double) height) > ((double) (com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingTop + com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingBottom)) * 1.5d) {
                paddingFitsTopDown = true;
            } else {
                paddingFitsTopDown = false;
            }
            if (paddingFitsSides && !paddingFitsTopDown) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingTop = 0;
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingBottom = 0;
            } else if (paddingFitsTopDown && !paddingFitsSides) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingRight = 0;
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.mapPaddingLeft = 0;
            }
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon7.Anon1());
            this.val$finalView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    class Anon8 implements com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentAttached {
        Anon8() {
        }

        public void onAttached() {
            com.navdy.client.app.ui.base.BaseGoogleMapFragment.this.getFragmentManager().beginTransaction().setCustomAnimations(17498112, 17498113).show(com.navdy.client.app.ui.base.BaseGoogleMapFragment.this).commitAllowingStateLoss();
        }
    }

    class Anon9 implements com.google.android.gms.maps.OnMapReadyCallback {
        final /* synthetic */ boolean val$animate;
        final /* synthetic */ com.google.android.gms.maps.CameraUpdate val$cameraUpdate;

        Anon9(boolean z, com.google.android.gms.maps.CameraUpdate cameraUpdate) {
            this.val$animate = z;
            this.val$cameraUpdate = cameraUpdate;
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            try {
                if (this.val$animate) {
                    googleMap.animateCamera(this.val$cameraUpdate);
                } else {
                    googleMap.moveCamera(this.val$cameraUpdate);
                }
            } catch (java.lang.Exception e) {
                com.navdy.client.app.ui.base.BaseGoogleMapFragment.logger.e("moveMapInternal: Unable to move the map", e);
            }
        }
    }

    private interface OnGoogleMapFragmentAttached {
        void onAttached();
    }

    private interface OnGoogleMapFragmentReady {
        void onReady();
    }

    public interface OnShowMapListener {
        void onShow();
    }

    static {
        com.google.android.gms.maps.MapsInitializer.initialize(com.navdy.client.app.NavdyApplication.getAppContext());
    }

    public void show(@android.support.annotation.Nullable com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnShowMapListener onShowMapListener) {
        logger.v("show");
        whenReady(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon2(onShowMapListener));
    }

    @android.support.annotation.MainThread
    public void centerOnLastKnownLocation(boolean animate) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        centerMap(animate, this.navdyLocationManager.getPhoneCoordinates(), this.navdyLocationManager.getCarCoordinates());
    }

    @android.support.annotation.MainThread
    public void moveMap(android.location.Location location, float zoom, boolean animate) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        moveMap(new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude()), zoom, animate);
    }

    @android.support.annotation.MainThread
    public void moveMap(com.google.android.gms.maps.model.LatLng latLng, float zoom, boolean animate) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        moveMapInternal(buildCameraUpdate(latLng, zoom), animate);
    }

    @android.support.annotation.MainThread
    public void zoomTo(com.google.android.gms.maps.model.LatLngBounds latLngBounds, boolean animate) {
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon3(latLngBounds, animate));
    }

    public void hide() {
        whenAttached(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon4());
    }

    @com.squareup.otto.Subscribe
    public void onLimitCellDataEvent(com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent event) {
        getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon5(event));
    }

    public void onAttach(android.content.Context context) {
        super.onAttach(context);
        this.bus.register(this);
        this.isAttached.set(true);
        while (!this.attachCompleteListeners.isEmpty()) {
            ((com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentAttached) this.attachCompleteListeners.poll()).onAttached();
        }
    }

    public void onInflate(android.app.Activity activity, android.util.AttributeSet attrs, android.os.Bundle savedInstanceState) {
        logger.v("onInflate");
        if (com.navdy.client.app.framework.util.GmsUtils.finishIfGmsIsNotUpToDate(activity)) {
            logger.e("GMS is out of date or missing, preventing inflation");
            return;
        }
        super.onInflate(activity, attrs, savedInstanceState);
        android.content.res.TypedArray viewStyles = activity.obtainStyledAttributes(attrs, com.navdy.client.R.styleable.GoogleMapFragment);
        this.myLocationEnabled = viewStyles.getBoolean(4, true);
        this.myLocationButtonEnabled = viewStyles.getBoolean(5, false);
        this.carLocationEnabled = viewStyles.getBoolean(6, true);
        this.scrollGesturesEnabled = viewStyles.getBoolean(7, true);
        this.centerMapOnLastKnownLocation = viewStyles.getBoolean(8, true);
        this.trafficEnabled = viewStyles.getBoolean(9, true);
        this.mapToolbarEnabled = viewStyles.getBoolean(10, false);
        this.autoCenter = viewStyles.getBoolean(11, false);
        this.mapPaddingTop = viewStyles.getDimensionPixelSize(0, 0);
        this.mapPaddingBottom = viewStyles.getDimensionPixelSize(1, 0);
        this.mapPaddingLeft = viewStyles.getDimensionPixelSize(2, 0);
        this.mapPaddingRight = viewStyles.getDimensionPixelSize(3, 0);
        viewStyles.recycle();
    }

    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        logger.v("onCreateView");
        getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon6());
        try {
            android.view.View view = super.onCreateView(inflater, container, savedInstanceState);
            if (view == null || !view.getViewTreeObserver().isAlive()) {
                logger.e("container view is null or its viewtreeobserver is not alive");
                return view;
            }
            view.getViewTreeObserver().addOnGlobalLayoutListener(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon7(view));
            return view;
        } catch (Throwable t) {
            logger.e("could not inflate Google Map Fragment", t);
            return inflater.inflate(com.navdy.client.R.layout.error_loading_google_maps, container);
        }
    }

    public void onResume() {
        super.onResume();
        while (!this.readyCompleteListeners.isEmpty()) {
            ((com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentReady) this.readyCompleteListeners.poll()).onReady();
        }
        if (this.centerMapOnLastKnownLocation) {
            centerOnLastKnownLocation(false);
        }
        if (this.carLocationEnabled && this.navdyLocationManager.getCarCoordinates() != null) {
            updateCarMarker(this.navdyLocationManager.getCarCoordinates());
        }
        if (this.myLocationEnabled || this.carLocationEnabled) {
            this.navdyLocationManager.addListener(this.navdyLocationListener);
        }
    }

    public void onPause() {
        if (this.myLocationEnabled || this.carLocationEnabled) {
            this.navdyLocationManager.removeListener(this.navdyLocationListener);
        }
        super.onPause();
    }

    public void onDetach() {
        if (this.isAttached.get()) {
            this.bus.unregister(this);
            this.isAttached.set(false);
        }
        super.onDetach();
    }

    public android.content.Context getContext() {
        return com.navdy.client.app.NavdyApplication.getAppContext();
    }

    @android.support.annotation.MainThread
    private void whenAttached(com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentAttached attachListener) {
        if (attachListener == null) {
            logger.w("tried to add a null attachListener");
            return;
        }
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (isAdded()) {
            attachListener.onAttached();
        } else if (isRemoving()) {
            logger.v("whenAttached, isRemoving, no-op");
        } else {
            this.attachCompleteListeners.add(attachListener);
        }
    }

    @android.support.annotation.MainThread
    private void whenReady(com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnGoogleMapFragmentReady readyListener) {
        if (readyListener == null) {
            logger.w("tried to add a null readyListener");
            return;
        }
        com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
        if (isResumed()) {
            readyListener.onReady();
        } else if (isRemoving()) {
            logger.v("whenAttached, isRemoving, no-op");
        } else {
            this.readyCompleteListeners.add(readyListener);
        }
    }

    private void fadeIn() {
        whenAttached(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon8());
    }

    private void centerMap(boolean animate, @android.support.annotation.Nullable com.navdy.service.library.events.location.Coordinate phoneLocation, @android.support.annotation.Nullable com.navdy.service.library.events.location.Coordinate carLocation) {
        if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(phoneLocation) || !com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(carLocation)) {
            if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(phoneLocation)) {
                moveMap(new com.google.android.gms.maps.model.LatLng(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()), 16.0f, animate);
            } else if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(carLocation)) {
                moveMap(new com.google.android.gms.maps.model.LatLng(carLocation.latitude.doubleValue(), carLocation.longitude.doubleValue()), 16.0f, animate);
            }
        } else if (com.navdy.client.app.framework.map.MapUtils.distanceBetween(phoneLocation, carLocation) > MIN_DISTANCE_CENTER_MAP_BOTH) {
            com.google.android.gms.maps.model.LatLngBounds.Builder builder = new com.google.android.gms.maps.model.LatLngBounds.Builder();
            builder.include(new com.google.android.gms.maps.model.LatLng(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()));
            builder.include(new com.google.android.gms.maps.model.LatLng(carLocation.latitude.doubleValue(), carLocation.longitude.doubleValue()));
            zoomTo(builder.build(), animate);
        } else {
            moveMap(new com.google.android.gms.maps.model.LatLng(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()), 16.0f, animate);
        }
    }

    private void moveMapInternal(@android.support.annotation.NonNull com.google.android.gms.maps.CameraUpdate cameraUpdate, boolean animate) {
        getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon9(animate, cameraUpdate));
    }

    private com.google.android.gms.maps.model.LatLng getTargetSafely(com.google.android.gms.maps.GoogleMap googleMap) {
        try {
            return googleMap.getCameraPosition().target;
        } catch (java.lang.RuntimeException e) {
            logger.e("Unable to get camera target because: " + e.getCause());
            return null;
        }
    }

    private com.google.android.gms.maps.model.LatLngBounds getLatLongBoundsSafely(com.google.android.gms.maps.GoogleMap googleMap) {
        try {
            return googleMap.getProjection().getVisibleRegion().latLngBounds;
        } catch (java.lang.RuntimeException e) {
            logger.e("Unable to get lat long bounds because: " + e.getCause());
            return null;
        }
    }

    private com.google.android.gms.maps.CameraUpdate buildCameraUpdate(@android.support.annotation.NonNull com.google.android.gms.maps.model.LatLng latLng, float zoom) {
        com.google.android.gms.maps.model.CameraPosition.Builder builder = new com.google.android.gms.maps.model.CameraPosition.Builder().target(latLng);
        if (zoom != -1.0f) {
            builder.zoom(zoom);
        }
        return com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition(builder.build());
    }

    private void updateCarMarker(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate carLocation) {
        if (this.carLocationEnabled) {
            getMapAsync(new com.navdy.client.app.ui.base.BaseGoogleMapFragment.Anon10(carLocation));
        }
    }
}
