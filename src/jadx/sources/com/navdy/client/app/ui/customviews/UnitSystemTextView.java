package com.navdy.client.app.ui.customviews;

public class UnitSystemTextView extends android.support.v7.widget.AppCompatTextView implements com.navdy.client.app.framework.i18n.I18nManager.Listener {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.customviews.UnitSystemTextView.class);
    private final android.content.Context context;
    private com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem currentUnitSystem;
    private double currentValue;
    private final com.navdy.client.app.framework.i18n.I18nManager i18nManager;

    public UnitSystemTextView(android.content.Context context2) {
        this(context2, null, 0);
    }

    public UnitSystemTextView(android.content.Context context2, android.util.AttributeSet attrs) {
        this(context2, attrs, 0);
    }

    public UnitSystemTextView(android.content.Context context2, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context2, attrs, defStyleAttr);
        this.context = context2;
        this.i18nManager = com.navdy.client.app.framework.i18n.I18nManager.getInstance();
        this.currentUnitSystem = this.i18nManager.getUnitSystem();
        this.i18nManager.addListener(this);
    }

    public void setDistance(double meters) {
        if (this.currentUnitSystem == com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC) {
            java.lang.String convertedValue = java.lang.Double.toString(round(0.001d * meters));
            setText(this.context.getString(com.navdy.client.R.string.kilometers_abbrev, new java.lang.Object[]{convertedValue}));
        } else {
            java.lang.String convertedValue2 = java.lang.Double.toString(round(6.21371E-4d * meters));
            setText(this.context.getString(com.navdy.client.R.string.miles_abbrev, new java.lang.Object[]{convertedValue2}));
        }
        this.currentValue = meters;
    }

    public void onUnitSystemChanged(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem) {
        logger.v("unit system changed to " + unitSystem.name() + ", setting new value");
        this.currentUnitSystem = unitSystem;
        setDistance(this.currentValue);
    }

    protected void onDetachedFromWindow() {
        this.i18nManager.removeListener(this);
        super.onDetachedFromWindow();
    }

    private double round(double value) {
        return ((double) java.lang.Math.round(value * 10.0d)) / 10.0d;
    }

    public boolean isInEditMode() {
        return false;
    }
}
