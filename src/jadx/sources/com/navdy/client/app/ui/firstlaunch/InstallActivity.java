package com.navdy.client.app.ui.firstlaunch;

public class InstallActivity extends com.navdy.client.app.ui.base.BaseActivity {
    static final java.lang.String EXTRA_MOUNT = "extra_mount";
    static final java.lang.String EXTRA_STEP = "extra_step";
    private boolean comingFromSettings = false;
    private com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter installPagerAdapter = new com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter(getSupportFragmentManager());
    private boolean isFirstTimeInPager = true;
    private int startingStep = com.navdy.client.R.layout.fle_install_overview;
    private android.support.v4.view.ViewPager viewPager;
    private boolean weHadCarInfo = false;

    class Anon1 implements android.support.v4.view.ViewPager.OnPageChangeListener {
        Anon1() {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            com.navdy.client.app.ui.firstlaunch.InstallActivity.this.handler.removeCallbacksAndMessages(null);
            java.lang.String screen = com.navdy.client.app.ui.firstlaunch.InstallActivity.this.installPagerAdapter.getScreenAtPosition(position);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(screen)) {
                com.navdy.client.app.tracking.Tracker.tagScreen(screen);
            }
            com.navdy.client.app.ui.firstlaunch.InstallActivity.sLogger.v("trigger system gc");
            java.lang.System.gc();
        }

        public void onPageScrollStateChanged(int state) {
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_install);
        this.comingFromSettings = getIntent().getBooleanExtra(com.navdy.client.app.ui.firstlaunch.InstallCompleteActivity.EXTRA_COMING_FROM_SETTINGS, false);
        this.viewPager = (android.support.v4.view.ViewPager) findViewById(com.navdy.client.R.id.view_pager);
        if (this.viewPager == null) {
            this.logger.e("Missing UI element: ViewPager !!!");
            return;
        }
        this.viewPager.setOffscreenPageLimit(1);
        this.viewPager.addOnPageChangeListener(new com.navdy.client.app.ui.firstlaunch.InstallActivity.Anon1());
        java.lang.String screen = this.installPagerAdapter.getScreenAtPosition(0);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(screen)) {
            com.navdy.client.app.tracking.Tracker.tagScreen(screen);
        }
        this.viewPager.setAdapter(this.installPagerAdapter);
        android.content.Intent in = getIntent();
        this.startingStep = in.getIntExtra(EXTRA_STEP, com.navdy.client.R.layout.fle_install_overview);
        java.lang.String mount = in.getStringExtra(EXTRA_MOUNT);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(mount)) {
            com.navdy.client.app.framework.models.MountInfo.MountType type = com.navdy.client.app.framework.models.MountInfo.MountType.getMountTypeForValue(mount);
            this.installPagerAdapter.setCurrentMountType(type);
            if (type != com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
                com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
            }
        }
        if (this.startingStep == com.navdy.client.R.layout.fle_install_short_mount || this.startingStep == com.navdy.client.R.layout.fle_install_secure_mount) {
            this.installPagerAdapter.setCurrentMountType(com.navdy.client.app.framework.models.MountInfo.MountType.SHORT);
        }
        if (this.startingStep == com.navdy.client.R.layout.fle_install_medium_or_tall_mount && this.installPagerAdapter.getCurrentMountType() == com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
            this.installPagerAdapter.setCurrentMountType(com.navdy.client.app.framework.models.MountInfo.MountType.TALL);
        }
        if (this.startingStep >= this.installPagerAdapter.getCount()) {
            this.installPagerAdapter.setHasPickedMount(true);
        }
        this.viewPager.setCurrentItem(com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter.getPositionForStep(this.startingStep, com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter.getInstallFlow(this.installPagerAdapter.getCurrentMountType())));
    }

    protected void onResume() {
        boolean weAreNotOnTheLastPage;
        super.onResume();
        hideSystemUI();
        if (this.weHadCarInfo != com.navdy.client.app.tracking.Tracker.weHaveCarInfo()) {
            this.weHadCarInfo = com.navdy.client.app.tracking.Tracker.weHaveCarInfo();
            this.installPagerAdapter.notifyDataSetChanged();
        }
        if (this.viewPager.getCurrentItem() < this.installPagerAdapter.getCount() - 1) {
            weAreNotOnTheLastPage = true;
        } else {
            weAreNotOnTheLastPage = false;
        }
        if (this.isFirstTimeInPager && weAreNotOnTheLastPage) {
            this.isFirstTimeInPager = false;
        }
    }

    public void onBackPressed() {
        android.view.View dialog = findViewById(com.navdy.client.R.id.custom_dialog);
        if (dialog == null || dialog.getVisibility() != View.VISIBLE) {
            if (this.viewPager != null) {
                int currentItem = this.viewPager.getCurrentItem();
                if (this.installPagerAdapter != null && (this.installPagerAdapter.isOnStepAfterCablePicker(currentItem) || this.installPagerAdapter.isOnStepAfterLocatingObd(currentItem))) {
                    super.onBackPressed();
                    return;
                } else if (currentItem - 1 >= 0) {
                    this.viewPager.setCurrentItem(currentItem - 1);
                    return;
                }
            }
            super.onBackPressed();
            return;
        }
        dialog.setVisibility(View.GONE);
    }

    public void onBackClick(android.view.View view) {
        onBackPressed();
    }

    public void onWatchClick(android.view.View view) {
        startVideoPlayerForCurrentStep();
    }

    public void onNextClick(android.view.View view) {
        if (this.viewPager != null) {
            int currentItem = this.viewPager.getCurrentItem();
            if (this.installPagerAdapter.isOnStepBeforeCablePicker(currentItem)) {
                startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CablePickerActivity.class));
            } else if (currentItem + 1 < this.installPagerAdapter.getCount()) {
                this.viewPager.setCurrentItem(currentItem + 1);
            } else if (this.comingFromSettings) {
                finish();
            } else {
                startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallCompleteActivity.class));
            }
        }
    }

    public void onLocateObdClick(android.view.View view) {
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CarInfoActivity.class);
        intent.putExtra("extra_next_step", "installation_flow");
        startActivity(intent);
    }

    public void startVideoPlayerForCurrentStep() {
        startVideoPlayerForThisStep(com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter.getStepForCurrentPosition(this.viewPager.getCurrentItem(), this.installPagerAdapter.getCurrentMountType()), this);
    }

    public static void startVideoPlayerForThisStep(int step, android.app.Activity activity) {
        java.lang.String videoUrl = getVideoUrlForStep(step);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(videoUrl)) {
            sLogger.e("Unable to start video player. the video url is null !");
            return;
        }
        android.content.Intent intent = new android.content.Intent(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.EXTRA_VIDEO_URL, videoUrl);
        activity.startActivity(intent);
    }

    @android.support.annotation.Nullable
    public static java.lang.String getVideoUrlForStep(@android.support.annotation.LayoutRes int step) {
        int videoUrlIndex;
        switch (step) {
            case com.navdy.client.R.layout.fle_install_dial /*2130903151*/:
                videoUrlIndex = 7;
                break;
            case com.navdy.client.R.layout.fle_install_locate_obd /*2130903154*/:
                videoUrlIndex = 4;
                break;
            case com.navdy.client.R.layout.fle_install_medium_or_tall_mount /*2130903155*/:
                videoUrlIndex = 2;
                break;
            case com.navdy.client.R.layout.fle_install_overview /*2130903157*/:
                videoUrlIndex = 0;
                break;
            case com.navdy.client.R.layout.fle_install_secure_mount /*2130903158*/:
                videoUrlIndex = 3;
                break;
            case com.navdy.client.R.layout.fle_install_short_mount /*2130903159*/:
                videoUrlIndex = 1;
                break;
            case com.navdy.client.R.layout.fle_install_tidying_up /*2130903160*/:
                videoUrlIndex = 5;
                break;
            case com.navdy.client.R.layout.fle_install_turn_on /*2130903161*/:
                videoUrlIndex = 6;
                break;
            default:
                videoUrlIndex = -1;
                break;
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (context == null) {
            sLogger.e("Unable to start video player. context is null !");
            return null;
        }
        android.content.res.Resources resources = context.getResources();
        if (resources == null) {
            sLogger.e("Unable to start video player. resources are null !");
            return null;
        }
        java.lang.String[] videoUrls = resources.getStringArray(com.navdy.client.R.array.video_urls);
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box"), "Old_Box")) {
            videoUrls = resources.getStringArray(com.navdy.client.R.array.video_urls_2017);
        }
        if (videoUrls.length > 0 && videoUrlIndex >= 0 && videoUrlIndex <= videoUrls.length) {
            return videoUrls[videoUrlIndex];
        }
        sLogger.e("Unable to start video player. the video url array is null !");
        return null;
    }

    public void onMyLensIsTooHighClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        switch (this.installPagerAdapter.getCurrentMountType()) {
            case SHORT:
                showDialogForTooHighWithLowMount();
                return;
            case MEDIUM:
            case TALL:
                showDialogForTooHighWithMediumTallMount();
                return;
            default:
                return;
        }
    }

    public void onMyLensIsOkClick(android.view.View view) {
        this.installPagerAdapter.setHasPickedMount(true);
        if (com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, false)) {
            showDialogForWhatEndedUpWorking();
            return;
        }
        switch (this.installPagerAdapter.getCurrentMountType()) {
            case SHORT:
                onShortWorkedClick(view);
                return;
            case MEDIUM:
                onMediumWorkedClick(view);
                return;
            case TALL:
                onTallWorkedClick(view);
                return;
            default:
                return;
        }
    }

    public void onMyLensIsTooLowClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        switch (this.installPagerAdapter.getCurrentMountType()) {
            case SHORT:
                showDialogForTooLowWithLowMount();
                return;
            case MEDIUM:
            case TALL:
                showDialogForTooLowWithMediumTallMount();
                return;
            default:
                return;
        }
    }

    public void hideCustomDialog() {
        android.view.View dialog = findViewById(com.navdy.client.R.id.custom_dialog);
        if (dialog != null) {
            dialog.setVisibility(View.GONE);
        }
    }

    public void showDialogForTooHighWithLowMount() {
        showThisDialog(com.navdy.client.R.string.is_the_lens_too_high, com.navdy.client.R.string.try_raising_seat, true, false, false);
    }

    public void showDialogForTooLowWithLowMount() {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.MountPickerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.MountPickerActivity.EXTRA_USE_CASE, 1);
        startActivity(intent);
    }

    public void showDialogForTooHighWithMediumTallMount() {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.MountPickerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.MountPickerActivity.EXTRA_USE_CASE, 2);
        startActivity(intent);
    }

    public void showDialogForTooLowWithMediumTallMount() {
        showThisDialog(com.navdy.client.R.string.is_the_lens_too_low, com.navdy.client.R.string.if_medium_try_tall_if_tall_contact_us, true, true, false);
    }

    public void showDialogForWhatEndedUpWorking() {
        showThisDialog(com.navdy.client.R.string.what_ended_up_working, com.navdy.client.R.string.what_ended_up_working_desc, false, false, false, true, true, true, false);
    }

    public void showThisDialog(int titleResId, int descResId, boolean showContactUs, boolean showMediumTallMount, boolean showShortMount) {
        showThisDialog(titleResId, descResId, showContactUs, showMediumTallMount, showShortMount, false, false, false, true);
    }

    public void showThisDialog(int titleResId, int descResId, boolean showContactUs, boolean showMediumTallMount, boolean showShortMount, boolean showShortWorked, boolean showMediumWorked, boolean showTallWorked, boolean showCancel) {
        android.view.View dialog = findViewById(com.navdy.client.R.id.custom_dialog);
        if (dialog != null) {
            dialog.setVisibility(View.VISIBLE);
        }
        android.widget.TextView title = (android.widget.TextView) findViewById(com.navdy.client.R.id.custom_dialog_title);
        if (title != null) {
            title.setVisibility(View.VISIBLE);
            title.setText(titleResId);
        }
        android.widget.TextView desc = (android.widget.TextView) findViewById(com.navdy.client.R.id.custom_dialog_desc);
        if (desc != null) {
            desc.setVisibility(View.VISIBLE);
            desc.setText(descResId);
        }
        updateThisButton(com.navdy.client.R.id.short_mount, showShortMount);
        updateThisButton(com.navdy.client.R.id.medium_tall_mount, showMediumTallMount);
        updateThisButton(com.navdy.client.R.id.contact_support, showContactUs);
        updateThisButton(com.navdy.client.R.id.cancel, showCancel);
        updateThisButton(com.navdy.client.R.id.short_worked, showShortWorked);
        updateThisButton(com.navdy.client.R.id.medium_worked, showMediumWorked);
        updateThisButton(com.navdy.client.R.id.tall_worked, showTallWorked);
    }

    public void updateThisButton(int buttonResId, boolean show) {
        android.widget.Button contactSupport = (android.widget.Button) findViewById(buttonResId);
        if (contactSupport != null) {
            contactSupport.setVisibility(show ? 0 : 8);
        }
    }

    public void onContactSupportClick(android.view.View view) {
        hideCustomDialog();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.ContactUsActivity.class);
        intent.putExtra(com.navdy.client.app.ui.settings.ContactUsActivity.EXTRA_DEFAULT_PROBLEM_TYPE, 1);
        startActivity(intent);
    }

    public void onMediumTallMountClick(android.view.View view) {
        onMountClick(com.navdy.client.app.framework.models.MountInfo.MountType.TALL);
    }

    public void onShortMountClick(android.view.View view) {
        onMountClick(com.navdy.client.app.framework.models.MountInfo.MountType.SHORT);
    }

    private void onMountClick(com.navdy.client.app.framework.models.MountInfo.MountType mountType) {
        setMountTypeAndForceRefreshAdapterOnLensCheck(mountType);
        onBackClick(null);
    }

    public void onCancelClick(android.view.View view) {
        hideCustomDialog();
    }

    public void onShortWorkedClick(android.view.View view) {
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Event.Install.USER_PICKED_SHORT_MOUNT);
        onWorkedClick(com.navdy.client.app.framework.models.MountInfo.MountType.SHORT);
    }

    public void onMediumWorkedClick(android.view.View view) {
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Event.Install.USER_PICKED_MEDIUM_MOUNT);
        onWorkedClick(com.navdy.client.app.framework.models.MountInfo.MountType.MEDIUM);
    }

    public void onTallWorkedClick(android.view.View view) {
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Event.Install.USER_PICKED_TALL_MOUNT);
        onWorkedClick(com.navdy.client.app.framework.models.MountInfo.MountType.TALL);
    }

    private void onWorkedClick(com.navdy.client.app.framework.models.MountInfo.MountType mountType) {
        setMountTypeAndForceRefreshAdapterOnLensCheck(mountType);
        onNextClick(null);
    }

    private void setMountTypeAndForceRefreshAdapterOnLensCheck(com.navdy.client.app.framework.models.MountInfo.MountType mountType) {
        hideCustomDialog();
        this.installPagerAdapter = new com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter(getSupportFragmentManager());
        this.installPagerAdapter.setCurrentMountType(mountType);
        this.viewPager.setAdapter(this.installPagerAdapter);
        this.viewPager.setCurrentItem(2);
    }

    public static boolean userHasFinishedInstall() {
        return com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FINISHED_INSTALL, false);
    }
}
