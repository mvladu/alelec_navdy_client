package com.navdy.client.app.ui.search;

public class SearchActivity extends com.navdy.client.app.ui.base.BaseActivity implements com.google.android.gms.location.LocationListener, android.view.View.OnClickListener, com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {
    private static final int AUTO_COMPLETE_MARGIN_BOTTOM = com.navdy.client.app.ui.UiUtils.convertDpToPx(2.0f);
    private static final int AUTO_COMPLETE_MARGIN_LEFT = com.navdy.client.app.ui.UiUtils.convertDpToPx(10.5f);
    private static final int AUTO_COMPLETE_MARGIN_RIGHT = com.navdy.client.app.ui.UiUtils.convertDpToPx(10.5f);
    private static final int AUTO_COMPLETE_MARGIN_TOP = com.navdy.client.app.ui.UiUtils.convertDpToPx(-14.0f);
    private static final java.lang.String EXTRA_LOCATION = "location";
    private static final java.lang.String EXTRA_SPEECH_RECOGNITION = "speech_recognition";
    private static final int KEY_LISTENER_DELAY = 500;
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 42;
    private static boolean hasDisplayedOfflineMessage = false;
    private android.widget.RelativeLayout card;
    private android.widget.ImageButton centerMap;
    private android.widget.EditText editText;
    private android.widget.ImageButton fab;
    private boolean fromActionSendIntent = false;
    private com.navdy.client.app.ui.base.BaseGoogleMapFragment googleMapFragment;
    private com.navdy.client.app.framework.search.GooglePlacesSearch googlePlacesSearch;
    private java.util.concurrent.atomic.AtomicBoolean ignoreAutoCompleteDelay = new java.util.concurrent.atomic.AtomicBoolean(false);
    private java.util.concurrent.atomic.AtomicBoolean ignoreNextKeystrokeKey = new java.util.concurrent.atomic.AtomicBoolean(false);
    private boolean isComingFromGoogleNow = false;
    private boolean isShowingMap = false;
    private android.location.Location lastLocation;
    @javax.inject.Inject
    com.navdy.client.app.framework.util.TTSAudioRouter mAudioRouter;
    private com.google.android.gms.common.api.GoogleApiClient mGoogleApiClient;
    private com.google.android.gms.location.LocationRequest mLocationRequest;
    private final com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    private android.widget.RelativeLayout offlineBanner;
    private com.google.android.gms.maps.model.Marker previousSelectedMarker;
    private boolean requestedLocationUpdates;
    private com.navdy.client.app.framework.models.Destination requestedPlaceDetailsDestinationBackup;
    private boolean rightButtonIsMic = true;
    private com.navdy.client.app.ui.search.SearchRecyclerAdapter searchAdapter;
    private android.support.v7.widget.RecyclerView searchRecycler;
    private int searchType;
    private boolean showServices = true;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.ui.search.SearchActivity.this.buildAndConnectGoogleApiClient();
            android.content.Context context = com.navdy.client.app.ui.search.SearchActivity.this.getApplicationContext();
            if (android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 && android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                com.navdy.client.app.ui.search.SearchActivity.this.lastLocation = com.google.android.gms.location.LocationServices.FusedLocationApi.getLastLocation(com.navdy.client.app.ui.search.SearchActivity.this.mGoogleApiClient);
            }
            com.navdy.client.app.ui.search.SearchActivity.this.getLatLngBounds();
            com.navdy.client.app.ui.search.SearchActivity.this.requestContactsPermission();
        }
    }

    class Anon10 implements android.view.View.OnKeyListener {
        Anon10() {
        }

        public boolean onKey(android.view.View v, int keyCode, android.view.KeyEvent event) {
            if (keyCode == 66 && event.getAction() == 0) {
                com.navdy.client.app.ui.search.SearchActivity.this.runTextSearchOrAskHud(com.navdy.client.app.ui.search.SearchActivity.this.editText.getText().toString());
            }
            return false;
        }
    }

    class Anon11 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ java.lang.String val$query;

        Anon11(java.lang.String str) {
            this.val$query = str;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            com.navdy.client.app.providers.NavdyContentProvider.addToSearchHistory(this.val$query);
            return null;
        }
    }

    class Anon12 implements com.navdy.client.app.framework.search.NavdySearch.SearchCallback {
        Anon12() {
        }

        public void onSearchStarted() {
        }

        public void onSearchCompleted(com.navdy.client.app.framework.search.SearchResults searchResults) {
            com.navdy.client.app.ui.search.SearchActivity.this.putSearchResultExtraAndFinishIfNoUiRequired(searchResults);
            com.navdy.client.app.ui.search.SearchActivity.this.addDistancesToDestinationsIfNeeded(searchResults);
            com.navdy.client.app.ui.search.SearchActivity.this.updateRecyclerViewWithSearchResults(searchResults, true);
            com.navdy.client.app.ui.search.SearchActivity.this.hideProgressDialog();
        }
    }

    class Anon13 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.search.SearchResults val$searchResults;
        final /* synthetic */ boolean val$showNoResultsFoundDialog;

        Anon13(com.navdy.client.app.framework.search.SearchResults searchResults, boolean z) {
            this.val$searchResults = searchResults;
            this.val$showNoResultsFoundDialog = z;
        }

        public void run() {
            boolean showMapFab = true;
            com.navdy.client.app.ui.search.SearchActivity.this.clearAndApplyListOrDropDownLook();
            java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts = this.val$searchResults.getContacts();
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithContactsForTextSearch(contacts);
            java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleResults = this.val$searchResults.getGoogleResults();
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithTextSearchResults(googleResults);
            java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> hudDestinations = this.val$searchResults.getHudSearchResults();
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithPlaceSearchResults(hudDestinations);
            java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinationsFromDatabase = this.val$searchResults.getDestinationsFromDatabase();
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithDatabaseData(destinationsFromDatabase, this.val$searchResults.getQuery());
            if (this.val$showNoResultsFoundDialog) {
                com.navdy.client.app.ui.search.SearchActivity.this.showNoResultsFoundDialogIfAllAreEmpty(contacts, googleResults, hudDestinations, destinationsFromDatabase);
            }
            com.navdy.client.app.ui.search.SearchActivity.this.hideProgressDialog();
            if ((googleResults == null || googleResults.isEmpty()) && ((hudDestinations == null || hudDestinations.isEmpty()) && (destinationsFromDatabase == null || destinationsFromDatabase.isEmpty()))) {
                showMapFab = false;
            }
            com.navdy.client.app.ui.search.SearchActivity.this.showList(showMapFab);
        }
    }

    class Anon14 implements java.lang.Runnable {
        final /* synthetic */ java.util.List val$googleResults;

        Anon14(java.util.List list) {
            this.val$googleResults = list;
        }

        public void run() {
            com.navdy.client.app.ui.search.SearchActivity.this.addDistancesToDestinations(this.val$googleResults);
        }
    }

    class Anon15 implements com.navdy.client.app.framework.search.NavdySearch.SearchCallback {
        Anon15() {
        }

        public void onSearchStarted() {
        }

        public void onSearchCompleted(com.navdy.client.app.framework.search.SearchResults searchResults) {
            com.navdy.client.app.ui.search.SearchActivity.this.putSearchResultExtraAndFinishIfNoUiRequired(searchResults);
            com.navdy.client.app.ui.search.SearchActivity.this.updateRecyclerViewWithSearchResults(searchResults, true);
        }
    }

    class Anon16 implements java.lang.Runnable {
        final /* synthetic */ java.util.List val$finalDestinations;

        Anon16(java.util.List list) {
            this.val$finalDestinations = list;
        }

        public void run() {
            com.navdy.client.app.ui.search.SearchActivity.this.addDistancesToDestinations(this.val$finalDestinations);
        }
    }

    class Anon17 implements java.lang.Runnable {
        final /* synthetic */ java.util.List val$destinations;

        Anon17(java.util.List list) {
            this.val$destinations = list;
        }

        public void run() {
            com.navdy.client.app.ui.search.SearchActivity.this.clearAndApplyListOrDropDownLook();
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithTextSearchResults(this.val$destinations);
            if (this.val$destinations == null || this.val$destinations.size() <= 0) {
                com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.addNoResultsItem();
                com.navdy.client.app.ui.search.SearchActivity.this.showNoResultsFoundDialog();
            }
            com.navdy.client.app.ui.search.SearchActivity.this.showList();
        }
    }

    class Anon18 extends android.os.AsyncTask<java.lang.Integer, java.lang.Void, java.lang.Integer> {
        Anon18() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            com.navdy.client.app.ui.search.SearchActivity.this.showProgressDialog();
            com.navdy.client.app.ui.search.SearchActivity.this.clearAndApplyListOrDropDownLook();
        }

        /* access modifiers changed from: protected */
        public java.lang.Integer doInBackground(java.lang.Integer... ids) {
            if (ids == null || ids.length != 1) {
                return java.lang.Integer.valueOf(-1);
            }
            int id = com.navdy.client.app.providers.NavdyContentProvider.removeFromSearchHistory(ids[0].intValue());
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.setUpServicesAndHistory(com.navdy.client.app.ui.search.SearchActivity.this.showServices);
            return java.lang.Integer.valueOf(id);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Integer nbRows) {
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.notifyDataSetChanged();
            com.navdy.client.app.ui.search.SearchActivity.this.hideProgressDialog();
            if (nbRows.intValue() < 1) {
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.unable_to_remove_search_history, new java.lang.Object[0]);
            }
        }
    }

    class Anon19 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$d;

        Anon19(com.navdy.client.app.framework.models.Destination destination) {
            this.val$d = destination;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            com.navdy.client.app.providers.NavdyContentProvider.addToSearchHistory(this.val$d.name);
            return null;
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.cant_search_without_location_permission, new java.lang.Object[0]);
            com.navdy.client.app.ui.search.SearchActivity.this.requestContactsPermission();
        }
    }

    class Anon20 implements android.content.DialogInterface.OnClickListener {
        final /* synthetic */ android.view.View val$searchRow;

        Anon20(android.view.View view) {
            this.val$searchRow = view;
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            com.navdy.client.app.ui.search.SearchActivity.hasDisplayedOfflineMessage = true;
            com.navdy.client.app.ui.search.SearchActivity.this.onClick(this.val$searchRow);
        }
    }

    class Anon21 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$d;

        Anon21(com.navdy.client.app.framework.models.Destination destination) {
            this.val$d = destination;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            if (this.val$d != null) {
                com.navdy.client.app.providers.NavdyContentProvider.addToSearchHistory(this.val$d.name);
            }
            return null;
        }
    }

    class Anon22 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$d;

        class Anon1 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
            Anon1() {
            }

            public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
                com.navdy.client.app.ui.search.SearchActivity.this.putSearchResultExtraAndFinish(destination);
            }

            public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
                com.navdy.client.app.ui.search.SearchActivity.this.logger.v("navigationHelper failed to get latLng for ContactModel Destination");
                com.navdy.client.app.ui.search.SearchActivity.this.putSearchResultExtraAndFinish(destination);
            }
        }

        Anon22(com.navdy.client.app.framework.models.Destination destination) {
            this.val$d = destination;
        }

        public void run() {
            com.navdy.client.app.ui.search.SearchActivity.this.logger.v("setResultForContactAndFinish: " + this.val$d.rawAddressNotForDisplay);
            com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(this.val$d, new com.navdy.client.app.ui.search.SearchActivity.Anon22.Anon1());
        }
    }

    class Anon23 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, com.navdy.client.app.framework.models.Destination> {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon23(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destination = destination;
        }

        /* access modifiers changed from: protected */
        public com.navdy.client.app.framework.models.Destination doInBackground(java.lang.Void... voids) {
            com.navdy.client.app.ui.search.SearchActivity.this.logger.v("Destination address: " + this.val$destination);
            if (this.val$destination == null) {
                return null;
            }
            com.navdy.client.app.providers.NavdyContentProvider.addToSearchHistory(this.val$destination.name);
            this.val$destination.reloadSelfFromDatabase();
            return this.val$destination;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(com.navdy.client.app.framework.models.Destination d) {
            android.content.Intent result = new android.content.Intent();
            result.putExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT, d);
            if (com.navdy.client.app.ui.search.SearchActivity.this.isShowingMap) {
                com.navdy.client.app.tracking.SetDestinationTracker.getInstance().setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.SEARCH_MAP);
            }
            if (com.navdy.client.app.ui.search.SearchActivity.this.isComingFromGoogleNow) {
                android.util.Pair<java.lang.String, java.lang.String> pair = d.getTitleAndSubtitle();
                if (pair != null && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim((java.lang.CharSequence) pair.first)) {
                    com.navdy.client.app.ui.search.SearchActivity.this.mAudioRouter.processTTSRequest(com.navdy.client.app.ui.search.SearchActivity.this.getString(com.navdy.client.R.string.ok_navigating_to_x, new java.lang.Object[]{pair.first}), null, false);
                }
                result.putExtra(com.navdy.client.app.ui.search.SearchConstants.EXTRA_IS_VOICE_SEARCH, true);
            }
            com.navdy.client.app.ui.search.SearchActivity.this.setResult(-1, result);
            com.navdy.client.app.ui.search.SearchActivity.this.hideProgressDialog();
            com.navdy.client.app.ui.search.SearchActivity.this.finish();
        }
    }

    class Anon24 implements android.content.DialogInterface.OnClickListener {
        Anon24() {
        }

        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            com.navdy.client.app.ui.search.SearchActivity.this.editText.requestFocus();
            ((android.view.inputmethod.InputMethodManager) com.navdy.client.app.ui.search.SearchActivity.this.getSystemService("input_method")).toggleSoftInput(2, 0);
        }
    }

    class Anon25 implements com.google.android.gms.maps.OnMapReadyCallback {

        class Anon1 implements com.google.android.gms.maps.GoogleMap.OnMarkerClickListener {
            Anon1() {
            }

            public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {
                if (marker != com.navdy.client.app.ui.search.SearchActivity.this.previousSelectedMarker) {
                    com.navdy.client.app.ui.search.SearchActivity.this.unselectCurrentMarker();
                    com.navdy.client.app.ui.search.SearchActivity.this.selectThisMarker(marker);
                }
                return false;
            }
        }

        class Anon2 implements com.google.android.gms.maps.GoogleMap.OnMapClickListener {
            Anon2() {
            }

            public void onMapClick(com.google.android.gms.maps.model.LatLng latLng) {
                com.navdy.client.app.ui.search.SearchActivity.this.unselectCurrentMarker();
            }
        }

        Anon25() {
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            googleMap.setOnMarkerClickListener(new com.navdy.client.app.ui.search.SearchActivity.Anon25.Anon1());
            googleMap.setOnMapClickListener(new com.navdy.client.app.ui.search.SearchActivity.Anon25.Anon2());
        }
    }

    class Anon3 implements android.view.View.OnTouchListener {
        Anon3() {
        }

        @android.annotation.SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouch(android.view.View v, android.view.MotionEvent event) {
            com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(com.navdy.client.app.ui.search.SearchActivity.this);
            com.navdy.client.app.ui.search.SearchActivity.this.searchRecycler.requestFocus();
            return false;
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.cant_search_contacts_without_location_permission, new java.lang.Object[0]);
        }
    }

    class Anon5 implements android.view.View.OnClickListener {
        Anon5() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.search.SearchActivity.this.onBackPressed();
        }
    }

    class Anon6 implements android.view.View.OnClickListener {
        Anon6() {
        }

        public void onClick(android.view.View view) {
            if (com.navdy.client.app.ui.search.SearchActivity.this.rightButtonIsMic) {
                com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(com.navdy.client.app.ui.search.SearchActivity.this);
                com.navdy.client.app.ui.search.SearchActivity.this.startVoiceRecognitionActivity();
                return;
            }
            com.navdy.client.app.ui.search.SearchActivity.this.resetEditText();
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$constraint;

        class Anon1 implements com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener {
            final /* synthetic */ java.util.List val$contactList;

            /* renamed from: com.navdy.client.app.ui.search.SearchActivity$Anon7$Anon1$Anon1 reason: collision with other inner class name */
            class C0067Anon1 extends android.os.AsyncTask<java.lang.String, java.lang.Void, android.support.v4.util.Pair<java.util.ArrayList<com.navdy.client.app.framework.models.Destination>, java.util.ArrayList<java.lang.String>>> {
                final /* synthetic */ java.util.List val$autocompleteResults;

                C0067Anon1(java.util.List list) {
                    this.val$autocompleteResults = list;
                }

                /* access modifiers changed from: protected */
                public android.support.v4.util.Pair<java.util.ArrayList<com.navdy.client.app.framework.models.Destination>, java.util.ArrayList<java.lang.String>> doInBackground(java.lang.String... params) {
                    return new android.support.v4.util.Pair<>(com.navdy.client.app.providers.NavdyContentProvider.getDestinationsFromSearchQuery(com.navdy.client.app.ui.search.SearchActivity.Anon7.this.val$constraint), com.navdy.client.app.providers.NavdyContentProvider.getSearchHistoryFromSearchQuery(com.navdy.client.app.ui.search.SearchActivity.Anon7.this.val$constraint));
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(android.support.v4.util.Pair<java.util.ArrayList<com.navdy.client.app.framework.models.Destination>, java.util.ArrayList<java.lang.String>> pair) {
                    com.navdy.client.app.tracking.SetDestinationTracker.getInstance().setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.SEARCH_AUTOCOMPLETE);
                    java.util.ArrayList<com.navdy.client.app.framework.models.Destination> knownDestinations = pair.first;
                    java.util.ArrayList<java.lang.String> previousSearches = pair.second;
                    if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.search.SearchActivity.Anon7.this.val$constraint, com.navdy.client.app.ui.search.SearchActivity.this.editText.getText().toString())) {
                        com.navdy.client.app.ui.search.SearchActivity.this.clearAndApplyListOrDropDownLook();
                        com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.setLastQuery(com.navdy.client.app.ui.search.SearchActivity.Anon7.this.val$constraint);
                        com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithContactsForAutoComplete(com.navdy.client.app.ui.search.SearchActivity.Anon7.Anon1.this.val$contactList);
                        com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.addNoResultsItemIfEmpty(com.navdy.client.app.ui.search.SearchActivity.Anon7.Anon1.this.val$contactList, knownDestinations, this.val$autocompleteResults, previousSearches);
                        com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithDatabaseData(knownDestinations, com.navdy.client.app.ui.search.SearchActivity.Anon7.this.val$constraint);
                        com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithAutocomplete(this.val$autocompleteResults, previousSearches, com.navdy.client.app.ui.search.SearchActivity.Anon7.this.val$constraint);
                        com.navdy.client.app.ui.search.SearchActivity.this.showAutoComplete();
                    }
                }
            }

            Anon1(java.util.List list) {
                this.val$contactList = list;
            }

            public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> autocompleteResults, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
                com.navdy.client.app.ui.search.SearchActivity.this.logger.v("runGoogleQueryAutoComplete");
                new com.navdy.client.app.ui.search.SearchActivity.Anon7.Anon1.C0067Anon1(autocompleteResults).execute(new java.lang.String[]{com.navdy.client.app.ui.search.SearchActivity.Anon7.this.val$constraint});
            }
        }

        Anon7(java.lang.String str) {
            this.val$constraint = str;
        }

        public void run() {
            new com.navdy.client.app.framework.search.GooglePlacesSearch(new com.navdy.client.app.ui.search.SearchActivity.Anon7.Anon1(com.navdy.client.app.framework.util.ContactsManager.getInstance().getContactsAndLoadPhotos(this.val$constraint, java.util.EnumSet.of(com.navdy.client.app.framework.util.ContactsManager.Attributes.ADDRESS))), com.navdy.client.app.ui.search.SearchActivity.this.handler).runQueryAutoCompleteWebApi(this.val$constraint);
        }
    }

    class Anon8 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, android.support.v4.util.Pair<java.util.List<com.navdy.client.app.framework.models.ContactModel>, java.util.ArrayList<com.navdy.client.app.framework.models.Destination>>> {
        final /* synthetic */ java.lang.String val$constraint;

        Anon8(java.lang.String str) {
            this.val$constraint = str;
        }

        /* access modifiers changed from: protected */
        public android.support.v4.util.Pair<java.util.List<com.navdy.client.app.framework.models.ContactModel>, java.util.ArrayList<com.navdy.client.app.framework.models.Destination>> doInBackground(java.lang.Void... unusedParams) {
            com.navdy.client.app.ui.search.SearchActivity.this.logger.v("runOfflineAutocompleteSearch doInBackground");
            return new android.support.v4.util.Pair<>(com.navdy.client.app.framework.util.ContactsManager.getInstance().getContactsAndLoadPhotos(this.val$constraint, java.util.EnumSet.of(com.navdy.client.app.framework.util.ContactsManager.Attributes.ADDRESS)), com.navdy.client.app.providers.NavdyContentProvider.getDestinationsFromSearchQuery(this.val$constraint));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(android.support.v4.util.Pair<java.util.List<com.navdy.client.app.framework.models.ContactModel>, java.util.ArrayList<com.navdy.client.app.framework.models.Destination>> pair) {
            com.navdy.client.app.ui.search.SearchActivity.this.logger.v("runOfflineAutocompleteSearch onPostExecute");
            java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts = pair.first;
            java.util.ArrayList<com.navdy.client.app.framework.models.Destination> knownDestinations = pair.second;
            java.lang.String query = com.navdy.client.app.ui.search.SearchActivity.this.editText.getText().toString();
            if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.val$constraint, query)) {
                com.navdy.client.app.ui.search.SearchActivity.this.clearAndApplyListOrDropDownLook();
                com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.setLastQuery(query);
                com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithContactsForTextSearch(contacts);
                com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.updateRecyclerViewWithDatabaseData(knownDestinations, com.navdy.client.app.ui.search.SearchActivity.this.editText.getText().toString());
                com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.addNoResultsItemIfEmpty(contacts, knownDestinations);
                if (com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected()) {
                    com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.createSearchForMoreFooter(com.navdy.client.app.ui.search.SearchActivity.this.editText.getText().toString());
                }
                com.navdy.client.app.ui.search.SearchActivity.this.showAutoComplete();
            }
        }
    }

    class Anon9 implements android.text.TextWatcher {

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.lang.String val$string;

            Anon1(java.lang.String str) {
                this.val$string = str;
            }

            public void run() {
                if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.client.app.NavdyApplication.getAppContext())) {
                    com.navdy.client.app.ui.search.SearchActivity.this.runAutoCompleteSearch(this.val$string);
                } else {
                    com.navdy.client.app.ui.search.SearchActivity.this.runOfflineAutocompleteSearch(this.val$string);
                }
            }
        }

        Anon9() {
        }

        public void beforeTextChanged(java.lang.CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(android.text.Editable s) {
            com.navdy.client.app.ui.search.SearchActivity.this.handler.removeCallbacksAndMessages(null);
            java.lang.String string = s.toString();
            boolean textIsEmpty = com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(string);
            if (textIsEmpty) {
                com.navdy.client.app.ui.search.SearchActivity.this.setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.EMPTY);
            } else {
                com.navdy.client.app.ui.search.SearchActivity.this.setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE);
            }
            com.navdy.client.app.ui.search.SearchActivity.this.clearAndApplyListOrDropDownLook();
            if (textIsEmpty) {
                com.navdy.client.app.ui.search.SearchActivity.this.rightButtonIsMic = true;
                com.navdy.client.app.ui.search.SearchActivity.this.updateRightButton();
                com.navdy.client.app.ui.search.SearchActivity.this.showList();
                com.navdy.client.app.ui.search.SearchActivity.this.fab.setVisibility(View.GONE);
                new com.navdy.client.app.ui.search.SearchActivity.ServiceAndHistoryAsyncTask(com.navdy.client.app.ui.search.SearchActivity.this, null).execute(new java.lang.Void[0]);
                return;
            }
            com.navdy.client.app.ui.search.SearchActivity.this.rightButtonIsMic = false;
            com.navdy.client.app.ui.search.SearchActivity.this.updateRightButton();
            if (com.navdy.client.app.ui.search.SearchActivity.this.ignoreNextKeystrokeKey.get()) {
                com.navdy.client.app.ui.search.SearchActivity.this.ignoreNextKeystrokeKey.set(false);
                return;
            }
            com.navdy.client.app.framework.util.SystemUtils.ensureOnMainThread();
            int keyListenerDelay = 500;
            if (com.navdy.client.app.ui.search.SearchActivity.this.ignoreAutoCompleteDelay.get()) {
                com.navdy.client.app.ui.search.SearchActivity.this.ignoreAutoCompleteDelay.set(false);
                keyListenerDelay = 0;
            }
            com.navdy.client.app.ui.search.SearchActivity.this.handler.postDelayed(new com.navdy.client.app.ui.search.SearchActivity.Anon9.Anon1(string), (long) keyListenerDelay);
        }
    }

    private class ServiceAndHistoryAsyncTask<Params, Progress, Result> extends android.os.AsyncTask<Params, Progress, Result> {
        private ServiceAndHistoryAsyncTask() {
        }

        /* synthetic */ ServiceAndHistoryAsyncTask(com.navdy.client.app.ui.search.SearchActivity x0, com.navdy.client.app.ui.search.SearchActivity.Anon1 x1) {
            this();
        }

        protected void onPreExecute() {
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.clear();
        }

        protected Result doInBackground(Params[] paramsArr) {
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.setUpServicesAndHistory(com.navdy.client.app.ui.search.SearchActivity.this.showServices);
            return null;
        }

        protected void onPostExecute(Result result) {
            com.navdy.client.app.ui.search.SearchActivity.this.searchAdapter.notifyDataSetChanged();
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        boolean z;
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.search_activity);
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        checkServices();
        setupToolbarButtons();
        this.searchRecycler = (android.support.v7.widget.RecyclerView) findViewById(com.navdy.client.R.id.search_recycler_view);
        this.card = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.card);
        this.offlineBanner = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.offline_banner);
        this.fab = (android.widget.ImageButton) findViewById(com.navdy.client.R.id.fab);
        this.centerMap = (android.widget.ImageButton) findViewById(com.navdy.client.R.id.center_on_driver_button);
        this.googleMapFragment = (com.navdy.client.app.ui.base.BaseGoogleMapFragment) getFragmentManager().findFragmentById(com.navdy.client.R.id.map);
        requestLocationPermission(new com.navdy.client.app.ui.search.SearchActivity.Anon1(), new com.navdy.client.app.ui.search.SearchActivity.Anon2());
        setRecyclerView();
        setEditTextListener();
        this.googlePlacesSearch = new com.navdy.client.app.framework.search.GooglePlacesSearch(this, this.handler);
        setHintText();
        android.content.Intent intent = getIntent();
        if (intent.getBooleanExtra(EXTRA_SPEECH_RECOGNITION, false)) {
            startVoiceRecognitionActivity();
        } else {
            java.lang.String action = intent.getAction();
            boolean isGoogleNowSearch = com.google.android.gms.actions.SearchIntents.ACTION_SEARCH.equals(action);
            this.fromActionSendIntent = com.navdy.client.app.ui.search.SearchConstants.ACTION_SEND.equals(action);
            boolean isDestinationFinderSearch = com.navdy.client.app.ui.search.SearchConstants.ACTION_SEARCH.equals(action);
            if (isGoogleNowSearch || isDestinationFinderSearch) {
                z = true;
            } else {
                z = false;
            }
            this.isComingFromGoogleNow = z;
            if (isGoogleNowSearch) {
                com.navdy.client.app.framework.LocalyticsManager.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SEARCH_USING_GOOGLE_NOW);
            }
            if (isDestinationFinderSearch) {
                com.navdy.client.app.framework.LocalyticsManager.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SEARCH_USING_DESTINATION_FINDER);
            }
            this.logger.d("Search is coming from Google Now = " + this.isComingFromGoogleNow);
            if ((this.isComingFromGoogleNow || this.fromActionSendIntent) && intent.hasExtra("query")) {
                java.lang.String query = intent.getStringExtra("query");
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
                    showProgressDialog();
                    if (this.editText != null) {
                        this.ignoreNextKeystrokeKey.set(true);
                        this.editText.setText(query);
                    }
                    runTextSearchOrAskHud(query);
                }
            }
        }
        com.navdy.client.app.tracking.SetDestinationTracker.getInstance().setSourceValue("Search");
        showAutoComplete();
        findViewById(com.navdy.client.R.id.touchDetection).setOnTouchListener(new com.navdy.client.app.ui.search.SearchActivity.Anon3());
    }

    protected void onResume() {
        super.onResume();
        updateOfflineBannerVisibility();
        com.navdy.client.app.tracking.Tracker.tagScreen("Search");
    }

    public void onBackPressed() {
        if (this.editText == null || this.editText.getText().length() <= 0) {
            super.onBackPressed();
        } else {
            resetEditText();
        }
    }

    private void requestContactsPermission() {
        requestContactsPermission(null, new com.navdy.client.app.ui.search.SearchActivity.Anon4());
    }

    private void setupToolbarButtons() {
        android.widget.ImageButton backButton = (android.widget.ImageButton) findViewById(com.navdy.client.R.id.back_button);
        if (backButton != null) {
            backButton.setOnClickListener(new com.navdy.client.app.ui.search.SearchActivity.Anon5());
            android.widget.ImageButton rightButton = (android.widget.ImageButton) findViewById(com.navdy.client.R.id.right_button);
            if (rightButton != null) {
                rightButton.setOnClickListener(new com.navdy.client.app.ui.search.SearchActivity.Anon6());
            }
        }
    }

    private void resetEditText() {
        setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.EMPTY);
        this.editText.setText("");
    }

    private void updateRightButton() {
        android.widget.ImageButton rightButton = (android.widget.ImageButton) findViewById(com.navdy.client.R.id.right_button);
        if (rightButton != null) {
            rightButton.setImageResource(this.rightButtonIsMic ? com.navdy.client.R.drawable.icon_searchbar_mic : com.navdy.client.R.drawable.icon_searchbar_close);
        }
    }

    private void startVoiceRecognitionActivity() {
        android.content.Intent intent = new android.content.Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("calling_package", getClass().getPackage().getName());
        intent.putExtra("android.speech.extra.PROMPT", getString(com.navdy.client.R.string.search_or_say_address));
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.MAX_RESULTS", 5);
        try {
            startActivityForResult(intent, 42);
        } catch (java.lang.Exception e) {
            openMarketAppFor("com.google.android.googlequicksearchbox");
        }
    }

    private void checkServices() {
        android.content.Intent intent = getIntent();
        if (intent != null) {
            this.searchType = intent.getIntExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPE_EXTRA, com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.SEARCH.getCode());
            this.showServices = this.searchType == com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.SEARCH.getCode();
        }
    }

    private void setRecyclerView() {
        android.support.v7.widget.LinearLayoutManager layoutManager = new android.support.v7.widget.LinearLayoutManager(this);
        layoutManager.setOrientation(1);
        this.searchAdapter = new com.navdy.client.app.ui.search.SearchRecyclerAdapter(this.searchType, this.googleMapFragment, this.card);
        if (this.searchRecycler != null) {
            this.searchRecycler.setAdapter(this.searchAdapter);
        }
        new com.navdy.client.app.ui.search.SearchActivity.ServiceAndHistoryAsyncTask(this, null).execute(new java.lang.Void[0]);
        this.searchAdapter.setClickListener(this);
        if (this.searchRecycler != null) {
            this.searchRecycler.setLayoutManager(layoutManager);
        }
    }

    private void setHintText() {
        if (this.searchType == com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.SEARCH.getCode()) {
            this.editText.setHint(com.navdy.client.R.string.search_hint);
        } else if (this.searchType == com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.WORK.getCode()) {
            this.editText.setHint(com.navdy.client.R.string.search_hint_work);
        } else if (this.searchType == com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.HOME.getCode()) {
            this.editText.setHint(com.navdy.client.R.string.search_hint_home);
        } else if (this.searchType == com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.FAVORITE.getCode()) {
            this.editText.setHint(com.navdy.client.R.string.search_hint_favorite);
        }
    }

    private synchronized void buildAndConnectGoogleApiClient() {
        this.mGoogleApiClient = new com.google.android.gms.common.api.GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(com.google.android.gms.location.places.Places.GEO_DATA_API).addApi(com.google.android.gms.location.places.Places.PLACE_DETECTION_API).addApi(com.google.android.gms.location.LocationServices.API).build();
        this.mGoogleApiClient.connect();
    }

    private void getLatLngBounds() {
        this.logger.v("getLatLngBounds");
        if (this.lastLocation == null) {
            com.navdy.service.library.events.location.Coordinate coordinate = this.navdyLocationManager.getSmartStartCoordinates();
            if (coordinate != null) {
                this.lastLocation = new android.location.Location("");
                this.lastLocation.setLatitude(coordinate.latitude.doubleValue());
                this.lastLocation.setLongitude(coordinate.longitude.doubleValue());
            }
        }
        if (this.lastLocation != null) {
            com.google.android.gms.maps.model.LatLng latLng = new com.google.android.gms.maps.model.LatLng(this.lastLocation.getLatitude(), this.lastLocation.getLongitude());
            com.google.android.gms.maps.model.LatLng northEast = com.google.maps.android.SphericalUtil.computeOffset(com.google.maps.android.SphericalUtil.computeOffset(latLng, 1609.0d, 0.0d), 1609.0d, 90.0d);
            com.google.android.gms.maps.model.LatLng southWest = com.google.maps.android.SphericalUtil.computeOffset(com.google.maps.android.SphericalUtil.computeOffset(latLng, 1609.0d, 180.0d), 1609.0d, 270.0d);
            new com.google.android.gms.maps.model.LatLngBounds(new com.google.android.gms.maps.model.LatLng(southWest.latitude, southWest.longitude), new com.google.android.gms.maps.model.LatLng(northEast.latitude, northEast.longitude));
        }
    }

    private void clearAndApplyListOrDropDownLook() {
        clear();
        applyListOrDropDownLook();
    }

    private void applyListOrDropDownLook() {
        boolean isAutoComplete;
        int height = -1;
        int marginTop = 0;
        int marginLeft = 0;
        int marginRight = 0;
        int marginBottom = 0;
        if (this.searchAdapter == null || this.searchAdapter.currentMode != com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE) {
            isAutoComplete = false;
        } else {
            isAutoComplete = true;
        }
        if (isAutoComplete) {
            height = -2;
            marginTop = AUTO_COMPLETE_MARGIN_TOP;
            marginLeft = AUTO_COMPLETE_MARGIN_LEFT;
            marginRight = AUTO_COMPLETE_MARGIN_RIGHT;
            marginBottom = AUTO_COMPLETE_MARGIN_BOTTOM;
        }
        boolean canReachInternet = com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet();
        if (this.searchRecycler != null) {
            android.widget.RelativeLayout.LayoutParams layoutParams = new android.widget.RelativeLayout.LayoutParams(-1, height);
            if (canReachInternet) {
                layoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
            } else {
                layoutParams.setMargins(marginLeft, 0, marginRight, marginBottom);
            }
            layoutParams.addRule(3, com.navdy.client.R.id.offline_banner);
            this.searchRecycler.setLayoutParams(layoutParams);
            if (isAutoComplete) {
                this.searchRecycler.setBackgroundResource(com.navdy.client.R.drawable.autocomplete_bg);
            } else {
                this.searchRecycler.setBackgroundResource(com.navdy.client.R.color.white);
            }
            this.searchRecycler.scrollToPosition(0);
        }
        if (this.offlineBanner != null) {
            android.widget.RelativeLayout.LayoutParams layoutParams2 = new android.widget.RelativeLayout.LayoutParams(-1, -2);
            layoutParams2.setMargins(marginLeft, marginTop, marginRight, 0);
            layoutParams2.addRule(3, com.navdy.client.R.id.search_toolbar);
            this.offlineBanner.setLayoutParams(layoutParams2);
        }
        updatePoweredByGoogleHeaderVisibility(canReachInternet);
    }

    private void runAutoCompleteSearch(java.lang.String constraint) {
        if (constraint != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.search.SearchActivity.Anon7(constraint), 1);
        }
    }

    private void runOfflineAutocompleteSearch(java.lang.String constraint) {
        if (constraint != null) {
            new com.navdy.client.app.ui.search.SearchActivity.Anon8(constraint).execute(new java.lang.Void[0]);
        }
    }

    private void setEditTextListener() {
        try {
            this.editText = (android.widget.EditText) findViewById(com.navdy.client.R.id.search_edit_text);
            this.editText.addTextChangedListener(new com.navdy.client.app.ui.search.SearchActivity.Anon9());
            this.editText.setOnKeyListener(new com.navdy.client.app.ui.search.SearchActivity.Anon10());
        } catch (java.lang.Exception e) {
            this.logger.e((java.lang.Throwable) e);
        }
    }

    private boolean runTextSearchOrAskHud(java.lang.String query) {
        com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
            return true;
        }
        setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH);
        showProgressDialog();
        new com.navdy.client.app.ui.search.SearchActivity.Anon11(query).execute(new java.lang.Void[0]);
        new com.navdy.client.app.framework.search.NavdySearch(new com.navdy.client.app.ui.search.SearchActivity.Anon12(), true).runSearch(query);
        return false;
    }

    private void putSearchResultExtraAndFinishIfNoUiRequired(com.navdy.client.app.framework.search.SearchResults searchResults) {
        if (this.isComingFromGoogleNow || this.fromActionSendIntent) {
            com.navdy.client.app.framework.models.Destination firstDestination = searchResults.getFirstDestination();
            if (firstDestination != null) {
                putSearchResultExtraAndFinish(firstDestination);
            } else {
                this.logger.e("Unable to get search results for Google now or hud voice search query!");
            }
        }
    }

    private void updateRecyclerViewWithSearchResults(com.navdy.client.app.framework.search.SearchResults searchResults, boolean showNoResultsFoundDialog) {
        runOnUiThread(new com.navdy.client.app.ui.search.SearchActivity.Anon13(searchResults, showNoResultsFoundDialog));
    }

    private void addDistancesToDestinationsIfNeeded(com.navdy.client.app.framework.search.SearchResults searchResults) {
        java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleResults = searchResults.getGoogleResults();
        if ((googleResults != null && !googleResults.isEmpty()) && !this.isComingFromGoogleNow) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.search.SearchActivity.Anon14(googleResults), 1, com.navdy.service.library.task.TaskManager.TaskPriority.LOW);
        }
    }

    private void runServiceSearch(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceType) {
        if (serviceType != null) {
            setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH);
            if (com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
                this.rightButtonIsMic = false;
                updateRightButton();
                com.navdy.client.app.framework.search.NavdySearch.runServiceSearch(this.googlePlacesSearch, serviceType);
            } else if (com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected()) {
                new com.navdy.client.app.framework.search.NavdySearch(new com.navdy.client.app.ui.search.SearchActivity.Anon15(), true).runSearch(serviceType.getHudServiceType());
            } else {
                hideProgressDialog();
            }
        }
    }

    private void setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode newMode) {
        if (this.searchAdapter != null) {
            this.logger.d("Setting current mode to " + newMode.name());
            this.searchAdapter.currentMode = newMode;
            return;
        }
        this.logger.w("Unable to current mode to " + newMode.name() + " because the searchAdapter is null");
    }

    public void onGoogleSearchResult(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
        this.logger.v("onGoogleSearchResult: " + queryType);
        if (destinations == null) {
            destinations = new java.util.ArrayList<>(0);
        }
        com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker = com.navdy.client.app.tracking.SetDestinationTracker.getInstance();
        if (queryType == com.navdy.client.app.framework.search.GooglePlacesSearch.Query.TEXTSEARCH) {
            setDestinationTracker.setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.SEARCH_RESULT);
        }
        switch (queryType) {
            case NEARBY:
            case TEXTSEARCH:
                if (!destinations.isEmpty() && !this.isComingFromGoogleNow) {
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.search.SearchActivity.Anon16(destinations), 1, com.navdy.service.library.task.TaskManager.TaskPriority.LOW);
                }
                updateRecyclerViewForServiceSearch(destinations);
                this.searchAdapter.updateNearbySearchResultsWithKnownDestinationData();
                hideProgressDialog();
                return;
            case DETAILS:
                this.logger.v("successfully received detail callback from web service: " + destinations);
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                if (!destinations.isEmpty()) {
                    com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult result = (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) destinations.get(0);
                    if (result != null) {
                        result.toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.DETAILS, destination);
                        destination.persistPlaceDetailInfo();
                        destination.updateDestinationListsAsync();
                    }
                } else {
                    destination = this.requestedPlaceDetailsDestinationBackup;
                }
                if (destination != null) {
                    putSearchResultExtraAndFinish(destination);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void updateRecyclerViewForServiceSearch(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations) {
        runOnUiThread(new com.navdy.client.app.ui.search.SearchActivity.Anon17(destinations));
    }

    /* access modifiers changed from: private */
    @android.support.annotation.WorkerThread
    public void addDistancesToDestinations(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        this.logger.v("addDistancesToDestinations");
        if (destinations != null && destinations.size() > 0) {
            for (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destination : destinations) {
                try {
                    double destinationLat = java.lang.Double.parseDouble(destination.lat);
                    double destinationLng = java.lang.Double.parseDouble(destination.lng);
                    android.location.Location destinationLocation = new android.location.Location("");
                    destinationLocation.setLatitude(destinationLat);
                    destinationLocation.setLongitude(destinationLng);
                    if (this.lastLocation != null) {
                        destination.distance = (double) this.lastLocation.distanceTo(destinationLocation);
                    }
                } catch (java.lang.Exception e) {
                    this.logger.e((java.lang.Throwable) e);
                    throw e;
                }
            }
            this.logger.v("status of destinations: " + destinations);
        }
    }

    private void requestLocationUpdate() {
        createLocationRequest();
        this.logger.i("calling requestLocationUpdates from FusedLocationApi");
        android.content.Context context = getApplicationContext();
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 && android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            com.google.android.gms.location.LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (com.google.android.gms.location.LocationListener) this);
        }
        this.requestedLocationUpdates = true;
    }

    private void createLocationRequest() {
        this.logger.i("creating LocationRequest Object");
        this.mLocationRequest = com.google.android.gms.location.LocationRequest.create().setPriority(100).setInterval(10000).setFastestInterval(1000);
    }

    private void stopLocationUpdates() {
        if (this.requestedLocationUpdates) {
            com.google.android.gms.location.LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
            this.requestedLocationUpdates = false;
        }
    }

    public void onLocationChanged(android.location.Location location) {
        if (this.lastLocation == null) {
            this.logger.v("Location changed");
            this.lastLocation = location;
            stopLocationUpdates();
        }
    }

    public void onConnected(android.os.Bundle bundle) {
        this.logger.i("GoogleApiClient connected.");
        android.content.Context context = getApplicationContext();
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 && android.support.v4.app.ActivityCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.lastLocation = com.google.android.gms.location.LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        }
        if (this.lastLocation == null) {
            this.logger.e("lastLocation was null. Now requesting location update.");
            requestLocationUpdate();
        }
    }

    public void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult connectionResult) {
        this.logger.e("onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    public void onConnectionSuspended(int i) {
        this.logger.e("GoogleApiClient connection suspended.");
    }

    public void onClick(android.view.View v) {
        com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker = com.navdy.client.app.tracking.SetDestinationTracker.getInstance();
        int position = ((java.lang.Integer) v.getTag()).intValue();
        this.logger.v("item clicked at position: " + position);
        com.navdy.client.app.framework.models.Destination d = this.searchAdapter != null ? this.searchAdapter.getDestinationAt(position) : null;
        if (d == null) {
            this.logger.e("Unable to retrieve a destination from position: " + position);
        } else if (d.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.NO_RESULTS.getValue()) {
        } else {
            if (d.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_HISTORY.getValue()) {
                if (v.getId() == com.navdy.client.R.id.nav_button) {
                    new com.navdy.client.app.ui.search.SearchActivity.Anon18().execute(new java.lang.Integer[]{java.lang.Integer.valueOf(d.id)});
                    return;
                }
                this.ignoreAutoCompleteDelay.set(true);
                this.editText.setText(d.name);
                this.editText.setSelection(d.name.length());
                this.searchRecycler.requestFocus();
                new com.navdy.client.app.ui.search.SearchActivity.Anon19(d).execute(new java.lang.Void[0]);
            } else if (d.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.AUTOCOMPLETE.getValue() && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(d.placeId)) {
                setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH);
                this.ignoreNextKeystrokeKey.set(true);
                this.editText.setText(d.name);
                this.editText.setSelection(d.name.length());
                com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
                runTextSearchOrAskHud(d.name);
            } else if (d.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.MORE.getValue()) {
                runTextSearchOrAskHud(this.editText.getText().toString());
            } else if (d.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.CONTACT_ADDRESS_SELECTION.getValue()) {
                if (this.searchAdapter != null) {
                    com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem si = this.searchAdapter.getItemAt(position);
                    if (si != null && si.contact != null) {
                        setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH);
                        clearAndApplyListOrDropDownLook();
                        this.searchAdapter.updateRecyclerViewWithContact(si.contact);
                    }
                }
            } else if (v.getId() != com.navdy.client.R.id.nav_button && !isFavoriteSearch(this.searchType)) {
                android.content.Intent i = new android.content.Intent(this, com.navdy.client.app.ui.details.DetailsActivity.class);
                i.putExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT, d);
                i.putExtra(EXTRA_LOCATION, this.lastLocation);
                startActivityForResult(i, 0);
            } else if (!com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() && !hasDisplayedOfflineMessage && !isFavoriteSearch(this.searchType)) {
                createOfflineDialogForOnClick(v).show();
            } else if (d.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.AUTOCOMPLETE.getValue()) {
                setDestinationTracker.setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.SEARCH_AUTOCOMPLETE);
                setResultForAutocompleteAndFinish(d);
            } else if (d.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.CONTACT.getValue()) {
                setDestinationTracker.setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.SEARCH_CONTACT);
                setResultForContactAndFinish(d);
            } else {
                putSearchResultExtraAndFinish(d);
            }
        }
    }

    public void onServiceClick(android.view.View v) {
        this.logger.v("User clicked on a service search.");
        showProgressDialog();
        this.ignoreNextKeystrokeKey.set(true);
        com.navdy.client.app.tracking.SetDestinationTracker.getInstance().setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.SEARCH_QUICK);
        switch (v.getId()) {
            case com.navdy.client.R.id.service_gas_icon /*2131755884*/:
                this.editText.setText(com.navdy.client.R.string.gas);
                runServiceSearch(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.GAS);
                break;
            case com.navdy.client.R.id.service_food_icon /*2131755885*/:
                this.editText.setText(com.navdy.client.R.string.food);
                runServiceSearch(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.FOOD);
                break;
            case com.navdy.client.R.id.service_atm_icon /*2131755886*/:
                this.editText.setText(com.navdy.client.R.string.atm);
                runServiceSearch(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.ATM);
                break;
            case com.navdy.client.R.id.service_parking_icon /*2131755887*/:
                this.editText.setText(com.navdy.client.R.string.parking);
                runServiceSearch(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.PARKING);
                break;
            default:
                this.logger.e("Unknown service view in onServiceClick: " + v.getId());
                break;
        }
        this.editText.setSelection(this.editText.getText().length());
        com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
    }

    static boolean isFavoriteSearch(int searchType2) {
        return searchType2 == 1 || searchType2 == 2 || searchType2 == 3;
    }

    private void setResultForAutocompleteAndFinish(com.navdy.client.app.framework.models.Destination d) {
        showProgressDialog();
        this.requestedPlaceDetailsDestinationBackup = d;
        this.logger.d("Getting place details for: " + d);
        this.googlePlacesSearch.runDetailsSearchWebApi(d.placeId);
    }

    private android.app.Dialog createOfflineDialogForOnClick(android.view.View searchRow) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setCancelable(true);
        java.lang.String title = getString(com.navdy.client.R.string.offline_mode);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(title)) {
            builder.setTitle(title);
        }
        java.lang.String message = getString(com.navdy.client.R.string.offline_mode_explanation);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(message)) {
            builder.setMessage(message);
        }
        builder.setPositiveButton(getString(com.navdy.client.R.string.send_to_navdy), new com.navdy.client.app.ui.search.SearchActivity.Anon20(searchRow));
        builder.setNegativeButton(getString(com.navdy.client.R.string.cancel), null);
        return builder.create();
    }

    private void setResultForContactAndFinish(com.navdy.client.app.framework.models.Destination d) {
        showProgressDialog();
        new com.navdy.client.app.ui.search.SearchActivity.Anon21(d).execute(new java.lang.Void[0]);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.search.SearchActivity.Anon22(d), 3);
    }

    private void putSearchResultExtraAndFinish(com.navdy.client.app.framework.models.Destination destination) {
        showProgressDialog();
        new com.navdy.client.app.ui.search.SearchActivity.Anon23(destination).execute(new java.lang.Void[0]);
    }

    private void showNoResultsFoundDialogIfAllAreEmpty(java.util.List... lists) {
        if (lists == null || lists.length <= 0) {
            showNoResultsFoundDialog();
            return;
        }
        int length = lists.length;
        int i = 0;
        while (i < length) {
            java.util.List list = lists[i];
            if (list == null || list.isEmpty()) {
                i++;
            } else {
                return;
            }
        }
        showNoResultsFoundDialog();
    }

    private void showNoResultsFoundDialog() {
        if (!isActivityDestroyed()) {
            if (this.searchAdapter != null) {
                clear();
                setCurrentMode(com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE);
                this.searchAdapter.addSearchPoweredByGoogleHeader();
                this.searchAdapter.addNoResultsItem();
            }
            java.lang.String title = getString(com.navdy.client.R.string.no_results_found);
            if (this.isComingFromGoogleNow) {
                this.mAudioRouter.processTTSRequest(title, null, false);
            }
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setCancelable(true);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(title)) {
                builder.setTitle(title);
            }
            builder.setPositiveButton(getString(com.navdy.client.R.string.ok), new com.navdy.client.app.ui.search.SearchActivity.Anon24());
            builder.create().show();
        }
    }

    private void clear() {
        if (this.searchAdapter != null) {
            this.searchAdapter.clear();
        }
        if (this.searchRecycler != null) {
            this.searchRecycler.getRecycledViewPool().clear();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        if (resultCode == 1) {
            finish();
        } else if (requestCode == 42 && resultCode == -1) {
            java.util.ArrayList<java.lang.String> matches = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (matches == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim((java.lang.CharSequence) matches.get(0))) {
                showLongToast(com.navdy.client.R.string.unable_to_hear_any_search, new java.lang.Object[0]);
                return;
            }
            java.lang.String query = (java.lang.String) matches.get(0);
            setEditTextWithoutTriggeringAutocomplete(query);
            runTextSearchOrAskHud(query);
        }
    }

    private void setEditTextWithoutTriggeringAutocomplete(java.lang.String query) {
        this.editText.setText(query);
        this.editText.setSelection(query.length());
        this.handler.removeCallbacksAndMessages(null);
        this.rightButtonIsMic = false;
        updateRightButton();
    }

    public static void startSearchActivityFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES searchType2, boolean searchMic, android.app.Activity activity) {
        android.content.Intent i = new android.content.Intent(activity.getApplicationContext(), com.navdy.client.app.ui.search.SearchActivity.class);
        i.putExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPE_EXTRA, searchType2.getCode());
        i.putExtra(EXTRA_SPEECH_RECOGNITION, searchMic);
        activity.startActivityForResult(i, searchType2.getCode());
    }

    public void onFabClick(android.view.View view) {
        if (!this.isShowingMap) {
            showMap();
        } else {
            showList();
        }
    }

    private void showMap() {
        this.isShowingMap = true;
        unselectCurrentMarker();
        com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(this);
        this.previousSelectedMarker = null;
        com.navdy.client.app.ui.base.BaseSupportFragment.showThisFragment(this.googleMapFragment, this);
        this.centerMap.setVisibility(View.VISIBLE);
        this.fab.setVisibility(View.VISIBLE);
        this.fab.setImageResource(com.navdy.client.R.drawable.icon_fab_search_list);
        this.searchRecycler.setVisibility(View.GONE);
        this.searchAdapter.centerMapOnMarkers(false);
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new com.navdy.client.app.ui.search.SearchActivity.Anon25());
        }
        showInstructionalCard();
    }

    private void unselectCurrentMarker() {
        updatePin(this.previousSelectedMarker, false);
        this.previousSelectedMarker = null;
        showInstructionalCard();
    }

    private void selectThisMarker(@android.support.annotation.Nullable com.google.android.gms.maps.model.Marker marker) {
        java.lang.Integer destinationIndex = updatePin(marker, true);
        this.previousSelectedMarker = marker;
        showCardFor(destinationIndex);
    }

    @android.support.annotation.Nullable
    private java.lang.Integer updatePin(@android.support.annotation.Nullable com.google.android.gms.maps.model.Marker marker, boolean isSelected) {
        float f;
        int pinAsset;
        if (marker == null) {
            return null;
        }
        java.lang.Integer destinationIndex = null;
        if (this.searchAdapter != null) {
            destinationIndex = this.searchAdapter.getDestinationIndexForMarker(marker.getId());
            if (destinationIndex == null) {
                return null;
            }
            com.navdy.client.app.framework.models.Destination destination = this.searchAdapter.getDestinationAt(destinationIndex.intValue());
            if (destination != null) {
                if (isSelected) {
                    pinAsset = destination.getPinAsset();
                } else {
                    pinAsset = destination.getUnselectedPinAsset();
                }
                com.navdy.client.app.framework.map.MapUtils.setMarkerImageResource(marker, pinAsset);
            }
        }
        if (isSelected) {
            f = 1.0f;
        } else {
            f = 0.0f;
        }
        marker.setZIndex(f);
        return destinationIndex;
    }

    private void showAutoComplete() {
        hideProgressDialog();
        showList();
        this.fab.setVisibility(View.GONE);
    }

    private void showList() {
        showList(true);
    }

    private void showList(boolean showMapFab) {
        int i = 0;
        this.isShowingMap = false;
        this.searchRecycler.setVisibility(View.VISIBLE);
        this.fab.setImageResource(com.navdy.client.R.drawable.icon_fab_search_map);
        android.widget.ImageButton imageButton = this.fab;
        if (!showMapFab) {
            i = 8;
        }
        imageButton.setVisibility(i);
        this.editText.requestFocus();
        com.navdy.client.app.ui.base.BaseSupportFragment.hideThisFragment(this.googleMapFragment, this);
        this.centerMap.setVisibility(View.GONE);
        hideCard();
    }

    public void centerOnDriver(android.view.View view) {
        this.searchAdapter.centerMapOnMarkers(true);
    }

    private void showCardFor(@android.support.annotation.Nullable java.lang.Integer destinationIndex) {
        if (destinationIndex != null && destinationIndex.intValue() >= 0) {
            this.card.setVisibility(View.VISIBLE);
            this.searchAdapter.setCardData(new com.navdy.client.app.ui.search.SearchViewHolder(this.card), destinationIndex.intValue());
        }
    }

    private void hideCard() {
        this.card.setVisibility(View.GONE);
    }

    private void showInstructionalCard() {
        com.navdy.client.app.ui.customviews.DestinationImageView icon = (com.navdy.client.app.ui.customviews.DestinationImageView) this.card.findViewById(com.navdy.client.R.id.search_row_image);
        android.view.View row = this.card.findViewById(com.navdy.client.R.id.search_row);
        android.view.View navBtn = this.card.findViewById(com.navdy.client.R.id.nav_button);
        android.widget.TextView title = (android.widget.TextView) this.card.findViewById(com.navdy.client.R.id.search_row_title);
        android.widget.TextView details = (android.widget.TextView) this.card.findViewById(com.navdy.client.R.id.search_row_details);
        android.view.View price = this.card.findViewById(com.navdy.client.R.id.search_row_price);
        android.view.View distance = this.card.findViewById(com.navdy.client.R.id.search_row_distance);
        this.card.setVisibility(View.VISIBLE);
        row.setOnClickListener(null);
        icon.setImageResource(com.navdy.client.R.drawable.icon_badge_pick);
        navBtn.setVisibility(View.GONE);
        title.setText(com.navdy.client.R.string.pick_a_result);
        details.setText(com.navdy.client.R.string.tap_a_pin);
        distance.setVisibility(View.GONE);
        price.setVisibility(View.GONE);
    }

    @com.squareup.otto.Subscribe
    public void handleReachabilityStateChange(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            this.logger.v("reachabilityEvent: " + reachabilityEvent.isReachable);
            if (this.editText != null && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.editText.getText())) {
                this.searchAdapter.notifyDataSetChanged();
            }
            updateOfflineBannerVisibility();
            applyListOrDropDownLook();
        }
    }

    public void updateOfflineBannerVisibility() {
        boolean canReachInternet = com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet();
        this.offlineBanner.setVisibility(canReachInternet ? 8 : 0);
        updatePoweredByGoogleHeaderVisibility(canReachInternet);
    }

    public void updatePoweredByGoogleHeaderVisibility(boolean canReachInternet) {
        if (this.searchAdapter == null) {
            return;
        }
        if (canReachInternet) {
            this.searchAdapter.addSearchPoweredByGoogleHeader();
        } else {
            this.searchAdapter.removeSearchPoweredByGoogleHeader();
        }
    }

    public void onRefreshConnectivityClick(android.view.View view) {
        com.navdy.client.app.framework.AppInstance.getInstance().checkForNetwork();
    }
}
