package com.navdy.client.app.ui.firstlaunch;

public class CarInfoActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    public static final int DEFAULT_OBD_LOCATION = (obdLocations.length - 1);
    private static final int[] obdLocations = {com.navdy.client.R.drawable.obd_port_location_1, com.navdy.client.R.drawable.obd_port_location_2, com.navdy.client.R.drawable.obd_port_location_3, com.navdy.client.R.drawable.obd_port_location_4, com.navdy.client.R.drawable.obd_port_location_5, com.navdy.client.R.drawable.obd_port_location_6, com.navdy.client.R.drawable.obd_port_location_7, com.navdy.client.R.drawable.obd_port_location_8, com.navdy.client.R.drawable.obd_port_location_9, com.navdy.client.R.drawable.img_obd_location_unknown};
    private java.lang.String nextStep;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_vehicle_obd_locator);
        android.widget.TextView yearMakeModel = (android.widget.TextView) findViewById(com.navdy.client.R.id.subtitle);
        android.widget.ImageView obdLocation = (android.widget.ImageView) findViewById(com.navdy.client.R.id.obd_location);
        android.widget.ImageView obdCrimePhoto = (android.widget.ImageView) findViewById(com.navdy.client.R.id.obd_crime_photo);
        android.widget.TextView help = (android.widget.TextView) findViewById(com.navdy.client.R.id.help);
        android.widget.Button done = (android.widget.Button) findViewById(com.navdy.client.R.id.done);
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        java.lang.String makeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String yearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String modelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        int obdLocationNumber = customerPrefs.getInt(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NUM, DEFAULT_OBD_LOCATION);
        android.text.Spanned helpString = com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.need_help_contact_support);
        this.nextStep = getIntent().getStringExtra("extra_next_step");
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yearString) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(makeString) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(modelString)) {
            startEditInfoActivity(null);
            return;
        }
        if (yearMakeModel != null) {
            yearMakeModel.setText(com.navdy.client.app.ui.settings.SettingsUtils.getCarYearMakeModelString());
        }
        if (obdLocation != null) {
            if (obdLocationNumber < 0 || obdLocationNumber >= obdLocations.length) {
                obdLocation.setImageResource(obdLocations[DEFAULT_OBD_LOCATION]);
            } else {
                obdLocation.setImageResource(obdLocations[obdLocationNumber]);
            }
        }
        android.graphics.Bitmap crimePhoto = com.navdy.client.app.tracking.Tracker.getObdImage();
        if (obdCrimePhoto != null) {
            if (crimePhoto != null) {
                obdCrimePhoto.setImageBitmap(crimePhoto);
            } else {
                obdCrimePhoto.setImageResource(com.navdy.client.R.drawable.image_generic_port);
            }
        }
        if (help != null) {
            help.setText(helpString);
        }
        com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder toolbarBuilder = new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder();
        toolbarBuilder.title((int) com.navdy.client.R.string.obd2_port_locator).build();
        if (done == null) {
            return;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.nextStep, "installation_flow") || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.nextStep, "app_setup")) {
            done.setText(com.navdy.client.R.string.next);
            done.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, com.navdy.client.R.drawable.icon_installaton_next, 0);
            return;
        }
        done.setText(com.navdy.client.R.string.done);
        done.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
    }

    private java.lang.String getLocalizedLocationDescription(java.lang.String accessNote, java.lang.String note) {
        java.lang.String obdLocationString;
        java.lang.String accessNote2 = com.navdy.client.app.ui.firstlaunch.CarMdUtils.getLocalizedAccessNote(accessNote);
        java.lang.String note2 = com.navdy.client.app.ui.firstlaunch.CarMdUtils.getLocalizedNote(note);
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(accessNote2)) {
            obdLocationString = note2;
        } else {
            obdLocationString = context.getString(com.navdy.client.R.string.text_with_parentheses, new java.lang.Object[]{note2, accessNote2});
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(obdLocationString)) {
            return context.getString(com.navdy.client.R.string.obd_location_unknown);
        }
        return obdLocationString;
    }

    protected void onResume() {
        super.onResume();
        hideSystemUiDependingOnOrientation();
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        java.lang.String obdLocationString = getLocalizedLocationDescription(customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE, ""), customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NOTE, ""));
        android.widget.TextView obdLocationDesc = (android.widget.TextView) findViewById(com.navdy.client.R.id.obd_locator_description);
        if (obdLocationDesc != null) {
            obdLocationDesc.setText(obdLocationString);
        }
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.CAR_INFO);
    }

    protected void onPause() {
        com.navdy.client.app.framework.FlashlightManager.getInstance().turnFlashLightOff();
        super.onPause();
    }

    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 2) {
            hideSystemUI();
        } else if (newConfig.orientation == 1) {
            showSystemUI();
        }
    }

    public void startEditInfoActivity(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.class));
        finish();
    }

    public void onFlashlightClick(android.view.View view) {
        com.navdy.client.app.framework.FlashlightManager.getInstance().switchLight(this);
    }

    public void onHelpClick(android.view.View view) {
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.ContactUsActivity.class);
        intent.putExtra(com.navdy.client.app.ui.settings.ContactUsActivity.EXTRA_DEFAULT_PROBLEM_TYPE, 1);
        startActivity(intent);
    }

    public void onBackClick(android.view.View view) {
        finish();
    }

    public void onDoneClick(android.view.View view) {
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.nextStep, "installation_flow")) {
            android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
            intent.putExtra("extra_step", com.navdy.client.R.layout.fle_install_tidying_up);
            startActivity(intent);
            return;
        }
        finish();
    }

    public void onCrimePhotoClick(android.view.View view) {
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.PhotoViewerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.PhotoViewerActivity.EXTRA_FILE_PATH, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.OBD_IMAGE_FILE_NAME);
        startActivity(intent);
    }

    public void onWatchVideoClick(android.view.View view) {
        com.navdy.client.app.ui.firstlaunch.InstallActivity.startVideoPlayerForThisStep(com.navdy.client.R.layout.fle_install_locate_obd, this);
    }
}
