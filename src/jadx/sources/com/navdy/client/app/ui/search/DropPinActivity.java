package com.navdy.client.app.ui.search;

public class DropPinActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener {
    private static final int ADDRESS_LOOKUP_DELAY = 500;
    public static final int NB_CAMERA_CHANGE_B4_SHOW_ADDRESS = 0;
    private static final int SHOW_DETAILS_SCREEN = 42;
    private android.view.View cardRow;
    private com.navdy.client.app.framework.models.Destination currentDestination = new com.navdy.client.app.framework.models.Destination();
    private com.navdy.client.app.ui.customviews.UnitSystemTextView distanceText;
    private android.widget.ImageView dropPin;
    private android.widget.TextView firstLine;
    private com.navdy.client.app.ui.base.BaseGoogleMapFragment googleMapFragment;
    private android.widget.ImageView illustration;
    private int mapHasMoved = 0;
    private android.widget.ImageView navButton;
    private final com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    private final android.view.View.OnClickListener onCardViewClick = new com.navdy.client.app.ui.search.DropPinActivity.Anon1();
    private android.widget.TextView secondLine;

    class Anon1 implements android.view.View.OnClickListener {

        /* renamed from: com.navdy.client.app.ui.search.DropPinActivity$Anon1$Anon1 reason: collision with other inner class name */
        class C0065Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
            C0065Anon1() {
            }

            /* access modifiers changed from: protected */
            public java.lang.Void doInBackground(java.lang.Void... params) {
                com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.handleNewCoordsAndAddress(com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.displayLat, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.displayLong, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.navigationLat, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.navigationLong, null, com.navdy.client.app.framework.models.Destination.Precision.PRECISE);
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(java.lang.Void aVoid) {
                android.content.Intent i = new android.content.Intent(com.navdy.client.app.ui.search.DropPinActivity.this.getApplicationContext(), com.navdy.client.app.ui.details.DetailsActivity.class);
                i.putExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination);
                com.navdy.client.app.ui.search.DropPinActivity.this.startActivityForResult(i, 42);
            }
        }

        Anon1() {
        }

        public void onClick(android.view.View v) {
            new com.navdy.client.app.ui.search.DropPinActivity.Anon1.C0065Anon1().execute(new java.lang.Void[0]);
        }
    }

    class Anon2 implements android.view.View.OnClickListener {

        class Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
            Anon1() {
            }

            /* access modifiers changed from: protected */
            public java.lang.Void doInBackground(java.lang.Void... params) {
                com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.handleNewCoordsAndAddress(com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.displayLat, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.displayLong, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.navigationLat, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.navigationLong, null, com.navdy.client.app.framework.models.Destination.Precision.PRECISE);
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(java.lang.Void aVoid) {
                com.navdy.client.app.ui.search.DropPinActivity.this.navdyRouteHandler.requestNewRoute(com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination);
            }
        }

        Anon2() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.tracking.SetDestinationTracker.getInstance().tagSetDestinationEvent(com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination);
            new com.navdy.client.app.ui.search.DropPinActivity.Anon2.Anon1().execute(new java.lang.Void[0]);
        }
    }

    class Anon3 implements com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnShowMapListener {
        Anon3() {
        }

        public void onShow() {
            com.navdy.client.app.ui.search.DropPinActivity.this.setUpPinDrop();
        }
    }

    class Anon4 implements android.view.View.OnTouchListener {
        Anon4() {
        }

        public boolean onTouch(android.view.View v, android.view.MotionEvent event) {
            if (event.getAction() == 0) {
                com.navdy.client.app.ui.search.DropPinActivity.this.showDroppedPin();
            }
            return false;
        }
    }

    class Anon5 implements com.google.android.gms.maps.OnMapReadyCallback {

        class Anon1 implements com.google.android.gms.maps.GoogleMap.OnMapClickListener {
            Anon1() {
            }

            public void onMapClick(com.google.android.gms.maps.model.LatLng latLng) {
                if (latLng != null) {
                    com.navdy.client.app.ui.search.DropPinActivity.this.googleMapFragment.moveMap(new com.google.android.gms.maps.model.LatLng(latLng.latitude, latLng.longitude), 14.0f, true);
                }
            }
        }

        class Anon2 implements com.google.android.gms.maps.GoogleMap.OnCameraChangeListener {

            class Anon1 implements java.lang.Runnable {
                final /* synthetic */ com.google.android.gms.maps.model.CameraPosition val$cameraPosition;

                /* renamed from: com.navdy.client.app.ui.search.DropPinActivity$Anon5$Anon2$Anon1$Anon1 reason: collision with other inner class name */
                class C0066Anon1 implements java.lang.Runnable {
                    final /* synthetic */ com.google.android.gms.maps.model.LatLng val$center;

                    C0066Anon1(com.google.android.gms.maps.model.LatLng latLng) {
                        this.val$center = latLng;
                    }

                    public void run() {
                        if (!com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.hasDetailedAddress()) {
                            if (com.navdy.client.app.ui.search.DropPinActivity.this.firstLine != null) {
                                com.navdy.client.app.ui.search.DropPinActivity.this.firstLine.setText(com.navdy.client.R.string.dropped_pin);
                            }
                            if (com.navdy.client.app.ui.search.DropPinActivity.this.secondLine != null) {
                                com.navdy.client.app.ui.search.DropPinActivity.this.secondLine.setText(com.navdy.client.app.ui.search.DropPinActivity.this.getString(com.navdy.client.R.string.lat_long, new java.lang.Object[]{java.lang.String.valueOf(this.val$center.latitude), java.lang.String.valueOf(this.val$center.longitude)}));
                            }
                        } else {
                            android.util.Pair<java.lang.String, java.lang.String> splitAddress = com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.getSplitAddress();
                            if (com.navdy.client.app.ui.search.DropPinActivity.this.firstLine != null) {
                                com.navdy.client.app.ui.search.DropPinActivity.this.firstLine.setText((java.lang.CharSequence) splitAddress.first);
                            }
                            if (com.navdy.client.app.ui.search.DropPinActivity.this.secondLine != null) {
                                com.navdy.client.app.ui.search.DropPinActivity.this.secondLine.setText((java.lang.CharSequence) splitAddress.second);
                            }
                        }
                        com.navdy.service.library.events.location.Coordinate userLocation = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates();
                        if (!(com.navdy.client.app.ui.search.DropPinActivity.this.distanceText == null || userLocation == null)) {
                            com.navdy.client.app.ui.search.DropPinActivity.this.distanceText.setDistance((double) com.navdy.client.app.framework.map.MapUtils.distanceBetween(userLocation, this.val$center));
                            com.navdy.client.app.ui.search.DropPinActivity.this.distanceText.setVisibility(View.VISIBLE);
                        }
                        com.navdy.client.app.ui.search.DropPinActivity.this.navButton.setVisibility(View.VISIBLE);
                        com.navdy.client.app.ui.search.DropPinActivity.this.cardRow.setOnClickListener(com.navdy.client.app.ui.search.DropPinActivity.this.onCardViewClick);
                    }
                }

                Anon1(com.google.android.gms.maps.model.CameraPosition cameraPosition) {
                    this.val$cameraPosition = cameraPosition;
                }

                public void run() {
                    com.google.android.gms.maps.model.LatLng center = this.val$cameraPosition.target;
                    java.lang.Runnable reverseGeoCode = new com.navdy.client.app.ui.search.DropPinActivity.Anon5.Anon2.Anon1.C0066Anon1(center);
                    com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination = new com.navdy.client.app.framework.models.Destination();
                    com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination.setCoordsToSame(center);
                    com.navdy.client.app.framework.map.MapUtils.doReverseGeocodingFor(center, com.navdy.client.app.ui.search.DropPinActivity.this.currentDestination, reverseGeoCode);
                }
            }

            Anon2() {
            }

            public void onCameraChange(com.google.android.gms.maps.model.CameraPosition cameraPosition) {
                com.navdy.client.app.ui.search.DropPinActivity.this.handler.removeCallbacksAndMessages(null);
                if (com.navdy.client.app.ui.search.DropPinActivity.this.mapHasMoved < 0) {
                    com.navdy.client.app.ui.search.DropPinActivity.this.mapHasMoved = com.navdy.client.app.ui.search.DropPinActivity.this.mapHasMoved + 1;
                    return;
                }
                int delay = 500;
                if (com.navdy.client.app.ui.search.DropPinActivity.this.mapHasMoved == 0) {
                    com.navdy.client.app.ui.search.DropPinActivity.this.mapHasMoved = com.navdy.client.app.ui.search.DropPinActivity.this.mapHasMoved + 1;
                    delay = 0;
                    if (com.navdy.client.app.ui.search.DropPinActivity.this.dropPin != null) {
                        android.view.animation.TranslateAnimation animation = new android.view.animation.TranslateAnimation(0.0f, 0.0f, -50.0f, 0.0f);
                        animation.setDuration(200);
                        animation.setInterpolator(new android.view.animation.DecelerateInterpolator());
                        com.navdy.client.app.ui.search.DropPinActivity.this.dropPin.startAnimation(animation);
                        com.navdy.client.app.ui.search.DropPinActivity.this.dropPin.setVisibility(View.VISIBLE);
                    }
                    if (com.navdy.client.app.ui.search.DropPinActivity.this.illustration != null) {
                        com.navdy.client.app.ui.search.DropPinActivity.this.illustration.setImageResource(com.navdy.client.R.drawable.icon_badge_place);
                    }
                    if (com.navdy.client.app.ui.search.DropPinActivity.this.navButton != null) {
                        com.navdy.client.app.ui.search.DropPinActivity.this.navButton.setVisibility(View.VISIBLE);
                    }
                }
                com.navdy.client.app.ui.search.DropPinActivity.this.showUpdatingLocation();
                com.navdy.client.app.ui.search.DropPinActivity.this.handler.postDelayed(new com.navdy.client.app.ui.search.DropPinActivity.Anon5.Anon2.Anon1(cameraPosition), (long) delay);
            }
        }

        Anon5() {
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            googleMap.setOnMapClickListener(new com.navdy.client.app.ui.search.DropPinActivity.Anon5.Anon1());
            googleMap.setOnCameraChangeListener(new com.navdy.client.app.ui.search.DropPinActivity.Anon5.Anon2());
        }
    }

    public void centerOnMyLocation(android.view.View view) {
        com.navdy.service.library.events.location.Coordinate coordinate = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (coordinate != null && this.googleMapFragment != null) {
            this.googleMapFragment.moveMap(new com.google.android.gms.maps.model.LatLng(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue()), 14.0f, true);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 42 && resultCode == 1) {
            finish();
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.drop_pin_activity);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.pick_a_place).build();
        this.dropPin = (android.widget.ImageView) findViewById(com.navdy.client.R.id.drop_pin);
        this.illustration = (android.widget.ImageView) findViewById(com.navdy.client.R.id.illustration);
        this.firstLine = (android.widget.TextView) findViewById(com.navdy.client.R.id.card_first_line);
        this.secondLine = (android.widget.TextView) findViewById(com.navdy.client.R.id.card_second_line);
        this.navButton = (android.widget.ImageView) findViewById(com.navdy.client.R.id.nav_button);
        this.cardRow = findViewById(com.navdy.client.R.id.card_row);
        this.distanceText = (com.navdy.client.app.ui.customviews.UnitSystemTextView) findViewById(com.navdy.client.R.id.card_sub_illustration_text);
        android.widget.TextView thirdLine = (android.widget.TextView) findViewById(com.navdy.client.R.id.card_third_line);
        if (this.dropPin != null) {
            this.dropPin.setVisibility(View.GONE);
        }
        if (this.illustration != null) {
            this.illustration.setImageResource(com.navdy.client.R.drawable.icon_pin_plain_grey_sm);
        }
        if (this.firstLine != null) {
            this.firstLine.setText(com.navdy.client.R.string.pick_a_place);
        }
        if (this.secondLine != null) {
            this.secondLine.setText(com.navdy.client.R.string.drag_to_select_place);
        }
        if (thirdLine != null) {
            thirdLine.setVisibility(View.GONE);
        }
        if (this.navButton != null) {
            this.navButton.setVisibility(View.GONE);
            this.navButton.setImageResource(com.navdy.client.R.drawable.button_send_route);
            this.navButton.setOnClickListener(new com.navdy.client.app.ui.search.DropPinActivity.Anon2());
        }
        if (this.cardRow != null) {
            this.cardRow.setOnClickListener(this.onCardViewClick);
        }
        this.googleMapFragment = (com.navdy.client.app.ui.base.BaseGoogleMapFragment) getFragmentManager().findFragmentById(com.navdy.client.R.id.map);
        if (this.googleMapFragment != null) {
            this.googleMapFragment.hide();
            com.navdy.service.library.events.location.Coordinate coordinate = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates();
            if (coordinate != null) {
                this.googleMapFragment.moveMap(new com.google.android.gms.maps.model.LatLng(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue()), 14.0f, false);
            }
        }
        com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker = com.navdy.client.app.tracking.SetDestinationTracker.getInstance();
        setDestinationTracker.setSourceValue("Drop_Pin");
        setDestinationTracker.setDestinationType("Drop_Pin");
    }

    protected void onResume() {
        super.onResume();
        updateOfflineBannerVisibility();
        if (this.googleMapFragment != null) {
            this.googleMapFragment.show(new com.navdy.client.app.ui.search.DropPinActivity.Anon3());
        }
        this.navdyRouteHandler.addListener(this);
    }

    protected void onPause() {
        this.navdyRouteHandler.removeListener(this);
        super.onPause();
    }

    public void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        finish();
    }

    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
        finish();
    }

    public void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        com.navdy.client.app.ui.routing.RoutingActivity.startRoutingActivity(this);
        finish();
    }

    public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        if (error == com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE) {
            finish();
        }
    }

    public void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        finish();
    }

    public void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo progress) {
        finish();
    }

    public void onReroute() {
        finish();
    }

    public void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    public void onStopRoute() {
    }

    private void setUpPinDrop() {
        android.view.View container = findViewById(com.navdy.client.R.id.map_touch_listener);
        if (container != null) {
            container.setOnTouchListener(new com.navdy.client.app.ui.search.DropPinActivity.Anon4());
        }
        if (this.googleMapFragment == null) {
            this.logger.e("Calling setUpPinDrop with a null googleMapFragment");
        } else {
            this.googleMapFragment.getMapAsync(new com.navdy.client.app.ui.search.DropPinActivity.Anon5());
        }
    }

    private void showDroppedPin() {
        showThis(com.navdy.client.R.string.dropped_pin);
    }

    private void showUpdatingLocation() {
        showThis(com.navdy.client.R.string.updating_location);
    }

    private void showThis(@android.support.annotation.StringRes int stringRes) {
        this.firstLine.setText(stringRes);
        this.secondLine.setText("");
        this.navButton.setVisibility(View.GONE);
        this.cardRow.setOnClickListener(null);
        this.distanceText.setVisibility(View.INVISIBLE);
    }

    @com.squareup.otto.Subscribe
    public void handleReachabilityStateChange(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        android.view.View offlineBanner = findViewById(com.navdy.client.R.id.offline_banner);
        if (offlineBanner != null) {
            offlineBanner.setVisibility(com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() ? 8 : 0);
        }
    }

    public void onRefreshConnectivityClick(android.view.View view) {
        com.navdy.client.app.framework.AppInstance.getInstance().checkForNetwork();
    }
}
