package com.navdy.client.app.ui.favorites;

public class FavoritesEditActivity extends com.navdy.client.app.ui.base.BaseEditActivity {
    public static final java.lang.String EXTRA_FAVORITE = "favorite";
    private com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
    private com.navdy.client.app.ui.base.BaseGoogleMapFragment googleMapFragment;
    private android.widget.EditText label;
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private android.support.v7.widget.Toolbar myToolbar;
    private android.widget.RelativeLayout offlineBanner;

    class Anon1 implements android.text.TextWatcher {
        Anon1() {
        }

        public void beforeTextChanged(java.lang.CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) {
            if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.getFavoriteLabel(), s)) {
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.somethingChanged = true;
            }
        }

        public void afterTextChanged(android.text.Editable s) {
        }
    }

    class Anon2 implements com.google.android.gms.maps.OnMapReadyCallback {
        Anon2() {
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            com.google.android.gms.maps.model.LatLng position = null;
            if (com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination != null) {
                if (com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.hasValidDisplayCoordinates()) {
                    position = new com.google.android.gms.maps.model.LatLng(com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.displayLat, com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.displayLong);
                } else if (com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.hasValidNavCoordinates()) {
                    position = new com.google.android.gms.maps.model.LatLng(com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.navigationLat, com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.navigationLong);
                }
            }
            if (position != null) {
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.googleMapFragment.moveMap(position, 14.0f, false);
                int pinAsset = com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.getPinAsset();
                com.google.android.gms.maps.model.MarkerOptions markerOptions = new com.google.android.gms.maps.model.MarkerOptions();
                markerOptions.title(com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.getFavoriteLabel());
                markerOptions.draggable(false);
                markerOptions.position(position);
                markerOptions.icon(com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource(pinAsset));
                googleMap.addMarker(markerOptions);
                android.location.Location location = new android.location.Location("");
                location.setLatitude(position.latitude);
                location.setLongitude(position.longitude);
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.googleMapFragment.moveMap(location, 14.0f, false);
                return;
            }
            com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.googleMapFragment.centerOnLastKnownLocation(false);
        }
    }

    class Anon3 implements java.lang.Runnable {

        class Anon1 implements com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback {

            /* renamed from: com.navdy.client.app.ui.favorites.FavoritesEditActivity$Anon3$Anon1$Anon1 reason: collision with other inner class name */
            class C0056Anon1 implements java.lang.Runnable {
                C0056Anon1() {
                }

                public void run() {
                    com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.toast_favorite_delete_error, new java.lang.Object[0]);
                }
            }

            Anon1() {
            }

            public void onQueryCompleted(int nbRows, @android.support.annotation.Nullable android.net.Uri uri) {
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.hideProgressDialog();
                if (nbRows <= 0) {
                    com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.runOnUiThread(new com.navdy.client.app.ui.favorites.FavoritesEditActivity.Anon3.Anon1.C0056Anon1());
                }
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.saveOnExit = false;
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.finish();
            }
        }

        Anon3() {
        }

        public void run() {
            com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.deleteFavoriteFromDbAsync(new com.navdy.client.app.ui.favorites.FavoritesEditActivity.Anon3.Anon1());
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            if (com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.id <= 0) {
                com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.logger.e("Missing favorite id. Currently unable to save.");
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.toast_favorite_edit_error, new java.lang.Object[0]);
                return;
            }
            com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.saveChanges();
        }
    }

    class Anon5 extends android.os.AsyncTask<java.lang.Object, java.lang.Object, java.lang.Boolean> {
        final /* synthetic */ java.lang.String val$labelString;

        Anon5(java.lang.String str) {
            this.val$labelString = str;
        }

        /* access modifiers changed from: protected */
        public java.lang.Boolean doInBackground(java.lang.Object... params) {
            boolean success = false;
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.val$labelString)) {
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.getFavoriteLabel(), this.val$labelString)) {
                    cancel(true);
                } else {
                    com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.setFavoriteLabel(this.val$labelString);
                    success = com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.updateOnlyFavoriteFieldsInDb() > 0;
                }
            }
            return java.lang.Boolean.valueOf(success);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Boolean success) {
            if (com.navdy.client.app.ui.favorites.FavoritesEditActivity.this.destination.favoriteType != -1) {
                return;
            }
            if (success.booleanValue()) {
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.toast_favorite_edit_saved, new java.lang.Object[0]);
            } else {
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.toast_favorite_edit_error, new java.lang.Object[0]);
            }
        }
    }

    public static void startFavoriteEditActivity(android.app.Activity activity, android.os.Parcelable favorite) {
        android.content.Intent intent = new android.content.Intent(activity.getApplicationContext(), com.navdy.client.app.ui.favorites.FavoritesEditActivity.class);
        intent.putExtra(EXTRA_FAVORITE, favorite);
        activity.startActivity(intent);
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.favorites_edit_activity);
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.myToolbar = new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.edit_favorite).build();
        this.label = (android.widget.EditText) findViewById(com.navdy.client.R.id.label_input);
        android.widget.TextView address = (android.widget.TextView) findViewById(com.navdy.client.R.id.address_input);
        this.offlineBanner = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.offline_banner);
        this.googleMapFragment = (com.navdy.client.app.ui.base.BaseGoogleMapFragment) getFragmentManager().findFragmentById(com.navdy.client.R.id.map);
        android.content.Intent in = getIntent();
        if (in != null) {
            this.destination = (com.navdy.client.app.framework.models.Destination) in.getParcelableExtra(EXTRA_FAVORITE);
        }
        if (!(address == null || this.destination == null)) {
            address.setText(com.navdy.client.app.framework.util.StringUtils.join(this.destination.getAddressLines(), "\n"));
        }
        if (!(this.label == null || this.destination == null)) {
            if (this.destination.favoriteType == -3 || this.destination.favoriteType == -2) {
                android.widget.TextView labelText = (android.widget.TextView) findViewById(com.navdy.client.R.id.label_text);
                if (labelText != null) {
                    this.label.setVisibility(View.INVISIBLE);
                    if (this.destination.favoriteType == -3) {
                        labelText.setText(com.navdy.client.R.string.home);
                    } else {
                        labelText.setText(com.navdy.client.R.string.work);
                    }
                    labelText.setVisibility(View.VISIBLE);
                }
            } else {
                this.label.setText(this.destination.getFavoriteLabel());
                this.label.addTextChangedListener(new com.navdy.client.app.ui.favorites.FavoritesEditActivity.Anon1());
            }
        }
        if (!com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
            this.offlineBanner.setVisibility(View.VISIBLE);
        }
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new com.navdy.client.app.ui.favorites.FavoritesEditActivity.Anon2());
        }
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.EDIT_FAVORITES);
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.BluetoothPairActivity.class).putExtra(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
        this.myToolbar.getMenu().add(0, com.navdy.client.R.id.menu_delete, 0, com.navdy.client.R.string.delete_btn).setIcon(com.navdy.client.R.drawable.ic_delete).setShowAsAction(2);
        return true;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            case com.navdy.client.R.id.menu_delete /*2131756069*/:
                onDelete(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onDelete(android.view.View view) {
        if (!this.destination.isPersisted()) {
            this.logger.e("Trying to delete a favorite that is not in DB: " + this.destination);
        }
        showProgressDialog();
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.favorites.FavoritesEditActivity.Anon3(), 3);
    }

    protected void saveChanges() {
        if (this.saveOnExit) {
            if (this.destination.id == 0) {
                this.destination.reloadSelfFromDatabaseAsync(new com.navdy.client.app.ui.favorites.FavoritesEditActivity.Anon4());
            }
            new com.navdy.client.app.ui.favorites.FavoritesEditActivity.Anon5(this.label.getText().toString().trim()).execute(new java.lang.Object[0]);
        }
    }

    public void onRefreshConnectivityClick(android.view.View view) {
        com.navdy.client.app.framework.AppInstance.getInstance().checkForNetwork();
    }
}
