package com.navdy.client.app.ui.customviews;

public class TripCardView extends android.widget.RelativeLayout implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener {
    private static final int COLOR_RED = android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.red);
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.customviews.TripCardView.class);
    private android.view.View bottomSheet;
    private android.view.View bottomSheetCalculating;
    private android.widget.TextView bottomSheetCalculatingText;
    protected android.widget.ImageView icon;
    protected final com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler;
    protected android.widget.TextView tripAddressFirstLine;
    protected android.widget.TextView tripAddressSecondLine;
    protected android.widget.TextView tripName;
    private com.navdy.client.app.ui.customviews.ArrivalTimeTextView tripOverviewArrivalTime;
    private com.navdy.client.app.ui.customviews.UnitSystemTextView tripOverviewDistance;
    private android.widget.TextView tripOverviewETA;

    public TripCardView(android.content.Context context) {
        this(context, null, 0);
    }

    public TripCardView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TripCardView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.icon = (android.widget.ImageView) findViewById(com.navdy.client.R.id.illustration);
        this.tripName = (android.widget.TextView) findViewById(com.navdy.client.R.id.trip_name);
        this.tripAddressFirstLine = (android.widget.TextView) findViewById(com.navdy.client.R.id.trip_address_first_line);
        this.tripAddressSecondLine = (android.widget.TextView) findViewById(com.navdy.client.R.id.trip_address_second_line);
        this.tripOverviewDistance = (com.navdy.client.app.ui.customviews.UnitSystemTextView) findViewById(com.navdy.client.R.id.trip_card_distance);
        this.tripOverviewETA = (android.widget.TextView) findViewById(com.navdy.client.R.id.trip_card_eta);
        this.tripOverviewArrivalTime = (com.navdy.client.app.ui.customviews.ArrivalTimeTextView) findViewById(com.navdy.client.R.id.trip_card_arrival_time);
        this.bottomSheet = findViewById(com.navdy.client.R.id.trip_card_bottom_sheet);
        this.bottomSheetCalculating = findViewById(com.navdy.client.R.id.trip_card_bottom_sheet_calculating);
        this.bottomSheetCalculatingText = (android.widget.TextView) findViewById(com.navdy.client.R.id.trip_card_bottom_sheet_calculating_text);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.navdyRouteHandler.addListener(this);
    }

    protected void onDetachedFromWindow() {
        this.navdyRouteHandler.removeListener(this);
        super.onDetachedFromWindow();
    }

    public void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        setNameAddressAndIcon(destination);
        showCalculating();
    }

    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
        if (error != com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE) {
            setNameAddressAndIcon(pendingRoute.getDestination());
            showError();
            return;
        }
        updateFields(pendingRoute.getDestination(), pendingRoute.getDistanceToDestination(), pendingRoute.getTimeToDestination(), pendingRoute.isTrafficHeavy);
    }

    public void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        updateFields(destination, -1, -1, false);
        showCalculating();
    }

    public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        if (error != com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error.NONE) {
            updateFields(route.getDestination(), -1, -1, false);
            showError();
            return;
        }
        updateFields(route.getDestination(), route.getDistanceToDestination(), route.getTimeToDestination(), route.isTrafficHeavy);
    }

    public void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        updateFields(route.getDestination(), route.getDistanceToDestination(), route.getTimeToDestination(), route.isTrafficHeavy);
    }

    public void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo progress) {
        updateFields(progress.getDestination(), progress.getDistanceToDestination(), progress.getTimeToDestination(), progress.isTrafficHeavy);
    }

    public void onReroute() {
        showCalculating();
    }

    public void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    public void onStopRoute() {
    }

    private void showCalculating() {
        this.bottomSheetCalculatingText.setText(com.navdy.client.R.string.calculating_dot_dot_dot);
        this.bottomSheet.setVisibility(View.INVISIBLE);
        this.bottomSheetCalculating.setVisibility(View.VISIBLE);
    }

    private void showError() {
        this.bottomSheetCalculatingText.setText(com.navdy.client.R.string.error_please_retry_route);
        this.bottomSheet.setVisibility(View.INVISIBLE);
        this.bottomSheetCalculating.setVisibility(View.VISIBLE);
    }

    private void updateFields(com.navdy.client.app.framework.models.Destination destination, int distance, int timeToDestination, boolean isTrafficHeavy) {
        if (distance < 0 || timeToDestination < 0) {
            showCalculating();
        } else {
            if (isTrafficHeavy) {
                this.tripOverviewArrivalTime.setTextColor(COLOR_RED);
            }
            hideMessages();
            updateTextFields(distance, timeToDestination);
        }
        setNameAddressAndIcon(destination);
    }

    private void hideMessages() {
        this.bottomSheetCalculating.setVisibility(View.GONE);
        this.bottomSheet.setVisibility(View.VISIBLE);
    }

    private void updateTextFields(int distance, int durationWithTraffic) {
        this.tripOverviewETA.setText(com.navdy.client.app.framework.util.TimeStampUtils.getDurationStringInHoursAndMinutes(java.lang.Integer.valueOf(durationWithTraffic)));
        this.tripOverviewDistance.setDistance((double) distance);
        this.tripOverviewArrivalTime.setTimeToArrival(durationWithTraffic);
    }

    protected java.lang.String appendPrefix(java.lang.String name) {
        return name;
    }

    public void setNameAddressAndIcon(com.navdy.client.app.framework.models.Destination destination) {
        int asset;
        if (destination == null) {
            logger.d("Destination object is null");
            return;
        }
        if (this.icon != null) {
            if (this instanceof com.navdy.client.app.ui.customviews.PendingTripCardView) {
                asset = destination.getBadgeAssetForPendingTrip();
            } else {
                asset = destination.getBadgeAssetForActiveTrip();
            }
            this.icon.setImageResource(asset);
        }
        android.util.Pair<java.lang.String, java.lang.String> titleAndSubtitle = destination.getTitleAndSubtitle();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim((java.lang.CharSequence) titleAndSubtitle.first)) {
            logger.d("titleAndSubtitle is empty: " + titleAndSubtitle.toString());
            return;
        }
        if (this.tripName != null) {
            this.tripName.setText(appendPrefix((java.lang.String) titleAndSubtitle.first));
        } else {
            logger.w("Current route has no name!");
        }
        if (this.tripAddressFirstLine != null) {
            this.tripAddressFirstLine.setText(com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress((java.lang.String) titleAndSubtitle.second));
        } else {
            logger.w("Current route has no address!");
        }
    }

    public boolean isInEditMode() {
        return false;
    }
}
