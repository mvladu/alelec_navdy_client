package com.navdy.client.app.ui.bluetooth;

public class BluetoothFramelayoutFragment extends com.navdy.client.app.ui.base.BaseDialogFragment implements android.view.View.OnClickListener {
    public static final java.lang.String EXTRA_KILL_ACTIVITY_ON_CANCEL = "1";
    public static final java.lang.String EXTRA_USER_WAS_IN_FIRST_LAUNCH_EXPERIENCE = "USER_WAS_IN_FIRST_LAUNCH";
    private static final int TIMEOUT = 15000;
    public android.content.Context context;
    private int currentLayout;
    private java.lang.String deviceName = "";
    private android.view.LayoutInflater inflater;
    private boolean killActivityOnCancel;
    protected com.navdy.client.app.framework.AppInstance mAppInstance;
    protected android.bluetooth.BluetoothAdapter mBluetoothAdapter;
    protected com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener mDeviceListUpdateListener;
    protected java.util.ArrayList<com.navdy.client.debug.devicepicker.Device> mDevices;
    private android.support.v7.widget.RecyclerView mRecyclerView;
    protected com.navdy.service.library.device.RemoteDeviceRegistry mRemoteDeviceRegistry;
    private boolean pairing = false;
    private int position;
    private com.navdy.client.app.ui.bluetooth.BluetoothDialogRecyclerAdapter recyclerAdapter;
    private android.widget.FrameLayout rootView;
    private android.support.v4.widget.SwipeRefreshLayout swipeContainer;
    private boolean userWasInFirstLaunch = false;

    class Anon1 implements com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener {
        Anon1() {
        }

        public void onDeviceListChanged(java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> deviceList) {
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.addConnectionInfo(deviceList);
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.logger.v("DeviceListChanged. Here is the current deviceList: " + com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.mDevices);
            if (!(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.currentLayout == com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_pairing || com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.currentLayout == com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching_found)) {
                com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching_found);
            }
            if (com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.mRecyclerView != null) {
                com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.updateRecyclerView(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.mDevices);
            }
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        Anon2() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.goToSettings();
        }
    }

    class Anon3 implements android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener {
        Anon3() {
        }

        public void onRefresh() {
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.stopRefresh();
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.rootView.removeAllViews();
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.selectDialogBasedOnState();
        }
    }

    class Anon4 implements android.view.View.OnClickListener {
        Anon4() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.retryPairing(view);
        }
    }

    class Anon5 implements android.view.View.OnClickListener {
        Anon5() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.getActivity().finish();
        }
    }

    class Anon6 implements android.view.View.OnClickListener {
        Anon6() {
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.getFragmentManager().popBackStack();
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            if (com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.isAlive()) {
                com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.logger.v("entered postDelayed");
                com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.logger.v("mDevices size: " + com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.mDevices.size());
                if (com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.mDevices.isEmpty()) {
                    com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.logger.v("mDevices was empty");
                    com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching_not_found);
                    return;
                }
                if (!(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.currentLayout == com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching_found || com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.currentLayout == com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_pairing)) {
                    com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching_found);
                }
                com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.updateRecyclerView(com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.mDevices);
            }
        }
    }

    class Anon8 implements android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener {
        Anon8() {
        }

        public void onRefresh() {
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.logger.v("swipeContainer: refresh called");
            com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.this.selectDialogBasedOnState();
        }
    }

    private enum BluetoothState {
        NO_BLUETOOTH,
        DISABLED,
        ENABLED
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mBluetoothAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        this.context = com.navdy.client.app.NavdyApplication.getAppContext();
        setupBluetooth(this.context);
        android.content.Intent intent = getActivity().getIntent();
        if (intent != null) {
            this.killActivityOnCancel = intent.getBooleanExtra(EXTRA_KILL_ACTIVITY_ON_CANCEL, false);
            this.userWasInFirstLaunch = intent.getBooleanExtra(EXTRA_USER_WAS_IN_FIRST_LAUNCH_EXPERIENCE, false);
        }
    }

    public void onDismiss(android.content.DialogInterface dialog) {
        this.logger.v("onDismiss");
        if (getFragmentManager() != null) {
            if (isAlive() && isInForeground()) {
                getFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
            }
            if (this.killActivityOnCancel) {
                android.app.Activity activity = getActivity();
                if (!com.navdy.client.app.ui.base.BaseActivity.isEnding(activity)) {
                    activity.finish();
                }
            }
        }
    }

    public synchronized android.app.Dialog onCreateDialog(android.os.Bundle savedInstanceState) {
        android.support.v7.app.AlertDialog.Builder builder;
        this.logger.v("onCreateDialog");
        builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        this.inflater = getActivity().getLayoutInflater();
        this.rootView = (android.widget.FrameLayout) this.inflater.inflate(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_framelayout, null);
        builder.setView((android.view.View) this.rootView);
        selectDialogBasedOnState();
        return builder.create();
    }

    public synchronized void setupBluetooth(android.content.Context context2) {
        this.logger.v("setupBluetooth called");
        this.mDevices = new java.util.ArrayList<>();
        this.logger.v("mDevices: " + java.lang.System.identityHashCode(this.mDevices));
        this.mRemoteDeviceRegistry = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(context2);
        this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        addConnectionInfo(this.mRemoteDeviceRegistry.getKnownConnectionInfo());
        this.mDeviceListUpdateListener = new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon1();
    }

    public synchronized void showDialog(int res) {
        if (this.rootView != null) {
            this.logger.v("showDialog called on res: " + getResources().getResourceEntryName(res));
            if (this.currentLayout != res) {
                if (this.currentLayout != 0) {
                    this.logger.v("savedLayout: " + this.currentLayout);
                    this.rootView.removeAllViews();
                }
                this.currentLayout = res;
                android.widget.FrameLayout.LayoutParams layoutParams = new android.widget.FrameLayout.LayoutParams(-2, -2);
                if (this.inflater == null && getActivity() != null) {
                    this.inflater = getActivity().getLayoutInflater();
                }
                if (this.inflater != null) {
                    this.rootView.addView(this.inflater.inflate(res, this.rootView, false), layoutParams);
                }
                setUpCancelButton();
                switch (res) {
                    case com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_connect_failed /*2130903131*/:
                        ((android.widget.Button) this.rootView.findViewById(com.navdy.client.R.id.retry_pairing_button)).setOnClickListener(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon4());
                        break;
                    case com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_not_compatible /*2130903134*/:
                        ((android.widget.Button) this.rootView.findViewById(com.navdy.client.R.id.go_to_bluetooth_settings_button)).setOnClickListener(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon2());
                        break;
                    case com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_pairing /*2130903135*/:
                        ((android.widget.TextView) this.rootView.findViewById(com.navdy.client.R.id.device_name)).setText(this.deviceName);
                        break;
                    case com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching /*2130903136*/:
                        break;
                    case com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching_found /*2130903137*/:
                        setupRecyclerView();
                        break;
                    case com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching_not_found /*2130903139*/:
                        this.swipeContainer = (android.support.v4.widget.SwipeRefreshLayout) this.rootView.findViewById(com.navdy.client.R.id.swipe_container);
                        this.swipeContainer.setColorSchemeResources(com.navdy.client.R.color.blue, com.navdy.client.R.color.green_success, com.navdy.client.R.color.yellow, com.navdy.client.R.color.purple_warm);
                        this.swipeContainer.setOnRefreshListener(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon3());
                        break;
                }
            }
        }
    }

    public synchronized void setUpCancelButton() {
        if (this.rootView != null) {
            android.widget.Button cancelButton = (android.widget.Button) this.rootView.findViewById(com.navdy.client.R.id.cancel_button);
            if (cancelButton != null) {
                if (this.killActivityOnCancel) {
                    cancelButton.setOnClickListener(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon5());
                } else {
                    cancelButton.setOnClickListener(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon6());
                }
            }
        }
    }

    public synchronized void setDeviceName(java.lang.String prettyName) {
        this.deviceName = prettyName;
    }

    public synchronized void selectDialogBasedOnState() {
        switch (getBluetoothState()) {
            case NO_BLUETOOTH:
            case DISABLED:
                showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_not_compatible);
                break;
            case ENABLED:
                if (!this.pairing) {
                    showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_searching);
                    if (this.mRemoteDeviceRegistry != null) {
                        this.mDevices.clear();
                        this.logger.v("selectDialogBasedOnState(): mRemoteDeviceRegistry.startScanning()");
                        this.mRemoteDeviceRegistry.stopScanning();
                        this.mRemoteDeviceRegistry.startScanning();
                    }
                    if (this.mRecyclerView != null) {
                        this.logger.v("RecyclerView was not null");
                        updateRecyclerView(this.mDevices);
                    }
                    this.handler.postDelayed(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon7(), 15000);
                    break;
                } else {
                    showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_pairing);
                    break;
                }
        }
    }

    public synchronized com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.BluetoothState getBluetoothState() {
        com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.BluetoothState bluetoothState;
        if (this.mBluetoothAdapter == null) {
            bluetoothState = com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.BluetoothState.NO_BLUETOOTH;
        } else if (!this.mBluetoothAdapter.isEnabled()) {
            bluetoothState = com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.BluetoothState.DISABLED;
        } else {
            bluetoothState = com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.BluetoothState.ENABLED;
        }
        return bluetoothState;
    }

    public synchronized void stopRefresh() {
        this.logger.v("swipeContainer: stopRefresh");
        this.swipeContainer.setRefreshing(false);
    }

    public synchronized void onResume() {
        super.onResume();
        this.logger.v("added listener to mRemoteDeviceRegistry");
        this.mRemoteDeviceRegistry.addListener(this.mDeviceListUpdateListener);
        selectDialogBasedOnState();
    }

    public synchronized void onPause() {
        if (this.mRemoteDeviceRegistry != null) {
            this.mRemoteDeviceRegistry.stopScanning();
            this.logger.v("removing mRemoteDeviceRegistry listener");
            this.mRemoteDeviceRegistry.removeListener(this.mDeviceListUpdateListener);
        }
        super.onPause();
    }

    public synchronized void setupRecyclerView() {
        this.logger.v("setupRecyclerView()");
        if (this.rootView != null) {
            this.mRecyclerView = (android.support.v7.widget.RecyclerView) this.rootView.findViewById(com.navdy.client.R.id.bluetooth_recycler_view);
        }
        android.support.v7.widget.LinearLayoutManager layoutManager = new android.support.v7.widget.LinearLayoutManager(getActivity());
        layoutManager.setOrientation(1);
        if (this.mRecyclerView != null) {
            this.mRecyclerView.setLayoutManager(layoutManager);
        }
        this.recyclerAdapter = new com.navdy.client.app.ui.bluetooth.BluetoothDialogRecyclerAdapter();
        this.recyclerAdapter.setClickListener(this);
        if (this.mRecyclerView != null) {
            this.mRecyclerView.setAdapter(this.recyclerAdapter);
        }
        if (this.rootView != null) {
            this.swipeContainer = (android.support.v4.widget.SwipeRefreshLayout) this.rootView.findViewById(com.navdy.client.R.id.swipe_container);
            this.swipeContainer.setColorSchemeResources(com.navdy.client.R.color.blue, com.navdy.client.R.color.green_success, com.navdy.client.R.color.yellow, com.navdy.client.R.color.purple_warm);
            this.swipeContainer.setOnRefreshListener(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment.Anon8());
        }
    }

    public synchronized void updateRecyclerView(java.util.ArrayList<com.navdy.client.debug.devicepicker.Device> devices) {
        this.logger.v("updateRecyclerView");
        java.util.Iterator it = devices.iterator();
        while (it.hasNext()) {
            com.navdy.client.debug.devicepicker.Device device = (com.navdy.client.debug.devicepicker.Device) it.next();
            if (!this.recyclerAdapter.contains(device.getPrettyName())) {
                this.recyclerAdapter.addDevice(device);
                this.recyclerAdapter.notifyDataSetChanged();
            }
        }
    }

    protected synchronized void addConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo, boolean updateIfFound) {
        com.navdy.client.debug.devicepicker.Device newDevice = new com.navdy.client.debug.devicepicker.Device(connectionInfo);
        int index = this.mDevices.indexOf(newDevice);
        if (index != -1) {
            ((com.navdy.client.debug.devicepicker.Device) this.mDevices.get(index)).add(connectionInfo, updateIfFound);
        } else {
            this.mDevices.add(newDevice);
            this.logger.v("Added a device" + newDevice.getPrettyName());
        }
    }

    protected synchronized void addConnectionInfo(java.util.Set<com.navdy.service.library.device.connection.ConnectionInfo> connectionInfoSet) {
        this.logger.v("mDevices: add " + java.lang.System.identityHashCode(this.mDevices));
        for (com.navdy.service.library.device.connection.ConnectionInfo connectionInfo : connectionInfoSet) {
            addConnectionInfo(connectionInfo, true);
        }
    }

    public synchronized void selectOption(int position2) {
        java.lang.String str;
        if (this.mDevices == null || position2 >= this.mDevices.size() || position2 < 0) {
            com.navdy.service.library.log.Logger logger = this.logger;
            java.lang.StringBuilder append = new java.lang.StringBuilder().append("Can't selectOption position is ").append(position2).append(" and mDevice ");
            if (this.mDevices != null) {
                str = "only has " + this.mDevices.size() + " entries";
            } else {
                str = "is null";
            }
            logger.e(append.append(str).toString());
        } else {
            this.mRemoteDeviceRegistry.stopScanning();
            this.logger.v("mRemoteDeviceRegistry.stopScanning");
            this.logger.v("mDevices: selectOption " + java.lang.System.identityHashCode(this.mDevices));
            this.logger.v("mDevices size during selectOption: " + this.mDevices.size());
            com.navdy.client.debug.devicepicker.Device pickedDevice = (com.navdy.client.debug.devicepicker.Device) this.mDevices.get(position2);
            setDeviceName(pickedDevice.getPrettyName());
            this.pairing = true;
            showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_pairing);
            this.mRemoteDeviceRegistry.setDefaultConnectionInfo(pickedDevice.getPreferredConnectionInfo());
            this.mAppInstance.initializeDevice();
        }
    }

    @com.squareup.otto.Subscribe
    public void appInstanceDeviceConnectionEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent deviceConnectedEvent) {
        this.logger.v("appInstanceDeviceConnectionEvent");
        pairingComplete(true);
        finishBluetoothPairActivity();
    }

    @com.squareup.otto.Subscribe
    public void appInstanceDeviceConnectionFailedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectionFailedEvent deviceConnectionFailedEvent) {
        this.logger.e("appInstanceDeviceConnectionFailedEvent: " + deviceConnectionFailedEvent);
        pairingComplete(false);
        showDialog(com.navdy.client.R.layout.fl_fragment_bluetooth_dialog_connect_failed);
    }

    private void pairingComplete(boolean connected) {
        this.pairing = false;
        java.util.HashMap<java.lang.String, java.lang.String> pairPhoneToDisplayValues = new java.util.HashMap<>();
        pairPhoneToDisplayValues.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.SUCCESS_ATTRIBUTE, java.lang.Boolean.toString(connected));
        pairPhoneToDisplayValues.put("First_Launch", java.lang.Boolean.toString(this.userWasInFirstLaunch));
        com.navdy.client.app.framework.LocalyticsManager.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.PAIR_PHONE_TO_DISPLAY, pairPhoneToDisplayValues);
    }

    public synchronized void onClick(android.view.View v) {
        int position2 = this.mRecyclerView.getChildAdapterPosition(v);
        this.logger.v("onItemClick called. position is " + position2);
        this.position = position2;
        selectOption(position2);
    }

    public synchronized void finishBluetoothPairActivity() {
        this.logger.v("finishBluetoothPairActivity");
        android.app.Activity currentActivity = getActivity();
        if (currentActivity != null) {
            this.logger.v("currentActivity.finish(): " + currentActivity);
            currentActivity.finish();
        }
    }

    public synchronized void goToSettings() {
        this.logger.v("go to bluetooth settings");
        android.content.Intent intentOpenBluetoothSettings = new android.content.Intent();
        intentOpenBluetoothSettings.setAction("android.settings.BLUETOOTH_SETTINGS");
        startActivity(intentOpenBluetoothSettings);
    }

    public synchronized void retryPairing(android.view.View view) {
        selectOption(this.position);
    }
}
