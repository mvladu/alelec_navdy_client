package com.navdy.client.app.ui.firstlaunch;

public class BoxPickerActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_box_picker);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.select_box).build();
        loadImage(com.navdy.client.R.id.illustration, com.navdy.client.R.drawable.image_navdy_select);
        loadImage(com.navdy.client.R.id.new_box_illustration, com.navdy.client.R.drawable.image_navdy_sm);
        loadImage(com.navdy.client.R.id.new_box_plus_mounts_illustration, com.navdy.client.R.drawable.image_navdy_plus_sm);
        loadImage(com.navdy.client.R.id.old_box_illustration, com.navdy.client.R.drawable.image_navdy_oversized_sm);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.BOX_PICKER);
    }

    public void onNewBoxClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "New_Box").apply();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.BoxViewerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.BoxViewerActivity.EXTRA_BOX, "New_Box");
        startActivity(intent);
    }

    public void onNewBoxPlusMountsClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "New_Box_Plus_Mounts").apply();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.BoxViewerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.BoxViewerActivity.EXTRA_BOX, "New_Box_Plus_Mounts");
        startActivity(intent);
    }

    public void onOldBoxClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box").apply();
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.BoxViewerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.BoxViewerActivity.EXTRA_BOX, "Old_Box");
        startActivity(intent);
    }

    public void onBackClick(android.view.View view) {
        finish();
    }
}
