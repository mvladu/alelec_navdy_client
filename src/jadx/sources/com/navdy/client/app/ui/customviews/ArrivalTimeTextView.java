package com.navdy.client.app.ui.customviews;

public class ArrivalTimeTextView extends android.widget.TextView {
    private static final long UPDATE_INTERVAL = 60000;
    private int currentTimeToArrival;
    private final java.lang.Runnable updateTime;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.ui.customviews.ArrivalTimeTextView.this.setText(com.navdy.client.app.framework.util.TimeStampUtils.getArrivalTimeString(java.lang.Integer.valueOf(com.navdy.client.app.ui.customviews.ArrivalTimeTextView.this.currentTimeToArrival)));
            com.navdy.client.app.ui.customviews.ArrivalTimeTextView.this.postDelayed(this, 60000);
        }
    }

    public ArrivalTimeTextView(android.content.Context context) {
        this(context, null);
    }

    public ArrivalTimeTextView(android.content.Context context, android.util.AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ArrivalTimeTextView(android.content.Context context, android.util.AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.updateTime = new com.navdy.client.app.ui.customviews.ArrivalTimeTextView.Anon1();
    }

    public void setTimeToArrival(int timeToArrival) {
        removeCallbacks(this.updateTime);
        this.currentTimeToArrival = timeToArrival;
        post(this.updateTime);
    }

    protected void onDetachedFromWindow() {
        removeCallbacks(this.updateTime);
        super.onDetachedFromWindow();
    }
}
