package com.navdy.client.app.ui.settings;

class FeatureVideosAdapter extends android.support.v7.widget.RecyclerView.Adapter {
    private static final int CACHE_SIZE = 52428800;
    private static final int IMAGE_MAX_SIZE = 4194304;
    private android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
    private final com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo[] featureVideoArray = {new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_features_overview", this.context.getString(com.navdy.client.R.string.features_overview), com.navdy.client.R.drawable.image_video_features_overview, "1:59", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/00-FeatureOverview-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_power", this.context.getString(com.navdy.client.R.string.features_power), com.navdy.client.R.drawable.image_video_power, "0:30", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/01-PowerAndYourNavdyDisplay-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_connectiing", this.context.getString(com.navdy.client.R.string.features_connection), com.navdy.client.R.drawable.image_video_connectiing, "0:53", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/02-ConnectingtoYourNavdyDisplay-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_mobile_app_overview", this.context.getString(com.navdy.client.R.string.features_mobile_overview), com.navdy.client.R.drawable.image_video_mobile_app_overview, "0:53", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/03-MobileAppOverview-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_dial", this.context.getString(com.navdy.client.R.string.features_dial), com.navdy.client.R.drawable.image_video_dial, "0:47", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/04-UsingYourNavdyDial-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_gestures", this.context.getString(com.navdy.client.R.string.features_gestures), com.navdy.client.R.drawable.image_gestures, "1:03", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/05-UsingGestures-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_brightness", this.context.getString(com.navdy.client.R.string.features_brightness), com.navdy.client.R.drawable.image_video_brightness, "0:31", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/06-AdjustingBrightness-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_dash_mode", this.context.getString(com.navdy.client.R.string.features_dash_mode), com.navdy.client.R.drawable.image_video_dash_mode, "1:32", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/07-DashMode-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_map_mode", this.context.getString(com.navdy.client.R.string.features_map_mode), com.navdy.client.R.drawable.image_video_map_mode, "1:24", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/08-MapMode-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_searching", this.context.getString(com.navdy.client.R.string.features_search), com.navdy.client.R.drawable.image_video_searching, "1:14", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/09-SearchingForANewDestination-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_adding_favorites", this.context.getString(com.navdy.client.R.string.features_favorites), com.navdy.client.R.drawable.image_video_adding_favorites, "0:46", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/10-AddingFavoritePlaces-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_driving_favorite", this.context.getString(com.navdy.client.R.string.features_favorites_navigation), com.navdy.client.R.drawable.image_video_driving_favorite, "0:27", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/11-DrivingToAFavPlace-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_driving_suggested", this.context.getString(com.navdy.client.R.string.features_suggested_places), com.navdy.client.R.drawable.image_video_driving_suggested, "0:30", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/12-DrivingToASuggestedPlace-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_answering_calls", this.context.getString(com.navdy.client.R.string.features_answering_calls), com.navdy.client.R.drawable.image_video_answering_calls, "0:29", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/13-AnsweringCalls-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_making_calls", this.context.getString(com.navdy.client.R.string.features_making_calls), com.navdy.client.R.drawable.image_video_making_calls, "0:38", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/14-MakingCalls-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_reading_glances", this.context.getString(com.navdy.client.R.string.features_reading_glances), com.navdy.client.R.drawable.image_video_reading_glances, "1:43", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/15-ReadingGlancesFromYourPhone-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_music", this.context.getString(com.navdy.client.R.string.features_controlling_music), com.navdy.client.R.drawable.image_video_music, "0:43", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/16-ControllingYourMusic-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_google_now", this.context.getString(com.navdy.client.R.string.features_using_google_now), com.navdy.client.R.drawable.image_video_google_now, "0:55", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/17-UsingGoogleNow-Android-App.mp4"), new com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo("image_video_up_to_date", this.context.getString(com.navdy.client.R.string.feature_updating_navdy), com.navdy.client.R.drawable.image_video_up_to_date, "0:38", "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/18-KeepingNavdyUpToDate-Android-App.mp4")};
    private java.util.ArrayList<com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo> featureVideoArrayList = new java.util.ArrayList<>();
    private java.util.HashSet<java.lang.String> hasWatched = new java.util.HashSet<>();
    private com.navdy.client.app.framework.util.ImageCache imageCache;
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.settings.FeatureVideosAdapter.class);

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo val$featureVideo;

        Anon1(com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo featureVideo) {
            this.val$featureVideo = featureVideo;
        }

        public void onClick(android.view.View view) {
            com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.saveVideoAsWatched(this.val$featureVideo.sharedPrefKey);
            android.content.Intent intent = new android.content.Intent(com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.context, com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.class);
            intent.putExtra(com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.EXTRA_VIDEO_URL, this.val$featureVideo.link);
            com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.context.startActivity(intent);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            int width = com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.context.getResources().getDimensionPixelSize(com.navdy.client.R.dimen.feature_video_width);
            int height = com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.context.getResources().getDimensionPixelSize(com.navdy.client.R.dimen.feature_video_height);
            java.util.Iterator it = com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.featureVideoArrayList.iterator();
            while (it.hasNext()) {
                com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo featureVideo = (com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo) it.next();
                android.graphics.Bitmap bitmap = com.navdy.client.app.framework.util.ImageUtils.getBitmap(featureVideo.imageResource, width, height);
                com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.logger.d("bitmap size: " + com.navdy.client.app.framework.util.ImageCache.getBitmapSize(bitmap));
                com.navdy.client.app.ui.settings.FeatureVideosAdapter.this.imageCache.putBitmap(featureVideo.title, bitmap);
            }
        }
    }

    public static class FeatureVideo {
        public java.lang.String duration;
        public int imageResource;
        public java.lang.String link;
        public java.lang.String sharedPrefKey;
        public java.lang.String title;

        public FeatureVideo(java.lang.String sharedPrefKey2, java.lang.String title2, int imageResource2, java.lang.String duration2, java.lang.String link2) {
            this.sharedPrefKey = sharedPrefKey2;
            this.title = title2;
            this.imageResource = imageResource2;
            this.duration = duration2;
            this.link = link2;
        }
    }

    FeatureVideosAdapter(android.content.Context activityContext) {
        this.context = activityContext;
        this.featureVideoArrayList = new java.util.ArrayList<>(java.util.Arrays.asList(this.featureVideoArray));
        this.imageCache = new com.navdy.client.app.framework.util.ImageCache(CACHE_SIZE, 4194304);
        initImageCache();
        initHasWatched();
    }

    public com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo getItem(int position) {
        return (com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo) this.featureVideoArrayList.get(position);
    }

    public int getItemCount() {
        return this.featureVideoArrayList.size();
    }

    public com.navdy.client.app.ui.settings.FeatureVideoViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        return new com.navdy.client.app.ui.settings.FeatureVideoViewHolder(android.view.LayoutInflater.from(this.context).inflate(com.navdy.client.R.layout.feature_video_layout, parent, false));
    }

    public void onBindViewHolder(android.support.v7.widget.RecyclerView.ViewHolder viewHolder, int position) {
        com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo featureVideo = (com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo) this.featureVideoArrayList.get(position);
        if (viewHolder instanceof com.navdy.client.app.ui.settings.FeatureVideoViewHolder) {
            com.navdy.client.app.ui.settings.FeatureVideoViewHolder featureVideoViewHolder = (com.navdy.client.app.ui.settings.FeatureVideoViewHolder) viewHolder;
            if (featureVideo != null) {
                featureVideoViewHolder.title.setText(featureVideo.title);
                android.graphics.Bitmap bitmap = this.imageCache.getBitmap(featureVideo.title);
                if (bitmap != null) {
                    featureVideoViewHolder.image.setImageBitmap(bitmap);
                } else {
                    featureVideoViewHolder.image.setImageResource(featureVideo.imageResource);
                }
                featureVideoViewHolder.duration.setText(featureVideo.duration);
                if (this.hasWatched.contains(featureVideo.sharedPrefKey)) {
                    featureVideoViewHolder.watchedLayout.setVisibility(View.VISIBLE);
                } else {
                    featureVideoViewHolder.watchedLayout.setVisibility(View.INVISIBLE);
                }
                featureVideoViewHolder.row.setOnClickListener(new com.navdy.client.app.ui.settings.FeatureVideosAdapter.Anon1(featureVideo));
                return;
            }
            return;
        }
        this.logger.e("The ViewHolder was not a FeatureVideoViewHolder.");
    }

    private void initImageCache() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.FeatureVideosAdapter.Anon2(), 1);
    }

    private void initHasWatched() {
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        java.util.Iterator it = this.featureVideoArrayList.iterator();
        while (it.hasNext()) {
            com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo featureVideo = (com.navdy.client.app.ui.settings.FeatureVideosAdapter.FeatureVideo) it.next();
            if (sharedPreferences.getBoolean(featureVideo.sharedPrefKey, false)) {
                this.hasWatched.add(featureVideo.sharedPrefKey);
            }
        }
    }

    private void saveVideoAsWatched(java.lang.String featureVideoKey) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(featureVideoKey)) {
            this.logger.e("featureVideoKey was empty");
            return;
        }
        this.hasWatched.add(featureVideoKey);
        this.logger.d("hasWatched: " + featureVideoKey);
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(featureVideoKey, true).apply();
    }
}
