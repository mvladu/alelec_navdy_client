package com.navdy.client.app.ui.settings;

class CalendarGlobalSwitchViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ android.widget.Switch val$sweetch;

        Anon1(android.widget.Switch switchR) {
            this.val$sweetch = switchR;
        }

        public void onClick(android.view.View v) {
            this.val$sweetch.callOnClick();
        }
    }

    class Anon2 implements android.widget.CompoundButton.OnCheckedChangeListener {
        final /* synthetic */ android.widget.CompoundButton.OnCheckedChangeListener val$checkedChangeListener;

        Anon2(android.widget.CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
            this.val$checkedChangeListener = onCheckedChangeListener;
        }

        public void onCheckedChanged(android.widget.CompoundButton buttonView, boolean isChecked) {
            android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (sharedPrefs != null) {
                sharedPrefs.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDARS_ENABLED, isChecked).apply();
                if (this.val$checkedChangeListener != null) {
                    this.val$checkedChangeListener.onCheckedChanged(buttonView, isChecked);
                }
            }
        }
    }

    CalendarGlobalSwitchViewHolder(android.view.View v, android.widget.CompoundButton.OnCheckedChangeListener checkedChangeListener) {
        super(v);
        android.widget.Switch sweetch = (android.widget.Switch) v.findViewById(com.navdy.client.R.id.sweetch);
        if (sweetch != null) {
            sweetch.setText(com.navdy.client.R.string.show_all_calendars);
            v.setOnClickListener(new com.navdy.client.app.ui.settings.CalendarGlobalSwitchViewHolder.Anon1(sweetch));
            android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (sharedPrefs != null) {
                sweetch.setChecked(sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.CALENDARS_ENABLED, true));
            }
            sweetch.setOnCheckedChangeListener(new com.navdy.client.app.ui.settings.CalendarGlobalSwitchViewHolder.Anon2(checkedChangeListener));
        }
    }
}
