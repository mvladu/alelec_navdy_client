package com.navdy.client.app.ui.settings;

public class AudioDialogActivity$$ViewInjector {

    /* compiled from: AudioDialogActivity$$ViewInjector */
    static class Anon1 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioDialogActivity val$target;

        Anon1(com.navdy.client.app.ui.settings.AudioDialogActivity audioDialogActivity) {
            this.val$target = audioDialogActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    /* compiled from: AudioDialogActivity$$ViewInjector */
    static class Anon2 extends butterknife.internal.DebouncingOnClickListener {
        final /* synthetic */ com.navdy.client.app.ui.settings.AudioDialogActivity val$target;

        Anon2(com.navdy.client.app.ui.settings.AudioDialogActivity audioDialogActivity) {
            this.val$target = audioDialogActivity;
        }

        public void doClick(android.view.View p0) {
            this.val$target.onClick(p0);
        }
    }

    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.app.ui.settings.AudioDialogActivity target, java.lang.Object source) {
        target.statusTitle = (android.widget.TextView) finder.findOptionalView(source, com.navdy.client.R.id.title);
        target.statusImage = (android.widget.ImageView) finder.findOptionalView(source, com.navdy.client.R.id.status_image);
        target.volumeProgress = (android.widget.ProgressBar) finder.findOptionalView(source, com.navdy.client.R.id.volume);
        target.outputDeviceName = (android.widget.TextView) finder.findOptionalView(source, com.navdy.client.R.id.audio_output_device_name);
        android.view.View view = finder.findOptionalView(source, com.navdy.client.R.id.settings);
        if (view != null) {
            view.setOnClickListener(new com.navdy.client.app.ui.settings.AudioDialogActivity$$ViewInjector.Anon1(target));
        }
        android.view.View view2 = finder.findOptionalView(source, com.navdy.client.R.id.ok);
        if (view2 != null) {
            view2.setOnClickListener(new com.navdy.client.app.ui.settings.AudioDialogActivity$$ViewInjector.Anon2(target));
        }
    }

    public static void reset(com.navdy.client.app.ui.settings.AudioDialogActivity target) {
        target.statusTitle = null;
        target.statusImage = null;
        target.volumeProgress = null;
        target.outputDeviceName = null;
    }
}
