package com.navdy.client.app.ui.base;

public class BaseDialogFragment extends android.app.DialogFragment {
    protected com.navdy.client.app.ui.base.BaseActivity baseActivity;
    protected final android.os.Handler handler = new android.os.Handler();
    /* access modifiers changed from: protected */
    public final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(getClass());
    private volatile boolean mIsInForeground = false;

    public void onCreate(android.os.Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
    }

    public void onPause() {
        this.logger.v("::onPause");
        super.onPause();
        this.mIsInForeground = false;
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
        this.mIsInForeground = true;
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public void onSaveInstanceState(android.os.Bundle state) {
        this.mIsInForeground = false;
        super.onSaveInstanceState(state);
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public void onAttach(android.app.Activity activity) {
        this.logger.v("::onAttach");
        super.onAttach(activity);
        if (activity instanceof com.navdy.client.app.ui.base.BaseActivity) {
            this.baseActivity = (com.navdy.client.app.ui.base.BaseActivity) activity;
        }
    }

    public void onDetach() {
        this.logger.v("::onDetach");
        this.baseActivity = null;
        super.onDetach();
    }

    public void onDestroy() {
        this.logger.v("::onDestroy");
        super.onDestroy();
    }

    public boolean isAlive() {
        return this.baseActivity != null && !this.baseActivity.isActivityDestroyed();
    }
}
