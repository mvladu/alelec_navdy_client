package com.navdy.client.app.ui.settings;

public class GeneralSettingsActivity extends com.navdy.client.app.ui.base.BaseEditActivity {
    private final com.navdy.client.app.framework.util.BusProvider bus = com.navdy.client.app.framework.util.BusProvider.getInstance();
    private android.widget.Switch gestureSwitch;
    private android.widget.RadioButton googleNow;
    private final com.navdy.client.app.framework.i18n.I18nManager i18nManager = com.navdy.client.app.framework.i18n.I18nManager.getInstance();
    private android.widget.Switch limitCellDataSwitch;
    private final android.widget.CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new com.navdy.client.app.ui.settings.GeneralSettingsActivity.Anon2();
    private android.widget.RadioButton placeSearch;
    private final android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
    private android.widget.Switch smartSuggestionsSwitch;
    private final android.view.View.OnClickListener unitSystemClickListener = new com.navdy.client.app.ui.settings.GeneralSettingsActivity.Anon1();
    private android.widget.RadioButton unitSystemImperial;
    private android.widget.RadioButton unitSystemMetric;

    class Anon1 implements android.view.View.OnClickListener {
        Anon1() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.settings.GeneralSettingsActivity.this.somethingChanged = true;
        }
    }

    class Anon2 implements android.widget.CompoundButton.OnCheckedChangeListener {
        Anon2() {
        }

        public void onCheckedChanged(android.widget.CompoundButton compoundButton, boolean isChecked) {
            com.navdy.client.app.ui.settings.GeneralSettingsActivity.this.somethingChanged = true;
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
        }
    }

    class Anon4 implements android.view.View.OnClickListener {
        Anon4() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.settings.GeneralSettingsActivity.this.somethingChanged = true;
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SWITCHED_TO_DEFAULT);
        }
    }

    class Anon5 implements android.view.View.OnClickListener {
        Anon5() {
        }

        public void onClick(android.view.View v) {
            com.navdy.client.app.ui.settings.GeneralSettingsActivity.this.somethingChanged = true;
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SWITCHED_TO_PLACE_SEARCH);
        }
    }

    public static class LimitCellDataEvent {
        public final boolean hasLimitedCellData;

        LimitCellDataEvent(boolean hasLimitedCellData2) {
            this.hasLimitedCellData = hasLimitedCellData2;
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_general);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_general).build();
    }

    protected void onResume() {
        super.onResume();
        initUnitSystemSettings();
        initDialLongPress();
        initGesturesSettings();
        initLimitCellularDataSettings();
        initSmartSuggestionsSettings();
    }

    public void onSmartSuggestionsClick(android.view.View view) {
        this.smartSuggestionsSwitch.toggle();
    }

    public void onLimitCellularDataClick(android.view.View view) {
        this.limitCellDataSwitch.toggle();
    }

    public void onAllowGesturesClick(android.view.View view) {
        this.gestureSwitch.toggle();
    }

    protected void saveChanges() {
        int value;
        if (this.unitSystemMetric.isChecked()) {
            this.i18nManager.setUnitSystem(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC);
        } else if (this.unitSystemImperial.isChecked()) {
            this.i18nManager.setUnitSystem(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL);
        } else {
            this.logger.e("invalid state for radio group");
        }
        com.navdy.client.app.ui.glances.GlanceUtils.saveGlancesConfigurationChanges(com.navdy.client.app.ui.settings.SettingsConstants.HUD_GESTURE, this.gestureSwitch.isChecked());
        com.navdy.client.app.ui.settings.SettingsUtils.sendHudSettings(this.gestureSwitch.isChecked());
        if (com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData() != this.limitCellDataSwitch.isChecked()) {
            com.navdy.client.app.ui.settings.SettingsUtils.setLimitCellularData(this.limitCellDataSwitch.isChecked());
            this.bus.post(new com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent(this.limitCellDataSwitch.isChecked()));
        }
        android.content.SharedPreferences.Editor putBoolean = this.sharedPrefs.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.RECOMMENDATIONS_ENABLED, this.smartSuggestionsSwitch.isChecked());
        java.lang.String str = com.navdy.client.app.ui.settings.SettingsConstants.DIAL_LONG_PRESS_ACTION;
        if (this.placeSearch.isChecked()) {
            value = com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue();
        } else {
            value = com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT.getValue();
        }
        putBoolean.putInt(str, value).apply();
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.settings.GeneralSettingsActivity.Anon3(), 1);
    }

    private void initUnitSystemSettings() {
        this.unitSystemMetric = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.unit_system_metric);
        this.unitSystemImperial = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.unit_system_imperial);
        com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem = this.i18nManager.getUnitSystem();
        if (unitSystem == com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC) {
            this.unitSystemMetric.setChecked(true);
        } else if (unitSystem == com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL) {
            this.unitSystemImperial.setChecked(true);
        } else {
            this.logger.e("invalid unit system");
        }
        this.unitSystemMetric.setOnClickListener(this.unitSystemClickListener);
        this.unitSystemImperial.setOnClickListener(this.unitSystemClickListener);
    }

    private void initDialLongPress() {
        this.googleNow = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.dial_long_press_google_now);
        this.placeSearch = (android.widget.RadioButton) findViewById(com.navdy.client.R.id.dial_long_press_place_search);
        android.content.SharedPreferences settingsPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (!settingsPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_DIAL_LONG_PRESS_CAPABLE, false)) {
            hideIfFound(com.navdy.client.R.id.dial_long_press);
            hideIfFound(com.navdy.client.R.id.dial_long_press_group);
            hideIfFound(com.navdy.client.R.id.dial_long_press_desc);
            return;
        }
        if (settingsPrefs.getInt(com.navdy.client.app.ui.settings.SettingsConstants.DIAL_LONG_PRESS_ACTION, com.navdy.client.app.ui.settings.SettingsConstants.DIAL_LONG_PRESS_ACTION_DEFAULT) != com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue()) {
            this.googleNow.setChecked(true);
        } else {
            this.placeSearch.setChecked(true);
        }
        this.googleNow.setOnClickListener(new com.navdy.client.app.ui.settings.GeneralSettingsActivity.Anon4());
        this.placeSearch.setOnClickListener(new com.navdy.client.app.ui.settings.GeneralSettingsActivity.Anon5());
    }

    private void hideIfFound(@android.support.annotation.IdRes int resId) {
        android.view.View v = findViewById(resId);
        if (v != null) {
            v.setVisibility(View.GONE);
        }
    }

    private void initGesturesSettings() {
        boolean gesturesAreEnabled = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_GESTURE, true);
        this.gestureSwitch = (android.widget.Switch) findViewById(com.navdy.client.R.id.allow_gestures_switch);
        if (this.gestureSwitch != null) {
            this.gestureSwitch.setChecked(gesturesAreEnabled);
            this.gestureSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    private void initLimitCellularDataSettings() {
        boolean limitCellularDataEnabled = com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData();
        this.limitCellDataSwitch = (android.widget.Switch) findViewById(com.navdy.client.R.id.limit_cell_data_switch);
        if (this.limitCellDataSwitch != null) {
            this.limitCellDataSwitch.setChecked(limitCellularDataEnabled);
            this.limitCellDataSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    private void initSmartSuggestionsSettings() {
        boolean smartSuggestionsEnabled = this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.RECOMMENDATIONS_ENABLED, true);
        this.smartSuggestionsSwitch = (android.widget.Switch) findViewById(com.navdy.client.R.id.smart_suggestions_switch);
        if (this.smartSuggestionsSwitch != null) {
            this.smartSuggestionsSwitch.setChecked(smartSuggestionsEnabled);
            this.smartSuggestionsSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    public void onLearnMoreClick(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.GestureDialogActivity.class));
    }
}
