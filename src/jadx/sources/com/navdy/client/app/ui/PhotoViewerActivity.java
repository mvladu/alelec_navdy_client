package com.navdy.client.app.ui;

public class PhotoViewerActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    public static final java.lang.String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.photo_viewer);
        java.lang.String filePath = getIntent().getStringExtra(EXTRA_FILE_PATH);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(filePath)) {
            this.logger.e("Image path not specified !");
            finish();
            return;
        }
        android.widget.ImageView image = (android.widget.ImageView) findViewById(com.navdy.client.R.id.image);
        if (image == null) {
            this.logger.e("Image layout element not found !");
            finish();
            return;
        }
        android.graphics.Bitmap bitmap = com.navdy.client.app.tracking.Tracker.getBitmapFromInternalStorage(filePath);
        if (bitmap == null) {
            this.logger.e("Unable to get a bitmap from the filePath: " + filePath + " !");
            image.setImageResource(com.navdy.client.R.drawable.image_generic_port);
            return;
        }
        image.setImageBitmap(bitmap);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
    }

    public void onDoneClick(android.view.View view) {
        finish();
    }
}
