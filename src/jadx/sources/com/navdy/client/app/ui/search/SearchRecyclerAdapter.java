package com.navdy.client.app.ui.search;

public class SearchRecyclerAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.navdy.client.app.ui.search.SearchViewHolder> {
    private static final int CONTACT_PHOTO_RES_VALUE = 0;
    private static final double INVALID_DISTANCE = -1.0d;
    private static final int NO_FLAGS = 0;
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.search.SearchRecyclerAdapter.class);
    private android.widget.RelativeLayout card;
    private java.util.HashMap<java.lang.String, android.graphics.Bitmap> contactPhotos;
    private android.content.Context context;
    com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode currentMode = com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.EMPTY;
    private com.navdy.client.app.ui.base.BaseGoogleMapFragment googleMapFragment;
    private java.lang.String lastQuery;
    private android.view.View.OnClickListener listener;
    private java.util.HashMap<java.lang.String, java.lang.Integer> markers = new java.util.HashMap<>();
    private final com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    private java.util.List<com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem> searchItems;
    private final int searchType;

    class Anon1 implements android.view.View.OnClickListener {
        final /* synthetic */ int val$position;

        Anon1(int i) {
            this.val$position = i;
        }

        public void onClick(android.view.View v) {
            v.setTag(java.lang.Integer.valueOf(this.val$position));
            com.navdy.client.app.ui.search.SearchRecyclerAdapter.this.listener.onClick(v);
            com.navdy.client.app.ui.search.SearchRecyclerAdapter.logger.v("click on item at position " + this.val$position);
        }
    }

    class Anon2 implements android.view.View.OnClickListener {
        final /* synthetic */ int val$position;

        Anon2(int i) {
            this.val$position = i;
        }

        public void onClick(android.view.View v) {
            v.setTag(java.lang.Integer.valueOf(this.val$position));
            com.navdy.client.app.ui.search.SearchRecyclerAdapter.this.listener.onClick(v);
        }
    }

    class Anon3 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ java.util.List val$items;

        Anon3(java.util.List list) {
            this.val$items = list;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... params) {
            for (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem : this.val$items) {
                searchItem.destination.reloadSelfFromDatabase();
                com.navdy.client.app.ui.search.SearchRecyclerAdapter.this.setLastRoutedIfRecent(searchItem);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            com.navdy.client.app.ui.search.SearchRecyclerAdapter.this.notifyDataSetChanged();
        }
    }

    class Anon4 implements com.google.android.gms.maps.OnMapReadyCallback {
        Anon4() {
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            if (googleMap != null) {
                try {
                    googleMap.clear();
                } catch (java.lang.IllegalArgumentException e) {
                    com.navdy.client.app.ui.search.SearchRecyclerAdapter.logger.e("Unable to clear the google map due to IllegalArgumentException.", e);
                }
            }
        }
    }

    class Anon5 implements com.google.android.gms.maps.OnMapReadyCallback {
        final /* synthetic */ int val$destinationIndex;

        Anon5(int i) {
            this.val$destinationIndex = i;
        }

        public void onMapReady(com.google.android.gms.maps.GoogleMap googleMap) {
            com.google.android.gms.maps.model.LatLng latlong;
            com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.ui.search.SearchRecyclerAdapter.this.getDestinationAt(this.val$destinationIndex);
            if (destination == null) {
                com.navdy.client.app.ui.search.SearchRecyclerAdapter.logger.e("Unable to retrieve a destination from position: " + this.val$destinationIndex);
            } else if (!destination.hasOneValidSetOfCoordinates()) {
                com.navdy.client.app.ui.search.SearchRecyclerAdapter.logger.e("Unable to retrieve valid coordinates for the destination at position: " + this.val$destinationIndex);
            } else {
                com.google.android.gms.maps.model.MarkerOptions mo = new com.google.android.gms.maps.model.MarkerOptions();
                if (destination.hasValidDisplayCoordinates()) {
                    latlong = new com.google.android.gms.maps.model.LatLng(destination.displayLat, destination.displayLong);
                } else {
                    latlong = new com.google.android.gms.maps.model.LatLng(destination.navigationLat, destination.navigationLong);
                }
                mo.position(latlong);
                mo.draggable(false);
                mo.icon(com.google.android.gms.maps.model.BitmapDescriptorFactory.fromResource(destination.getUnselectedPinAsset()));
                com.navdy.client.app.ui.search.SearchRecyclerAdapter.this.markers.put(googleMap.addMarker(mo).getId(), java.lang.Integer.valueOf(this.val$destinationIndex));
            }
        }
    }

    public enum ImageEnum {
        PLACE,
        CONTACT,
        CONTACT_PHOTO,
        SEARCH,
        SEARCH_HISTORY,
        NONE
    }

    enum Mode {
        EMPTY,
        AUTO_COMPLETE,
        FULL_SEARCH
    }

    static class SearchItem {
        public com.navdy.client.app.framework.models.ContactModel contact;
        public com.navdy.client.app.framework.models.Destination destination;
        public java.lang.Double distance;
        com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum imageEnum;
        java.lang.String price;

        SearchItem(com.navdy.client.app.framework.models.Destination destination2, java.lang.Double distance2, java.lang.String price2, com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum imageEnum2, com.navdy.client.app.framework.models.ContactModel contact2) {
            this.destination = destination2;
            this.distance = distance2;
            this.price = price2;
            this.imageEnum = imageEnum2;
            this.contact = contact2;
        }

        int getRes(boolean isAutocomplete) {
            switch (this.imageEnum) {
                case PLACE:
                    if (isAutocomplete) {
                        return this.destination.getBadgeAssetForAutoComplete();
                    }
                    return this.destination.getBadgeAssetForSearchResults();
                case CONTACT:
                    return com.navdy.client.R.drawable.ic_person_black;
                case SEARCH:
                case SEARCH_HISTORY:
                    return com.navdy.client.R.drawable.recent_search_icon;
                default:
                    return 0;
            }
        }
    }

    SearchRecyclerAdapter(int searchType2, com.navdy.client.app.ui.base.BaseGoogleMapFragment googleMapFragment2, android.widget.RelativeLayout card2) {
        this.searchType = searchType2;
        this.searchItems = new java.util.ArrayList();
        this.contactPhotos = new java.util.HashMap<>();
        this.context = com.navdy.client.app.NavdyApplication.getAppContext();
        this.googleMapFragment = googleMapFragment2;
        this.card = card2;
        logger.v("bus registered");
    }

    void setUpServicesAndHistory(boolean showService) {
        if (showService && (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.context) || com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected())) {
            addSearchPoweredByGoogleHeader();
            android.location.Location lastLocation = this.navdyLocationManager.getSmartStartLocation();
            if (!(lastLocation == null || lastLocation.getLongitude() == 0.0d || lastLocation.getLatitude() == 0.0d)) {
                com.navdy.client.app.framework.models.Destination servicesDestination = new com.navdy.client.app.framework.models.Destination();
                servicesDestination.name = "";
                servicesDestination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.SERVICES_SEARCH.getValue();
                this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(servicesDestination, java.lang.Double.valueOf(-1.0d), "", com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.NONE, null));
            }
        }
        android.database.Cursor cursor = null;
        try {
            cursor = com.navdy.client.app.providers.NavdyContentProvider.getSearchHistoryCursor();
            while (cursor != null && cursor.moveToNext()) {
                int idIndex = cursor.getColumnIndex("_id");
                int queryIndex = cursor.getColumnIndex("query");
                int id = cursor.getInt(idIndex);
                java.lang.String query = cursor.getString(queryIndex);
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                destination.id = id;
                destination.name = query;
                destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_HISTORY.getValue();
                this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination, java.lang.Double.valueOf(-1.0d), "", com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.SEARCH_HISTORY, null));
            }
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    void setClickListener(android.view.View.OnClickListener onClickListener) {
        logger.v("setClickListener: " + onClickListener);
        this.listener = onClickListener;
    }

    public int getItemCount() {
        return (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH ? 1 : 0) + this.searchItems.size();
    }

    @android.support.annotation.Nullable
    com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem getItemAt(int position) {
        if (position < 0 || position >= this.searchItems.size()) {
            return null;
        }
        return (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem) this.searchItems.get(position);
    }

    @android.support.annotation.Nullable
    com.navdy.client.app.framework.models.Destination getDestinationAt(int position) {
        com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem item = getItemAt(position);
        if (item != null) {
            return item.destination;
        }
        return null;
    }

    public int getItemViewType(int position) {
        if (position >= this.searchItems.size()) {
            return com.navdy.client.app.framework.models.Destination.SearchType.FOOTER.getValue();
        }
        com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem = (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem) this.searchItems.get(position);
        com.navdy.client.app.framework.models.Destination destination = null;
        if (searchItem != null) {
            destination = searchItem.destination;
        }
        if (destination != null) {
            return destination.searchResultType;
        }
        return com.navdy.client.app.framework.models.Destination.SearchType.UNKNOWN.getValue();
    }

    public com.navdy.client.app.ui.search.SearchViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
        android.view.View v;
        if (isHeader(viewType)) {
            v = android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.search_header_powered_by_google, parent, false);
            setTitleForHeader(v, viewType);
        } else if (viewType == com.navdy.client.app.framework.models.Destination.SearchType.SERVICES_SEARCH.getValue()) {
            return new com.navdy.client.app.ui.search.SearchViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.search_list_item_services, parent, false));
        } else {
            if (viewType == com.navdy.client.app.framework.models.Destination.SearchType.NO_RESULTS.getValue()) {
                return new com.navdy.client.app.ui.search.SearchViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.search_list_item_no_results, parent, false));
            }
            if (viewType == com.navdy.client.app.framework.models.Destination.SearchType.FOOTER.getValue()) {
                return new com.navdy.client.app.ui.search.SearchViewHolder(android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.padding_footer_layout, parent, false));
            }
            v = android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.search_list_item, parent, false);
        }
        return new com.navdy.client.app.ui.search.SearchViewHolder(v);
    }

    private void setTitleForHeader(android.view.View v, int viewType) {
        android.widget.TextView leftTitle = (android.widget.TextView) v.findViewById(com.navdy.client.R.id.left_title);
        android.widget.ImageView rightSideImage = (android.widget.ImageView) v.findViewById(com.navdy.client.R.id.right_side_image);
        if (leftTitle == null) {
            logger.e("The Search Header view doesn't exist");
        } else if (viewType == com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue()) {
            leftTitle.setText(com.navdy.client.R.string.search_powered_by);
            if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.context) && rightSideImage != null) {
                rightSideImage.setImageResource(com.navdy.client.R.drawable.google_2015_logo);
                rightSideImage.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onBindViewHolder(com.navdy.client.app.ui.search.SearchViewHolder searchViewHolder, int position) {
        setCardData(searchViewHolder, position);
    }

    void setCardData(com.navdy.client.app.ui.search.SearchViewHolder searchViewHolder, int position) {
        int itemViewType = getItemViewType(position);
        if (itemViewType != com.navdy.client.app.framework.models.Destination.SearchType.FOOTER.getValue()) {
            com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem = (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem) this.searchItems.get(position);
            if (searchItem == null) {
                logger.e("Unable to get the searchItem at position: " + position);
                return;
            }
            com.navdy.client.app.framework.models.Destination destination = searchItem.destination;
            if (destination == null) {
                logger.e("Unable to get the destination at position: " + position);
            } else if (searchViewHolder == null) {
                logger.e("Unable to get the searchViewHolder at position: " + position);
            } else if (isHeader(itemViewType)) {
                android.view.View v = searchViewHolder.itemView;
                setTitleForHeader(v, itemViewType);
                android.widget.ImageView rightSideImage = (android.widget.ImageView) v.findViewById(com.navdy.client.R.id.right_side_image);
                if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.context)) {
                    if (rightSideImage != null) {
                        rightSideImage.setVisibility(View.INVISIBLE);
                    }
                } else if (itemViewType == com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue()) {
                    if (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE) {
                        v.setBackgroundResource(com.navdy.client.R.color.white);
                    } else {
                        v.setBackgroundResource(com.navdy.client.R.color.search_header_online_background);
                    }
                    if (rightSideImage != null) {
                        rightSideImage.setImageResource(com.navdy.client.R.drawable.google_2015_logo);
                        rightSideImage.setVisibility(View.VISIBLE);
                    }
                }
                if (position == getItemCount() - 1 || this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE) {
                    searchViewHolder.hideBottomDivider();
                } else {
                    searchViewHolder.showLongDivider();
                }
            } else {
                if (position == getItemCount() - 1) {
                    searchViewHolder.hideBottomDivider();
                } else if (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH) {
                    searchViewHolder.showLongDivider();
                } else {
                    searchViewHolder.showShortDivider();
                }
                if (itemViewType != com.navdy.client.app.framework.models.Destination.SearchType.NO_RESULTS.getValue() && itemViewType != com.navdy.client.app.framework.models.Destination.SearchType.SERVICES_SEARCH.getValue() && position < this.searchItems.size() && !this.searchItems.isEmpty()) {
                    android.util.Pair<java.lang.String, java.lang.String> titleAndSubtitle = destination.getTitleAndSubtitle();
                    java.lang.String title = (java.lang.String) titleAndSubtitle.first;
                    if (destination.searchResultType == com.navdy.client.app.framework.models.Destination.SearchType.MORE.getValue()) {
                        searchViewHolder.setTitle(title);
                    } else if (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE) {
                        android.text.Spannable highlightedText = highlightSearchedText(title, this.lastQuery);
                        if (highlightedText != null) {
                            searchViewHolder.setTitle(highlightedText);
                        } else {
                            searchViewHolder.setTitle(title, com.navdy.client.R.color.grey);
                        }
                    } else {
                        searchViewHolder.setTitle(title, com.navdy.client.R.color.black);
                    }
                    java.lang.String subTitle = (java.lang.String) titleAndSubtitle.second;
                    if (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE) {
                        android.text.Spannable highlightedText2 = highlightSearchedText(subTitle, this.lastQuery);
                        if (highlightedText2 != null) {
                            searchViewHolder.setDetails(highlightedText2);
                        } else {
                            searchViewHolder.setDetails(subTitle, com.navdy.client.R.color.grey);
                        }
                    } else {
                        searchViewHolder.setDetails(subTitle);
                    }
                    if (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH) {
                        setRowHeightTo(searchViewHolder.row, com.navdy.client.R.dimen.card_height);
                    } else {
                        setRowHeightTo(searchViewHolder.row, com.navdy.client.R.dimen.card_height_small);
                    }
                    boolean isAutocomplete = this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.AUTO_COMPLETE;
                    switch (searchItem.imageEnum) {
                        case PLACE:
                            searchViewHolder.setImage(searchItem, isAutocomplete);
                            searchViewHolder.showNavButton();
                            break;
                        case CONTACT:
                            searchViewHolder.setImage(title);
                            searchViewHolder.showNavButton();
                            break;
                        case CONTACT_PHOTO:
                            searchViewHolder.setImage((android.graphics.Bitmap) this.contactPhotos.get(title));
                            searchViewHolder.showNavButton();
                            break;
                        case SEARCH_HISTORY:
                            searchViewHolder.setImage(searchItem, isAutocomplete);
                            searchViewHolder.showDeleteButton();
                            break;
                        default:
                            searchViewHolder.setImage(searchItem, isAutocomplete);
                            searchViewHolder.hideNavButton();
                            break;
                    }
                    if (itemViewType == com.navdy.client.app.framework.models.Destination.SearchType.CONTACT_ADDRESS_SELECTION.getValue() || (com.navdy.client.app.ui.search.SearchActivity.isFavoriteSearch(this.searchType) && destination.searchResultType != com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_HISTORY.getValue())) {
                        searchViewHolder.hideNavButton();
                    }
                    searchViewHolder.setDistance(searchItem.distance.doubleValue());
                    searchViewHolder.setPrice(searchItem.price);
                    searchViewHolder.row.setOnClickListener(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.Anon1(position));
                    searchViewHolder.navButton.setOnClickListener(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.Anon2(position));
                }
            }
        }
    }

    private void setRowHeightTo(android.view.View row, int dimenRes) {
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) row.getLayoutParams();
        params.height = (int) ((float) this.context.getResources().getDimensionPixelSize(dimenRes));
        row.setLayoutParams(params);
    }

    private static boolean isHeader(int itemViewType) {
        return itemViewType == com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue();
    }

    private static android.text.Spannable highlightSearchedText(java.lang.String autocompleteName, java.lang.String query) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(autocompleteName) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
            return null;
        }
        java.lang.String autocompleteNameLowercase = autocompleteName.toLowerCase();
        java.lang.String queryLowercase = query.toLowerCase();
        android.text.Spannable queryWithHighlightedSelection = android.text.Spannable.Factory.getInstance().newSpannable(autocompleteName);
        int startIndex = 0;
        int length = autocompleteName.length();
        int queryLength = queryLowercase.length();
        while (true) {
            int lastEndIndex = startIndex;
            int startIndex2 = autocompleteNameLowercase.indexOf(queryLowercase, lastEndIndex);
            if (startIndex2 >= 0) {
                int endIndex = startIndex2 + queryLength;
                if (endIndex > length) {
                    endIndex = length;
                }
                if (startIndex2 > lastEndIndex) {
                    queryWithHighlightedSelection.setSpan(new android.text.style.ForegroundColorSpan(-7829368), lastEndIndex, startIndex2, 0);
                }
                queryWithHighlightedSelection.setSpan(new android.text.style.ForegroundColorSpan(-16777216), startIndex2, endIndex, 0);
                startIndex = endIndex;
                if (startIndex >= length) {
                    break;
                }
            } else {
                startIndex = lastEndIndex;
                break;
            }
        }
        if (startIndex >= length) {
            return queryWithHighlightedSelection;
        }
        queryWithHighlightedSelection.setSpan(new android.text.style.ForegroundColorSpan(-7829368), startIndex, length, 0);
        return queryWithHighlightedSelection;
    }

    void updateRecyclerViewWithAutocomplete(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> newAddresses, @android.support.annotation.Nullable java.util.ArrayList<java.lang.String> previousSearches, @android.support.annotation.Nullable java.lang.CharSequence constraint) {
        if (newAddresses == null) {
            newAddresses = new java.util.ArrayList<>(0);
        }
        if (previousSearches == null) {
            previousSearches = new java.util.ArrayList<>(0);
        }
        logger.v("updateRecyclerViewWithAutocomplete");
        for (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult newAddress : newAddresses) {
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newAddress.place_id)) {
                insertAutoCompleteItem(newAddress, true);
            }
        }
        if (constraint != null) {
            createSearchForMoreFooter(constraint.toString());
        } else {
            constraint = "";
        }
        java.util.ArrayList<java.lang.String> googleSearchSuggestions = new java.util.ArrayList<>(newAddresses.size());
        for (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult newAddress2 : newAddresses) {
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(newAddress2.place_id) && !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(newAddress2.name, constraint)) {
                insertAutoCompleteItem(newAddress2);
                googleSearchSuggestions.add(newAddress2.name.toLowerCase());
            }
        }
        java.util.Iterator it = previousSearches.iterator();
        while (it.hasNext()) {
            java.lang.String previousSearch = (java.lang.String) it.next();
            if (!googleSearchSuggestions.contains(previousSearch.toLowerCase()) && !previousSearch.equalsIgnoreCase(constraint.toString())) {
                insertPreviousSearch(previousSearch);
                googleSearchSuggestions.add(previousSearch.toLowerCase());
            }
        }
    }

    private void insertAutoCompleteItem(com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult newAddress) {
        insertAutoCompleteItem(newAddress, false);
    }

    private void insertAutoCompleteItem(com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult newAddress, boolean checkForDuplicates) {
        com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
        destination.name = newAddress.name;
        destination.rawAddressNotForDisplay = newAddress.address;
        destination.placeId = newAddress.place_id;
        destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.AUTOCOMPLETE.getValue();
        if (checkForDuplicates) {
            for (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem : this.searchItems) {
                if (destination.equals(searchItem.destination)) {
                    return;
                }
            }
        }
        com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum imageEnum = com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.PLACE;
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.placeId)) {
            imageEnum = com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.SEARCH;
        }
        this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination, java.lang.Double.valueOf(-1.0d), "", imageEnum, null));
        notifyItemInserted(this.searchItems.size() - 1);
    }

    private void insertPreviousSearch(java.lang.String previousSearch) {
        com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
        destination.name = previousSearch;
        destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.AUTOCOMPLETE.getValue();
        this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination, java.lang.Double.valueOf(-1.0d), "", com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.SEARCH, null));
        notifyItemInserted(this.searchItems.size() - 1);
    }

    void updateRecyclerViewWithDatabaseData(@android.support.annotation.Nullable java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinationsFromDatabase, java.lang.String query) {
        if (destinationsFromDatabase != null && !destinationsFromDatabase.isEmpty()) {
            logger.v("updateRecyclerViewWithDatabaseData");
            java.util.Iterator<com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem> searchIt = this.searchItems.iterator();
            while (searchIt.hasNext()) {
                com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem = (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem) searchIt.next();
                if (searchItem == null) {
                    searchIt.remove();
                } else {
                    java.util.Iterator<com.navdy.client.app.framework.models.Destination> iterator = destinationsFromDatabase.iterator();
                    while (iterator.hasNext()) {
                        com.navdy.client.app.framework.models.Destination destination = (com.navdy.client.app.framework.models.Destination) iterator.next();
                        if (destination.equals(searchItem.destination)) {
                            searchItem.destination = destination;
                            setLastRoutedIfRecent(searchItem);
                            iterator.remove();
                        }
                    }
                }
            }
            com.navdy.client.app.framework.util.VoiceCommandUtils.addHomeAndWorkIfMatch(destinationsFromDatabase, query);
            java.util.Iterator it = destinationsFromDatabase.iterator();
            while (it.hasNext()) {
                com.navdy.client.app.framework.models.Destination destination2 = (com.navdy.client.app.framework.models.Destination) it.next();
                com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum imageEnum = destination2.getImageEnum();
                destination2.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.RECENT_PLACES.getValue();
                java.lang.String lastRoutedOn = "";
                if (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH && destination2.isRecentDestination()) {
                    lastRoutedOn = this.context.getString(com.navdy.client.R.string.last_routed_on, new java.lang.Object[]{com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(destination2.lastRoutedDate)});
                }
                this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination2, java.lang.Double.valueOf(-1.0d), lastRoutedOn, imageEnum, null));
                addMarker(this.searchItems.size() - 1);
            }
            notifyDataSetChanged();
        }
    }

    private void setLastRoutedIfRecent(com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem) {
        if (this.currentMode == com.navdy.client.app.ui.search.SearchRecyclerAdapter.Mode.FULL_SEARCH && searchItem.destination.isRecentDestination()) {
            searchItem.destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.RECENT_PLACES.getValue();
            java.lang.String dateAndTime = com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(searchItem.destination.lastRoutedDate);
            searchItem.price = this.context.getString(com.navdy.client.R.string.last_routed_on, new java.lang.Object[]{dateAndTime});
            searchItem.distance = java.lang.Double.valueOf(0.0d);
        }
    }

    void updateRecyclerViewWithPlaceSearchResults(java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> placesSearchResults) {
        java.lang.Double distance;
        if (placesSearchResults != null && !placesSearchResults.isEmpty()) {
            logger.v("updateRecyclerViewWithPlaceSearchResults");
            addSearchPoweredByGoogleHeader();
            for (com.navdy.service.library.events.places.PlacesSearchResult placesSearchResult : placesSearchResults) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                com.navdy.client.app.framework.models.Destination.placesSearchResultToDestinationObject(placesSearchResult, destination);
                if (placesSearchResult.distance != null) {
                    distance = placesSearchResult.distance;
                } else {
                    distance = java.lang.Double.valueOf(-1.0d);
                }
                this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination, distance, "", com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.PLACE, null));
                addMarker(this.searchItems.size() - 1);
            }
            notifyDataSetChanged();
        }
    }

    void updateRecyclerViewWithTextSearchResults(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> textSearchResults) {
        if (textSearchResults != null && !textSearchResults.isEmpty()) {
            logger.v("updateRecyclerViewWithTextSearchResults");
            addSearchPoweredByGoogleHeader();
            for (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult result : textSearchResults) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                result.toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.TEXT_SEARCH, destination);
                this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination, java.lang.Double.valueOf(result.distance), getPriceString(result.price_level), destination.getImageEnum(), null));
                addMarker(this.searchItems.size() - 1);
            }
            notifyDataSetChanged();
        }
    }

    void updateNearbySearchResultsWithKnownDestinationData() {
        new com.navdy.client.app.ui.search.SearchRecyclerAdapter.Anon3(this.searchItems).execute(new java.lang.Void[0]);
    }

    void updateRecyclerViewWithContactsForTextSearch(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts) {
        if (contacts != null && contacts.size() > 0) {
            logger.v("updateRecyclerViewWithContactsForTextSearch");
            for (com.navdy.client.app.framework.models.ContactModel contact : contacts) {
                java.util.ArrayList<com.navdy.client.app.framework.models.Address> addresses = getDeduplicatedAddressesForContact(contact);
                if (addresses.size() > 0) {
                    java.util.Iterator it = addresses.iterator();
                    while (it.hasNext()) {
                        com.navdy.client.app.framework.models.Address address = (com.navdy.client.app.framework.models.Address) it.next();
                        if (address != null) {
                            addContactWithOneAddress(contact, address);
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    void updateRecyclerViewWithContactsForAutoComplete(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts) {
        if (contacts != null && contacts.size() > 0) {
            logger.v("updateRecyclerViewWithContactsForAutoComplete");
            for (com.navdy.client.app.framework.models.ContactModel contact : contacts) {
                java.util.ArrayList<com.navdy.client.app.framework.models.Address> addresses = getDeduplicatedAddressesForContact(contact);
                if (addresses.size() > 0) {
                    if (addresses.size() == 1) {
                        addContactWithOneAddress(contact, (com.navdy.client.app.framework.models.Address) addresses.get(0));
                    } else {
                        addContactWithMultipleAddress(contact, addresses);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    @android.support.annotation.NonNull
    private java.util.ArrayList<com.navdy.client.app.framework.models.Address> getDeduplicatedAddressesForContact(@android.support.annotation.Nullable com.navdy.client.app.framework.models.ContactModel contact) {
        if (contact == null || contact.addresses == null || contact.addresses.isEmpty()) {
            return new java.util.ArrayList<>();
        }
        java.util.ArrayList<java.lang.String> addedAddresses = new java.util.ArrayList<>(contact.addresses.size());
        java.util.ArrayList<com.navdy.client.app.framework.models.Address> validAddresses = new java.util.ArrayList<>(contact.addresses.size());
        for (com.navdy.client.app.framework.models.Address address : contact.addresses) {
            if (address != null) {
                java.lang.String fullAddress = getCleanAddressString(address);
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(fullAddress) && !addedAddresses.contains(fullAddress)) {
                    addedAddresses.add(fullAddress);
                    validAddresses.add(address);
                }
            }
        }
        return validAddresses;
    }

    public static java.lang.String getCleanAddressString(@android.support.annotation.Nullable com.navdy.client.app.framework.models.Address address) {
        if (address == null) {
            return "";
        }
        java.lang.String fullAddress = address.getFullAddress();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(fullAddress)) {
            return "";
        }
        return fullAddress.replace("\n", " ");
    }

    private void addContactWithOneAddress(@android.support.annotation.Nullable com.navdy.client.app.framework.models.ContactModel contact, @android.support.annotation.Nullable com.navdy.client.app.framework.models.Address address) {
        java.lang.String addressType;
        com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum imageEnum;
        if (contact != null && address != null) {
            java.lang.String fullAddress = getCleanAddressString(address);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(fullAddress)) {
                switch (address.type) {
                    case 0:
                        addressType = address.label;
                        break;
                    case 1:
                        addressType = this.context.getString(com.navdy.client.R.string.home_address_type);
                        break;
                    case 2:
                        addressType = this.context.getString(com.navdy.client.R.string.work_address_type);
                        break;
                    default:
                        addressType = this.context.getString(com.navdy.client.R.string.other_address_type);
                        break;
                }
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                destination.name = contact.name;
                destination.rawAddressNotForDisplay = fullAddress;
                destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.CONTACT.getValue();
                destination.type = com.navdy.client.app.framework.models.Destination.Type.CONTACT;
                destination.contactLookupKey = contact.lookupKey;
                destination.lastKnownContactId = contact.id;
                destination.lastContactLookup = contact.lookupTimestamp;
                if (contact.photo != null) {
                    imageEnum = com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.CONTACT_PHOTO;
                    this.contactPhotos.put(contact.name, contact.photo);
                } else {
                    imageEnum = com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.CONTACT;
                }
                this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination, java.lang.Double.valueOf(-1.0d), addressType, imageEnum, null));
            }
        }
    }

    private void addContactWithMultipleAddress(@android.support.annotation.Nullable com.navdy.client.app.framework.models.ContactModel contact, @android.support.annotation.Nullable java.util.ArrayList<com.navdy.client.app.framework.models.Address> preDeduplicatedAddresses) {
        com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum imageEnum;
        if (contact != null && preDeduplicatedAddresses != null) {
            com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
            destination.name = contact.name;
            destination.rawAddressNotForDisplay = this.context.getResources().getQuantityString(com.navdy.client.R.plurals.this_many_addresses, preDeduplicatedAddresses.size(), new java.lang.Object[]{java.lang.Integer.valueOf(preDeduplicatedAddresses.size())});
            destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.CONTACT_ADDRESS_SELECTION.getValue();
            if (contact.photo != null) {
                imageEnum = com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.CONTACT_PHOTO;
                this.contactPhotos.put(contact.name, contact.photo);
            } else {
                imageEnum = com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.CONTACT;
            }
            contact.addresses = preDeduplicatedAddresses;
            this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(destination, java.lang.Double.valueOf(-1.0d), "", imageEnum, contact));
        }
    }

    void updateRecyclerViewWithContact(com.navdy.client.app.framework.models.ContactModel contact) {
        if (contact == null || contact.addresses == null) {
            addNoResultsItem();
            return;
        }
        logger.v("updateRecyclerViewWithContact");
        for (com.navdy.client.app.framework.models.Address address : contact.addresses) {
            addContactWithOneAddress(contact, address);
        }
    }

    void createSearchForMoreFooter(java.lang.String autocompleteQuery) {
        com.navdy.client.app.framework.models.Destination moreDestination = new com.navdy.client.app.framework.models.Destination();
        moreDestination.name = autocompleteQuery;
        moreDestination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.MORE.getValue();
        this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(moreDestination, java.lang.Double.valueOf(-1.0d), "", com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.SEARCH, null));
        notifyDataSetChanged();
    }

    private boolean firstItemIsPoweredByGoogleHeader() {
        if (this.searchItems == null || this.searchItems.size() <= 0) {
            return false;
        }
        com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem = (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem) this.searchItems.get(0);
        if (searchItem == null || searchItem.destination == null || searchItem.destination.searchResultType != com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue()) {
            return false;
        }
        return true;
    }

    void addSearchPoweredByGoogleHeader() {
        if (com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() && !firstItemIsPoweredByGoogleHeader()) {
            com.navdy.client.app.framework.models.Destination header = new com.navdy.client.app.framework.models.Destination();
            header.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.SEARCH_POWERED_BY_GOOGLE_HEADER.getValue();
            this.searchItems.add(0, new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(header, java.lang.Double.valueOf(-1.0d), "", com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.NONE, null));
        }
    }

    void removeSearchPoweredByGoogleHeader() {
        if (firstItemIsPoweredByGoogleHeader()) {
            this.searchItems.remove(0);
        }
    }

    void addNoResultsItemIfEmpty(java.util.List... lists) {
        if (lists == null || lists.length <= 0) {
            addNoResultsItem();
            return;
        }
        int length = lists.length;
        int i = 0;
        while (i < length) {
            java.util.List list = lists[i];
            if (list == null || list.isEmpty()) {
                i++;
            } else {
                return;
            }
        }
        addNoResultsItem();
    }

    void addNoResultsItem() {
        com.navdy.client.app.framework.models.Destination noResultItem = new com.navdy.client.app.framework.models.Destination();
        noResultItem.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.NO_RESULTS.getValue();
        this.searchItems.add(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem(noResultItem, java.lang.Double.valueOf(-1.0d), "", com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.NONE, null));
    }

    public static java.lang.String getPriceString(java.lang.String level) {
        if (level == null) {
            return "";
        }
        java.lang.StringBuilder stringPrice = new java.lang.StringBuilder();
        try {
            int price = java.lang.Integer.parseInt(level);
            for (int i = 0; i < price; i++) {
                stringPrice.append("$");
            }
        } catch (java.lang.Exception e) {
            logger.w("Unable to parse a price string out of " + level);
        }
        return stringPrice.toString();
    }

    void clear() {
        logger.v("clear");
        this.searchItems.clear();
        notifyDataSetChanged();
        this.contactPhotos.clear();
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.Anon4());
        }
        if (this.markers != null) {
            this.markers.clear();
        }
        if (this.card != null) {
            this.card.setVisibility(View.GONE);
        }
    }

    private void addMarker(int destinationIndex) {
        if (this.googleMapFragment != null) {
            try {
                this.googleMapFragment.getMapAsync(new com.navdy.client.app.ui.search.SearchRecyclerAdapter.Anon5(destinationIndex));
            } catch (java.lang.Exception e) {
                logger.e("Unable to add marker: " + e);
            }
        }
    }

    java.lang.Integer getDestinationIndexForMarker(java.lang.String markerId) {
        return (java.lang.Integer) this.markers.get(markerId);
    }

    void centerMapOnMarkers(boolean animate) {
        if (this.googleMapFragment != null) {
            java.util.List<com.google.android.gms.maps.model.LatLng> sanitizedLocations = new java.util.ArrayList<>();
            com.navdy.service.library.events.location.Coordinate myLocation = this.navdyLocationManager.getSmartStartCoordinates();
            if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(myLocation)) {
                sanitizedLocations.add(new com.google.android.gms.maps.model.LatLng(myLocation.latitude.doubleValue(), myLocation.longitude.doubleValue()));
            }
            if (this.searchItems != null) {
                for (com.navdy.client.app.ui.search.SearchRecyclerAdapter.SearchItem searchItem : this.searchItems) {
                    com.navdy.client.app.framework.models.Destination destination = searchItem.destination;
                    if (destination != null) {
                        com.google.android.gms.maps.model.LatLng position = null;
                        if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(destination.displayLat, destination.displayLong)) {
                            position = new com.google.android.gms.maps.model.LatLng(destination.displayLat, destination.displayLong);
                        } else if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(destination.navigationLat, destination.navigationLong)) {
                            position = new com.google.android.gms.maps.model.LatLng(destination.navigationLat, destination.navigationLong);
                        }
                        if (position != null) {
                            sanitizedLocations.add(position);
                        }
                    }
                }
            }
            if (sanitizedLocations.size() > 1) {
                com.google.android.gms.maps.model.LatLngBounds.Builder builder = com.google.android.gms.maps.model.LatLngBounds.builder();
                for (com.google.android.gms.maps.model.LatLng latLng : sanitizedLocations) {
                    builder.include(latLng);
                }
                this.googleMapFragment.zoomTo(builder.build(), animate);
            } else if (sanitizedLocations.size() > 0) {
                this.googleMapFragment.moveMap((com.google.android.gms.maps.model.LatLng) sanitizedLocations.get(0), 14.0f, animate);
            }
        }
    }

    void setLastQuery(java.lang.String query) {
        this.lastQuery = query;
    }
}
