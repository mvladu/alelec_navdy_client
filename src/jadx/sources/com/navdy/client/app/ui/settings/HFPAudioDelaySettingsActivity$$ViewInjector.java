package com.navdy.client.app.ui.settings;

public class HFPAudioDelaySettingsActivity$$ViewInjector {
    public static void inject(butterknife.ButterKnife.Finder finder, com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity target, java.lang.Object source) {
        target.btnPlaySequence = (android.widget.Button) finder.findRequiredView(source, com.navdy.client.R.id.play_sequence, "field 'btnPlaySequence'");
        target.multipleChoiceLayout = (com.navdy.client.app.ui.customviews.MultipleChoiceLayout) finder.findRequiredView(source, com.navdy.client.R.id.sequence_delay_choice_layout, "field 'multipleChoiceLayout'");
    }

    public static void reset(com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity target) {
        target.btnPlaySequence = null;
        target.multipleChoiceLayout = null;
    }
}
