package com.navdy.client.app.ui.routing;

public class ActiveTripActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity implements com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener {
    private static final int HERE_PADDING = com.navdy.client.app.framework.map.MapUtils.hereMapSidePadding;
    private static final float MAP_ORIENTATION = 0.0f;
    private com.here.android.mpa.mapping.MapPolyline currentProgressMapPolyline;
    private com.here.android.mpa.mapping.MapMarker destinationMapMarker;
    private com.navdy.client.app.ui.base.BaseHereMapFragment hereMapFragment;
    private final com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    private com.here.android.mpa.mapping.MapPolyline routeMapPolyline;

    class Anon1 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        Anon1() {
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            hereMap.setTrafficInfoVisible(true);
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
            com.navdy.client.app.ui.routing.ActiveTripActivity.this.logger.e("HereMapFragment failed to initialize");
        }
    }

    class Anon2 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        Anon2() {
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            android.location.Location location = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartLocation();
            if (location != null) {
                hereMap.setCenter(new com.here.android.mpa.common.GeoCoordinate(location.getLatitude(), location.getLongitude()), com.here.android.mpa.mapping.Map.Animation.BOW);
            }
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon3 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        Anon3() {
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            hereMap.setOrientation(0.0f, com.here.android.mpa.mapping.Map.Animation.BOW);
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon4 implements android.content.DialogInterface.OnClickListener {
        Anon4() {
        }

        public void onClick(android.content.DialogInterface dialog, int which) {
            if (which == -1) {
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.navdyRouteHandler.stopRouting();
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.finish();
            }
        }
    }

    class Anon5 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        Anon5(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destination = destination;
        }

        public void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker = this.val$destination.getHereMapMarker();
            if (com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker != null) {
                hereMap.addMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker);
            }
            com.navdy.client.app.ui.routing.ActiveTripActivity.this.hereMapFragment.centerOnUserLocationAndDestination(this.val$destination, com.here.android.mpa.mapping.Map.Animation.LINEAR);
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon6 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        final /* synthetic */ com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo val$progress;

        Anon6(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo) {
            this.val$progress = navdyRouteInfo;
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            if (com.navdy.client.app.ui.routing.ActiveTripActivity.this.currentProgressMapPolyline != null) {
                hereMap.removeMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.currentProgressMapPolyline);
            }
            com.here.android.mpa.common.GeoPolyline progressPolyline = this.val$progress.getProgress();
            if (progressPolyline != null) {
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.currentProgressMapPolyline = com.navdy.client.app.framework.map.MapUtils.generateProgressPolyline(progressPolyline);
                hereMap.addMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.currentProgressMapPolyline);
            }
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon7 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        Anon7() {
        }

        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            if (com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker != null) {
                hereMap.removeMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker);
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker = null;
            }
            if (com.navdy.client.app.ui.routing.ActiveTripActivity.this.routeMapPolyline != null) {
                hereMap.removeMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.routeMapPolyline);
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.routeMapPolyline = null;
            }
            if (com.navdy.client.app.ui.routing.ActiveTripActivity.this.currentProgressMapPolyline != null) {
                hereMap.removeMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.currentProgressMapPolyline);
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.currentProgressMapPolyline = null;
            }
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    class Anon8 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady {
        final /* synthetic */ com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo val$route;

        Anon8(com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo navdyRouteInfo) {
            this.val$route = navdyRouteInfo;
        }

        public void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker = this.val$route.getDestination().getHereMapMarker();
            if (com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker != null) {
                hereMap.addMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.destinationMapMarker);
            }
            com.here.android.mpa.common.GeoPolyline geoPolyline = this.val$route.getRoute();
            if (geoPolyline != null) {
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.routeMapPolyline = com.navdy.client.app.framework.map.MapUtils.generateRoutePolyline(geoPolyline);
                hereMap.addMapObject(com.navdy.client.app.ui.routing.ActiveTripActivity.this.routeMapPolyline);
                com.navdy.client.app.ui.routing.ActiveTripActivity.this.hereMapFragment.centerOnRoute(geoPolyline);
                return;
            }
            com.navdy.client.app.ui.routing.ActiveTripActivity.this.routeMapPolyline = null;
        }

        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    public static void startActiveTripActivity(android.content.Context context) {
        if (context == null || !(context instanceof android.app.Activity)) {
            sLogger.e("startActiveTripActivity, context is invalid");
        } else if (context instanceof com.navdy.client.app.ui.routing.ActiveTripActivity) {
            sLogger.v("starting ActiveTripActivity from itself, no-op");
        } else {
            context.startActivity(new android.content.Intent(context, com.navdy.client.app.ui.routing.ActiveTripActivity.class));
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.active_trip_activity);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.active_trip_title).build();
        this.hereMapFragment = (com.navdy.client.app.ui.base.BaseHereMapFragment) getFragmentManager().findFragmentById(com.navdy.client.R.id.map_fragment);
        android.view.View northUpImg = findViewById(com.navdy.client.R.id.north_up_button);
        if (this.hereMapFragment != null) {
            android.content.res.Resources res = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
            this.hereMapFragment.setUsableArea(res.getDimensionPixelSize(com.navdy.client.R.dimen.search_bar_margin) + com.navdy.client.app.framework.map.MapUtils.hereMapTopDownPadding, northUpImg.getWidth() + (res.getDimensionPixelSize(com.navdy.client.R.dimen.util_button_margin) * 2) + com.navdy.client.app.framework.map.MapUtils.hereMapSidePadding, HERE_PADDING, HERE_PADDING);
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon1());
        }
        android.widget.TextView end = (android.widget.TextView) findViewById(com.navdy.client.R.id.end_trip);
        if (end != null) {
            end.setVisibility(View.VISIBLE);
        }
    }

    protected void onResume() {
        super.onResume();
        this.navdyRouteHandler.addListener(this);
        updateOfflineBannerVisibility();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.ACTIVE_TRIP);
    }

    protected void onPause() {
        this.navdyRouteHandler.removeListener(this);
        super.onPause();
    }

    public void centerOnDriver(android.view.View view) {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling centerOnDriver with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon2());
        }
    }

    public void northUpOrientation(android.view.View view) {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling northUpOrientation with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon3());
        }
    }

    public void onEndTripClick(android.view.View view) {
        showQuestionDialog(com.navdy.client.R.string.are_you_sure, com.navdy.client.R.string.do_you_want_to_end_trip, com.navdy.client.R.string.end_trip, com.navdy.client.R.string.cancel, new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon4(), null);
    }

    public void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        finish();
    }

    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.Nullable com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
        finish();
    }

    public void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        clearMapObjects();
        if (this.hereMapFragment == null) {
            this.logger.e("Calling hereMapFragment with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenReady(new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon5(destination));
        }
    }

    public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
    }

    public void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        startMapRoute(route);
    }

    public void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo progress) {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling onTripProgress with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon6(progress));
        }
    }

    public void onReroute() {
    }

    public void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    public void onStopRoute() {
        finish();
    }

    private void clearMapObjects() {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling clearMapObjects with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon7());
        }
    }

    private void startMapRoute(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        clearMapObjects();
        if (this.hereMapFragment == null) {
            this.logger.e("Calling startMapRoute with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenReady(new com.navdy.client.app.ui.routing.ActiveTripActivity.Anon8(route));
        }
    }

    @com.squareup.otto.Subscribe
    public void handleReachabilityStateChange(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        android.view.View offlineBanner = findViewById(com.navdy.client.R.id.offline_banner);
        if (offlineBanner != null) {
            offlineBanner.setVisibility(com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet() ? 8 : 0);
        }
    }

    public void onRefreshConnectivityClick(android.view.View view) {
        com.navdy.client.app.framework.AppInstance.getInstance().checkForNetwork();
    }
}
