package com.navdy.client.app.ui.firstlaunch;

public class InstallMountFragment extends com.navdy.client.app.ui.firstlaunch.InstallCardFragment {
    public InstallMountFragment() {
        setLayoutId(com.navdy.client.R.layout.fle_install_mounts);
    }

    public void setMountType(com.navdy.client.app.framework.models.MountInfo.MountType type) {
        this.mountType = type;
        updateMountShown();
    }

    public void updateMountShown() {
        if (this.rootView != null) {
            android.view.View shortMount = this.rootView.findViewById(com.navdy.client.R.id.short_mount_card);
            android.view.View tallOrMediumMount = this.rootView.findViewById(com.navdy.client.R.id.tall_or_medium_mount_card);
            if (shortMount == null || tallOrMediumMount == null) {
                this.logger.e("Missing layout elements !");
            } else if (this.mountType != null) {
                switch (this.mountType) {
                    case MEDIUM:
                    case TALL:
                        shortMount.setVisibility(View.GONE);
                        tallOrMediumMount.setVisibility(View.VISIBLE);
                        return;
                    default:
                        shortMount.setVisibility(View.VISIBLE);
                        tallOrMediumMount.setVisibility(View.GONE);
                        return;
                }
            }
        }
    }

    @android.support.annotation.Nullable
    public android.view.View onCreateView(android.view.LayoutInflater inflater, @android.support.annotation.Nullable android.view.ViewGroup container, @android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        boolean showNewShortMountAssets = false;
        this.rootView = inflater.inflate(com.navdy.client.R.layout.fle_install_mounts, container, false);
        com.navdy.client.app.framework.util.ImageCache imageCache = null;
        com.navdy.client.app.ui.firstlaunch.InstallActivity activity = (com.navdy.client.app.ui.firstlaunch.InstallActivity) getActivity();
        if (activity != null) {
            imageCache = activity.imageCache;
        }
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box"), "Old_Box")) {
            showNewShortMountAssets = true;
        }
        com.navdy.client.app.framework.util.ImageUtils.loadImage((android.widget.ImageView) this.rootView.findViewById(com.navdy.client.R.id.illustration2), com.navdy.client.R.drawable.asset_install_medium_installed, imageCache);
        android.widget.ImageView imageView = (android.widget.ImageView) this.rootView.findViewById(com.navdy.client.R.id.illustration);
        int asset = com.navdy.client.R.drawable.asset_install_2016_short_installed;
        if (showNewShortMountAssets) {
            asset = com.navdy.client.R.drawable.asset_install_2017_short_installed;
        }
        com.navdy.client.app.framework.util.ImageUtils.loadImage(imageView, asset, imageCache);
        updateMountShown();
        return this.rootView;
    }

    public java.lang.String getScreen() {
        if (this.mountType == com.navdy.client.app.framework.models.MountInfo.MountType.SHORT) {
            return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.SHORT_MOUNT;
        }
        return com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.MEDIUM_TALL_MOUNT;
    }
}
