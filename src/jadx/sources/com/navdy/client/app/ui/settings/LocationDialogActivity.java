package com.navdy.client.app.ui.settings;

public class LocationDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {
    public static final java.lang.String PENDING_DESTINATION = "pending_destination";
    private com.navdy.client.app.framework.models.Destination pendingDestination;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.dialog_location);
        this.pendingDestination = (com.navdy.client.app.framework.models.Destination) getIntent().getExtras().getParcelable(PENDING_DESTINATION);
    }

    protected void onResume() {
        super.onResume();
        try {
            if (android.provider.Settings.Secure.getInt(com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver(), "location_mode") == 3) {
                finish();
            }
        } catch (android.provider.Settings.SettingNotFoundException e) {
            this.logger.e("location setting not found: " + e);
        }
    }

    protected void onPause() {
        super.onPause();
    }

    public void onLocationSettingsClick(android.view.View view) {
        startActivity(new android.content.Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    public void onCancelClick(android.view.View view) {
        com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().requestNewRoute(this.pendingDestination);
        finish();
    }
}
