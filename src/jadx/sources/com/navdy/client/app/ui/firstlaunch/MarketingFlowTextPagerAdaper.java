package com.navdy.client.app.ui.firstlaunch;

public class MarketingFlowTextPagerAdaper extends android.support.v4.view.PagerAdapter {
    private int[] descs = new int[0];
    private android.view.LayoutInflater mLayoutInflater;
    private android.view.View.OnClickListener onClickListener;
    private int[] titles = new int[0];

    public MarketingFlowTextPagerAdaper(android.content.Context context, int[] titles2, int[] descs2, android.view.View.OnClickListener onClickListener2) {
        this.titles = (int[]) titles2.clone();
        this.descs = (int[]) descs2.clone();
        this.onClickListener = onClickListener2;
        this.mLayoutInflater = (android.view.LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.titles.length;
    }

    public boolean isViewFromObject(android.view.View view, java.lang.Object object) {
        return view == object;
    }

    public java.lang.Object instantiateItem(android.view.ViewGroup container, int position) {
        android.view.View itemView = this.mLayoutInflater.inflate(com.navdy.client.R.layout.fle_marketing_flow_bottom_text, container, false);
        if (itemView == null) {
            return null;
        }
        itemView.setOnClickListener(this.onClickListener);
        android.widget.TextView title = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.title);
        if (title != null) {
            title.setText(this.titles[position]);
        }
        android.widget.TextView desc = (android.widget.TextView) itemView.findViewById(com.navdy.client.R.id.desc);
        if (desc != null) {
            desc.setText(this.descs[position]);
        }
        container.addView(itemView);
        return itemView;
    }

    public void destroyItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        container.removeView((android.widget.RelativeLayout) object);
    }
}
