package com.navdy.client.app.ui.settings;

public class CalendarSettingsActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    private com.navdy.client.app.ui.settings.CalendarAdapter adapter;
    private android.view.View permissionScreen;
    private android.support.v7.widget.RecyclerView recyclerView;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.settings_calendar);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.menu_calendar).build();
        this.permissionScreen = findViewById(com.navdy.client.R.id.permission_screen);
        this.recyclerView = (android.support.v7.widget.RecyclerView) findViewById(com.navdy.client.R.id.list);
        if (this.recyclerView != null) {
            this.adapter = new com.navdy.client.app.ui.settings.CalendarAdapter();
            this.recyclerView.setAdapter(this.adapter);
            android.support.v7.widget.LinearLayoutManager layoutManager = new android.support.v7.widget.LinearLayoutManager(getApplicationContext());
            layoutManager.setOrientation(1);
            this.recyclerView.setLayoutManager(layoutManager);
        }
        android.widget.TextView desc = (android.widget.TextView) findViewById(com.navdy.client.R.id.desc);
        if (desc != null) {
            desc.setText(com.navdy.client.app.framework.util.StringUtils.fromHtml((int) com.navdy.client.R.string.fle_app_setup_calendar_desc_fail));
        }
    }

    protected void onResume() {
        super.onResume();
        if (this.adapter.getItemCount() <= 0) {
            if (this.permissionScreen != null) {
                this.permissionScreen.setVisibility(View.VISIBLE);
            }
            if (this.recyclerView != null) {
                this.recyclerView.setVisibility(View.GONE);
                return;
            }
            return;
        }
        if (this.permissionScreen != null) {
            this.permissionScreen.setVisibility(View.GONE);
        }
        if (this.recyclerView != null) {
            this.recyclerView.setVisibility(View.VISIBLE);
        }
    }

    protected void onPause() {
        com.navdy.client.app.framework.util.SuggestionManager.forceSuggestionFullRefresh();
        super.onPause();
    }

    public void onDescriptionClick(android.view.View view) {
        com.navdy.client.app.framework.util.SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
    }
}
