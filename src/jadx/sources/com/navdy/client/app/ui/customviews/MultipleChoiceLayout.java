package com.navdy.client.app.ui.customviews;

public class MultipleChoiceLayout extends android.support.v7.widget.RecyclerView {
    private com.navdy.client.app.ui.customviews.MultipleChoiceLayout.MultipleChoiceAdapter adapter;
    private com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceListener choiceListener;
    private android.support.v7.widget.RecyclerView.LayoutManager layoutManager;
    private int regularTextColor;
    private int selectedColor;

    public interface ChoiceListener {
        void onChoiceSelected(java.lang.String str, int i);
    }

    static class ChoiceViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        @butterknife.InjectView(2131755276)
        android.view.View highlightView;
        @butterknife.InjectView(2131755021)
        android.widget.TextView textView;

        public ChoiceViewHolder(android.view.View view) {
            super(view);
            butterknife.ButterKnife.inject((java.lang.Object) this, view);
        }
    }

    class MultipleChoiceAdapter extends android.support.v7.widget.RecyclerView.Adapter<com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceViewHolder> {
        private int selectedIndex;
        private java.lang.CharSequence[] texts;

        class Anon1 implements android.view.View.OnClickListener {
            final /* synthetic */ int val$position;
            final /* synthetic */ java.lang.CharSequence val$text;

            Anon1(int i, java.lang.CharSequence charSequence) {
                this.val$position = i;
                this.val$text = charSequence;
            }

            public void onClick(android.view.View view) {
                com.navdy.client.app.ui.customviews.MultipleChoiceLayout.MultipleChoiceAdapter.this.setSelectedIndex(this.val$position);
                if (com.navdy.client.app.ui.customviews.MultipleChoiceLayout.this.choiceListener != null) {
                    com.navdy.client.app.ui.customviews.MultipleChoiceLayout.this.choiceListener.onChoiceSelected((java.lang.String) this.val$text, this.val$position);
                }
            }
        }

        MultipleChoiceAdapter() {
        }

        public void setTexts(java.lang.CharSequence[] texts2) {
            this.texts = texts2;
            notifyDataSetChanged();
        }

        public com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceViewHolder onCreateViewHolder(android.view.ViewGroup parent, int viewType) {
            return new com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceViewHolder((android.view.ViewGroup) android.view.LayoutInflater.from(parent.getContext()).inflate(com.navdy.client.R.layout.choice_selected_text, parent, false));
        }

        public void onBindViewHolder(com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceViewHolder holder, int position) {
            java.lang.CharSequence text = this.texts[position];
            holder.textView.setText(text);
            if (position == this.selectedIndex) {
                holder.textView.setTextColor(com.navdy.client.app.ui.customviews.MultipleChoiceLayout.this.selectedColor);
                holder.highlightView.setVisibility(View.VISIBLE);
            } else {
                holder.textView.setTextColor(com.navdy.client.app.ui.customviews.MultipleChoiceLayout.this.regularTextColor);
                holder.highlightView.setVisibility(View.GONE);
            }
            holder.itemView.setClickable(true);
            holder.itemView.setOnClickListener(new com.navdy.client.app.ui.customviews.MultipleChoiceLayout.MultipleChoiceAdapter.Anon1(position, text));
        }

        public int getItemCount() {
            if (this.texts != null) {
                return this.texts.length;
            }
            return 0;
        }

        public void setSelectedIndex(int selectedIndex2) {
            this.selectedIndex = selectedIndex2;
            notifyDataSetChanged();
        }
    }

    public MultipleChoiceLayout(android.content.Context context) {
        this(context, null);
    }

    public MultipleChoiceLayout(android.content.Context context, @android.support.annotation.Nullable android.util.AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public MultipleChoiceLayout(android.content.Context context, @android.support.annotation.Nullable android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        android.content.res.TypedArray customAttributes = context.obtainStyledAttributes(attrs, com.navdy.client.R.styleable.MultipleChoiceLayout, defStyle, 0);
        this.adapter = new com.navdy.client.app.ui.customviews.MultipleChoiceLayout.MultipleChoiceAdapter();
        setAdapter(this.adapter);
        int spanCount = 0;
        if (customAttributes != null) {
            java.lang.CharSequence[] texts = customAttributes.getTextArray(0);
            customAttributes.recycle();
            this.adapter.setTexts(texts);
            spanCount = texts.length;
        }
        this.layoutManager = new android.support.v7.widget.GridLayoutManager(context, spanCount);
        setLayoutManager(this.layoutManager);
        this.regularTextColor = context.getResources().getColor(com.navdy.client.R.color.grey_4);
        this.selectedColor = context.getResources().getColor(com.navdy.client.R.color.navdy_blue);
    }

    public void setSelectedIndex(int index) {
        this.adapter.setSelectedIndex(index);
    }

    public com.navdy.client.app.ui.customviews.MultipleChoiceLayout.MultipleChoiceAdapter getAdapter() {
        return this.adapter;
    }

    public void setChoiceListener(com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceListener choiceListener2) {
        this.choiceListener = choiceListener2;
    }
}
