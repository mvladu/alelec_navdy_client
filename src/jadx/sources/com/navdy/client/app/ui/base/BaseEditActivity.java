package com.navdy.client.app.ui.base;

public abstract class BaseEditActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    android.widget.CompoundButton.OnCheckedChangeListener checkChangedListener = new com.navdy.client.app.ui.base.BaseEditActivity.Anon1();
    protected boolean saveOnExit = true;
    /* access modifiers changed from: protected */
    public boolean somethingChanged = false;

    class Anon1 implements android.widget.CompoundButton.OnCheckedChangeListener {
        Anon1() {
        }

        public void onCheckedChanged(android.widget.CompoundButton buttonView, boolean isChecked) {
            com.navdy.client.app.ui.base.BaseEditActivity.this.somethingChanged = true;
        }
    }

    protected abstract void saveChanges();

    protected void discardChanges() {
    }

    protected void onPause() {
        if (this.saveOnExit) {
            if (this.somethingChanged) {
                saveChanges();
            }
            this.somethingChanged = false;
        }
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
        this.saveOnExit = true;
        this.somethingChanged = false;
    }

    protected void initCompoundButton(android.widget.CompoundButton button, boolean isChecked) {
        initCompoundButton(button, isChecked, this.checkChangedListener);
    }

    protected void initCompoundButton(android.widget.CompoundButton button, boolean isChecked, android.widget.CompoundButton.OnCheckedChangeListener listener) {
        if (button != null) {
            button.setChecked(isChecked);
            button.setOnCheckedChangeListener(listener);
        }
    }
}
