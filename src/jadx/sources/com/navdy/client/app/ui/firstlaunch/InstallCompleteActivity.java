package com.navdy.client.app.ui.firstlaunch;

public class InstallCompleteActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    public static final java.lang.String EXTRA_COMING_FROM_SETTINGS = "comming_from_settings";
    private boolean comingFromSettings = false;

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_install_complete);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.install_complete).build();
        this.comingFromSettings = getIntent().getBooleanExtra(EXTRA_COMING_FROM_SETTINGS, false);
        if (this.comingFromSettings) {
            android.widget.RelativeLayout bottomBar = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.bottom_bar);
            if (bottomBar != null) {
                bottomBar.setVisibility(View.GONE);
            }
            android.view.View bottomBarDivider = findViewById(com.navdy.client.R.id.bottom_bar_divider);
            if (bottomBarDivider != null) {
                bottomBarDivider.setVisibility(View.GONE);
            }
        }
        loadImage(com.navdy.client.R.id.img1, com.navdy.client.R.drawable.image_2017_short_overview_fw);
        loadImage(com.navdy.client.R.id.img2, com.navdy.client.R.drawable.image_install_2017_short_fw);
        loadImage(com.navdy.client.R.id.img2b, com.navdy.client.R.drawable.image_install_med_fw);
        loadImage(com.navdy.client.R.id.img3, com.navdy.client.R.drawable.image_install_2017_short_success_fw);
        loadImage(com.navdy.client.R.id.img4, com.navdy.client.R.drawable.image_install_2017_secure_fw);
        loadImage(com.navdy.client.R.id.img5, com.navdy.client.R.drawable.image_install_cords_fw);
        loadImage(com.navdy.client.R.id.img7, com.navdy.client.R.drawable.image_install_tidying_fw);
        loadImage(com.navdy.client.R.id.img8, com.navdy.client.R.drawable.image_install_2017_power_fw);
        loadImage(com.navdy.client.R.id.img9, com.navdy.client.R.drawable.image_install_dial_fw);
    }

    protected void onResume() {
        super.onResume();
        if (android.text.TextUtils.equals(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box"), "New_Box")) {
            android.view.View card = findViewById(com.navdy.client.R.id.medium_tall_mount);
            if (card != null) {
                card.setVisibility(View.GONE);
            }
        }
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install.FINISHED);
    }

    public void onOverviewClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_overview);
    }

    public void onInstallShortMountClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_short_mount);
    }

    public void onInstallMediumOrTallMountClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_medium_or_tall_mount);
    }

    public void onLensCheckClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_lens_check);
    }

    public void onSecuringMountClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_secure_mount);
    }

    public void onPickCableClick(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CablePickerActivity.class));
    }

    public void onTidyingUpClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_tidying_up);
    }

    public void onTurnOnClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_turn_on);
    }

    public void onInstallingDialClick(android.view.View view) {
        startInstallActivityAtThisStep(com.navdy.client.R.layout.fle_install_dial);
    }

    private void startInstallActivityAtThisStep(int step) {
        java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>(1);
        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.VIDEO_URL, com.navdy.client.app.ui.firstlaunch.InstallActivity.getVideoUrlForStep(step));
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.VIDEO_TAPPED_ON_INSTALL_COMPLETE, attributes);
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
        intent.putExtra("extra_step", step);
        if (this.comingFromSettings) {
            intent.putExtra(EXTRA_COMING_FROM_SETTINGS, true);
        }
        startActivity(intent);
        finish();
    }

    public void onNextClick(android.view.View view) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FINISHED_INSTALL, true).apply();
        if (this.comingFromSettings) {
            finish();
        } else if (!com.navdy.client.app.tracking.Tracker.weHaveCarInfo()) {
            android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity.class);
            intent.putExtra("extra_next_step", "app_setup");
            startActivity(intent);
        } else {
            com.navdy.client.app.ui.firstlaunch.AppSetupActivity.goToAppSetup(this);
        }
    }

    public void onLookDownClick(android.view.View view) {
        android.widget.ScrollView sv = (android.widget.ScrollView) findViewById(com.navdy.client.R.id.scroll_view);
        if (sv != null) {
            sv.smoothScrollBy(0, (int) view.getY());
        }
    }
}
