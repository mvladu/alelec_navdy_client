package com.navdy.client.app.ui.firstlaunch;

public class MarketingFlowImagePagerAdaper extends com.navdy.client.app.ui.ImageResourcePagerAdaper {
    public MarketingFlowImagePagerAdaper(android.content.Context context, int[] images, com.navdy.client.app.framework.util.ImageCache imageCache) {
        super(context, images, imageCache);
    }

    public int getCount() {
        return this.imageResourceIds.length + 1;
    }

    public java.lang.Object instantiateItem(android.view.ViewGroup container, int position) {
        android.widget.ImageView imageView = (android.widget.ImageView) this.mLayoutInflater.inflate(com.navdy.client.R.layout.fle_marketing_flow_bg_image, container, false);
        if (imageView == null) {
            return null;
        }
        if (position >= this.imageResourceIds.length) {
            imageView.setBackgroundColor(0);
        } else {
            imageView.setImageResource(this.imageResourceIds[position]);
        }
        container.addView(imageView);
        return imageView;
    }
}
