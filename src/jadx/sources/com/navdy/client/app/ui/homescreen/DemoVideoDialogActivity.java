package com.navdy.client.app.ui.homescreen;

public class DemoVideoDialogActivity extends com.navdy.client.app.ui.base.BaseActivity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.dialog_demo_video);
    }

    public void onWatchVideoClick(android.view.View v) {
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_WATCHED_THE_DEMO, true).apply();
        java.lang.String videoUrl = getString(com.navdy.client.R.string.demo_video_url);
        android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.class);
        intent.putExtra(com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity.EXTRA_VIDEO_URL, videoUrl);
        startActivity(intent);
        finish();
    }

    public void onCloseClick(android.view.View view) {
        onBackPressed();
    }
}
