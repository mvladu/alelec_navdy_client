package com.navdy.client.app.ui.firstlaunch;

public class CheckMountActivity extends com.navdy.client.app.ui.base.BaseActivity {

    class Anon1 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, com.navdy.client.app.framework.models.MountInfo> {
        final /* synthetic */ java.lang.String val$box;

        Anon1(java.lang.String str) {
            this.val$box = str;
        }

        /* access modifiers changed from: protected */
        public com.navdy.client.app.framework.models.MountInfo doInBackground(java.lang.Void... params) {
            return com.navdy.client.app.ui.settings.SettingsUtils.getMountInfo();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(com.navdy.client.app.framework.models.MountInfo mountInfo) {
            com.navdy.client.app.ui.firstlaunch.CheckMountActivity.this.hideProgressDialog();
            boolean noneOfTheMountsWillWork = mountInfo.noneOfTheMountsWillWork();
            if ((mountInfo.shortSupported || !android.text.TextUtils.equals(this.val$box, "New_Box")) && !noneOfTheMountsWillWork) {
                android.content.Intent intent = new android.content.Intent(com.navdy.client.app.ui.firstlaunch.CheckMountActivity.this.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.InstallActivity.class);
                if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.val$box, "New_Box")) {
                    intent.putExtra("extra_mount", mountInfo.recommendedMount.getValue());
                } else {
                    intent.putExtra("extra_mount", com.navdy.client.app.framework.models.MountInfo.MountType.SHORT.getValue());
                }
                com.navdy.client.app.ui.firstlaunch.CheckMountActivity.this.startActivity(intent);
            } else {
                android.content.Intent intent2 = new android.content.Intent(com.navdy.client.app.ui.firstlaunch.CheckMountActivity.this.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.MountPickerActivity.class);
                if (noneOfTheMountsWillWork) {
                    intent2.putExtra(com.navdy.client.app.ui.firstlaunch.MountPickerActivity.EXTRA_USE_CASE, 3);
                }
                com.navdy.client.app.ui.firstlaunch.CheckMountActivity.this.startActivity(intent2);
            }
            com.navdy.client.app.ui.firstlaunch.CheckMountActivity.this.finish();
        }
    }

    protected void onCreate(@android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        java.lang.String box = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box");
        showProgressDialog();
        new com.navdy.client.app.ui.firstlaunch.CheckMountActivity.Anon1(box).execute(new java.lang.Void[0]);
    }
}
