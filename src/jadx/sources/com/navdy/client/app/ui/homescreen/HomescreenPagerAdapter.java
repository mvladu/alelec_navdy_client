package com.navdy.client.app.ui.homescreen;

class HomescreenPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {
    private static final int FAVORITES_FRAGMENT_POSITION = 1;
    private static final int GLANCES_FRAGMENT_POSITION = 2;
    private static final int HOME_FRAGMENT_POSITION = 0;
    private static final int TOTAL_FRAGMENTS = 4;
    private static final int TRIPS_FRAGMENT_POSITION = 3;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.HomescreenPagerAdapter.class);
    private final java.lang.Boolean[] fragmentExists = new java.lang.Boolean[4];
    private final android.support.v4.app.Fragment[] fragmentList = new android.support.v4.app.Fragment[4];
    private final java.lang.String[] fragmentNames = new java.lang.String[4];
    private int hiddenFragmentsCount = 1;

    public HomescreenPagerAdapter(android.support.v4.app.FragmentManager manager) {
        super(manager);
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        this.fragmentList[0] = new com.navdy.client.app.ui.homescreen.SuggestionsFragment();
        this.fragmentNames[0] = context.getString(com.navdy.client.R.string.home_fragment);
        this.fragmentList[1] = new com.navdy.client.app.ui.favorites.FavoritesFragment();
        this.fragmentNames[1] = context.getString(com.navdy.client.R.string.favorites_fragment);
        this.fragmentList[2] = new com.navdy.client.app.ui.glances.GlancesFragment();
        this.fragmentNames[2] = context.getString(com.navdy.client.R.string.glances_fragment);
        java.util.Arrays.fill(this.fragmentExists, java.lang.Boolean.valueOf(false));
    }

    public java.lang.Object instantiateItem(android.view.ViewGroup container, int position) {
        if (this.fragmentExists[position].booleanValue()) {
            return this.fragmentList[position];
        }
        this.fragmentExists[position] = java.lang.Boolean.valueOf(true);
        return super.instantiateItem(container, position);
    }

    public void destroyItem(android.view.ViewGroup container, int position, java.lang.Object object) {
    }

    public android.support.v4.app.Fragment getItem(int position) {
        return this.fragmentList[position];
    }

    public int getCount() {
        return this.fragmentList.length - this.hiddenFragmentsCount;
    }

    public java.lang.CharSequence getPageTitle(int position) {
        return this.fragmentNames[position].toUpperCase();
    }

    public void setPrimaryItem(android.view.ViewGroup container, int position, java.lang.Object object) {
        super.setPrimaryItem(container, position, object);
    }

    public void showHiddenTabs() {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        this.hiddenFragmentsCount = 0;
        this.fragmentNames[1] = com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.favs_fragment);
        this.fragmentList[3] = new com.navdy.client.app.ui.trips.TripsFragment();
        this.fragmentNames[3] = context.getString(com.navdy.client.R.string.trips_fragment);
        notifyDataSetChanged();
    }
}
