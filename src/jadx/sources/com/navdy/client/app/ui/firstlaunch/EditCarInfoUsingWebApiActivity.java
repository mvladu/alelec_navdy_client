package com.navdy.client.app.ui.firstlaunch;

@java.lang.Deprecated
public class EditCarInfoUsingWebApiActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    static final java.lang.String EXTRA_NEXT_STEP = "extra_next_step";
    static final java.lang.String INSTALL = "installation_flow";
    private static final int OTHER_DIALOG_ID = 1;

    class Anon1 extends com.navdy.client.app.framework.util.CarMdCallBack {
        final /* synthetic */ com.navdy.client.app.framework.util.CarMdClient val$carMdClient;
        final /* synthetic */ android.widget.Spinner val$make;
        final /* synthetic */ java.lang.String val$makeString;
        final /* synthetic */ android.widget.Spinner val$model;
        final /* synthetic */ org.droidparts.adapter.widget.StringSpinnerAdapter val$modelAdapter;
        final /* synthetic */ android.widget.Spinner val$year;
        final /* synthetic */ org.droidparts.adapter.widget.StringSpinnerAdapter val$yearAdapter;

        /* renamed from: com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity$Anon1$Anon1 reason: collision with other inner class name */
        class C0059Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.ArrayList val$list;

            C0059Anon1(java.util.ArrayList arrayList) {
                this.val$list = arrayList;
            }

            public void run() {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
                this.val$list.add(0, context.getString(com.navdy.client.R.string.settings_profile_make_label));
                this.val$list.add(context.getString(com.navdy.client.R.string.other));
                org.droidparts.adapter.widget.StringSpinnerAdapter adapter = new org.droidparts.adapter.widget.StringSpinnerAdapter(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$make, (java.util.List<java.lang.String>) this.val$list);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.d("setting make list to: " + this.val$list);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$make.setAdapter(adapter);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$year.setAdapter(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$yearAdapter);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$model.setAdapter(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$modelAdapter);
                int selection = this.val$list.indexOf(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$makeString);
                if (selection >= 0) {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.this.val$make.setSelection(selection);
                }
            }
        }

        class Anon2 implements java.lang.Runnable {
            final /* synthetic */ java.lang.Throwable val$error;

            Anon2(java.lang.Throwable th) {
                this.val$error = th;
            }

            public void run() {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.need_internet_for_this_feature, new java.lang.Object[0]);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.v("Unable to lookup the list of makes from car md.", this.val$error);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showManualEntry(null);
            }
        }

        Anon1(com.navdy.client.app.framework.util.CarMdClient carMdClient, android.widget.Spinner spinner, android.widget.Spinner spinner2, org.droidparts.adapter.widget.StringSpinnerAdapter stringSpinnerAdapter, android.widget.Spinner spinner3, org.droidparts.adapter.widget.StringSpinnerAdapter stringSpinnerAdapter2, java.lang.String str) {
            this.val$carMdClient = carMdClient;
            this.val$make = spinner;
            this.val$year = spinner2;
            this.val$yearAdapter = stringSpinnerAdapter;
            this.val$model = spinner3;
            this.val$modelAdapter = stringSpinnerAdapter2;
            this.val$makeString = str;
        }

        public void processResponse(okhttp3.ResponseBody responseBody) {
            try {
                java.lang.String jsonString = responseBody.string();
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: response: " + jsonString);
                java.util.ArrayList<java.lang.String> list = this.val$carMdClient.getListFromJsonResponse(jsonString);
                if (list == null || list.isEmpty()) {
                    throw new java.lang.Exception("Empty make list!");
                }
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.C0059Anon1(list));
            } catch (java.lang.Exception e) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: unable to get the list of makes out of the response body: " + responseBody);
                processFailure(e);
            }
        }

        public void processFailure(java.lang.Throwable error) {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1.Anon2(error));
        }
    }

    class Anon2 implements android.widget.AdapterView.OnItemSelectedListener {
        final /* synthetic */ com.navdy.client.app.framework.util.CarMdClient val$carMdClient;
        final /* synthetic */ android.widget.Spinner val$make;
        final /* synthetic */ android.widget.Spinner val$model;
        final /* synthetic */ org.droidparts.adapter.widget.StringSpinnerAdapter val$modelAdapter;
        final /* synthetic */ android.widget.Spinner val$year;
        final /* synthetic */ org.droidparts.adapter.widget.StringSpinnerAdapter val$yearAdapter;
        final /* synthetic */ java.lang.String val$yearString;

        class Anon1 extends com.navdy.client.app.framework.util.CarMdCallBack {

            /* renamed from: com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity$Anon2$Anon1$Anon1 reason: collision with other inner class name */
            class C0060Anon1 implements java.lang.Runnable {
                final /* synthetic */ java.util.ArrayList val$list;

                C0060Anon1(java.util.ArrayList arrayList) {
                    this.val$list = arrayList;
                }

                public void run() {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                    android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
                    this.val$list.add(0, context.getString(com.navdy.client.R.string.settings_profile_year_label));
                    this.val$list.add(context.getString(com.navdy.client.R.string.other));
                    org.droidparts.adapter.widget.StringSpinnerAdapter adapter = new org.droidparts.adapter.widget.StringSpinnerAdapter(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.this.val$year, (java.util.List<java.lang.String>) this.val$list);
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.d("setting year list to: " + this.val$list);
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.this.val$year.setAdapter(adapter);
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.this.val$model.setAdapter(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.this.val$modelAdapter);
                    int selection = this.val$list.indexOf(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.this.val$yearString);
                    if (selection >= 0) {
                        com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.this.val$year.setSelection(selection);
                    }
                }
            }

            /* renamed from: com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity$Anon2$Anon1$Anon2 reason: collision with other inner class name */
            class C0061Anon2 implements java.lang.Runnable {
                final /* synthetic */ java.lang.Throwable val$error;

                C0061Anon2(java.lang.Throwable th) {
                    this.val$error = th;
                }

                public void run() {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.v("Unable to lookup the list of years from car md.", this.val$error);
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showManualEntry(null);
                }
            }

            Anon1() {
            }

            public void processResponse(okhttp3.ResponseBody responseBody) {
                try {
                    java.util.ArrayList<java.lang.String> list = com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.this.val$carMdClient.getListFromJsonResponse(responseBody.string());
                    if (list == null || list.isEmpty()) {
                        throw new java.lang.Exception("Empty make list!");
                    }
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.Anon1.C0060Anon1(list));
                } catch (java.lang.Exception e) {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: unable to get the list of years out of the response body: " + responseBody);
                    processFailure(e);
                }
            }

            public void processFailure(java.lang.Throwable error) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.Anon1.C0061Anon2(error));
            }
        }

        Anon2(android.widget.Spinner spinner, android.widget.Spinner spinner2, org.droidparts.adapter.widget.StringSpinnerAdapter stringSpinnerAdapter, android.widget.Spinner spinner3, org.droidparts.adapter.widget.StringSpinnerAdapter stringSpinnerAdapter2, com.navdy.client.app.framework.util.CarMdClient carMdClient, java.lang.String str) {
            this.val$make = spinner;
            this.val$year = spinner2;
            this.val$yearAdapter = stringSpinnerAdapter;
            this.val$model = spinner3;
            this.val$modelAdapter = stringSpinnerAdapter2;
            this.val$carMdClient = carMdClient;
            this.val$yearString = str;
        }

        public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int selectedMakePosition, long id) {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.d("onItemSelected selectedMakePosition: " + selectedMakePosition);
            java.lang.String selectedMake = (java.lang.String) this.val$make.getSelectedItem();
            if (selectedMakePosition <= 0) {
                this.val$year.setAdapter(this.val$yearAdapter);
                this.val$model.setAdapter(this.val$modelAdapter);
                return;
            }
            if (selectedMakePosition >= this.val$make.getCount() - 1) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showDialogAboutOther();
            }
            com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showProgressDialog();
            this.val$carMdClient.getYears(selectedMake, new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2.Anon1());
        }

        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
        }
    }

    class Anon3 implements android.widget.AdapterView.OnItemSelectedListener {
        final /* synthetic */ com.navdy.client.app.framework.util.CarMdClient val$carMdClient;
        final /* synthetic */ android.widget.Spinner val$make;
        final /* synthetic */ android.widget.Spinner val$model;
        final /* synthetic */ org.droidparts.adapter.widget.StringSpinnerAdapter val$modelAdapter;
        final /* synthetic */ java.lang.String val$modelString;
        final /* synthetic */ android.widget.Spinner val$year;

        class Anon1 extends com.navdy.client.app.framework.util.CarMdCallBack {

            /* renamed from: com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity$Anon3$Anon1$Anon1 reason: collision with other inner class name */
            class C0062Anon1 implements java.lang.Runnable {
                final /* synthetic */ java.util.ArrayList val$list;

                C0062Anon1(java.util.ArrayList arrayList) {
                    this.val$list = arrayList;
                }

                public void run() {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                    android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
                    this.val$list.add(0, context.getString(com.navdy.client.R.string.settings_profile_model_label));
                    this.val$list.add(context.getString(com.navdy.client.R.string.other));
                    org.droidparts.adapter.widget.StringSpinnerAdapter adapter = new org.droidparts.adapter.widget.StringSpinnerAdapter(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.this.val$model, (java.util.List<java.lang.String>) this.val$list);
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.d("setting model list to: " + this.val$list);
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.this.val$model.setAdapter(adapter);
                    int selection = this.val$list.indexOf(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.this.val$modelString);
                    if (selection >= 0) {
                        com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.this.val$model.setSelection(selection);
                    }
                }
            }

            class Anon2 implements java.lang.Runnable {
                final /* synthetic */ java.lang.Throwable val$error;

                Anon2(java.lang.Throwable th) {
                    this.val$error = th;
                }

                public void run() {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.v("Unable to lookup the list of models from car md.", this.val$error);
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showManualEntry(null);
                }
            }

            Anon1() {
            }

            public void processResponse(okhttp3.ResponseBody responseBody) {
                try {
                    java.util.ArrayList<java.lang.String> list = com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.this.val$carMdClient.getListFromJsonResponse(responseBody.string());
                    if (list == null || list.isEmpty()) {
                        throw new java.lang.Exception("Empty make list!");
                    }
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.Anon1.C0062Anon1(list));
                } catch (java.lang.Exception e) {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: unable to get the list of makes out of the response body: " + responseBody);
                    processFailure(e);
                }
            }

            public void processFailure(java.lang.Throwable error) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.Anon1.Anon2(error));
            }
        }

        Anon3(android.widget.Spinner spinner, android.widget.Spinner spinner2, android.widget.Spinner spinner3, org.droidparts.adapter.widget.StringSpinnerAdapter stringSpinnerAdapter, com.navdy.client.app.framework.util.CarMdClient carMdClient, java.lang.String str) {
            this.val$make = spinner;
            this.val$year = spinner2;
            this.val$model = spinner3;
            this.val$modelAdapter = stringSpinnerAdapter;
            this.val$carMdClient = carMdClient;
            this.val$modelString = str;
        }

        public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int selectedYearPosition, long id) {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.d("onItemSelected selectedYearPosition: " + selectedYearPosition);
            java.lang.String selectedMake = (java.lang.String) this.val$make.getSelectedItem();
            int selectedMakePosition = this.val$make.getSelectedItemPosition();
            if (selectedMakePosition > 0 && selectedMakePosition < this.val$make.getCount() - 1) {
                java.lang.String selectedYear = (java.lang.String) this.val$year.getSelectedItem();
                if (selectedYearPosition <= 0) {
                    this.val$model.setAdapter(this.val$modelAdapter);
                    return;
                }
                if (selectedYearPosition >= this.val$year.getCount() - 1) {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showDialogAboutOther();
                }
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showProgressDialog();
                this.val$carMdClient.getModels(selectedMake, selectedYear, new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3.Anon1());
            }
        }

        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
        }
    }

    class Anon4 implements android.widget.AdapterView.OnItemSelectedListener {
        final /* synthetic */ android.widget.Spinner val$model;

        Anon4(android.widget.Spinner spinner) {
            this.val$model = spinner;
        }

        public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int selectedModelPosition, long id) {
            if (selectedModelPosition > 0 && selectedModelPosition >= this.val$model.getCount() - 1) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.showDialogAboutOther();
            }
        }

        public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            if (com.navdy.client.app.ui.settings.SettingsUtils.isUsingDefaultObdScanSetting()) {
                com.navdy.client.app.ui.settings.SettingsUtils.setDefaultObdSettingDependingOnBlacklistAsync();
            }
        }
    }

    private class ObdInfoCallBack extends com.navdy.client.app.framework.util.CarMdCallBack {
        private android.content.SharedPreferences customerPrefs;

        class Anon1 extends com.navdy.client.app.framework.util.CarMdCallBack {
            Anon1() {
            }

            public void processResponse(okhttp3.ResponseBody response) {
                try {
                    com.navdy.client.app.tracking.Tracker.saveObdImageToInternalStorage(android.graphics.BitmapFactory.decodeStream(response.byteStream()));
                } catch (java.lang.Exception e) {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.e("Something went wrong while trying to process the response for the OBD photo.", e);
                } finally {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.ObdInfoCallBack.this.goToNextStep();
                }
            }

            public void processFailure(java.lang.Throwable error) {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.e("Unable to parse car md obd location photo response.", error);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.ObdInfoCallBack.this.goToNextStep();
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.ObdInfoCallBack.this.goToNextStep();
            }
        }

        class Anon3 implements java.lang.Runnable {
            final /* synthetic */ java.lang.String val$nextStep;

            Anon3(java.lang.String str) {
                this.val$nextStep = str;
            }

            public void run() {
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.val$nextStep, com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.INSTALL)) {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CheckMountActivity.class));
                    return;
                }
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.getApplicationContext(), com.navdy.client.app.ui.firstlaunch.CarInfoActivity.class));
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.finish();
            }
        }

        ObdInfoCallBack(android.content.SharedPreferences customerPrefs2) {
            this.customerPrefs = customerPrefs2;
        }

        public void processResponse(okhttp3.ResponseBody responseBody) {
            try {
                com.navdy.client.app.framework.util.CarMdClient.CarMdObdLocationResponse carMdObdLocationResponse = com.navdy.client.app.framework.util.CarMdClient.getInstance().parseCarMdObdLocationResponse(responseBody);
                com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.saveObdInfo(this.customerPrefs, carMdObdLocationResponse.note, carMdObdLocationResponse.accessNote, carMdObdLocationResponse.locationNumber);
                try {
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(carMdObdLocationResponse.accessImageURL)) {
                        com.navdy.client.app.framework.util.CarMdClient.getInstance().sendCarMdRequest(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.ObdInfoCallBack.Anon1(), com.navdy.client.app.framework.util.CarMdClient.getInstance().buildCarMdRequest(okhttp3.HttpUrl.parse(carMdObdLocationResponse.accessImageURL), null));
                        return;
                    }
                    goToNextStep();
                } catch (java.lang.Exception e) {
                    com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.e("Something went wrong while trying to get the obd location photo.", e);
                    goToNextStep();
                }
            } catch (java.lang.Exception e2) {
                processFailure(e2);
            }
        }

        public void processFailure(java.lang.Throwable error) {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.logger.e("Unable to parse car md obd location response.", error);
            com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.ObdInfoCallBack.Anon2());
        }

        private void goToNextStep() {
            com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.runOnUiThread(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.ObdInfoCallBack.Anon3(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.this.getIntent().getStringExtra(com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.EXTRA_NEXT_STEP)));
        }
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_vehicle_car_md_info);
        initCarSelectorScreen();
    }

    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.EDIT_CAR_INFO);
    }

    public void showManualEntry(android.view.View view) {
        setContentView((int) com.navdy.client.R.layout.fle_vehicle_car_info);
        initManualEntryScreen();
    }

    public void showCarSelector(android.view.View view) {
        setContentView((int) com.navdy.client.R.layout.fle_vehicle_car_md_info);
        initCarSelectorScreen();
    }

    private void initCarSelectorScreen() {
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.select_your_car).build();
        android.widget.Spinner make = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_year);
        android.widget.Spinner year = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_make);
        android.widget.Spinner model = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        java.lang.String makeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String yearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String modelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        java.util.ArrayList arrayList = new java.util.ArrayList(1);
        arrayList.add(context.getString(com.navdy.client.R.string.settings_profile_make_label));
        org.droidparts.adapter.widget.StringSpinnerAdapter stringSpinnerAdapter = new org.droidparts.adapter.widget.StringSpinnerAdapter(make, (java.util.List<java.lang.String>) arrayList);
        make.setAdapter(stringSpinnerAdapter);
        java.util.ArrayList arrayList2 = new java.util.ArrayList(1);
        arrayList2.add(context.getString(com.navdy.client.R.string.settings_profile_year_label));
        org.droidparts.adapter.widget.StringSpinnerAdapter yearAdapter = new org.droidparts.adapter.widget.StringSpinnerAdapter(year, (java.util.List<java.lang.String>) arrayList2);
        year.setAdapter(yearAdapter);
        java.util.ArrayList arrayList3 = new java.util.ArrayList(1);
        arrayList3.add(context.getString(com.navdy.client.R.string.settings_profile_model_label));
        org.droidparts.adapter.widget.StringSpinnerAdapter modelAdapter = new org.droidparts.adapter.widget.StringSpinnerAdapter(model, (java.util.List<java.lang.String>) arrayList3);
        model.setAdapter(modelAdapter);
        com.navdy.client.app.framework.util.CarMdClient carMdClient = com.navdy.client.app.framework.util.CarMdClient.getInstance();
        showProgressDialog();
        carMdClient.getMakes(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon1(carMdClient, make, year, yearAdapter, model, modelAdapter, makeString));
        make.setOnItemSelectedListener(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon2(make, year, yearAdapter, model, modelAdapter, carMdClient, yearString));
        year.setOnItemSelectedListener(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon3(make, year, model, modelAdapter, carMdClient, modelString));
        model.setOnItemSelectedListener(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon4(model));
    }

    public void onSetCarMdInfoClick(android.view.View view) {
        android.widget.Spinner make = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_year);
        android.widget.Spinner year = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_make);
        android.widget.Spinner model = (android.widget.Spinner) findViewById(com.navdy.client.R.id.pick_a_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        int makeInt = make.getSelectedItemPosition();
        int yearInt = year.getSelectedItemPosition();
        int modelInt = model.getSelectedItemPosition();
        java.lang.String makeString = make.getSelectedItem().toString();
        java.lang.String yearString = year.getSelectedItem().toString();
        java.lang.String modelString = model.getSelectedItem().toString();
        if (makeInt == 0 || makeInt >= make.getCount() - 1 || yearInt == 0 || yearInt >= year.getCount() - 1 || modelInt == 0 || modelInt >= model.getCount() - 1 || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yearString) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(makeString) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(modelString)) {
            showLongToast(com.navdy.client.R.string.please_enter_car_info, new java.lang.Object[0]);
        } else {
            saveCarInfoAndDownloadObdInfo(customerPrefs, makeString, yearString, modelString, false);
        }
    }

    public void saveCarInfoAndDownloadObdInfo(android.content.SharedPreferences customerPrefs, java.lang.String makeString, java.lang.String yearString, java.lang.String modelString, boolean comesFromManualEntry) {
        showProgressDialog();
        saveCarInfo(customerPrefs, makeString, yearString, modelString, comesFromManualEntry, com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(getIntent().getStringExtra(EXTRA_NEXT_STEP), INSTALL));
        com.navdy.client.app.tracking.Tracker.resetPhotoAndLocation(customerPrefs);
        com.navdy.client.app.framework.util.CarMdClient.getInstance().getObdLocation(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.ObdInfoCallBack(customerPrefs), makeString, yearString, modelString);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.firstlaunch.EditCarInfoUsingWebApiActivity.Anon5(), 1);
    }

    public void onBackClick(android.view.View view) {
        finish();
    }

    private void showDialogAboutOther() {
        showSimpleDialog(1, getString(com.navdy.client.R.string.manual_entry), getString(com.navdy.client.R.string.manual_entry_dialog_description));
        showManualEntry(null);
    }

    private void initManualEntryScreen() {
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.select_your_car).build();
        android.widget.EditText make = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_make);
        android.widget.EditText year = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_year);
        android.widget.EditText model = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
        java.lang.String makeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String yearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String modelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        make.setText(makeString);
        year.setText(yearString);
        model.setText(modelString);
    }

    public void onSetCarInfoClick(android.view.View view) {
        android.widget.EditText make = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_make);
        android.widget.EditText year = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_year);
        android.widget.EditText model = (android.widget.EditText) findViewById(com.navdy.client.R.id.enter_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        java.lang.String makeString = make.getText().toString();
        java.lang.String yearString = year.getText().toString();
        java.lang.String modelString = model.getText().toString();
        if (!fieldsAreValid(makeString, yearString, modelString)) {
            showLongToast(com.navdy.client.R.string.please_enter_car_info, new java.lang.Object[0]);
            return;
        }
        saveCarInfoAndDownloadObdInfo(com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences(), makeString, yearString, modelString, true);
    }

    public boolean fieldsAreValid(java.lang.String makeString, java.lang.String yearString, java.lang.String modelString) {
        return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(yearString) && yearString.matches("[12][0-9]{3}") && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(makeString) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(modelString);
    }

    public static void saveCarInfo(@android.support.annotation.NonNull android.content.SharedPreferences customerPrefs, @android.support.annotation.NonNull java.lang.String makeString, @android.support.annotation.NonNull java.lang.String yearString, @android.support.annotation.NonNull java.lang.String modelString, boolean comesFromManualEntry, boolean isInFle) {
        java.lang.String oldMakeString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
        java.lang.String oldYearString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
        java.lang.String oldModelString = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(oldMakeString, makeString) || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(oldYearString, yearString) || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(oldModelString, modelString)) {
            sLogger.v("Resetting car md data.");
            com.navdy.client.app.tracking.Tracker.resetPhotoAndLocation(customerPrefs);
            java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>(3);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_YEAR, yearString);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MAKE, makeString);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MODEL, modelString);
            attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.DURING_FLE, isInFle ? "True" : "False");
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.CAR_INFO_CHANGED, attributes);
        }
        long serial = customerPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
        java.lang.String makeString2 = makeString.trim();
        java.lang.String yearString2 = yearString.trim();
        java.lang.String modelString2 = modelString.trim();
        customerPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, makeString2).putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, yearString2).putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, modelString2).putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MANUAL_ENTRY, comesFromManualEntry).putLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, 1 + serial).apply();
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MAKE, makeString2);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_YEAR, yearString2);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MODEL, modelString2);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MANUAL_ENTRY, java.lang.Boolean.toString(comesFromManualEntry));
    }

    public static void saveObdInfo(@android.support.annotation.NonNull android.content.SharedPreferences customerPrefs, @android.support.annotation.NonNull java.lang.String note, @android.support.annotation.NonNull java.lang.String accessNote, int locationNumber) {
        customerPrefs.edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NOTE, note).putString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE, accessNote).putInt(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_OBD_LOCATION_NUM, locationNumber).apply();
    }
}
