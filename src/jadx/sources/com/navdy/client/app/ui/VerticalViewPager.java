package com.navdy.client.app.ui;

public class VerticalViewPager extends android.support.v4.view.ViewPager {

    private static class VerticalPageTransformer implements android.support.v4.view.ViewPager.PageTransformer {
        private VerticalPageTransformer() {
        }

        public void transformPage(android.view.View view, float position) {
            if (position < -1.0f) {
                view.setAlpha(0.0f);
            } else if (position <= 1.0f) {
                view.setAlpha(1.0f);
                view.setTranslationX(((float) view.getWidth()) * (-position));
                view.setTranslationY(position * ((float) view.getHeight()));
            } else {
                view.setAlpha(0.0f);
            }
        }
    }

    public VerticalViewPager(android.content.Context context) {
        super(context);
        init();
    }

    public VerticalViewPager(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setPageTransformer(true, new com.navdy.client.app.ui.VerticalViewPager.VerticalPageTransformer());
        setOverScrollMode(2);
    }

    private android.view.MotionEvent swapXY(android.view.MotionEvent event) {
        android.view.MotionEvent ev = android.view.MotionEvent.obtain(event);
        float width = (float) getWidth();
        float height = (float) getHeight();
        ev.setLocation((ev.getY() / height) * width, (ev.getX() / width) * height);
        return ev;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent ev) {
        return super.onInterceptTouchEvent(swapXY(ev));
    }

    public boolean onTouchEvent(android.view.MotionEvent ev) {
        return super.onTouchEvent(swapXY(ev));
    }
}
