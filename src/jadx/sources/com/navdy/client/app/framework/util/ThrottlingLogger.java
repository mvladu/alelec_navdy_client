package com.navdy.client.app.framework.util;

public class ThrottlingLogger {
    private android.util.SparseLongArray lastLogTimes = new android.util.SparseLongArray();
    private final com.navdy.service.library.log.Logger logger;
    private final long wait;

    public ThrottlingLogger(@android.support.annotation.NonNull com.navdy.service.library.log.Logger logger2, long wait2) {
        this.logger = logger2;
        this.wait = wait2;
    }

    public void v(int tag, java.lang.String msg) {
        if (checkTime(tag)) {
            this.logger.v(msg);
        }
    }

    public void v(int tag, java.lang.String msg, java.lang.Throwable tr) {
        if (checkTime(tag)) {
            this.logger.v(msg, tr);
        }
    }

    public void d(int tag, java.lang.String msg) {
        if (checkTime(tag)) {
            this.logger.d(msg);
        }
    }

    public void d(int tag, java.lang.String msg, java.lang.Throwable tr) {
        if (checkTime(tag)) {
            this.logger.d(msg, tr);
        }
    }

    public void i(int tag, java.lang.String msg) {
        if (checkTime(tag)) {
            this.logger.i(msg);
        }
    }

    public void i(int tag, java.lang.String msg, java.lang.Throwable tr) {
        if (checkTime(tag)) {
            this.logger.i(msg, tr);
        }
    }

    public void w(int tag, java.lang.String msg) {
        if (checkTime(tag)) {
            this.logger.w(msg);
        }
    }

    public void w(int tag, java.lang.String msg, java.lang.Throwable tr) {
        if (checkTime(tag)) {
            this.logger.w(msg, tr);
        }
    }

    public void e(int tag, java.lang.String msg) {
        if (checkTime(tag)) {
            this.logger.e(msg);
        }
    }

    public void e(int tag, java.lang.String msg, java.lang.Throwable tr) {
        if (checkTime(tag)) {
            this.logger.e(msg, tr);
        }
    }

    private synchronized boolean checkTime(int tag) {
        boolean isReady;
        long currentTime = java.lang.System.currentTimeMillis();
        isReady = currentTime - this.lastLogTimes.get(tag, 0) > this.wait;
        if (isReady) {
            this.lastLogTimes.put(tag, currentTime);
        }
        return isReady;
    }
}
