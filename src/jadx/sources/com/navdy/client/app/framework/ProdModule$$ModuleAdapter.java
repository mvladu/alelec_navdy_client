package com.navdy.client.app.framework;

public final class ProdModule$$ModuleAdapter extends dagger.internal.ModuleAdapter<com.navdy.client.app.framework.ProdModule> {
    private static final java.lang.Class<?>[] INCLUDES = new java.lang.Class[0];
    private static final java.lang.String[] INJECTS = {"members/com.navdy.client.app.NavdyApplication", "members/com.navdy.client.app.ui.settings.AudioSettingsActivity", "members/com.navdy.client.app.framework.servicehandler.SpeechServiceHandler", "members/com.navdy.client.app.framework.util.TTSAudioRouter", "members/com.navdy.client.app.framework.util.CarMdClient", "members/com.navdy.client.app.framework.servicehandler.NetworkStatusManager", "members/com.navdy.client.app.framework.AppInstance", "members/com.navdy.client.app.ui.search.SearchActivity", "members/com.navdy.client.app.ui.homescreen.SuggestionsFragment", "members/com.navdy.client.app.ui.routing.RoutingActivity", "members/com.navdy.client.app.ui.favorites.FavoritesEditActivity", "members/com.navdy.client.debug.SubmitTicketFragment", "members/com.navdy.client.app.framework.servicehandler.VoiceServiceHandler", "members/com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity", "members/com.navdy.client.app.service.ActivityRecognizedCallbackService"};
    private static final java.lang.Class<?>[] STATIC_INJECTIONS = new java.lang.Class[0];

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideHttpManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.service.library.network.http.IHttpManager> implements javax.inject.Provider<com.navdy.service.library.network.http.IHttpManager> {
        private final com.navdy.client.app.framework.ProdModule module;

        public ProvideHttpManagerProvidesAdapter(com.navdy.client.app.framework.ProdModule module2) {
            super("com.navdy.service.library.network.http.IHttpManager", true, "com.navdy.client.app.framework.ProdModule", "provideHttpManager");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.service.library.network.http.IHttpManager get() {
            return this.module.provideHttpManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideNetworkStatusManagerProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.client.app.framework.servicehandler.NetworkStatusManager> implements javax.inject.Provider<com.navdy.client.app.framework.servicehandler.NetworkStatusManager> {
        private final com.navdy.client.app.framework.ProdModule module;

        public ProvideNetworkStatusManagerProvidesAdapter(com.navdy.client.app.framework.ProdModule module2) {
            super("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", true, "com.navdy.client.app.framework.ProdModule", "provideNetworkStatusManager");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.client.app.framework.servicehandler.NetworkStatusManager get() {
            return this.module.provideNetworkStatusManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideSharedPreferencesProvidesAdapter extends dagger.internal.ProvidesBinding<android.content.SharedPreferences> implements javax.inject.Provider<android.content.SharedPreferences> {
        private final com.navdy.client.app.framework.ProdModule module;

        public ProvideSharedPreferencesProvidesAdapter(com.navdy.client.app.framework.ProdModule module2) {
            super("android.content.SharedPreferences", true, "com.navdy.client.app.framework.ProdModule", "provideSharedPreferences");
            this.module = module2;
            setLibrary(true);
        }

        public android.content.SharedPreferences get() {
            return this.module.provideSharedPreferences();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTTSAudioRouterProvidesAdapter extends dagger.internal.ProvidesBinding<com.navdy.client.app.framework.util.TTSAudioRouter> implements javax.inject.Provider<com.navdy.client.app.framework.util.TTSAudioRouter> {
        private final com.navdy.client.app.framework.ProdModule module;

        public ProvideTTSAudioRouterProvidesAdapter(com.navdy.client.app.framework.ProdModule module2) {
            super("com.navdy.client.app.framework.util.TTSAudioRouter", true, "com.navdy.client.app.framework.ProdModule", "provideTTSAudioRouter");
            this.module = module2;
            setLibrary(true);
        }

        public com.navdy.client.app.framework.util.TTSAudioRouter get() {
            return this.module.provideTTSAudioRouter();
        }
    }

    public ProdModule$$ModuleAdapter() {
        super(com.navdy.client.app.framework.ProdModule.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, true);
    }

    public void getBindings(dagger.internal.BindingsGroup bindings, com.navdy.client.app.framework.ProdModule module) {
        bindings.contributeProvidesBinding("com.navdy.service.library.network.http.IHttpManager", new com.navdy.client.app.framework.ProdModule$$ModuleAdapter.ProvideHttpManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.client.app.framework.util.TTSAudioRouter", new com.navdy.client.app.framework.ProdModule$$ModuleAdapter.ProvideTTSAudioRouterProvidesAdapter(module));
        bindings.contributeProvidesBinding("android.content.SharedPreferences", new com.navdy.client.app.framework.ProdModule$$ModuleAdapter.ProvideSharedPreferencesProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", new com.navdy.client.app.framework.ProdModule$$ModuleAdapter.ProvideNetworkStatusManagerProvidesAdapter(module));
    }
}
