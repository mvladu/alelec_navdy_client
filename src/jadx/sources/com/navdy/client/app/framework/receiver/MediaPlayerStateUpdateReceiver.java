package com.navdy.client.app.framework.receiver;

public class MediaPlayerStateUpdateReceiver extends android.content.BroadcastReceiver {
    private static final java.lang.String INTENT_PARAM_PLAYING = "playing";
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.receiver.MediaPlayerStateUpdateReceiver.class);

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        sLogger.d("onReceive, intent: " + intent);
        java.lang.String action = intent.getAction();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(action)) {
            java.lang.String packageName = action.substring(0, action.lastIndexOf(46));
            if (com.navdy.client.app.framework.glances.GlanceConstants.SPOTIFY.equals(packageName) && intent.getBooleanExtra(INTENT_PARAM_PLAYING, false)) {
                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().setLastMusicApp(packageName);
            }
        }
    }
}
