package com.navdy.client.app.framework.servicehandler;

public class NetworkStatusManager {
    public static final boolean ENABLE_REACHABILITY_CHECK = false;
    private static final int ENDPOINT_CONNECTION_TIMEOUT = 30000;
    private static final int FAIL_PING_COUNT = 2;
    private static final int FAIL_PING_TIMEOUT = 30000;
    private static final int FAST_FAIL_PING_TIMEOUT = 2000;
    public static final int NETWORK_CHECK_RETRY_INTERVAL = 15000;
    private static final java.lang.String PING_URL = "http://www.google.com";
    public static final int REACHABILITY_CHECK_INITIAL_DELAY = 5000;
    private static final int SUCCESS_PING_TIMEOUT = 120000;
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.class);
    private android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private boolean isNetworkReachable;
    private com.navdy.service.library.events.settings.NetworkStateChange lastState;
    @javax.inject.Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager;
    private java.lang.Runnable makeNetworkCall = new com.navdy.client.app.framework.servicehandler.NetworkStatusManager.Anon3();
    private java.lang.Runnable networkCheck = new com.navdy.client.app.framework.servicehandler.NetworkStatusManager.Anon2();
    private java.lang.Runnable reachableCheck = new com.navdy.client.app.framework.servicehandler.NetworkStatusManager.Anon1();
    private android.content.BroadcastReceiver receiver = new com.navdy.client.app.framework.servicehandler.NetworkStatusManager.Anon4();
    private boolean registered;
    private com.navdy.service.library.device.RemoteDevice remoteDevice;
    private int retryCount;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.makeNetworkCall, 5);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.v("networkCheck");
            com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.checkForNetwork();
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            okhttp3.Response response = null;
            try {
                if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.context)) {
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.sendEvent();
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.d("Ping request bailing out: no n/w, stop reachable");
                    if (response != null) {
                        try {
                            okhttp3.ResponseBody body = response.body();
                            if (body != null) {
                                body.close();
                            }
                        } catch (Throwable t) {
                            com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.e(t);
                        }
                    }
                } else {
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.access$Anon404(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this);
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.d("Ping request start:" + com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.retryCount + " reachable:" + com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.isNetworkReachable);
                    response = com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.mHttpManager.getClientCopy().connectTimeout(30000, java.util.concurrent.TimeUnit.MILLISECONDS).build().newCall(new okhttp3.Request.Builder().head().url(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.PING_URL).get().build()).execute();
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.d("Ping response succeeded :" + com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.retryCount);
                    if (!com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.isNetworkReachable) {
                        com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.isNetworkReachable = true;
                        com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.v("n/w is reachable now");
                        com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.sendEvent();
                        com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.isNetworkReachable));
                    } else {
                        com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.v("n/w still reachable");
                        com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.sendEvent();
                    }
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.retryCount = 0;
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.v("run reachable check in 120000");
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.handler.removeCallbacks(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.reachableCheck);
                    com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.handler.postDelayed(com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.reachableCheck, com.here.odnp.config.OdnpConfigStatic.OEM_MAX_HIGH_POWER_INTERVAL);
                    if (response != null) {
                        try {
                            okhttp3.ResponseBody body2 = response.body();
                            if (body2 != null) {
                                body2.close();
                            }
                        } catch (Throwable t2) {
                            com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.e(t2);
                        }
                    }
                }
            } catch (Throwable t3) {
                com.navdy.client.app.framework.servicehandler.NetworkStatusManager.sLogger.e(t3);
            }
        }
    }

    class Anon4 extends android.content.BroadcastReceiver {
        Anon4() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                com.navdy.client.app.framework.servicehandler.NetworkStatusManager.this.checkForNetwork();
            }
        }
    }

    public static class ReachabilityEvent {
        public boolean isReachable;

        ReachabilityEvent(boolean reachability) {
            this.isReachable = reachability;
        }
    }

    static /* synthetic */ int access$Anon404(com.navdy.client.app.framework.servicehandler.NetworkStatusManager x0) {
        int i = x0.retryCount + 1;
        x0.retryCount = i;
        return i;
    }

    public NetworkStatusManager() {
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
    }

    public boolean checkForNetwork() {
        if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.context)) {
            sLogger.d("checkForNetwork: Connectivity changed, no n/w, stop reachable");
            cleanState();
            sendEvent();
            com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent(false));
            this.handler.removeCallbacks(this.networkCheck);
            this.handler.postDelayed(this.networkCheck, 15000);
        } else {
            sLogger.d("checkForNetwork: Connectivity changed, n/w available, Connected to internet");
            sendEvent();
            com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent(true));
        }
        return false;
    }

    public synchronized void register() {
        cleanState();
        if (!this.registered) {
            android.content.IntentFilter filter = new android.content.IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
            this.registered = true;
            sLogger.v("stop reachable check");
            this.handler.removeCallbacks(this.reachableCheck);
            if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.context)) {
                sLogger.v("launch reachable check");
                this.isNetworkReachable = true;
            } else {
                sLogger.v("Not connected to the n/w, Check after some time");
                this.handler.postDelayed(this.networkCheck, 15000);
            }
            this.context.registerReceiver(this.receiver, filter);
        }
    }

    public synchronized void unregister() {
        cleanState();
        if (this.registered) {
            com.navdy.client.app.NavdyApplication.getAppContext().unregisterReceiver(this.receiver);
            this.registered = false;
        }
    }

    public void onRemoteDeviceConnected(com.navdy.service.library.device.RemoteDevice remoteDevice2) {
        this.lastState = null;
        this.remoteDevice = remoteDevice2;
        sendEvent();
    }

    public void onRemoteDeviceDisconnected() {
        this.remoteDevice = null;
        this.lastState = null;
    }

    private void sendEvent() {
        try {
            if (this.remoteDevice == null) {
                sLogger.v("[NetworkStateChange] hud not connected");
                return;
            }
            com.navdy.service.library.events.settings.NetworkStateChange status = getNetworkStateChange();
            if (isNetworkStateEquals(status, this.lastState)) {
                sLogger.v("[NetworkStateChange] already sent lastState[" + this.lastState.networkAvailable + "]");
                return;
            }
            this.lastState = status;
            sLogger.v("NetworkStateChange available[" + status.networkAvailable + "] cell[" + status.cellNetwork + "] wifi[" + status.wifiNetwork + "] reach[" + status.reachability + "]");
            this.remoteDevice.postEvent((com.squareup.wire.Message) status);
            sLogger.v("[NetworkStateChange] status sent");
        } catch (Throwable t) {
            sLogger.e("[NetworkStateChange]", t);
        }
    }

    private com.navdy.service.library.events.settings.NetworkStateChange getNetworkStateChange() {
        boolean cell;
        boolean wifi = true;
        boolean connected = com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.context);
        if (!connected || !com.navdy.service.library.util.SystemUtils.isConnectedToCellNetwork(this.context)) {
            cell = false;
        } else {
            cell = true;
        }
        if (!connected || !com.navdy.service.library.util.SystemUtils.isConnectedToWifi(this.context)) {
            wifi = false;
        }
        boolean isReachable = canReachInternet();
        if (connected && !isReachable) {
            sLogger.v("connected but not reachable");
            connected = false;
        }
        return new com.navdy.service.library.events.settings.NetworkStateChange(java.lang.Boolean.valueOf(connected), java.lang.Boolean.valueOf(cell), java.lang.Boolean.valueOf(wifi), java.lang.Boolean.valueOf(isReachable));
    }

    private boolean isNetworkStateEquals(com.navdy.service.library.events.settings.NetworkStateChange left, com.navdy.service.library.events.settings.NetworkStateChange right) {
        return left != null && right != null && isBooleanEquals(left.networkAvailable, right.networkAvailable) && isBooleanEquals(left.cellNetwork, right.cellNetwork) && isBooleanEquals(left.wifiNetwork, right.wifiNetwork) && isBooleanEquals(left.reachability, right.reachability);
    }

    private boolean isBooleanEquals(java.lang.Boolean left, java.lang.Boolean right) {
        return java.lang.Boolean.TRUE.equals(left) == java.lang.Boolean.TRUE.equals(right);
    }

    public boolean canReachInternet() {
        return com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(this.context);
    }

    private void cleanState() {
        this.retryCount = 0;
        sLogger.v("stop reachable check");
        this.handler.removeCallbacks(this.reachableCheck);
        this.isNetworkReachable = false;
        this.lastState = null;
    }
}
