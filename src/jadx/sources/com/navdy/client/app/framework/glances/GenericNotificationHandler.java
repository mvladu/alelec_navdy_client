package com.navdy.client.app.framework.glances;

public class GenericNotificationHandler {
    public static void handleGenericNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        if (com.navdy.client.app.ui.glances.GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            java.lang.String message = getBestAvailableText(notification);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(message)) {
                java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
                java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name(), message));
                com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
            }
        }
    }

    @android.support.annotation.Nullable
    private static java.lang.String getBestAvailableText(android.app.Notification notification) {
        java.lang.CharSequence ticker = notification.tickerText;
        android.os.Bundle extras = android.support.v7.app.NotificationCompat.getExtras(notification);
        if (extras != null) {
            java.lang.String bigText = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.bigText");
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(bigText)) {
                return bigText;
            }
            java.lang.String text = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text)) {
                return text;
            }
            java.lang.String subText = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.subText");
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(subText)) {
                return subText;
            }
            java.lang.String title = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.title");
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(title)) {
                return title;
            }
        }
        if (ticker != null) {
            return ticker.toString();
        }
        return null;
    }
}
