package com.navdy.client.app.framework.models;

public class MountInfo {
    public boolean mediumSupported = true;
    public com.navdy.client.app.framework.models.MountInfo.MountType recommendedMount = com.navdy.client.app.framework.models.MountInfo.MountType.SHORT;
    public boolean shortSupported = true;
    public boolean tallSupported = true;

    public enum MountType {
        SHORT(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.SHORT_MOUNT),
        MEDIUM(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.MEDIUM_MOUNT),
        TALL(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.TALL_MOUNT);
        
        java.lang.String type;

        private MountType(java.lang.String type2) {
            this.type = type2;
        }

        public java.lang.String getValue() {
            return this.type;
        }

        public static com.navdy.client.app.framework.models.MountInfo.MountType getMountTypeForValue(java.lang.String value) {
            char c = 65535;
            switch (value.hashCode()) {
                case -1362194858:
                    if (value.equals(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.SHORT_MOUNT)) {
                        c = 1;
                        break;
                    }
                    break;
                case -236362033:
                    if (value.equals(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.MEDIUM_MOUNT)) {
                        c = 2;
                        break;
                    }
                    break;
                case 548719111:
                    if (value.equals(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.TALL_MOUNT)) {
                        c = 3;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 2:
                    return MEDIUM;
                case 3:
                    return TALL;
                default:
                    return SHORT;
            }
        }

        public static com.navdy.client.app.framework.models.MountInfo.MountType getMountTypeForName(java.lang.String name) {
            char c = 65535;
            switch (name.hashCode()) {
                case -2024701067:
                    if (name.equals("MEDIUM")) {
                        c = 2;
                        break;
                    }
                    break;
                case 2567341:
                    if (name.equals("TALL")) {
                        c = 3;
                        break;
                    }
                    break;
                case 79011047:
                    if (name.equals("SMALL")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 2:
                    return MEDIUM;
                case 3:
                    return TALL;
                default:
                    return SHORT;
            }
        }
    }

    public boolean noneOfTheMountsWillWork() {
        return !this.shortSupported && !this.mediumSupported && !this.tallSupported;
    }
}
