package com.navdy.client.app.framework.glances;

public class GlancesHelper {
    private static final float HUD_VERSION_GLANCE_CHANGE = 0.2f;
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.glances.GlancesHelper.class);

    static java.util.HashSet<java.lang.String> getActions(android.app.Notification notif) {
        java.util.HashSet<java.lang.String> actions = new java.util.HashSet<>();
        int actionCount = android.support.v4.app.NotificationCompat.getActionCount(notif);
        for (int actionIndex = 0; actionIndex < actionCount; actionIndex++) {
            android.support.v4.app.NotificationCompat.Action action = android.support.v4.app.NotificationCompat.getAction(notif, actionIndex);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(action.title)) {
                actions.add(action.title.toString());
            }
        }
        return actions;
    }

    public static void sendEvent(com.navdy.service.library.events.glances.GlanceEvent message) {
        printGlance(message);
        com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent((com.squareup.wire.Message) message);
        }
    }

    public static java.lang.String getId() {
        return java.util.UUID.randomUUID().toString();
    }

    private static void printGlance(com.navdy.service.library.events.glances.GlanceEvent event) {
        sLogger.v("[glance-event] id[" + event.id + "]" + " app[" + event.provider + "]" + " type[" + event.glanceType + "]" + " postTime[" + event.postTime + "]");
        if (event.glanceData == null || event.glanceData.size() == 0) {
            sLogger.v("[glance-event] no data");
        } else {
            for (com.navdy.service.library.events.glances.KeyValue keyValue : event.glanceData) {
                sLogger.v("[glance-event] data key[" + keyValue.key + "] val[" + keyValue.value + "]");
            }
        }
        if (event.actions == null || event.actions.size() == 0) {
            sLogger.v("[glance-event] no actions");
            return;
        }
        for (com.navdy.service.library.events.glances.GlanceEvent.GlanceActions action : event.actions) {
            sLogger.v("[glance-event] action[" + action + "]");
        }
    }

    public static boolean isHudVersionCompatible(java.lang.String version) {
        try {
            if (java.lang.Float.parseFloat(version) >= HUD_VERSION_GLANCE_CHANGE) {
                return true;
            }
            return false;
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }
}
