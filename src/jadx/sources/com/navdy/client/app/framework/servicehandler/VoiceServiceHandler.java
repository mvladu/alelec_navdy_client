package com.navdy.client.app.framework.servicehandler;

public class VoiceServiceHandler {
    private static final java.lang.String GOOGLE_APP_PACKAGE = "com.google.android.googlequicksearchbox";
    private static final int VOICE_SEARCH_MAX_RESULTS = 10;
    private static com.navdy.client.app.framework.servicehandler.VoiceServiceHandler instance = null;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.class);
    private boolean alreadyRetried = false;
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private int hfpSoundId = -1;
    private android.media.SoundPool hfpSoundPool = new android.media.SoundPool(1, 0, 0);
    private boolean isReadyForSpeech = false;
    private boolean listeningOverBt;
    @javax.inject.Inject
    com.navdy.client.app.framework.util.TTSAudioRouter mAudioRouter;
    private android.speech.RecognitionListener recognitionListener = new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon2();
    private final android.content.Intent recognizerIntent;
    private final android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
    private android.speech.SpeechRecognizer sr;
    private java.util.concurrent.atomic.AtomicBoolean userCanceledVoiceSearch = new java.util.concurrent.atomic.AtomicBoolean(false);

    class Anon1 implements com.navdy.client.app.framework.util.TTSAudioRouter.VoiceSearchSetupListener {

        /* renamed from: com.navdy.client.app.framework.servicehandler.VoiceServiceHandler$Anon1$Anon1 reason: collision with other inner class name */
        class C0050Anon1 implements java.lang.Runnable {
            C0050Anon1() {
            }

            public void run() {
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sr = android.speech.SpeechRecognizer.createSpeechRecognizer(com.navdy.client.app.NavdyApplication.getAppContext());
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sr.setRecognitionListener(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.recognitionListener);
                try {
                    com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sr.startListening(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.recognizerIntent);
                } catch (java.lang.SecurityException e) {
                    com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.e("Unable to start the listening intent due to SecurityException.", e);
                    com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.recognitionListener.onError(9);
                }
            }
        }

        Anon1() {
        }

        public void onAudioInputReady(boolean usingBluetooth, boolean hfpConnectionFailed) {
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt = usingBluetooth;
            if (com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt && com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.hfpSoundId != -1) {
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.hfpSoundPool.play(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.hfpSoundId, 1.0f, 1.0f, 1, 0, 1.0f);
            }
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.handler.post(new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon1.C0050Anon1());
        }
    }

    class Anon2 implements android.speech.RecognitionListener {
        private static final long MAX_LISTENING_TIME = 30000;
        private static final long MIN_LISTENING_TIME = 2000;
        private long beginTimestamp = 0;
        private android.os.Handler handler = new android.os.Handler();

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon2.this.onError(6);
            }
        }

        Anon2() {
        }

        public void onReadyForSpeech(android.os.Bundle params) {
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.isReadyForSpeech = true;
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d("onReadyForSpeech true");
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_LISTENING).listeningOverBluetooth(java.lang.Boolean.valueOf(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt)).build());
            startTimeoutTimer();
        }

        void startTimeoutTimer() {
            this.handler.postDelayed(new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon2.Anon1(), 30000);
        }

        public void onBeginningOfSpeech() {
            this.beginTimestamp = new java.util.Date().getTime();
            this.handler.removeCallbacksAndMessages(null);
            startTimeoutTimer();
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_LISTENING).volumeLevel(java.lang.Integer.valueOf(50)).listeningOverBluetooth(java.lang.Boolean.valueOf(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt)).build());
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d("onBeginningOfSpeech");
        }

        public void onRmsChanged(float rmsdB) {
        }

        public void onBufferReceived(byte[] buffer) {
        }

        public void onEndOfSpeech() {
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d("onEndOfSpeech");
            this.handler.removeCallbacksAndMessages(null);
            startTimeoutTimer();
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SEARCHING).listeningOverBluetooth(java.lang.Boolean.valueOf(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt)).build());
        }

        public void onError(int error) {
            java.lang.String errorType;
            this.handler.removeCallbacksAndMessages(null);
            if (com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.userCanceledVoiceSearch.get()) {
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.v("Voice search was canceled on the HUD so ignoring this error: " + error);
            } else if (com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.isReadyForSpeech || error != 7) {
                com.navdy.service.library.events.audio.VoiceSearchResponse.Builder responseBuilder = new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder();
                responseBuilder.state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_ERROR);
                long timeListened = new java.util.Date().getTime() - this.beginTimestamp;
                if (error == 7) {
                    if (timeListened <= 0 || timeListened >= MIN_LISTENING_TIME) {
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d(timeListened + "ms. elapsed since we started listening.");
                    } else {
                        java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>();
                        attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.HudVoiceSearchAttributes.NOISE_RETRY, java.lang.Boolean.toString(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.alreadyRetried));
                        com.navdy.client.app.framework.LocalyticsManager.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.HUD_VOICE_SEARCH_AMBIENT_NOISE_FAILURE, attributes);
                        if (!com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.alreadyRetried && com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sr != null) {
                            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d("Only " + timeListened + "ms. elapsed since we started listening and we haven't retried yet so starting the mic again.");
                            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.alreadyRetried = true;
                            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_LISTENING).listeningOverBluetooth(java.lang.Boolean.valueOf(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt)).build());
                            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sr.startListening(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.recognizerIntent);
                            return;
                        }
                    }
                }
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.onStopListening();
                android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
                switch (error) {
                    case 1:
                    case 2:
                    case 4:
                    case 5:
                        switch (error) {
                            case 1:
                                errorType = "ERROR_NETWORK_TIMEOUT";
                                break;
                            case 4:
                                errorType = "ERROR_SERVER";
                                break;
                            case 5:
                                errorType = "ERROR_CLIENT";
                                break;
                            default:
                                errorType = "ERROR_NETWORK";
                                break;
                        }
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.e("onError: " + errorType);
                        java.lang.String message = context.getString(com.navdy.client.R.string.network_issues);
                        responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.OFFLINE);
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.mAudioRouter.processTTSRequest(message, null, false);
                        break;
                    case 3:
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.e("onError: ERROR_AUDIO");
                        java.lang.String message2 = context.getString(com.navdy.client.R.string.audio_recording_error);
                        responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.FAILED_TO_START);
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.mAudioRouter.processTTSRequest(message2, null, false);
                        break;
                    case 6:
                    case 7:
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.e("onError: " + (error == 6 ? "SpeechRecognizer.ERROR_SPEECH_TIMEOUT" : "ERROR_NO_MATCH"));
                        if (error == 7 && timeListened > 0 && timeListened < MIN_LISTENING_TIME) {
                            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d("Only " + timeListened + "ms. elapsed since we started listening and we've already retried so giving up due to environment noise.");
                            responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.AMBIENT_NOISE);
                            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(com.navdy.client.R.string.environment_too_loud), null, false);
                            break;
                        } else {
                            responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED);
                            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(com.navdy.client.R.string.no_voice_match), null, false);
                            break;
                        }
                        break;
                    case 8:
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.e("onError: ERROR_RECOGNIZER_BUSY");
                        responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.FAILED_TO_START);
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(com.navdy.client.R.string.mic_busy), null, false);
                        break;
                    case 9:
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.e("onError: ERROR_INSUFFICIENT_PERMISSIONS");
                        responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NEED_PERMISSION);
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.mAudioRouter.processTTSRequest(context.getString(com.navdy.client.R.string.need_mic_permissions), null, false);
                        break;
                    default:
                        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.e("onError " + error);
                        responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.FAILED_TO_START);
                        break;
                }
                responseBuilder.listeningOverBluetooth(java.lang.Boolean.valueOf(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt));
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.onVoiceSearchComplete();
            } else {
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.w("Ignoring an error that occurred before the engine was ready for speech. Error: " + error);
            }
        }

        public void onPartialResults(android.os.Bundle partialResults) {
        }

        public void onResults(android.os.Bundle results) {
            this.handler.removeCallbacksAndMessages(null);
            if (com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.userCanceledVoiceSearch.get()) {
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.v("Voice search was canceled on the HUD so ignoring the results.");
                return;
            }
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.onStopListening();
            java.lang.String query = null;
            float confidenceLevel = -1.0f;
            if (results != null) {
                float[] confidence = results.getFloatArray("confidence_scores");
                java.util.ArrayList data = results.getStringArrayList("results_recognition");
                int i = 0;
                while (true) {
                    if (data == null || i >= data.size()) {
                        break;
                    } else if (data.get(i) != null) {
                        query = (java.lang.String) data.get(i);
                        if (confidence != null && i < confidence.length) {
                            confidenceLevel = confidence[i];
                        }
                    } else {
                        i++;
                    }
                }
            }
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d("onResult: \"" + query + "\" confidence level: " + confidenceLevel);
            if (confidenceLevel <= 0.0f || confidenceLevel >= 0.5f) {
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SEARCHING).recognizedWords(query).listeningOverBluetooth(java.lang.Boolean.valueOf(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.listeningOverBt)).confidenceLevel(java.lang.Integer.valueOf((int) (100.0f * confidenceLevel))).build());
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.handleVoiceCommand(query);
                if (query != null) {
                    com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.logAnonymousAnalytics(query);
                    return;
                }
                return;
            }
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.handleError(com.navdy.client.R.string.not_sure_i_understand);
        }

        public void onEvent(int eventType, android.os.Bundle params) {
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.sLogger.d("onEvent " + eventType);
        }
    }

    class Anon3 implements com.navdy.client.app.framework.search.NavdySearch.SearchCallback {
        final /* synthetic */ java.lang.String val$prefix;
        final /* synthetic */ java.lang.String val$query;

        Anon3(java.lang.String str, java.lang.String str2) {
            this.val$query = str;
            this.val$prefix = str2;
        }

        public void onSearchStarted() {
            com.navdy.service.library.events.audio.VoiceSearchResponse.Builder responseBuilder = new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder();
            responseBuilder.state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SEARCHING).recognizedWords(this.val$query);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
        }

        public void onSearchCompleted(com.navdy.client.app.framework.search.SearchResults searchResults) {
            if (com.navdy.client.app.framework.search.SearchResults.isEmpty(searchResults)) {
                com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sayNoResultFound(searchResults.getQuery());
                return;
            }
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sendDestinationsToHud(searchResults.getDestinationsList(), searchResults.getQuery(), this.val$prefix);
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().stopRouting();
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.ui.Screen val$screen;

        Anon5(com.navdy.service.library.events.ui.Screen screen) {
            this.val$screen = screen;
        }

        public void run() {
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(this.val$screen).build());
        }
    }

    class Anon6 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
        final /* synthetic */ java.util.ArrayList val$destinations;
        final /* synthetic */ java.lang.String val$prefix;
        final /* synthetic */ java.lang.String val$query;

        Anon6(java.util.ArrayList arrayList, java.lang.String str, java.lang.String str2) {
            this.val$destinations = arrayList;
            this.val$query = str;
            this.val$prefix = str2;
        }

        public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
            replaceAndSend(destination);
        }

        public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
            replaceAndSend(destination);
        }

        private void replaceAndSend(com.navdy.client.app.framework.models.Destination destination) {
            this.val$destinations.set(0, destination);
            com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.this.sendToHud(this.val$destinations, this.val$query, this.val$prefix);
        }
    }

    public static synchronized com.navdy.client.app.framework.servicehandler.VoiceServiceHandler getInstance() {
        com.navdy.client.app.framework.servicehandler.VoiceServiceHandler voiceServiceHandler;
        synchronized (com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.class) {
            if (instance == null) {
                instance = new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler();
            }
            voiceServiceHandler = instance;
        }
        return voiceServiceHandler;
    }

    private VoiceServiceHandler() {
        try {
            this.hfpSoundId = this.hfpSoundPool.load(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.raw.voice_search, 1);
        } catch (java.lang.RuntimeException re) {
            sLogger.e("RuntimeException loading the sound ", re);
        }
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.recognizerIntent = new android.content.Intent("android.speech.action.RECOGNIZE_SPEECH");
        this.recognizerIntent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        this.recognizerIntent.putExtra("calling_package", getClass().getPackage().getName());
        this.recognizerIntent.putExtra("android.speech.extra.PARTIAL_RESULTS", false);
        this.recognizerIntent.putExtra("android.speech.extra.PROMPT", true);
        this.recognizerIntent.putExtra("android.speech.extra.MAX_RESULTS", 1);
        this.recognizerIntent.putExtra("android.speech.extras.SPEECH_INPUT_MINIMUM_LENGTH_MILLIS", 7000);
        this.recognizerIntent.putExtra("android.speech.extras.SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS", com.navdy.client.app.ui.settings.AudioDialogActivity.DELAY_MILLIS);
        this.recognizerIntent.putExtra("android.speech.extras.SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS", com.navdy.client.app.ui.settings.AudioDialogActivity.DELAY_MILLIS);
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onVoiceAssistRequest(com.navdy.service.library.events.audio.VoiceAssistRequest voiceAssistRequest) {
        handleGoogleNowRequest(this.mAudioRouter);
    }

    @com.squareup.otto.Subscribe
    public void onVoiceSearchRequest(com.navdy.service.library.events.audio.VoiceSearchRequest voiceSearchRequest) {
        if (voiceSearchRequest == null || !java.lang.Boolean.TRUE.equals(voiceSearchRequest.end)) {
            this.userCanceledVoiceSearch.set(false);
            handleVoiceSearchRequest();
            return;
        }
        sLogger.v("Receiving request to cancel Voice search from the HUD");
        this.userCanceledVoiceSearch.set(true);
        onStopListening();
        onVoiceSearchComplete();
    }

    private void handleGoogleNowRequest(@android.support.annotation.NonNull com.navdy.client.app.framework.util.TTSAudioRouter audioRouter) {
        sLogger.d("Handle Google now request");
        try {
            audioRouter.cancelTtsAndClearQueue();
            android.content.Intent intent = new android.content.Intent("android.speech.action.VOICE_SEARCH_HANDS_FREE");
            intent.addFlags(268435456);
            intent.setPackage(GOOGLE_APP_PACKAGE);
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            java.util.List<android.content.pm.ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(intent, 0);
            if (resolveInfoList == null || resolveInfoList.size() == 0) {
                sLogger.e("Google now is not installed");
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState.VOICE_ASSIST_NOT_AVAILABLE));
                return;
            }
            context.startActivity(intent);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse.VoiceAssistState.VOICE_ASSIST_LISTENING));
        } catch (Throwable th) {
            sLogger.e("Unable to tell the HUD that we couldn't start Google Now", th);
        }
    }

    private void handleVoiceSearchRequest() {
        this.mAudioRouter.cancelTtsAndClearQueue();
        if (!this.sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USE_EXPERIMENTAL_HUD_VOICE_ENABLED, false) || this.sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_VOICE_USED_BEFORE, false)) {
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_STARTING).build());
            this.alreadyRetried = false;
            this.mAudioRouter.setVoiceSearchSetupListener(new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon1());
            this.mAudioRouter.startVoiceSearchSession();
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.NAVIGATE_USING_HUD_VOICE_SEARCH);
            return;
        }
        this.sharedPreferences.edit().putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_VOICE_USED_BEFORE, true).apply();
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        java.lang.String helloThere = context.getString(com.navdy.client.R.string.hello_there);
        this.mAudioRouter.processTTSRequest(helloThere + context.getString(com.navdy.client.R.string.i_can_do_this), null, false);
        com.navdy.service.library.events.audio.VoiceSearchResponse.Builder responseBuilder = new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder();
        responseBuilder.state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_ERROR);
        responseBuilder.error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NOT_AVAILABLE);
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
        onVoiceSearchComplete();
    }

    private void onStopListening() {
        this.isReadyForSpeech = false;
        if (this.listeningOverBt && this.hfpSoundId != -1) {
            this.hfpSoundPool.play(this.hfpSoundId, 1.0f, 1.0f, 1, 0, 1.0f);
        }
        if (this.sr != null) {
            try {
                this.sr.stopListening();
                this.sr.cancel();
                this.sr.destroy();
            } catch (java.lang.Exception e) {
                sLogger.e("Something went wrong while trying to close the speech recognizer.", e);
            }
            this.sr = null;
        }
    }

    private void onVoiceSearchComplete() {
        this.mAudioRouter.stopVoiceSearchSession();
    }

    private void handleVoiceCommand(java.lang.String voiceCommand) {
        java.lang.String query;
        java.lang.String prefix;
        sLogger.d("Handling the command: \"" + voiceCommand + "\"");
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(voiceCommand)) {
            handleError(com.navdy.client.R.string.no_voice_match);
            return;
        }
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        java.lang.String navdy = context.getString(com.navdy.client.R.string.navdy) + " ";
        if (voiceCommand.toLowerCase().startsWith(navdy)) {
            voiceCommand = voiceCommand.substring(navdy.length() - 1);
        }
        java.lang.String navdy2 = " " + context.getString(com.navdy.client.R.string.navdy);
        if (voiceCommand.toLowerCase().endsWith(navdy2)) {
            voiceCommand = voiceCommand.substring(0, voiceCommand.length() - navdy2.length());
        }
        if (handleSpecialCommands(voiceCommand, this.mAudioRouter)) {
            com.navdy.service.library.events.audio.VoiceSearchResponse.Builder responseBuilder = new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder();
            responseBuilder.state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SUCCESS);
            responseBuilder.recognizedWords(voiceCommand);
            responseBuilder.prefix(voiceCommand);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
            return;
        }
        com.navdy.client.app.framework.util.VoiceCommand vc = com.navdy.client.app.framework.util.VoiceCommandUtils.getMatchingVoiceCommand(com.navdy.client.R.raw.navigation_prefixes, voiceCommand);
        if (vc == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(vc.query)) {
            prefix = null;
            query = voiceCommand;
        } else {
            prefix = vc.prefix;
            query = vc.query;
        }
        sLogger.d("Cleaned up voice query is: " + query);
        if (!handleHomeAndWork(query)) {
            new com.navdy.client.app.framework.search.NavdySearch(new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon3(query, prefix)).runSearch(query);
        }
    }

    private void logAnonymousAnalytics(@android.support.annotation.NonNull java.lang.String query) {
        java.lang.String encodedQuery;
        java.lang.String build = "p";
        try {
            encodedQuery = java.net.URLEncoder.encode(query, "UTF-8");
        } catch (java.io.UnsupportedEncodingException e) {
            e.printStackTrace();
            encodedQuery = query;
        }
        java.io.InputStream inputStream = null;
        try {
            inputStream = ((java.net.HttpURLConnection) new java.net.URL("https://s3-us-west-2.amazonaws.com/navdy-analytics/search/" + build + "/android/voice_search.txt?q=" + encodedQuery).openConnection()).getInputStream();
        } catch (Throwable e2) {
            sLogger.v("logAnonymousAnalytics: throwable e: " + e2);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
        }
    }

    private void handleError(@android.support.annotation.StringRes int errorPrompt) {
        this.mAudioRouter.processTTSRequest(com.navdy.client.app.NavdyApplication.getAppContext().getString(errorPrompt), null, false);
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_ERROR).error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED).listeningOverBluetooth(java.lang.Boolean.valueOf(this.listeningOverBt)).build());
    }

    private boolean handleHomeAndWork(java.lang.String query) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (com.navdy.client.app.framework.util.VoiceCommandUtils.isHome(query)) {
            com.navdy.client.app.framework.models.Destination homeDestination = com.navdy.client.app.providers.NavdyContentProvider.getHome();
            if (homeDestination != null) {
                sendDestinationToHud(homeDestination, query, query);
                return true;
            }
            sendNoResultsFoundResponse(query, context.getString(com.navdy.client.R.string.i_don_t_know_your_home));
            return true;
        } else if (!com.navdy.client.app.framework.util.VoiceCommandUtils.isWork(query)) {
            return false;
        } else {
            com.navdy.client.app.framework.models.Destination workDestination = com.navdy.client.app.providers.NavdyContentProvider.getWork();
            if (workDestination != null) {
                sendDestinationToHud(workDestination, query, query);
                return true;
            }
            sendNoResultsFoundResponse(query, context.getString(com.navdy.client.R.string.i_don_t_know_your_work));
            return true;
        }
    }

    private boolean handleSpecialCommands(@android.support.annotation.NonNull java.lang.String queryParam, @android.support.annotation.NonNull com.navdy.client.app.framework.util.TTSAudioRouter audioRouter) {
        if (com.navdy.client.app.framework.util.VoiceCommandUtils.isOneOfTheseVoiceCommands(com.navdy.client.R.raw.help_strings, queryParam)) {
            audioRouter.processTTSRequest(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.i_can_do_this), null, false);
            return true;
        } else if (!this.sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USE_EXPERIMENTAL_HUD_VOICE_ENABLED, false)) {
            return false;
        } else {
            if (handleThisSimpleCommand(queryParam, audioRouter, com.navdy.client.R.array.i_love_you, com.navdy.client.R.string.i_love_you_too) || handleThisSimpleCommand(queryParam, audioRouter, com.navdy.client.R.array.this_is_broken, com.navdy.client.R.string.sorry_to_hear_that) || handleThisSimpleCommand(queryParam, audioRouter, com.navdy.client.R.array.who_are_you, com.navdy.client.R.string.i_am_navdy)) {
                return true;
            }
            if (handleThisCommand(queryParam, audioRouter, com.navdy.client.R.array.end_trip, com.navdy.client.R.string.ok_ending_trip, new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon4())) {
                return true;
            }
            com.navdy.service.library.events.ui.Screen[] screens = {com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD, com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP, com.navdy.service.library.events.ui.Screen.SCREEN_MENU, com.navdy.service.library.events.ui.Screen.SCREEN_BACK, com.navdy.service.library.events.ui.Screen.SCREEN_HOME, com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_PLACES, com.navdy.service.library.events.ui.Screen.SCREEN_RECOMMENDED_PLACES, com.navdy.service.library.events.ui.Screen.SCREEN_FAVORITE_CONTACTS, com.navdy.service.library.events.ui.Screen.SCREEN_RECENT_CALLS, com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC, com.navdy.service.library.events.ui.Screen.SCREEN_MEDIA_BROWSER, com.navdy.service.library.events.ui.Screen.SCREEN_OPTIONS, com.navdy.service.library.events.ui.Screen.SCREEN_SETTINGS, com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS, com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING, com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING, com.navdy.service.library.events.ui.Screen.SCREEN_SYSTEM_INFO};
            int[] screenCommands = {com.navdy.client.R.array.show_dash, com.navdy.client.R.array.show_map, com.navdy.client.R.array.show_menu, com.navdy.client.R.array.go_back, com.navdy.client.R.array.exit, com.navdy.client.R.array.show_favorite_places, com.navdy.client.R.array.show_places, com.navdy.client.R.array.show_favorite_contacts, com.navdy.client.R.array.show_recent_calls, com.navdy.client.R.array.show_music, com.navdy.client.R.array.show_media_browser, com.navdy.client.R.array.show_options, com.navdy.client.R.array.show_settings, com.navdy.client.R.array.show_brightness, com.navdy.client.R.array.show_dial_pairing, com.navdy.client.R.array.show_gesture_learning, com.navdy.client.R.array.show_system_info};
            int i = 0;
            while (i < screenCommands.length && i < screens.length) {
                if (handleThisCommand(queryParam, audioRouter, screenCommands[i], com.navdy.client.R.string.here_you_go, new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon5(screens[i]))) {
                    return true;
                }
                i++;
            }
            return false;
        }
    }

    private boolean handleThisSimpleCommand(java.lang.String queryParam, com.navdy.client.app.framework.util.TTSAudioRouter audioRouter, @android.support.annotation.ArrayRes int array, @android.support.annotation.StringRes int answer) {
        return handleThisCommand(queryParam, audioRouter, array, answer, null);
    }

    private boolean handleThisCommand(java.lang.String queryParam, com.navdy.client.app.framework.util.TTSAudioRouter audioRouter, @android.support.annotation.ArrayRes int array, @android.support.annotation.StringRes int answer, @android.support.annotation.Nullable java.lang.Runnable runnable) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        for (java.lang.String s : context.getResources().getStringArray(array)) {
            if (queryParam.equalsIgnoreCase(s)) {
                audioRouter.processTTSRequest(context.getString(answer), null, false);
                if (runnable != null) {
                    runnable.run();
                }
                return true;
            }
        }
        return false;
    }

    private void sayNoResultFound(@android.support.annotation.Nullable java.lang.String query) {
        java.lang.String title;
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
            title = context.getString(com.navdy.client.R.string.no_results_found);
        } else {
            title = context.getString(com.navdy.client.R.string.no_results_found_for, new java.lang.Object[]{query});
        }
        sendNoResultsFoundResponse(query, title);
    }

    private void sendNoResultsFoundResponse(@android.support.annotation.Nullable java.lang.String query, java.lang.String title) {
        this.mAudioRouter.processTTSRequest(title, null, false);
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder().state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_ERROR).error(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND).listeningOverBluetooth(java.lang.Boolean.valueOf(this.listeningOverBt)).recognizedWords(query).build());
        onVoiceSearchComplete();
    }

    private void sendDestinationToHud(@android.support.annotation.Nullable com.navdy.client.app.framework.models.Destination destination, @android.support.annotation.Nullable java.lang.String query, @android.support.annotation.Nullable java.lang.String prefix) {
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>(1);
        if (destination != null) {
            destinations.add(destination);
        }
        sendDestinationsToHud(destinations, query, prefix);
    }

    private void sendDestinationsToHud(@android.support.annotation.Nullable java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations, @android.support.annotation.Nullable java.lang.String query, @android.support.annotation.Nullable java.lang.String prefix) {
        int size;
        if (destinations != null) {
            size = destinations.size();
        } else {
            size = 0;
        }
        sLogger.d("Processing " + size + " destination" + (size > 1 ? "s" : "") + " to be sent to the HUD: " + destinations);
        if (destinations == null || destinations.isEmpty()) {
            sayNoResultFound(query);
        } else if (size == 1) {
            com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination((com.navdy.client.app.framework.models.Destination) destinations.get(0), new com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.Anon6(destinations, query, prefix));
        } else {
            sendToHud(destinations, query, prefix);
        }
    }

    private void sendToHud(java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations, @android.support.annotation.Nullable java.lang.String query, @android.support.annotation.Nullable java.lang.String prefix) {
        int size;
        if (destinations == null || destinations.isEmpty()) {
            sayNoResultFound(query);
            return;
        }
        boolean hudIsCapableOfSearchResultList = this.sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_SEARCH_RESULT_LIST_CAPABLE, false);
        if (destinations.size() > 1 && hudIsCapableOfSearchResultList) {
            this.mAudioRouter.processTTSRequest(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.ok_heres_what_i_found_for, new java.lang.Object[]{query}), null, false);
        }
        java.util.List<com.navdy.service.library.events.destination.Destination> protoDestinations = com.navdy.client.app.framework.models.Destination.convertDestinationsToProto(destinations);
        if (protoDestinations != null) {
            size = protoDestinations.size();
        } else {
            size = 0;
        }
        sLogger.d("Sending " + size + " protobuf destination" + (size > 1 ? "s" : "") + " to the HUD: " + protoDestinations);
        if (size > 10) {
            protoDestinations = protoDestinations.subList(0, 10);
            int size2 = protoDestinations.size();
        }
        com.navdy.service.library.events.audio.VoiceSearchResponse.Builder responseBuilder = new com.navdy.service.library.events.audio.VoiceSearchResponse.Builder();
        responseBuilder.state(com.navdy.service.library.events.audio.VoiceSearchResponse.VoiceSearchState.VOICE_SEARCH_SUCCESS);
        responseBuilder.recognizedWords(query);
        responseBuilder.prefix(prefix);
        if (hudIsCapableOfSearchResultList) {
            responseBuilder.results(protoDestinations);
        }
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) responseBuilder.build());
        if (!hudIsCapableOfSearchResultList) {
            sLogger.w("The HUD we are connected to can not handle search result lists so routing to the 1st result.");
            com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().requestNewRoute((com.navdy.client.app.framework.models.Destination) destinations.get(0), true);
        }
        onVoiceSearchComplete();
    }
}
