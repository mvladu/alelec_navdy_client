package com.navdy.client.app.framework.callcontrol;

public class TelephonySupport21 implements com.navdy.client.app.framework.callcontrol.TelephonyInterface {
    static java.util.HashSet<java.lang.String> TELECOM_PACKAGES = new java.util.HashSet<>();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport21.class);
    protected android.content.Context context;

    enum Action {
        ACCEPT_CALL,
        REJECT_CALL,
        END_CALL
    }

    static {
        TELECOM_PACKAGES.add("com.android.server.telecom");
        TELECOM_PACKAGES.add("com.android.phone");
    }

    public TelephonySupport21(android.content.Context context2) {
        this.context = context2;
    }

    public void acceptRingingCall() {
        handleCallAction(com.navdy.client.app.framework.callcontrol.TelephonySupport21.Action.ACCEPT_CALL);
    }

    public void rejectRingingCall() {
        handleCallAction(com.navdy.client.app.framework.callcontrol.TelephonySupport21.Action.REJECT_CALL);
    }

    public void endCall() {
        handleCallAction(com.navdy.client.app.framework.callcontrol.TelephonySupport21.Action.END_CALL);
    }

    public void toggleMute() {
        sLogger.w("not implemented");
    }

    @android.annotation.TargetApi(21)
    protected void handleCallAction(com.navdy.client.app.framework.callcontrol.TelephonySupport21.Action action) {
        try {
            android.media.session.MediaSessionManager mediaSessionManager = (android.media.session.MediaSessionManager) this.context.getSystemService("media_session");
            if (mediaSessionManager == null) {
                sLogger.e("Unable to handleCallAction. mediaSessionManager is null");
                return;
            }
            java.util.List<android.media.session.MediaController> mediaControllerList = mediaSessionManager.getActiveSessions(new android.content.ComponentName(this.context, com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.class));
            sLogger.v("mediaSessions[" + mediaControllerList.size() + "] action[" + action + "]");
            for (android.media.session.MediaController m : mediaControllerList) {
                java.lang.String packageName = m.getPackageName();
                sLogger.v("package is " + packageName);
                if (TELECOM_PACKAGES.contains(packageName)) {
                    switch (action) {
                        case ACCEPT_CALL:
                            accept(m);
                            break;
                        case END_CALL:
                            end(m);
                            break;
                        case REJECT_CALL:
                            reject(m);
                            break;
                    }
                }
            }
        } catch (Throwable t) {
            sLogger.e("Unable to handleCallAction", t);
        }
    }

    @android.annotation.TargetApi(21)
    protected void accept(android.media.session.MediaController m) {
        m.dispatchMediaButtonEvent(android.view.KeyEvent.changeFlags(new android.view.KeyEvent(1, 79), 0));
        sLogger.i("HEADSETHOOK keyup sent:" + m.getPackageName());
    }

    @android.annotation.TargetApi(21)
    protected void reject(android.media.session.MediaController m) {
        m.dispatchMediaButtonEvent(android.view.KeyEvent.changeFlags(new android.view.KeyEvent(1, 79), 128));
        sLogger.i("HEADSETHOOK keyup sent:" + m.getPackageName());
    }

    protected void end(android.media.session.MediaController m) {
        reject(m);
    }
}
