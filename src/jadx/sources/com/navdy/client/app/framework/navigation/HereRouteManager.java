package com.navdy.client.app.framework.navigation;

public class HereRouteManager {
    private static final boolean VERBOSE = false;
    private static final java.lang.Object initListenersLock = new java.lang.Object();
    private static final com.navdy.client.app.framework.navigation.HereRouteManager instance = new com.navdy.client.app.framework.navigation.HereRouteManager();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.navigation.HereRouteManager.class);
    private final java.util.Queue<com.navdy.client.app.framework.navigation.HereRouteManager.OnHereRouteManagerInitialized> initListeners = new java.util.LinkedList();
    private final com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    private volatile com.navdy.client.app.framework.navigation.HereRouteManager.State state = com.navdy.client.app.framework.navigation.HereRouteManager.State.INITIALIZING;

    class Anon1 implements com.navdy.client.app.framework.navigation.HereRouteManager.OnHereRouteManagerInitialized {
        final /* synthetic */ double val$latitude;
        final /* synthetic */ com.navdy.client.app.framework.navigation.HereRouteManager.Listener val$listener;
        final /* synthetic */ double val$longitude;

        Anon1(double d, double d2, com.navdy.client.app.framework.navigation.HereRouteManager.Listener listener) {
            this.val$latitude = d;
            this.val$longitude = d2;
            this.val$listener = listener;
        }

        public void onInit() {
            com.navdy.service.library.events.location.Coordinate location = com.navdy.client.app.framework.navigation.HereRouteManager.this.navdyLocationManager.getSmartStartCoordinates();
            if (location != null) {
                com.navdy.client.app.framework.navigation.HereRouteManager.this.calculateRouteWithCoords(new com.here.android.mpa.common.GeoCoordinate(location.latitude.doubleValue(), location.longitude.doubleValue()), new com.here.android.mpa.common.GeoCoordinate(this.val$latitude, this.val$longitude), this.val$listener);
            }
        }

        public void onFailed() {
            this.val$listener.onRouteCalculated(com.navdy.client.app.framework.navigation.HereRouteManager.Error.HERE_INTERNAL_ERROR, null);
        }
    }

    class Anon2 implements com.here.android.mpa.routing.CoreRouter.Listener {
        final /* synthetic */ com.navdy.client.app.framework.navigation.HereRouteManager.Listener val$listener;

        Anon2(com.navdy.client.app.framework.navigation.HereRouteManager.Listener listener) {
            this.val$listener = listener;
        }

        public void onCalculateRouteFinished(java.util.List<com.here.android.mpa.routing.RouteResult> list, com.here.android.mpa.routing.RoutingError routingError) {
            if (routingError != com.here.android.mpa.routing.RoutingError.NONE || list == null || list.isEmpty() || list.get(0) == null || ((com.here.android.mpa.routing.RouteResult) list.get(0)).getRoute() == null) {
                if (routingError == com.here.android.mpa.routing.RoutingError.NO_END_POINT) {
                    com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.NO_END_POINT);
                }
                com.navdy.client.app.framework.navigation.HereRouteManager.logger.e("HereRouteManager Error: Here Internal Error " + routingError);
                this.val$listener.onRouteCalculated(com.navdy.client.app.framework.navigation.HereRouteManager.Error.HERE_INTERNAL_ERROR, null);
                return;
            }
            this.val$listener.onRouteCalculated(com.navdy.client.app.framework.navigation.HereRouteManager.Error.NONE, ((com.here.android.mpa.routing.RouteResult) list.get(0)).getRoute());
        }

        public void onProgress(int i) {
        }
    }

    class Anon3 implements com.here.android.mpa.common.OnEngineInitListener {
        Anon3() {
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            synchronized (com.navdy.client.app.framework.navigation.HereRouteManager.initListenersLock) {
                if (error != com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                    com.navdy.client.app.framework.navigation.HereRouteManager.logger.e("HereRouteManager failed to initialize with error: " + error.name());
                    com.navdy.client.app.framework.navigation.HereRouteManager.this.state = com.navdy.client.app.framework.navigation.HereRouteManager.State.FAILED;
                    while (!com.navdy.client.app.framework.navigation.HereRouteManager.this.initListeners.isEmpty()) {
                        ((com.navdy.client.app.framework.navigation.HereRouteManager.OnHereRouteManagerInitialized) com.navdy.client.app.framework.navigation.HereRouteManager.this.initListeners.poll()).onFailed();
                    }
                } else {
                    com.navdy.client.app.framework.navigation.HereRouteManager.logger.i("HereRouteManager successfully initialized, running initListeners");
                    com.navdy.client.app.framework.navigation.HereRouteManager.this.state = com.navdy.client.app.framework.navigation.HereRouteManager.State.INITIALIZED;
                    while (!com.navdy.client.app.framework.navigation.HereRouteManager.this.initListeners.isEmpty()) {
                        ((com.navdy.client.app.framework.navigation.HereRouteManager.OnHereRouteManagerInitialized) com.navdy.client.app.framework.navigation.HereRouteManager.this.initListeners.poll()).onInit();
                    }
                }
            }
        }
    }

    public enum Error {
        NONE,
        NO_ROUTES,
        NO_LOCATION,
        HERE_INTERNAL_ERROR
    }

    public interface Listener {
        void onPreCalculation(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle routeHandle);

        void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Error error, @android.support.annotation.Nullable com.here.android.mpa.routing.Route route);
    }

    private interface OnHereRouteManagerInitialized {
        void onFailed();

        void onInit();
    }

    public static class RouteHandle {
        private final com.here.android.mpa.routing.CoreRouter router;

        /* synthetic */ RouteHandle(com.here.android.mpa.routing.CoreRouter x0, com.navdy.client.app.framework.navigation.HereRouteManager.Anon1 x1) {
            this(x0);
        }

        private RouteHandle(com.here.android.mpa.routing.CoreRouter router2) {
            this.router = router2;
        }

        public void cancel() {
            if (this.router.isBusy()) {
                this.router.cancel();
            }
        }
    }

    private enum State {
        INITIALIZING,
        INITIALIZED,
        FAILED
    }

    public static com.navdy.client.app.framework.navigation.HereRouteManager getInstance() {
        return instance;
    }

    private HereRouteManager() {
        initialize();
    }

    public void calculateRoute(double latitude, double longitude, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Listener listener) {
        whenInitialized(new com.navdy.client.app.framework.navigation.HereRouteManager.Anon1(latitude, longitude, listener));
    }

    private void calculateRouteWithCoords(com.here.android.mpa.common.GeoCoordinate navdyLocation, com.here.android.mpa.common.GeoCoordinate destination, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.HereRouteManager.Listener listener) {
        com.here.android.mpa.routing.RoutePlan routePlan = new com.here.android.mpa.routing.RoutePlan();
        routePlan.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint(navdyLocation));
        routePlan.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint(destination));
        com.here.android.mpa.routing.RouteOptions routeOptions = new com.here.android.mpa.routing.RouteOptions();
        routeOptions.setRouteCount(1);
        routeOptions.setTransportMode(com.here.android.mpa.routing.RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
        routePlan.setRouteOptions(routeOptions);
        com.here.android.mpa.routing.CoreRouter router = new com.here.android.mpa.routing.CoreRouter();
        listener.onPreCalculation(new com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle(router, null));
        router.calculateRoute(routePlan, new com.navdy.client.app.framework.navigation.HereRouteManager.Anon2(listener));
    }

    private void initialize() {
        com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.app.framework.navigation.HereRouteManager.Anon3());
    }

    private void whenInitialized(com.navdy.client.app.framework.navigation.HereRouteManager.OnHereRouteManagerInitialized callback) {
        if (this.state == com.navdy.client.app.framework.navigation.HereRouteManager.State.INITIALIZING) {
            synchronized (initListenersLock) {
                this.initListeners.add(callback);
            }
        } else if (this.state == com.navdy.client.app.framework.navigation.HereRouteManager.State.INITIALIZED) {
            callback.onInit();
        }
    }
}
