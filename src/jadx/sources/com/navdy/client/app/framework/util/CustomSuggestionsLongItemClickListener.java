package com.navdy.client.app.framework.util;

public interface CustomSuggestionsLongItemClickListener {
    boolean onItemLongClick(android.view.View view, int i, com.navdy.client.app.framework.models.Suggestion suggestion);
}
