package com.navdy.client.app.framework.glances;

public class SocialNotificationHandler {
    private static com.navdy.service.library.log.Logger sLogger = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger;

    public static void handleSocialNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        if (com.navdy.client.app.ui.glances.GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            char c = 65535;
            switch (packageName.hashCode()) {
                case 10619783:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.TWITTER)) {
                        c = 1;
                        break;
                    }
                    break;
                case 714499313:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.FACEBOOK)) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    handleFacebookNotification(sbn);
                    return;
                case 1:
                    handleTwitterNotification(sbn);
                    return;
                default:
                    sLogger.w("calendar notification not handled [" + packageName + "]");
                    return;
            }
        }
    }

    private static void handleFacebookNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        java.lang.String event = android.support.v7.app.NotificationCompat.getExtras(notification).getString("android.text");
        sLogger.v("[navdyinfo-facebook] event[" + event + "]");
        java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), event));
        com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_SOCIAL).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
    }

    private static void handleTwitterNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        android.os.Bundle extras = android.support.v7.app.NotificationCompat.getExtras(notification);
        java.lang.String sender = extras.getString("android.title");
        java.lang.String message = extras.getString("android.text");
        java.lang.String to = null;
        if (message != null && message.startsWith(com.navdy.client.app.framework.glances.GlanceConstants.EMAIL_AT)) {
            int index = message.indexOf(" ");
            if (index != -1) {
                to = message.substring(0, index);
                message = message.substring(index + 1);
            }
        }
        sLogger.v("[navdyinfo-twitter] sender[" + sender + "] to[" + to + "] message[" + message + "]");
        java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_FROM.name(), sender));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_TO.name(), to));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name(), message));
        com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_SOCIAL).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
    }
}
