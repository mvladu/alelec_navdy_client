package com.navdy.client.app.framework.util;

public class ContactObserver extends android.database.ContentObserver {
    private static final long CHECK_GAINED_CONTACTS_PERMISSION_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(1);
    private static final long REFRESH_DELAY = java.util.concurrent.TimeUnit.SECONDS.toMillis(15);
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.ContactObserver.class);
    private com.navdy.client.app.framework.servicehandler.ContactServiceHandler contactServiceHandler;
    private android.os.Handler handler;
    private java.lang.Runnable refreshContacts = new com.navdy.client.app.framework.util.ContactObserver.Anon1();

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.util.ContactObserver.logger.v("sending fresh contact info to Display");
            com.navdy.client.app.framework.util.ContactObserver.this.contactServiceHandler.onFavoriteContactsRequest(new com.navdy.service.library.events.contacts.FavoriteContactsRequest.Builder().maxContacts(java.lang.Integer.valueOf(30)).build());
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ android.os.Handler val$handler;

        Anon2(android.os.Handler handler) {
            this.val$handler = handler;
        }

        public void run() {
            if (com.navdy.client.app.ui.homescreen.HomescreenActivity.weHaveContactsPermission()) {
                com.navdy.client.app.framework.util.ContactObserver.logger.i("has gained contacts permission");
                this.val$handler.post(com.navdy.client.app.framework.util.ContactObserver.this.refreshContacts);
                return;
            }
            this.val$handler.postDelayed(this, com.navdy.client.app.framework.util.ContactObserver.CHECK_GAINED_CONTACTS_PERMISSION_INTERVAL);
        }
    }

    public ContactObserver(com.navdy.client.app.framework.servicehandler.ContactServiceHandler contactServiceHandler2, android.os.Handler handler2) {
        super(handler2);
        this.handler = handler2;
        this.contactServiceHandler = contactServiceHandler2;
        boolean hasContactPermission = com.navdy.client.app.ui.homescreen.HomescreenActivity.weHaveContactsPermission();
        logger.v("init hasContactPermission=" + hasContactPermission);
        if (!hasContactPermission) {
            handler2.post(new com.navdy.client.app.framework.util.ContactObserver.Anon2(handler2));
        }
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        logger.v("onChange to Contacts");
        refreshContactsWithDebounce();
    }

    private void refreshContactsWithDebounce() {
        this.handler.removeCallbacks(this.refreshContacts);
        this.handler.postDelayed(this.refreshContacts, REFRESH_DELAY);
    }
}
