package com.navdy.client.app.framework.servicehandler;

public class SettingsServiceHandler {
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.class);

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest val$driverProfilePreferencesRequest;

        Anon1(com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest driverProfilePreferencesRequest) {
            this.val$driverProfilePreferencesRequest = driverProfilePreferencesRequest;
        }

        public void run() {
            try {
                long serial = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences().getLong(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM, com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
                if (this.val$driverProfilePreferencesRequest.serial_number.longValue() == serial) {
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.v("Driver profile version up to date");
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.sendRemoteMessage(new com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(serial), null));
                    return;
                }
                com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            } catch (Throwable t) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.e(t);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, null, java.lang.Long.valueOf(0), null));
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.InputPreferencesRequest val$inputPreferencesRequest;

        Anon2(com.navdy.service.library.events.preferences.InputPreferencesRequest inputPreferencesRequest) {
            this.val$inputPreferencesRequest = inputPreferencesRequest;
        }

        public void run() {
            try {
                long serial = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getLong(com.navdy.client.app.ui.settings.SettingsConstants.HUD_SERIAL_NUM, 0);
                if (this.val$inputPreferencesRequest.serial_number.longValue() == serial) {
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.v("Input prefs version up to date");
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.sendRemoteMessage(new com.navdy.service.library.events.preferences.InputPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(serial), null));
                    return;
                }
                com.navdy.client.app.ui.settings.SettingsUtils.sendDialSettingsToTheHudBasedOnSharedPrefValue();
            } catch (Throwable t) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.e(t);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.preferences.InputPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, null, java.lang.Long.valueOf(0), null));
            }
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest val$displaySpeakerPreferencesRequest;

        Anon3(com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest displaySpeakerPreferencesRequest) {
            this.val$displaySpeakerPreferencesRequest = displaySpeakerPreferencesRequest;
        }

        public void run() {
            try {
                long serial = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getLong(com.navdy.client.app.ui.settings.SettingsConstants.DISPLAY_AUDIO_SERIAL_NUM, 0);
                if (this.val$displaySpeakerPreferencesRequest.serial_number.longValue() == serial) {
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.v("Speaker prefs version up to date");
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.sendRemoteMessage(new com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(serial), null));
                    return;
                }
                com.navdy.client.app.ui.settings.SettingsUtils.sendSpeakerSettingsToTheHudBasedOnSharedPrefValue();
            } catch (Throwable t) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.e(t);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, null, java.lang.Long.valueOf(0), null));
            }
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.NavigationPreferencesRequest val$navigationPreferencesRequest;

        Anon4(com.navdy.service.library.events.preferences.NavigationPreferencesRequest navigationPreferencesRequest) {
            this.val$navigationPreferencesRequest = navigationPreferencesRequest;
        }

        public void run() {
            try {
                long serial = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getLong("nav_serial_number", 0);
                if (this.val$navigationPreferencesRequest.serial_number.longValue() == serial) {
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.v("Nav prefs version up to date");
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.sendRemoteMessage(new com.navdy.service.library.events.preferences.NavigationPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(serial), null));
                    return;
                }
                com.navdy.client.app.ui.settings.SettingsUtils.sendNavSettingsToTheHudBasedOnSharedPrefValue();
            } catch (Throwable t) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.e(t);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.preferences.NavigationPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, null, java.lang.Long.valueOf(0), null));
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.AudioPreferencesRequest val$audioPreferencesRequest;

        Anon5(com.navdy.service.library.events.preferences.AudioPreferencesRequest audioPreferencesRequest) {
            this.val$audioPreferencesRequest = audioPreferencesRequest;
        }

        public void run() {
            long serial = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getLong(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SERIAL_NUM, 0);
            if (this.val$audioPreferencesRequest.serial_number.longValue() == serial) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.v("Audio prefs version up to date");
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.sendRemoteMessage(new com.navdy.service.library.events.preferences.AudioPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(serial), null));
                return;
            }
            com.navdy.client.app.ui.settings.SettingsUtils.sendAudioPreferencesToDisplay(false);
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.glances.CannedMessagesRequest val$cannedMessagesRequest;

        Anon6(com.navdy.service.library.events.glances.CannedMessagesRequest cannedMessagesRequest) {
            this.val$cannedMessagesRequest = cannedMessagesRequest;
        }

        public void run() {
            try {
                android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
                if (!sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_CANNED_RESPONSE_CAPABLE, false)) {
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.w("HUD requested canned messages but is not capable of it so ignoring request.");
                    return;
                }
                long serial = sharedPrefs.getLong("nav_serial_number", 0);
                if (this.val$cannedMessagesRequest.serial_number.longValue() == serial) {
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.v("CannedMessage prefs version up to date");
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.sendRemoteMessage(new com.navdy.service.library.events.glances.CannedMessagesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(serial), null));
                    return;
                }
                com.navdy.client.app.ui.settings.SettingsUtils.sendMessagingSettingsToTheHudBasedOnSharedPrefValue();
            } catch (Throwable t) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.e(t);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.glances.CannedMessagesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, null, java.lang.Long.valueOf(0), null));
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.NavigationPreferencesUpdate val$navigationPreferencesUpdate;

        Anon7(com.navdy.service.library.events.preferences.NavigationPreferencesUpdate navigationPreferencesUpdate) {
            this.val$navigationPreferencesUpdate = navigationPreferencesUpdate;
        }

        public void run() {
            if (this.val$navigationPreferencesUpdate == null || this.val$navigationPreferencesUpdate.preferences == null) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.e("Received a NavigationPreferencesUpdate with null preferences!");
                return;
            }
            android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
            if (sharedPrefs != null) {
                long navSerialNumber = sharedPrefs.getLong("nav_serial_number", 0) + 1;
                android.content.SharedPreferences.Editor editor = sharedPrefs.edit();
                com.navdy.service.library.events.preferences.NavigationPreferences p = this.val$navigationPreferencesUpdate.preferences;
                boolean shortestRouteIsOn = p.routingType == com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType.ROUTING_SHORTEST;
                boolean autoRecalcIsOn = p.rerouteForTraffic == com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic.REROUTE_AUTOMATIC;
                editor.putLong("nav_serial_number", navSerialNumber);
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_ROUTE_CALCULATION, java.lang.Boolean.valueOf(shortestRouteIsOn));
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_RECALC, java.lang.Boolean.valueOf(autoRecalcIsOn));
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_HIGHWAYS, p.allowHighways);
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TOLL_ROADS, p.allowTollRoads);
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_FERRIES, p.allowFerries);
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_TUNNELS, p.allowTunnels);
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_UNPAVED_ROADS, p.allowUnpavedRoads);
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.NAVIGATION_AUTO_TRAINS, p.allowAutoTrains);
                editor.apply();
                if (p.spokenTurnByTurn != null || p.spokenSpeedLimitWarnings != null) {
                    editor.putLong(com.navdy.client.app.ui.settings.SettingsConstants.DISPLAY_AUDIO_SERIAL_NUM, sharedPrefs.getLong(com.navdy.client.app.ui.settings.SettingsConstants.DISPLAY_AUDIO_SERIAL_NUM, 0) + 1);
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, p.spokenTurnByTurn);
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_SPEED_WARNINGS, p.spokenSpeedLimitWarnings);
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.putOnlyIfNotNull(editor, com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_CAMERA_WARNINGS, p.spokenCameraWarnings);
                    editor.apply();
                }
            }
        }
    }

    class Anon8 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.preferences.NotificationPreferencesRequest val$notificationPreferencesRequest;

        Anon8(com.navdy.service.library.events.preferences.NotificationPreferencesRequest notificationPreferencesRequest) {
            this.val$notificationPreferencesRequest = notificationPreferencesRequest;
        }

        public void run() {
            try {
                long serial = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getLong(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES_SERIAL_NUM, 0);
                if (this.val$notificationPreferencesRequest.serial_number.longValue() == serial) {
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.v("Glances prefs version up to date");
                    com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.this.sendRemoteMessage(new com.navdy.service.library.events.preferences.NotificationPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT, null, java.lang.Long.valueOf(serial), null));
                    return;
                }
                com.navdy.client.app.ui.glances.GlanceUtils.sendGlancesSettingsToTheHudBasedOnSharedPrefValue();
            } catch (Throwable t) {
                com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.sLogger.e(t);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.preferences.NotificationPreferencesUpdate(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR, null, java.lang.Long.valueOf(0), null));
            }
        }
    }

    public SettingsServiceHandler() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onDriverProfileRequest(com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest driverProfilePreferencesRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon1(driverProfilePreferencesRequest), 1);
    }

    @com.squareup.otto.Subscribe
    public void onInputRequest(com.navdy.service.library.events.preferences.InputPreferencesRequest inputPreferencesRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon2(inputPreferencesRequest), 1);
    }

    @com.squareup.otto.Subscribe
    public void onSpeakerRequest(com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest displaySpeakerPreferencesRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon3(displaySpeakerPreferencesRequest), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationRequest(com.navdy.service.library.events.preferences.NavigationPreferencesRequest navigationPreferencesRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon4(navigationPreferencesRequest), 1);
    }

    @com.squareup.otto.Subscribe
    public void onAudioPreferencesRequest(com.navdy.service.library.events.preferences.AudioPreferencesRequest audioPreferencesRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon5(audioPreferencesRequest), 1);
    }

    @com.squareup.otto.Subscribe
    public void onCannedMessagesRequest(com.navdy.service.library.events.glances.CannedMessagesRequest cannedMessagesRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon6(cannedMessagesRequest), 1);
    }

    @com.squareup.otto.Subscribe
    public void onNavigationPreferencesUpdate(com.navdy.service.library.events.preferences.NavigationPreferencesUpdate navigationPreferencesUpdate) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon7(navigationPreferencesUpdate), 1);
    }

    private void putOnlyIfNotNull(android.content.SharedPreferences.Editor editor, java.lang.String key, java.lang.Boolean value) {
        if (value != null) {
            editor.putBoolean(key, value.booleanValue());
        }
    }

    @com.squareup.otto.Subscribe
    public void onNotificationRequest(com.navdy.service.library.events.preferences.NotificationPreferencesRequest notificationPreferencesRequest) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler.Anon8(notificationPreferencesRequest), 1);
    }

    @com.squareup.otto.Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange connectionStateChange) {
        if (com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            com.navdy.client.app.ui.settings.SettingsUtils.updateAllSettingsIfNecessary();
        }
    }

    private void sendRemoteMessage(com.squareup.wire.Message message) {
        com.navdy.client.app.framework.DeviceConnection.postEvent(message);
    }
}
