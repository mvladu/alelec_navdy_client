package com.navdy.client.app.framework.util;

public final class CarMdClient$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.framework.util.CarMdClient> implements dagger.MembersInjector<com.navdy.client.app.framework.util.CarMdClient> {
    private dagger.internal.Binding<com.navdy.service.library.network.http.IHttpManager> mHttpManager;

    public CarMdClient$$InjectAdapter() {
        super(null, "members/com.navdy.client.app.framework.util.CarMdClient", false, com.navdy.client.app.framework.util.CarMdClient.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.client.app.framework.util.CarMdClient.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
    }

    public void injectMembers(com.navdy.client.app.framework.util.CarMdClient object) {
        object.mHttpManager = (com.navdy.service.library.network.http.IHttpManager) this.mHttpManager.get();
    }
}
