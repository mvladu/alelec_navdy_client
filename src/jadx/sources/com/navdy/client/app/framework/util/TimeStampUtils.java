package com.navdy.client.app.framework.util;

public class TimeStampUtils {
    private static final double MILE_PER_METER = 6.21371E-4d;

    public static java.lang.String getDurationStringInHoursAndMinutes(java.lang.Integer eta) {
        java.lang.String estimatedETA;
        java.lang.String estimatedETA2 = "";
        if (eta == null || eta.intValue() < 0) {
            return estimatedETA2;
        }
        long minutes = java.util.concurrent.TimeUnit.SECONDS.toMinutes((long) eta.intValue());
        long hours = java.util.concurrent.TimeUnit.MINUTES.toHours(minutes);
        long days = java.util.concurrent.TimeUnit.HOURS.toDays(hours);
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        if (days > 0) {
            long hours2 = hours - java.util.concurrent.TimeUnit.DAYS.toHours(days);
            if (hours2 > 0) {
                estimatedETA = context.getString(com.navdy.client.R.string.days_and_hours_format, new java.lang.Object[]{java.lang.Long.valueOf(days), java.lang.Long.valueOf(hours2)});
            } else {
                estimatedETA = context.getString(com.navdy.client.R.string.days_format, new java.lang.Object[]{java.lang.Long.valueOf(days)});
            }
        } else if (hours > 0) {
            long minutes2 = minutes - java.util.concurrent.TimeUnit.HOURS.toMinutes(hours);
            if (minutes2 > 0) {
                estimatedETA = context.getString(com.navdy.client.R.string.hours_and_minutes_format, new java.lang.Object[]{java.lang.Long.valueOf(hours), java.lang.Long.valueOf(minutes2)});
            } else {
                estimatedETA = context.getString(com.navdy.client.R.string.hours_format, new java.lang.Object[]{java.lang.Long.valueOf(hours)});
            }
        } else {
            estimatedETA = context.getString(com.navdy.client.R.string.minutes_format, new java.lang.Object[]{java.lang.Long.valueOf(minutes)});
        }
        return estimatedETA;
    }

    public static java.lang.String getDistanceStringInMiles(java.lang.Integer distance) {
        java.lang.String estimatedDistance = "";
        if (distance == null || distance.intValue() < 0) {
            return estimatedDistance;
        }
        java.lang.Double milesDouble = java.lang.Double.valueOf(((double) distance.intValue()) * 6.21371E-4d);
        return new java.text.DecimalFormat("#.#").format(milesDouble) + " mi";
    }

    public static java.lang.String getArrivalTimeString(java.lang.Integer eta) {
        if (eta == null || eta.intValue() < 0) {
            return "";
        }
        return new java.text.SimpleDateFormat(android.text.format.DateFormat.is24HourFormat(com.navdy.client.app.NavdyApplication.getAppContext()) ? "HH:mm" : "h:mm aa", java.util.Locale.getDefault()).format(new java.util.Date(java.lang.Long.valueOf(new java.util.Date().getTime() + java.util.concurrent.TimeUnit.SECONDS.toMillis((long) eta.intValue())).longValue()));
    }
}
