package com.navdy.client.app.framework.models;

public class Destination implements android.os.Parcelable, java.io.Serializable {
    public static final android.os.Parcelable.Creator<com.navdy.client.app.framework.models.Destination> CREATOR = new com.navdy.client.app.framework.models.Destination.Anon1();
    private static final int DESTINATION_NOT_FOUND = -1;
    private static final float DISTANCE_LIMIT = 5.0f;
    public static final java.lang.String IDENTIFIER_CONTACT_ID_SEPARATOR = "|";
    public static final java.lang.String IDENTIFIER_NAMESPACE_CONTACT = "contact:";
    public static final java.lang.String IDENTIFIER_NAMESPACE_DB_ID = "db-id:";
    public static final java.lang.String IDENTIFIER_NAMESPACE_PLACE_ID = "google-places-id:";
    private static final long PLACE_ID_REFRESH_LIMIT = 2592000000L;
    private static final boolean VERBOSE = false;
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.Destination.class);
    java.lang.String city;
    public java.lang.String contactLookupKey;
    public java.lang.String country;
    private java.lang.String countryCode;
    public double displayLat;
    public double displayLong;
    public boolean doNotSuggest;
    private java.lang.String favoriteLabel;
    private int favoriteOrder;
    public int favoriteType;
    private boolean hasBeenProcessed;
    public int id;
    private boolean isCalendarEvent;
    public long lastContactLookup;
    public long lastKnownContactId;
    long lastPlaceIdRefresh;
    public long lastRoutedDate;
    public java.lang.String name;
    public double navigationLat;
    public double navigationLong;
    public java.lang.String placeDetailJson;
    public java.lang.String placeId;
    public com.navdy.client.app.framework.models.Destination.Precision precisionLevel;
    public java.lang.String rawAddressNotForDisplay;
    public java.util.ArrayList<java.lang.String> rawAddressVariations;
    public int searchResultType;
    public java.lang.String state;
    public java.lang.String streetName;
    public java.lang.String streetNumber;
    public com.navdy.client.app.framework.models.Destination.Type type;
    java.lang.String zipCode;

    static class Anon1 implements android.os.Parcelable.Creator<com.navdy.client.app.framework.models.Destination> {
        Anon1() {
        }

        public com.navdy.client.app.framework.models.Destination createFromParcel(android.os.Parcel in) {
            return new com.navdy.client.app.framework.models.Destination(in);
        }

        public com.navdy.client.app.framework.models.Destination[] newArray(int size) {
            return new com.navdy.client.app.framework.models.Destination[size];
        }
    }

    class Anon10 implements java.lang.Runnable {
        Anon10() {
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination.this.updateLastRoutedDateInDb();
        }
    }

    class Anon11 extends android.os.AsyncTask<java.lang.Object, java.lang.Object, java.lang.Integer> {
        final /* synthetic */ com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback val$callback;
        final /* synthetic */ int val$specialType;

        Anon11(int i, com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback queryResultCallback) {
            this.val$specialType = i;
            this.val$callback = queryResultCallback;
        }

        /* access modifiers changed from: protected */
        public java.lang.Integer doInBackground(java.lang.Object... params) {
            return java.lang.Integer.valueOf(com.navdy.client.app.framework.models.Destination.this.saveDestinationAsFavorite(this.val$specialType));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Integer nbRows) {
            if (this.val$callback != null) {
                this.val$callback.onQueryCompleted(nbRows.intValue(), null);
            }
        }
    }

    class Anon12 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback val$callback;

        Anon12(com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback queryResultCallback) {
            this.val$callback = queryResultCallback;
        }

        public void run() {
            int nbRows = com.navdy.client.app.framework.models.Destination.this.deleteFavoriteFromDb();
            if (this.val$callback != null) {
                this.val$callback.onQueryCompleted(nbRows, null);
            }
        }
    }

    class Anon2 implements com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener {
        final /* synthetic */ java.lang.Runnable val$callback;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ java.util.List val$destinations;
            final /* synthetic */ com.navdy.client.app.framework.search.GooglePlacesSearch.Error val$error;

            Anon1(com.navdy.client.app.framework.search.GooglePlacesSearch.Error error, java.util.List list) {
                this.val$error = error;
                this.val$destinations = list;
            }

            public void run() {
                if (this.val$error != com.navdy.client.app.framework.search.GooglePlacesSearch.Error.NONE) {
                    com.navdy.client.app.framework.models.Destination.logger.e("received error while refreshing google place details: " + this.val$error.name());
                    if (com.navdy.client.app.framework.models.Destination.Anon2.this.val$callback != null) {
                        com.navdy.client.app.framework.models.Destination.Anon2.this.val$callback.run();
                    }
                } else if (this.val$destinations != null && !this.val$destinations.isEmpty()) {
                    ((com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) this.val$destinations.get(0)).toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.DETAILS, com.navdy.client.app.framework.models.Destination.this);
                    if (com.navdy.client.app.framework.models.Destination.Anon2.this.val$callback != null) {
                        com.navdy.client.app.framework.models.Destination.Anon2.this.val$callback.run();
                    }
                    com.navdy.client.app.framework.models.Destination.this.persistPlaceDetailInfoAndUpdateLists();
                }
            }
        }

        Anon2(java.lang.Runnable runnable) {
            this.val$callback = runnable;
        }

        public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon2.Anon1(error, destinations), 1, com.navdy.service.library.task.TaskManager.TaskPriority.LOW);
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination.this.persistPlaceDetailInfoAndUpdateLists();
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination.this.persistPlaceDetailInfoAndUpdateLists();
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$onFinish;

        Anon5(java.lang.Runnable runnable) {
            this.val$onFinish = runnable;
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination.this.reloadSelfFromDatabase();
            this.val$onFinish.run();
        }
    }

    class Anon6 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback val$callback;

        Anon6(com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback queryResultCallback) {
            this.val$callback = queryResultCallback;
        }

        public void run() {
            android.net.Uri uri = com.navdy.client.app.framework.models.Destination.this.updateAllColumnsOrInsertNewEntryInDb();
            if (this.val$callback != null) {
                this.val$callback.onQueryCompleted(uri != null ? 1 : 0, uri);
            }
        }
    }

    class Anon7 implements java.lang.Runnable {
        Anon7() {
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination.this.updateDoNotSuggest();
        }
    }

    class Anon8 implements java.lang.Runnable {
        Anon8() {
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination.this.saveToDb();
        }
    }

    class Anon9 implements java.lang.Runnable {
        Anon9() {
        }

        public void run() {
            com.navdy.client.app.framework.models.Destination.this.deleteFromDb();
        }
    }

    public static class MultilineAddress {
        public java.lang.String addressFirstLine = "";
        public java.lang.String addressSecondLine = "";
        public java.lang.String title = "";

        MultilineAddress(com.navdy.client.app.framework.models.Destination destination) {
            java.lang.String destinationTitle = destination.getTitle();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationTitle)) {
                android.util.Pair<java.lang.String, java.lang.String> splitAddress = destination.getSplitAddress();
                if (!com.navdy.client.app.framework.i18n.AddressUtils.isTitleIsInTheAddress(destinationTitle, (java.lang.String) splitAddress.first)) {
                    this.title = destinationTitle;
                    this.addressFirstLine = com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress((java.lang.String) splitAddress.first);
                    this.addressSecondLine = com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress((java.lang.String) splitAddress.second);
                    return;
                }
            }
            java.util.ArrayList<java.lang.String> lines = destination.getAddressLines();
            if (lines.size() > 0) {
                this.title = (java.lang.String) lines.get(0);
            }
            if (lines.size() > 1) {
                this.addressFirstLine = (java.lang.String) lines.get(1);
            }
            this.addressSecondLine = com.navdy.client.app.framework.i18n.AddressUtils.joinRemainingLines(lines, 2);
        }
    }

    public enum Precision {
        UNKNOWN(0),
        IMPRECISE(1),
        PRECISE(2);
        
        int level;

        private Precision(int level2) {
            this.level = level2;
        }

        public int getValue() {
            return this.level;
        }

        public static com.navdy.client.app.framework.models.Destination.Precision get(int level2) {
            return values()[level2];
        }

        public static com.navdy.client.app.framework.models.Destination.Precision get(boolean isImprecise) {
            return isImprecise ? IMPRECISE : PRECISE;
        }

        public boolean isPrecise() {
            return this != IMPRECISE;
        }
    }

    public enum SearchType {
        SEARCH_POWERED_BY_GOOGLE_HEADER,
        SERVICES_SEARCH,
        SEARCH_HISTORY,
        AUTOCOMPLETE,
        CONTACT,
        CONTACT_ADDRESS_SELECTION,
        TEXT_SEARCH,
        RECENT_PLACES,
        DETAILS,
        MORE,
        NO_RESULTS,
        UNKNOWN,
        FOOTER;

        public int getValue() {
            return ordinal();
        }
    }

    public enum Type {
        UNKNOWN,
        CONTACT,
        CALENDAR_EVENT,
        PLACE,
        GAS_STATION,
        AIRPORT,
        PARKING,
        TRANSIT,
        ATM,
        BANK,
        SCHOOL,
        STORE,
        COFFEE,
        RESTAURANT,
        GYM,
        PARK,
        HOSPITAL,
        ENTERTAINMENT,
        BAR;

        public int getValue() {
            return ordinal();
        }

        public static com.navdy.client.app.framework.models.Destination.Type get(int value) {
            return values()[value];
        }

        public com.navdy.service.library.events.places.PlaceType toProtobufPlaceType() {
            switch (this) {
                case CONTACT:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_CONTACT;
                case CALENDAR_EVENT:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_CALENDAR_EVENT;
                case GAS_STATION:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS;
                case AIRPORT:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_AIRPORT;
                case PARKING:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARKING;
                case TRANSIT:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_TRANSIT;
                case ATM:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ATM;
                case BANK:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_BANK;
                case SCHOOL:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_SCHOOL;
                case STORE:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_STORE;
                case COFFEE:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_COFFEE;
                case RESTAURANT:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_RESTAURANT;
                case GYM:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GYM;
                case PARK:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARK;
                case HOSPITAL:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_HOSPITAL;
                case ENTERTAINMENT:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ENTERTAINMENT;
                case BAR:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_BAR;
                default:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
            }
        }
    }

    public Destination() {
        this.name = "";
        this.rawAddressNotForDisplay = "";
        this.streetNumber = "";
        this.streetName = "";
        this.city = "";
        this.state = "";
        this.zipCode = "";
        this.country = "";
        this.countryCode = com.navdy.client.app.framework.i18n.AddressUtils.DEFAULT_COUNTRY_CODE;
        this.placeDetailJson = "";
        this.isCalendarEvent = false;
        this.precisionLevel = com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN;
        this.type = com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
        this.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.UNKNOWN.getValue();
        this.hasBeenProcessed = false;
        this.rawAddressVariations = new java.util.ArrayList<>();
    }

    protected Destination(android.os.Parcel in) {
        boolean z = true;
        this.name = "";
        this.rawAddressNotForDisplay = "";
        this.streetNumber = "";
        this.streetName = "";
        this.city = "";
        this.state = "";
        this.zipCode = "";
        this.country = "";
        this.countryCode = com.navdy.client.app.framework.i18n.AddressUtils.DEFAULT_COUNTRY_CODE;
        this.placeDetailJson = "";
        this.isCalendarEvent = false;
        this.precisionLevel = com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN;
        this.type = com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
        this.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.UNKNOWN.getValue();
        this.hasBeenProcessed = false;
        this.rawAddressVariations = new java.util.ArrayList<>();
        this.id = in.readInt();
        this.placeId = in.readString();
        this.lastPlaceIdRefresh = in.readLong();
        this.displayLat = in.readDouble();
        this.displayLong = in.readDouble();
        this.navigationLat = in.readDouble();
        this.navigationLong = in.readDouble();
        this.name = in.readString();
        this.rawAddressNotForDisplay = in.readString();
        this.streetNumber = in.readString();
        this.streetName = in.readString();
        this.city = in.readString();
        this.state = in.readString();
        this.zipCode = in.readString();
        this.country = in.readString();
        this.countryCode = in.readString();
        this.doNotSuggest = in.readInt() != 0;
        this.lastRoutedDate = in.readLong();
        this.favoriteLabel = in.readString();
        this.favoriteOrder = in.readInt();
        this.favoriteType = in.readInt();
        this.searchResultType = in.readInt();
        this.placeDetailJson = in.readString();
        this.precisionLevel = com.navdy.client.app.framework.models.Destination.Precision.get(in.readInt());
        this.type = com.navdy.client.app.framework.models.Destination.Type.get(in.readInt());
        if (in.readInt() == 0) {
            z = false;
        }
        this.hasBeenProcessed = z;
        this.contactLookupKey = in.readString();
        this.lastKnownContactId = in.readLong();
        this.lastContactLookup = in.readLong();
        in.readStringList(this.rawAddressVariations);
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        int i = 1;
        dest.writeInt(this.id);
        dest.writeString(this.placeId);
        dest.writeLong(this.lastPlaceIdRefresh);
        dest.writeDouble(this.displayLat);
        dest.writeDouble(this.displayLong);
        dest.writeDouble(this.navigationLat);
        dest.writeDouble(this.navigationLong);
        dest.writeString(this.name);
        dest.writeString(this.rawAddressNotForDisplay);
        dest.writeString(this.streetNumber);
        dest.writeString(this.streetName);
        dest.writeString(this.city);
        dest.writeString(this.state);
        dest.writeString(this.zipCode);
        dest.writeString(this.country);
        dest.writeString(this.countryCode);
        dest.writeInt(this.doNotSuggest ? 1 : 0);
        dest.writeLong(this.lastRoutedDate);
        dest.writeString(this.favoriteLabel);
        dest.writeInt(this.favoriteOrder);
        dest.writeInt(this.favoriteType);
        dest.writeInt(this.searchResultType);
        dest.writeString(this.placeDetailJson);
        dest.writeInt(this.precisionLevel.getValue());
        dest.writeInt(this.type.getValue());
        if (!this.hasBeenProcessed) {
            i = 0;
        }
        dest.writeInt(i);
        dest.writeString(this.contactLookupKey);
        dest.writeLong(this.lastKnownContactId);
        dest.writeLong(this.lastContactLookup);
        dest.writeStringList(this.rawAddressVariations);
    }

    public Destination(java.lang.String name2, java.lang.String rawAddressNotForDisplay2) {
        this(0, null, 0, 0.0d, 0.0d, 0.0d, 0.0d, name2, rawAddressNotForDisplay2, "", "", "", "", "", "", "", false, 0, null, 0, 0, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.models.Destination.Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id2, java.lang.String placeId2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2) {
        this(id2, placeId2, 0, displayLat2, displayLong2, navigationLat2, navigationLong2, name2, rawAddressNotForDisplay2, "", "", "", "", "", "", "", false, 0, null, 0, 0, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.models.Destination.Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id2, java.lang.String placeId2, long lastPlaceIdRefresh2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2, java.lang.String streetNumber2, java.lang.String streetName2, java.lang.String city2, java.lang.String state2, java.lang.String zipCode2, java.lang.String country2, java.lang.String countryCode2) {
        this(id2, placeId2, lastPlaceIdRefresh2, displayLat2, displayLong2, navigationLat2, navigationLong2, name2, rawAddressNotForDisplay2, streetNumber2, streetName2, city2, state2, zipCode2, country2, countryCode2, false, 0, null, 0, 0, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.models.Destination.Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id2, java.lang.String placeId2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2, long lastRoutedDate2) {
        this(id2, placeId2, 0, displayLat2, displayLong2, navigationLat2, navigationLong2, name2, rawAddressNotForDisplay2, "", "", "", "", "", "", "", false, lastRoutedDate2, null, 0, 0, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.models.Destination.Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id2, java.lang.String placeId2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2, long lastRoutedDate2, com.navdy.client.app.framework.models.Destination.Type type2) {
        this(id2, placeId2, 0, displayLat2, displayLong2, navigationLat2, navigationLong2, name2, rawAddressNotForDisplay2, "", "", "", "", "", "", "", false, lastRoutedDate2, null, 0, 0, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, type2, null, -1, -1);
    }

    public Destination(int id2, java.lang.String placeId2, long lastPlaceIdRefresh2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2, java.lang.String streetNumber2, java.lang.String streetName2, java.lang.String city2, java.lang.String state2, java.lang.String zipCode2, java.lang.String country2, java.lang.String countryCode2, long lastRoutedDate2) {
        this(id2, placeId2, lastPlaceIdRefresh2, displayLat2, displayLong2, navigationLat2, navigationLong2, name2, rawAddressNotForDisplay2, streetNumber2, streetName2, city2, state2, zipCode2, country2, countryCode2, false, lastRoutedDate2, null, 0, 0, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.models.Destination.Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id2, java.lang.String placeId2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2, java.lang.String favoriteLabel2, int favoriteOrder2, int favoriteType2) {
        this(id2, placeId2, 0, displayLat2, displayLong2, navigationLat2, navigationLong2, name2, rawAddressNotForDisplay2, "", "", "", "", "", "", "", false, 0, favoriteLabel2, favoriteOrder2, favoriteType2, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.models.Destination.Type.UNKNOWN, null, -1, -1);
    }

    public Destination(int id2, java.lang.String placeId2, long lastPlaceIdRefresh2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2, java.lang.String streetNumber2, java.lang.String streetName2, java.lang.String city2, java.lang.String state2, java.lang.String zipCode2, java.lang.String country2, java.lang.String countryCode2, java.lang.String favoriteLabel2, int favoriteOrder2, int favoriteType2) {
        this(id2, placeId2, lastPlaceIdRefresh2, displayLat2, displayLong2, navigationLat2, navigationLong2, name2, rawAddressNotForDisplay2, streetNumber2, streetName2, city2, state2, zipCode2, country2, countryCode2, false, 0, favoriteLabel2, favoriteOrder2, favoriteType2, "", com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN, com.navdy.client.app.framework.models.Destination.Type.UNKNOWN, null, -1, -1);
    }

    public Destination(@android.support.annotation.NonNull com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult) {
        this(0, null, getLatitude(navigationRouteResult), getLongitude(navigationRouteResult), getLatitude(navigationRouteResult), getLongitude(navigationRouteResult), navigationRouteResult.label, navigationRouteResult.address);
    }

    public Destination(@android.support.annotation.NonNull com.navdy.service.library.events.destination.Destination destination) {
        this(0, destination.place_id, destination.display_position.latitude.doubleValue(), destination.display_position.longitude.doubleValue(), destination.navigation_position.latitude.doubleValue(), destination.navigation_position.longitude.doubleValue(), destination.destination_title, destination.full_address, 0, fromProtobufPlaceType(destination.place_type));
        if (fromProtobufPlaceType(destination.place_type) == com.navdy.client.app.framework.models.Destination.Type.CALENDAR_EVENT) {
            this.isCalendarEvent = true;
        }
    }

    public Destination(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        this();
        mergeWith(destination);
    }

    public Destination(int id2, java.lang.String placeIdentifier, long lastPlaceIdRefresh2, double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, java.lang.String name2, java.lang.String rawAddressNotForDisplay2, java.lang.String streetNumber2, java.lang.String streetName2, java.lang.String city2, java.lang.String state2, java.lang.String zipCode2, java.lang.String country2, java.lang.String countryCode2, boolean doNotSuggest2, long lastRoutedDate2, java.lang.String favoriteLabel2, int favoriteOrder2, int favoriteType2, java.lang.String placeDetailJson2, com.navdy.client.app.framework.models.Destination.Precision precisionLevel2, com.navdy.client.app.framework.models.Destination.Type type2, java.lang.String contactLookupKey2, long lastKnownContatId, long lastContactLookup2) {
        this.name = "";
        this.rawAddressNotForDisplay = "";
        this.streetNumber = "";
        this.streetName = "";
        this.city = "";
        this.state = "";
        this.zipCode = "";
        this.country = "";
        this.countryCode = com.navdy.client.app.framework.i18n.AddressUtils.DEFAULT_COUNTRY_CODE;
        this.placeDetailJson = "";
        this.isCalendarEvent = false;
        this.precisionLevel = com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN;
        this.type = com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
        this.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.UNKNOWN.getValue();
        this.hasBeenProcessed = false;
        this.rawAddressVariations = new java.util.ArrayList<>();
        this.id = id2;
        this.lastPlaceIdRefresh = lastPlaceIdRefresh2;
        this.displayLat = displayLat2;
        this.displayLong = displayLong2;
        this.navigationLat = navigationLat2;
        this.navigationLong = navigationLong2;
        this.name = name2;
        this.rawAddressNotForDisplay = rawAddressNotForDisplay2;
        this.streetNumber = streetNumber2;
        this.streetName = streetName2;
        this.city = city2;
        this.state = state2;
        this.zipCode = zipCode2;
        this.country = country2;
        this.countryCode = countryCode2;
        this.doNotSuggest = doNotSuggest2;
        this.lastRoutedDate = lastRoutedDate2;
        this.favoriteLabel = favoriteLabel2;
        this.favoriteOrder = favoriteOrder2;
        this.favoriteType = favoriteType2;
        this.placeDetailJson = placeDetailJson2;
        this.precisionLevel = precisionLevel2;
        this.type = type2;
        this.contactLookupKey = contactLookupKey2;
        this.lastKnownContactId = lastKnownContatId;
        this.lastContactLookup = lastContactLookup2;
        setIdentifier(placeIdentifier);
    }

    public java.lang.String toString() {
        return java.lang.String.format(java.util.Locale.US, "Destination{id=%d,\tplaceId=%s,\tlastPlaceIdRefresh=%s,\tname=%s,\tType=%s,\tprecisionLevel=%s\t,address=%s,\tdisplayLat=%s,\tdisplayLong=%s,\tnavigationLat=%s,\tnavigationLong=%s,\tstreetNumber=%s,\tstreetName=%s,\tcity=%s,\tstate=%s,\tzipCode=%s,\tcountry=%s (%s),\tdoNotSuggest=%s,\tlastRoutedDate=%s,\tfavoriteLabel=%s,\tfavoriteOrder=%d,\tfavoriteType=%d,\tisCalendarEvent=%s,\tsearchResultType=%d,\tcontactLookupKey=%s,\tlastKnownContactId=%d,\tlastContactLookup=%d}", new java.lang.Object[]{java.lang.Integer.valueOf(this.id), this.placeId, com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(this.lastPlaceIdRefresh), this.name, this.type.name(), this.precisionLevel.name(), this.rawAddressNotForDisplay, java.lang.Double.valueOf(this.displayLat), java.lang.Double.valueOf(this.displayLong), java.lang.Double.valueOf(this.navigationLat), java.lang.Double.valueOf(this.navigationLong), this.streetNumber, this.streetName, this.city, this.state, this.zipCode, this.country, this.countryCode, java.lang.Boolean.valueOf(this.doNotSuggest), com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(this.lastRoutedDate), this.favoriteLabel, java.lang.Integer.valueOf(this.favoriteOrder), java.lang.Integer.valueOf(this.favoriteType), java.lang.Boolean.valueOf(this.isCalendarEvent), java.lang.Integer.valueOf(this.searchResultType), this.contactLookupKey, java.lang.Long.valueOf(this.lastKnownContactId), java.lang.Long.valueOf(this.lastContactLookup)});
    }

    public java.lang.String toShortString() {
        return java.lang.String.format(java.util.Locale.US, "Destination{id=%d,\tplaceId=%s,\tlastPlaceIdRefresh=%s,\tname=%s,\tType=%s,\tprecisionLevel=%s\t,address=%s,\tdisplayLat=%s,\tdisplayLong=%s,\tnavigationLat=%s,\tnavigationLong=%s,\thasDetailedAddress=%s,\tdoNotSuggest=%s,\tlastRoutedDate=%s,\tfavoriteLabel=%s,\tfavoriteType=%d,\tisCalendarEvent=%s,\tsearchResultType=%d}", new java.lang.Object[]{java.lang.Integer.valueOf(this.id), this.placeId, com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(this.lastPlaceIdRefresh), this.name, this.type.name(), this.precisionLevel.name(), this.rawAddressNotForDisplay, java.lang.Double.valueOf(this.displayLat), java.lang.Double.valueOf(this.displayLong), java.lang.Double.valueOf(this.navigationLat), java.lang.Double.valueOf(this.navigationLong), java.lang.Boolean.valueOf(hasDetailedAddress()), java.lang.Boolean.valueOf(this.doNotSuggest), com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(this.lastRoutedDate), this.favoriteLabel, java.lang.Integer.valueOf(this.favoriteType), java.lang.Boolean.valueOf(this.isCalendarEvent), java.lang.Integer.valueOf(this.searchResultType)});
    }

    public java.lang.String toJsonString() {
        return "{\"id\":\"" + this.id + "\"," + " \"placeId\":\"" + this.placeId + "\"," + " \"name\":\"" + this.name + "\"," + " \"Type\":\"" + this.type.name() + "\"," + " \"precisionLevel\":\"" + this.precisionLevel.name() + "\"," + " \"address\":\"" + this.rawAddressNotForDisplay + "\"," + " \"displayLat\":\"" + this.displayLat + "\"," + " \"displayLong\":\"" + this.displayLong + "\"," + " \"navigationLat\":\"" + this.navigationLat + "\"," + " \"navigationLong\":\"" + this.navigationLong + "\"," + " \"streetNumber\":\"" + this.streetNumber + "\"," + " \"streetName\":\"" + this.streetName + "\"," + " \"city\":\"" + this.city + "\"," + " \"state\":\"" + this.state + "\"," + " \"zipCode\":\"" + this.zipCode + "\"," + " \"country\":\"" + this.country + "\" " + "(" + this.countryCode + ")," + " \"doNotSuggest\":\"" + this.doNotSuggest + "\"," + " \"lastRoutedDate\":\"" + com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(this.lastRoutedDate) + "\"," + " \"favoriteLabel\":\"" + this.favoriteLabel + "\"," + " \"favoriteOrder\":\"" + this.favoriteOrder + "\"," + " \"favoriteType\":\"" + this.favoriteType + "\"," + " \"searchResultType\":\"" + this.searchResultType + "\"," + " \"lastPlaceIdRefresh\":\"" + com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(this.lastPlaceIdRefresh) + "\"," + " \"placeDetailJson\":" + this.placeDetailJson + "\"," + " \"contactLookupKey\":\"" + this.contactLookupKey + "\"," + " \"lastKnownContactId\":\"" + this.lastKnownContactId + "\"," + " \"lastContactLookup\":\"" + this.lastContactLookup + "\"," + "}";
    }

    private static double getLatitude(@android.support.annotation.NonNull com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult) {
        java.util.List<java.lang.Float> routeLatLongs = navigationRouteResult.routeLatLongs;
        if (routeLatLongs == null || routeLatLongs.size() <= 1) {
            return 0.0d;
        }
        return (double) ((java.lang.Float) routeLatLongs.get(routeLatLongs.size() - 2)).floatValue();
    }

    private static double getLongitude(@android.support.annotation.NonNull com.navdy.service.library.events.navigation.NavigationRouteResult navigationRouteResult) {
        java.util.List<java.lang.Float> routeLatLongs = navigationRouteResult.routeLatLongs;
        if (routeLatLongs == null || routeLatLongs.size() <= 1) {
            return 0.0d;
        }
        return (double) ((java.lang.Float) routeLatLongs.get(routeLatLongs.size() - 1)).floatValue();
    }

    public int describeContents() {
        return 0;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public java.lang.String getPlaceId() {
        return this.placeId;
    }

    public void setPlaceId(java.lang.String placeId2) {
        this.placeId = placeId2;
    }

    public double getDisplayLat() {
        return this.displayLat;
    }

    public void setDisplayLat(double displayLat2) {
        this.displayLat = displayLat2;
    }

    public double getDisplayLng() {
        return this.displayLong;
    }

    public void setDisplayLong(double displayLong2) {
        this.displayLong = displayLong2;
    }

    public com.navdy.service.library.events.location.Coordinate getDisplayCoordinate() {
        return new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(this.displayLat)).longitude(java.lang.Double.valueOf(this.displayLong)).build();
    }

    public double getNavigationLat() {
        return this.navigationLat;
    }

    public void setNavigationLat(double navigationLat2) {
        this.navigationLat = navigationLat2;
    }

    public double getNavigationLong() {
        return this.navigationLong;
    }

    public void setNavigationLong(double navigationLong2) {
        this.navigationLong = navigationLong2;
    }

    public com.navdy.service.library.events.location.Coordinate getNavigationCoordinate() {
        return new com.navdy.service.library.events.location.Coordinate.Builder().latitude(java.lang.Double.valueOf(this.navigationLat)).longitude(java.lang.Double.valueOf(this.navigationLong)).build();
    }

    public java.lang.String getName() {
        return this.name;
    }

    public void setName(java.lang.String name2) {
        this.name = name2;
    }

    private java.lang.String getRawAddressNotForDisplay() {
        return this.rawAddressNotForDisplay;
    }

    public void setRawAddressNotForDisplay(java.lang.String address) {
        this.rawAddressNotForDisplay = com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress(address);
    }

    public java.lang.String getStreetNumber() {
        return this.streetNumber;
    }

    public void setStreetNumber(java.lang.String streetNumber2) {
        this.streetNumber = streetNumber2;
    }

    public java.lang.String getStreetName() {
        return this.streetName;
    }

    public void setStreetName(java.lang.String streetName2) {
        this.streetName = streetName2;
    }

    public java.lang.String getCity() {
        return this.city;
    }

    public void setCity(java.lang.String city2) {
        this.city = city2;
    }

    public java.lang.String getState() {
        return this.state;
    }

    public void setState(java.lang.String state2) {
        this.state = state2;
    }

    public java.lang.String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(java.lang.String zipCode2) {
        this.zipCode = zipCode2;
    }

    public java.lang.String getCountry() {
        return this.country;
    }

    public void setCountry(java.lang.String country2) {
        this.country = country2;
    }

    public java.lang.String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(java.lang.String countryCode2) {
        if (countryCode2 != null && countryCode2.length() > 2) {
            java.lang.String iso2 = com.navdy.client.app.framework.i18n.AddressUtils.convertIso3ToIso2(countryCode2);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(iso2)) {
                countryCode2 = iso2;
            }
        }
        this.countryCode = countryCode2;
    }

    public boolean isDoNotSuggest() {
        return this.doNotSuggest;
    }

    public void setDoNotSuggest(boolean doNotSuggest2) {
        this.doNotSuggest = doNotSuggest2;
    }

    public long getLastRoutedDate() {
        return this.lastRoutedDate;
    }

    public void setLastRoutedDate(long lastRoutedDate2) {
        this.lastRoutedDate = lastRoutedDate2;
    }

    public int getSearchResultType() {
        return this.searchResultType;
    }

    public void setSearchResultType(int searchResultType2) {
        this.searchResultType = searchResultType2;
    }

    public java.lang.String getFavoriteLabel() {
        if (isHome()) {
            return com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.home);
        }
        if (isWork()) {
            return com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.work);
        }
        return this.favoriteLabel;
    }

    public void setFavoriteLabel(java.lang.String favoriteLabel2) {
        this.favoriteLabel = favoriteLabel2;
    }

    public int getFavoriteOrder() {
        return this.favoriteOrder;
    }

    public void setFavoriteOrder(int favoriteOrder2) {
        this.favoriteOrder = favoriteOrder2;
    }

    public int getFavoriteType() {
        return this.favoriteType;
    }

    public void setFavoriteType(int favoriteType2) {
        this.favoriteType = favoriteType2;
    }

    public com.navdy.service.library.events.destination.Destination.FavoriteType getFavoriteTypeForProto() {
        switch (this.favoriteType) {
            case com.navdy.client.app.providers.NavdyContentProviderConstants.FAVORITE_CONTACT /*-4*/:
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CONTACT;
            case -3:
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
            case -2:
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK;
            default:
                if (isFavoriteDestination()) {
                    return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM;
                }
                return com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
        }
    }

    public com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum getImageEnum() {
        if (getFavoriteTypeForProto() == com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CONTACT) {
            return com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.CONTACT;
        }
        return com.navdy.client.app.ui.search.SearchRecyclerAdapter.ImageEnum.PLACE;
    }

    public boolean hasAlreadyBeenProcessed() {
        return this.hasBeenProcessed;
    }

    public static boolean equals(com.navdy.client.app.framework.models.Destination d1, com.navdy.client.app.framework.models.Destination d2) {
        return ((d1 == null && d2 == null) || !(d1 == null || d2 == null)) && (d1 == d2 || ((d1.id > 0 && d1.id == d2.id) || ((!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(d1.placeId) && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(d1.placeId, d2.placeId)) || ((com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(d1.placeId) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(d2.placeId)) && haveMatchingAddresses(d1, d2)))));
    }

    private static boolean haveMatchingAddresses(com.navdy.client.app.framework.models.Destination d1, com.navdy.client.app.framework.models.Destination d2) {
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(d1.rawAddressNotForDisplay, d2.rawAddressNotForDisplay)) {
            return true;
        }
        if (d1.rawAddressVariations != null) {
            java.util.Iterator it = d1.rawAddressVariations.iterator();
            while (it.hasNext()) {
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim((java.lang.String) it.next(), d2.rawAddressNotForDisplay)) {
                    return true;
                }
            }
        }
        if (d2.rawAddressVariations != null) {
            java.util.Iterator it2 = d2.rawAddressVariations.iterator();
            while (it2.hasNext()) {
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(d1.rawAddressNotForDisplay, (java.lang.String) it2.next())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean equals(java.lang.Object o) {
        if (o instanceof com.navdy.client.app.framework.models.Destination) {
            return equals(this, (com.navdy.client.app.framework.models.Destination) o);
        }
        return super.equals(o);
    }

    public int hashCode() {
        return this.id;
    }

    public boolean isPersisted() {
        return this.id > 0;
    }

    @android.support.annotation.NonNull
    public static java.util.List<com.navdy.client.app.framework.models.Destination> mergeAndDeduplicateLists(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.Destination> list1, @android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.Destination> list2) {
        if (list1 == null) {
            list1 = new java.util.ArrayList<>(0);
        }
        if (list2 == null) {
            list2 = new java.util.ArrayList<>(0);
        }
        int initialCapacity = list1.size() + list2.size();
        if (initialCapacity == 0) {
            return new java.util.ArrayList(0);
        }
        java.util.List<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>(initialCapacity);
        destinations.addAll(list1);
        for (com.navdy.client.app.framework.models.Destination destination2 : list2) {
            java.util.Iterator<com.navdy.client.app.framework.models.Destination> iterator = list1.iterator();
            boolean merged = false;
            while (true) {
                if (!iterator.hasNext()) {
                    break;
                }
                com.navdy.client.app.framework.models.Destination destination1 = new com.navdy.client.app.framework.models.Destination((com.navdy.client.app.framework.models.Destination) iterator.next());
                if (destination1.equals(destination2)) {
                    destination1.mergeWith(destination2);
                    merged = true;
                    break;
                }
            }
            if (!merged) {
                destinations.add(destination2);
            }
        }
        return destinations;
    }

    private void mergeWith(com.navdy.client.app.framework.models.Destination destination) {
        boolean z;
        boolean z2 = false;
        if (this.id <= 0 && destination.id > 0) {
            this.id = destination.id;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.placeId)) {
            this.placeId = destination.placeId;
        }
        if (destination.lastPlaceIdRefresh > this.lastPlaceIdRefresh) {
            this.lastPlaceIdRefresh = destination.lastPlaceIdRefresh;
        }
        if (!hasValidDisplayCoordinates() && destination.hasValidDisplayCoordinates()) {
            this.displayLat = destination.displayLat;
            this.displayLong = destination.displayLong;
        }
        if (!hasValidNavCoordinates() && destination.hasValidNavCoordinates()) {
            this.navigationLat = destination.navigationLat;
            this.navigationLong = destination.navigationLong;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.name) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.name)) {
            this.name = destination.name;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.rawAddressNotForDisplay)) {
            this.rawAddressNotForDisplay = destination.rawAddressNotForDisplay;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetNumber) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.streetNumber)) {
            this.streetNumber = destination.streetNumber;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetName) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.streetName)) {
            this.streetName = destination.streetName;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.city) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.city)) {
            this.city = destination.city;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.state) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.state)) {
            this.state = destination.state;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.zipCode) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.zipCode)) {
            this.zipCode = destination.zipCode;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.country) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.country)) {
            this.country = destination.country;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.countryCode) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.countryCode)) {
            this.countryCode = destination.countryCode;
        }
        if (!this.doNotSuggest) {
            this.doNotSuggest = destination.doNotSuggest;
        }
        if (destination.lastRoutedDate > this.lastRoutedDate) {
            this.lastRoutedDate = destination.lastRoutedDate;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.favoriteLabel) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.favoriteLabel)) {
            this.favoriteLabel = destination.favoriteLabel;
        }
        if (this.favoriteOrder <= 0 && destination.favoriteOrder > 0) {
            this.favoriteOrder = destination.favoriteOrder;
        }
        if (this.favoriteType == 0 && destination.favoriteType != 0) {
            this.favoriteType = destination.favoriteType;
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.placeDetailJson) && (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeDetailJson) || this.lastPlaceIdRefresh < destination.lastPlaceIdRefresh)) {
            this.placeDetailJson = destination.placeDetailJson;
        }
        if (this.precisionLevel == com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN && destination.precisionLevel != com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN) {
            this.precisionLevel = destination.precisionLevel;
        }
        if (this.type == com.navdy.client.app.framework.models.Destination.Type.UNKNOWN && destination.type != com.navdy.client.app.framework.models.Destination.Type.UNKNOWN) {
            this.type = destination.type;
        }
        if (this.hasBeenProcessed || destination.hasBeenProcessed) {
            z = true;
        } else {
            z = false;
        }
        this.hasBeenProcessed = z;
        if (this.isCalendarEvent || destination.isCalendarEvent) {
            z2 = true;
        }
        this.isCalendarEvent = z2;
        if ((destination.lastContactLookup > 0 && destination.lastContactLookup > this.lastContactLookup) || (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.contactLookupKey) && this.lastKnownContactId <= 0)) {
            this.contactLookupKey = destination.contactLookupKey;
            this.lastKnownContactId = destination.lastKnownContactId;
            this.lastContactLookup = destination.lastContactLookup;
        }
        if (destination.rawAddressVariations != null) {
            java.util.Iterator it = destination.rawAddressVariations.iterator();
            while (it.hasNext()) {
                java.lang.String rawAddressVariation = (java.lang.String) it.next();
                boolean found = false;
                if (this.rawAddressVariations != null) {
                    java.util.Iterator it2 = this.rawAddressVariations.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim((java.lang.String) it2.next(), rawAddressVariation)) {
                                found = true;
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                } else {
                    this.rawAddressVariations = new java.util.ArrayList<>();
                }
                if (!found) {
                    this.rawAddressVariations.add(rawAddressVariation);
                }
            }
        }
    }

    @android.support.annotation.NonNull
    public java.util.ArrayList<java.lang.String> getAddressLines() {
        java.util.ArrayList<java.lang.String> lines;
        java.util.ArrayList<java.lang.String> lines2 = new java.util.ArrayList<>();
        if (hasDetailedAddress()) {
            com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat addressFormat = com.navdy.client.app.framework.i18n.AddressUtils.getAddressFormatFor(this);
            if (addressFormat != null) {
                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.countryCode, com.navdy.client.app.framework.location.NavdyLocationManager.getLastKnownCountryCode())) {
                    lines = addressFormat.localLines;
                } else {
                    lines = addressFormat.foreignLines;
                }
                for (int i = 0; i < lines.size(); i++) {
                    lines.set(i, com.navdy.client.app.framework.i18n.AddressUtils.insertAddressPartsInFormat((java.lang.String) lines.get(i), this));
                }
                return lines;
            }
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            this.rawAddressNotForDisplay = com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress(this.rawAddressNotForDisplay);
            if (this.rawAddressNotForDisplay.contains(",")) {
                java.lang.String[] splitAddress = this.rawAddressNotForDisplay.split(", *");
                if (splitAddress.length > 0) {
                    return new java.util.ArrayList<>(java.util.Arrays.asList(splitAddress));
                }
            }
            int nbChars = com.navdy.client.app.NavdyApplication.getAppContext().getResources().getInteger(com.navdy.client.R.integer.nb_first_line_address_char);
            if (this.rawAddressNotForDisplay.length() < nbChars) {
                lines2.add(this.rawAddressNotForDisplay);
            } else {
                java.lang.String[] addressParts = this.rawAddressNotForDisplay.split(" ");
                java.lang.StringBuilder firstLineSb = new java.lang.StringBuilder(addressParts[0]);
                java.lang.StringBuilder secondLineSb = new java.lang.StringBuilder();
                int index = 1;
                while (firstLineSb.length() < nbChars && index < addressParts.length) {
                    firstLineSb.append(" ");
                    firstLineSb.append(addressParts[index]);
                    index++;
                }
                while (index < addressParts.length) {
                    secondLineSb.append(addressParts[index]);
                    index++;
                    if (index < addressParts.length) {
                        secondLineSb.append(" ");
                    }
                }
                lines2.add(firstLineSb.toString());
                if (secondLineSb.length() > 0) {
                    lines2.add(secondLineSb.toString());
                }
            }
        } else if (hasValidDisplayCoordinates()) {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            lines2.add(context.getString(com.navdy.client.R.string.latitude_is, new java.lang.Object[]{java.lang.Double.valueOf(this.displayLat)}));
            lines2.add(context.getString(com.navdy.client.R.string.longitude_is, new java.lang.Object[]{java.lang.Double.valueOf(this.displayLong)}));
        }
        return lines2;
    }

    public java.lang.String getAddressForDisplay() {
        java.util.ArrayList<java.lang.String> lines = getAddressLines();
        if (lines.size() > 0) {
            return com.navdy.client.app.framework.i18n.AddressUtils.joinRemainingLines(lines, 0);
        }
        return this.rawAddressNotForDisplay;
    }

    public android.util.Pair<java.lang.String, java.lang.String> getSplitAddress() {
        java.lang.String firstLine;
        java.lang.String secondLine;
        java.util.ArrayList<java.lang.String> addressLines = getAddressLines();
        if (addressLines.size() <= 0) {
            firstLine = this.rawAddressNotForDisplay;
            secondLine = "";
        } else {
            firstLine = (java.lang.String) addressLines.get(0);
            secondLine = com.navdy.client.app.framework.i18n.AddressUtils.joinRemainingLines(addressLines, 1);
        }
        return new android.util.Pair<>(firstLine.trim(), secondLine.trim());
    }

    public android.util.Pair<java.lang.String, java.lang.String> getTitleAndSubtitle() {
        java.lang.String destinationSubtitle;
        java.lang.String destinationTitle = getTitle();
        java.util.ArrayList<java.lang.String> lines = getAddressLines();
        if (lines.size() > 0 && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationTitle)) {
            destinationTitle = (java.lang.String) lines.get(0);
            destinationSubtitle = com.navdy.client.app.framework.i18n.AddressUtils.joinRemainingLines(lines, 1);
        } else if ((lines.size() > 0 && com.navdy.client.app.framework.i18n.AddressUtils.isTitleIsInTheAddress(destinationTitle, com.navdy.client.app.framework.i18n.AddressUtils.joinRemainingLines(lines, 0))) || com.navdy.client.app.framework.i18n.AddressUtils.isTitleIsInTheAddress(destinationTitle, this.rawAddressNotForDisplay)) {
            destinationSubtitle = com.navdy.client.app.framework.i18n.AddressUtils.joinRemainingLines(lines, 1);
        } else if (lines.size() > 0) {
            destinationSubtitle = com.navdy.client.app.framework.i18n.AddressUtils.joinRemainingLines(lines, 0);
        } else {
            destinationSubtitle = this.rawAddressNotForDisplay;
        }
        return new android.util.Pair<>(com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress(destinationTitle), com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress(destinationSubtitle));
    }

    @android.support.annotation.Nullable
    public java.lang.String getTitle() {
        java.lang.String destinationTitle = null;
        if (isFavoriteDestination()) {
            if (this.favoriteType == -3) {
                destinationTitle = com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.home);
            } else if (this.favoriteType == -2) {
                destinationTitle = com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.work);
            } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.favoriteLabel)) {
                destinationTitle = this.favoriteLabel;
            }
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationTitle) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.name)) {
            return destinationTitle;
        }
        return this.name;
    }

    @android.support.annotation.NonNull
    public com.navdy.client.app.framework.models.Destination.MultilineAddress getMultilineAddress() {
        return new com.navdy.client.app.framework.models.Destination.MultilineAddress(this);
    }

    public boolean hasDetailedAddress() {
        return !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetName) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.city) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.countryCode);
    }

    public boolean hasStreetNumber() {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetNumber)) {
            return true;
        }
        if (this.rawAddressNotForDisplay != null) {
            java.lang.String streetAddress = (java.lang.String) getSplitAddress().first;
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(streetAddress) && streetAddress.matches("^[0-9]+[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]")) {
                return true;
            }
        }
        return false;
    }

    private void setAddressFieldsFromHereAddress(com.here.android.mpa.search.Address hereAddress) {
        if (hereAddress == null) {
            logger.e("address is null");
            return;
        }
        this.country = hereAddress.getCountryName();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(hereAddress.getCountryCode())) {
            this.countryCode = com.navdy.client.app.framework.i18n.AddressUtils.convertIso3ToIso2(hereAddress.getCountryCode());
        }
        this.city = hereAddress.getCity();
        this.zipCode = hereAddress.getPostalCode();
        this.streetName = hereAddress.getStreet();
        this.state = hereAddress.getState();
        java.lang.String hereAddressText = com.navdy.client.app.framework.util.StringUtils.fromHtml(hereAddress.getText()).toString();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(hereAddressText)) {
            hereAddressText = hereAddressText.replaceAll("\n\n", " ").replaceAll("\n", " ");
        }
        this.rawAddressNotForDisplay = hereAddressText;
        this.streetNumber = hereAddress.getHouseNumber();
    }

    public com.navdy.service.library.events.destination.Destination toProtobufDestinationObject() {
        com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType2 = getFavoriteTypeForProto();
        android.util.Pair<java.lang.String, java.lang.String> titleAndSubtitle = com.navdy.client.app.framework.i18n.AddressUtils.cleanUpDebugPrefix(getTitleAndSubtitle());
        java.lang.String firstLine = (java.lang.String) titleAndSubtitle.first;
        java.lang.String secondLine = (java.lang.String) titleAndSubtitle.second;
        java.util.ArrayList<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers = getPhoneNumbers();
        return new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(this.navigationLat), java.lang.Double.valueOf(this.navigationLong))).display_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(this.displayLat), java.lang.Double.valueOf(this.displayLong))).full_address(this.rawAddressNotForDisplay).destination_title(firstLine).destination_subtitle(secondLine).place_type(this.type.toProtobufPlaceType()).favorite_type(favoriteType2).identifier(java.lang.Integer.toString(this.id)).place_id(getPlaceIndentifier()).last_navigated_to(java.lang.Long.valueOf(this.lastRoutedDate)).phoneNumbers(phoneNumbers).contacts(getContacts()).build();
    }

    public java.lang.String getPlaceIndentifier() {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId)) {
            return IDENTIFIER_NAMESPACE_PLACE_ID + this.placeId;
        }
        if (this.lastKnownContactId > 0 || !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            return IDENTIFIER_NAMESPACE_CONTACT + this.lastKnownContactId + IDENTIFIER_CONTACT_ID_SEPARATOR + this.contactLookupKey;
        }
        return IDENTIFIER_NAMESPACE_DB_ID + this.id;
    }

    public void setIdentifier(java.lang.String identifier) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(identifier)) {
            if (identifier.startsWith(IDENTIFIER_NAMESPACE_PLACE_ID)) {
                this.placeId = identifier.substring(IDENTIFIER_NAMESPACE_PLACE_ID.length());
            } else if (!identifier.startsWith(IDENTIFIER_NAMESPACE_CONTACT) || !identifier.contains(IDENTIFIER_CONTACT_ID_SEPARATOR)) {
                if (identifier.startsWith(IDENTIFIER_NAMESPACE_DB_ID)) {
                    identifier = identifier.substring(IDENTIFIER_NAMESPACE_DB_ID.length());
                }
                try {
                    this.id = java.lang.Integer.parseInt(identifier);
                } catch (java.lang.NumberFormatException e) {
                    this.placeId = identifier;
                }
            } else {
                java.lang.String tmp = identifier.substring(IDENTIFIER_NAMESPACE_CONTACT.length());
                int i = tmp.indexOf(IDENTIFIER_CONTACT_ID_SEPARATOR);
                if (i > 0 && i < tmp.length()) {
                    try {
                        this.lastKnownContactId = (long) java.lang.Integer.parseInt(tmp.substring(0, i));
                    } catch (java.lang.NumberFormatException e2) {
                    }
                }
                java.lang.String lookupString = tmp.substring(i + 1);
                if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(lookupString, com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID)) {
                    this.contactLookupKey = lookupString;
                }
                this.lastContactLookup = new java.util.Date().getTime();
            }
        }
    }

    private java.util.ArrayList<com.navdy.service.library.events.contacts.PhoneNumber> getPhoneNumbers() {
        java.util.ArrayList<com.navdy.service.library.events.contacts.PhoneNumber> phoneNumbers = new java.util.ArrayList<>();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeDetailJson) && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            logger.d("No phone number because no placeDetailJson: " + this.placeDetailJson + " and no contactLookupKey: " + this.contactLookupKey + " for destination: " + this);
        } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeDetailJson)) {
            try {
                org.json.JSONObject jsonObject = new org.json.JSONObject(this.placeDetailJson);
                java.lang.String number = jsonObject.optString("formatted_phone_number");
                logger.d("formatted_phone_number = " + number);
                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(number)) {
                    number = jsonObject.optString("formatted_phone_number");
                    logger.d("formatted_phone_number = " + number);
                }
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(number)) {
                    phoneNumbers.add(new com.navdy.service.library.events.contacts.PhoneNumber.Builder().number(number).build());
                }
            } catch (org.json.JSONException e) {
                logger.e("Unable to parse placeDetailJson: " + this.placeDetailJson, e);
            }
        }
        return phoneNumbers;
    }

    public java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> getContacts() {
        java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> contacts = new java.util.ArrayList<>();
        logger.d("contactLookupKey = " + this.contactLookupKey + ", lastKnownContactId = " + this.lastKnownContactId);
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            return contacts;
        }
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        long contactId = com.navdy.client.app.framework.util.ContactsManager.getContactIdFromLookupKey(contentResolver, this.contactLookupKey, this.lastKnownContactId);
        if (contactId < 0) {
            contactId = this.lastKnownContactId;
        }
        java.util.List<com.navdy.client.app.framework.models.PhoneNumberModel> numbers = com.navdy.client.app.framework.util.ContactsManager.getPhoneNumbers(contentResolver, contactId);
        if (numbers.size() > 0) {
            java.lang.String contactName = com.navdy.client.app.framework.util.ContactsManager.getContactName(contentResolver, contactId);
            java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> primary = new java.util.ArrayList<>();
            java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> mobile = new java.util.ArrayList<>();
            java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> home = new java.util.ArrayList<>();
            java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> work = new java.util.ArrayList<>();
            java.util.ArrayList<com.navdy.service.library.events.contacts.Contact> other = new java.util.ArrayList<>();
            for (com.navdy.client.app.framework.models.PhoneNumberModel num : numbers) {
                logger.d("number = " + num + " for destination: " + this);
                if (!num.isFax()) {
                    com.navdy.service.library.events.contacts.Contact.Builder builder = new com.navdy.service.library.events.contacts.Contact.Builder().name(contactName).number(num.number).numberType(num.getPhoneNumberProtobufType()).label(num.customType);
                    if (!num.isPrimary) {
                        switch (num.type) {
                            case 1:
                                home.add(builder.build());
                                break;
                            case 2:
                                mobile.add(builder.build());
                                break;
                            case 3:
                                work.add(builder.build());
                                break;
                            default:
                                other.add(0, builder.build());
                                break;
                        }
                    } else {
                        primary.add(builder.build());
                    }
                }
            }
            contacts.addAll(primary);
            contacts.addAll(mobile);
            contacts.addAll(home);
            contacts.addAll(work);
            contacts.addAll(other);
            return deDuplicatePhoneNumbersWhileKeepingOrder(contacts);
        }
        logger.d("No phone numbers for contact lookup key: " + this.contactLookupKey);
        return contacts;
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    private <T> java.util.ArrayList<T> deDuplicatePhoneNumbersWhileKeepingOrder(@android.support.annotation.Nullable java.util.ArrayList<T> list) {
        if (list == null || list.size() <= 0) {
            return list;
        }
        java.util.ArrayList<T> dedupped = new java.util.ArrayList<>(list.size());
        for (int i = 0; i < list.size(); i++) {
            boolean foundDuplicate = false;
            T number = list.get(i);
            java.lang.String num = getComparablePhoneNumber(number);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(num)) {
                int j = 0;
                while (true) {
                    if (j >= i) {
                        break;
                    } else if (num.equals(getComparablePhoneNumber(list.get(j)))) {
                        foundDuplicate = true;
                        break;
                    } else {
                        j++;
                    }
                }
                if (!foundDuplicate) {
                    dedupped.add(number);
                }
            }
        }
        return dedupped;
    }

    private java.lang.String getComparablePhoneNumber(java.lang.Object number) {
        java.lang.String num;
        if (number == null) {
            return null;
        }
        if (!(number instanceof com.navdy.service.library.events.contacts.PhoneNumber) && !(number instanceof com.navdy.service.library.events.contacts.Contact)) {
            return null;
        }
        if (number instanceof com.navdy.service.library.events.contacts.PhoneNumber) {
            num = ((com.navdy.service.library.events.contacts.PhoneNumber) number).number;
        } else {
            num = ((com.navdy.service.library.events.contacts.Contact) number).number;
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(num)) {
            return num.replaceAll("[^0-9+]", "");
        }
        return null;
    }

    @android.support.annotation.Nullable
    public static java.util.ArrayList<com.navdy.service.library.events.destination.Destination> convertDestinationsToProto(java.util.List<com.navdy.client.app.framework.models.Destination> destinations) {
        if (destinations == null || destinations.isEmpty()) {
            return null;
        }
        java.util.ArrayList<com.navdy.service.library.events.destination.Destination> protobufDestinations = new java.util.ArrayList<>(destinations.size());
        for (com.navdy.client.app.framework.models.Destination destination : destinations) {
            if (destination != null) {
                protobufDestinations.add(destination.toProtobufDestinationObject());
            }
        }
        return protobufDestinations;
    }

    public boolean isPlaceDetailsInfoStale() {
        return this.lastPlaceIdRefresh < new java.util.Date().getTime() - PLACE_ID_REFRESH_LIMIT || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeDetailJson);
    }

    public void refreshPlaceIdDataAndUpdateListsAsync() {
        refreshPlaceIdDataAndUpdateListsAsync(null);
    }

    public void refreshPlaceIdDataAndUpdateListsAsync(java.lang.Runnable callback) {
        if (!com.navdy.client.app.framework.util.NetworkUtils.isNetworkAvailable()) {
            logger.i("Unable to refresh google place details due to lack of internet access.");
            if (callback != null) {
                callback.run();
            }
        } else if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId)) {
            logger.i("No place ID so no refresh of google place details.");
            if (callback != null) {
                callback.run();
            }
        } else if (!isPlaceDetailsInfoStale()) {
            logger.i("No need to refresh google place details at this point.");
            if (callback != null) {
                callback.run();
            }
        } else {
            logger.d("Refresh google place details for: " + this);
            new com.navdy.client.app.framework.search.GooglePlacesSearch(new com.navdy.client.app.framework.models.Destination.Anon2(callback)).runDetailsSearchWebApi(this.placeId);
        }
    }

    public void persistPlaceDetailInfoAndUpdateListsAsync() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon3(), 1, com.navdy.service.library.task.TaskManager.TaskPriority.LOW);
    }

    private void persistPlaceDetailInfoAndUpdateLists() {
        updateDestinationLists(persistPlaceDetailInfo());
    }

    public void updateDestinationListsAsync() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon4(), 1, com.navdy.service.library.task.TaskManager.TaskPriority.LOW);
    }

    private void updateDestinationLists(int nbRows) {
        if (nbRows > 0) {
            boolean isFavoriteDestination = isFavoriteDestination();
            if (!isFavoriteDestination && (nbRows > 1 || this.id <= 0)) {
                isFavoriteDestination = isFavoriteDestination(this.placeId);
            }
            if (isFavoriteDestination) {
                com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
            }
            boolean isSuggestion = false;
            java.util.Iterator it = com.navdy.client.app.framework.util.SuggestionManager.getSuggestions().iterator();
            while (it.hasNext()) {
                com.navdy.client.app.framework.models.Suggestion suggestion = (com.navdy.client.app.framework.models.Suggestion) it.next();
                if (!(suggestion == null || suggestion.destination == null || !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(suggestion.destination.placeId, this.placeId))) {
                    isSuggestion = true;
                }
            }
            if (isSuggestion) {
                com.navdy.client.app.framework.util.SuggestionManager.buildSuggestionList(true);
            }
        }
    }

    public int persistPlaceDetailInfo() {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId)) {
            return -1;
        }
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            return contentResolver.update(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, getPlaceDetailInfoContentValues(), "place_id=?", new java.lang.String[]{this.placeId});
        }
        logger.e("Unable to get contentResolver !");
        return -1;
    }

    public void reloadSelfFromDatabase() {
        com.navdy.client.app.framework.models.Destination dbDestination = com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(this);
        if (dbDestination == null) {
            logger.d("reloadSelfFromDatabase: destination not found in DB");
        } else {
            mergeWith(dbDestination);
        }
    }

    public void reloadSelfFromDatabaseAsync(java.lang.Runnable onFinish) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon5(onFinish), 1);
    }

    public int findInDb() {
        int id2 = -1;
        try {
            android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
            android.util.Pair<java.lang.String, java.lang.String[]> comparisonSelectionClause = getComparisonSelectionClause();
            if (comparisonSelectionClause == null) {
                return -1;
            }
            android.database.Cursor c = contentResolver.query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, new java.lang.String[]{"_id"}, (java.lang.String) comparisonSelectionClause.first, (java.lang.String[]) comparisonSelectionClause.second, null);
            if (c != null && c.moveToFirst()) {
                id2 = c.getInt(0);
                logger.d("Found this destination in the db under id: " + id2);
            }
            com.navdy.service.library.util.IOUtils.closeStream(c);
            return id2;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    private void makeSureWeHaveDestinationInDb() {
        if (!isPersisted()) {
            this.id = findInDb();
            if (this.id > 0) {
                logger.v("Found this ID: " + this.id);
                return;
            }
            logger.e("Couldn't find a matching destination in the db");
            android.net.Uri uri = saveToDb();
            if (uri != null) {
                try {
                    this.id = java.lang.Integer.parseInt(uri.getLastPathSegment());
                } catch (java.lang.NumberFormatException e) {
                    logger.e("Unable to parse destination id from the URI path segment", e);
                }
            }
        }
    }

    @android.support.annotation.Nullable
    public android.util.Pair<java.lang.String, java.lang.String[]> getComparisonSelectionClause() {
        java.lang.String selection;
        java.lang.String[] selectionArgs;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId) && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            selection = "place_id=?";
            selectionArgs = new java.lang.String[]{this.placeId};
        } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            selection = "(place_id is not null AND place_id=?) OR (place_id is null AND address=?)";
            selectionArgs = new java.lang.String[]{this.placeId, this.rawAddressNotForDisplay};
        } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.rawAddressNotForDisplay)) {
            return null;
        } else {
            java.lang.StringBuilder selectionBuilder = new java.lang.StringBuilder("address=?");
            java.util.ArrayList<java.lang.String> args = new java.util.ArrayList<>();
            args.add(this.rawAddressNotForDisplay);
            if (this.rawAddressVariations != null) {
                java.util.Iterator it = this.rawAddressVariations.iterator();
                while (it.hasNext()) {
                    selectionBuilder.append(org.droidparts.contract.SQL.OR).append((java.lang.String) it.next()).append("=?");
                }
            }
            selection = selectionBuilder.toString();
            selectionArgs = (java.lang.String[]) args.toArray(new java.lang.String[args.size()]);
        }
        return new android.util.Pair<>(selection, selectionArgs);
    }

    private int updateDb(android.content.ContentValues cv) {
        makeSureWeHaveDestinationInDb();
        android.content.ContentResolver cr = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (cr != null) {
            int nbRows = cr.update(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, cv, java.lang.String.format("%s=?", new java.lang.Object[]{"_id"}), new java.lang.String[]{java.lang.String.valueOf(this.id)});
            logger.d("update returned " + nbRows + " rows.");
            return nbRows;
        }
        logger.e("Unable to get content resolver !");
        return 0;
    }

    private void updateAllColumnsOrInsertNewEntryInDbAsync() {
        updateAllColumnsOrInsertNewEntryInDbAsync(null);
    }

    private void updateAllColumnsOrInsertNewEntryInDbAsync(com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback callback) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon6(callback), 1);
    }

    @android.support.annotation.WorkerThread
    public android.net.Uri updateAllColumnsOrInsertNewEntryInDb() {
        logger.v("Saving this destination to the DB: " + toString());
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        if (((long) this.id) <= 0) {
            this.id = findInDb();
            if (((long) this.id) < 0) {
                return saveToDb();
            }
        }
        updateDb(getAllContentValues());
        return android.net.Uri.withAppendedPath(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, java.lang.String.valueOf(this.id));
    }

    public void updateDoNotSuggestAsync() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon7(), 1);
    }

    private void updateDoNotSuggest() {
        if (this.id <= 0) {
            logger.e("Trying to updateDoNotSuggest on an object that has no ID!");
            return;
        }
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver == null) {
            logger.e("Unable to get a content resolver");
            return;
        }
        android.content.ContentValues contentValues = new android.content.ContentValues(1);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DO_NOT_SUGGEST, java.lang.Boolean.valueOf(this.doNotSuggest));
        contentResolver.update(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, contentValues, "_id=?", new java.lang.String[]{java.lang.String.valueOf(this.id)});
    }

    private void saveToDbAsync() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon8(), 1);
    }

    private android.net.Uri saveToDb() {
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            android.net.Uri uri = contentResolver.insert(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, getAllContentValues());
            if (uri != null) {
                this.id = java.lang.Integer.parseInt(uri.getLastPathSegment());
            }
            if (isRecentDestination()) {
                com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.SUGGESTIONS);
            }
            if (!isFavoriteDestination()) {
                return uri;
            }
            com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SAVING_A_FAVORITE);
            com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
            return uri;
        }
        logger.e("Unable to get contentResolver !");
        return null;
    }

    @android.support.annotation.NonNull
    private android.content.ContentValues getPlaceDetailInfoContentValues() {
        return getContentValues(false);
    }

    @android.support.annotation.NonNull
    private android.content.ContentValues getAllContentValues() {
        return getContentValues(true);
    }

    @android.support.annotation.NonNull
    private android.content.ContentValues getContentValues(boolean includeExtras) {
        android.content.ContentValues destinationValues = new android.content.ContentValues();
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_ID, this.placeId);
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_PLACE_ID_REFRESH, java.lang.Long.valueOf(this.lastPlaceIdRefresh));
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, java.lang.Double.valueOf(this.displayLat));
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, java.lang.Double.valueOf(this.displayLong));
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, java.lang.Double.valueOf(this.navigationLat));
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG, java.lang.Double.valueOf(this.navigationLong));
        if (this.name == null) {
            this.name = "";
        }
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAME, this.name);
        this.rawAddressNotForDisplay = com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress(this.rawAddressNotForDisplay);
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ADDRESS, this.rawAddressNotForDisplay);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetNumber)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER, this.streetNumber);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetName)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NAME, this.streetName);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.city)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CITY, this.city);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.state)) {
            destinationValues.put("state", this.state);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.zipCode)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ZIP_CODE, this.zipCode);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.country)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY, this.country);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.countryCode)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE, this.countryCode);
        }
        if (includeExtras) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DO_NOT_SUGGEST, java.lang.Integer.valueOf(this.doNotSuggest ? 1 : 0));
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE, java.lang.Long.valueOf(this.lastRoutedDate));
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LABEL, this.favoriteLabel);
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ORDER, java.lang.Integer.valueOf(this.favoriteOrder));
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE, java.lang.Integer.valueOf(this.favoriteType));
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeDetailJson)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_DETAIL_JSON, this.placeDetailJson);
        }
        destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PRECISION_LEVEL, java.lang.Integer.valueOf(this.precisionLevel.getValue()));
        destinationValues.put("type", java.lang.Integer.valueOf(this.type.getValue()));
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.contactLookupKey)) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY, this.contactLookupKey);
        }
        if (this.lastKnownContactId > 0) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID, java.lang.Long.valueOf(this.lastKnownContactId));
        }
        if (this.lastContactLookup > 0) {
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP, java.lang.Long.valueOf(this.lastContactLookup));
        }
        return destinationValues;
    }

    private boolean shouldDeleteDestinationEntryAsWell() {
        return !isFavoriteDestination() && !isRecentDestination() && !isInferredDestination() && !this.doNotSuggest;
    }

    public void deleteFromDbAsync() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon9(), 1);
    }

    private int deleteFromDb() {
        boolean success = false;
        android.content.ContentResolver cr = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        int nbDeleted = 0;
        if (cr != null) {
            nbDeleted = cr.delete(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, java.lang.String.format("%s=?", new java.lang.Object[]{"_id"}), new java.lang.String[]{java.lang.String.valueOf(this.id)});
            success = nbDeleted > 0;
        } else {
            logger.e("Unable to get content resolver !");
        }
        if (success) {
            if (isFavoriteDestination()) {
                com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.DELETING_A_FAVORITE);
                com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
            }
            if (isRecentDestination()) {
                com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.SUGGESTIONS);
            }
        } else if (isFavoriteDestination()) {
            com.navdy.client.app.ui.base.BaseActivity.showLongToast(com.navdy.client.R.string.toast_favorite_delete_error, new java.lang.Object[0]);
        }
        return nbDeleted;
    }

    public boolean hasOneValidSetOfCoordinates() {
        return hasValidNavCoordinates() || hasValidDisplayCoordinates();
    }

    public boolean hasValidDisplayCoordinates() {
        return com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(this.displayLat, this.displayLong);
    }

    public boolean hasValidNavCoordinates() {
        return com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(this.navigationLat, this.navigationLong);
    }

    public void setCoordsToSame(com.google.android.gms.maps.model.LatLng latlng) {
        this.displayLat = latlng.latitude;
        this.displayLong = latlng.longitude;
        this.navigationLat = latlng.latitude;
        this.navigationLong = latlng.longitude;
        this.precisionLevel = com.navdy.client.app.framework.models.Destination.Precision.PRECISE;
        this.hasBeenProcessed = true;
    }

    @android.support.annotation.WorkerThread
    public void handleNewCoordsAndAddress(double displayLat2, double displayLong2, double navigationLat2, double navigationLong2, com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precision) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        boolean newNavIsValid = com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(navigationLat2, navigationLong2);
        if (address != null && (!hasDetailedAddress() || newNavIsValid)) {
            setAddressFieldsFromHereAddress(address);
        }
        if (newNavIsValid) {
            this.displayLat = displayLat2;
            this.displayLong = displayLong2;
            this.navigationLat = navigationLat2;
            this.navigationLong = navigationLong2;
            if (precision != com.navdy.client.app.framework.models.Destination.Precision.UNKNOWN) {
                this.precisionLevel = precision;
            }
            logger.d("Navigation latLng from Here: " + navigationLat2 + "," + navigationLong2 + "; precision=" + precision);
        }
        this.hasBeenProcessed = true;
        android.content.ContentValues coordValues = getContentValuesForCoords();
        android.content.ContentValues addrValues = getContentValuesForAddress();
        if (saveContentValuesToDb(coordValues, addrValues) <= 0) {
            logger.e("Error while handling new coords and address from here. Unable to update the DB with values: coordValues: " + coordValues + " and addrValues:" + addrValues);
        }
    }

    private int saveContentValuesToDb(android.content.ContentValues... contentValues) {
        makeSureWeHaveDestinationInDb();
        android.content.ContentValues allContentValues = new android.content.ContentValues();
        for (android.content.ContentValues contentValueGroup : contentValues) {
            if (contentValueGroup != null && contentValueGroup.size() > 0) {
                allContentValues.putAll(contentValueGroup);
            }
        }
        if (allContentValues.size() < 1) {
            logger.d("No content values were found for updating the DB");
            return -1;
        }
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (contentResolver != null) {
            android.content.ContentValues destinationValues = new android.content.ContentValues();
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, java.lang.Double.valueOf(this.navigationLat));
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG, java.lang.Double.valueOf(this.navigationLong));
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, java.lang.Double.valueOf(this.displayLat));
            destinationValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, java.lang.Double.valueOf(this.displayLong));
            return contentResolver.update(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, allContentValues, "_id=?", new java.lang.String[]{java.lang.String.valueOf(this.id)});
        }
        logger.e("Unable to get contentResolver !");
        return -1;
    }

    private android.content.ContentValues getContentValuesForCoords() {
        android.content.ContentValues coordValues = new android.content.ContentValues();
        if (hasValidNavCoordinates()) {
            coordValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, java.lang.Double.valueOf(this.navigationLat));
            coordValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG, java.lang.Double.valueOf(this.navigationLong));
            coordValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PRECISION_LEVEL, java.lang.Integer.valueOf(this.precisionLevel.getValue()));
        }
        if (hasValidDisplayCoordinates()) {
            coordValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, java.lang.Double.valueOf(this.displayLat));
            coordValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, java.lang.Double.valueOf(this.displayLong));
        }
        return coordValues;
    }

    private android.content.ContentValues getContentValuesForAddress() {
        android.content.ContentValues addressValues = new android.content.ContentValues();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetNumber)) {
            addressValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER, this.streetNumber);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.streetName)) {
            addressValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NAME, this.streetName);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.city)) {
            addressValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CITY, this.city);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.state)) {
            addressValues.put("state", this.state);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.zipCode)) {
            addressValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ZIP_CODE, this.zipCode);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.country)) {
            addressValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY, this.country);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.countryCode)) {
            addressValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY_CODE, this.countryCode);
        }
        return addressValues;
    }

    public static android.util.Pair<java.lang.String, java.lang.String[]> getCoordinateBoundingBoxSelectionClause(double latitude, double longitude) {
        android.location.Location[] boundingBox = com.navdy.client.app.framework.map.MapUtils.getBoundingBox(latitude, longitude, 5.0d);
        if (boundingBox.length != 2) {
            return new android.util.Pair<>(null, null);
        }
        return new android.util.Pair<>(java.lang.String.format("((%s >= ? AND %s <= ?) AND (%s >= ? AND %s <= ?)) OR ((%s >= ? AND %s <= ?) AND (%s >= ? AND %s <= ?))", new java.lang.Object[]{com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LAT, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_DISPLAY_LONG, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LAT, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG, com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAVIGATION_LONG}), new java.lang.String[]{java.lang.String.valueOf(boundingBox[0].getLatitude()), java.lang.String.valueOf(boundingBox[1].getLatitude()), java.lang.String.valueOf(boundingBox[0].getLongitude()), java.lang.String.valueOf(boundingBox[1].getLongitude()), java.lang.String.valueOf(boundingBox[0].getLatitude()), java.lang.String.valueOf(boundingBox[1].getLatitude()), java.lang.String.valueOf(boundingBox[0].getLongitude()), java.lang.String.valueOf(boundingBox[1].getLongitude())});
    }

    public int getPinAsset() {
        return getPinAsset(true, false, false);
    }

    public int getUnselectedPinAsset() {
        return getPinAsset(false, false, false);
    }

    public int getRecentPinAsset() {
        return getPinAsset(true, true, false);
    }

    private int getActiveTripPinAsset() {
        return getPinAsset(true, false, true);
    }

    private int getPinAsset(boolean isSelected, boolean recentTakesPrecedence, boolean isActiveTrip) {
        if (isHome()) {
            if (isSelected) {
                return com.navdy.client.R.drawable.icon_pin_home;
            }
            return com.navdy.client.R.drawable.icon_pin_home_unselected;
        } else if (isWork()) {
            return isSelected ? com.navdy.client.R.drawable.icon_pin_work : com.navdy.client.R.drawable.icon_pin_work_unselected;
        } else {
            if (isContact()) {
                return isSelected ? com.navdy.client.R.drawable.icon_pin_user_blue : com.navdy.client.R.drawable.icon_pin_user_grey;
            }
            if (isFavoriteDestination()) {
                return isSelected ? com.navdy.client.R.drawable.icon_pin_favorite : com.navdy.client.R.drawable.icon_pin_favorite_unselected;
            }
            if (!recentTakesPrecedence || isActiveTrip || !isRecentDestination()) {
                switch (this.type) {
                    case CALENDAR_EVENT:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_calendar : com.navdy.client.R.drawable.icon_pin_calendar_unselected;
                    case GAS_STATION:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_gas : com.navdy.client.R.drawable.icon_pin_gas_unselected;
                    case AIRPORT:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_airport : com.navdy.client.R.drawable.icon_pin_airport_unselected;
                    case PARKING:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_parking : com.navdy.client.R.drawable.icon_pin_parking_unselected;
                    case TRANSIT:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_transit : com.navdy.client.R.drawable.icon_pin_transit_unselected;
                    case ATM:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_atm : com.navdy.client.R.drawable.icon_pin_atm_unselected;
                    case BANK:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_bank : com.navdy.client.R.drawable.icon_pin_bank_unselected;
                    case SCHOOL:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_school : com.navdy.client.R.drawable.icon_pin_school_unselected;
                    case STORE:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_store : com.navdy.client.R.drawable.icon_pin_store_unselected;
                    case COFFEE:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_coffee : com.navdy.client.R.drawable.icon_pin_coffee_unselected;
                    case RESTAURANT:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_restaurant : com.navdy.client.R.drawable.icon_pin_restaurant_unselected;
                    case GYM:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_gym : com.navdy.client.R.drawable.icon_pin_gym_unselected;
                    case PARK:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_park : com.navdy.client.R.drawable.icon_pin_park_unselected;
                    case HOSPITAL:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_hospital : com.navdy.client.R.drawable.icon_pin_hospital_unselected;
                    case ENTERTAINMENT:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_entertainment : com.navdy.client.R.drawable.icon_pin_entertainment_unselected;
                    case BAR:
                        return isSelected ? com.navdy.client.R.drawable.icon_pin_bar : com.navdy.client.R.drawable.icon_pin_bar_unselected;
                    default:
                        if (isActiveTrip || !isRecentDestination()) {
                            return isSelected ? com.navdy.client.R.drawable.icon_pin_place : com.navdy.client.R.drawable.icon_pin_place_unselected;
                        }
                        if (isSelected) {
                            return com.navdy.client.R.drawable.icon_pin_recent;
                        }
                        r0 = com.navdy.client.R.drawable.icon_pin_recent_unselected;
                        return com.navdy.client.R.drawable.icon_pin_recent_unselected;
                }
            } else if (isSelected) {
                return com.navdy.client.R.drawable.icon_pin_recent;
            } else {
                r0 = com.navdy.client.R.drawable.icon_pin_recent_unselected;
                return com.navdy.client.R.drawable.icon_pin_recent_unselected;
            }
        }
    }

    public int getBadgeAsset() {
        return getBadgeAsset(true, false, false, false);
    }

    public int getBadgeAssetForActiveTrip() {
        return getBadgeAsset(false, false, false, true);
    }

    public int getBadgeAssetForPendingTrip() {
        return getBadgeAsset(true, false, false, true);
    }

    public int getBadgeAssetForSuggestion() {
        return getBadgeAsset(false, false, true, false);
    }

    int getBadgeAssetForRecent() {
        return getBadgeAsset(true, true, false, false);
    }

    public int getBadgeAssetForAutoComplete() {
        return getBadgeAsset(true, true, true, false);
    }

    public int getBadgeAssetForSearchResults() {
        return getBadgeAsset(true, false, false, false);
    }

    private int getBadgeAsset(boolean isOverWhiteBg, boolean isGrey, boolean recentTakesPrecedence, boolean isActiveTrip) {
        int i = 2130837787;
        if (isHome()) {
            if (isGrey) {
                return com.navdy.client.R.drawable.icon_badge_home_light;
            }
            return com.navdy.client.R.drawable.icon_badge_home;
        } else if (isWork()) {
            return isGrey ? com.navdy.client.R.drawable.icon_badge_work_light : com.navdy.client.R.drawable.icon_badge_work;
        } else {
            if (isContact()) {
                if (isGrey) {
                    return com.navdy.client.R.drawable.icon_badge_user_grey;
                }
                r0 = com.navdy.client.R.drawable.icon_badge_user_blue;
                return com.navdy.client.R.drawable.icon_badge_user_blue;
            } else if (isFavoriteDestination()) {
                return isGrey ? com.navdy.client.R.drawable.icon_badge_favorite_light : com.navdy.client.R.drawable.icon_badge_favorite;
            } else {
                if (!recentTakesPrecedence || isActiveTrip || !isRecentDestination()) {
                    switch (this.type) {
                        case CONTACT:
                            if (isGrey) {
                                return com.navdy.client.R.drawable.icon_badge_user_grey;
                            }
                            r0 = com.navdy.client.R.drawable.icon_badge_user_blue;
                            return com.navdy.client.R.drawable.icon_badge_user_blue;
                        case CALENDAR_EVENT:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_calendar_light : com.navdy.client.R.drawable.icon_badge_calendar;
                        case GAS_STATION:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_gas_light : com.navdy.client.R.drawable.icon_badge_gas;
                        case AIRPORT:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_airport_light : com.navdy.client.R.drawable.icon_badge_airport;
                        case PARKING:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_parking_light : com.navdy.client.R.drawable.icon_badge_parking;
                        case TRANSIT:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_transit_light : com.navdy.client.R.drawable.icon_badge_transit;
                        case ATM:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_atm_light : com.navdy.client.R.drawable.icon_badge_atm;
                        case BANK:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_bank_light : com.navdy.client.R.drawable.icon_badge_bank;
                        case SCHOOL:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_school_light : com.navdy.client.R.drawable.icon_badge_school;
                        case STORE:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_store_light : com.navdy.client.R.drawable.icon_badge_store;
                        case COFFEE:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_coffee_light : com.navdy.client.R.drawable.icon_badge_coffee;
                        case RESTAURANT:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_restaurant_light : com.navdy.client.R.drawable.icon_badge_restaurant;
                        case GYM:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_gym_light : com.navdy.client.R.drawable.icon_badge_gym;
                        case PARK:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_park_light : com.navdy.client.R.drawable.icon_badge_park;
                        case HOSPITAL:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_hospital_light : com.navdy.client.R.drawable.icon_badge_hospital;
                        case ENTERTAINMENT:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_entertainment_light : com.navdy.client.R.drawable.icon_badge_entertainment;
                        case BAR:
                            return isGrey ? com.navdy.client.R.drawable.icon_badge_bar_light : com.navdy.client.R.drawable.icon_badge_bar;
                        default:
                            if (isActiveTrip || !isRecentDestination()) {
                                return getDefaultBadgeAsset(isOverWhiteBg, isGrey);
                            }
                            if (!isGrey) {
                                i = com.navdy.client.R.drawable.icon_badge_recent;
                            }
                            return i;
                    }
                } else if (isGrey) {
                    r0 = com.navdy.client.R.drawable.icon_badge_recent_light;
                    return com.navdy.client.R.drawable.icon_badge_recent_light;
                } else {
                    r0 = com.navdy.client.R.drawable.icon_badge_recent;
                    return com.navdy.client.R.drawable.icon_badge_recent;
                }
            }
        }
    }

    private static int getDefaultBadgeAsset(boolean isOverWhiteBg, boolean isLight) {
        if (isOverWhiteBg) {
            return isLight ? com.navdy.client.R.drawable.icon_badge_place_light : com.navdy.client.R.drawable.icon_badge_place;
        }
        return com.navdy.client.R.drawable.icon_badge_active_trip;
    }

    @android.support.annotation.Nullable
    public com.here.android.mpa.mapping.MapMarker getHereMapMarker() {
        double latitude;
        double longitude;
        com.here.android.mpa.common.Image image = new com.here.android.mpa.common.Image();
        try {
            image.setImageResource(getActiveTripPinAsset());
            if (hasValidDisplayCoordinates()) {
                latitude = this.displayLat;
                longitude = this.displayLong;
            } else {
                latitude = this.navigationLat;
                longitude = this.navigationLong;
            }
            com.here.android.mpa.mapping.MapMarker mapMarker = new com.here.android.mpa.mapping.MapMarker(new com.here.android.mpa.common.GeoCoordinate(latitude, longitude), image);
            mapMarker.setZIndex(com.navdy.client.app.NavdyApplication.getAppContext().getResources().getInteger(com.navdy.client.R.integer.map_marker_z_index));
            mapMarker.setAnchorPoint(new android.graphics.PointF(((float) image.getWidth()) / 2.0f, (float) image.getHeight()));
            return mapMarker;
        } catch (java.io.IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isRecentDestination() {
        return this.lastRoutedDate > 0;
    }

    public void updateLastRoutedDateInDbAsync() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon10(), 1);
    }

    public int updateLastRoutedDateInDb() {
        if (this.lastRoutedDate <= 0) {
            this.lastRoutedDate = new java.util.Date().getTime();
        }
        makeSureWeHaveDestinationInDb();
        android.content.ContentValues recentValues = new android.content.ContentValues();
        recentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_ROUTED_DATE, java.lang.Long.valueOf(new java.util.Date().getTime()));
        int nbUpdatedRows = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().update(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, recentValues, java.lang.String.format("%s=?", new java.lang.Object[]{"_id"}), new java.lang.String[]{java.lang.String.valueOf(this.id)});
        logger.v("Updated " + nbUpdatedRows + " entries");
        if (nbUpdatedRows > 0) {
            com.navdy.client.app.framework.util.SuggestionManager.forceSuggestionFullRefresh();
        }
        return nbUpdatedRows;
    }

    public void setIsCalendarEvent(boolean is) {
        this.isCalendarEvent = is;
    }

    public boolean isCalendarEvent() {
        return this.isCalendarEvent || this.type == com.navdy.client.app.framework.models.Destination.Type.CALENDAR_EVENT;
    }

    public boolean isContact() {
        return this.type == com.navdy.client.app.framework.models.Destination.Type.CONTACT || this.favoriteType == -4;
    }

    public boolean isHome() {
        return this.favoriteType == -3;
    }

    public boolean isWork() {
        return this.favoriteType == -2;
    }

    public boolean isFavoriteDestination() {
        return isFavoriteDestination(this.favoriteType);
    }

    public static boolean isFavoriteDestination(int favoriteType2) {
        return favoriteType2 != 0;
    }

    private static boolean isFavoriteDestination(java.lang.String placeId2) {
        boolean isFavoriteDestination = false;
        android.database.Cursor cursor = null;
        try {
            cursor = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().query(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, new java.lang.String[]{com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE}, "place_id=?", new java.lang.String[]{placeId2}, null);
            if (cursor != null && cursor.moveToFirst()) {
                isFavoriteDestination = isFavoriteDestination(cursor.getInt(0));
            }
            return isFavoriteDestination;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    public void saveDestinationAsFavoritesAsync(int specialType, com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback callback) {
        new com.navdy.client.app.framework.models.Destination.Anon11(specialType, callback).execute(new java.lang.Object[0]);
    }

    /* JADX INFO: finally extract failed */
    private int saveDestinationAsFavorite(int specialType) {
        int oldOrder;
        int newOrder;
        int nbRows = -1;
        android.database.Cursor favoritesCursor = null;
        try {
            favoritesCursor = com.navdy.client.app.providers.NavdyContentProvider.getFavoritesCursor();
            int count = favoritesCursor.getCount();
            com.navdy.service.library.util.IOUtils.closeObject(favoritesCursor);
            if (!isPersisted() || !isFavoriteDestination()) {
                oldOrder = count;
            } else {
                oldOrder = this.favoriteOrder;
            }
            this.favoriteType = specialType;
            if (specialType == -1 && isContact()) {
                this.favoriteType = -4;
            }
            if (specialType == -3) {
                newOrder = 0;
            } else if (specialType == -2) {
                com.navdy.client.app.framework.models.Destination home = com.navdy.client.app.providers.NavdyContentProvider.getHome();
                if (home == null || home.favoriteOrder != 0) {
                    newOrder = 0;
                } else {
                    newOrder = 1;
                }
            } else {
                newOrder = count;
            }
            logger.v("Moving favorite from position " + oldOrder + " to " + newOrder);
            this.favoriteOrder = newOrder;
            shiftListForMove(oldOrder, newOrder);
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.favoriteLabel)) {
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.name)) {
                    this.favoriteLabel = this.name;
                } else {
                    this.favoriteLabel = this.rawAddressNotForDisplay;
                }
            }
            if (!isPersisted() && findInDb() > 0) {
                reloadSelfFromDatabase();
            }
            if (isPersisted()) {
                nbRows = updateOnlyFavoriteFieldsInDb();
            } else if (saveToDb() != null) {
                nbRows = 1;
            }
            com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
            return nbRows;
        } catch (Throwable th) {
            com.navdy.service.library.util.IOUtils.closeObject(favoritesCursor);
            throw th;
        }
    }

    public int updateOnlyFavoriteFieldsInDb() {
        return updateOnlyFavoriteFieldsInDb(true);
    }

    public int updateOnlyFavoriteFieldsInDb(boolean sendToDisplay) {
        android.content.ContentValues cv = new android.content.ContentValues();
        cv.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_NAME, this.name);
        cv.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LABEL, this.favoriteLabel);
        cv.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ORDER, java.lang.Integer.valueOf(this.favoriteOrder));
        cv.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_FAVORITE_TYPE, java.lang.Integer.valueOf(this.favoriteType));
        int nbRows = updateDb(cv);
        if (nbRows <= 0) {
            logger.e(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.toast_favorite_edit_error));
        } else if (sendToDisplay) {
            com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
        }
        return nbRows;
    }

    public void deleteFavoriteFromDbAsync(com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback callback) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.models.Destination.Anon12(callback), 1);
    }

    /* JADX INFO: finally extract failed */
    private int deleteFavoriteFromDb() {
        android.database.Cursor favoritesCursor = null;
        try {
            favoritesCursor = com.navdy.client.app.providers.NavdyContentProvider.getFavoritesCursor();
            int newOrder = favoritesCursor.getCount();
            com.navdy.service.library.util.IOUtils.closeObject(favoritesCursor);
            int oldOrder = this.favoriteOrder;
            logger.v("Moving favorite from position " + oldOrder + " to " + newOrder);
            this.favoriteOrder = newOrder;
            shiftListForMove(oldOrder, newOrder);
            if (shouldDeleteDestinationEntryAsWell()) {
                return deleteFromDb();
            }
            this.favoriteLabel = "";
            this.favoriteOrder = 0;
            this.favoriteType = 0;
            return updateOnlyFavoriteFieldsInDb();
        } catch (Throwable th) {
            com.navdy.service.library.util.IOUtils.closeObject(favoritesCursor);
            throw th;
        }
    }

    private static void shiftListForMove(int oldOrder, int newOrder) {
        int startOrder;
        int endOrder;
        int shift;
        if (newOrder < oldOrder) {
            startOrder = newOrder;
            endOrder = oldOrder - 1;
            shift = 1;
        } else if (newOrder > oldOrder) {
            startOrder = oldOrder + 1;
            endOrder = newOrder;
            shift = -1;
        } else {
            logger.v("No need to shift list");
            return;
        }
        android.database.Cursor cursor = null;
        try {
            cursor = com.navdy.client.app.providers.NavdyContentProvider.getFavoritesCursor(selectionForShift(startOrder, endOrder));
            android.content.ContentValues contentValues = new android.content.ContentValues();
            int i = 0;
            while (cursor != null && i < cursor.getCount()) {
                com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.providers.NavdyContentProvider.getDestinationItemAt(cursor, i);
                contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_ORDER, java.lang.Integer.valueOf(destination.favoriteOrder + shift));
                destination.updateDb(contentValues);
                i++;
            }
        } finally {
            com.navdy.service.library.util.IOUtils.closeObject(cursor);
        }
    }

    @android.support.annotation.NonNull
    private static android.util.Pair<java.lang.String, java.lang.String[]> selectionForShift(int startOrder, int endOrder) {
        return new android.util.Pair<>("favorite_listing_order >= ? AND favorite_listing_order <= ?", new java.lang.String[]{java.lang.String.valueOf(startOrder), java.lang.String.valueOf(endOrder)});
    }

    private boolean isInferredDestination() {
        android.database.Cursor cursor = null;
        try {
            cursor = com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor(new android.util.Pair<>("destination_id = " + this.id, null));
            return cursor != null && cursor.getCount() > 0;
        } finally {
            com.navdy.service.library.util.IOUtils.closeObject(cursor);
        }
    }

    public static void placesSearchResultToDestinationObject(com.navdy.service.library.events.places.PlacesSearchResult placesSearchResult, com.navdy.client.app.framework.models.Destination destination) {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(placesSearchResult.label)) {
            destination.name = placesSearchResult.label;
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(placesSearchResult.address)) {
            destination.rawAddressNotForDisplay = com.navdy.client.app.framework.i18n.AddressUtils.sanitizeAddress(placesSearchResult.address);
        }
        if (placesSearchResult.destinationLocation != null) {
            destination.displayLat = placesSearchResult.destinationLocation.latitude.doubleValue();
            destination.displayLong = placesSearchResult.destinationLocation.longitude.doubleValue();
        }
        if (placesSearchResult.navigationPosition != null) {
            destination.navigationLat = placesSearchResult.navigationPosition.latitude.doubleValue();
            destination.navigationLong = placesSearchResult.navigationPosition.longitude.doubleValue();
        }
        destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.TEXT_SEARCH.getValue();
    }

    public java.lang.String getNameForDisplay() {
        java.lang.String name2 = "";
        if (isFavoriteDestination()) {
            name2 = this.favoriteLabel;
        }
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(name2)) {
            return this.name;
        }
        return name2;
    }

    private static com.navdy.client.app.framework.models.Destination.Type fromProtobufPlaceType(com.navdy.service.library.events.places.PlaceType placeType) {
        if (placeType == null) {
            return com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
        }
        switch (placeType) {
            case PLACE_TYPE_AIRPORT:
                return com.navdy.client.app.framework.models.Destination.Type.AIRPORT;
            case PLACE_TYPE_ATM:
                return com.navdy.client.app.framework.models.Destination.Type.ATM;
            case PLACE_TYPE_BANK:
                return com.navdy.client.app.framework.models.Destination.Type.BANK;
            case PLACE_TYPE_BAR:
                return com.navdy.client.app.framework.models.Destination.Type.BAR;
            case PLACE_TYPE_CALENDAR_EVENT:
                return com.navdy.client.app.framework.models.Destination.Type.CALENDAR_EVENT;
            case PLACE_TYPE_COFFEE:
                return com.navdy.client.app.framework.models.Destination.Type.COFFEE;
            case PLACE_TYPE_CONTACT:
                return com.navdy.client.app.framework.models.Destination.Type.CONTACT;
            case PLACE_TYPE_ENTERTAINMENT:
                return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
            case PLACE_TYPE_GAS:
                return com.navdy.client.app.framework.models.Destination.Type.GAS_STATION;
            case PLACE_TYPE_GYM:
                return com.navdy.client.app.framework.models.Destination.Type.GYM;
            case PLACE_TYPE_HOSPITAL:
                return com.navdy.client.app.framework.models.Destination.Type.HOSPITAL;
            case PLACE_TYPE_PARK:
                return com.navdy.client.app.framework.models.Destination.Type.PARK;
            case PLACE_TYPE_PARKING:
                return com.navdy.client.app.framework.models.Destination.Type.PARKING;
            case PLACE_TYPE_RESTAURANT:
                return com.navdy.client.app.framework.models.Destination.Type.RESTAURANT;
            case PLACE_TYPE_SCHOOL:
                return com.navdy.client.app.framework.models.Destination.Type.SCHOOL;
            case PLACE_TYPE_STORE:
                return com.navdy.client.app.framework.models.Destination.Type.STORE;
            case PLACE_TYPE_TRANSIT:
                return com.navdy.client.app.framework.models.Destination.Type.TRANSIT;
            default:
                return com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
        }
    }

    public com.here.android.mpa.search.Address getHereAddress() {
        com.here.android.mpa.search.Address hereAddress = new com.here.android.mpa.search.Address();
        if (this.rawAddressNotForDisplay != null) {
            hereAddress.setText(this.rawAddressNotForDisplay);
        }
        if (this.country != null) {
            hereAddress.setCountryName(this.country);
        }
        if (this.countryCode != null) {
            if (this.countryCode.length() == 2) {
                hereAddress.setCountryCode(com.navdy.client.app.framework.i18n.AddressUtils.convertIso2ToIso3(this.countryCode));
            } else {
                hereAddress.setCountryCode(this.countryCode);
            }
        }
        if (this.state != null) {
            hereAddress.setState(this.state);
        }
        if (this.city != null) {
            hereAddress.setCity(this.city);
        }
        if (this.zipCode != null) {
            hereAddress.setPostalCode(this.zipCode);
        }
        return hereAddress;
    }
}
