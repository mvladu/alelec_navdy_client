package com.navdy.client.app.framework.models;

public class GoogleTextSearchDestinationResult {
    public static final java.lang.String FORMATTED_PHONE_NUMBER = "formatted_phone_number";
    public static final java.lang.String INTERNATIONAL_PHONE_NUMBER = "formatted_phone_number";
    static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.class);
    public java.lang.String address;
    public java.lang.String city;
    public java.lang.String country;
    public java.lang.String countryCode;
    public double distance;
    public java.lang.String icon;
    public java.lang.String jsonString;
    public java.lang.String lat;
    public java.lang.String lng;
    public java.lang.String name;
    public java.util.List<java.lang.String> open_hours;
    public boolean open_now;
    public java.lang.String phone;
    public java.lang.String place_id;
    public java.lang.String price_level;
    public java.lang.String rating;
    public java.lang.String state;
    public java.lang.String streetName;
    public java.lang.String streetNumber;
    public java.lang.String[] types;
    public java.lang.String url;
    public java.lang.String zipCode;

    public static com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult createDestinationObject(java.lang.String jsonString2) {
        try {
            return new com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult(new org.json.JSONObject(jsonString2));
        } catch (org.json.JSONException e) {
            logger.e("Unable to parse the json string for a Google text search result: " + jsonString2, e);
            return null;
        }
    }

    public static com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult createDestinationObject(org.json.JSONObject jsonObject) {
        try {
            return new com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult(jsonObject);
        } catch (org.json.JSONException e) {
            logger.e("Unable to parse the json object for a Google text search result: " + jsonObject, e);
            return null;
        }
    }

    private GoogleTextSearchDestinationResult(org.json.JSONObject jsonObject) throws org.json.JSONException {
        this.jsonString = jsonObject.toString();
        if (jsonObject.has("name")) {
            this.name = jsonObject.getString("name");
        } else if (jsonObject.has("terms")) {
            org.json.JSONArray terms = jsonObject.getJSONArray("terms");
            this.name = terms.getJSONObject(0).getString("value");
            java.util.ArrayList<java.lang.String> addressParts = new java.util.ArrayList<>();
            for (int i = 1; i < terms.length(); i++) {
                addressParts.add(terms.getJSONObject(i).getString("value"));
            }
            this.address = com.navdy.client.app.framework.util.StringUtils.join(addressParts, org.droidparts.contract.SQL.DDL.SEPARATOR);
        } else if (jsonObject.has("description")) {
            this.name = jsonObject.getString("description");
        }
        if (jsonObject.has("geometry")) {
            org.json.JSONObject location = jsonObject.getJSONObject("geometry").getJSONObject("location");
            this.lat = location.getString("lat");
            this.lng = location.getString("lng");
        }
        if (jsonObject.has("routes")) {
            org.json.JSONArray results = (org.json.JSONArray) jsonObject.get("routes");
            logger.d("Routes: " + (results != null ? results.toString() : com.amazonaws.services.s3.internal.Constants.NULL_VERSION_ID));
            if (results != null && results.length() > 0) {
                org.json.JSONObject firstRoute = results.getJSONObject(0);
                if (firstRoute != null) {
                    org.json.JSONArray legs = (org.json.JSONArray) firstRoute.get("legs");
                    if (legs != null && legs.length() > 0) {
                        org.json.JSONObject lastLeg = legs.getJSONObject(legs.length() - 1);
                        if (lastLeg != null) {
                            org.json.JSONObject endLocation = lastLeg.getJSONObject("end_location");
                            if (endLocation != null) {
                                java.lang.String endLocationLng = endLocation.getString("lng");
                                java.lang.String endLocationLat = endLocation.getString("lat");
                                logger.d("Received endLocation from the last leg: " + endLocationLat + org.droidparts.contract.SQL.DDL.SEPARATOR + endLocationLng);
                                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(endLocationLat) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(endLocationLng)) {
                                    this.lat = endLocationLat;
                                    this.lng = endLocationLng;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (jsonObject.has("lat") && jsonObject.has("lng")) {
            this.lat = jsonObject.getString("lat");
            this.lng = jsonObject.getString("lng");
        }
        if (jsonObject.has("opening_hours")) {
            this.open_hours = new java.util.ArrayList();
            org.json.JSONObject opening_hours = jsonObject.getJSONObject("opening_hours");
            this.open_now = java.lang.Boolean.valueOf(opening_hours.getString("open_now")).booleanValue();
            org.json.JSONArray weekday_text = opening_hours.getJSONArray("weekday_text");
            if (weekday_text.length() > 0) {
                for (int i2 = 0; i2 < weekday_text.length(); i2++) {
                    java.lang.String dayOfWeek = weekday_text.getString(i2);
                    if (dayOfWeek != null) {
                        this.open_hours.add(dayOfWeek);
                        logger.v("day of week: " + dayOfWeek);
                    }
                }
            }
        }
        if (jsonObject.has("formatted_address")) {
            this.address = jsonObject.getString("formatted_address");
        } else if (jsonObject.has("vicinity")) {
            this.address = jsonObject.getString("vicinity");
        }
        if (com.navdy.client.app.framework.i18n.AddressUtils.isTitleIsInTheAddress(this.name, this.address)) {
            this.name = "";
        }
        org.json.JSONArray addressComponents = jsonObject.optJSONArray("address_components");
        if (addressComponents != null) {
            for (int aci = 0; aci < addressComponents.length(); aci++) {
                try {
                    org.json.JSONObject obj = addressComponents.getJSONObject(aci);
                    java.lang.String longName = obj.getString("long_name");
                    org.json.JSONArray types2 = obj.getJSONArray("types");
                    int ti = 0;
                    while (true) {
                        if (ti >= types2.length()) {
                            break;
                        }
                        java.lang.String type = types2.getString(ti);
                        if (com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER.equals(type)) {
                            this.streetNumber = longName;
                            break;
                        } else if ("route".equals(type)) {
                            this.streetName = longName;
                            break;
                        } else if ("locality".equals(type)) {
                            this.city = longName;
                            break;
                        } else if ("administrative_area_level_1".equals(type)) {
                            this.state = longName;
                            break;
                        } else if (com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_COUNTRY.equals(type)) {
                            this.country = longName;
                            this.countryCode = obj.getString("short_name");
                            break;
                        } else if ("postal_code".equals(type)) {
                            this.zipCode = longName;
                            break;
                        } else {
                            ti++;
                        }
                    }
                } catch (java.lang.Exception e) {
                }
            }
        }
        if (jsonObject.has("formatted_phone_number")) {
            this.phone = jsonObject.getString("formatted_phone_number");
        }
        if (jsonObject.has("website")) {
            this.url = jsonObject.getString("website");
        }
        if (jsonObject.has(io.fabric.sdk.android.services.settings.SettingsJsonConstants.APP_ICON_KEY)) {
            this.icon = jsonObject.getString(io.fabric.sdk.android.services.settings.SettingsJsonConstants.APP_ICON_KEY);
        }
        if (jsonObject.has(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_ID)) {
            this.place_id = jsonObject.getString(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_PLACE_ID);
        }
        if (jsonObject.has("types")) {
            this.types = jsonObject.getJSONArray("types").toString().split(org.droidparts.contract.SQL.DDL.SEPARATOR);
        }
        if (jsonObject.has("price_level")) {
            this.price_level = jsonObject.getString("price_level");
        }
        if (jsonObject.has("rating")) {
            this.rating = jsonObject.getString("rating");
        }
        jsonObject.remove("reviews");
        jsonObject.remove("photos");
        jsonObject.remove("reference");
        this.jsonString = jsonObject.toString();
    }

    public java.lang.String toString() {
        return java.lang.String.format("Name: %s,\tLat: %s,\tLng: %s,\tAddress: %s,\tstreetNumber: %s,\tstreetName: %s,\tcity: %s,\tstate: %s,\tzipCode: %s,\tcountry: %s (%s),\tSuggestionType: %s,\tRating: %s,\tPrice Level: %s,\tPlace Id: %s,\tOpen Hours: %s,\tOpen Now: %s,\tPhone: %s,\tUrl: %s", new java.lang.Object[]{this.name, this.lat, this.lng, this.address, this.streetNumber, this.streetName, this.city, this.state, this.zipCode, this.country, this.countryCode, java.util.Arrays.toString(this.types), this.rating, this.price_level, this.place_id, this.open_hours, java.lang.Boolean.valueOf(this.open_now), this.phone, this.url});
    }

    public void toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType searchResultType, com.navdy.client.app.framework.models.Destination destination) {
        destination.name = this.name;
        double newDisplayLat = java.lang.Double.parseDouble(this.lat);
        double newDisplayLong = java.lang.Double.parseDouble(this.lng);
        if (!(newDisplayLat == destination.displayLat && newDisplayLong == destination.displayLong)) {
            destination.navigationLat = 0.0d;
            destination.navigationLong = 0.0d;
        }
        destination.displayLat = newDisplayLat;
        destination.displayLong = newDisplayLong;
        destination.rawAddressNotForDisplay = this.address;
        destination.streetNumber = this.streetNumber;
        destination.streetName = this.streetName;
        destination.city = this.city;
        destination.state = this.state;
        destination.zipCode = this.zipCode;
        destination.country = this.country;
        destination.setCountryCode(this.countryCode);
        destination.placeId = this.place_id;
        destination.searchResultType = searchResultType.getValue();
        destination.placeDetailJson = this.jsonString;
        destination.type = getType(this.types);
    }

    private com.navdy.client.app.framework.models.Destination.Type getType(java.lang.String[] types2) {
        com.navdy.client.app.framework.models.Destination.Type destinationType = com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
        if (types2 == null || types2.length <= 0) {
            return destinationType;
        }
        try {
            return parseTypes(new org.json.JSONArray(types2[0]));
        } catch (org.json.JSONException e) {
            e.printStackTrace();
            return destinationType;
        }
    }

    public static com.navdy.client.app.framework.models.Destination.Type parseTypes(org.json.JSONArray array) throws org.json.JSONException {
        com.navdy.client.app.framework.models.Destination.Type destinationType = com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
        if (array.length() <= 0) {
            return destinationType;
        }
        for (int i = 0; i < array.length(); i++) {
            com.navdy.client.app.framework.models.Destination.Type tempType = getType((java.lang.String) array.get(i));
            if (tempType != com.navdy.client.app.framework.models.Destination.Type.UNKNOWN && tempType != com.navdy.client.app.framework.models.Destination.Type.PLACE) {
                return tempType;
            }
            if (destinationType == com.navdy.client.app.framework.models.Destination.Type.UNKNOWN) {
                destinationType = tempType;
            }
        }
        return destinationType;
    }

    @android.support.annotation.NonNull
    private static com.navdy.client.app.framework.models.Destination.Type getType(@android.support.annotation.NonNull java.lang.String type) {
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "accounting")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "airport")) {
            return com.navdy.client.app.framework.models.Destination.Type.AIRPORT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "amusement_park")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "aquarium")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "art_gallery")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "atm")) {
            return com.navdy.client.app.framework.models.Destination.Type.ATM;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bakery")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bank")) {
            return com.navdy.client.app.framework.models.Destination.Type.BANK;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bar")) {
            return com.navdy.client.app.framework.models.Destination.Type.BAR;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "beauty_salon")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bicycle_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "book_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bowling_alley")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bus_station")) {
            return com.navdy.client.app.framework.models.Destination.Type.TRANSIT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "cafe")) {
            return com.navdy.client.app.framework.models.Destination.Type.COFFEE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "campground")) {
            return com.navdy.client.app.framework.models.Destination.Type.PARK;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_dealer")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_rental")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_repair")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_wash")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "casino")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "cemetery")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "church")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "city_hall")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "clothing_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "convenience_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "courthouse")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "dentist")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "department_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "doctor")) {
            return com.navdy.client.app.framework.models.Destination.Type.HOSPITAL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "electrician")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "electronics_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "embassy")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "establishment")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "finance")) {
            return com.navdy.client.app.framework.models.Destination.Type.BANK;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "fire_station")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "florist")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "food")) {
            return com.navdy.client.app.framework.models.Destination.Type.RESTAURANT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "funeral_home")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "furniture_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "gas_station")) {
            return com.navdy.client.app.framework.models.Destination.Type.GAS_STATION;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "general_contractor")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "grocery_or_supermarket")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "gym")) {
            return com.navdy.client.app.framework.models.Destination.Type.GYM;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hair_care")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hardware_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "health")) {
            return com.navdy.client.app.framework.models.Destination.Type.HOSPITAL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hindu_temple")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "home_goods_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hospital")) {
            return com.navdy.client.app.framework.models.Destination.Type.HOSPITAL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "insurance_agency")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "jewelry_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "laundry")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "lawyer")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "library")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "liquor_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "local_government_office")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "locksmith")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "lodging")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "meal_delivery")) {
            return com.navdy.client.app.framework.models.Destination.Type.RESTAURANT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "meal_takeaway")) {
            return com.navdy.client.app.framework.models.Destination.Type.RESTAURANT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "mosque")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "movie_rental")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "movie_theater")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "moving_company")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "museum")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "night_club")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "painter")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "park")) {
            return com.navdy.client.app.framework.models.Destination.Type.PARK;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "parking")) {
            return com.navdy.client.app.framework.models.Destination.Type.PARKING;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "pet_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "pharmacy")) {
            return com.navdy.client.app.framework.models.Destination.Type.HOSPITAL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "physiotherapist")) {
            return com.navdy.client.app.framework.models.Destination.Type.HOSPITAL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "place_of_worship")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "plumber")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "police")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "post_office")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "real_estate_agency")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "restaurant")) {
            return com.navdy.client.app.framework.models.Destination.Type.RESTAURANT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "roofing_contractor")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "rv_park")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "school")) {
            return com.navdy.client.app.framework.models.Destination.Type.SCHOOL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "shoe_store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "shopping_mall")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "spa")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "stadium")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "street_address")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "storage")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "store")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "subway_station")) {
            return com.navdy.client.app.framework.models.Destination.Type.TRANSIT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "synagogue")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "taxi_stand")) {
            return com.navdy.client.app.framework.models.Destination.Type.TRANSIT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "train_station")) {
            return com.navdy.client.app.framework.models.Destination.Type.TRANSIT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "transit_station")) {
            return com.navdy.client.app.framework.models.Destination.Type.TRANSIT;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "travel_agency")) {
            return com.navdy.client.app.framework.models.Destination.Type.STORE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "university")) {
            return com.navdy.client.app.framework.models.Destination.Type.SCHOOL;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "veterinary_care")) {
            return com.navdy.client.app.framework.models.Destination.Type.PLACE;
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "zoo")) {
            return com.navdy.client.app.framework.models.Destination.Type.ENTERTAINMENT;
        }
        return com.navdy.client.app.framework.models.Destination.Type.UNKNOWN;
    }
}
