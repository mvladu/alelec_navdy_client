package com.navdy.client.app.framework.location;

public final class NavdyLocationManager {
    private static final long COUNTRY_CODE_REFRESH_RATE = java.util.concurrent.TimeUnit.DAYS.toMillis(1);
    private static final java.lang.String FAKE_CAR_LOCATION_ACTION = "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION";
    private static final java.lang.String FAKE_CAR_LOCATION_ACTION_CANCEL = "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION_CANCEL";
    private static final java.lang.String FAKE_PHONE_LOCATION_ACTION = "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION";
    private static final java.lang.String FAKE_PHONE_LOCATION_ACTION_CANCEL = "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION_CANCEL";
    private static final java.lang.String LATITUDE_PREFERENCE = "latitude";
    private static final java.lang.String LOCATION_ACCURACY_PREFERENCE = "accuracy";
    private static final java.lang.String LOCATION_PROVIDER_PREFERENCE = "provider";
    private static final java.lang.String LOCATION_TIME_PREFERENCE = "time";
    private static final java.lang.String LONGITUDE_PREFERENCE = "longitude";
    private static final long MAX_AGE_LOCATION_FIX = 30000;
    private static final float MIN_DISTANCE_LOCATION_CHANGE = 25.0f;
    private static final int MIN_TIME_LOCATION_CHANGE = 5000;
    private static final java.lang.String NAVDY_LOCATION_MANAGER_PREFS = "navdy_location_manager";
    private static final int SMART_START_LOCATION_LIMIT = 500;
    private static com.navdy.client.app.framework.location.NavdyLocationManager instance;
    private static java.lang.String lastKnownCountryCode = com.navdy.client.app.framework.i18n.AddressUtils.DEFAULT_COUNTRY_CODE;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.location.NavdyLocationManager.class);
    private static final java.lang.Object navdyLocationManagerLock = new java.lang.Object();
    private static long nextCountryCodeUpdate = 0;
    private java.util.List<java.lang.String> bestProviders;
    private android.location.Location currentCarLocation;
    private android.location.Location currentPhoneLocation;
    private boolean didProperlyInitialize;
    private final android.content.BroadcastReceiver fakeLocationReceiver = new com.navdy.client.app.framework.location.NavdyLocationManager.Anon2();
    private boolean isInjectingFakeCarLocations;
    private boolean isInjectingFakePhoneLocations;
    private final java.util.List<java.lang.ref.WeakReference<com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener>> listeners;
    private boolean listening;
    private final android.location.LocationListener locationListener = new com.navdy.client.app.framework.location.NavdyLocationManager.Anon1();
    private android.location.LocationManager locationManager;
    private final android.content.SharedPreferences preferences;

    class Anon1 implements android.location.LocationListener {
        Anon1() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        public void onLocationChanged(android.location.Location location) {
            synchronized (com.navdy.client.app.framework.location.NavdyLocationManager.navdyLocationManagerLock) {
                if (!com.navdy.client.app.framework.location.NavdyLocationManager.this.isInjectingFakePhoneLocations) {
                    if (com.navdy.client.app.framework.location.NavdyLocationManager.this.isRecentAndAccurateEnough(location)) {
                        com.navdy.client.app.framework.location.NavdyLocationManager.logger.v("update new location from locationListener: " + location);
                        synchronized (com.navdy.client.app.framework.location.NavdyLocationManager.navdyLocationManagerLock) {
                            com.navdy.client.app.framework.location.NavdyLocationManager.this.currentPhoneLocation = location;
                            com.navdy.client.app.framework.location.NavdyLocationManager.updateLastKnownCountryCode(com.navdy.client.app.framework.location.NavdyLocationManager.this.currentPhoneLocation);
                        }
                        com.navdy.client.app.framework.location.NavdyLocationManager.this.saveLocationToPreferences(location);
                        com.navdy.client.app.framework.location.NavdyLocationManager.this.callListenersForPhoneLocation(location);
                    }
                }
            }
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
        }

        public void onProviderEnabled(java.lang.String provider) {
        }

        public void onProviderDisabled(java.lang.String provider) {
        }
    }

    class Anon2 extends android.content.BroadcastReceiver {
        Anon2() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            com.navdy.client.app.framework.location.NavdyLocationManager.logger.w("non-debug build, sorry!");
        }
    }

    class Anon3 implements java.util.Comparator<android.location.Location> {
        Anon3() {
        }

        public int compare(android.location.Location lhs, android.location.Location rhs) {
            return (int) (com.navdy.client.app.framework.location.NavdyLocationManager.this.getLocationScore(lhs) - com.navdy.client.app.framework.location.NavdyLocationManager.this.getLocationScore(rhs));
        }
    }

    class Anon4 implements java.lang.Runnable {
        Anon4() {
        }

        /* JADX INFO: finally extract failed */
        public void run() {
            double navdyLatitude = 0.0d;
            double navdyLongitude = 0.0d;
            long time = 0;
            android.database.Cursor tripsCursor = null;
            try {
                tripsCursor = com.navdy.client.app.providers.NavdyContentProvider.getTripsCursor();
                if (tripsCursor != null && tripsCursor.moveToFirst()) {
                    com.navdy.client.app.framework.models.Trip trip = com.navdy.client.app.providers.NavdyContentProvider.getTripsItemAt(tripsCursor, tripsCursor.getPosition());
                    if (trip != null) {
                        if (trip.endLat == 0.0d && trip.endLong == 0.0d) {
                            navdyLatitude = trip.startLat;
                            navdyLongitude = trip.startLong;
                        } else {
                            navdyLatitude = trip.endLat;
                            navdyLongitude = trip.endLong;
                        }
                        time = trip.endTime;
                    }
                }
                com.navdy.service.library.util.IOUtils.closeStream(tripsCursor);
                android.location.Location carLocation = new android.location.Location("");
                carLocation.setLatitude(navdyLatitude);
                carLocation.setLongitude(navdyLongitude);
                carLocation.setTime(time);
                if (com.navdy.client.app.framework.location.NavdyLocationManager.this.isValidLocation(carLocation)) {
                    synchronized (com.navdy.client.app.framework.location.NavdyLocationManager.navdyLocationManagerLock) {
                        com.navdy.client.app.framework.location.NavdyLocationManager.this.currentCarLocation = carLocation;
                    }
                }
            } catch (Throwable th) {
                com.navdy.service.library.util.IOUtils.closeStream(tripsCursor);
                throw th;
            }
        }
    }

    static class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
        final /* synthetic */ long val$now;

        Anon5(com.navdy.client.app.framework.models.Destination destination, long j) {
            this.val$destination = destination;
            this.val$now = j;
        }

        public void run() {
            java.lang.String countryCode = this.val$destination.getCountryCode();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(countryCode)) {
                com.navdy.client.app.framework.location.NavdyLocationManager.lastKnownCountryCode = countryCode;
                com.navdy.client.app.framework.location.NavdyLocationManager.nextCountryCodeUpdate = this.val$now + com.navdy.client.app.framework.location.NavdyLocationManager.COUNTRY_CODE_REFRESH_RATE;
            }
        }
    }

    public static class CarLocationChangedEvent {
        public final com.navdy.service.library.events.location.Coordinate coordinate;

        public CarLocationChangedEvent(com.navdy.service.library.events.location.Coordinate coordinate2) {
            this.coordinate = coordinate2;
        }
    }

    public interface OnNavdyLocationChangedListener {
        void onCarLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate coordinate);

        void onPhoneLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate coordinate);
    }

    public static com.navdy.client.app.framework.location.NavdyLocationManager getInstance() {
        com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager;
        synchronized (navdyLocationManagerLock) {
            if (instance == null) {
                instance = new com.navdy.client.app.framework.location.NavdyLocationManager();
            }
            navdyLocationManager = instance;
        }
        return navdyLocationManager;
    }

    private NavdyLocationManager() {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        this.listeners = new java.util.ArrayList();
        this.preferences = context.getSharedPreferences(NAVDY_LOCATION_MANAGER_PREFS, 0);
        this.didProperlyInitialize = false;
        this.isInjectingFakePhoneLocations = false;
        this.isInjectingFakeCarLocations = false;
        getLastTripUpdateCoordinates();
        initLocationServices();
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        registerFakeLocationReceiver(context);
    }

    public void initLocationServices() {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        boolean hasPermissions = android.support.v4.content.ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0;
        synchronized (navdyLocationManagerLock) {
            if (!this.didProperlyInitialize && hasPermissions) {
                this.locationManager = (android.location.LocationManager) context.getSystemService("location");
                this.bestProviders = getBestAvailableLocationProviders();
                java.util.List<android.location.Location> candidateLocations = new java.util.ArrayList<>(3);
                if (this.locationManager != null) {
                    for (java.lang.String provider : this.bestProviders) {
                        android.location.Location lastKnownLocation = this.locationManager.getLastKnownLocation(provider);
                        if (lastKnownLocation != null) {
                            candidateLocations.add(lastKnownLocation);
                        }
                    }
                }
                java.lang.String provider2 = this.preferences.getString(LOCATION_PROVIDER_PREFERENCE, "");
                float lat = this.preferences.getFloat(LATITUDE_PREFERENCE, 0.0f);
                float lng = this.preferences.getFloat(LONGITUDE_PREFERENCE, 0.0f);
                long time = this.preferences.getLong(LOCATION_TIME_PREFERENCE, 0);
                float accuracy = this.preferences.getFloat(LOCATION_ACCURACY_PREFERENCE, Float.MAX_VALUE);
                android.location.Location savedLocation = new android.location.Location(provider2);
                savedLocation.setLatitude((double) lat);
                savedLocation.setLongitude((double) lng);
                savedLocation.setTime(time);
                savedLocation.setAccuracy(accuracy);
                logger.v("initLocationServices, taken from prefs: " + savedLocation);
                candidateLocations.add(savedLocation);
                java.util.Collections.sort(candidateLocations, new com.navdy.client.app.framework.location.NavdyLocationManager.Anon3());
                this.currentPhoneLocation = (android.location.Location) candidateLocations.get(0);
                saveLocationToPreferences(this.currentPhoneLocation);
                this.didProperlyInitialize = true;
                logger.v("Properly initialized, currentPhoneLocation=" + this.currentPhoneLocation);
            } else if (!hasPermissions) {
                logger.w("Error with location permissions!");
            }
        }
    }

    public void addListener(@android.support.annotation.NonNull com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener navdyLocationListener) {
        synchronized (navdyLocationManagerLock) {
            this.listeners.add(new java.lang.ref.WeakReference(navdyLocationListener));
            if (!this.listening) {
                try {
                    if (android.support.v4.content.ContextCompat.checkSelfPermission(com.navdy.client.app.NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0 && this.locationManager != null) {
                        for (java.lang.String provider : this.bestProviders) {
                            this.locationManager.requestLocationUpdates(provider, com.here.odnp.config.OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL, MIN_DISTANCE_LOCATION_CHANGE, this.locationListener, android.os.Looper.getMainLooper());
                        }
                        this.listening = true;
                        logger.v("location listener installed[" + this.bestProviders + "]" + java.lang.System.identityHashCode(this.locationListener));
                    }
                } catch (Throwable t) {
                    logger.e(t);
                }
            }
        }
    }

    public void removeListener(com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener navdyLocationListener) {
        synchronized (navdyLocationManagerLock) {
            java.util.ListIterator<java.lang.ref.WeakReference<com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener>> iterator = this.listeners.listIterator();
            while (iterator.hasNext()) {
                com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener listener = (com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener) ((java.lang.ref.WeakReference) iterator.next()).get();
                if (listener == null || listener.equals(navdyLocationListener)) {
                    iterator.remove();
                }
            }
            if (this.listening && this.listeners.size() == 0) {
                logger.v("stopListeningForLocationChange");
                try {
                    if (android.support.v4.content.ContextCompat.checkSelfPermission(com.navdy.client.app.NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
                        this.locationManager.removeUpdates(this.locationListener);
                        logger.v("location listener removed:" + java.lang.System.identityHashCode(this.locationListener));
                    }
                } catch (Throwable t) {
                    logger.e(t);
                }
                this.listening = false;
            }
        }
    }

    @android.support.annotation.Nullable
    public android.location.Location getSmartStartLocation() {
        android.location.Location carLocation;
        android.location.Location phoneLocation;
        synchronized (navdyLocationManagerLock) {
            carLocation = this.currentCarLocation;
            phoneLocation = this.currentPhoneLocation;
        }
        if (carLocation == null && phoneLocation == null) {
            logger.v("Both car and phone location are null :'(");
            return null;
        } else if (carLocation == null) {
            logger.v("Using phone location because car location is null");
            return phoneLocation;
        } else if (phoneLocation == null) {
            logger.v("Using car location because phone location is null");
            return carLocation;
        } else if (com.navdy.client.app.framework.map.MapUtils.distanceBetween(phoneLocation, carLocation) <= 500.0f) {
            logger.v("Using car location because the car and the phone are close to each other.");
            return carLocation;
        } else if (phoneLocation.getTime() > carLocation.getTime()) {
            logger.v("Using phone location because car and phone are more than 500m apart and phone location is the most recent");
            return phoneLocation;
        } else {
            logger.v("Using car location because car and phone are more than 500m apart and car location is the most recent");
            return carLocation;
        }
    }

    @android.support.annotation.Nullable
    private android.location.Location getPhoneLocation() {
        android.location.Location location;
        synchronized (navdyLocationManagerLock) {
            location = this.currentPhoneLocation;
        }
        return location;
    }

    @android.support.annotation.Nullable
    private android.location.Location getCarLocation() {
        android.location.Location location;
        synchronized (navdyLocationManagerLock) {
            location = this.currentCarLocation;
        }
        return location;
    }

    @android.support.annotation.Nullable
    public com.navdy.service.library.events.location.Coordinate getSmartStartCoordinates() {
        return getCoordsFromLocation(getSmartStartLocation());
    }

    @android.support.annotation.Nullable
    public com.navdy.service.library.events.location.Coordinate getPhoneCoordinates() {
        return getCoordsFromLocation(getPhoneLocation());
    }

    @android.support.annotation.Nullable
    public com.navdy.service.library.events.location.Coordinate getCarCoordinates() {
        return getCoordsFromLocation(getCarLocation());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x004c, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r5.currentCarLocation = r0;
        updateLastKnownCountryCode(r5.currentCarLocation);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0054, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0055, code lost:
        callListenersForCarLocation(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r1 = r6.coordinate;
        r0 = new android.location.Location("gps");
        r0.setLatitude(r1.latitude.doubleValue());
        r0.setLongitude(r1.longitude.doubleValue());
        r0.setTime(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0030, code lost:
        if (isValidLocation(r0) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0032, code lost:
        logger.v("new car location: " + r0);
        r3 = navdyLocationManagerLock;
     */
    @com.squareup.otto.Subscribe
    public void onCarLocationChanged(com.navdy.client.app.framework.location.NavdyLocationManager.CarLocationChangedEvent event) {
        synchronized (navdyLocationManagerLock) {
            if (this.isInjectingFakeCarLocations) {
            }
        }
    }

    private void getLastTripUpdateCoordinates() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.location.NavdyLocationManager.Anon4(), 1);
    }

    private boolean isValidLocation(android.location.Location location) {
        return (location == null || (location.getLatitude() == 0.0d && location.getLongitude() == 0.0d)) ? false : true;
    }

    @android.support.annotation.NonNull
    private java.util.List<java.lang.String> getBestAvailableLocationProviders() {
        android.location.Criteria criteria = new android.location.Criteria();
        criteria.setAccuracy(2);
        criteria.setPowerRequirement(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setSpeedRequired(false);
        android.location.LocationManager locationManager2 = (android.location.LocationManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("location");
        java.util.List<java.lang.String> providers = null;
        if (locationManager2 != null) {
            providers = locationManager2.getProviders(criteria, true);
        }
        if (providers == null) {
            providers = new java.util.ArrayList<>();
        }
        if (providers.size() == 0) {
            providers.add("passive");
        }
        return providers;
    }

    @android.support.annotation.Nullable
    private com.navdy.service.library.events.location.Coordinate getCoordsFromLocation(@android.support.annotation.Nullable android.location.Location location) {
        if (location == null) {
            return null;
        }
        return com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(location.getLatitude(), location.getLongitude());
    }

    private boolean isRecentAndAccurateEnough(android.location.Location location) {
        boolean newLocationIsRecent;
        boolean newLocationIsAccurate;
        boolean isMoreAccurateThanCurrentLocation;
        boolean currentLocationIsTooOld;
        if (!com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(location)) {
            return false;
        }
        long now = java.lang.System.currentTimeMillis();
        if (now - location.getTime() < 30000) {
            newLocationIsRecent = true;
        } else {
            newLocationIsRecent = false;
        }
        if (location.getAccuracy() < 500.0f) {
            newLocationIsAccurate = true;
        } else {
            newLocationIsAccurate = false;
        }
        if (!newLocationIsRecent || !newLocationIsAccurate) {
            return false;
        }
        if (this.currentPhoneLocation == null) {
            return true;
        }
        if (this.currentPhoneLocation.getAccuracy() > location.getAccuracy()) {
            isMoreAccurateThanCurrentLocation = true;
        } else {
            isMoreAccurateThanCurrentLocation = false;
        }
        if (now - this.currentPhoneLocation.getTime() > 30000) {
            currentLocationIsTooOld = true;
        } else {
            currentLocationIsTooOld = false;
        }
        if (isMoreAccurateThanCurrentLocation || currentLocationIsTooOld) {
            return true;
        }
        return false;
    }

    private void callListenersForPhoneLocation(android.location.Location phoneLocation) {
        logger.v("phone location has changed to" + phoneLocation + ", calling listeners");
        com.navdy.service.library.events.location.Coordinate phoneCoords = getCoordsFromLocation(phoneLocation);
        if (phoneCoords != null) {
            for (java.lang.ref.WeakReference<com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener> listenerWeakReference : this.listeners) {
                com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener listener = (com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener) listenerWeakReference.get();
                if (listener != null) {
                    listener.onPhoneLocationChanged(phoneCoords);
                }
            }
        }
    }

    private void callListenersForCarLocation(android.location.Location carLocation) {
        com.navdy.service.library.events.location.Coordinate carCoords = getCoordsFromLocation(carLocation);
        if (carCoords != null) {
            for (java.lang.ref.WeakReference<com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener> listenerWeakReference : this.listeners) {
                com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener listener = (com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener) listenerWeakReference.get();
                if (listener != null) {
                    listener.onCarLocationChanged(carCoords);
                }
            }
        }
    }

    private void saveLocationToPreferences(android.location.Location location) {
        this.preferences.edit().putFloat(LATITUDE_PREFERENCE, (float) location.getLatitude()).putFloat(LONGITUDE_PREFERENCE, (float) location.getLongitude()).putLong(LOCATION_TIME_PREFERENCE, location.getTime()).putString(LOCATION_PROVIDER_PREFERENCE, location.getProvider()).putFloat(LOCATION_ACCURACY_PREFERENCE, location.getAccuracy()).apply();
    }

    private void registerFakeLocationReceiver(android.content.Context context) {
        android.content.IntentFilter intentFilter = new android.content.IntentFilter();
        intentFilter.addAction(FAKE_PHONE_LOCATION_ACTION);
        intentFilter.addAction(FAKE_PHONE_LOCATION_ACTION_CANCEL);
        intentFilter.addAction(FAKE_CAR_LOCATION_ACTION);
        intentFilter.addAction(FAKE_CAR_LOCATION_ACTION_CANCEL);
        context.registerReceiver(this.fakeLocationReceiver, intentFilter);
    }

    private double getLocationScore(@android.support.annotation.NonNull android.location.Location location) {
        return ((double) location.getAccuracy()) - (((double) location.getTime()) / 1000.0d);
    }

    public static java.lang.String getLastKnownCountryCode() {
        return lastKnownCountryCode;
    }

    public static void updateLastKnownCountryCode(android.location.Location currentLocation) {
        if (currentLocation != null) {
            long now = new java.util.Date().getTime();
            if (now >= nextCountryCodeUpdate) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                com.navdy.client.app.framework.map.MapUtils.doReverseGeocodingFor(new com.google.android.gms.maps.model.LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destination, new com.navdy.client.app.framework.location.NavdyLocationManager.Anon5(destination, now));
            }
        }
    }

    public static void forceLastKnownCountryCodeUpdate() {
        nextCountryCodeUpdate = 0;
    }
}
