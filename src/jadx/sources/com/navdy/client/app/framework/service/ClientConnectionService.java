package com.navdy.client.app.framework.service;

public class ClientConnectionService extends com.navdy.service.library.device.connection.ConnectionService {
    private final android.os.IBinder binder = new com.navdy.client.app.framework.service.ClientConnectionService.LocalBinder();
    private com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster deviceChangeBroadcaster = new com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster();
    private java.util.List<java.lang.Object> serviceHandlers;

    public static class DeviceChangeBroadcaster extends com.navdy.service.library.util.Listenable<com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Listener> {

        class Anon1 implements com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.EventDispatcher {
            final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$newDevice;

            Anon1(com.navdy.service.library.device.RemoteDevice remoteDevice) {
                this.val$newDevice = remoteDevice;
            }

            public void dispatchEvent(com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster source, com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Listener listener) {
                listener.onDeviceChanged(this.val$newDevice);
            }
        }

        interface EventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster, com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Listener> {
        }

        public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
            void onDeviceChanged(com.navdy.service.library.device.RemoteDevice remoteDevice);
        }

        void dispatchDeviceChangedEvent(com.navdy.service.library.device.RemoteDevice newDevice) {
            dispatchToListeners(new com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Anon1(newDevice));
        }
    }

    public class LocalBinder extends android.os.Binder {
        public LocalBinder() {
        }

        public com.navdy.client.app.framework.service.ClientConnectionService getService() {
            return com.navdy.client.app.framework.service.ClientConnectionService.this;
        }
    }

    public void addListener(com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Listener listener) {
        this.deviceChangeBroadcaster.addListener(listener);
    }

    public void removeListener(com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Listener listener) {
        this.deviceChangeBroadcaster.removeListener(listener);
    }

    public void onCreate() {
        super.onCreate();
        com.navdy.client.app.framework.servicehandler.ContactServiceHandler contactServiceHandler = com.navdy.client.app.framework.servicehandler.ContactServiceHandler.getInstance();
        this.serviceHandlers = new java.util.ArrayList();
        this.serviceHandlers.add(com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler.getInstance());
        this.serviceHandlers.add(new com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler(this));
        this.serviceHandlers.add(new com.navdy.client.app.framework.servicehandler.SpeechServiceHandler());
        this.serviceHandlers.add(com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.getInstance());
        this.serviceHandlers.add(com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance());
        this.serviceHandlers.add(contactServiceHandler);
        this.serviceHandlers.add(new com.navdy.client.app.framework.servicehandler.PhotoServiceHandler(this));
        this.serviceHandlers.add(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.getInstance());
        this.serviceHandlers.add(new com.navdy.client.app.framework.servicehandler.MessageServiceHandler());
        this.serviceHandlers.add(new com.navdy.client.app.framework.servicehandler.SettingsServiceHandler());
        this.serviceHandlers.add(com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler.getInstance());
        this.serviceHandlers.add(new com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler());
        if (com.navdy.client.app.ui.base.BaseActivity.weHaveContactsPermission()) {
            getContentResolver().registerContentObserver(android.provider.ContactsContract.Contacts.CONTENT_URI, true, new com.navdy.client.app.framework.util.ContactObserver(contactServiceHandler, this.serviceHandler));
        }
    }

    protected com.navdy.service.library.device.connection.ConnectionListener[] getConnectionListeners(android.content.Context context) {
        return new com.navdy.service.library.device.connection.ConnectionListener[]{new com.navdy.service.library.device.connection.AcceptorListener(context, new com.navdy.service.library.network.BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF)};
    }

    protected com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        return new com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster[0];
    }

    protected com.navdy.service.library.device.connection.ProxyService createProxyService() throws java.io.IOException {
        return new com.navdy.proxy.ProxyListener(new com.navdy.service.library.network.BTSocketAcceptor("Navdy-Proxy-Tunnel", NAVDY_PROXY_TUNNEL_UUID));
    }

    public com.navdy.service.library.device.RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        if (!getClass().getName().equals(intent.getAction())) {
            return super.onBind(intent);
        }
        this.logger.d("returning local binder");
        return this.binder;
    }

    protected void setRemoteDevice(com.navdy.service.library.device.RemoteDevice remoteDevice) {
        boolean deviceChanged = this.mRemoteDevice != remoteDevice;
        super.setRemoteDevice(remoteDevice);
        if (deviceChanged) {
            this.deviceChangeBroadcaster.dispatchDeviceChangedEvent(remoteDevice);
        }
    }
}
