package com.navdy.client.app.framework.models;

public class PhoneNumberModel {
    public java.lang.String customType;
    public boolean isPrimary;
    public java.lang.String number;
    public int type;

    public PhoneNumberModel(java.lang.String number2, int type2, java.lang.String customType2, boolean isPrimary2) {
        this.number = number2;
        this.type = type2;
        this.customType = customType2;
        this.isPrimary = isPrimary2;
    }

    boolean isFax() {
        return this.type == 5 || this.type == 4 || this.type == 13;
    }

    com.navdy.service.library.events.contacts.PhoneNumberType getPhoneNumberProtobufType() {
        switch (this.type) {
            case 1:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
            case 2:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_MOBILE;
            case 3:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_WORK;
            default:
                return com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_OTHER;
        }
    }

    public java.lang.String toString() {
        return "PhoneNumberModel{number='" + this.number + '\'' + ", type=" + this.type + ", customType='" + this.customType + '\'' + ", isPrimary=" + this.isPrimary + '}';
    }
}
