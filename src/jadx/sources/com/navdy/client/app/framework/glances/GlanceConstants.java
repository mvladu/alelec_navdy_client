package com.navdy.client.app.framework.glances;

public class GlanceConstants {
    public static final java.lang.String ACTION_EMAIL_GUESTS = "Email guests";
    public static final java.lang.String ACTION_MAP = "Map";
    public static final java.lang.String ANDROID_BIG_TEXT = "android.bigText";
    public static final java.lang.String ANDROID_CALENDAR = "com.android.calendar";
    public static final java.lang.String ANDROID_PEOPLE = "android.people";
    public static final java.lang.String ANDROID_SUB_TEXT = "android.subText";
    public static final java.lang.String ANDROID_SUMMARY_TEXT = "android.summaryText";
    public static final java.lang.String ANDROID_TEMPLATE = "android.template";
    public static final java.lang.String ANDROID_TEXT = "android.text";
    public static final java.lang.String ANDROID_TEXT_LINES = "android.textLines";
    public static final java.lang.String ANDROID_TITLE = "android.title";
    public static final java.lang.String APPLE_MUSIC = "com.apple.android.music";
    public static final java.lang.String CAR_EXT = "android.car.EXTENSIONS";
    public static final java.lang.String CAR_EXT_CONVERSATION = "car_conversation";
    public static final java.lang.String CAR_EXT_CONVERSATION_MESSAGES = "messages";
    public static final java.lang.String CAR_EXT_CONVERSATION_MESSAGES_AUTHOR = "author";
    public static final java.lang.String CAR_EXT_CONVERSATION_MESSAGES_TEXT = "text";
    public static final java.lang.String CAR_EXT_CONVERSATION_PARTICIPANTS = "participants";
    public static final java.lang.String COMMA = ",";
    public static final java.lang.String EMAIL_AT = "@";
    public static final java.lang.String EMPTY = "";
    public static final java.lang.String FACEBOOK = "com.facebook.katana";
    public static final java.lang.String FACEBOOK_MESSENGER = "com.facebook.orca";
    public static final java.lang.String FUEL_PACKAGE = "com.navdy.fuel";
    public static final boolean GLANCES_APP_GLANCES_ENABLED_DEFAULT = false;
    public static final boolean GLANCES_DRIVING_GLANCES_ENABLED_DEFAULT = true;
    public static final boolean GLANCES_OTHER_APPS_ENABLED_DEFAULT = false;
    public static final java.lang.String GOOGLE_CALENDAR = "com.google.android.calendar";
    public static final java.lang.String GOOGLE_HANGOUTS = "com.google.android.talk";
    public static final java.lang.String GOOGLE_INBOX = "com.google.android.apps.inbox";
    public static final java.lang.String GOOGLE_MAIL = "com.google.android.gm";
    public static final java.lang.String GOOGLE_MUSIC = "com.google.android.music";
    public static final java.lang.String MAIL_TO = "mailto:";
    public static final java.lang.String MICROSOFT_OUTLOOK = "com.microsoft.office.outlook";
    public static final java.lang.String MUSIC_PACKAGE = "com.navdy.music";
    public static final java.lang.String NAPSTER = "com.rhapsody";
    public static final java.lang.String NEW_LINE = "\n";
    public static final java.lang.String NOTIFICATION_INBOX_STYLE = "android.app.Notification$InboxStyle";
    public static final java.lang.String PANDORA = "com.pandora.android";
    public static final java.lang.String PHONE_PACKAGE = "com.navdy.phone";
    public static final java.lang.String SAMSUNG_MUSIC = "com.sec.android.app.music";
    public static final java.lang.String SLACK = "com.Slack";
    public static final java.lang.String SMS_PACKAGE = "com.navdy.sms";
    public static final java.lang.String SOUNDCLOUD = "com.soundcloud.android";
    public static final java.lang.String SPOTIFY = "com.spotify.music";
    public static final java.lang.String TRAFFIC_PACKAGE = "com.navdy.traffic";
    public static final java.lang.String TWITTER = "com.twitter.android";
    public static final java.lang.String WHATS_APP = "com.whatsapp";
    public static final java.lang.String YOUTUBE_MUSIC = "com.google.android.apps.youtube.music";
    private static final java.util.Map<com.navdy.client.app.framework.glances.GlanceConstants.Group, java.util.Set<java.lang.String>> groups = new com.navdy.client.app.framework.glances.GlanceConstants.Anon1();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.glances.GlanceConstants.class);

    static class Anon1 extends java.util.HashMap<com.navdy.client.app.framework.glances.GlanceConstants.Group, java.util.Set<java.lang.String>> {
        Anon1() {
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.DRIVING_GLANCES, new java.util.HashSet());
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.WHITE_LIST, new java.util.HashSet());
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.EMAIL_GROUP, new java.util.HashSet());
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.MESSAGING_GROUP, new java.util.HashSet());
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.CALENDAR_GROUP, new java.util.HashSet());
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.SOCIAL_GROUP, new java.util.HashSet());
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.MUSIC_GROUP, new java.util.HashSet());
            put(com.navdy.client.app.framework.glances.GlanceConstants.Group.IGNORE_GROUP, new java.util.HashSet());
        }
    }

    public enum Group {
        DRIVING_GLANCES,
        WHITE_LIST,
        EMAIL_GROUP,
        MESSAGING_GROUP,
        CALENDAR_GROUP,
        SOCIAL_GROUP,
        MUSIC_GROUP,
        IGNORE_GROUP
    }

    static {
        java.util.Set<java.lang.String> drivingGlances = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.DRIVING_GLANCES);
        java.util.Set<java.lang.String> whiteList = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.WHITE_LIST);
        java.util.Set<java.lang.String> emailGroup = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.EMAIL_GROUP);
        java.util.Set<java.lang.String> messagingGroup = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.MESSAGING_GROUP);
        java.util.Set<java.lang.String> calendarGroup = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.CALENDAR_GROUP);
        java.util.Set<java.lang.String> socialGroup = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.SOCIAL_GROUP);
        java.util.Set<java.lang.String> musicGroup = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.MUSIC_GROUP);
        java.util.Set<java.lang.String> ignoreGroup = (java.util.Set) groups.get(com.navdy.client.app.framework.glances.GlanceConstants.Group.IGNORE_GROUP);
        emailGroup.add(GOOGLE_MAIL);
        emailGroup.add(GOOGLE_INBOX);
        emailGroup.add(MICROSOFT_OUTLOOK);
        messagingGroup.add(SLACK);
        messagingGroup.add(GOOGLE_HANGOUTS);
        messagingGroup.add(WHATS_APP);
        messagingGroup.add(FACEBOOK_MESSENGER);
        calendarGroup.add(GOOGLE_CALENDAR);
        socialGroup.add(FACEBOOK);
        socialGroup.add(TWITTER);
        musicGroup.add(GOOGLE_MUSIC);
        musicGroup.add(PANDORA);
        musicGroup.add(SPOTIFY);
        musicGroup.add(APPLE_MUSIC);
        musicGroup.add(YOUTUBE_MUSIC);
        musicGroup.add(SAMSUNG_MUSIC);
        musicGroup.add(NAPSTER);
        musicGroup.add(SOUNDCLOUD);
        drivingGlances.add(PHONE_PACKAGE);
        drivingGlances.add(SMS_PACKAGE);
        drivingGlances.add(FUEL_PACKAGE);
        drivingGlances.add(MUSIC_PACKAGE);
        drivingGlances.add(TRAFFIC_PACKAGE);
        whiteList.add(GOOGLE_MAIL);
        whiteList.addAll(messagingGroup);
        whiteList.addAll(calendarGroup);
        whiteList.addAll(socialGroup);
        ignoreGroup.add("com.google.android.gms");
        ignoreGroup.add("com.android.mms");
        ignoreGroup.add("com.android.systemui");
    }

    public static boolean isPackageInGroup(java.lang.String packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group group) {
        java.util.Set<java.lang.String> packages = (java.util.Set) groups.get(group);
        if (packages != null) {
            return packages.contains(packageName);
        }
        logger.e("Cannot get package for group " + java.lang.String.valueOf(group));
        return false;
    }

    public static void addPackageToGroup(java.lang.String packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group group) {
        java.util.Set<java.lang.String> packages = (java.util.Set) groups.get(group);
        if (packages != null) {
            packages.add(packageName);
        } else {
            logger.e("Cannot get package for group " + java.lang.String.valueOf(group));
        }
    }
}
