package com.navdy.client.app.framework.map;

public final class HereGeocoder {
    private static final int MAX_COLLECTION_SIZE = 1;
    private static final java.lang.String ROAD_ACCESS_TYPE = "road";
    private static final android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.map.HereGeocoder.class);

    static class Anon1 implements com.here.android.mpa.common.OnEngineInitListener {
        final /* synthetic */ com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback val$requestCallback;
        final /* synthetic */ java.lang.String val$searchQuery;

        Anon1(com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback, java.lang.String str) {
            this.val$requestCallback = onGeocodeCompleteCallback;
            this.val$searchQuery = str;
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            if (error != com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                com.navdy.client.app.framework.map.HereGeocoder.logger.e("makeHereRequestForDestination, OnEngineInitListener error: " + error);
                this.val$requestCallback.onError(com.here.android.mpa.search.ErrorCode.NOT_INITIALIZED);
            } else if (this.val$searchQuery == null) {
                com.navdy.client.app.framework.map.HereGeocoder.logger.w("makeHereRequestForDestination, no searchQuery");
                this.val$requestCallback.onError(com.here.android.mpa.search.ErrorCode.BAD_REQUEST);
            } else {
                com.navdy.client.app.framework.map.HereGeocoder.makeRequestInternal(com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates(), this.val$searchQuery, this.val$requestCallback);
            }
        }
    }

    static class Anon2 implements com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.DiscoveryResultPage> {
        final /* synthetic */ com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback val$requestCallback;

        Anon2(com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback) {
            this.val$requestCallback = onGeocodeCompleteCallback;
        }

        public void onCompleted(com.here.android.mpa.search.DiscoveryResultPage discoveryResultPage, com.here.android.mpa.search.ErrorCode searchResponseError) {
            if (searchResponseError != com.here.android.mpa.search.ErrorCode.NONE) {
                this.val$requestCallback.onError(searchResponseError);
                return;
            }
            if (discoveryResultPage != null) {
                java.util.List<com.here.android.mpa.search.PlaceLink> placeLinks = discoveryResultPage.getPlaceLinks();
                if (placeLinks != null && placeLinks.size() > 0) {
                    com.here.android.mpa.search.PlaceLink placeLink = (com.here.android.mpa.search.PlaceLink) placeLinks.get(0);
                    if (placeLink != null) {
                        com.here.android.mpa.search.PlaceRequest detailsRequest = placeLink.getDetailsRequest();
                        if (detailsRequest != null) {
                            if (detailsRequest.execute(com.navdy.client.app.framework.map.HereGeocoder.getPlaceResultListener(this.val$requestCallback)) != com.here.android.mpa.search.ErrorCode.NONE) {
                                this.val$requestCallback.onError(searchResponseError);
                                return;
                            }
                            return;
                        }
                    }
                }
            }
            this.val$requestCallback.onError(searchResponseError);
        }
    }

    static class Anon3 implements com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.Place> {
        final /* synthetic */ com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback val$requestCallback;

        Anon3(com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback onGeocodeCompleteCallback) {
            this.val$requestCallback = onGeocodeCompleteCallback;
        }

        public void onCompleted(com.here.android.mpa.search.Place place, com.here.android.mpa.search.ErrorCode placeResponseError) {
            if (placeResponseError != com.here.android.mpa.search.ErrorCode.NONE) {
                this.val$requestCallback.onError(placeResponseError);
                return;
            }
            if (place != null) {
                com.here.android.mpa.search.Location location = place.getLocation();
                if (location != null) {
                    com.here.android.mpa.common.GeoCoordinate displayCoord = location.getCoordinate();
                    com.here.android.mpa.common.GeoCoordinate navCoord = null;
                    java.util.List<com.here.android.mpa.search.NavigationPosition> accessPoints = location.getAccessPoints();
                    if (accessPoints != null) {
                        for (com.here.android.mpa.search.NavigationPosition accessPoint : accessPoints) {
                            if (accessPoint.getAccessType() != null && accessPoint.getAccessType().equals(com.navdy.client.app.framework.map.HereGeocoder.ROAD_ACCESS_TYPE)) {
                                navCoord = accessPoint.getCoordinate();
                            }
                        }
                    }
                    com.navdy.client.app.framework.models.Destination.Precision precision = com.navdy.client.app.framework.models.Destination.Precision.PRECISE;
                    java.util.List<com.here.android.mpa.search.Category> categories = place.getCategories();
                    if (categories != null) {
                        java.util.Iterator it = categories.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            java.lang.String id = ((com.here.android.mpa.search.Category) it.next()).getId();
                            if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(id, "administrative-region") && !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(id, "city-town-village")) {
                                if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(id, "postal-area")) {
                                    break;
                                }
                            } else {
                                precision = com.navdy.client.app.framework.models.Destination.Precision.IMPRECISE;
                            }
                        }
                        precision = com.navdy.client.app.framework.models.Destination.Precision.IMPRECISE;
                    }
                    if (displayCoord != null) {
                        this.val$requestCallback.onComplete(displayCoord, navCoord, location.getAddress(), precision);
                        return;
                    } else {
                        this.val$requestCallback.onError(placeResponseError);
                        return;
                    }
                }
            }
            this.val$requestCallback.onError(placeResponseError);
        }
    }

    public interface OnGeocodeCompleteCallback {
        void onComplete(@android.support.annotation.NonNull com.here.android.mpa.common.GeoCoordinate geoCoordinate, @android.support.annotation.Nullable com.here.android.mpa.common.GeoCoordinate geoCoordinate2, @android.support.annotation.Nullable com.here.android.mpa.search.Address address, com.navdy.client.app.framework.models.Destination.Precision precision);

        void onError(@android.support.annotation.NonNull com.here.android.mpa.search.ErrorCode errorCode);
    }

    private static class TimeoutWrappedResultListener<T> implements com.here.android.mpa.search.ResultListener<T> {
        private static final long GEOCODE_REQUEST_TIMEOUT_MILLIS = 30000;
        boolean cancelled;
        public boolean complete;
        private final com.here.android.mpa.search.ResultListener<T> listener;
        private final java.lang.Runnable timeout = new com.navdy.client.app.framework.map.HereGeocoder.TimeoutWrappedResultListener.Anon1();

        class Anon1 implements java.lang.Runnable {
            Anon1() {
            }

            public void run() {
                synchronized (com.navdy.client.app.framework.map.HereGeocoder.TimeoutWrappedResultListener.this) {
                    if (com.navdy.client.app.framework.map.HereGeocoder.TimeoutWrappedResultListener.this.complete) {
                        com.navdy.client.app.framework.map.HereGeocoder.logger.v("Geocode call already complete");
                    } else {
                        com.navdy.client.app.framework.map.HereGeocoder.TimeoutWrappedResultListener.this.cancelled = true;
                        com.navdy.client.app.framework.map.HereGeocoder.TimeoutWrappedResultListener.this.listener.onCompleted(null, com.here.android.mpa.search.ErrorCode.CANCELLED);
                        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Debug.HERE_GEOCODE_TIMEOUT);
                        com.navdy.client.app.framework.map.HereGeocoder.logger.w("Geocode call timed out");
                    }
                }
            }
        }

        TimeoutWrappedResultListener(com.here.android.mpa.search.ResultListener<T> listener2) {
            this.listener = listener2;
            com.navdy.client.app.framework.map.HereGeocoder.handler.postDelayed(this.timeout, 30000);
        }

        public void onCompleted(T locations, com.here.android.mpa.search.ErrorCode errorCode) {
            synchronized (this) {
                com.navdy.client.app.framework.map.HereGeocoder.handler.removeCallbacks(this.timeout);
                this.complete = true;
                if (this.cancelled) {
                    com.navdy.client.app.framework.map.HereGeocoder.logger.e("Geocode call completed after timeout, error: " + errorCode);
                    com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Debug.HERE_GEOCODE_COMPLETED_AFTER_TIMEOUT);
                } else {
                    com.navdy.client.app.framework.map.HereGeocoder.logger.v("Geocode call completed successfully");
                    this.listener.onCompleted(locations, errorCode);
                }
            }
        }
    }

    private HereGeocoder() {
    }

    public static void makeRequest(java.lang.String searchQuery, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback requestCallback) {
        if (!com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
            requestCallback.onError(com.here.android.mpa.search.ErrorCode.NETWORK_COMMUNICATION);
        }
        com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.app.framework.map.HereGeocoder.Anon1(requestCallback, searchQuery));
    }

    private static void makeRequestInternal(com.navdy.service.library.events.location.Coordinate coordinate, java.lang.String searchQuery, com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback requestCallback) {
        if (requestCallback == null) {
            logger.w("makeHereRequestForDestination, no callback. Exiting");
        } else if (coordinate == null) {
            logger.w("makeHereRequestForDestination, no coordinates. Calling onError");
            requestCallback.onError(com.here.android.mpa.search.ErrorCode.BAD_LOCATION);
        } else {
            logger.v("makeHereRequestForDestination, searchQuery[" + searchQuery + "]" + " coordinate[" + coordinate.latitude + org.droidparts.contract.SQL.DDL.SEPARATOR + coordinate.longitude + "]");
            com.here.android.mpa.search.ErrorCode searchRequestError = new com.here.android.mpa.search.SearchRequest(searchQuery).setSearchCenter(new com.here.android.mpa.common.GeoCoordinate(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue())).setCollectionSize(1).execute(getSearchResultsListener(requestCallback));
            if (searchRequestError != com.here.android.mpa.search.ErrorCode.NONE) {
                requestCallback.onError(searchRequestError);
            }
        }
    }

    private static com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.DiscoveryResultPage> getSearchResultsListener(com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback requestCallback) {
        return new com.navdy.client.app.framework.map.HereGeocoder.TimeoutWrappedResultListener(new com.navdy.client.app.framework.map.HereGeocoder.Anon2(requestCallback));
    }

    /* access modifiers changed from: private */
    @android.support.annotation.NonNull
    public static com.here.android.mpa.search.ResultListener<com.here.android.mpa.search.Place> getPlaceResultListener(com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback requestCallback) {
        return new com.navdy.client.app.framework.map.HereGeocoder.TimeoutWrappedResultListener(new com.navdy.client.app.framework.map.HereGeocoder.Anon3(requestCallback));
    }
}
