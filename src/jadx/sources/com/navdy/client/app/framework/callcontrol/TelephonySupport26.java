package com.navdy.client.app.framework.callcontrol;

public class TelephonySupport26 extends com.navdy.client.app.framework.callcontrol.TelephonySupport21 {
    private static com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport26.class);

    public TelephonySupport26(android.content.Context context) {
        super(context);
    }

    @android.annotation.TargetApi(26)
    public void acceptRingingCall() {
        android.telecom.TelecomManager telecomManager = (android.telecom.TelecomManager) this.context.getSystemService("telecom");
        if (telecomManager == null) {
            sLogger.e("Unable to handleCallAction. telecomManager is null");
            acceptRingingCallFallback();
            return;
        }
        try {
            telecomManager.getClass().getMethod("acceptRingingCall", new java.lang.Class[0]).invoke(telecomManager, new java.lang.Object[0]);
        } catch (java.lang.Exception e) {
            sLogger.e("Unable to use the Telecom Manager directly.", e);
            acceptRingingCallFallback();
        }
    }

    private void acceptRingingCallFallback() {
        super.acceptRingingCall();
    }

    public void endCall() {
        if (!callTelephonyManagerMethod("endCall")) {
            super.endCall();
        }
    }

    private boolean callTelephonyManagerMethod(java.lang.String methodName) {
        android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) this.context.getSystemService("phone");
        if (tm == null) {
            sLogger.e("Unable to get the Telephony Manager.");
            return false;
        }
        try {
            tm.getClass().getMethod(methodName, new java.lang.Class[0]).invoke(tm, new java.lang.Object[0]);
            return true;
        } catch (java.lang.Exception e) {
            sLogger.e("Unable to use the Telephony Manager directly.", e);
            return false;
        }
    }
}
