package com.navdy.client.app.framework.util;

public class ContactImageHelper {
    public static final int DEFAULT_IMAGE_INDEX = -1;
    public static final int NO_CONTACT_COLOR = android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.grey);
    public static final int NO_CONTACT_IMAGE = 2130837731;
    private static final java.util.Map<java.lang.Integer, java.lang.Integer> sContactColorMap = new java.util.HashMap();
    private static final java.util.Map<java.lang.Integer, java.lang.Integer> sContactImageMap = new java.util.HashMap();
    private static final com.navdy.client.app.framework.util.ContactImageHelper sInstance = new com.navdy.client.app.framework.util.ContactImageHelper();

    static {
        sContactColorMap.put(java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_0)));
        int counter = 0 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(0), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_0));
        sContactColorMap.put(java.lang.Integer.valueOf(counter), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_1)));
        int counter2 = counter + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_1));
        sContactColorMap.put(java.lang.Integer.valueOf(counter2), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_2)));
        int counter3 = counter2 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter2), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_2));
        sContactColorMap.put(java.lang.Integer.valueOf(counter3), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_3)));
        int counter4 = counter3 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter3), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_3));
        sContactColorMap.put(java.lang.Integer.valueOf(counter4), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_4)));
        int counter5 = counter4 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter4), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_4));
        sContactColorMap.put(java.lang.Integer.valueOf(counter5), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_5)));
        int counter6 = counter5 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter5), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_5));
        sContactColorMap.put(java.lang.Integer.valueOf(counter6), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_6)));
        int counter7 = counter6 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter6), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_6));
        sContactColorMap.put(java.lang.Integer.valueOf(counter7), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_7)));
        int counter8 = counter7 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter7), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_7));
        sContactColorMap.put(java.lang.Integer.valueOf(counter8), java.lang.Integer.valueOf(android.support.v4.content.ContextCompat.getColor(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.R.color.icon_user_bg_8)));
        int i = counter8 + 1;
        sContactImageMap.put(java.lang.Integer.valueOf(counter8), java.lang.Integer.valueOf(com.navdy.client.R.drawable.icon_user_bg_8));
    }

    public static com.navdy.client.app.framework.util.ContactImageHelper getInstance() {
        return sInstance;
    }

    public int getContactImageIndex(java.lang.String contactId) {
        if (contactId == null) {
            return -1;
        }
        return java.lang.Math.abs(normalizeToFilename(contactId).hashCode() % sContactImageMap.size());
    }

    public int getResourceId(int index) {
        if (index < 0 || index >= sContactImageMap.size()) {
            return com.navdy.client.R.drawable.ic_search_no_profile;
        }
        return ((java.lang.Integer) sContactImageMap.get(java.lang.Integer.valueOf(index))).intValue();
    }

    public int getResourceColor(int index) {
        if (index < 0 || index >= sContactColorMap.size()) {
            return NO_CONTACT_COLOR;
        }
        return ((java.lang.Integer) sContactColorMap.get(java.lang.Integer.valueOf(index))).intValue();
    }

    public int getDriverImageResId(java.lang.String deviceName) {
        return getResourceId(java.lang.Math.abs(normalizeToFilename(deviceName).hashCode() % sContactImageMap.size()));
    }

    public static java.lang.String normalizeToFilename(java.lang.String str) {
        return com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(str) ? str : str.replaceAll("[^a-zA-Z0-9\\._]+", "");
    }
}
