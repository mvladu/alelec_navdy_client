package com.navdy.client.app.framework.servicehandler;

public class MusicServiceHandler {
    public static final com.navdy.service.library.events.audio.MusicTrackInfo EMPTY_TRACK_INFO = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().dataSource(com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE).playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).build();
    public static final int INDEX_PLAY_LIST_RETRY_INTERVAL = 10000;
    private static final int MAX_RETRIES_TO_INDEX_PLAY_LISTS = 5;
    public static final int PLAY_LIST_INDEXING_DELAY_MILLIS = 5000;
    private static final int THROTTLING_LOGGER_WAIT = 5000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.MusicServiceHandler.class);
    private static com.navdy.client.app.framework.servicehandler.MusicServiceHandler singleton = null;
    private static final com.navdy.client.app.framework.util.ThrottlingLogger throttlingLogger = new com.navdy.client.app.framework.util.ThrottlingLogger(sLogger, com.here.odnp.config.OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL);
    private java.lang.String currentArtworkHash;
    private android.graphics.Bitmap currentMediaArtwork = null;
    @android.support.annotation.NonNull
    private com.navdy.service.library.events.audio.MusicTrackInfo currentMediaTrackInfo = EMPTY_TRACK_INFO;
    private volatile int indexingRetries = 0;
    private android.os.Handler musicObserverHandler;
    private android.os.Looper musicObserverLooper;
    private com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper musicSeekHelper = null;
    private boolean postPhotoUpdates = false;
    private java.lang.Runnable retryIndexingPlayListsRunnable = new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon1();
    private boolean shouldPerformFullPlaylistIndex = true;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.indexingRetries = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.indexingRetries + 1;
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Retrying initialization of the playlist data base");
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.indexAllPlaylists(true);
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.indexAllPlaylists(true);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicCollectionRequest val$request;

        Anon3(com.navdy.service.library.events.audio.MusicCollectionRequest musicCollectionRequest) {
            this.val$request = musicCollectionRequest;
        }

        /* JADX WARNING: Removed duplicated region for block: B:161:0x05b9  */
        /* JADX WARNING: Removed duplicated region for block: B:211:0x0707  */
        /* JADX WARNING: Removed duplicated region for block: B:248:0x0819  */
        public void run() {
            android.database.Cursor membersCursor;
            android.database.Cursor membersCursor2;
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.d("onMusicCollectionRequest " + this.val$request);
            int offset = this.val$request.offset != null ? this.val$request.offset.intValue() : 0;
            int limit = this.val$request.limit != null ? this.val$request.limit.intValue() : 0;
            int count = 0;
            boolean canShuffle = true;
            java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> collectionList = null;
            java.util.ArrayList arrayList = null;
            java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> characterMap = null;
            if (com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(this.val$request.collectionSource)) {
                if (this.val$request.collectionId != null) {
                    if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(this.val$request.collectionType)) {
                        android.database.Cursor membersCursor3 = com.navdy.client.app.framework.util.MusicDbUtils.getPlaylistMembersCursor(this.val$request.collectionId);
                        if (membersCursor3 != null) {
                            try {
                                if (membersCursor3.moveToPosition(offset)) {
                                    count = membersCursor3.getCount();
                                    if (limit == 0) {
                                        limit = count;
                                    }
                                    java.util.ArrayList arrayList2 = new java.util.ArrayList(limit);
                                    do {
                                        try {
                                            arrayList2.add(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS).collectionId(this.val$request.collectionId).playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).name(membersCursor3.getString(membersCursor3.getColumnIndex("title"))).author(membersCursor3.getString(membersCursor3.getColumnIndex("artist"))).album(membersCursor3.getString(membersCursor3.getColumnIndex("album"))).trackId(java.lang.String.valueOf(membersCursor3.getInt(membersCursor3.getColumnIndex("SourceId")))).build());
                                            if (!membersCursor3.moveToNext()) {
                                                break;
                                            }
                                        } catch (Throwable th) {
                                            th = th;
                                            java.util.ArrayList arrayList3 = arrayList2;
                                            com.navdy.service.library.util.IOUtils.closeObject(membersCursor3);
                                            throw th;
                                        }
                                    } while (arrayList2.size() < limit);
                                    arrayList = arrayList2;
                                }
                            } catch (Throwable th2) {
                                t = th2;
                                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error querying GPM database: " + t);
                                com.navdy.service.library.util.IOUtils.closeObject(membersCursor3);
                                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicCollectionResponse.Builder().collectionInfo(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(java.lang.Integer.valueOf(count)).canShuffle(java.lang.Boolean.valueOf(canShuffle)).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
                            }
                        }
                        com.navdy.service.library.util.IOUtils.closeObject(membersCursor3);
                    } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(this.val$request.collectionType)) {
                        boolean groupByAlbums = com.squareup.wire.Wire.get(this.val$request.groupBy, com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN) == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS;
                        android.database.Cursor albumsCursor = com.navdy.client.app.framework.util.MusicDbUtils.getArtistsAlbums(this.val$request.collectionId);
                        android.database.Cursor artistTracksCursor = null;
                        if (groupByAlbums) {
                            if (albumsCursor != null) {
                                try {
                                    if (albumsCursor.moveToPosition(offset)) {
                                        count = albumsCursor.getCount();
                                        if (limit == 0) {
                                            limit = count;
                                        }
                                        java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> collectionList2 = new java.util.ArrayList<>(limit);
                                        do {
                                            try {
                                                collectionList2.add(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(java.lang.String.valueOf(albumsCursor.getString(albumsCursor.getColumnIndex("_id")))).name(albumsCursor.getString(albumsCursor.getColumnIndex("album"))).size(java.lang.Integer.valueOf(albumsCursor.getInt(albumsCursor.getColumnIndex("numsongs")))).build());
                                                if (!albumsCursor.moveToNext()) {
                                                    break;
                                                }
                                            } catch (Throwable th3) {
                                                th = th3;
                                                java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> list = collectionList2;
                                                com.navdy.service.library.util.IOUtils.closeObject(albumsCursor);
                                                com.navdy.service.library.util.IOUtils.closeObject(artistTracksCursor);
                                                throw th;
                                            }
                                        } while (collectionList2.size() < limit);
                                        collectionList = collectionList2;
                                    }
                                } catch (Throwable th4) {
                                    t = th4;
                                    com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                                    com.navdy.service.library.util.IOUtils.closeObject(albumsCursor);
                                    com.navdy.service.library.util.IOUtils.closeObject(artistTracksCursor);
                                    com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicCollectionResponse.Builder().collectionInfo(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(java.lang.Integer.valueOf(count)).canShuffle(java.lang.Boolean.valueOf(canShuffle)).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
                                }
                            }
                            canShuffle = com.navdy.client.app.framework.util.MusicDbUtils.getArtistSongsCount(this.val$request.collectionId) > 1;
                        } else {
                            artistTracksCursor = com.navdy.client.app.framework.util.MusicDbUtils.getArtistMembersCursor(this.val$request.collectionId);
                            if (artistTracksCursor != null && artistTracksCursor.moveToPosition(offset)) {
                                count = artistTracksCursor.getCount();
                                if (limit == 0) {
                                    limit = count;
                                }
                                java.util.ArrayList arrayList4 = new java.util.ArrayList(limit);
                                do {
                                    try {
                                        arrayList4.add(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS).collectionId(this.val$request.collectionId).playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).name(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("title"))).author(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("artist"))).album(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("album"))).trackId(java.lang.String.valueOf(artistTracksCursor.getInt(artistTracksCursor.getColumnIndex("_id")))).build());
                                        if (!artistTracksCursor.moveToNext()) {
                                            break;
                                        }
                                    } catch (Throwable th5) {
                                        th = th5;
                                        java.util.ArrayList arrayList5 = arrayList4;
                                        com.navdy.service.library.util.IOUtils.closeObject(albumsCursor);
                                        com.navdy.service.library.util.IOUtils.closeObject(artistTracksCursor);
                                        throw th;
                                    }
                                } while (arrayList4.size() < limit);
                                arrayList = arrayList4;
                            }
                            if (artistTracksCursor != null) {
                                if (artistTracksCursor.moveToFirst() && artistTracksCursor.getCount() > 1) {
                                    canShuffle = true;
                                }
                            }
                            canShuffle = false;
                        }
                        com.navdy.service.library.util.IOUtils.closeObject(albumsCursor);
                        com.navdy.service.library.util.IOUtils.closeObject(artistTracksCursor);
                    } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(this.val$request.collectionType)) {
                        android.database.Cursor membersCursor4 = com.navdy.client.app.framework.util.MusicDbUtils.getAlbumMembersCursor(this.val$request.collectionId);
                        if (membersCursor4 != null) {
                            try {
                                if (membersCursor4.moveToPosition(offset)) {
                                    count = membersCursor4.getCount();
                                    if (limit == 0) {
                                        limit = count;
                                    }
                                    java.util.ArrayList arrayList6 = new java.util.ArrayList(limit);
                                    do {
                                        try {
                                            arrayList6.add(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(this.val$request.collectionId).playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).name(membersCursor4.getString(membersCursor4.getColumnIndex("title"))).author(membersCursor4.getString(membersCursor4.getColumnIndex("artist"))).album(membersCursor4.getString(membersCursor4.getColumnIndex("album"))).trackId(java.lang.String.valueOf(membersCursor4.getInt(membersCursor4.getColumnIndex("_id")))).build());
                                            if (!membersCursor4.moveToNext()) {
                                                break;
                                            }
                                        } catch (Throwable th6) {
                                            th = th6;
                                            java.util.ArrayList arrayList7 = arrayList6;
                                            com.navdy.service.library.util.IOUtils.closeObject(membersCursor4);
                                            throw th;
                                        }
                                    } while (arrayList6.size() < limit);
                                    arrayList = arrayList6;
                                }
                            } catch (Throwable th7) {
                                t = th7;
                                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                                com.navdy.service.library.util.IOUtils.closeObject(membersCursor4);
                                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicCollectionResponse.Builder().collectionInfo(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(java.lang.Integer.valueOf(count)).canShuffle(java.lang.Boolean.valueOf(canShuffle)).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
                            }
                        }
                        com.navdy.service.library.util.IOUtils.closeObject(membersCursor4);
                    }
                } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(this.val$request.collectionType)) {
                    android.database.Cursor playlistsCursor = com.navdy.client.app.framework.util.MusicDbUtils.getPlaylistsCursor();
                    if (playlistsCursor != null) {
                        try {
                            if (playlistsCursor.moveToPosition(offset)) {
                                count = playlistsCursor.getCount();
                                if (limit == 0) {
                                    limit = count;
                                }
                                java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> collectionList3 = new java.util.ArrayList<>(limit);
                                do {
                                    try {
                                        int playlistId = playlistsCursor.getInt(playlistsCursor.getColumnIndex("playlist_id"));
                                        java.lang.String playlistName = playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name"));
                                        membersCursor2 = com.navdy.client.app.framework.util.MusicDbUtils.getPlaylistMembersCursor(playlistId);
                                        if (membersCursor2 != null) {
                                            if (membersCursor2.moveToFirst()) {
                                                collectionList3.add(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS).collectionId(java.lang.String.valueOf(playlistId)).name(playlistName).size(java.lang.Integer.valueOf(membersCursor2.getCount())).build());
                                            }
                                        }
                                        com.navdy.service.library.util.IOUtils.closeObject(membersCursor2);
                                        if (!playlistsCursor.moveToNext()) {
                                            break;
                                        }
                                    } catch (Throwable th8) {
                                        th = th8;
                                        java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> list2 = collectionList3;
                                        com.navdy.service.library.util.IOUtils.closeObject(playlistsCursor);
                                        throw th;
                                    }
                                } while (collectionList3.size() < limit);
                                collectionList = collectionList3;
                            }
                        } catch (Throwable th9) {
                            t = th9;
                            try {
                                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error querying GPM database: " + t);
                                com.navdy.service.library.util.IOUtils.closeObject(playlistsCursor);
                                if (((java.lang.Boolean) com.squareup.wire.Wire.get(this.val$request.includeCharacterMap, java.lang.Boolean.valueOf(false))).booleanValue()) {
                                }
                                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicCollectionResponse.Builder().collectionInfo(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(java.lang.Integer.valueOf(count)).canShuffle(java.lang.Boolean.valueOf(canShuffle)).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
                            } catch (Throwable th10) {
                                th = th10;
                                com.navdy.service.library.util.IOUtils.closeObject(playlistsCursor);
                                throw th;
                            }
                        }
                    }
                    com.navdy.service.library.util.IOUtils.closeObject(playlistsCursor);
                    if (((java.lang.Boolean) com.squareup.wire.Wire.get(this.val$request.includeCharacterMap, java.lang.Boolean.valueOf(false))).booleanValue()) {
                        characterMap = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.createCharacterMap(com.navdy.client.app.framework.util.MusicDbUtils.getPlaylistsCharacterMapCursor());
                    }
                } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(this.val$request.collectionType)) {
                    android.database.Cursor artistsCursor = com.navdy.client.app.framework.util.MusicDbUtils.getArtistsCursor();
                    boolean groupByAlbums2 = com.squareup.wire.Wire.get(this.val$request.groupBy, com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN) == com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS;
                    if (artistsCursor != null) {
                        try {
                            if (artistsCursor.moveToPosition(offset)) {
                                count = artistsCursor.getCount();
                                if (limit == 0) {
                                    limit = count;
                                }
                                java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> collectionList4 = new java.util.ArrayList<>(limit);
                                do {
                                    try {
                                        java.lang.String artistId = artistsCursor.getString(artistsCursor.getColumnIndex("_id"));
                                        java.lang.String artistName = artistsCursor.getString(artistsCursor.getColumnIndex("artist"));
                                        membersCursor = groupByAlbums2 ? com.navdy.client.app.framework.util.MusicDbUtils.getArtistsAlbums(artistId) : com.navdy.client.app.framework.util.MusicDbUtils.getArtistMembersCursor(artistId);
                                        if (membersCursor != null) {
                                            if (membersCursor.moveToFirst()) {
                                                int albumsCount = membersCursor.getCount();
                                                com.navdy.service.library.events.audio.MusicCollectionInfo.Builder infoBuilder = new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS).collectionId(java.lang.String.valueOf(artistId)).name(artistName).size(java.lang.Integer.valueOf(membersCursor.getCount()));
                                                if (groupByAlbums2) {
                                                    infoBuilder.subtitle(com.navdy.client.app.NavdyApplication.getAppContext().getResources().getQuantityString(com.navdy.client.R.plurals.album_count, albumsCount, new java.lang.Object[]{java.lang.Integer.valueOf(albumsCount)}));
                                                }
                                                collectionList4.add(infoBuilder.build());
                                            }
                                        }
                                        com.navdy.service.library.util.IOUtils.closeObject(membersCursor);
                                        if (!artistsCursor.moveToNext()) {
                                            break;
                                        }
                                    } catch (Throwable th11) {
                                        th = th11;
                                        java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> list3 = collectionList4;
                                        com.navdy.service.library.util.IOUtils.closeObject(artistsCursor);
                                        throw th;
                                    }
                                } while (collectionList4.size() < limit);
                                collectionList = collectionList4;
                            }
                        } catch (Throwable th12) {
                            t = th12;
                            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                            com.navdy.service.library.util.IOUtils.closeObject(artistsCursor);
                            if (((java.lang.Boolean) com.squareup.wire.Wire.get(this.val$request.includeCharacterMap, java.lang.Boolean.valueOf(false))).booleanValue()) {
                            }
                            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicCollectionResponse.Builder().collectionInfo(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(java.lang.Integer.valueOf(count)).canShuffle(java.lang.Boolean.valueOf(canShuffle)).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
                        }
                    }
                    com.navdy.service.library.util.IOUtils.closeObject(artistsCursor);
                    if (((java.lang.Boolean) com.squareup.wire.Wire.get(this.val$request.includeCharacterMap, java.lang.Boolean.valueOf(false))).booleanValue()) {
                        characterMap = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.createCharacterMap(com.navdy.client.app.framework.util.MusicDbUtils.getArtistsCharacterMapCursor());
                    }
                } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(this.val$request.collectionType)) {
                    android.database.Cursor albumsCursor2 = com.navdy.client.app.framework.util.MusicDbUtils.getAlbumsCursor();
                    if (albumsCursor2 != null) {
                        try {
                            if (albumsCursor2.moveToPosition(offset)) {
                                count = albumsCursor2.getCount();
                                if (limit == 0) {
                                    limit = count;
                                }
                                java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> collectionList5 = new java.util.ArrayList<>(limit);
                                do {
                                    try {
                                        collectionList5.add(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(java.lang.String.valueOf(albumsCursor2.getString(albumsCursor2.getColumnIndex("_id")))).name(albumsCursor2.getString(albumsCursor2.getColumnIndex("album"))).size(java.lang.Integer.valueOf(albumsCursor2.getInt(albumsCursor2.getColumnIndex("numsongs")))).subtitle(albumsCursor2.getString(albumsCursor2.getColumnIndex("artist"))).build());
                                        if (!albumsCursor2.moveToNext()) {
                                            break;
                                        }
                                    } catch (Throwable th13) {
                                        th = th13;
                                        java.util.List<com.navdy.service.library.events.audio.MusicCollectionInfo> list4 = collectionList5;
                                        com.navdy.service.library.util.IOUtils.closeObject(albumsCursor2);
                                        throw th;
                                    }
                                } while (collectionList5.size() < limit);
                                collectionList = collectionList5;
                            }
                        } catch (Throwable th14) {
                            t = th14;
                            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                            com.navdy.service.library.util.IOUtils.closeObject(albumsCursor2);
                            if (((java.lang.Boolean) com.squareup.wire.Wire.get(this.val$request.includeCharacterMap, java.lang.Boolean.valueOf(false))).booleanValue()) {
                            }
                            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicCollectionResponse.Builder().collectionInfo(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(java.lang.Integer.valueOf(count)).canShuffle(java.lang.Boolean.valueOf(canShuffle)).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
                        }
                    }
                    com.navdy.service.library.util.IOUtils.closeObject(albumsCursor2);
                    if (((java.lang.Boolean) com.squareup.wire.Wire.get(this.val$request.includeCharacterMap, java.lang.Boolean.valueOf(false))).booleanValue()) {
                        characterMap = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.createCharacterMap(com.navdy.client.app.framework.util.MusicDbUtils.getAlbumsCharacterMapCursor());
                    }
                }
            }
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicCollectionResponse.Builder().collectionInfo(new com.navdy.service.library.events.audio.MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(java.lang.Integer.valueOf(count)).canShuffle(java.lang.Boolean.valueOf(canShuffle)).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicArtworkRequest val$request;

        Anon4(com.navdy.service.library.events.audio.MusicArtworkRequest musicArtworkRequest) {
            this.val$request = musicArtworkRequest;
        }

        public void run() {
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.val$request.collectionId)) {
                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("This artwork request is only for collections");
                return;
            }
            try {
                long collectionId = (long) java.lang.Integer.parseInt(this.val$request.collectionId);
                android.graphics.Bitmap bitmap = null;
                android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
                if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(this.val$request.collectionType)) {
                    bitmap = com.navdy.client.app.framework.util.MusicUtils.getArtworkForAlbum(contentResolver, collectionId);
                } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(this.val$request.collectionType)) {
                    bitmap = com.navdy.client.app.framework.util.MusicUtils.getArtworkForArtist(contentResolver, collectionId);
                } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(this.val$request.collectionType)) {
                    bitmap = com.navdy.client.app.framework.util.MusicUtils.getArtworkForPlaylist(contentResolver, collectionId);
                }
                if (bitmap == null || bitmap.getByteCount() == 0) {
                    com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.i("Received photo has null or empty byte array");
                }
                android.graphics.Bitmap artwork = null;
                okio.ByteString photo = null;
                if (bitmap != null) {
                    try {
                        artwork = com.navdy.service.library.util.ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap, this.val$request.size.intValue(), this.val$request.size.intValue(), com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT);
                        byte[] data = com.navdy.service.library.util.IOUtils.bitmap2ByteBuffer(artwork);
                        if (data == null) {
                            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error artworking");
                            if (artwork != null) {
                                artwork.recycle();
                                return;
                            }
                            return;
                        }
                        photo = okio.ByteString.of(data);
                    } catch (java.lang.Exception e) {
                        com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Error updating the artwork", e);
                        if (artwork != null) {
                            artwork.recycle();
                            return;
                        }
                        return;
                    } catch (Throwable th) {
                        if (artwork != null) {
                            artwork.recycle();
                        }
                        throw th;
                    }
                }
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.audio.MusicArtworkResponse.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).photo(photo).build());
                if (artwork != null) {
                    artwork.recycle();
                }
            } catch (Throwable e2) {
                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Couldn't parse " + this.val$request.collectionId, e2);
            }
        }
    }

    class Anon5 implements java.lang.Runnable {
        Anon5() {
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.indexAllPlaylists(false);
        }
    }

    class Anon6 implements java.lang.Runnable {
        Anon6() {
        }

        public void run() {
            android.database.Cursor membersCursor;
            android.database.Cursor playlistsCursor = com.navdy.client.app.providers.NavdyContentProvider.getPlaylistsCursor();
            if (playlistsCursor != null) {
                try {
                    if (playlistsCursor.moveToFirst()) {
                        do {
                            int playlistId = playlistsCursor.getInt(playlistsCursor.getColumnIndex("playlist_id"));
                            java.lang.String playlistName = playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name"));
                            membersCursor = com.navdy.client.app.providers.NavdyContentProvider.getPlaylistMembersCursor(playlistId);
                            if (membersCursor != null) {
                                if (membersCursor.moveToFirst()) {
                                    int count = 0;
                                    do {
                                        java.lang.String artist = membersCursor.getString(membersCursor.getColumnIndex("artist"));
                                        java.lang.String album = membersCursor.getString(membersCursor.getColumnIndex("album"));
                                        java.lang.String title = membersCursor.getString(membersCursor.getColumnIndex("title"));
                                        java.lang.String sourceId = membersCursor.getString(membersCursor.getColumnIndex("SourceId"));
                                        if (sourceId.matches("\\d+")) {
                                            if (count == 0) {
                                                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.d("Playlist ID: " + playlistId + ", Name: " + playlistName);
                                            }
                                            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.d("- Artist: " + artist + ", Album: " + album + ", Title: " + title + ", ID: " + sourceId);
                                            count++;
                                        }
                                    } while (membersCursor.moveToNext());
                                }
                            }
                            com.navdy.service.library.util.IOUtils.closeStream(membersCursor);
                        } while (playlistsCursor.moveToNext());
                    }
                } catch (Throwable th) {
                    com.navdy.service.library.util.IOUtils.closeStream(playlistsCursor);
                    throw th;
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(playlistsCursor);
        }
    }

    private class GpmPlaylistObserver extends android.database.ContentObserver {
        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("/playlists/(\\d+)/?.*");
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler.GpmPlaylistObserver.GpmObserverRunnable runnable = new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.GpmPlaylistObserver.GpmObserverRunnable(this, null);

        private class GpmObserverRunnable implements java.lang.Runnable {
            java.lang.Integer playlistId;

            private GpmObserverRunnable() {
            }

            /* synthetic */ GpmObserverRunnable(com.navdy.client.app.framework.servicehandler.MusicServiceHandler.GpmPlaylistObserver x0, com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon1 x1) {
                this();
            }

            public void run() {
                if (this.playlistId != null) {
                    com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.indexPlaylist(this.playlistId.intValue());
                } else {
                    com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.indexAllPlaylists(true);
                }
            }

            void setPlaylistId(java.lang.Integer playlistId2) {
                this.playlistId = playlistId2;
            }
        }

        GpmPlaylistObserver() {
            super(com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.musicObserverHandler);
        }

        public void onChange(boolean selfChange, android.net.Uri uri) {
            java.lang.String path = uri.getPath();
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.d("onChange: " + path + org.droidparts.contract.SQL.DDL.OPENING_BRACE + uri + ")");
            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(path)) {
                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.musicObserverHandler.removeCallbacks(this.runnable);
                this.runnable.setPlaylistId(null);
                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.musicObserverHandler.postDelayed(this.runnable, 200);
                return;
            }
            java.util.regex.Matcher matcher = this.pattern.matcher(path);
            if (matcher.matches()) {
                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.d("Match: " + matcher.group());
                java.lang.String playlistIdStr = matcher.group(1);
                try {
                    int playlistId = java.lang.Integer.parseInt(playlistIdStr);
                    com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.musicObserverHandler.removeCallbacks(this.runnable);
                    this.runnable.setPlaylistId(java.lang.Integer.valueOf(playlistId));
                    com.navdy.client.app.framework.servicehandler.MusicServiceHandler.this.musicObserverHandler.postDelayed(this.runnable, 200);
                } catch (java.lang.NumberFormatException e) {
                    com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger.e("Can't convert to integer: " + playlistIdStr);
                }
            }
        }
    }

    public interface MusicSeekHelper {
        void executeMusicSeekAction(com.navdy.service.library.events.audio.MusicEvent.Action action);
    }

    public com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper getMusicSeekHelper() {
        return this.musicSeekHelper;
    }

    public void setMusicSeekHelper(com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper musicSeekHelper2) {
        this.musicSeekHelper = musicSeekHelper2;
    }

    private MusicServiceHandler() {
        sLogger.d("MusicServiceHandler ctor");
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        android.os.HandlerThread thread = new android.os.HandlerThread("MusicObserver");
        thread.start();
        this.musicObserverLooper = thread.getLooper();
        this.musicObserverHandler = new android.os.Handler(this.musicObserverLooper);
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler.GpmPlaylistObserver gpmPlaylistObserver = new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.GpmPlaylistObserver();
        com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().registerContentObserver(com.navdy.client.app.framework.util.MusicDbUtils.getGpmPlaylistsUri(), true, gpmPlaylistObserver);
        this.musicObserverHandler.postDelayed(new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon2(), com.here.odnp.config.OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL);
    }

    protected void finalize() throws java.lang.Throwable {
        super.finalize();
        this.musicObserverLooper.quit();
    }

    public static synchronized com.navdy.client.app.framework.servicehandler.MusicServiceHandler getInstance() {
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler musicServiceHandler;
        synchronized (com.navdy.client.app.framework.servicehandler.MusicServiceHandler.class) {
            if (singleton == null) {
                singleton = new com.navdy.client.app.framework.servicehandler.MusicServiceHandler();
            }
            musicServiceHandler = singleton;
        }
        return musicServiceHandler;
    }

    @com.squareup.otto.Subscribe
    public void onHUDDisconnected(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        sLogger.v("HUD is disconnected - reset internal state");
        this.postPhotoUpdates = false;
        com.navdy.client.app.framework.util.MusicUtils.executeLongPressedKeyUp();
        com.navdy.client.app.framework.util.MusicUtils.stopInternalMusicPlayer();
    }

    @com.squareup.otto.Subscribe
    public void onHUDConnected(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent event) {
        sLogger.v("HUD is connected - doing some initial stuff for music");
        this.postPhotoUpdates = true;
    }

    @com.squareup.otto.Subscribe
    public void onMusicCollectionRequest(com.navdy.service.library.events.audio.MusicCollectionRequest request) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon3(request), 1);
    }

    private java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> createCharacterMap(android.database.Cursor cursor) {
        java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> characterMap = null;
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> characterMap2 = new java.util.ArrayList<>();
                    int characterOffset = 0;
                    boolean indexedNumbers = false;
                    boolean indexedSymbols = false;
                    do {
                        boolean shouldAdd = false;
                        try {
                            java.lang.String c = cursor.getString(cursor.getColumnIndex(com.navdy.client.app.framework.util.MusicDbUtils.LETTER_COLUMN));
                            int count = cursor.getInt(cursor.getColumnIndex(com.navdy.client.app.framework.util.MusicDbUtils.COUNT_COLUMN));
                            if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(c) || c.length() > 1) {
                                sLogger.e("Bad value in character map cursor: " + c);
                            } else {
                                if (c.matches("[A-Z]")) {
                                    shouldAdd = true;
                                } else if (c.matches("\\d")) {
                                    if (!indexedNumbers) {
                                        c = "#";
                                        indexedNumbers = true;
                                        shouldAdd = true;
                                    }
                                } else if (!indexedSymbols) {
                                    c = "%";
                                    indexedSymbols = true;
                                    shouldAdd = true;
                                }
                                if (shouldAdd) {
                                    characterMap2.add(new com.navdy.service.library.events.audio.MusicCharacterMap.Builder().character(java.lang.String.valueOf(c)).offset(java.lang.Integer.valueOf(characterOffset)).build());
                                }
                                characterOffset += count;
                            }
                        } catch (Throwable th) {
                            th = th;
                            java.util.List<com.navdy.service.library.events.audio.MusicCharacterMap> list = characterMap2;
                            com.navdy.service.library.util.IOUtils.closeStream(cursor);
                            throw th;
                        }
                    } while (cursor.moveToNext());
                    characterMap = characterMap2;
                }
            } catch (Throwable th2) {
                t = th2;
                sLogger.e("Couldn't generate character map: " + t);
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                return characterMap;
            }
        }
        com.navdy.service.library.util.IOUtils.closeStream(cursor);
        return characterMap;
    }

    @com.squareup.otto.Subscribe
    public void onMusicCapabilitiesRequest(com.navdy.service.library.events.audio.MusicCapabilitiesRequest musicCapabilitiesRequest) {
        sLogger.d("onMusicCapabilitiesRequest");
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) getMusicCapabilities());
    }

    public static com.navdy.service.library.events.audio.MusicCapabilitiesResponse getMusicCapabilities() {
        com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus authorizationStatus;
        if (com.navdy.client.app.ui.base.BaseActivity.weHaveStoragePermission()) {
            authorizationStatus = com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus.MUSIC_AUTHORIZATION_AUTHORIZED;
        } else {
            authorizationStatus = com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus.MUSIC_AUTHORIZATION_DENIED;
        }
        java.util.List<com.navdy.service.library.events.audio.MusicCollectionType> collectionTypes = new java.util.ArrayList<>();
        collectionTypes.add(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS);
        collectionTypes.add(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS);
        collectionTypes.add(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS);
        java.util.List<com.navdy.service.library.events.audio.MusicCapability> capabilitiesList = new java.util.ArrayList<>();
        capabilitiesList.add(new com.navdy.service.library.events.audio.MusicCapability.Builder().collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionTypes(collectionTypes).authorizationStatus(authorizationStatus).build());
        return new com.navdy.service.library.events.audio.MusicCapabilitiesResponse.Builder().capabilities(capabilitiesList).build();
    }

    @com.squareup.otto.Subscribe
    public void onMusicArtworkRequest(com.navdy.service.library.events.audio.MusicArtworkRequest request) {
        sLogger.d("onMusicArtworkRequest " + request);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon4(request), 1);
    }

    @com.squareup.otto.Subscribe
    public void onMusicTrackInfoRequest(com.navdy.service.library.events.audio.MusicTrackInfoRequest request) {
        sLogger.d("Request for track info received.");
        sendCurrentTrackInfo();
    }

    @com.squareup.otto.Subscribe
    public void onMusicEvent(com.navdy.service.library.events.audio.MusicEvent event) {
        sLogger.d("Request for performing some action to the music received.");
        try {
            com.navdy.client.app.framework.util.MusicUtils.executeMusicAction(event);
        } catch (java.io.IOException e) {
            sLogger.e("Cannot load music", e);
        } catch (android.content.pm.PackageManager.NameNotFoundException e2) {
            sLogger.e("Cannot start music player", e2);
        } catch (java.lang.UnsupportedOperationException e3) {
            sLogger.e("Cannot execute action", e3);
        }
    }

    @com.squareup.otto.Subscribe
    public void onPhotoUpdatesRequest(com.navdy.service.library.events.photo.PhotoUpdatesRequest request) {
        sLogger.d("Request for changing photo updating status received with value " + java.lang.String.valueOf(request.start) + " for imageResourceIds of type " + java.lang.String.valueOf(request.photoType));
        if (com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART.equals(request.photoType)) {
            this.postPhotoUpdates = request.start.booleanValue();
            sendCurrentMediaArtworkAsUpdate();
        }
    }

    private void sendRemoteMessage(com.squareup.wire.Message message) {
        sLogger.v("Sending message to the HUD: " + java.lang.String.valueOf(message));
        com.navdy.client.app.framework.DeviceConnection.postEvent(message);
    }

    private void sendCurrentTrackInfo() {
        sendRemoteMessage(getCurrentMediaTrackInfo());
    }

    @android.support.annotation.NonNull
    public synchronized com.navdy.service.library.events.audio.MusicTrackInfo getCurrentMediaTrackInfo() {
        return this.currentMediaTrackInfo;
    }

    public synchronized void setCurrentMediaTrackInfo(@android.support.annotation.Nullable com.navdy.service.library.events.audio.MusicTrackInfo currentMediaTrackInfo2) {
        sLogger.v("setCurrentMediaTrackInfo " + currentMediaTrackInfo2);
        if (currentMediaTrackInfo2 == this.currentMediaTrackInfo) {
            sLogger.d("Got same track info object - disregarded");
        } else if (currentMediaTrackInfo2 == null) {
            sLogger.i("Got null track info object - cleaning");
            this.currentMediaTrackInfo = EMPTY_TRACK_INFO;
            setCurrentMediaArtwork(null, null);
            sendRemoteMessage(this.currentMediaTrackInfo);
        } else if (currentMediaTrackInfo2.equals(this.currentMediaTrackInfo)) {
            throttlingLogger.i(1, "Got no new data in the update - disregarded");
        } else {
            this.currentMediaTrackInfo = currentMediaTrackInfo2;
            sendRemoteMessage(this.currentMediaTrackInfo);
        }
    }

    private boolean isTheSameAsCurrentSong(com.navdy.service.library.events.audio.MusicTrackInfo theOtherSong) {
        return com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(theOtherSong), com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(getCurrentMediaTrackInfo()));
    }

    public void checkAndSetCurrentMediaArtwork(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo, android.graphics.Bitmap artwork) {
        if (!isTheSameAsCurrentSong(trackInfo)) {
            sLogger.w("Tried to set artwork from the wrong song - ignored");
            return;
        }
        if (artwork == null) {
            sLogger.i("null set as current media artwork (artwork reset)");
        }
        java.lang.String artworkHash = com.navdy.service.library.util.IOUtils.hashForBitmap(artwork);
        sLogger.v("Got album artwork with hash " + artworkHash);
        if (com.navdy.client.app.framework.util.MusicUtils.isDefaultArtwork(artworkHash)) {
            sLogger.i("default album art found - ignored");
        } else {
            setCurrentMediaArtwork(artwork, artworkHash);
        }
    }

    private synchronized void setCurrentMediaArtwork(android.graphics.Bitmap artwork, java.lang.String hash) {
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.currentArtworkHash, hash)) {
            this.currentMediaArtwork = artwork;
            this.currentArtworkHash = hash;
            sendCurrentMediaArtworkAsUpdate();
        } else {
            sLogger.i("Album art is identical to current, ignoring");
        }
    }

    public synchronized android.graphics.Bitmap getCurrentMediaArtwork() {
        return this.currentMediaArtwork;
    }

    private void sendCurrentMediaArtworkAsUpdate() {
        sLogger.i("sendCurrentMediaArtworkAsUpdate, postPhotoUpdates: " + this.postPhotoUpdates);
        if (this.postPhotoUpdates) {
            sLogger.v("Sending music artwork photo update");
            com.navdy.service.library.events.photo.PhotoUpdate.Builder builder = new com.navdy.service.library.events.photo.PhotoUpdate.Builder();
            builder.photoType(com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART);
            builder.identifier(com.navdy.service.library.util.MusicDataUtils.photoIdentifierFromTrackInfo(getCurrentMediaTrackInfo()));
            android.graphics.Bitmap art = getCurrentMediaArtwork();
            sLogger.d("State of currentMediaArtwork: " + art);
            if (art != null) {
                builder = builder.photo(okio.ByteString.of(com.navdy.service.library.util.IOUtils.bitmap2ByteBuffer(art)));
            }
            if (builder.photo == null) {
                sLogger.e("Photo within the photo update is null");
            }
            sendRemoteMessage(builder.build());
        }
    }

    public void setLastMusicApp(java.lang.String packageName) {
        sLogger.d("setLastMusicApp " + packageName);
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().putString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_MEDIA_APP, packageName).apply();
    }

    @android.support.annotation.Nullable
    public java.lang.String getLastMusicApp() {
        java.lang.String packageName = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getString(com.navdy.client.app.ui.settings.SettingsConstants.LAST_MEDIA_APP, null);
        sLogger.d("getLastMusicApp " + packageName);
        return packageName;
    }

    public boolean isMusicPlayerActive() {
        return !EMPTY_TRACK_INFO.equals(this.currentMediaTrackInfo);
    }

    public void indexPlaylists() {
        this.musicObserverHandler.post(new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon5());
    }

    /* access modifiers changed from: private */
    @android.support.annotation.WorkerThread
    public void indexAllPlaylists(boolean initializing) {
        sLogger.d("indexAllPlaylists (" + initializing + ")");
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.database.Cursor playlistsCursor = null;
        try {
            com.navdy.client.app.providers.NavdyContentProvider.deleteAllPlaylists();
            int analyticsPlaylistCount = 0;
            int analyticsTrackCount = 0;
            playlistsCursor = com.navdy.client.app.framework.util.MusicDbUtils.getGpmPlaylistsCursor();
            if (playlistsCursor != null && playlistsCursor.moveToFirst()) {
                this.indexingRetries = 0;
                this.shouldPerformFullPlaylistIndex = false;
                do {
                    analyticsTrackCount += indexPlaylistMembers(playlistsCursor.getInt(playlistsCursor.getColumnIndex("_id")), playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name")));
                    analyticsPlaylistCount++;
                } while (playlistsCursor.moveToNext());
                java.util.Map<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>(2);
                attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.MusicAttributes.NUM_GPM_PLAYLISTS_INDEXED, java.lang.String.valueOf(analyticsPlaylistCount));
                attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.MusicAttributes.NUM_GPM_TRACKS_INDEXED, java.lang.String.valueOf(analyticsTrackCount));
                com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.MUSIC_PLAYLIST_COMPLETE_REINDEX, attributes);
            } else if (playlistsCursor == null) {
                sLogger.e("Cursor returned is null, we will retry, Retries : " + this.indexingRetries);
                if (!initializing || this.indexingRetries > 5) {
                    sLogger.e("Not retrying as the maximum retries has reached");
                } else {
                    this.musicObserverHandler.removeCallbacks(this.retryIndexingPlayListsRunnable);
                    this.musicObserverHandler.postDelayed(this.retryIndexingPlayListsRunnable, 10000);
                }
            }
        } catch (Throwable t) {
            sLogger.e("Unable to index playlists: " + t);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(playlistsCursor);
        }
    }

    /* access modifiers changed from: private */
    @android.support.annotation.WorkerThread
    public void indexPlaylist(int playlistId) {
        sLogger.d("indexPlaylist: " + playlistId);
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.database.Cursor cursor = null;
        try {
            com.navdy.client.app.providers.NavdyContentProvider.deletePlaylist(playlistId);
            cursor = com.navdy.client.app.framework.util.MusicDbUtils.getGpmPlaylistCursor(playlistId);
            if (cursor != null && cursor.moveToFirst()) {
                java.lang.String playlistName = cursor.getString(cursor.getColumnIndex("playlist_name"));
                if (playlistName != null) {
                    indexPlaylistMembers(playlistId, playlistName);
                }
            }
        } catch (java.lang.Exception e) {
            sLogger.e("Exception while deleting playlist ", e);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    @android.support.annotation.WorkerThread
    private int indexPlaylistMembers(int playlistId, java.lang.String playlistName) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        android.database.Cursor membersCursor = com.navdy.client.app.framework.util.MusicDbUtils.getGpmPlaylistMembersCursor(playlistId);
        int analyticsTrackCount = 0;
        if (membersCursor != null) {
            try {
                if (membersCursor.moveToFirst()) {
                    int count = 0;
                    do {
                        java.lang.String sourceId = membersCursor.getString(membersCursor.getColumnIndex("SourceId"));
                        if (sourceId.matches("\\d+")) {
                            if (count == 0) {
                                sLogger.v("Playlist ID: " + playlistId + ", Name: " + playlistName);
                                com.navdy.client.app.providers.NavdyContentProvider.addPlaylistToDb(playlistId, playlistName);
                            }
                            java.lang.String artist = membersCursor.getString(membersCursor.getColumnIndex("artist"));
                            java.lang.String album = membersCursor.getString(membersCursor.getColumnIndex("album"));
                            java.lang.String title = membersCursor.getString(membersCursor.getColumnIndex("title"));
                            sLogger.v("- Artist: " + artist + ", Album: " + album + ", Title: " + title + ", ID: " + sourceId);
                            com.navdy.client.app.providers.NavdyContentProvider.addPlaylistMemberToDb(playlistId, sourceId, artist, album, title);
                            count++;
                        }
                        analyticsTrackCount++;
                    } while (membersCursor.moveToNext());
                }
            } catch (Throwable th) {
                if (membersCursor != null) {
                    membersCursor.close();
                }
                throw th;
            }
        }
        if (membersCursor != null) {
            membersCursor.close();
        }
        return analyticsTrackCount;
    }

    private void logInternalPlaylistTables() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon6(), 1);
    }

    public boolean shouldPerformFullPlaylistIndex() {
        return this.shouldPerformFullPlaylistIndex;
    }
}
