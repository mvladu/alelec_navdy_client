package com.navdy.client.app.framework;

public class AppInstance extends com.navdy.service.library.util.Listenable<com.navdy.client.app.framework.AppInstance.Listener> implements com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Listener, com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {
    private static final long ACTIVITY_RECOGNITION_UPDATE_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(10);
    private static final android.content.IntentFilter DATETIME_CHANGE_INTENT_FILTER = new android.content.IntentFilter("android.intent.action.DATE_CHANGED");
    private static final java.lang.String EMPTY = "";
    private static final int REQUEST_CODE = 34567;
    private static final long SUGGESTION_KICK_OFF_DELAY_AFTER_CONNECTED = 15000;
    private static final long TRIP_DB_CHECK_KICK_OFF_DELAY_AFTER_CONNECTED = 60000;
    @android.annotation.SuppressLint({"StaticFieldLeak"})
    private static volatile com.navdy.client.app.framework.AppInstance sInstance;
    private static final java.lang.Object sInstanceLock = new java.lang.Object();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.AppInstance.class);
    private boolean busIsRegistered = false;
    private com.navdy.client.app.framework.servicehandler.CalendarHandler calendarHandler = null;
    private final android.content.BroadcastReceiver datetimeUpdateReceiver = new com.navdy.client.app.framework.AppInstance.Anon6();
    private volatile boolean deviceConnected = false;
    private boolean isHudMapEngineReady;
    private com.google.android.gms.common.api.GoogleApiClient mApiClient;
    private boolean mAppInitialized = false;
    private com.navdy.client.app.framework.servicehandler.BatteryStatusManager mBatteryStatus;
    private com.navdy.client.app.framework.service.ClientConnectionService mConnectionService;
    private android.content.Context mContext = com.navdy.client.app.NavdyApplication.getAppContext();
    public com.navdy.client.app.framework.servicehandler.LocationTransmitter mLocationTransmitter;
    private android.os.Handler mMainHandler = new android.os.Handler(this.mContext.getMainLooper());
    private com.navdy.service.library.events.navigation.NavigationSessionState mNavigationSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    @javax.inject.Inject
    com.navdy.client.app.framework.servicehandler.NetworkStatusManager mNetworkStatus;
    private com.navdy.service.library.device.RemoteDeviceRegistry mRemoteDeviceRegistry;
    private android.content.ServiceConnection serviceConnection = new com.navdy.client.app.framework.AppInstance.Anon1();

    class Anon1 implements android.content.ServiceConnection {
        Anon1() {
        }

        public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
            if (!(service instanceof com.navdy.client.app.framework.service.ClientConnectionService.LocalBinder)) {
                com.navdy.client.app.framework.AppInstance.sLogger.v("service is not a LocalBinder, no-op");
                return;
            }
            com.navdy.client.app.framework.AppInstance.sLogger.i("Connected to ClientConnectionService");
            com.navdy.client.app.framework.AppInstance.this.mConnectionService = ((com.navdy.client.app.framework.service.ClientConnectionService.LocalBinder) service).getService();
            com.navdy.client.app.framework.AppInstance.this.mConnectionService.addListener(com.navdy.client.app.framework.AppInstance.this);
            com.navdy.client.app.framework.AppInstance.this.initializeDevice();
        }

        public void onServiceDisconnected(android.content.ComponentName name) {
            com.navdy.client.app.framework.AppInstance.sLogger.i("Disconnected from ClientConnectionService service");
            com.navdy.client.app.framework.AppInstance.this.mConnectionService = null;
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ boolean val$big;
        final /* synthetic */ int val$toastDuration;
        final /* synthetic */ java.lang.String val$toastText;

        Anon2(java.lang.String str, int i, boolean z) {
            this.val$toastText = str;
            this.val$toastDuration = i;
            this.val$big = z;
        }

        public void run() {
            android.widget.Toast toast = android.widget.Toast.makeText(com.navdy.client.app.framework.AppInstance.this.mContext, this.val$toastText, this.val$toastDuration);
            android.view.View view = toast.getView();
            if (this.val$big) {
                if (view instanceof android.widget.LinearLayout) {
                    android.widget.TextView messageTextView = (android.widget.TextView) ((android.widget.LinearLayout) view).getChildAt(0);
                    if (messageTextView != null) {
                        messageTextView.setTextSize(48.0f);
                    }
                }
                toast.setGravity(17, 0, 0);
            }
            toast.show();
        }
    }

    class Anon3 implements java.lang.Runnable {
        Anon3() {
        }

        public void run() {
            com.navdy.client.app.framework.suggestion.DestinationSuggestionService.suggestDestination(com.navdy.client.app.framework.AppInstance.this.mContext, false);
        }
    }

    class Anon5 implements com.navdy.client.app.framework.AppInstance.EventDispatcher {
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$newDevice;

        Anon5(com.navdy.service.library.device.RemoteDevice remoteDevice) {
            this.val$newDevice = remoteDevice;
        }

        public void dispatchEvent(com.navdy.client.app.framework.AppInstance source, com.navdy.client.app.framework.AppInstance.Listener listener) {
            listener.onDeviceChanged(this.val$newDevice);
        }
    }

    class Anon6 extends android.content.BroadcastReceiver {
        Anon6() {
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.this.getRemoteDevice();
            if (remoteDevice != null) {
                com.navdy.client.app.framework.AppInstance.this.sendCurrentTime(remoteDevice, android.text.format.DateFormat.is24HourFormat(context));
            }
        }
    }

    interface EventDispatcher extends com.navdy.service.library.util.Listenable.EventDispatcher<com.navdy.client.app.framework.AppInstance, com.navdy.client.app.framework.AppInstance.Listener> {
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onDeviceChanged(com.navdy.service.library.device.RemoteDevice remoteDevice);
    }

    static {
        DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIME_SET");
        DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIMEZONE_CHANGED");
    }

    private AppInstance() {
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
    }

    public static com.navdy.client.app.framework.AppInstance getInstance() {
        if (sInstance == null) {
            synchronized (sInstanceLock) {
                if (sInstance == null) {
                    sInstance = new com.navdy.client.app.framework.AppInstance();
                }
            }
        }
        return sInstance;
    }

    public synchronized void initializeApp() {
        if (!this.mAppInitialized) {
            this.mAppInitialized = true;
            initializeRegistry();
            initializeConnectionService();
            com.navdy.client.app.framework.service.TaskRemovalService.startService(this.mContext);
            this.mApiClient = new com.google.android.gms.common.api.GoogleApiClient.Builder(com.navdy.client.app.NavdyApplication.getAppContext()).addApi(com.google.android.gms.location.ActivityRecognition.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
            this.mApiClient.connect();
        }
    }

    public void initializeDevice() {
        com.navdy.service.library.device.connection.ConnectionInfo connectionInfo = this.mRemoteDeviceRegistry.getDefaultConnectionInfo();
        if (connectionInfo != null && this.mConnectionService != null) {
            this.mConnectionService.connect(connectionInfo);
        }
    }

    private void initializeRegistry() {
        this.mRemoteDeviceRegistry = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(this.mContext);
        this.mRemoteDeviceRegistry.addRemoteDeviceScanner(new com.navdy.service.library.device.discovery.BTRemoteDeviceScanner(this.mContext, 12));
    }

    private void initializeConnectionService() {
        sLogger.v("init DeviceConnection");
        com.navdy.client.app.framework.DeviceConnection.getInstance();
        sLogger.d("Binding to client connection service");
        android.content.Intent intent = new android.content.Intent(this.mContext, com.navdy.client.app.framework.service.ClientConnectionService.class);
        intent.setAction(com.navdy.client.app.framework.service.ClientConnectionService.class.getName());
        try {
            this.mContext.bindService(intent, this.serviceConnection, 1);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            this.mAppInitialized = false;
        }
    }

    public void setLocationTransmitter(com.navdy.client.app.framework.servicehandler.LocationTransmitter transmitter) {
        if (this.mLocationTransmitter != null) {
            this.mLocationTransmitter.stop();
        }
        this.mLocationTransmitter = transmitter;
        com.navdy.service.library.device.RemoteDevice device = this.mConnectionService.getRemoteDevice();
        if (device != null && device.isConnected() && this.mLocationTransmitter != null) {
            this.mLocationTransmitter.start();
        }
    }

    @android.support.annotation.Nullable
    public com.navdy.service.library.device.RemoteDevice getRemoteDevice() {
        if (this.mConnectionService != null) {
            return this.mConnectionService.getRemoteDevice();
        }
        return null;
    }

    public com.navdy.service.library.events.navigation.NavigationSessionState getNavigationSessionState() {
        return this.mNavigationSessionState;
    }

    public boolean isHudMapEngineReady() {
        return this.isHudMapEngineReady;
    }

    @java.lang.Deprecated
    public void showToast(java.lang.String toastText, boolean big) {
        showToast(toastText, big, 1);
    }

    @java.lang.Deprecated
    public void showToast(java.lang.String toastText, boolean big, int toastDuration) {
        this.mMainHandler.post(new com.navdy.client.app.framework.AppInstance.Anon2(toastText, toastDuration, big));
    }

    void onDeviceConnectedFirstResponder(com.navdy.service.library.device.RemoteDevice device) {
        this.deviceConnected = true;
        if (!this.busIsRegistered) {
            this.busIsRegistered = true;
            com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
        }
        com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent(device));
        requestActivityRecognitionUpdates();
        if (device != null) {
            sendPhoneInfo(device);
            sendCurrentTime(device, android.text.format.DateFormat.is24HourFormat(com.navdy.client.app.NavdyApplication.getAppContext()));
        }
        if (this.mLocationTransmitter == null) {
            this.mLocationTransmitter = new com.navdy.client.app.framework.servicehandler.LocationTransmitter(this.mContext, device);
        }
        if (this.mBatteryStatus == null) {
            this.mBatteryStatus = new com.navdy.client.app.framework.servicehandler.BatteryStatusManager(device);
        }
        this.mNetworkStatus.onRemoteDeviceConnected(device);
        this.mLocationTransmitter.start();
        this.mContext.registerReceiver(this.datetimeUpdateReceiver, DATETIME_CHANGE_INTENT_FILTER);
        this.mBatteryStatus.register();
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.HUD_CONNECTION_ESTABLISHED);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.MOBILE_APP_VERSION, com.navdy.client.BuildConfig.VERSION_NAME);
        if (com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.RECOMMENDATIONS_ENABLED, true)) {
            this.mMainHandler.postDelayed(new com.navdy.client.app.framework.AppInstance.Anon3(), 15000);
        }
        if (this.calendarHandler == null) {
            this.calendarHandler = new com.navdy.client.app.framework.servicehandler.CalendarHandler(this.mContext);
        }
        this.calendarHandler.registerObserver();
        com.navdy.client.app.ui.homescreen.CalendarUtils.sendCalendarsToHud(device);
        com.navdy.client.app.framework.util.SupportTicketService.submitTicketsIfConditionsAreMet();
    }

    void onDeviceDisconnectedFirstResponder(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        java.lang.String showName;
        this.mNavigationSessionState = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED;
        this.deviceConnected = false;
        removeActivityRecognitionUpdates();
        this.isHudMapEngineReady = false;
        com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent(device, cause));
        if (this.mBatteryStatus != null) {
            this.mBatteryStatus.unregister();
            this.mBatteryStatus = null;
        }
        this.mNetworkStatus.onRemoteDeviceDisconnected();
        if (this.mLocationTransmitter != null) {
            this.mLocationTransmitter.stop();
            this.mLocationTransmitter = null;
        }
        try {
            showName = device.getDeviceId().getDisplayName();
        } catch (java.lang.Exception e) {
            showName = "?";
        }
        showToast("Disconnected: " + showName, false);
        try {
            sLogger.v("unregistering datetimeUpdateReceiver");
            this.mContext.unregisterReceiver(this.datetimeUpdateReceiver);
        } catch (java.lang.IllegalArgumentException e2) {
            sLogger.e("Cannot unregister datetime update receiver", e2);
        }
        com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.HUD_CONNECTION_LOST);
        if (this.calendarHandler != null) {
            this.calendarHandler.unregisterObserver();
        }
        if (this.busIsRegistered) {
            this.busIsRegistered = false;
            com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
        }
    }

    private synchronized void requestActivityRecognitionUpdates() {
        sLogger.d("GoogleApiClient : requesting activity updates");
        if (this.mApiClient.isConnected()) {
            sLogger.d("GoogleApiClient is already connected, requesting the updates");
            android.app.PendingIntent pendingIntent = android.app.PendingIntent.getService(com.navdy.client.app.NavdyApplication.getAppContext(), REQUEST_CODE, new android.content.Intent(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.service.ActivityRecognizedCallbackService.class), 134217728);
            if (com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi != null) {
                com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(this.mApiClient, ACTIVITY_RECOGNITION_UPDATE_INTERVAL, pendingIntent);
            }
        } else {
            sLogger.d("The GoogleApiClient is not connected yet.Waiting for it to be connected");
        }
    }

    private synchronized void removeActivityRecognitionUpdates() {
        sLogger.d("GoogleApiClient : removing activity updates");
        if (this.mApiClient != null && this.mApiClient.isConnected()) {
            android.app.PendingIntent pendingIntent = android.app.PendingIntent.getService(com.navdy.client.app.NavdyApplication.getAppContext(), REQUEST_CODE, new android.content.Intent(com.navdy.client.app.NavdyApplication.getAppContext(), com.navdy.client.app.service.ActivityRecognizedCallbackService.class), 134217728);
            if (!(com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi == null || pendingIntent == null)) {
                try {
                    com.google.android.gms.location.ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(this.mApiClient, pendingIntent);
                } catch (java.lang.NullPointerException e) {
                    sLogger.e("Something went wrong while trying to remove activity recognition updates");
                }
            }
        }
        return;
    }

    public boolean isDeviceConnected() {
        return this.deviceConnected;
    }

    public boolean canReachInternet() {
        return this.mNetworkStatus.canReachInternet();
    }

    public void checkForNetwork() {
        this.mNetworkStatus.checkForNetwork();
    }

    @com.squareup.otto.Subscribe
    public void onNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent status) {
        this.mNavigationSessionState = status.sessionState;
        if (status.sessionState == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_READY) {
            this.isHudMapEngineReady = true;
        }
    }

    @com.squareup.otto.Subscribe
    public void onDeviceInfo(com.navdy.service.library.events.DeviceInfo deviceInfo) {
        com.navdy.service.library.device.RemoteDevice device = getRemoteDevice();
        if (device != null) {
            device.setDeviceInfo(deviceInfo);
        } else {
            sLogger.e("Unable to get remote device in order to update device info!");
        }
        sLogger.i("Display DeviceInfo: " + deviceInfo);
        if (deviceInfo.capabilities != null) {
            com.navdy.client.app.ui.settings.SettingsUtils.checkCapabilities(deviceInfo.capabilities);
        } else {
            com.navdy.client.app.ui.settings.SettingsUtils.checkLegacyCapabilityList(deviceInfo.legacyCapabilities);
        }
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.HUD_SERIAL_NUMBER, deviceInfo.deviceUuid);
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.HUD_VERSION_NUMBER, deviceInfo.systemVersion);
        com.navdy.client.ota.OTAUpdateService.bPersistDeviceInfo(deviceInfo);
        com.navdy.client.ota.OTAUpdateService.startService(this.mContext);
        com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.DeviceConnection.DeviceInfoEvent(deviceInfo));
    }

    @com.squareup.otto.Subscribe
    public void onTransmitLocation(com.navdy.service.library.events.location.TransmitLocation transmitLocation) {
        if (transmitLocation == null) {
            sLogger.e("location transmission event with a null transmit location extension !!! WTF !!!");
        } else if (java.lang.Boolean.TRUE.equals(transmitLocation.sendLocation)) {
            sLogger.v("starting location transmission");
            this.mLocationTransmitter.start();
        } else {
            sLogger.v("stopping location transmission");
            this.mLocationTransmitter.stop();
        }
    }

    public void onDeviceChanged(com.navdy.service.library.device.RemoteDevice newDevice) {
        sLogger.d("DeviceChanged: " + newDevice);
        dispatchToListeners(new com.navdy.client.app.framework.AppInstance.Anon5(newDevice));
    }

    private void sendPhoneInfo(com.navdy.service.library.device.RemoteDevice device) {
        if (device == null) {
            sLogger.w("called setPhoneInfo with a null RemoteDevice!");
            return;
        }
        try {
            com.navdy.service.library.events.Capabilities.Builder capabilitiesBuilder = new com.navdy.service.library.events.Capabilities.Builder();
            capabilitiesBuilder.placeTypeSearch(java.lang.Boolean.valueOf(true));
            capabilitiesBuilder.voiceSearch(java.lang.Boolean.valueOf(true));
            capabilitiesBuilder.navCoordsLookup(java.lang.Boolean.valueOf(true));
            capabilitiesBuilder.cannedResponseToSms(java.lang.Boolean.valueOf(true));
            capabilitiesBuilder.supportsPlayAudioRequest(java.lang.Boolean.valueOf(true));
            com.navdy.service.library.events.Capabilities capabilities = capabilitiesBuilder.build();
            com.navdy.service.library.device.NavdyDeviceId deviceId = com.navdy.service.library.device.NavdyDeviceId.getThisDevice(this.mContext);
            java.lang.String deviceName = deviceId.getDeviceName();
            if (deviceName == null) {
                deviceName = "";
            }
            com.navdy.service.library.events.DeviceInfo phoneInfo = new com.navdy.service.library.events.DeviceInfo.Builder().deviceId(deviceId.toString()).clientVersion(com.navdy.client.BuildConfig.VERSION_NAME).protocolVersion(com.navdy.service.library.Version.PROTOCOL_VERSION.toString()).deviceName(deviceName).systemVersion(getSystemVersion()).model(android.os.Build.MODEL).deviceUuid("UUID").systemApiLevel(java.lang.Integer.valueOf(android.os.Build.VERSION.SDK_INT)).kernelVersion(java.lang.System.getProperty("os.version")).platform(com.navdy.service.library.events.DeviceInfo.Platform.PLATFORM_Android).buildType(android.os.Build.TYPE).deviceMake(android.os.Build.MANUFACTURER).capabilities(capabilities).build();
            sLogger.v("sending device info:" + phoneInfo);
            device.postEvent((com.squareup.wire.Message) phoneInfo);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private java.lang.String getSystemVersion() {
        if (!java.lang.String.valueOf(1).equals(android.os.Build.VERSION.INCREMENTAL)) {
            return com.navdy.client.BuildConfig.VERSION_NAME;
        }
        return android.os.Build.VERSION.INCREMENTAL;
    }

    private void sendCurrentTime(com.navdy.service.library.device.RemoteDevice remoteDevice, boolean is24HourFormat) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.settings.DateTimeConfiguration(java.lang.Long.valueOf(calendar.getTimeInMillis()), calendar.getTimeZone().getID(), is24HourFormat ? com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_24_HOUR : com.navdy.service.library.events.settings.DateTimeConfiguration.Clock.CLOCK_12_HOUR));
    }

    public synchronized void onConnected(@android.support.annotation.Nullable android.os.Bundle bundle) {
        sLogger.d("GoogleApiClient : connected ");
        if (isDeviceConnected()) {
            sLogger.d("Remote device is already connected");
            requestActivityRecognitionUpdates();
        }
    }

    public void onConnectionSuspended(int i) {
        sLogger.d("GoogleApiClient : onConnectionSuspended , Connection :" + i);
    }

    public void onConnectionFailed(@android.support.annotation.NonNull com.google.android.gms.common.ConnectionResult connectionResult) {
        sLogger.d("GoogleApiClient : onConnectionFailed , Connection result : " + connectionResult);
    }
}
