package com.navdy.client.app.framework.models;

public class TransparentCircle extends android.view.View {
    private static final boolean VERBOSE = false;
    private android.graphics.Bitmap bitmap;
    private android.graphics.Canvas canvas;
    private android.graphics.Paint eraser;
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.TransparentCircle.class);

    public TransparentCircle(android.content.Context context) {
        super(context);
        initPaintObjectAsEraser();
    }

    public TransparentCircle(android.content.Context context, android.util.AttributeSet attrs) {
        super(context, attrs);
        initPaintObjectAsEraser();
    }

    public TransparentCircle(android.content.Context context, android.util.AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initPaintObjectAsEraser();
    }

    private void initPaintObjectAsEraser() {
        this.eraser = new android.graphics.Paint();
        this.eraser.setXfermode(new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.CLEAR));
        this.eraser.setAntiAlias(true);
    }

    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        if (!(width == oldWidth && height == oldHeight)) {
            this.bitmap = android.graphics.Bitmap.createBitmap(width, height, android.graphics.Bitmap.Config.ARGB_8888);
            this.canvas = new android.graphics.Canvas(this.bitmap);
        }
        super.onSizeChanged(width, height, oldWidth, oldHeight);
    }

    protected void onDraw(android.graphics.Canvas canvas2) {
        int radius;
        int width = getWidth();
        int height = getHeight();
        if (width > height) {
            radius = height / 2;
        } else {
            radius = width / 2;
        }
        this.bitmap.eraseColor(0);
        this.canvas.drawColor(-16777216);
        this.canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) radius, this.eraser);
        canvas2.drawBitmap(this.bitmap, 0.0f, 0.0f, null);
        super.onDraw(canvas2);
    }
}
