package com.navdy.client.app.framework.util;

public class VoiceCommandUtils {
    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    public static com.navdy.client.app.framework.util.VoiceCommand getMatchingVoiceCommand(@android.support.annotation.RawRes int rawRes, @android.support.annotation.Nullable java.lang.String query) {
        com.navdy.client.app.framework.util.VoiceCommand vc = null;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
            java.io.InputStream inputStream = null;
            java.util.Scanner sc = null;
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            java.lang.String lowercaseQuery = query.toLowerCase().trim();
            try {
                inputStream = context.getResources().openRawResource(rawRes);
                java.util.Scanner sc2 = new java.util.Scanner(inputStream, "UTF-8");
                while (true) {
                    try {
                        if (!sc2.hasNextLine()) {
                            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                            com.navdy.service.library.util.IOUtils.closeStream(sc2);
                            break;
                        }
                        java.lang.String line = sc2.nextLine();
                        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(line)) {
                            java.lang.String line2 = line.toLowerCase().replaceAll("\\{.*?\\}", "(.*?)");
                            java.util.regex.Matcher matcher = java.util.regex.Pattern.compile(line2).matcher(lowercaseQuery);
                            if (matcher.matches() && matcher.groupCount() == 1) {
                                vc = new com.navdy.client.app.framework.util.VoiceCommand();
                                vc.prefix = line2;
                                vc.query = matcher.group(1);
                                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                                com.navdy.service.library.util.IOUtils.closeStream(sc2);
                                break;
                            }
                        }
                    } catch (Throwable th) {
                        th = th;
                        sc = sc2;
                        com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                        com.navdy.service.library.util.IOUtils.closeStream(sc);
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                com.navdy.service.library.util.IOUtils.closeStream(sc);
                throw th;
            }
        }
        return vc;
    }

    @android.support.annotation.CheckResult
    public static boolean isOneOfTheseVoiceCommands(@android.support.annotation.RawRes int CommandListRawRes, @android.support.annotation.Nullable java.lang.String query) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(query)) {
            return false;
        }
        java.io.InputStream inputStream = null;
        java.util.Scanner sc = null;
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        java.lang.String lowercaseQuery = query.toLowerCase().trim();
        try {
            inputStream = context.getResources().openRawResource(CommandListRawRes);
            java.util.Scanner sc2 = new java.util.Scanner(inputStream, "UTF-8");
            while (sc2.hasNextLine()) {
                try {
                    java.lang.String line = sc2.nextLine();
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(line) && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(line.toLowerCase(), lowercaseQuery)) {
                        com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                        com.navdy.service.library.util.IOUtils.closeStream(sc2);
                        return true;
                    }
                } catch (Throwable th) {
                    th = th;
                    sc = sc2;
                    com.navdy.service.library.util.IOUtils.closeStream(inputStream);
                    com.navdy.service.library.util.IOUtils.closeStream(sc);
                    throw th;
                }
            }
            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            com.navdy.service.library.util.IOUtils.closeStream(sc2);
            return false;
        } catch (Throwable th2) {
            th = th2;
            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
            com.navdy.service.library.util.IOUtils.closeStream(sc);
            throw th;
        }
    }

    public static void addHomeAndWorkIfMatch(@android.support.annotation.NonNull java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinationsFromDatabase, @android.support.annotation.Nullable java.lang.String query) {
        if (isHome(query)) {
            com.navdy.client.app.framework.models.Destination homeDestination = com.navdy.client.app.providers.NavdyContentProvider.getHome();
            if (homeDestination != null) {
                destinationsFromDatabase.add(0, homeDestination);
            }
        } else if (isWork(query)) {
            com.navdy.client.app.framework.models.Destination workDestination = com.navdy.client.app.providers.NavdyContentProvider.getWork();
            if (workDestination != null) {
                destinationsFromDatabase.add(0, workDestination);
            }
        }
    }

    public static boolean isHome(@android.support.annotation.Nullable java.lang.String query) {
        return isOneOfTheseVoiceCommands(com.navdy.client.R.raw.home_strings, query);
    }

    public static boolean isWork(@android.support.annotation.Nullable java.lang.String query) {
        return isOneOfTheseVoiceCommands(com.navdy.client.R.raw.work_strings, query);
    }
}
