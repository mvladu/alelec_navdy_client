package com.navdy.client.app.framework.map;

public class HereMapsConfigurator {
    private static final com.navdy.client.app.framework.map.HereMapsConfigurator sInstance = new com.navdy.client.app.framework.map.HereMapsConfigurator();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.map.HereMapsConfigurator.class);
    private volatile boolean isAlreadyConfigured;

    public static com.navdy.client.app.framework.map.HereMapsConfigurator getInstance() {
        return sInstance;
    }

    @android.support.annotation.WorkerThread
    public synchronized void updateMapsConfig() {
        if (this.isAlreadyConfigured) {
            sLogger.i("MWConfig is already configured");
        } else if (!com.navdy.client.app.framework.PathManager.getInstance().hasDiskCacheDir()) {
            sLogger.i("diskcache does not exist");
        } else {
            com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            java.util.List<java.lang.String> hereMapsConfigFolders = com.navdy.client.app.framework.PathManager.getInstance().getHereMapsConfigDirs();
            java.lang.String latestHereMapConfigFolder = com.navdy.client.app.framework.PathManager.getInstance().getLatestHereMapsConfigPath();
            com.navdy.client.app.framework.util.PackagedResource mwConfigLatest = new com.navdy.client.app.framework.util.PackagedResource("mwconfig_client", latestHereMapConfigFolder);
            try {
                sLogger.i("MWConfig: starting");
                long l1 = android.os.SystemClock.elapsedRealtime();
                mwConfigLatest.updateFromResources(context, com.navdy.client.R.raw.mwconfig_client, com.navdy.client.R.raw.mwconfig_client_checksum);
                removeOldHereMapsFolders(hereMapsConfigFolders, latestHereMapConfigFolder);
                sLogger.i("MWConfig: time took " + (android.os.SystemClock.elapsedRealtime() - l1));
                this.isAlreadyConfigured = true;
            } catch (Throwable throwable) {
                sLogger.e("MWConfig error", throwable);
            }
        }
        return;
    }

    private void removeOldHereMapsFolders(java.util.List<java.lang.String> hereMapsConfigFolders, java.lang.String latestHereMapConfigFolder) {
        for (java.lang.String hereMapsConfigFolder : hereMapsConfigFolders) {
            if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(hereMapsConfigFolder, latestHereMapConfigFolder)) {
                com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.client.app.NavdyApplication.getAppContext(), new java.io.File(hereMapsConfigFolder));
            }
        }
    }
}
