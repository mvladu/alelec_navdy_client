package com.navdy.client.app.framework.util;

public final class SystemUtils {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("SystemUtils");

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.Runnable val$runnable;

        Anon1(java.lang.Runnable runnable) {
            this.val$runnable = runnable;
        }

        public void run() {
            this.val$runnable.run();
        }
    }

    public static class CallOnMainThreadException extends java.lang.RuntimeException {
    }

    private SystemUtils() {
    }

    public static void dismissKeyboard(android.app.Activity activity) {
        sLogger.v("dismissKeyboard() called");
        android.view.inputmethod.InputMethodManager inputManager = (android.view.inputmethod.InputMethodManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("input_method");
        if (inputManager.isAcceptingText()) {
            sLogger.v("keyboard is accepting text");
            try {
                android.view.View currentFocus = activity.getCurrentFocus();
                if (currentFocus != null) {
                    inputManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
                }
            } catch (java.lang.Exception e) {
                sLogger.v("SystemUtils failed hide keyboard");
            }
        }
    }

    public static void closeCursor(android.database.Cursor cursor) {
        if (cursor != null) {
            try {
                cursor.close();
            } catch (Throwable throwable) {
                sLogger.e(throwable);
            }
        }
    }

    @android.support.annotation.MainThread
    public static void ensureOnMainThread() {
        if (!isOnUiThread()) {
            throw new com.navdy.client.app.framework.util.SystemUtils.CallOnMainThreadException();
        }
    }

    @android.support.annotation.WorkerThread
    public static void ensureNotOnMainThread() {
        if (isOnUiThread()) {
            throw new com.navdy.client.app.framework.util.SystemUtils.CallOnMainThreadException();
        }
    }

    public static boolean isOnUiThread() {
        return android.os.Looper.myLooper() == android.os.Looper.getMainLooper();
    }

    @android.support.annotation.CheckResult
    public static long convertToNumber(java.lang.String s) {
        return java.lang.Long.parseLong(s.replaceAll("[^0-9\\._]+", ""));
    }

    public static void sleep(int millis) {
        try {
            java.lang.Thread.sleep((long) millis);
        } catch (Throwable th) {
            sLogger.e("Unable to sleep!");
        }
    }

    public boolean canUseMaterialAnimations() {
        return android.os.Build.VERSION.SDK_INT >= 21;
    }

    public static void goToSystemSettingsAppInfoForOurApp(android.app.Activity activity) {
        if (com.navdy.client.app.ui.base.BaseActivity.isEnding(activity)) {
            sLogger.e("Can't call goToSystemSettingsAppInfoForOurApp while activity is ending.");
            return;
        }
        android.content.Intent intent = new android.content.Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setData(android.net.Uri.parse("package:" + activity.getPackageName()));
        activity.startActivity(intent);
    }

    public static boolean isAirplaneModeOn(android.content.Context context) {
        return android.provider.Settings.Global.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
    }

    public static boolean isPackageInstalled(java.lang.String packageName) {
        try {
            com.navdy.client.app.NavdyApplication.getAppContext().getPackageManager().getPackageInfo(packageName, 1);
            return true;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void logIntentExtras(android.content.Intent intent, com.navdy.service.library.log.Logger logger) {
        android.os.Bundle bundle = intent.getExtras();
        if (bundle != null) {
            for (java.lang.String key : bundle.keySet()) {
                logger.d("extra: [" + key + "=" + bundle.get(key) + "]");
            }
        }
    }

    public static int getTimeZoneAndDaylightSavingOffset(java.lang.Long timestamp) {
        return java.util.Calendar.getInstance().getTimeZone().getOffset(timestamp.longValue());
    }

    public static void runOnUiThread(java.lang.Runnable runnable) {
        if (isOnUiThread()) {
            runnable.run();
        } else {
            new android.os.Handler(android.os.Looper.getMainLooper()).post(new com.navdy.client.app.framework.util.SystemUtils.Anon1(runnable));
        }
    }
}
