package com.navdy.client.app.framework.i18n;

public class AddressUtils {
    public static final java.lang.String ADDRESS_SEPARATOR = ",";
    private static final java.lang.String CITY = "city";
    private static final java.lang.String COUNTRIES = "countries";
    private static final java.lang.String COUNTRY = "country";
    public static final java.lang.String DEFAULT_COUNTRY_CODE = "US";
    private static final java.lang.String FORMAT = "format";
    private static final java.lang.String FORMATS = "formats";
    private static final java.lang.String ISO_3166_1_ALPHA_3 = "ISO 3166-1-a3";
    private static final java.lang.String ISO_3166_2 = "ISO_3166-2";
    private static final java.lang.String NUMBER = "number";
    private static final java.lang.String STATE = "state";
    private static final java.lang.String STREET = "street";
    private static final java.lang.String ZIP = "zip";
    private static final java.util.HashMap<java.lang.String, com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat> formats = new java.util.HashMap<>();
    private static final java.util.Map<java.lang.String, java.lang.String> iso3ToIso2 = new java.util.HashMap();
    private static final java.util.HashMap<java.lang.String, java.lang.String> isoToFormatString = new java.util.HashMap<>();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.i18n.AddressUtils.class);

    public static class AddressFormat {
        public java.util.ArrayList<java.lang.String> foreignLines = new java.util.ArrayList<>();
        public java.util.ArrayList<java.lang.String> localLines = new java.util.ArrayList<>();

        static com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat getClone(com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat original) {
            if (original == null) {
                return null;
            }
            com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat af = new com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat();
            af.localLines = new java.util.ArrayList<>(original.localLines);
            af.foreignLines = new java.util.ArrayList<>(original.foreignLines);
            return af;
        }
    }

    static void initAddressFormats() {
        android.util.JsonReader reader = null;
        try {
            android.util.JsonReader reader2 = new android.util.JsonReader(new java.io.InputStreamReader(com.navdy.client.app.NavdyApplication.getAppContext().getResources().openRawResource(com.navdy.client.R.raw.address_formats)));
            try {
                reader2.beginObject();
                while (reader2.hasNext()) {
                    java.lang.String name = reader2.nextName();
                    char c = 65535;
                    switch (name.hashCode()) {
                        case -677443748:
                            if (name.equals(FORMATS)) {
                                c = 1;
                                break;
                            }
                            break;
                        case 1352637108:
                            if (name.equals(COUNTRIES)) {
                                c = 0;
                                break;
                            }
                            break;
                    }
                    switch (c) {
                        case 0:
                            readCountries(reader2);
                            break;
                        case 1:
                            readFormats(reader2);
                            break;
                        default:
                            reader2.skipValue();
                            break;
                    }
                }
                reader2.endObject();
                com.navdy.service.library.util.IOUtils.closeStream(reader2);
            } catch (java.lang.Exception e) {
                e = e;
                reader = reader2;
                try {
                    logger.e("Exception found: " + e);
                    throw new java.lang.RuntimeException(e);
                } catch (Throwable th) {
                    th = th;
                    com.navdy.service.library.util.IOUtils.closeStream(reader);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                reader = reader2;
                com.navdy.service.library.util.IOUtils.closeStream(reader);
                throw th;
            }
        } catch (java.lang.Exception e2) {
            e = e2;
            logger.e("Exception found: " + e);
            throw new java.lang.RuntimeException(e);
        }
    }

    private static void readCountries(android.util.JsonReader reader) throws java.io.IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            java.lang.String iso2 = null;
            java.lang.String iso3 = null;
            java.lang.String format = null;
            while (reader.hasNext()) {
                java.lang.String name = reader.nextName();
                char c = 65535;
                switch (name.hashCode()) {
                    case -1268779017:
                        if (name.equals(FORMAT)) {
                            c = 2;
                            break;
                        }
                        break;
                    case -991880995:
                        if (name.equals(ISO_3166_2)) {
                            c = 0;
                            break;
                        }
                        break;
                    case -256260126:
                        if (name.equals(ISO_3166_1_ALPHA_3)) {
                            c = 1;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        iso2 = reader.nextString();
                        break;
                    case 1:
                        iso3 = reader.nextString();
                        break;
                    case 2:
                        format = reader.nextString();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(format)) {
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(iso2)) {
                    isoToFormatString.put(iso2, format);
                }
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(iso3)) {
                    isoToFormatString.put(iso3, format);
                }
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(iso2) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(iso3)) {
                    iso3ToIso2.put(iso3, iso2);
                }
            }
            reader.endObject();
        }
        reader.endArray();
    }

    private static void readFormats(android.util.JsonReader reader) throws java.io.IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            reader.beginObject();
            java.lang.String formatName = reader.nextName();
            com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat format = new com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat();
            reader.beginObject();
            while (reader.hasNext()) {
                java.lang.String lineName = reader.nextName();
                char c = 65535;
                switch (lineName.hashCode()) {
                    case 199508948:
                        if (lineName.equals("foreign_lines")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 1753027883:
                        if (lineName.equals("local_lines")) {
                            c = 0;
                            break;
                        }
                        break;
                }
                switch (c) {
                    case 0:
                        reader.beginArray();
                        while (reader.hasNext()) {
                            java.lang.String localLine = reader.nextString();
                            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(localLine)) {
                                format.localLines.add(localLine);
                            }
                        }
                        reader.endArray();
                        break;
                    case 1:
                        reader.beginArray();
                        while (reader.hasNext()) {
                            java.lang.String foreignLine = reader.nextString();
                            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(foreignLine)) {
                                format.foreignLines.add(foreignLine);
                            }
                        }
                        reader.endArray();
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
            reader.endObject();
            reader.endObject();
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(formatName)) {
                formats.put(formatName, format);
            }
        }
        reader.endArray();
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    private static java.lang.String getFormatStringFor(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        if (!destination.hasDetailedAddress() || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destination.getCountryCode())) {
            return null;
        }
        return (java.lang.String) isoToFormatString.get(destination.getCountryCode());
    }

    @android.support.annotation.Nullable
    @android.support.annotation.CheckResult
    public static com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat getAddressFormatFor(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat format = null;
        java.lang.String formatString = getFormatStringFor(destination);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(formatString)) {
            format = com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat.getClone((com.navdy.client.app.framework.i18n.AddressUtils.AddressFormat) formats.get(formatString));
            if (format != null) {
            }
        }
        return format;
    }

    @android.support.annotation.CheckResult
    @android.support.annotation.NonNull
    public static java.lang.String insertAddressPartsInFormat(@android.support.annotation.NonNull java.lang.String line, @android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination d) {
        return com.navdy.client.app.framework.util.StringUtils.replaceOrErase(com.navdy.client.app.framework.util.StringUtils.replaceOrErase(com.navdy.client.app.framework.util.StringUtils.replaceOrErase(com.navdy.client.app.framework.util.StringUtils.replaceOrErase(com.navdy.client.app.framework.util.StringUtils.replaceOrErase(com.navdy.client.app.framework.util.StringUtils.replaceOrErase(line, NUMBER, d.getStreetNumber()), STREET, d.getStreetName()), ZIP, d.getZipCode()), "city", d.getCity()), "state", d.getState()), "country", d.getCountry());
    }

    @android.support.annotation.CheckResult
    @android.support.annotation.NonNull
    public static java.lang.String joinRemainingLines(java.util.ArrayList<java.lang.String> lines, int startAt) {
        if (lines.size() > startAt) {
            return com.navdy.client.app.framework.util.StringUtils.replaceAllSafely(com.navdy.client.app.framework.util.StringUtils.join(lines.subList(startAt, lines.size()), org.droidparts.contract.SQL.DDL.SEPARATOR), ", *, *", org.droidparts.contract.SQL.DDL.SEPARATOR).trim();
        }
        return "";
    }

    @android.support.annotation.CheckResult
    public static java.lang.String sanitizeAddress(java.lang.String address) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address)) {
            return address;
        }
        java.lang.String address2 = address.replaceAll("\n", org.droidparts.contract.SQL.DDL.SEPARATOR).trim();
        if (address2.endsWith(",")) {
            return address2.substring(0, address2.length() - ",".length());
        }
        return address2;
    }

    public static android.util.Pair<java.lang.String, java.lang.String> cleanUpDebugPrefix(android.util.Pair<java.lang.String, java.lang.String> addressPair) {
        return addressPair;
    }

    private static java.lang.String cleanUpDebugPrefix(@android.support.annotation.Nullable java.lang.String line) {
        return line;
    }

    public static boolean isTitleIsInTheAddress(@android.support.annotation.Nullable java.lang.String destinationTitle, @android.support.annotation.Nullable java.lang.String address) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(destinationTitle) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address)) {
            return false;
        }
        return address.startsWith(destinationTitle);
    }

    @android.support.annotation.Nullable
    public static java.lang.String convertIso3ToIso2(@android.support.annotation.NonNull java.lang.String iso3) {
        return (java.lang.String) iso3ToIso2.get(iso3);
    }

    @android.support.annotation.Nullable
    public static java.lang.String convertIso2ToIso3(@android.support.annotation.NonNull java.lang.String iso2) {
        for (java.util.Map.Entry<java.lang.String, java.lang.String> entries : iso3ToIso2.entrySet()) {
            if (android.text.TextUtils.equals((java.lang.String) entries.getValue(), iso2)) {
                return (java.lang.String) entries.getKey();
            }
        }
        return iso2;
    }
}
