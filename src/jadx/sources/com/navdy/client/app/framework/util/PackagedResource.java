package com.navdy.client.app.framework.util;

public class PackagedResource {
    private static final int BUFFER_SIZE = 16384;
    private static final java.lang.String MD5_SUFFIX = ".md5";
    private static final java.lang.Object lockObject = new java.lang.Object();
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.PackagedResource.class);
    private java.lang.String basePath;
    private java.lang.String destinationFilename;

    public PackagedResource(java.lang.String destinationFilename2, java.lang.String basePath2) {
        this.destinationFilename = destinationFilename2;
        this.basePath = basePath2;
    }

    public void updateFromResources(android.content.Context context, int resId, int hashResId) throws java.lang.Throwable {
        updateFromResources(context, resId, hashResId, false);
    }

    public void updateFromResources(android.content.Context context, int resId, int hashResId, boolean unzip) throws java.lang.Throwable {
        java.lang.String md5InApk;
        synchronized (lockObject) {
            boolean copyFile = true;
            java.io.InputStream md5InputStream = null;
            android.content.res.Resources resources = context.getResources();
            java.io.File md5File = new java.io.File(this.basePath, this.destinationFilename + MD5_SUFFIX);
            java.io.File configFile = new java.io.File(this.basePath, this.destinationFilename);
            try {
                java.io.InputStream md5InputStream2 = resources.openRawResource(hashResId);
                md5InApk = com.navdy.service.library.util.IOUtils.convertInputStreamToString(md5InputStream2, "UTF-8");
                com.navdy.service.library.util.IOUtils.closeStream(md5InputStream2);
                md5InputStream = null;
                if (!md5File.exists() || (!unzip && !configFile.exists())) {
                    sLogger.d("update required, no file");
                } else {
                    java.lang.String md5OnDevice = com.navdy.service.library.util.IOUtils.convertFileToString(md5File.getAbsolutePath());
                    if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(md5OnDevice, md5InApk)) {
                        copyFile = false;
                        sLogger.d("no update required signature matches:" + md5OnDevice);
                    } else {
                        sLogger.d("update required, device:" + md5OnDevice + " apk:" + md5InApk);
                    }
                }
            } catch (Throwable th) {
                com.navdy.service.library.util.IOUtils.closeStream(md5InputStream);
                throw th;
            }
            if (!copyFile) {
                com.navdy.service.library.util.IOUtils.closeStream(null);
                return;
            }
            com.navdy.service.library.util.IOUtils.copyFile(configFile.getAbsolutePath(), resources.openRawResource(resId));
            if (unzip) {
                unpackZip(this.basePath, configFile.getName());
                com.navdy.service.library.util.IOUtils.deleteFile(context, configFile.getAbsolutePath());
            }
            com.navdy.service.library.util.IOUtils.copyFile(md5File.getAbsolutePath(), md5InApk.getBytes());
            com.navdy.service.library.util.IOUtils.closeStream(null);
        }
    }

    public static boolean unpackZipFromResource(java.lang.String path, int resId) {
        java.io.InputStream is = null;
        try {
            is = com.navdy.client.app.NavdyApplication.getAppContext().getResources().openRawResource(resId);
            return unpack(path, is);
        } catch (Throwable e) {
            sLogger.e("Error reading resource", e);
            return false;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(is);
        }
    }

    public static boolean unpackZip(java.lang.String path, java.lang.String zipname) {
        java.io.InputStream is = null;
        try {
            java.io.InputStream is2 = new java.io.FileInputStream(path + java.io.File.separator + zipname);
            try {
                boolean unpack = unpack(path, is2);
                com.navdy.service.library.util.IOUtils.closeStream(is2);
                java.io.FileInputStream fileInputStream = is2;
                return unpack;
            } catch (Throwable th) {
                th = th;
                is = is2;
                com.navdy.service.library.util.IOUtils.closeStream(is);
                throw th;
            }
        } catch (Throwable th2) {
            e = th2;
            sLogger.e("Error reading resource", e);
            com.navdy.service.library.util.IOUtils.closeStream(is);
            return false;
        }
    }

    private static boolean unpack(java.lang.String path, java.io.InputStream is) {
        java.io.File destDir = new java.io.File(path);
        byte[] buffer = new byte[16384];
        if (!destDir.exists() && !destDir.mkdirs()) {
            sLogger.e("unpack could not create destination dirs");
        }
        java.util.zip.ZipInputStream zis = new java.util.zip.ZipInputStream(new java.io.BufferedInputStream(is));
        while (true) {
            try {
                java.util.zip.ZipEntry ze = zis.getNextEntry();
                if (ze != null) {
                    java.lang.String filePath = path + java.io.File.separator + ze.getName();
                    if (!ze.isDirectory()) {
                        extractFile(zis, filePath, buffer);
                    } else if (!new java.io.File(filePath).mkdirs()) {
                        sLogger.e("unpack could not create destination dirs");
                    }
                    zis.closeEntry();
                } else {
                    com.navdy.service.library.util.IOUtils.closeStream(zis);
                    return true;
                }
            } catch (Throwable th) {
                com.navdy.service.library.util.IOUtils.closeStream(zis);
                throw th;
            }
        }
    }

    private static void extractFile(java.util.zip.ZipInputStream zis, java.lang.String filePath, byte[] buffer) {
        java.io.FileOutputStream fout = null;
        try {
            java.io.FileOutputStream fout2 = new java.io.FileOutputStream(filePath);
            while (true) {
                try {
                    int count = zis.read(buffer);
                    if (count != -1) {
                        fout2.write(buffer, 0, count);
                    } else {
                        com.navdy.service.library.util.IOUtils.fileSync(fout2);
                        com.navdy.service.library.util.IOUtils.closeStream(fout2);
                        java.io.FileOutputStream fileOutputStream = fout2;
                        return;
                    }
                } catch (Throwable th) {
                    th = th;
                    fout = fout2;
                    com.navdy.service.library.util.IOUtils.closeStream(fout);
                    throw th;
                }
            }
        } catch (Throwable th2) {
            t = th2;
            sLogger.e("error extracting file", t);
            com.navdy.service.library.util.IOUtils.closeStream(fout);
        }
    }
}
