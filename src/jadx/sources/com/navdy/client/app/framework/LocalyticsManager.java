package com.navdy.client.app.framework;

public class LocalyticsManager {
    private static volatile com.navdy.client.app.framework.LocalyticsManager instance = null;
    private java.util.concurrent.atomic.AtomicBoolean localyticsIsInitialized = new java.util.concurrent.atomic.AtomicBoolean(false);
    private java.util.ArrayList<java.lang.Runnable> localyticsPendingTasks = new java.util.ArrayList<>();
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.LocalyticsManager.class);

    static class Anon1 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$screen;

        Anon1(java.lang.String str) {
            this.val$screen = str;
        }

        public void run() {
            com.localytics.android.Localytics.tagScreen(this.val$screen);
        }
    }

    static class Anon10 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$key;
        final /* synthetic */ java.lang.String[] val$value;

        Anon10(java.lang.String str, java.lang.String[] strArr) {
            this.val$key = str;
            this.val$value = strArr;
        }

        public void run() {
            com.localytics.android.Localytics.setProfileAttribute(this.val$key, this.val$value);
        }
    }

    static class Anon11 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$key;
        final /* synthetic */ long[] val$value;

        Anon11(java.lang.String str, long[] jArr) {
            this.val$key = str;
            this.val$value = jArr;
        }

        public void run() {
            com.localytics.android.Localytics.setProfileAttribute(this.val$key, this.val$value);
        }
    }

    static class Anon12 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$key;
        final /* synthetic */ java.util.Date[] val$value;

        Anon12(java.lang.String str, java.util.Date[] dateArr) {
            this.val$key = str;
            this.val$value = dateArr;
        }

        public void run() {
            com.localytics.android.Localytics.setProfileAttribute(this.val$key, this.val$value);
        }
    }

    static class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$eventName;

        Anon2(java.lang.String str) {
            this.val$eventName = str;
        }

        public void run() {
            com.localytics.android.Localytics.tagEvent(this.val$eventName);
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ java.util.Map val$attributes;
        final /* synthetic */ java.lang.String val$eventName;

        Anon3(java.lang.String str, java.util.Map map) {
            this.val$eventName = str;
            this.val$attributes = map;
        }

        public void run() {
            com.localytics.android.Localytics.tagEvent(this.val$eventName, this.val$attributes);
        }
    }

    static class Anon4 implements java.lang.Runnable {
        final /* synthetic */ java.util.Map val$attributes;
        final /* synthetic */ long val$customerValueIncrease;
        final /* synthetic */ java.lang.String val$eventName;

        Anon4(java.lang.String str, java.util.Map map, long j) {
            this.val$eventName = str;
            this.val$attributes = map;
            this.val$customerValueIncrease = j;
        }

        public void run() {
            com.localytics.android.Localytics.tagEvent(this.val$eventName, this.val$attributes, this.val$customerValueIncrease);
        }
    }

    static class Anon5 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$email;

        Anon5(java.lang.String str) {
            this.val$email = str;
        }

        public void run() {
            com.localytics.android.Localytics.setCustomerEmail(this.val$email);
        }
    }

    static class Anon6 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$fullName;

        Anon6(java.lang.String str) {
            this.val$fullName = str;
        }

        public void run() {
            com.localytics.android.Localytics.setCustomerFullName(this.val$fullName);
        }
    }

    static class Anon7 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$key;
        final /* synthetic */ java.lang.String val$value;

        Anon7(java.lang.String str, java.lang.String str2) {
            this.val$key = str;
            this.val$value = str2;
        }

        public void run() {
            com.localytics.android.Localytics.setProfileAttribute(this.val$key, this.val$value);
        }
    }

    static class Anon8 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$key;
        final /* synthetic */ long val$value;

        Anon8(java.lang.String str, long j) {
            this.val$key = str;
            this.val$value = j;
        }

        public void run() {
            com.localytics.android.Localytics.setProfileAttribute(this.val$key, this.val$value);
        }
    }

    static class Anon9 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$key;
        final /* synthetic */ java.util.Date val$value;

        Anon9(java.lang.String str, java.util.Date date) {
            this.val$key = str;
            this.val$value = date;
        }

        public void run() {
            com.localytics.android.Localytics.setProfileAttribute(this.val$key, this.val$value);
        }
    }

    private LocalyticsManager() {
    }

    public static com.navdy.client.app.framework.LocalyticsManager getInstance() {
        if (instance == null) {
            instance = new com.navdy.client.app.framework.LocalyticsManager();
        }
        return instance;
    }

    public synchronized void initLocalytics() {
        this.logger.v("initializing Localytics");
        com.localytics.android.Localytics.integrate(com.navdy.client.app.NavdyApplication.getAppContext());
        com.localytics.android.Localytics.registerPush(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.google_cloud_sender_id));
        this.localyticsIsInitialized.set(true);
        if (this.localyticsPendingTasks.size() > 0) {
            this.logger.v("Localytics is initialized. Now running " + this.localyticsPendingTasks.size() + " pending tasks.");
        } else {
            this.logger.v("Localytics is initialized. There are no pending tasks.");
        }
        java.util.Iterator it = this.localyticsPendingTasks.iterator();
        while (it.hasNext()) {
            ((java.lang.Runnable) it.next()).run();
        }
    }

    private synchronized void addToLocalyticsPendingTasks(java.lang.Runnable r) {
        this.localyticsPendingTasks.add(r);
        this.logger.v("Added 1 new task to the list of tasks waiting for Localytics initialization. The queue now has " + this.localyticsPendingTasks.size() + " pending tasks.");
    }

    public static void runWhenReady(java.lang.Runnable r) {
        com.navdy.client.app.framework.LocalyticsManager lm = getInstance();
        if (lm.localyticsIsInitialized.get()) {
            r.run();
        } else {
            lm.addToLocalyticsPendingTasks(r);
        }
    }

    public static void tagScreen(java.lang.String screen) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon1(screen));
    }

    public static void tagEvent(java.lang.String eventName) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon2(eventName));
    }

    public static void tagEvent(java.lang.String eventName, java.util.Map<java.lang.String, java.lang.String> attributes) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon3(eventName, attributes));
    }

    public static void tagEvent(java.lang.String eventName, java.util.Map<java.lang.String, java.lang.String> attributes, long customerValueIncrease) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon4(eventName, attributes, customerValueIncrease));
    }

    public static void setCustomerEmail(java.lang.String email) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon5(email));
    }

    public static void setCustomerFullName(java.lang.String fullName) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon6(fullName));
    }

    public static void setProfileAttribute(java.lang.String key, java.lang.String value) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon7(key, value));
    }

    public static void setProfileAttribute(java.lang.String key, long value) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon8(key, value));
    }

    public static void setProfileAttribute(java.lang.String key, java.util.Date value) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon9(key, value));
    }

    public static void setProfileAttribute(java.lang.String key, java.lang.String[] value) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon10(key, value));
    }

    public static void setProfileAttribute(java.lang.String key, long[] value) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon11(key, value));
    }

    public static void setProfileAttribute(java.lang.String key, java.util.Date[] value) {
        runWhenReady(new com.navdy.client.app.framework.LocalyticsManager.Anon12(key, value));
    }
}
