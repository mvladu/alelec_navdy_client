package com.navdy.client.app.framework.glances;

public class MessagingNotificationHandler {
    private static com.navdy.service.library.log.Logger sLogger = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger;

    public static void handleMessageNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        if (com.navdy.client.app.ui.glances.GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            char c = 65535;
            switch (packageName.hashCode()) {
                case -1547699361:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.WHATS_APP)) {
                        c = 2;
                        break;
                    }
                    break;
                case 908140028:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.FACEBOOK_MESSENGER)) {
                        c = 3;
                        break;
                    }
                    break;
                case 979613891:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.SLACK)) {
                        c = 1;
                        break;
                    }
                    break;
                case 1515426419:
                    if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_HANGOUTS)) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    handleGoogleTalkNotification(sbn);
                    return;
                case 1:
                    handleSlackNotification(sbn);
                    return;
                case 2:
                    handleWhatsAppNotification(sbn);
                    return;
                case 3:
                    handleFacebookMessengerNotification(sbn);
                    return;
                default:
                    sLogger.w("message notification not handled [" + packageName + "]");
                    return;
            }
        }
    }

    private static void handleGoogleTalkNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String body;
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        java.lang.String tag = sbn.getTag();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(tag)) {
            java.lang.String tag2 = tag.toLowerCase();
            if (tag2.contains(":sms:")) {
                sLogger.v("navdyinfo-hangout is sms[" + tag2 + "]");
                return;
            } else if (!tag2.contains(":")) {
                sLogger.v("navdyinfo-hangout is sms[" + tag2 + "]");
                return;
            }
        }
        if (notification.tickerText != null) {
            java.lang.String contactName = "";
            java.util.regex.Matcher matcher = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.splitTickerTextOnColon.matcher(notification.tickerText.toString());
            if (!matcher.matches() || matcher.groupCount() < 2) {
                body = notification.tickerText.toString();
            } else {
                contactName = matcher.group(1);
                body = matcher.group(2);
            }
            sLogger.d("[navdyinfo-hangout] hangout message received sender[" + contactName + "] message[" + body + "]");
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice == null) {
                sLogger.v("[navdyinfo-hangout]  lost connection");
            } else if (!com.navdy.client.app.framework.glances.GlancesHelper.isHudVersionCompatible(remoteDevice.getDeviceInfo().protocolVersion)) {
                remoteDevice.postEvent((com.squareup.wire.Message) com.navdy.client.debug.util.NotificationBuilder.buildSmsNotification(contactName, contactName, body, true));
            } else {
                java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
                java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), contactName));
                data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), body));
                com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
            }
        }
    }

    private static void handleSlackNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        android.os.Bundle extras = android.support.v7.app.NotificationCompat.getExtras(notification);
        java.lang.String from = extras.getString("android.title");
        java.lang.String message = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        java.lang.String domain = extras.getString("android.summaryText");
        sLogger.v("[navdyinfo-slack] from[" + from + "] domain[" + domain + "] message[" + message + "]");
        java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), message));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_DOMAIN.name(), domain));
        com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
    }

    private static void handleWhatsAppNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        android.os.Bundle extras = android.support.v7.app.NotificationCompat.getExtras(notification);
        java.lang.String from = extras.getString("android.title");
        java.lang.String message = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        java.lang.String firstParticipantInCarConversation = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getFirstParticipantInCarConversation(extras);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(firstParticipantInCarConversation)) {
            from = firstParticipantInCarConversation;
        }
        java.lang.String lastLine = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getLastTextLine(extras);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(lastLine)) {
            message = lastLine;
        } else {
            java.lang.String lastMessageInCarConversation = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getLastMessageInCarConversation(extras);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(lastMessageInCarConversation)) {
                message = lastMessageInCarConversation;
            }
        }
        sLogger.v("[navdyinfo-whatsapp] from[" + from + "] message[" + message + "]");
        java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), message));
        com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
    }

    private static void handleFacebookMessengerNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        android.app.Notification notification = sbn.getNotification();
        android.os.Bundle extras = android.support.v7.app.NotificationCompat.getExtras(notification);
        java.lang.String from = extras.getString("android.title");
        java.lang.String message = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        sLogger.v("[navdyinfo-fbmessenger] from[" + from + "] message[" + message + "]");
        java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM.name(), from));
        data.add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name(), message));
        com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_MESSAGE).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
    }
}
