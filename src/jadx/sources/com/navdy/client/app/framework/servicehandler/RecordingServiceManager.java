package com.navdy.client.app.framework.servicehandler;

public class RecordingServiceManager {
    private static final com.navdy.client.app.framework.servicehandler.RecordingServiceManager sInstance = new com.navdy.client.app.framework.servicehandler.RecordingServiceManager();
    private java.util.List<java.lang.ref.WeakReference<com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener>> mListeners = new java.util.ArrayList();

    public interface Listener {
        void onDriveRecordingsResponse(java.util.List<java.lang.String> list);
    }

    public static com.navdy.client.app.framework.servicehandler.RecordingServiceManager getInstance() {
        return sInstance;
    }

    private RecordingServiceManager() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public void addListener(java.lang.ref.WeakReference<com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener> listener) {
        this.mListeners.add(listener);
    }

    public void removeListener(com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener listener) {
        this.mListeners.remove(new java.lang.ref.WeakReference(listener));
    }

    public void sendStartDriveRecordingEvent(java.lang.String label) {
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.debug.StartDriveRecordingEvent(label));
    }

    public void sendStopDriveRecordingEvent() {
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.debug.StopDriveRecordingEvent());
    }

    public void sendStartDrivePlaybackEvent(java.lang.String label) {
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.debug.StartDrivePlaybackEvent(label, java.lang.Boolean.valueOf(false)));
    }

    public void sendStopDrivePlaybackEvent() {
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.debug.StopDrivePlaybackEvent());
    }

    public void sendDriveRecordingsRequest() {
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.debug.DriveRecordingsRequest());
    }

    @com.squareup.otto.Subscribe
    public void onStartDrivePlaybackResponse(com.navdy.service.library.events.debug.StartDrivePlaybackResponse response) {
        if (response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Drive playback started successfully", 0).show();
        } else {
            android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Drive playback ERROR", 0).show();
        }
    }

    @com.squareup.otto.Subscribe
    public void onStartDriveRecordingResponse(com.navdy.service.library.events.debug.StartDriveRecordingResponse response) {
        if (response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Drive recording started successfully", 0).show();
        } else {
            android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Drive recording ERROR", 0).show();
        }
    }

    @com.squareup.otto.Subscribe
    public void onDriveRecordingsResponse(com.navdy.service.library.events.debug.DriveRecordingsResponse response) {
        for (java.lang.ref.WeakReference<com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener> listenerRef : this.mListeners) {
            com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener listener = (com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener) listenerRef.get();
            if (listener != null) {
                listener.onDriveRecordingsResponse(response.recordings);
            }
        }
    }

    @com.squareup.otto.Subscribe
    public void onStopDriveRecordingResponse(com.navdy.service.library.events.debug.StopDriveRecordingResponse response) {
        if (response.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
            android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Drive recording was successfully saved!", 0).show();
        } else {
            android.widget.Toast.makeText(com.navdy.client.app.NavdyApplication.getAppContext(), "Drive recording finished with ERROR", 0).show();
        }
    }
}
