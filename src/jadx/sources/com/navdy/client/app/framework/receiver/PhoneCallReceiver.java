package com.navdy.client.app.framework.receiver;

public class PhoneCallReceiver extends android.content.BroadcastReceiver {
    private static com.navdy.service.library.events.callcontrol.PhoneEvent currentStatus;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.receiver.PhoneCallReceiver.class);

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        if (intent != null) {
            try {
                java.lang.String action = intent.getAction();
                if (action.equals("android.intent.action.NEW_OUTGOING_CALL")) {
                    java.lang.String phoneNumber = getResultData();
                    if (phoneNumber == null) {
                        phoneNumber = intent.getStringExtra("android.intent.extra.PHONE_NUMBER");
                    }
                    sLogger.d("New outgoing call " + phoneNumber);
                    sendEvent(context, buildPhoneEvent(context, phoneNumber, com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING));
                } else if (action.equals("android.intent.action.PHONE_STATE")) {
                    android.os.Bundle bundle = intent.getExtras();
                    if (bundle != null) {
                        java.lang.String stateStr = bundle.getString("state");
                        java.lang.String number = bundle.getString("incoming_number");
                        com.navdy.service.library.events.callcontrol.PhoneStatus status = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
                        sLogger.d("Phone state change: " + stateStr + " - " + number);
                        if (android.telephony.TelephonyManager.EXTRA_STATE_IDLE.equals(stateStr)) {
                            status = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
                        } else if (android.telephony.TelephonyManager.EXTRA_STATE_OFFHOOK.equals(stateStr)) {
                            status = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK;
                        } else if (android.telephony.TelephonyManager.EXTRA_STATE_RINGING.equals(stateStr)) {
                            status = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING;
                        }
                        currentStatus = buildPhoneEvent(context, number, status);
                        sendEvent(context, currentStatus);
                        return;
                    }
                    sLogger.i("no extras for phone event");
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private com.navdy.service.library.events.callcontrol.PhoneEvent buildPhoneEvent(android.content.Context context, java.lang.String number, com.navdy.service.library.events.callcontrol.PhoneStatus status) {
        java.lang.String name = "";
        if (number != null) {
            name = com.navdy.client.debug.util.Contacts.lookupNameFromPhoneNumber(context, number);
        }
        return new com.navdy.service.library.events.callcontrol.PhoneEvent.Builder().number(number).status(status).contact_name(name).build();
    }

    private void sendEvent(android.content.Context context, com.navdy.service.library.events.callcontrol.PhoneEvent message) {
        com.navdy.service.library.events.NavdyEvent event = new com.navdy.service.library.events.NavdyEvent.Builder().type(com.navdy.service.library.events.NavdyEvent.MessageType.PhoneEvent).setExtension(com.navdy.service.library.events.Ext_NavdyEvent.phoneEvent, message).build();
        com.navdy.service.library.device.RemoteDevice device = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
        if (device != null) {
            device.postEvent(event);
        }
    }

    public static com.navdy.service.library.events.callcontrol.PhoneEvent getCurrentStatus() {
        return currentStatus;
    }
}
