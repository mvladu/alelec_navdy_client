package com.navdy.client.app.framework.search;

public class NavdySearch {
    public static final boolean FORCE_OFFLINE_SEARCH = false;
    private static final long HUD_SEARCH_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(1);
    public static final int MAX_RESULTS_FROM_HUD = 30;
    public static final java.lang.Integer SEARCH_AREA_VALUE = null;
    private android.os.Handler handler;
    private java.lang.String lastHudQuery;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.client.app.framework.search.NavdySearch.SearchCallback searchCallback;
    private com.navdy.client.app.framework.search.SearchResults searchResults;
    private boolean withContactPhotos;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.search.NavdySearch.this.onPlacesSearchResponse(new com.navdy.service.library.events.places.PlacesSearchResponse.Builder().searchQuery(com.navdy.client.app.framework.search.NavdySearch.this.lastHudQuery).status(com.navdy.service.library.events.RequestStatus.REQUEST_UNKNOWN_ERROR).statusDetail("Hud Search timed out").results(new java.util.ArrayList(0)).build());
        }
    }

    class Anon2 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ java.lang.String val$query;

        class Anon1 implements com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener {
            Anon1() {
            }

            public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleResults, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
                if (error != com.navdy.client.app.framework.search.GooglePlacesSearch.Error.NONE) {
                    com.navdy.client.app.framework.search.NavdySearch.this.logger.e("received Google places search error while running online search: " + error.name());
                    com.navdy.client.app.framework.search.NavdySearch.this.searchResults.setGoogleResults(null);
                } else {
                    com.navdy.client.app.framework.search.NavdySearch.this.searchResults.setGoogleResults(googleResults);
                }
                com.navdy.client.app.framework.search.NavdySearch.this.finishSearch();
            }
        }

        Anon2(java.lang.String str) {
            this.val$query = str;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... voids) {
            com.navdy.client.app.framework.search.NavdySearch.this.addMatchingContactsAndDbDestinationsToSearchResults(this.val$query);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            com.navdy.client.app.framework.search.GooglePlacesSearch googlePlacesSearch = new com.navdy.client.app.framework.search.GooglePlacesSearch(new com.navdy.client.app.framework.search.NavdySearch.Anon2.Anon1(), new android.os.Handler());
            java.lang.String newQuery = com.navdy.client.app.framework.search.NavdySearch.this.cleanUpNearestQuery(this.val$query);
            if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(newQuery, this.val$query)) {
                googlePlacesSearch.runTextSearchWebApi(this.val$query);
                return;
            }
            com.navdy.client.app.framework.search.NavdySearch.this.logger.v("Query was a 'the nearest' kind of query. We will seach for the closest: " + newQuery);
            googlePlacesSearch.runNearbySearchWebApi(newQuery);
        }
    }

    class Anon3 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ com.navdy.service.library.events.places.PlacesSearchResponse val$placesSearchResponse;

        Anon3(com.navdy.service.library.events.places.PlacesSearchResponse placesSearchResponse) {
            this.val$placesSearchResponse = placesSearchResponse;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... voids) {
            com.navdy.client.app.framework.search.NavdySearch.this.addMatchingContactsAndDbDestinationsToSearchResults(this.val$placesSearchResponse.searchQuery);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> hudSearchResults;
            if (this.val$placesSearchResponse.status == com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                hudSearchResults = this.val$placesSearchResponse.results;
            } else {
                hudSearchResults = null;
                com.navdy.client.app.framework.search.NavdySearch.this.logger.v("Request failed: " + this.val$placesSearchResponse.status);
            }
            com.navdy.client.app.framework.search.NavdySearch.this.searchResults.setHudSearchResults(hudSearchResults);
            com.navdy.client.app.framework.search.NavdySearch.this.finishSearch();
        }
    }

    class Anon4 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        final /* synthetic */ java.lang.String val$query;

        Anon4(java.lang.String str) {
            this.val$query = str;
        }

        /* access modifiers changed from: protected */
        public java.lang.Void doInBackground(java.lang.Void... voids) {
            com.navdy.client.app.framework.search.NavdySearch.this.addMatchingContactsAndDbDestinationsToSearchResults(this.val$query);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(java.lang.Void aVoid) {
            com.navdy.client.app.framework.search.NavdySearch.this.finishSearch();
        }
    }

    public interface SearchCallback {
        void onSearchCompleted(com.navdy.client.app.framework.search.SearchResults searchResults);

        void onSearchStarted();
    }

    public NavdySearch(com.navdy.client.app.framework.search.NavdySearch.SearchCallback searchCallback2) {
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.search.NavdySearch.class);
        this.searchResults = new com.navdy.client.app.framework.search.SearchResults();
        this.searchCallback = null;
        this.withContactPhotos = false;
        this.handler = new android.os.Handler();
        this.searchCallback = searchCallback2;
    }

    public NavdySearch(com.navdy.client.app.framework.search.NavdySearch.SearchCallback searchCallback2, boolean withContactPhotos2) {
        this(searchCallback2);
        this.withContactPhotos = withContactPhotos2;
    }

    @com.squareup.otto.Subscribe
    public void onPlacesSearchResponse(com.navdy.service.library.events.places.PlacesSearchResponse placesSearchResponse) {
        this.handler.removeCallbacksAndMessages(null);
        if (placesSearchResponse == null) {
            this.logger.d("Receiving search results from HUD that is null!");
            finishSearch();
            return;
        }
        this.logger.d("Receiving search results from HUD for \"" + placesSearchResponse.searchQuery + "\": " + placesSearchResponse.results);
        handleHudSearchResponse(placesSearchResponse);
        com.navdy.client.app.framework.util.BusProvider.getInstance().unregister(this);
    }

    public void runSearch(java.lang.String query) {
        if (this.searchCallback == null) {
            throw new java.lang.RuntimeException("NoSearchCallBackException", new java.lang.Throwable("No point running a search if there is no callback specified."));
        }
        this.searchResults.setQuery(query);
        this.searchCallback.onSearchStarted();
        com.navdy.client.app.framework.AppInstance appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        if (appInstance.canReachInternet()) {
            runOnlineSearch(query);
        } else if (appInstance.isDeviceConnected()) {
            com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
            java.lang.String query2 = cleanUpNearestQuery(query);
            this.lastHudQuery = query2;
            this.logger.d("Requesting search from HUD for \"" + query2 + "\"" + " with a " + SEARCH_AREA_VALUE + " search area" + " and " + 30 + " max results");
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.places.PlacesSearchRequest.Builder().searchQuery(query2).searchArea(SEARCH_AREA_VALUE).maxResults(java.lang.Integer.valueOf(30)).build());
            this.handler.postDelayed(new com.navdy.client.app.framework.search.NavdySearch.Anon1(), HUD_SEARCH_TIMEOUT);
        } else {
            runOfflineDisconnectSearch(query);
        }
    }

    private void finishSearch() {
        if (this.searchCallback != null) {
            this.searchCallback.onSearchCompleted(this.searchResults);
        }
    }

    private void addMatchingContactsAndDbDestinationsToSearchResults(@android.support.annotation.NonNull java.lang.String query) {
        java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts;
        java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts2;
        com.navdy.client.app.framework.util.ContactsManager contactsManager = com.navdy.client.app.framework.util.ContactsManager.getInstance();
        java.lang.String contactQuery = cleanUpContactQuery(query);
        this.logger.d("The cleaned up query for contact is: " + contactQuery);
        if (this.withContactPhotos) {
            contacts = contactsManager.getContactsAndLoadPhotos(contactQuery, java.util.EnumSet.of(com.navdy.client.app.framework.util.ContactsManager.Attributes.ADDRESS));
        } else {
            contacts = contactsManager.getContactsWithoutLoadingPhotos(contactQuery, java.util.EnumSet.of(com.navdy.client.app.framework.util.ContactsManager.Attributes.ADDRESS));
        }
        java.lang.String and = " " + com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.this_and_that) + " ";
        if (contactQuery.contains(and)) {
            java.lang.String contactQuery2 = contactQuery.replaceAll(and, " & ");
            if (this.withContactPhotos) {
                contacts2 = contactsManager.getContactsAndLoadPhotos(contactQuery2, java.util.EnumSet.of(com.navdy.client.app.framework.util.ContactsManager.Attributes.ADDRESS));
            } else {
                contacts2 = contactsManager.getContactsWithoutLoadingPhotos(contactQuery2, java.util.EnumSet.of(com.navdy.client.app.framework.util.ContactsManager.Attributes.ADDRESS));
            }
            for (com.navdy.client.app.framework.models.ContactModel contact2 : contacts2) {
                boolean found = false;
                java.util.Iterator it = contacts.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((com.navdy.client.app.framework.models.ContactModel) it.next()).equals(contact2)) {
                            found = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!found) {
                    contacts.add(contact2);
                }
            }
        }
        this.searchResults.setContacts(contacts);
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinationsFromDatabase = com.navdy.client.app.providers.NavdyContentProvider.getDestinationsFromSearchQuery(query);
        if (query.contains(and)) {
            java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinationsFromDatabase2 = com.navdy.client.app.providers.NavdyContentProvider.getDestinationsFromSearchQuery(query.replaceAll(and, " & "));
            if (destinationsFromDatabase != null) {
                destinationsFromDatabase = (java.util.ArrayList) com.navdy.client.app.framework.models.Destination.mergeAndDeduplicateLists(destinationsFromDatabase, destinationsFromDatabase2);
            } else {
                destinationsFromDatabase = destinationsFromDatabase2;
            }
        }
        if (destinationsFromDatabase != null) {
            java.util.Iterator it2 = destinationsFromDatabase.iterator();
            while (it2.hasNext()) {
                com.navdy.client.app.framework.models.Destination destination = (com.navdy.client.app.framework.models.Destination) it2.next();
                destination.rawAddressVariations = com.navdy.client.app.providers.NavdyContentProvider.getCacheKeysForDestinationId(destination.id);
            }
        }
        this.searchResults.setDestinationsFromDatabase(destinationsFromDatabase);
    }

    @android.support.annotation.NonNull
    private java.lang.String cleanUpContactQuery(@android.support.annotation.NonNull java.lang.String query) {
        com.navdy.client.app.framework.util.VoiceCommand vc = com.navdy.client.app.framework.util.VoiceCommandUtils.getMatchingVoiceCommand(com.navdy.client.R.raw.contact_home_strings, query);
        return (vc == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(vc.query)) ? query : vc.query;
    }

    /* access modifiers changed from: private */
    @android.support.annotation.NonNull
    public java.lang.String cleanUpNearestQuery(@android.support.annotation.NonNull java.lang.String query) {
        com.navdy.client.app.framework.util.VoiceCommand vc = com.navdy.client.app.framework.util.VoiceCommandUtils.getMatchingVoiceCommand(com.navdy.client.R.raw.nearest_strings, query);
        return (vc == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(vc.query)) ? query : vc.query;
    }

    private void runOnlineSearch(java.lang.String query) {
        new com.navdy.client.app.framework.search.NavdySearch.Anon2(query).execute(new java.lang.Void[0]);
    }

    private void handleHudSearchResponse(@android.support.annotation.NonNull com.navdy.service.library.events.places.PlacesSearchResponse placesSearchResponse) {
        if (this.lastHudQuery != null && this.lastHudQuery.equals(placesSearchResponse.searchQuery)) {
            this.lastHudQuery = null;
            new com.navdy.client.app.framework.search.NavdySearch.Anon3(placesSearchResponse).execute(new java.lang.Void[0]);
        }
    }

    private void runOfflineDisconnectSearch(java.lang.String query) {
        new com.navdy.client.app.framework.search.NavdySearch.Anon4(query).execute(new java.lang.Void[0]);
    }

    @android.support.annotation.Nullable
    static java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getDestinationListFromPlaceSearchResults(@android.support.annotation.Nullable java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> placesSearchResults) {
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>();
        if (placesSearchResults != null) {
            for (com.navdy.service.library.events.places.PlacesSearchResult placesSearchResult : placesSearchResults) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                com.navdy.client.app.framework.models.Destination.placesSearchResultToDestinationObject(placesSearchResult, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    @android.support.annotation.Nullable
    public static java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getDestinationListFromGoogleTextSearchResults(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleTextSearchResults) {
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>();
        if (googleTextSearchResults != null) {
            for (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult googleTextSearchDestinationResult : googleTextSearchResults) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                googleTextSearchDestinationResult.toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.TEXT_SEARCH, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    public static void runServiceSearch(com.navdy.client.app.framework.search.GooglePlacesSearch googlePlacesSearch, com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceType) {
        switch (serviceType) {
            case FOOD:
            case STORE:
                googlePlacesSearch.runTextSearchWebApi(serviceType.getHudServiceType());
                return;
            default:
                googlePlacesSearch.runServiceSearchWebApi(serviceType);
                return;
        }
    }
}
