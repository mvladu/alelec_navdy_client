package com.navdy.client.app.framework.service;

public class RemoteControllerNotificationListenerService extends com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService implements android.media.RemoteController.OnClientUpdateListener {
    private static final int MINIMUM_UPDATE_WAIT = 1000;
    private static final int REMOTE_CONTROL_PLAYSTATE_NONE = 0;
    private static final boolean VERBOSE = false;
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.class);
    private android.graphics.Bitmap albumArt = null;
    private android.media.AudioManager audioManager;
    private boolean ignoreMetadata;
    private double lastUpdateTime;
    private boolean lastUpdateWasClientMetadata;
    @android.support.annotation.NonNull
    private com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.EMPTY_TRACK_INFO;
    private int playbackState = 0;
    private android.media.RemoteController remoteController;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this.remoteController = new android.media.RemoteController(com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this, com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this);
            if (!com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this.remoteController.setArtworkConfiguration(200, 200)) {
                com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.logger.e("Couldn't set artwork configuration");
            }
            com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this.audioManager = (android.media.AudioManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("audio");
            try {
                com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this.audioManager.registerRemoteController(com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this.remoteController);
                com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().setMusicSeekHelper(new com.navdy.client.app.framework.music.RemoteControllerSeekHelper(com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this.remoteController));
                com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.logger.i("Remote controller and audio manager initialized");
            } catch (Throwable e) {
                com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.this.remoteController = null;
                com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.logger.e("Remote controller could not be initialized: " + e);
            }
        }
    }

    public void onCreate() {
        logger.i("onCreate");
        super.onCreate();
    }

    private void initializeRemoteController() {
        logger.i("initializeRemoteController");
        if (this.remoteController != null) {
            logger.i("RemoteController already initialized, returning");
        } else {
            new android.os.Handler(getMainLooper()).post(new com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService.Anon1());
        }
    }

    public android.os.IBinder onBind(android.content.Intent intent) {
        logger.d("onBind");
        if (android.os.Build.VERSION.SDK_INT < 21) {
            initializeRemoteController();
        }
        return super.onBind(intent);
    }

    public void onListenerConnected() {
        logger.d("onListenerConnected");
        initializeRemoteController();
        super.onListenerConnected();
    }

    public boolean onUnbind(android.content.Intent intent) {
        logger.d("onUnbind");
        return super.onUnbind(intent);
    }

    public void onDestroy() {
        logger.i("onDestroy");
        if (this.audioManager != null) {
            this.audioManager.unregisterRemoteController(this.remoteController);
        }
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().setMusicSeekHelper(null);
        super.onDestroy();
    }

    public void onClientChange(boolean clearing) {
        logger.v("onClientChange: " + clearing);
        if (clearing) {
            setMusicTrackInfo(null);
            this.albumArt = null;
            this.playbackState = 0;
        }
    }

    public void onClientPlaybackStateUpdate(int state) {
        logger.v("onClientPlaybackStateUpdate: " + state);
        checkForSpuriousStateUpdate(state);
        if (state != this.playbackState && state != 0) {
            this.playbackState = state;
            setMusicTrackInfo(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.musicTrackInfo).playbackState(musicPlaybackState(this.playbackState)).collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
            sendAlbumArt();
        }
    }

    public void onClientPlaybackStateUpdate(int state, long stateChangeTimeMs, long currentPosMs, float speed) {
        logger.v("onClientPlaybackStateUpdate: " + state + org.droidparts.contract.SQL.DDL.SEPARATOR + stateChangeTimeMs + org.droidparts.contract.SQL.DDL.SEPARATOR + currentPosMs + org.droidparts.contract.SQL.DDL.SEPARATOR + speed);
        checkForSpuriousStateUpdate(state);
        long currentTime = java.lang.System.currentTimeMillis();
        if (state == 0) {
            return;
        }
        if (this.playbackState != state || ((double) currentTime) - this.lastUpdateTime >= 1000.0d || this.lastUpdateWasClientMetadata) {
            this.lastUpdateTime = (double) currentTime;
            this.lastUpdateWasClientMetadata = false;
            setMusicTrackInfo(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.musicTrackInfo).playbackState(musicPlaybackState(state)).currentPosition(java.lang.Integer.valueOf((int) currentPosMs)).collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
            if (this.playbackState != state) {
                sendAlbumArt();
            }
            this.playbackState = state;
        }
    }

    private void checkForSpuriousStateUpdate(int state) {
        if (state >= 3 || this.playbackState >= 3) {
            this.ignoreMetadata = false;
            return;
        }
        logger.i("Spurious none/stop/pause detected, will ignore metadata updates until we leave none/stopped/paused states");
        this.ignoreMetadata = true;
    }

    public void onClientTransportControlUpdate(int transportControlFlags) {
        boolean z = true;
        logger.v("onClientTransportControlUpdate: " + java.lang.Integer.toBinaryString(transportControlFlags));
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler musicServiceHandler = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance();
        com.navdy.service.library.events.audio.MusicTrackInfo.Builder isPreviousAllowed = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.musicTrackInfo).isPreviousAllowed(java.lang.Boolean.valueOf((transportControlFlags & 1) != 0));
        if ((isAdvertisement() || !com.navdy.client.app.framework.glances.GlanceConstants.PANDORA.equals(musicServiceHandler.getLastMusicApp())) && (transportControlFlags & 128) == 0) {
            z = false;
        }
        setMusicTrackInfo(isPreviousAllowed.isNextAllowed(java.lang.Boolean.valueOf(z)).collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
        sendAlbumArt();
    }

    public void onClientMetadataUpdate(android.media.RemoteController.MetadataEditor metadataEditor) {
        logger.v("onClientMetadataUpdate");
        this.lastUpdateWasClientMetadata = true;
        if (this.ignoreMetadata) {
            logger.i("ignoring metadata due to prior spurious update");
            return;
        }
        setMusicTrackInfo(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.musicTrackInfo).dataSource(com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_OS_NOTIFICATION).album(metadataEditor.getString(1, "")).author(metadataEditor.getString(2, "")).name(metadataEditor.getString(7, "")).duration(java.lang.Integer.valueOf((int) metadataEditor.getLong(9, 0))).playbackState(musicPlaybackState(this.playbackState)).collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN).collectionType(com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN).collectionId(null).trackId(null).build());
        this.albumArt = null;
        android.graphics.Bitmap bitmap = metadataEditor.getBitmap(100, null);
        if (bitmap != null) {
            this.albumArt = bitmap;
            sendAlbumArt();
        }
    }

    public void onClientSessionEvent(java.lang.String s, android.os.Bundle b) {
        logger.v("onClientSessionEvent");
    }

    private void setMusicTrackInfo(@android.support.annotation.Nullable com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
        if (trackInfo == null) {
            this.musicTrackInfo = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.EMPTY_TRACK_INFO;
        } else {
            this.musicTrackInfo = trackInfo;
        }
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().setCurrentMediaTrackInfo(trackInfo);
    }

    private void sendAlbumArt() {
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().checkAndSetCurrentMediaArtwork(this.musicTrackInfo, this.albumArt);
    }

    private boolean isAdvertisement() {
        com.navdy.service.library.events.audio.MusicTrackInfo currentTrackInfo = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().getCurrentMediaTrackInfo();
        java.lang.String name = currentTrackInfo.name;
        return name != null && name.toLowerCase().contains("advertisement") && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(currentTrackInfo.author) && com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(currentTrackInfo.album);
    }

    private com.navdy.service.library.events.audio.MusicPlaybackState musicPlaybackState(int remoteControlClientPlaybackState) {
        switch (remoteControlClientPlaybackState) {
            case 1:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_STOPPED;
            case 2:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED;
            case 3:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING;
            case 4:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_FAST_FORWARDING;
            case 5:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_REWINDING;
            case 6:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_SKIPPING_TO_NEXT;
            case 7:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_SKIPPING_TO_PREVIOUS;
            case 8:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_BUFFERING;
            case 9:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_ERROR;
            default:
                return com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE;
        }
    }

    private void logMetadata(android.media.RemoteController.MetadataEditor metadataEditor) {
        logger.v("- METADATA_KEY_ALBUM: " + metadataEditor.getString(1, ""));
        logger.v("- METADATA_KEY_ARTIST: " + metadataEditor.getString(2, ""));
        logger.v("- METADATA_KEY_TITLE: " + metadataEditor.getString(7, ""));
        logger.v("- METADATA_KEY_DURATION: " + metadataEditor.getLong(9, 0));
        logger.v("- BITMAP_KEY_ARTWORK: " + metadataEditor.getBitmap(100, null));
    }
}
