package com.navdy.client.app.framework.map;

public class HereNavigableCoordinateWorker {
    private static final int MIN_WORDS_IN_ADDRESS = 4;
    private static final java.lang.Object lockObj = new java.lang.Object();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.class);
    private final java.util.List<com.navdy.client.app.framework.models.CalendarEvent> calendarEvents;
    private com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.NavigableCoordinateWorkerFinishCallback callback;
    private int currentCalendarEventIndex = -1;
    private final java.util.List<com.navdy.client.app.framework.models.CalendarEvent> processedCalendarEvents;
    private final android.os.Handler uiThreadHandler;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.this.callback.onFinish(com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.this.processedCalendarEvents);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.client.app.framework.models.CalendarEvent val$calendarEvent;

        Anon2(com.navdy.client.app.framework.models.CalendarEvent calendarEvent) {
            this.val$calendarEvent = calendarEvent;
        }

        public void run() {
            com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.this.process(this.val$calendarEvent);
        }
    }

    class Anon3 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
        final /* synthetic */ com.navdy.client.app.framework.models.CalendarEvent val$calendarEvent;

        Anon3(com.navdy.client.app.framework.models.CalendarEvent calendarEvent) {
            this.val$calendarEvent = calendarEvent;
        }

        public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
            com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.logger.d("Now processing: calendar event: " + this.val$calendarEvent.toString() + "; destination:" + destination);
            if (destination.precisionLevel.isPrecise() && destination.hasValidNavCoordinates()) {
                com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.logger.d("Now processing: Using the navCoords");
                com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.this.addToProcessedEventsListAndCallBack(this.val$calendarEvent, destination.navigationLat, destination.navigationLong);
            } else if (!destination.precisionLevel.isPrecise() || !destination.hasValidDisplayCoordinates()) {
                onFailure(destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.BAD_COORDS);
            } else {
                com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.logger.d("Now processing: Using the displayCoords");
                com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.this.addToProcessedEventsListAndCallBack(this.val$calendarEvent, destination.navigationLat, destination.navigationLong);
            }
        }

        public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
            com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.logger.d("Now processing: No coordinates found for: " + this.val$calendarEvent.toString());
            com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.this.processNextCalendarEvent();
        }
    }

    public interface NavigableCoordinateWorkerFinishCallback {
        void onFinish(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> list);
    }

    public HereNavigableCoordinateWorker(java.util.List<com.navdy.client.app.framework.models.CalendarEvent> list, com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.NavigableCoordinateWorkerFinishCallback callback2) {
        this.calendarEvents = list;
        this.processedCalendarEvents = new java.util.ArrayList();
        this.uiThreadHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        start(callback2);
    }

    private void start(com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.NavigableCoordinateWorkerFinishCallback callback2) {
        this.callback = callback2;
        processNextCalendarEvent();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    public void processNextCalendarEvent() {
        synchronized (lockObj) {
            this.currentCalendarEventIndex++;
            if (this.calendarEvents != null && this.calendarEvents.size() > 0 && this.calendarEvents.size() > this.currentCalendarEventIndex) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.Anon2((com.navdy.client.app.framework.models.CalendarEvent) this.calendarEvents.get(this.currentCalendarEventIndex)), 1);
            } else if (this.callback != null) {
                this.uiThreadHandler.post(new com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.Anon1());
            }
        }
    }

    private void process(com.navdy.client.app.framework.models.CalendarEvent calendarEvent) {
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(calendarEvent.location)) {
            logger.v("Now processing: The address is missing for: " + calendarEvent.toString());
            processNextCalendarEvent();
        } else if (!calendarEvent.location.contains(",")) {
            logger.v("Now processing: The address does not contain a comma: " + calendarEvent.toString());
            processNextCalendarEvent();
        } else {
            java.lang.String[] words = calendarEvent.location.replaceAll("\\W", " ").replaceAll("[0-9]", " ").trim().split(" +");
            if (words.length < 4) {
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                for (int i = 0; i < words.length; i++) {
                    sb.append(words[i]);
                    if (i + 1 < words.length) {
                        sb.append(",");
                    }
                }
                logger.v("Now processing: Not enough words: words are: " + sb.toString() + ", event: " + calendarEvent.toString());
                processNextCalendarEvent();
                return;
            }
            com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback onCompleteCallback = new com.navdy.client.app.framework.map.HereNavigableCoordinateWorker.Anon3(calendarEvent);
            com.navdy.client.app.framework.models.DestinationCacheEntry entry = com.navdy.client.app.providers.NavdyContentProvider.getCacheEntryIfExists(calendarEvent.location);
            if (entry == null) {
                com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(new com.navdy.client.app.framework.models.Destination(calendarEvent.displayName, calendarEvent.location), onCompleteCallback);
            } else if (entry.destination != null) {
                logger.v("destination with address " + calendarEvent.location + "found in cache, returning");
                onCompleteCallback.onSuccess(entry.destination);
            } else {
                onCompleteCallback.onFailure(null, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error.CACHE_NO_COORDS);
            }
        }
    }

    private void addToProcessedEventsListAndCallBack(com.navdy.client.app.framework.models.CalendarEvent calendarEvent, double navigationLat, double navigationLng) {
        synchronized (lockObj) {
            calendarEvent.latLng = new com.google.android.gms.maps.model.LatLng(navigationLat, navigationLng);
            this.processedCalendarEvents.add(calendarEvent);
        }
        processNextCalendarEvent();
    }
}
