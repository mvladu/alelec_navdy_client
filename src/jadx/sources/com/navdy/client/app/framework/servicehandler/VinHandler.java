package com.navdy.client.app.framework.servicehandler;

public class VinHandler {
    public VinHandler() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onObdStatusResponse(com.navdy.service.library.events.obd.ObdStatusResponse odbStatusResponse) {
        com.navdy.client.app.framework.LocalyticsManager.setProfileAttribute(com.navdy.client.app.tracking.TrackerConstants.Attributes.VIN, odbStatusResponse.vin);
    }
}
