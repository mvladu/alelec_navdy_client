package com.navdy.client.app.framework.servicehandler;

public final class SpeechServiceHandler$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.framework.servicehandler.SpeechServiceHandler> implements javax.inject.Provider<com.navdy.client.app.framework.servicehandler.SpeechServiceHandler>, dagger.MembersInjector<com.navdy.client.app.framework.servicehandler.SpeechServiceHandler> {
    private dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> audioRouter;
    private dagger.internal.Binding<android.content.SharedPreferences> sharedPreferences;

    public SpeechServiceHandler$$InjectAdapter() {
        super("com.navdy.client.app.framework.servicehandler.SpeechServiceHandler", "members/com.navdy.client.app.framework.servicehandler.SpeechServiceHandler", false, com.navdy.client.app.framework.servicehandler.SpeechServiceHandler.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.audioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", com.navdy.client.app.framework.servicehandler.SpeechServiceHandler.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", com.navdy.client.app.framework.servicehandler.SpeechServiceHandler.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.audioRouter);
        injectMembersBindings.add(this.sharedPreferences);
    }

    public com.navdy.client.app.framework.servicehandler.SpeechServiceHandler get() {
        com.navdy.client.app.framework.servicehandler.SpeechServiceHandler result = new com.navdy.client.app.framework.servicehandler.SpeechServiceHandler();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.framework.servicehandler.SpeechServiceHandler object) {
        object.audioRouter = (com.navdy.client.app.framework.util.TTSAudioRouter) this.audioRouter.get();
        object.sharedPreferences = (android.content.SharedPreferences) this.sharedPreferences.get();
    }
}
