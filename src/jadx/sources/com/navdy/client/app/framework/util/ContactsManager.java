package com.navdy.client.app.framework.util;

public class ContactsManager {
    private static final boolean VERBOSE = false;
    private static final java.lang.String[] addressProjection = {"contact_id", com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX, "data5", "data4", "data7", "data8", "data9", "data10", "data2", "display_name", "data3"};
    private static final java.util.ArrayList<com.navdy.client.app.framework.models.Address> emptyAddressList = new java.util.ArrayList<>();
    private static final java.util.ArrayList<com.navdy.client.app.framework.models.ContactModel> emptyContactList = new java.util.ArrayList<>();
    private static final java.lang.String[] genericProjection = {"contact_id", com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX, "display_name", "has_phone_number", "is_primary"};
    private static final com.navdy.client.app.framework.util.ContactsManager instance = new com.navdy.client.app.framework.util.ContactsManager();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.ContactsManager.class);
    private static final java.lang.String[] phoneProjection = {"contact_id", com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX, "data1", "data2", "display_name", "starred", "data3", "is_primary"};

    static class Anon1 implements com.navdy.client.app.framework.util.ImageUtils.StreamFactory {
        final /* synthetic */ android.content.ContentResolver val$contentResolver;
        final /* synthetic */ boolean val$highRes;
        final /* synthetic */ android.net.Uri val$uri;

        Anon1(android.content.ContentResolver contentResolver, android.net.Uri uri, boolean z) {
            this.val$contentResolver = contentResolver;
            this.val$uri = uri;
            this.val$highRes = z;
        }

        public java.io.InputStream getInputStream() throws java.io.FileNotFoundException {
            return android.provider.ContactsContract.Contacts.openContactPhotoInputStream(this.val$contentResolver, this.val$uri, this.val$highRes);
        }
    }

    public enum Attributes {
        ADDRESS,
        PHONE,
        BOTH
    }

    private ContactsManager() {
    }

    public static com.navdy.client.app.framework.util.ContactsManager getInstance() {
        return instance;
    }

    @android.support.annotation.NonNull
    public java.util.List<com.navdy.client.app.framework.models.ContactModel> getContactsWithoutLoadingPhotos(java.lang.String filter, java.util.EnumSet<com.navdy.client.app.framework.util.ContactsManager.Attributes> set) {
        return getContacts(filter, set, false);
    }

    public java.util.List<com.navdy.client.app.framework.models.ContactModel> getContactsAndLoadPhotos(java.lang.String filter, java.util.EnumSet<com.navdy.client.app.framework.util.ContactsManager.Attributes> set) {
        return getContacts(filter, set, true);
    }

    @android.support.annotation.WorkerThread
    private java.util.List<com.navdy.client.app.framework.models.ContactModel> getContacts(java.lang.String filter, java.util.EnumSet<com.navdy.client.app.framework.util.ContactsManager.Attributes> set, boolean loadPhotos) {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts = new java.util.ArrayList<>();
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        if (set.contains(com.navdy.client.app.framework.util.ContactsManager.Attributes.BOTH)) {
            contacts = getContactsWithBoth(filter, contentResolver);
        } else if (set.contains(com.navdy.client.app.framework.util.ContactsManager.Attributes.ADDRESS)) {
            contacts = getContactsWithAddresses(filter, contentResolver);
        } else if (set.contains(com.navdy.client.app.framework.util.ContactsManager.Attributes.PHONE)) {
            contacts = getContactsWithPhoneNumbers(filter, contentResolver, false);
        }
        if (loadPhotos) {
            for (com.navdy.client.app.framework.models.ContactModel contact : contacts) {
                contact.photo = loadContactPhoto(contact.id, false);
            }
        }
        return contacts;
    }

    @android.support.annotation.WorkerThread
    @android.support.annotation.NonNull
    public java.util.List<com.navdy.client.app.framework.models.ContactModel> getFavoriteContactsWithPhone() {
        com.navdy.client.app.framework.util.SystemUtils.ensureNotOnMainThread();
        return getContactsWithPhoneNumbers(null, com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver(), true);
    }

    private static java.util.List<com.navdy.client.app.framework.models.ContactModel> getContactsWithBoth(java.lang.String filter, android.content.ContentResolver contentResolver) {
        com.navdy.client.app.framework.models.ContactModel contact;
        java.util.HashMap<java.lang.String, com.navdy.client.app.framework.models.ContactModel> contacts = new java.util.HashMap<>();
        android.database.Cursor dataCursor = getGenericCursor(contentResolver, filter);
        if (dataCursor == null) {
            return emptyContactList;
        }
        while (dataCursor.moveToNext()) {
            try {
                long id = com.navdy.client.app.providers.DbUtils.getLong(dataCursor, "contact_id");
                java.lang.String lookupKey = com.navdy.client.app.providers.DbUtils.getString(dataCursor, com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX);
                java.lang.String name = com.navdy.client.app.providers.DbUtils.getString(dataCursor, "display_name");
                boolean hasPhoneNumber = com.navdy.client.app.providers.DbUtils.getBoolean(dataCursor, "has_phone_number");
                java.util.List<com.navdy.client.app.framework.models.PhoneNumberModel> phoneNumbers = null;
                java.util.List<com.navdy.client.app.framework.models.Address> addresses = getAddressFromLookupKey(contentResolver, lookupKey, id);
                if (hasPhoneNumber) {
                    long contactId = getContactIdFromLookupKey(contentResolver, lookupKey, id);
                    if (contactId < 0) {
                        contactId = id;
                    }
                    phoneNumbers = getPhoneNumbers(contentResolver, contactId);
                }
                if (contacts.get(name) != null) {
                    contact = (com.navdy.client.app.framework.models.ContactModel) contacts.get(name);
                } else {
                    contact = new com.navdy.client.app.framework.models.ContactModel();
                    contact.setName(name);
                    contacts.put(name, contact);
                }
                contact.setID(id);
                contact.setLookupKey(lookupKey);
                contact.setLookupTimestampToNow();
                contact.addAddresses(addresses);
                contact.addPhoneNumbers(phoneNumbers);
            } finally {
                com.navdy.client.app.framework.util.SystemUtils.closeCursor(dataCursor);
            }
        }
        return new java.util.ArrayList(contacts.values());
    }

    @android.support.annotation.Nullable
    public static java.util.List<com.navdy.client.app.framework.models.ContactModel> getContactsPreferablyWithPhoneNumber(@android.support.annotation.Nullable java.lang.String filter, @android.support.annotation.NonNull android.content.ContentResolver contentResolver) {
        java.util.List<com.navdy.client.app.framework.models.ContactModel> contactList = null;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(filter)) {
            android.database.Cursor dataCursor = null;
            try {
                dataCursor = contentResolver.query(android.provider.ContactsContract.Data.CONTENT_URI, genericProjection, "display_name LIKE ?", new java.lang.String[]{filter.trim() + "%"}, "has_phone_number DESC");
            } catch (java.lang.Exception e) {
                logger.e("Something went wrong while trying to get a contact cursor", e);
            }
            if (dataCursor != null) {
                try {
                    contactList = new java.util.ArrayList<>();
                    while (dataCursor.moveToNext()) {
                        long id = com.navdy.client.app.providers.DbUtils.getLong(dataCursor, "contact_id");
                        java.lang.String lookupKey = com.navdy.client.app.providers.DbUtils.getString(dataCursor, com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX);
                        java.lang.String name = com.navdy.client.app.providers.DbUtils.getString(dataCursor, "display_name");
                        com.navdy.client.app.framework.models.ContactModel contact = new com.navdy.client.app.framework.models.ContactModel();
                        contact.setName(name);
                        contact.setID(id);
                        contact.setLookupKey(lookupKey);
                        contact.setLookupTimestampToNow();
                        contactList.add(contact);
                    }
                } finally {
                    com.navdy.client.app.framework.util.SystemUtils.closeCursor(dataCursor);
                }
            }
        }
        return contactList;
    }

    private java.util.List<com.navdy.client.app.framework.models.ContactModel> getContactsWithAddresses(java.lang.String filter, android.content.ContentResolver contentResolver) {
        java.util.List<com.navdy.client.app.framework.models.ContactModel> addressesFromContactsHelper;
        android.database.Cursor addressCursor = null;
        try {
            addressCursor = getAddressCursor(contentResolver, filter);
            if (addressCursor == null) {
                addressesFromContactsHelper = emptyContactList;
            } else {
                addressesFromContactsHelper = getAddressesFromContactsHelper(addressCursor);
                com.navdy.client.app.framework.util.SystemUtils.closeCursor(addressCursor);
            }
            return addressesFromContactsHelper;
        } finally {
            com.navdy.client.app.framework.util.SystemUtils.closeCursor(addressCursor);
        }
    }

    private java.util.List<com.navdy.client.app.framework.models.ContactModel> getContactsWithPhoneNumbers(java.lang.String filter, android.content.ContentResolver contentResolver, boolean starredOnly) {
        java.util.List<com.navdy.client.app.framework.models.ContactModel> phoneNumbersFromContactsHelper;
        android.database.Cursor phoneCursor = null;
        try {
            phoneCursor = getPhoneCursor(contentResolver, filter, starredOnly);
            if (phoneCursor == null) {
                phoneNumbersFromContactsHelper = emptyContactList;
            } else {
                phoneNumbersFromContactsHelper = getPhoneNumbersFromContactsHelper(phoneCursor);
                com.navdy.client.app.framework.util.SystemUtils.closeCursor(phoneCursor);
            }
            return phoneNumbersFromContactsHelper;
        } finally {
            com.navdy.client.app.framework.util.SystemUtils.closeCursor(phoneCursor);
        }
    }

    private static java.util.List<com.navdy.client.app.framework.models.ContactModel> getAddressesFromContactsHelper(android.database.Cursor cursor) {
        com.navdy.client.app.framework.models.ContactModel contact;
        java.util.HashMap<java.lang.String, com.navdy.client.app.framework.models.ContactModel> contacts = new java.util.HashMap<>();
        if (cursor == null) {
            return emptyContactList;
        }
        while (cursor.moveToNext()) {
            java.lang.String name = com.navdy.client.app.providers.DbUtils.getString(cursor, "display_name");
            if (contacts.get(name) != null) {
                contact = (com.navdy.client.app.framework.models.ContactModel) contacts.get(name);
            } else {
                contact = new com.navdy.client.app.framework.models.ContactModel();
                contact.setName(name);
            }
            contact.addAddress(getAddress(cursor));
            contact.setID(java.lang.Long.valueOf(com.navdy.client.app.providers.DbUtils.getLong(cursor, "contact_id")).longValue());
            contact.setLookupKey(com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX));
            contact.setLookupTimestampToNow();
            contacts.put(name, contact);
        }
        return new java.util.ArrayList(contacts.values());
    }

    private static java.util.List<com.navdy.client.app.framework.models.ContactModel> getPhoneNumbersFromContactsHelper(android.database.Cursor cursor) {
        com.navdy.client.app.framework.models.ContactModel contact;
        java.util.HashMap<java.lang.String, com.navdy.client.app.framework.models.ContactModel> contacts = new java.util.LinkedHashMap<>();
        while (cursor.moveToNext()) {
            java.lang.String name = com.navdy.client.app.providers.DbUtils.getString(cursor, "display_name");
            if (contacts.get(name) != null) {
                contact = (com.navdy.client.app.framework.models.ContactModel) contacts.get(name);
            } else {
                contact = new com.navdy.client.app.framework.models.ContactModel();
                contact.setName(name);
            }
            contact.addPhoneNumber(getPhoneNumber(cursor));
            contact.favorite = com.navdy.client.app.providers.DbUtils.getInt(cursor, "starred") > 0;
            contact.setID(java.lang.Long.valueOf(com.navdy.client.app.providers.DbUtils.getLong(cursor, "contact_id")).longValue());
            contact.setLookupKey(com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX));
            contact.setLookupTimestampToNow();
            contacts.put(name, contact);
        }
        return new java.util.ArrayList(contacts.values());
    }

    public static long getContactIdFromLookupKey(android.content.ContentResolver contentResolver, java.lang.String key, long lastKnownContactId) throws java.lang.IllegalStateException {
        android.net.Uri uri;
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(key)) {
            return lastKnownContactId;
        }
        if (lastKnownContactId > 0) {
            uri = android.net.Uri.parse(android.provider.ContactsContract.Contacts.CONTENT_LOOKUP_URI + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + key + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + lastKnownContactId);
        } else {
            uri = android.net.Uri.parse(android.provider.ContactsContract.Contacts.CONTENT_LOOKUP_URI + com.navdy.client.debug.util.S3Constants.S3_FILE_DELIMITER + key);
        }
        android.database.Cursor cursor = null;
        try {
            cursor = contentResolver.query(uri, new java.lang.String[]{"_id", com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX}, null, null, null);
            if (cursor == null || !cursor.moveToNext()) {
                throw new java.lang.IllegalStateException("contact no longer exists for key");
            }
            long latestContactId = com.navdy.client.app.providers.DbUtils.getLong(cursor, "_id");
            updateContactData(key, lastKnownContactId, com.navdy.client.app.providers.DbUtils.getString(cursor, com.navdy.client.debug.util.S3Constants.LOOKUP_FOLDER_PREFIX), latestContactId);
            if (latestContactId <= 0) {
                return lastKnownContactId;
            }
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return latestContactId;
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
            return lastKnownContactId;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    /* JADX INFO: finally extract failed */
    public static java.util.List<com.navdy.client.app.framework.models.Address> getAddressFromLookupKey(android.content.ContentResolver contentResolver, java.lang.String lookupKey, long lastKnownContactId) {
        long contactId = getContactIdFromLookupKey(contentResolver, lookupKey, lastKnownContactId);
        if (contactId < 0) {
            return emptyAddressList;
        }
        try {
            android.database.Cursor cursor = contentResolver.query(android.provider.ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, addressProjection, "contact_id = ? AND mimetype = ?", new java.lang.String[]{java.lang.String.valueOf(contactId), "vnd.android.cursor.item/postal-address_v2"}, null);
            java.util.List<com.navdy.client.app.framework.models.Address> addresses = new java.util.ArrayList<>();
            while (cursor != null && cursor.moveToNext()) {
                addresses.add(getAddress(cursor));
            }
            com.navdy.client.app.framework.util.SystemUtils.closeCursor(cursor);
            return addresses;
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get an address cursor", e);
            com.navdy.client.app.framework.util.SystemUtils.closeCursor(null);
            return emptyAddressList;
        } catch (Throwable th) {
            com.navdy.client.app.framework.util.SystemUtils.closeCursor(null);
            throw th;
        }
    }

    private static com.navdy.client.app.framework.models.Address getAddress(android.database.Cursor cursor) {
        return new com.navdy.client.app.framework.models.Address(com.navdy.client.app.providers.DbUtils.getString(cursor, "data5"), com.navdy.client.app.providers.DbUtils.getString(cursor, "data4"), com.navdy.client.app.providers.DbUtils.getString(cursor, "data7"), com.navdy.client.app.providers.DbUtils.getString(cursor, "data8"), com.navdy.client.app.providers.DbUtils.getString(cursor, "data9"), com.navdy.client.app.providers.DbUtils.getString(cursor, "data10"), com.navdy.client.app.providers.DbUtils.getInt(cursor, "data2"), com.navdy.client.app.providers.DbUtils.getString(cursor, "display_name"), com.navdy.client.app.providers.DbUtils.getString(cursor, "data3"));
    }

    @android.support.annotation.NonNull
    public static java.util.List<com.navdy.client.app.framework.models.PhoneNumberModel> getPhoneNumbers(android.content.ContentResolver contentResolver, long contactId) {
        java.util.ArrayList<com.navdy.client.app.framework.models.PhoneNumberModel> numbers = new java.util.ArrayList<>();
        if (contactId >= 0) {
            android.database.Cursor cursor = null;
            try {
                cursor = contentResolver.query(android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneProjection, "contact_id = ?", new java.lang.String[]{java.lang.String.valueOf(contactId)}, null);
                while (cursor != null && cursor.moveToNext()) {
                    numbers.add(getPhoneNumber(cursor));
                }
            } catch (java.lang.Exception e) {
                logger.e("Something went wrong while trying to get a phone number cursor", e);
            } finally {
                com.navdy.client.app.framework.util.SystemUtils.closeCursor(cursor);
            }
        }
        return numbers;
    }

    @android.support.annotation.NonNull
    private static com.navdy.client.app.framework.models.PhoneNumberModel getPhoneNumber(android.database.Cursor cursor) {
        return new com.navdy.client.app.framework.models.PhoneNumberModel(com.navdy.client.app.providers.DbUtils.getString(cursor, "data1"), com.navdy.client.app.providers.DbUtils.getInt(cursor, "data2"), com.navdy.client.app.providers.DbUtils.getString(cursor, "data3"), com.navdy.client.app.providers.DbUtils.getBoolean(cursor, "is_primary"));
    }

    private static android.database.Cursor getPhoneCursor(android.content.ContentResolver contentResolver, java.lang.String startsWith, boolean starredOnly) {
        java.lang.String selection = null;
        java.lang.String[] args = null;
        if (starredOnly) {
            selection = "starred>?";
            args = new java.lang.String[]{java.lang.String.valueOf(0)};
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(startsWith)) {
            selection = "display_name LIKE ?";
            args = new java.lang.String[]{startsWith.trim() + "%"};
        }
        android.database.Cursor cursor = null;
        try {
            return contentResolver.query(android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phoneProjection, selection, args, "sort_key ASC");
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get a phone number cursor", e);
            return cursor;
        }
    }

    private static android.database.Cursor getAddressCursor(android.content.ContentResolver contentResolver, java.lang.String startsWith) {
        java.lang.String selection = null;
        java.lang.String[] selectionArgs = null;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(startsWith)) {
            selection = "display_name LIKE ?";
            selectionArgs = new java.lang.String[]{startsWith.trim() + "%"};
        }
        android.database.Cursor cursor = null;
        try {
            return contentResolver.query(android.provider.ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, addressProjection, selection, selectionArgs, "sort_key ASC");
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get an address cursor", e);
            return cursor;
        }
    }

    private static android.database.Cursor getGenericCursor(android.content.ContentResolver contentResolver, java.lang.String startsWith) {
        java.lang.String selection = null;
        java.lang.String[] selectionArgs = null;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(startsWith)) {
            selection = "display_name LIKE ?";
            selectionArgs = new java.lang.String[]{startsWith.trim() + "%"};
        }
        android.database.Cursor cursor = null;
        try {
            return contentResolver.query(android.provider.ContactsContract.Data.CONTENT_URI, genericProjection, selection, selectionArgs, "sort_key ASC");
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
            return cursor;
        }
    }

    @android.support.annotation.Nullable
    public static java.lang.String getContactName(android.content.ContentResolver cr, long id) {
        android.database.Cursor cursor = null;
        try {
            android.content.ContentResolver contentResolver = cr;
            cursor = contentResolver.query(android.provider.ContactsContract.Data.CONTENT_URI, new java.lang.String[]{"display_name"}, "contact_id=?", new java.lang.String[]{java.lang.String.valueOf(id)}, null);
            if (cursor != null && cursor.moveToFirst()) {
                return com.navdy.client.app.providers.DbUtils.getString(cursor, "display_name");
            }
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
            return null;
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    private static android.graphics.Bitmap loadContactPhoto(long id, boolean highRes) {
        java.io.InputStream input = null;
        try {
            android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
            android.net.Uri uri = android.content.ContentUris.withAppendedId(android.provider.ContactsContract.Contacts.CONTENT_URI, id);
            input = android.provider.ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, highRes);
            if (input == null) {
                return null;
            }
            android.graphics.Bitmap scaledBitmap = com.navdy.client.app.framework.util.ImageUtils.getScaledBitmap((com.navdy.client.app.framework.util.ImageUtils.StreamFactory) new com.navdy.client.app.framework.util.ContactsManager.Anon1(contentResolver, uri, highRes), com.navdy.client.app.NavdyApplication.getAppContext().getResources().getDimensionPixelSize(com.navdy.client.R.dimen.contact_image_width), com.navdy.client.app.NavdyApplication.getAppContext().getResources().getDimensionPixelSize(com.navdy.client.R.dimen.contact_image_height));
            com.navdy.service.library.util.IOUtils.closeStream(input);
            return scaledBitmap;
        } catch (java.lang.Exception e) {
            logger.e((java.lang.Throwable) e);
            return null;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(input);
        }
    }

    @android.annotation.TargetApi(14)
    private static java.lang.String getUserProfileEmail() {
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        android.database.Cursor cursor = null;
        try {
            cursor = contentResolver.query(android.net.Uri.withAppendedPath(android.provider.ContactsContract.Profile.CONTENT_URI, com.navdy.client.app.framework.util.CarMdClient.DATA), new java.lang.String[]{"data1"}, "mimetype=?", new java.lang.String[]{"vnd.android.cursor.item/email_v2"}, "is_primary DESC, is_primary DESC");
            if (cursor == null || cursor.getCount() <= 0) {
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                return null;
            } else if (cursor.moveToFirst()) {
                return com.navdy.client.app.providers.DbUtils.getString(cursor, "data1");
            } else {
                com.navdy.service.library.util.IOUtils.closeStream(cursor);
                return null;
            }
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
    }

    public static com.navdy.client.app.framework.models.UserAccountInfo lookUpUserProfileInfo() {
        android.database.Cursor cursor = null;
        com.navdy.client.app.framework.models.UserAccountInfo uai = new com.navdy.client.app.framework.models.UserAccountInfo("", "", null);
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        try {
            cursor = contentResolver.query(android.provider.ContactsContract.Profile.CONTENT_URI, new java.lang.String[]{"_id", "display_name"}, "is_user_profile = 1", null, null);
            if (cursor == null || !cursor.moveToFirst()) {
                logger.v("The profile cursor is null or empty!");
            } else {
                java.lang.String id = com.navdy.client.app.providers.DbUtils.getString(cursor, "_id");
                uai.fullName = com.navdy.client.app.providers.DbUtils.getString(cursor, "display_name");
                logger.v("***** name = " + uai.fullName);
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(id)) {
                    long contactId = java.lang.Long.parseLong(id);
                    uai.email = getUserProfileEmail();
                    logger.v("***** email = " + uai.email);
                    uai.photo = loadContactPhoto(contactId, true);
                    logger.v("***** photo = " + uai.photo);
                }
            }
        } catch (java.lang.Exception e) {
            logger.e("Something went wrong while trying to get a contact cursor", e);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(cursor);
        }
        return uai;
    }

    private static void updateContactData(java.lang.String oldLookupKey, long oldContactId, java.lang.String newLookupKey, long newContactId) {
        android.util.Pair<java.lang.String, java.lang.String[]> selection;
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(oldLookupKey, newLookupKey)) {
            logger.d("updateContactData: The lookup keys are the same so no-op");
            return;
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(oldLookupKey) && oldContactId > 0) {
            selection = new android.util.Pair<>("contact_lookup_key=? OR last_known_contact_id=?", new java.lang.String[]{oldLookupKey, java.lang.String.valueOf(oldContactId)});
        } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(oldLookupKey) && oldContactId <= 0) {
            selection = new android.util.Pair<>("contact_lookup_key=?", new java.lang.String[]{oldLookupKey});
        } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(oldLookupKey) || oldContactId <= 0) {
            logger.e("Can't update contact data because oldLookupKey = " + oldLookupKey + " and oldContactId = " + oldContactId);
            return;
        } else {
            selection = new android.util.Pair<>("last_known_contact_id=?", new java.lang.String[]{java.lang.String.valueOf(oldContactId)});
        }
        logger.d("Updating any destination that matches: selection = " + ((java.lang.String) selection.first) + " args = " + java.util.Arrays.toString((java.lang.Object[]) selection.second));
        android.content.ContentResolver contentResolver = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
        android.content.ContentValues contentValues = new android.content.ContentValues();
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTACT_LOOKUP_KEY, newLookupKey);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_KNOWN_CONTACT_ID, java.lang.Long.valueOf(newContactId));
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_LAST_CONTACT_LOOKUP, java.lang.Long.valueOf(new java.util.Date().getTime()));
        contentResolver.update(com.navdy.client.app.providers.NavdyContentProviderConstants.DESTINATIONS_CONTENT_URI, contentValues, (java.lang.String) selection.first, (java.lang.String[]) selection.second);
    }
}
