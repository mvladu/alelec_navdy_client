package com.navdy.client.app.framework.servicehandler;

public class LocationTransmitter {
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.LocationTransmitter.class);
    protected boolean isRunning;
    protected boolean mConnected;
    protected final android.content.Context mContext;
    private android.location.LocationListener mLocationListener = new com.navdy.client.app.framework.servicehandler.LocationTransmitter.Anon1();
    protected android.location.LocationManager mLocationManager;
    protected final android.os.Looper mLocationReceiverLooper;
    protected final com.navdy.service.library.device.RemoteDevice mRemoteDevice;

    class Anon1 implements android.location.LocationListener {
        Anon1() {
        }

        public void onLocationChanged(android.location.Location location) {
            com.navdy.client.app.framework.servicehandler.LocationTransmitter.this.transmitLocation(location);
        }

        public void onStatusChanged(java.lang.String provider, int status, android.os.Bundle extras) {
            com.navdy.client.app.framework.servicehandler.LocationTransmitter.sLogger.v("status: " + provider + com.navdy.client.app.framework.util.CrashlyticsAppender.SEPARATOR + status);
        }

        public void onProviderEnabled(java.lang.String provider) {
            com.navdy.client.app.framework.servicehandler.LocationTransmitter.sLogger.e("enabled: " + provider);
        }

        public void onProviderDisabled(java.lang.String provider) {
            com.navdy.client.app.framework.servicehandler.LocationTransmitter.sLogger.e("disabled: " + provider);
        }
    }

    class Anon2 implements com.navdy.service.library.device.RemoteDevice.Listener {
        Anon2() {
        }

        public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice device) {
        }

        public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice device) {
            com.navdy.client.app.framework.servicehandler.LocationTransmitter.this.mConnected = true;
        }

        public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
            com.navdy.client.app.framework.servicehandler.LocationTransmitter.this.mConnected = false;
        }

        public void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        }

        public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.events.NavdyEvent event) {
        }

        public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, byte[] event) {
        }
    }

    public LocationTransmitter(android.content.Context context, com.navdy.service.library.device.RemoteDevice remoteDevice) {
        android.os.HandlerThread handlerThread = new android.os.HandlerThread("location-transmitter");
        handlerThread.start();
        this.mLocationReceiverLooper = handlerThread.getLooper();
        this.mContext = context;
        this.mRemoteDevice = remoteDevice;
        remoteDevice.addListener(new com.navdy.client.app.framework.servicehandler.LocationTransmitter.Anon2());
        this.mConnected = this.mRemoteDevice.isConnected();
        this.mLocationManager = (android.location.LocationManager) this.mContext.getSystemService("location");
    }

    protected boolean transmitLocation(android.location.Location androidLocation) {
        if (!this.mConnected) {
            sLogger.e("Can't transmit: not connected");
            return false;
        } else if (androidLocation == null) {
            sLogger.e("Invalid location - null");
            return false;
        } else {
            com.navdy.service.library.events.location.Coordinate coordinate = new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(androidLocation.getLatitude()), java.lang.Double.valueOf(androidLocation.getLongitude()), java.lang.Float.valueOf(androidLocation.getAccuracy()), java.lang.Double.valueOf(androidLocation.getAltitude()), java.lang.Float.valueOf(androidLocation.getBearing()), java.lang.Float.valueOf(androidLocation.getSpeed()), java.lang.Long.valueOf(java.lang.System.currentTimeMillis()), androidLocation.getProvider());
            sLogger.v("sending coordinate: lat=" + coordinate.latitude + ", lng=" + coordinate.longitude);
            return this.mRemoteDevice.postEvent((com.squareup.wire.Message) coordinate);
        }
    }

    public boolean start() {
        try {
            if (this.isRunning) {
                sLogger.i("location transmitter already started.");
                return false;
            } else if (com.navdy.client.app.NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                this.mLocationManager.requestLocationUpdates("gps", 0, 0.0f, this.mLocationListener, this.mLocationReceiverLooper);
                this.isRunning = true;
                sLogger.v("starting location updates");
                sendLastLocation();
                return true;
            } else {
                sLogger.v("permission for ACCESS_FINE_LOCATION failed");
                return false;
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }

    public boolean stop() {
        if (!this.isRunning) {
            sLogger.e("location transmitter not running");
            return false;
        }
        try {
            if (com.navdy.client.app.NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                this.mLocationManager.removeUpdates(this.mLocationListener);
            } else {
                sLogger.v("mLocationManager.removeUpdates failed. Permission for ACCESS_FINE_LOCATION was not found.");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        sLogger.v("stopping location updates");
        this.isRunning = false;
        return true;
    }

    private void sendLastLocation() {
        try {
            int permission = com.navdy.client.app.NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
            android.location.Location lastLocation = null;
            java.util.List<java.lang.String> providers = this.mLocationManager.getAllProviders();
            if (permission == 0) {
                for (java.lang.String str : providers) {
                    android.location.Location l = this.mLocationManager.getLastKnownLocation(str);
                    if (l != null) {
                        if (lastLocation == null) {
                            lastLocation = l;
                        } else if (l.getTime() > lastLocation.getTime()) {
                            lastLocation = l;
                        }
                    }
                }
                if (lastLocation == null) {
                    sLogger.v("no last location to transmit");
                    return;
                }
                sLogger.v("transmit last location:" + lastLocation);
                transmitLocation(lastLocation);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
