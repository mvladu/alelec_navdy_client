package com.navdy.client.app.framework.i18n;

public class I18nManager {
    private static final java.util.List<java.lang.String> IMPERIAL_UNIT_SYSTEM_LOCALES = new java.util.ArrayList();
    private static final java.lang.String LIBERIA_COUNTRY = "LR";
    public static final double METERS_TO_KILOMETERS = 0.001d;
    public static final double METERS_TO_MILES = 6.21371E-4d;
    private static final java.lang.String MYANMAR_COUNTRY = "MM";
    private static final com.navdy.client.app.framework.i18n.I18nManager instance = new com.navdy.client.app.framework.i18n.I18nManager();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.i18n.I18nManager.class);
    private static final java.lang.Object unitSystemLock = new java.lang.Object();
    private com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem currentUnitSystem;
    private final java.util.List<java.lang.ref.WeakReference<com.navdy.client.app.framework.i18n.I18nManager.Listener>> listeners;

    class Anon1 implements com.navdy.client.app.framework.i18n.I18nManager.CallListener {
        Anon1() {
        }

        public void call(com.navdy.client.app.framework.i18n.I18nManager.Listener listener) {
            listener.onUnitSystemChanged(com.navdy.client.app.framework.i18n.I18nManager.this.currentUnitSystem);
        }
    }

    private interface CallListener {
        void call(com.navdy.client.app.framework.i18n.I18nManager.Listener listener);
    }

    public interface Listener {
        void onUnitSystemChanged(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem);
    }

    private class LocaleReceiver extends android.content.BroadcastReceiver {

        class Anon1 implements com.navdy.client.app.framework.i18n.I18nManager.CallListener {
            Anon1() {
            }

            public void call(com.navdy.client.app.framework.i18n.I18nManager.Listener listener) {
                listener.onUnitSystemChanged(com.navdy.client.app.framework.i18n.I18nManager.this.currentUnitSystem);
            }
        }

        class Anon2 implements java.lang.Runnable {
            Anon2() {
            }

            public void run() {
                com.navdy.client.app.ui.settings.SettingsUtils.incrementDriverProfileSerial();
                com.navdy.client.app.ui.settings.SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }

        private LocaleReceiver() {
        }

        /* synthetic */ LocaleReceiver(com.navdy.client.app.framework.i18n.I18nManager x0, com.navdy.client.app.framework.i18n.I18nManager.Anon1 x1) {
            this();
        }

        public void onReceive(android.content.Context context, android.content.Intent intent) {
            com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem;
            if (intent != null && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(intent.getAction(), "android.intent.action.LOCALE_CHANGED")) {
                com.navdy.client.app.framework.i18n.I18nManager.logger.i("Locale has changed, new locale is " + java.util.Locale.getDefault());
                if (com.navdy.client.app.ui.settings.SettingsUtils.getUnitSystem() == null) {
                    if (com.navdy.client.app.framework.i18n.I18nManager.this.driverCountryIsImperial()) {
                        unitSystem = com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL;
                    } else {
                        unitSystem = com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC;
                    }
                    synchronized (com.navdy.client.app.framework.i18n.I18nManager.unitSystemLock) {
                        com.navdy.client.app.framework.i18n.I18nManager.this.currentUnitSystem = unitSystem;
                        com.navdy.client.app.framework.i18n.I18nManager.this.callListeners(new com.navdy.client.app.framework.i18n.I18nManager.LocaleReceiver.Anon1());
                    }
                }
                com.navdy.client.app.framework.util.VersioningUtils.increaseVersionAndSendToDisplay(com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType.FAVORITES);
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.i18n.I18nManager.LocaleReceiver.Anon2(), 1);
            }
        }
    }

    static {
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(java.util.Locale.US.getCountry());
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(java.util.Locale.UK.getCountry());
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(LIBERIA_COUNTRY);
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(MYANMAR_COUNTRY);
        com.navdy.client.app.framework.i18n.AddressUtils.initAddressFormats();
    }

    public static com.navdy.client.app.framework.i18n.I18nManager getInstance() {
        return instance;
    }

    private I18nManager() {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem = com.navdy.client.app.ui.settings.SettingsUtils.getUnitSystem();
        if (unitSystem == null) {
            if (driverCountryIsImperial()) {
                unitSystem = com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL;
            } else {
                unitSystem = com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC;
            }
        }
        this.currentUnitSystem = unitSystem;
        com.navdy.client.app.NavdyApplication.getAppContext().registerReceiver(new com.navdy.client.app.framework.i18n.I18nManager.LocaleReceiver(this, null), new android.content.IntentFilter("android.intent.action.LOCALE_CHANGED"));
        this.listeners = new java.util.ArrayList();
    }

    private boolean driverCountryIsImperial() {
        return IMPERIAL_UNIT_SYSTEM_LOCALES.contains(java.util.Locale.getDefault().getCountry());
    }

    public void addListener(com.navdy.client.app.framework.i18n.I18nManager.Listener listener) {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem;
        this.listeners.add(new java.lang.ref.WeakReference(listener));
        synchronized (unitSystemLock) {
            unitSystem = this.currentUnitSystem;
        }
        listener.onUnitSystemChanged(unitSystem);
    }

    public void removeListener(com.navdy.client.app.framework.i18n.I18nManager.Listener listener) {
        this.listeners.remove(new java.lang.ref.WeakReference(listener));
    }

    public com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem getUnitSystem() {
        com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem;
        synchronized (unitSystemLock) {
            unitSystem = this.currentUnitSystem;
        }
        return unitSystem;
    }

    public void setUnitSystem(com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem unitSystem) {
        synchronized (unitSystemLock) {
            this.currentUnitSystem = unitSystem;
            com.navdy.client.app.ui.settings.SettingsUtils.setUnitSystem(unitSystem);
            callListeners(new com.navdy.client.app.framework.i18n.I18nManager.Anon1());
        }
    }

    private void callListeners(com.navdy.client.app.framework.i18n.I18nManager.CallListener callListener) {
        java.util.ListIterator<java.lang.ref.WeakReference<com.navdy.client.app.framework.i18n.I18nManager.Listener>> iterator = this.listeners.listIterator();
        while (iterator.hasNext()) {
            com.navdy.client.app.framework.i18n.I18nManager.Listener listener = (com.navdy.client.app.framework.i18n.I18nManager.Listener) ((java.lang.ref.WeakReference) iterator.next()).get();
            if (listener != null) {
                callListener.call(listener);
            } else {
                iterator.remove();
            }
        }
    }

    @android.support.annotation.Nullable
    public java.util.Locale getCurrentLocale() {
        android.content.res.Resources resources = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
        if (resources == null) {
            return null;
        }
        android.content.res.Configuration configuration = resources.getConfiguration();
        if (configuration == null) {
            return null;
        }
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return configuration.getLocales().get(0);
        }
        return configuration.locale;
    }
}
