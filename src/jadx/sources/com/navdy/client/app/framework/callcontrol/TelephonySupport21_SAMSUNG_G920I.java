package com.navdy.client.app.framework.callcontrol;

public class TelephonySupport21_SAMSUNG_G920I extends com.navdy.client.app.framework.callcontrol.TelephonySupport21 {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.class);

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ android.media.session.MediaController val$m;

        Anon1(android.media.session.MediaController mediaController) {
            this.val$m = mediaController;
        }

        public void run() {
            try {
                this.val$m.dispatchMediaButtonEvent(new android.view.KeyEvent(0, 79));
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.sLogger.i("HEADSETHOOK keydown sent:" + this.val$m.getPackageName());
                this.val$m.dispatchMediaButtonEvent(new android.view.KeyEvent(1, 79));
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.sLogger.i("HEADSETHOOK keyup sent:" + this.val$m.getPackageName());
            } catch (Throwable t) {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.sLogger.e(t);
            }
        }
    }

    class Anon2 implements java.lang.Runnable {
        Anon2() {
        }

        public void run() {
            try {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.sLogger.i("reject call:using telephony interface");
                com.navdy.client.app.framework.callcontrol.TelephonySupport.endPhoneCall();
            } catch (Throwable t) {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.sLogger.e("endCall failed", t);
            }
        }
    }

    public TelephonySupport21_SAMSUNG_G920I(android.content.Context context) {
        super(context);
    }

    @android.annotation.TargetApi(21)
    protected void accept(android.media.session.MediaController m) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.Anon1(m), 4);
    }

    @android.annotation.TargetApi(21)
    protected void reject(android.media.session.MediaController m) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I.Anon2(), 4);
    }

    protected void end(android.media.session.MediaController m) {
        accept(m);
    }
}
