package com.navdy.client.app.framework;

@dagger.Module(injects = {com.navdy.client.app.NavdyApplication.class, com.navdy.client.app.ui.settings.AudioSettingsActivity.class, com.navdy.client.app.framework.servicehandler.SpeechServiceHandler.class, com.navdy.client.app.framework.util.TTSAudioRouter.class, com.navdy.client.app.framework.util.CarMdClient.class, com.navdy.client.app.framework.servicehandler.NetworkStatusManager.class, com.navdy.client.app.framework.AppInstance.class, com.navdy.client.app.ui.search.SearchActivity.class, com.navdy.client.app.ui.homescreen.SuggestionsFragment.class, com.navdy.client.app.ui.routing.RoutingActivity.class, com.navdy.client.app.ui.favorites.FavoritesEditActivity.class, com.navdy.client.debug.SubmitTicketFragment.class, com.navdy.client.app.framework.servicehandler.VoiceServiceHandler.class, com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity.class, com.navdy.client.app.service.ActivityRecognizedCallbackService.class}, library = true)
public class ProdModule {
    private com.navdy.client.app.framework.util.TTSAudioRouter audioRouter;
    private android.content.Context context;
    private com.navdy.service.library.network.http.IHttpManager httpManager;
    private com.navdy.client.app.framework.servicehandler.NetworkStatusManager networkStatusManager;
    private android.content.SharedPreferences settingsSharedPreference;

    public ProdModule(android.content.Context context2) {
        this.context = context2;
    }

    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.service.library.network.http.IHttpManager provideHttpManager() {
        if (this.httpManager == null) {
            this.httpManager = new com.navdy.service.library.network.http.HttpManager();
        }
        return this.httpManager;
    }

    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.client.app.framework.util.TTSAudioRouter provideTTSAudioRouter() {
        if (this.audioRouter == null) {
            this.audioRouter = new com.navdy.client.app.framework.util.TTSAudioRouter();
        }
        return this.audioRouter;
    }

    @javax.inject.Singleton
    @dagger.Provides
    public android.content.SharedPreferences provideSharedPreferences() {
        if (this.settingsSharedPreference == null) {
            this.settingsSharedPreference = com.navdy.client.app.NavdyApplication.getAppContext().getSharedPreferences(com.navdy.client.app.ui.settings.SettingsConstants.SETTINGS, 0);
        }
        return this.settingsSharedPreference;
    }

    @javax.inject.Singleton
    @dagger.Provides
    public com.navdy.client.app.framework.servicehandler.NetworkStatusManager provideNetworkStatusManager() {
        if (this.networkStatusManager == null) {
            this.networkStatusManager = new com.navdy.client.app.framework.servicehandler.NetworkStatusManager();
            this.networkStatusManager.register();
        }
        return this.networkStatusManager;
    }
}
