package com.navdy.client.app.framework;

public class DeviceConnection implements com.navdy.service.library.device.RemoteDevice.Listener, com.navdy.client.app.framework.AppInstance.Listener {
    private static final com.navdy.client.app.framework.DeviceConnection instance = new com.navdy.client.app.framework.DeviceConnection();
    private android.os.Handler handler = new android.os.Handler(android.os.Looper.getMainLooper());
    private com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.DeviceConnection.class);
    private com.navdy.service.library.device.RemoteDevice remoteDevice;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$device;

        Anon1(com.navdy.service.library.device.RemoteDevice remoteDevice) {
            this.val$device = remoteDevice;
        }

        public void run() {
            com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.DeviceConnection.DeviceConnectingEvent(this.val$device));
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$device;

        Anon2(com.navdy.service.library.device.RemoteDevice remoteDevice) {
            this.val$device = remoteDevice;
        }

        public void run() {
            com.navdy.client.app.framework.AppInstance.getInstance().onDeviceConnectedFirstResponder(this.val$device);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.connection.Connection.ConnectionFailureCause val$cause;
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$device;

        Anon3(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause connectionFailureCause) {
            this.val$device = remoteDevice;
            this.val$cause = connectionFailureCause;
        }

        public void run() {
            com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.DeviceConnection.DeviceConnectionFailedEvent(this.val$device, this.val$cause));
        }
    }

    class Anon4 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.device.connection.Connection.DisconnectCause val$cause;
        final /* synthetic */ com.navdy.service.library.device.RemoteDevice val$device;

        Anon4(com.navdy.service.library.device.RemoteDevice remoteDevice, com.navdy.service.library.device.connection.Connection.DisconnectCause disconnectCause) {
            this.val$device = remoteDevice;
            this.val$cause = disconnectCause;
        }

        public void run() {
            com.navdy.client.app.framework.AppInstance.getInstance().onDeviceDisconnectedFirstResponder(this.val$device, this.val$cause);
        }
    }

    class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.squareup.wire.Message val$message;

        Anon5(com.squareup.wire.Message message) {
            this.val$message = message;
        }

        public void run() {
            com.navdy.client.app.framework.util.BusProvider.getInstance().post(this.val$message);
        }
    }

    public static class DeviceConnectedEvent extends com.navdy.client.app.framework.DeviceConnection.DeviceRelatedEvent {
        public DeviceConnectedEvent(com.navdy.service.library.device.RemoteDevice device) {
            super(device);
        }
    }

    public static class DeviceConnectingEvent extends com.navdy.client.app.framework.DeviceConnection.DeviceRelatedEvent {
        DeviceConnectingEvent(com.navdy.service.library.device.RemoteDevice device) {
            super(device);
        }
    }

    public static class DeviceConnectionFailedEvent extends com.navdy.client.app.framework.DeviceConnection.DeviceRelatedEvent {
        com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause;

        DeviceConnectionFailedEvent(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause2) {
            super(device);
            this.cause = cause2;
        }

        public java.lang.String toString() {
            return "DeviceConnectionFailedEvent: cause=[" + this.cause + "] device=[" + this.device + "]";
        }
    }

    public static class DeviceDisconnectedEvent extends com.navdy.client.app.framework.DeviceConnection.DeviceRelatedEvent {
        com.navdy.service.library.device.connection.Connection.DisconnectCause cause;

        public DeviceDisconnectedEvent(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause2) {
            super(device);
            this.cause = cause2;
        }

        public java.lang.String toString() {
            return "DeviceConnectionFailedEvent: cause=[" + this.cause + "] device=[" + this.device + "]";
        }
    }

    public static class DeviceInfoEvent {
        public com.navdy.service.library.events.DeviceInfo deviceInfo;

        DeviceInfoEvent(com.navdy.service.library.events.DeviceInfo deviceInfo2) {
            this.deviceInfo = deviceInfo2;
        }

        public java.lang.String toString() {
            return "DeviceInfoEvent: info=[" + this.deviceInfo + "]";
        }
    }

    private static class DeviceRelatedEvent {
        public com.navdy.service.library.device.RemoteDevice device;

        DeviceRelatedEvent(com.navdy.service.library.device.RemoteDevice device2) {
            this.device = device2;
        }
    }

    private DeviceConnection() {
        com.navdy.client.app.framework.AppInstance appInstance = com.navdy.client.app.framework.AppInstance.getInstance();
        appInstance.addListener(this);
        onDeviceChanged(appInstance.getRemoteDevice());
    }

    public static synchronized com.navdy.client.app.framework.DeviceConnection getInstance() {
        com.navdy.client.app.framework.DeviceConnection deviceConnection;
        synchronized (com.navdy.client.app.framework.DeviceConnection.class) {
            deviceConnection = instance;
        }
        return deviceConnection;
    }

    public synchronized com.navdy.service.library.device.connection.ConnectionInfo getConnectionInfo() {
        com.navdy.service.library.device.connection.ConnectionInfo connectionInfo;
        if (this.remoteDevice != null) {
            connectionInfo = this.remoteDevice.getActiveConnectionInfo();
        } else {
            connectionInfo = null;
        }
        return connectionInfo;
    }

    public synchronized com.navdy.service.library.device.connection.Connection.Status getConnectionStatus() {
        com.navdy.service.library.device.connection.Connection.Status status;
        if (this.remoteDevice != null) {
            status = this.remoteDevice.getConnectionStatus();
        } else {
            status = com.navdy.service.library.device.connection.Connection.Status.DISCONNECTED;
        }
        return status;
    }

    public com.navdy.service.library.device.connection.ConnectionType getConnectionType() {
        com.navdy.service.library.device.connection.ConnectionInfo info = getConnectionInfo();
        if (info != null) {
            return info.getType();
        }
        return com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF;
    }

    public static boolean isConnected() {
        boolean z;
        synchronized (instance) {
            z = instance.remoteDevice != null && instance.remoteDevice.isConnected();
        }
        return z;
    }

    public static boolean postEvent(com.squareup.wire.Message message) {
        boolean z;
        synchronized (instance) {
            z = instance.remoteDevice != null && instance.remoteDevice.postEvent(message);
        }
        return z;
    }

    public static boolean postEvent(com.navdy.service.library.events.NavdyEvent event) {
        boolean z;
        synchronized (instance) {
            z = instance.remoteDevice != null && instance.remoteDevice.postEvent(event);
        }
        return z;
    }

    private synchronized void close() {
        if (this.remoteDevice != null) {
            this.remoteDevice.removeListener(this);
            this.remoteDevice = null;
        }
    }

    public synchronized void onDeviceChanged(com.navdy.service.library.device.RemoteDevice newDevice) {
        if (this.remoteDevice != null) {
            this.remoteDevice.removeListener(this);
        }
        this.remoteDevice = newDevice;
        if (this.remoteDevice != null) {
            this.remoteDevice.addListener(this);
        }
    }

    public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice device) {
        this.logger.d("Connection status changed: CONNECTING | " + device);
        this.handler.post(new com.navdy.client.app.framework.DeviceConnection.Anon1(device));
    }

    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice device) {
        this.logger.d("Connection status changed: CONNECTED | " + device);
        this.handler.post(new com.navdy.client.app.framework.DeviceConnection.Anon2(device));
    }

    public void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.ConnectionFailureCause cause) {
        this.logger.d("Connection status changed: CONNECT_FAIL | " + device);
        this.handler.post(new com.navdy.client.app.framework.DeviceConnection.Anon3(device, cause));
    }

    public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.device.connection.Connection.DisconnectCause cause) {
        this.logger.d("Connection status changed: DISCONNECTED | " + device);
        this.handler.post(new com.navdy.client.app.framework.DeviceConnection.Anon4(device, cause));
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, byte[] event) {
    }

    public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice device, com.navdy.service.library.events.NavdyEvent event) {
        com.squareup.wire.Message message = com.navdy.service.library.events.NavdyEventUtil.messageFromEvent(event);
        if (message != null) {
            this.handler.post(new com.navdy.client.app.framework.DeviceConnection.Anon5(message));
        }
    }
}
