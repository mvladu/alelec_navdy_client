package com.navdy.client.app.framework.callcontrol;

public class TelephonySupport21_SAMSUNG_G950 extends com.navdy.client.app.framework.callcontrol.TelephonySupport21 {
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G950.class);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            try {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G950.sLogger.i("reject call:using telephony interface");
                com.navdy.client.app.framework.callcontrol.TelephonySupport.endPhoneCall();
            } catch (Throwable t) {
                com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G950.sLogger.e("endCall failed", t);
            }
        }
    }

    public TelephonySupport21_SAMSUNG_G950(android.content.Context context) {
        super(context);
    }

    @android.annotation.TargetApi(21)
    protected void reject(android.media.session.MediaController m) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G950.Anon1(), 4);
    }
}
