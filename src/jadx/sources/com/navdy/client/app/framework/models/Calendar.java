package com.navdy.client.app.framework.models;

public class Calendar {
    public java.lang.String accountName = "";
    public java.lang.String accountType = "";
    public int calendarColor = 0;
    public java.lang.String displayName = "";
    public long id = 0;
    public java.lang.String ownerAccount = "";
    public com.navdy.client.app.framework.models.Calendar.CalendarListItemType type;
    public boolean visible = true;

    public enum CalendarListItemType {
        GLOBAL_SWITCH(0),
        TITLE(1),
        CALENDAR(2);
        
        private int value;

        private CalendarListItemType(int value2) {
            this.value = 0;
            this.value = value2;
        }

        public int getValue() {
            return this.value;
        }

        public static com.navdy.client.app.framework.models.Calendar.CalendarListItemType fromValue(int value2) {
            return values()[value2];
        }
    }

    public Calendar(com.navdy.client.app.framework.models.Calendar.CalendarListItemType type2) {
        this.type = type2;
    }

    public Calendar(com.navdy.client.app.framework.models.Calendar.CalendarListItemType type2, java.lang.String displayName2) {
        this.type = type2;
        this.displayName = displayName2;
    }

    public Calendar(com.navdy.client.app.framework.models.Calendar.CalendarListItemType type2, long id2, java.lang.String displayName2, int calendarColor2, java.lang.String accountName2, java.lang.String accountType2, java.lang.String ownerAccount2, boolean visible2) {
        this.type = type2;
        this.id = id2;
        this.displayName = displayName2;
        this.calendarColor = calendarColor2;
        this.accountName = accountName2;
        this.accountType = accountType2;
        this.ownerAccount = ownerAccount2;
        this.visible = visible2;
    }

    public java.lang.String toString() {
        return "Calendar{type=" + this.type + ", id=" + this.id + ", displayName='" + this.displayName + '\'' + ", calendarColor=" + this.calendarColor + ", calendarName='" + this.accountName + '\'' + ", accountType='" + this.accountType + '\'' + ", ownerAccount='" + this.ownerAccount + '\'' + ", visible='" + this.visible + '\'' + '}';
    }
}
