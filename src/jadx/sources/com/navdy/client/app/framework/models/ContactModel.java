package com.navdy.client.app.framework.models;

public class ContactModel {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.ContactModel.class);
    public java.util.List<com.navdy.client.app.framework.models.Address> addresses = new java.util.ArrayList();
    public boolean favorite;
    public long id;
    public java.lang.String lookupKey;
    public long lookupTimestamp = -1;
    public java.lang.String name;
    public java.util.List<com.navdy.client.app.framework.models.PhoneNumberModel> phoneNumbers = new java.util.ArrayList();
    public android.graphics.Bitmap photo;

    public java.lang.String toString() {
        return "ContactModel{id=" + this.id + ", lookupKey='" + this.lookupKey + '\'' + ", lookupTimestamp=" + this.lookupTimestamp + ", name='" + this.name + '\'' + ", addresses=" + this.addresses + ", phoneNumbers=" + this.phoneNumbers + ", photo=" + this.photo + ", favorite=" + this.favorite + '}';
    }

    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.navdy.client.app.framework.models.ContactModel)) {
            return false;
        }
        com.navdy.client.app.framework.models.ContactModel otherContact = (com.navdy.client.app.framework.models.ContactModel) obj;
        if (this.addresses.size() != otherContact.addresses.size()) {
            return false;
        }
        for (com.navdy.client.app.framework.models.Address address : this.addresses) {
            boolean found = false;
            for (com.navdy.client.app.framework.models.Address address2 : otherContact.addresses) {
                if (!address.equals(address2)) {
                    found = true;
                }
            }
            if (!found) {
                return false;
            }
        }
        if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(this.name, otherContact.name) || this.id != otherContact.id) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (int) this.id;
    }

    public void addAddress(com.navdy.client.app.framework.models.Address address) {
        this.addresses.add(address);
    }

    public void addAddresses(java.util.List<com.navdy.client.app.framework.models.Address> addresses2) {
        this.addresses.addAll(addresses2);
    }

    public void addPhoneNumber(com.navdy.client.app.framework.models.PhoneNumberModel number) {
        this.phoneNumbers.add(number);
    }

    public void addPhoneNumbers(java.util.List<com.navdy.client.app.framework.models.PhoneNumberModel> numbers) {
        if (numbers != null) {
            this.phoneNumbers.addAll(numbers);
        }
    }

    public void setName(java.lang.String name2) {
        this.name = name2;
    }

    public void setID(long id2) {
        this.id = id2;
    }

    public void setLookupKey(java.lang.String lookupKey2) {
        this.lookupKey = lookupKey2;
    }

    public void setLookupTimestampToNow() {
        this.lookupTimestamp = new java.util.Date().getTime();
    }

    public void setPhoto(android.graphics.Bitmap photo2) {
        this.photo = photo2;
    }

    public void deduplicatePhoneNumbers() {
        if (this.phoneNumbers != null) {
            java.util.Map<java.lang.Long, com.navdy.client.app.framework.models.PhoneNumberModel> numbers = new java.util.LinkedHashMap<>();
            for (com.navdy.client.app.framework.models.PhoneNumberModel phoneNumber : this.phoneNumbers) {
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(phoneNumber.number)) {
                    try {
                        numbers.put(java.lang.Long.valueOf(com.navdy.client.app.framework.util.SystemUtils.convertToNumber(phoneNumber.number)), phoneNumber);
                    } catch (java.lang.Exception t) {
                        logger.v("Unable to convert this to a phone number: " + phoneNumber.number + " " + t.getMessage());
                    }
                }
            }
            this.phoneNumbers.clear();
            this.phoneNumbers.addAll(numbers.values());
        }
    }
}
