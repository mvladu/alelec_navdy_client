package com.navdy.client.app.framework.service;

public class NavdyCustomNotificationListenerService extends android.service.notification.NotificationListenerService {
    private static final int INDENT_INCREMENT = 4;
    private static final long NOTIFICATION_WAIT_FOR_MORE_DELAY = 1000;
    private static final java.lang.String SPACES = " ";
    public static final android.os.Handler notifListenerServiceHandler = new android.os.Handler();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger("NavdyNotifHandler");
    public static final java.util.regex.Pattern splitTickerTextOnColon = java.util.regex.Pattern.compile("(.*): (.*)");
    private java.util.List<java.lang.String> activeMusicApps = new java.util.ArrayList();
    private java.util.HashMap<java.lang.String, java.lang.Runnable> incomingNotifications = new java.util.HashMap<>();
    private java.util.HashMap<java.lang.String, java.lang.Boolean> knownGroups = new java.util.HashMap<>();
    private java.util.HashMap<java.lang.String, java.lang.Boolean> knownNotifications = new java.util.HashMap<>();
    private java.lang.String lastOngoing = "";
    private java.util.HashMap<java.lang.String, java.lang.Runnable> pendingGroupNotifications = new java.util.HashMap<>();
    private final android.content.SharedPreferences sharedPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ android.service.notification.StatusBarNotification val$sbn;

        Anon1(android.service.notification.StatusBarNotification statusBarNotification) {
            this.val$sbn = statusBarNotification;
        }

        public void run() {
            com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.handleStatusBarNotification(this.val$sbn);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ java.lang.String val$finalTicker;
        final /* synthetic */ android.app.Notification val$notif;
        final /* synthetic */ java.lang.String val$packageName;
        final /* synthetic */ android.service.notification.StatusBarNotification val$sbn;

        class Anon1 implements java.lang.Runnable {

            /* renamed from: com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService$Anon2$Anon1$Anon1 reason: collision with other inner class name */
            class C0048Anon1 implements java.lang.Runnable {

                /* renamed from: com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService$Anon2$Anon1$Anon1$Anon1 reason: collision with other inner class name */
                class C0049Anon1 implements java.lang.Runnable {
                    C0049Anon1() {
                    }

                    public void run() {
                        com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.categorizeAndHandleNotification(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$sbn, com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$packageName, com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$finalTicker);
                    }
                }

                C0048Anon1() {
                }

                public void run() {
                    com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.Anon1.C0048Anon1.C0049Anon1(), 7);
                }
            }

            Anon1() {
            }

            public void run() {
                java.lang.String group = android.support.v4.app.NotificationCompat.getGroup(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$notif);
                java.lang.Boolean isKnown = java.lang.Boolean.valueOf(!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(group) && java.lang.Boolean.TRUE.equals(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.knownGroups.get(group)));
                boolean isGroupSummary = android.support.v4.app.NotificationCompat.isGroupSummary(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$notif);
                com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger.d("NotifListenerService: handleStatusBarNotification: pkg [" + com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$packageName + "]" + " id [" + com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$sbn.getId() + "]" + " tag [" + com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$sbn.getTag() + "]" + " ticker[" + com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$finalTicker + "]" + " group[" + group + "]" + " isKnownGroup[" + isKnown + "]" + " isGroupSummary[" + isGroupSummary + "]");
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(group)) {
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.knownGroups.put(group, java.lang.Boolean.valueOf(true));
                }
                if (!isGroupSummary && !com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$packageName, com.navdy.client.app.framework.glances.GlanceConstants.WHATS_APP)) {
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger.i("Remove Pending Handling For Group: " + group);
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.categorizeAndHandleNotification(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$sbn, com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$packageName, com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$finalTicker);
                } else if (!java.lang.Boolean.TRUE.equals(isKnown) || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.this.val$packageName, com.navdy.client.app.framework.glances.GlanceConstants.WHATS_APP)) {
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger.i("Remove Pending Handling For Group: " + group);
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
                    java.lang.Runnable r = new com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.Anon1.C0048Anon1();
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger.i("Adding Pending Handling For Group: " + group);
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.pendingGroupNotifications.put(group, r);
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.notifListenerServiceHandler.postDelayed(r, 1000);
                } else {
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger.i("Ignoring summary notifications to prevent duplicates.");
                }
            }
        }

        Anon2(android.app.Notification notification, java.lang.String str, android.service.notification.StatusBarNotification statusBarNotification, java.lang.String str2) {
            this.val$notif = notification;
            this.val$packageName = str;
            this.val$sbn = statusBarNotification;
            this.val$finalTicker = str2;
        }

        public void run() {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2.Anon1(), 7);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ android.service.notification.StatusBarNotification val$sbn;

        Anon3(android.service.notification.StatusBarNotification statusBarNotification) {
            this.val$sbn = statusBarNotification;
        }

        public void run() {
            if (this.val$sbn != null) {
                com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger.d("onNotificationRemoved: pkg [" + this.val$sbn.getPackageName() + "]" + " tag [" + this.val$sbn.getTag() + "]" + " id [" + this.val$sbn.getId() + "]" + " time [" + this.val$sbn.getPostTime() + "]");
                if (com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.isMusicNotification(this.val$sbn)) {
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.removeActiveMusicApp(this.val$sbn.getPackageName());
                }
                com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.cancelPendingHandlerIfAnyFor(this.val$sbn);
                if (android.support.v4.app.NotificationCompat.isGroupSummary(this.val$sbn.getNotification())) {
                    java.lang.String group = android.support.v4.app.NotificationCompat.getGroup(this.val$sbn.getNotification());
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.knownGroups.remove(group);
                    com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
                }
                com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.knownNotifications.remove(com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.this.getHash(this.val$sbn));
            }
        }
    }

    public NavdyCustomNotificationListenerService() {
        com.navdy.client.app.framework.glances.GlanceConstants.addPackageToGroup(com.navdy.client.app.ui.settings.SettingsUtils.getDialerPackage(com.navdy.client.app.NavdyApplication.getAppContext().getPackageManager()), com.navdy.client.app.framework.glances.GlanceConstants.Group.IGNORE_GROUP);
    }

    public void onCreate() {
        sLogger.d("onCreate");
        super.onCreate();
    }

    public void onDestroy() {
        sLogger.d("onDestroy");
        super.onDestroy();
    }

    public void onNotificationPosted(android.service.notification.StatusBarNotification sbn) {
        if (sbn != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon1(sbn), 7);
        }
    }

    /* access modifiers changed from: private */
    @android.support.annotation.NonNull
    public java.lang.String getHash(@android.support.annotation.NonNull android.service.notification.StatusBarNotification sbn) {
        return sbn.getId() + com.navdy.client.app.framework.models.Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + sbn.getPackageName() + com.navdy.client.app.framework.models.Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + sbn.getTag() + com.navdy.client.app.framework.models.Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + getText(sbn);
    }

    @android.support.annotation.Nullable
    private java.lang.String getText(@android.support.annotation.Nullable android.service.notification.StatusBarNotification sbn) {
        if (sbn == null) {
            return null;
        }
        android.app.Notification notif = sbn.getNotification();
        if (notif == null) {
            return null;
        }
        android.os.Bundle extras = android.support.v4.app.NotificationCompat.getExtras(notif);
        if (extras == null) {
            return null;
        }
        java.lang.String text = getLastMessageInCarConversation(extras);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text)) {
            return text;
        }
        java.lang.String text2 = getStringSafely(extras, "android.text");
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text2)) {
            return text2;
        }
        java.lang.String text3 = getStringSafely(extras, "android.bigText");
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text3)) {
            return text3;
        }
        java.lang.String text4 = getStringSafely(extras, "android.title");
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text4)) {
            return text4;
        }
        if (notif.tickerText != null) {
            return notif.tickerText.toString();
        }
        return null;
    }

    @android.support.annotation.Nullable
    public static java.lang.String getLastMessageInCarConversation(@android.support.annotation.NonNull android.os.Bundle extras) {
        android.os.Bundle carExt = extras.getBundle(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT);
        if (carExt == null) {
            return null;
        }
        android.os.Bundle convo = carExt.getBundle(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT_CONVERSATION);
        if (convo == null) {
            return null;
        }
        android.os.Parcelable[] messages = convo.getParcelableArray(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES);
        if (messages == null || messages.length <= 0) {
            return null;
        }
        android.os.Parcelable lastMessage = messages[messages.length - 1];
        if (lastMessage == null || !(lastMessage instanceof android.os.Bundle)) {
            return null;
        }
        java.lang.String text = ((android.os.Bundle) lastMessage).getString(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES_TEXT);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(text)) {
            return text;
        }
        return null;
    }

    @android.support.annotation.Nullable
    public static java.lang.String getFirstParticipantInCarConversation(@android.support.annotation.NonNull android.os.Bundle extras) {
        android.os.Bundle carExt = extras.getBundle(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT);
        if (carExt != null) {
            android.os.Bundle convo = carExt.getBundle(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT_CONVERSATION);
            if (convo != null) {
                java.lang.String[] pariticipants = convo.getStringArray(com.navdy.client.app.framework.glances.GlanceConstants.CAR_EXT_CONVERSATION_PARTICIPANTS);
                if (pariticipants != null && pariticipants.length > 0) {
                    return pariticipants[0];
                }
            }
        }
        return null;
    }

    @android.support.annotation.Nullable
    public static java.lang.String getLastTextLine(@android.support.annotation.NonNull android.os.Bundle extras) {
        java.lang.Object textLines = extras.get("android.textLines");
        if (textLines != null && (textLines instanceof java.lang.CharSequence[])) {
            java.lang.CharSequence[] lines = (java.lang.CharSequence[]) textLines;
            java.lang.CharSequence lastLine = lines[lines.length - 1];
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(lastLine)) {
                return lastLine.toString();
            }
        }
        return null;
    }

    @android.support.annotation.Nullable
    public static java.lang.String getStringSafely(@android.support.annotation.NonNull android.os.Bundle extras, @android.support.annotation.NonNull java.lang.String key) {
        java.lang.Object o = extras.get(key);
        if (o instanceof java.lang.String) {
            return (java.lang.String) o;
        }
        if (o instanceof java.lang.CharSequence) {
            return o.toString();
        }
        return null;
    }

    private void handleStatusBarNotification(@android.support.annotation.NonNull android.service.notification.StatusBarNotification sbn) {
        try {
            java.lang.String packageName = sbn.getPackageName();
            android.app.Notification notif = sbn.getNotification();
            android.os.Bundle extras = android.support.v4.app.NotificationCompat.getExtras(notif);
            if (isMusicNotification(sbn)) {
                sLogger.i("Music notification posted: " + packageName);
                addActiveMusicApp(packageName);
            } else if (!this.sharedPrefs.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.GLANCES, false)) {
            } else {
                if (!sbn.isOngoing()) {
                    java.lang.String ticker = null;
                    if (notif.tickerText != null) {
                        ticker = notif.tickerText.toString();
                    }
                    if (com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group.IGNORE_GROUP)) {
                        sLogger.d("handleStatusBarNotification: ignored [" + packageName + "]");
                    } else if (!isConnectedToHud()) {
                        sLogger.v("handleStatusBarNotification: hud not connected [" + packageName + "]");
                    } else if (extras == null) {
                        sLogger.v("handleStatusBarNotification: no extras [" + packageName + "]");
                    } else {
                        java.lang.String notificationHashKey = getHash(sbn);
                        if (java.lang.Boolean.TRUE.equals(this.knownNotifications.get(notificationHashKey))) {
                            sLogger.v("handleStatusBarNotification: already seen this notification: [" + notificationHashKey + "] so won't send to HUD to avoid duplicates");
                            return;
                        }
                        this.knownNotifications.put(notificationHashKey, java.lang.Boolean.valueOf(true));
                        java.lang.Runnable runnable = new com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon2(notif, packageName, sbn, ticker);
                        cancelPendingHandlerIfAnyFor(sbn);
                        sLogger.d("onNotificationPosted:   Adding a pending runnable for " + notificationHashKey);
                        this.incomingNotifications.put(notificationHashKey, runnable);
                        notifListenerServiceHandler.postDelayed(runnable, 1000);
                    }
                } else if (!android.text.TextUtils.equals(this.lastOngoing, packageName)) {
                    sLogger.v("handleStatusBarNotification: ongoing notif skip:" + packageName);
                    this.lastOngoing = packageName;
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void categorizeAndHandleNotification(android.service.notification.StatusBarNotification sbn, java.lang.String packageName, java.lang.String ticker) {
        sLogger.d("categorizeAndHandleNotification: [" + packageName + "] ticker[" + ticker + "]");
        if (com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group.MESSAGING_GROUP)) {
            com.navdy.client.app.framework.glances.MessagingNotificationHandler.handleMessageNotification(sbn);
        } else if (com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group.EMAIL_GROUP)) {
            com.navdy.client.app.framework.glances.EmailNotificationHandler.handleEmailNotification(sbn);
        } else if (com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group.CALENDAR_GROUP) || com.navdy.client.app.framework.glances.GlanceConstants.ANDROID_CALENDAR.equals(packageName)) {
            if (!com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_CALENDAR.equals(packageName) || ticker != null) {
                com.navdy.client.app.framework.glances.CalendarNotificationHandler.handleCalendarNotification(sbn);
            } else {
                sLogger.v("null ticker");
            }
        } else if (com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group.SOCIAL_GROUP)) {
            com.navdy.client.app.framework.glances.SocialNotificationHandler.handleSocialNotification(sbn);
        } else {
            com.navdy.client.app.framework.glances.GenericNotificationHandler.handleGenericNotification(sbn);
            sLogger.v("NotifListenerService: generic notification [" + packageName + "]");
        }
    }

    public void onNotificationRemoved(android.service.notification.StatusBarNotification sbn) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.Anon3(sbn), 7);
    }

    public void cancelPendingHandlerIfAnyFor(android.service.notification.StatusBarNotification sbn) {
        java.lang.String notificationHashKey = getHash(sbn);
        java.lang.Runnable previousRunnable = (java.lang.Runnable) this.incomingNotifications.get(notificationHashKey);
        if (previousRunnable != null) {
            sLogger.d("onNotificationPosted: Removing a pending runnable for " + notificationHashKey);
            notifListenerServiceHandler.removeCallbacks(previousRunnable);
        }
    }

    public void removePendingHandlingForGroup(java.lang.String group) {
        java.lang.Runnable pendingRunnable = (java.lang.Runnable) this.pendingGroupNotifications.get(group);
        if (pendingRunnable != null) {
            notifListenerServiceHandler.removeCallbacks(pendingRunnable);
        }
    }

    private boolean isMusicNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        if (com.navdy.client.app.framework.glances.GlanceConstants.SPOTIFY.equals(packageName)) {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            java.lang.String category = android.support.v4.app.NotificationCompat.getCategory(sbn.getNotification());
            if (category != null && category.equals("transport")) {
                return true;
            }
        }
        if (!sbn.isOngoing() || !com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group.MUSIC_GROUP)) {
            return false;
        }
        return true;
    }

    public void addActiveMusicApp(java.lang.String packageName) {
        if (!this.activeMusicApps.contains(packageName)) {
            this.activeMusicApps.add(packageName);
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().setLastMusicApp(packageName);
        }
    }

    public void removeActiveMusicApp(java.lang.String packageName) {
        if (this.activeMusicApps.contains(packageName)) {
            this.activeMusicApps.remove(packageName);
        }
    }

    private static boolean isConnectedToHud() {
        return com.navdy.client.app.framework.AppInstance.getInstance().isDeviceConnected();
    }

    private void print(android.service.notification.StatusBarNotification sbn) {
        if (sbn != null) {
            android.app.Notification notif = sbn.getNotification();
            sLogger.d("################ STATUS BAR NOTIFICATION ###############");
            sLogger.d("Time: " + sbn.getPostTime());
            sLogger.d("ID: " + sbn.getId());
            sLogger.d("Tag: " + sbn.getTag());
            sLogger.d("Package: " + sbn.getPackageName());
            sLogger.d("Ongoing: " + sbn.isOngoing());
            sLogger.d("Clearable: " + sbn.isClearable());
            logNotification(notif, 2);
        }
    }

    private void logNotification(android.app.Notification notif, int indentation) {
        sLogger.d(java.lang.String.format("%" + indentation + "s################ NOTIFICATION", new java.lang.Object[]{SPACES}));
        sLogger.d(java.lang.String.format("%" + indentation + "sPriority: %s", new java.lang.Object[]{SPACES, java.lang.Integer.valueOf(notif.priority)}));
        sLogger.d(java.lang.String.format("%" + indentation + "sFlags: %s", new java.lang.Object[]{SPACES, java.lang.Integer.valueOf(notif.flags)}));
        sLogger.d(java.lang.String.format("%" + indentation + "sTicker text: %s", new java.lang.Object[]{SPACES, notif.tickerText}));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            sLogger.d(java.lang.String.format("%" + indentation + "sPublic Version: %s", new java.lang.Object[]{SPACES, notif.publicVersion}));
        }
        java.lang.String category = android.support.v4.app.NotificationCompat.getCategory(notif);
        java.lang.String group = android.support.v4.app.NotificationCompat.getGroup(notif);
        java.lang.String sortKey = android.support.v4.app.NotificationCompat.getSortKey(notif);
        boolean isGroupSummary = android.support.v4.app.NotificationCompat.isGroupSummary(notif);
        sLogger.d(java.lang.String.format("%" + indentation + "sCategory: %s", new java.lang.Object[]{SPACES, category}));
        sLogger.d(java.lang.String.format("%" + indentation + "sGroup: %s", new java.lang.Object[]{SPACES, group}));
        sLogger.d(java.lang.String.format("%" + indentation + "sSort Key: %s", new java.lang.Object[]{SPACES, sortKey}));
        sLogger.d(java.lang.String.format("%" + indentation + "sIs Group Summary: %s", new java.lang.Object[]{SPACES, java.lang.Boolean.valueOf(isGroupSummary)}));
        android.os.Bundle extras = android.support.v4.app.NotificationCompat.getExtras(notif);
        sLogger.d(java.lang.String.format("%" + indentation + "s####### BUNDLE", new java.lang.Object[]{SPACES}));
        logBundle(indentation + 4, extras);
        sLogger.d(java.lang.String.format("%" + indentation + "s####### ACTIONS", new java.lang.Object[]{SPACES}));
        int actionCount = android.support.v4.app.NotificationCompat.getActionCount(notif);
        sLogger.d(java.lang.String.format("%" + indentation + "sAction Count: %d", new java.lang.Object[]{SPACES, java.lang.Integer.valueOf(actionCount)}));
        for (int i = 0; i < actionCount; i++) {
            logAction(indentation, android.support.v4.app.NotificationCompat.getAction(notif, i));
        }
    }

    public void logBundle(int indentation, android.os.Bundle extras) {
        if (extras != null && !extras.isEmpty()) {
            for (java.lang.String key : extras.keySet()) {
                logUnknownObject(indentation, key, extras.get(key));
            }
        }
    }

    public void logUnknownObject(int indentation, java.lang.String key, java.lang.Object value) {
        if (value == null) {
            sLogger.d(java.lang.String.format("%" + indentation + "s- %s: null", new java.lang.Object[]{SPACES, key}));
        } else if (value instanceof android.os.Bundle) {
            sLogger.d(java.lang.String.format("%" + indentation + "s####### %s: \"%s\" (%s)", new java.lang.Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
            logBundle(indentation + 4, (android.os.Bundle) value);
        } else if (value instanceof android.support.v4.app.RemoteInput) {
            logRemoteInput(indentation, (android.support.v4.app.RemoteInput) value);
        } else if (value instanceof android.app.Notification.Action) {
            logAction(indentation, (android.app.Notification.Action) value);
        } else if (value instanceof android.app.Notification) {
            logNotification((android.app.Notification) value, indentation);
        } else if (value instanceof java.lang.Object[]) {
            sLogger.d(java.lang.String.format("%" + indentation + "s- %s: \"%s\" (%s)", new java.lang.Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
            java.lang.Object[] array = (java.lang.Object[]) value;
            for (int i = 0; i < array.length; i++) {
                logUnknownObject(indentation + 4, key + "[" + i + "]", array[i]);
            }
        } else if (value instanceof java.util.List) {
            sLogger.d(java.lang.String.format("%" + indentation + "s- %s: \"%s\" (%s)", new java.lang.Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
            java.util.List array2 = (java.util.List) value;
            for (int i2 = 0; i2 < array2.size(); i2++) {
                logUnknownObject(indentation + 4, key + "[" + i2 + "]", array2.get(i2));
            }
        } else {
            sLogger.d(java.lang.String.format("%" + indentation + "s- %s: \"%s\" (%s)", new java.lang.Object[]{SPACES, key, value.toString(), value.getClass().getName()}));
        }
    }

    public void logRemoteInput(int indentation, @android.support.annotation.NonNull android.app.RemoteInput remoteInput) {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            android.support.v4.app.RemoteInput.Builder rib = new android.support.v4.app.RemoteInput.Builder(remoteInput.getResultKey());
            rib.setLabel(remoteInput.getLabel());
            rib.setChoices(remoteInput.getChoices());
            rib.setAllowFreeFormInput(remoteInput.getAllowFreeFormInput());
            rib.addExtras(remoteInput.getExtras());
            logRemoteInput(indentation, rib.build());
        }
    }

    public void logRemoteInput(int indentation, @android.support.annotation.NonNull android.support.v4.app.RemoteInput input) {
        sLogger.d(java.lang.String.format("%" + indentation + "sInput: %s", new java.lang.Object[]{SPACES, input.getLabel()}));
        sLogger.d(java.lang.String.format("%" + indentation + "sAllowFreeFormInput: %s", new java.lang.Object[]{SPACES, java.lang.Boolean.valueOf(input.getAllowFreeFormInput())}));
        java.lang.CharSequence[] choices = input.getChoices();
        if (choices != null) {
            for (java.lang.CharSequence choice : choices) {
                sLogger.d(java.lang.String.format("%" + indentation + "s  + Choice: %s", new java.lang.Object[]{SPACES, choice}));
            }
        }
    }

    public void logAction(int indentation, @android.support.annotation.NonNull android.app.Notification.Action action) {
        logAction(indentation, new android.support.v4.app.NotificationCompat.Action.Builder(action.icon, action.title, action.actionIntent).build());
    }

    public void logAction(int indentation, @android.support.annotation.NonNull android.support.v4.app.NotificationCompat.Action action) {
        sLogger.d(java.lang.String.format("%" + indentation + "s-- %s:", new java.lang.Object[]{SPACES, action.title}));
        sLogger.d(java.lang.String.format("%" + (indentation + 4) + "sAction Intent: %s", new java.lang.Object[]{SPACES, action.actionIntent}));
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            sLogger.d(java.lang.String.format("%" + (indentation + 4) + "sAllow generated replies: %b", new java.lang.Object[]{SPACES, java.lang.Boolean.valueOf(action.getAllowGeneratedReplies())}));
        }
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            logBundle(indentation + 4, action.getExtras());
            android.support.v4.app.RemoteInput[] remoteInputs = action.getRemoteInputs();
            if (remoteInputs != null) {
                for (android.support.v4.app.RemoteInput remoteInput : remoteInputs) {
                    logRemoteInput(indentation + 4, remoteInput);
                }
            }
        }
    }
}
