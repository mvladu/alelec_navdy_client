package com.navdy.client.app.framework.search;

public final class GooglePlacesSearch {
    public static final int DEFAULT_RADIUS_VALUE = 1609;
    private static final java.lang.String DESTINATION_PLACE_ID_SYNTAX = "destination=place_id:";
    private static final java.lang.String DESTINATION_SYNTAX = "destination=";
    private static final java.lang.String INPUT_SYNTAX = "input=";
    private static final java.lang.String KEYWORD_SYNTAX = "keyword=";
    private static final java.lang.String KEY_SYNTAX = "key=";
    private static final java.lang.String LOCATION_SYNTAX = "location=";
    private static final java.lang.String OPENNOW_SYNTAX = "opennow=";
    private static final java.lang.String OPENNOW_VALUE = "true";
    private static final java.lang.String ORIGIN_SYNTAX = "origin=";
    private static final java.lang.String PLACE_ID_SYNTAX = "placeid=";
    private static final java.lang.String QUERY_SYNTAX = "query=";
    private static final java.lang.String RADIUS_SYNTAX = "radius=";
    private static final java.lang.String RANKBY_SYNTAX = "rankby=";
    private static final int SERVICE_RADIUS_VALUE = 8046;
    private static final java.lang.String TYPE_SYNTAX = "type=";
    private static final boolean VERBOSE = false;
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.search.GooglePlacesSearch.class);
    private java.lang.String apiKEY = "";
    private com.navdy.client.app.framework.search.GooglePlacesSearch.Query currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.NEARBY;
    private com.navdy.client.app.framework.models.Destination destination;
    private com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener googleSearchListener;
    private android.os.Handler handler;
    private java.lang.String location_value;
    private java.lang.Runnable onSuccess = new com.navdy.client.app.framework.search.GooglePlacesSearch.Anon1();
    private java.lang.String placeId;
    private java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> results;

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.search.GooglePlacesSearch.this.sendDestinationsToListener(com.navdy.client.app.framework.search.GooglePlacesSearch.this.results, com.navdy.client.app.framework.search.GooglePlacesSearch.Error.NONE);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ int val$searchRadius;
        final /* synthetic */ java.lang.String val$searchText;
        final /* synthetic */ com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes val$type;

        Anon2(java.lang.String str, com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceTypes, int i) {
            this.val$searchText = str;
            this.val$type = serviceTypes;
            this.val$searchRadius = i;
        }

        public void run() {
            if (com.navdy.client.app.framework.search.GooglePlacesSearch.this.currentQueryType == com.navdy.client.app.framework.search.GooglePlacesSearch.Query.DETAILS && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(com.navdy.client.app.framework.search.GooglePlacesSearch.this.placeId)) {
                java.lang.String placeDetailJson = com.navdy.client.app.providers.NavdyContentProvider.getNonStalePlaceDetailInfoFor(com.navdy.client.app.framework.search.GooglePlacesSearch.this.placeId);
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(placeDetailJson)) {
                    com.navdy.client.app.framework.search.GooglePlacesSearch.logger.v("Place detail data is not stale so using it.");
                    com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destinationResult = com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.createDestinationObject(placeDetailJson);
                    com.navdy.client.app.framework.search.GooglePlacesSearch.this.results = new java.util.ArrayList();
                    com.navdy.client.app.framework.search.GooglePlacesSearch.this.results.add(destinationResult);
                    if (com.navdy.client.app.framework.search.GooglePlacesSearch.this.handler != null) {
                        com.navdy.client.app.framework.search.GooglePlacesSearch.this.handler.post(com.navdy.client.app.framework.search.GooglePlacesSearch.this.onSuccess);
                        return;
                    } else {
                        com.navdy.client.app.framework.search.GooglePlacesSearch.this.onSuccess.run();
                        return;
                    }
                }
            }
            com.navdy.client.app.framework.search.GooglePlacesSearch.this.runSearchWebApi(this.val$searchText, this.val$type, this.val$searchRadius);
        }
    }

    public enum Error {
        NONE,
        SERVICE_ERROR
    }

    public interface GoogleSearchListener {
        void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> list, com.navdy.client.app.framework.search.GooglePlacesSearch.Query query, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error);
    }

    public enum Query {
        QUERY_AUTOCOMPLETE,
        TEXTSEARCH,
        DETAILS,
        NEARBY,
        DIRECTIONS;

        public static java.lang.String getBaseUrl(com.navdy.client.app.framework.search.GooglePlacesSearch.Query query) {
            switch (query) {
                case QUERY_AUTOCOMPLETE:
                    return "https://maps.googleapis.com/maps/api/place/queryautocomplete/json?";
                case TEXTSEARCH:
                    return "https://maps.googleapis.com/maps/api/place/textsearch/json?";
                case DETAILS:
                    return "https://maps.googleapis.com/maps/api/place/details/json?";
                case DIRECTIONS:
                    return "https://maps.googleapis.com/maps/api/directions/json?";
                default:
                    return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
            }
        }
    }

    private enum RankByTypes {
        PROMINENCE,
        DISTANCE;

        public static java.lang.String getBaseUrl(com.navdy.client.app.framework.search.GooglePlacesSearch.RankByTypes type) {
            switch (type) {
                case PROMINENCE:
                    return "prominence";
                default:
                    return "distance";
            }
        }
    }

    public enum ServiceTypes {
        GAS("gas_station", "Gas"),
        PARKING("parking", "Parking"),
        FOOD("restaurant", "Food"),
        COFFEE("cafe", "Coffee"),
        ATM("atm", "ATMs"),
        HOSPITAL("hospital", "Hospitals"),
        STORE("convenience_store", "Grocery Stores");
        
        java.lang.String baseUrl;
        java.lang.String hudServiceType;

        private ServiceTypes(java.lang.String baseUrl2, java.lang.String hudServiceType2) {
            this.baseUrl = baseUrl2;
            this.hudServiceType = hudServiceType2;
        }

        public java.lang.String getBaseUrl() {
            return this.baseUrl;
        }

        public java.lang.String getHudServiceType() {
            return this.hudServiceType;
        }
    }

    public GooglePlacesSearch(com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener listener, android.os.Handler handler2) {
        this.handler = handler2;
        init(listener);
    }

    public GooglePlacesSearch(com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener listener) {
        init(listener);
    }

    private void init(com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener listener) {
        this.apiKEY = KEY_SYNTAX + com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_google_web_service));
        this.googleSearchListener = listener;
        com.navdy.service.library.events.location.Coordinate location = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (location != null) {
            this.location_value = location.latitude + "," + location.longitude;
        }
    }

    public void runQueryAutoCompleteWebApi(java.lang.String query) {
        logger.d("executing Query Auto Complete");
        this.currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.QUERY_AUTOCOMPLETE;
        runSearchWebApiInTaskManager(query);
    }

    public void runTextSearchWebApi(java.lang.String query) {
        logger.d("executing Text Search");
        this.currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.TEXTSEARCH;
        runSearchWebApiInTaskManager(query);
    }

    public void runServiceSearchWebApi(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes type) {
        runServiceSearchWebApi(type, SERVICE_RADIUS_VALUE);
    }

    public void runServiceSearchWebApi(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes type, int searchRadius) {
        if (type == null) {
            logger.e("Trying to call runServiceSearchWebApi with a null service type. You crazy?");
            return;
        }
        logger.d("executing Service Search");
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.location_value)) {
            this.currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.NEARBY;
        } else {
            this.currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.TEXTSEARCH;
        }
        runSearchWebApiInTaskManager(null, type, searchRadius);
    }

    public void runNearbySearchWebApi(java.lang.String query) {
        logger.d("executing Nearby Search");
        this.currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.NEARBY;
        runSearchWebApiInTaskManager(query);
    }

    public void runDetailsSearchWebApi(java.lang.String placeId2) {
        this.placeId = placeId2;
        this.currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.DETAILS;
        runSearchWebApiInTaskManager(null);
    }

    public void runDirectionsSearchWebApi(com.navdy.client.app.framework.models.Destination d) {
        this.destination = d;
        this.placeId = d.placeId;
        this.currentQueryType = com.navdy.client.app.framework.search.GooglePlacesSearch.Query.DIRECTIONS;
        runSearchWebApiInTaskManager(null);
    }

    private void runSearchWebApiInTaskManager(java.lang.String searchText) {
        runSearchWebApiInTaskManager(searchText, null, DEFAULT_RADIUS_VALUE);
    }

    private void runSearchWebApiInTaskManager(@android.support.annotation.Nullable java.lang.String searchText, @android.support.annotation.Nullable com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes type, int searchRadius) {
        if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.client.app.NavdyApplication.getAppContext())) {
            sendDestinationsToListener(null, com.navdy.client.app.framework.search.GooglePlacesSearch.Error.SERVICE_ERROR);
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.search.GooglePlacesSearch.Anon2(searchText, type, searchRadius), 3);
    }

    private void runSearchWebApi(java.lang.String searchText, @android.support.annotation.Nullable com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes type, int searchRadius) {
        java.lang.String typeString = "";
        if (type != null) {
            typeString = type.getBaseUrl();
        }
        logger.v("Running search with following parameters: " + searchText + " type: " + type);
        java.lang.String encodedSearchText = android.net.Uri.encode(searchText);
        java.lang.StringBuilder url = new java.lang.StringBuilder();
        url.append(com.navdy.client.app.framework.search.GooglePlacesSearch.Query.getBaseUrl(this.currentQueryType));
        url.append(this.apiKEY);
        switch (this.currentQueryType) {
            case QUERY_AUTOCOMPLETE:
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(LOCATION_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                url.append(RADIUS_SYNTAX);
                url.append(DEFAULT_RADIUS_VALUE);
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(encodedSearchText)) {
                    url.append("&");
                    url.append(INPUT_SYNTAX);
                    url.append(encodedSearchText);
                    break;
                }
                break;
            case TEXTSEARCH:
                if (com.navdy.client.app.framework.map.MapUtils.parseLatLng(searchText) == null) {
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.location_value)) {
                        url.append("&");
                        url.append(LOCATION_SYNTAX);
                        url.append(this.location_value);
                    }
                    url.append("&");
                    url.append(RADIUS_SYNTAX);
                    url.append(DEFAULT_RADIUS_VALUE);
                }
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(encodedSearchText)) {
                    url.append("&");
                    url.append(QUERY_SYNTAX);
                    url.append(encodedSearchText);
                    break;
                }
                break;
            case DETAILS:
                url.append("&");
                url.append(PLACE_ID_SYNTAX);
                url.append(this.placeId);
                break;
            case DIRECTIONS:
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(ORIGIN_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.placeId)) {
                    if (this.destination != null && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.destination.rawAddressNotForDisplay)) {
                        url.append(DESTINATION_SYNTAX);
                        url.append(android.net.Uri.encode(this.destination.rawAddressNotForDisplay));
                        break;
                    } else {
                        logger.e("Trying to call Google Directions API with a no placeId or address.");
                        break;
                    }
                } else {
                    url.append(DESTINATION_PLACE_ID_SYNTAX);
                    url.append(this.placeId);
                    break;
                }
                break;
            case NEARBY:
                if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.location_value)) {
                    url.append("&");
                    url.append(LOCATION_SYNTAX);
                    url.append(this.location_value);
                }
                url.append("&");
                url.append(RANKBY_SYNTAX);
                if (type != null) {
                    switch (type) {
                        case GAS:
                        case PARKING:
                            url.append(com.navdy.client.app.framework.search.GooglePlacesSearch.RankByTypes.getBaseUrl(com.navdy.client.app.framework.search.GooglePlacesSearch.RankByTypes.DISTANCE));
                            break;
                        default:
                            url.append(com.navdy.client.app.framework.search.GooglePlacesSearch.RankByTypes.getBaseUrl(com.navdy.client.app.framework.search.GooglePlacesSearch.RankByTypes.PROMINENCE));
                            url.append("&");
                            url.append(RADIUS_SYNTAX);
                            if (searchRadius <= 0) {
                                url.append(SERVICE_RADIUS_VALUE);
                                break;
                            } else {
                                url.append(searchRadius);
                                break;
                            }
                    }
                } else {
                    url.append(com.navdy.client.app.framework.search.GooglePlacesSearch.RankByTypes.getBaseUrl(com.navdy.client.app.framework.search.GooglePlacesSearch.RankByTypes.DISTANCE));
                    url.append("&");
                    url.append(KEYWORD_SYNTAX);
                    url.append(encodedSearchText);
                }
                url.append("&");
                url.append(OPENNOW_SYNTAX);
                url.append(OPENNOW_VALUE);
                break;
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(typeString)) {
            url.append("&");
            url.append(TYPE_SYNTAX);
            url.append(typeString);
        }
        java.lang.String urlString = url.toString();
        logger.d("Calling Google with URL: " + urlString.replaceAll(this.apiKEY, "key=API_KEY"));
        this.results = getSearchResults(urlString);
        if (this.handler != null) {
            this.handler.post(this.onSuccess);
        } else {
            this.onSuccess.run();
        }
    }

    private void sendDestinationsToListener(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
        this.googleSearchListener.onGoogleSearchResult(destinations, this.currentQueryType, error);
    }

    private java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> getSearchResults(java.lang.String url) {
        java.io.InputStream inputStream = null;
        java.util.ArrayList<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> resultDestinations = new java.util.ArrayList<>();
        try {
            java.net.URL url2 = new java.net.URL(url);
            inputStream = ((java.net.HttpURLConnection) url2.openConnection()).getInputStream();
            org.json.JSONObject jsonObject = new org.json.JSONObject(com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8"));
            logger.v("jsonObject received: " + jsonObject.toString());
            if (this.currentQueryType == com.navdy.client.app.framework.search.GooglePlacesSearch.Query.DIRECTIONS) {
                com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destination2 = com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.createDestinationObject(jsonObject);
                if (destination2 != null) {
                    resultDestinations.add(destination2);
                }
            } else {
                if (this.currentQueryType == com.navdy.client.app.framework.search.GooglePlacesSearch.Query.QUERY_AUTOCOMPLETE) {
                    org.json.JSONArray predictions = (org.json.JSONArray) jsonObject.get("predictions");
                    int predictionsLength = predictions.length();
                    for (int i = 0; i < predictionsLength; i++) {
                        com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destination3 = com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.createDestinationObject(predictions.getJSONObject(i));
                        if (destination3 != null) {
                            resultDestinations.add(destination3);
                        }
                    }
                } else {
                    if (this.currentQueryType != com.navdy.client.app.framework.search.GooglePlacesSearch.Query.DETAILS) {
                        org.json.JSONArray results2 = (org.json.JSONArray) jsonObject.get("results");
                        int resultsLength = results2.length();
                        for (int i2 = 0; i2 < resultsLength; i2++) {
                            com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destination4 = com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.createDestinationObject(results2.getJSONObject(i2));
                            if (destination4 != null) {
                                resultDestinations.add(destination4);
                            }
                        }
                    } else {
                        com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destination5 = com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.createDestinationObject((org.json.JSONObject) jsonObject.get("result"));
                        if (destination5 != null) {
                            resultDestinations.add(destination5);
                        }
                    }
                }
            }
            logger.v("resultDestinations size after parsing response: " + resultDestinations.size());
            return resultDestinations;
        } catch (Throwable e) {
            logger.v("throwable e: " + e);
            return null;
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(inputStream);
        }
    }

    public void setListener(com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener listener) {
        this.googleSearchListener = listener;
    }
}
