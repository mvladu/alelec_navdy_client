package com.navdy.client.app.framework.util;

public class MusicUtils {
    private static final android.net.Uri ALBUM_ART_URI = android.net.Uri.parse("content://media/external/audio/albumart");
    public static final int ARTWORK_SIZE = 200;
    private static final android.graphics.BitmapFactory.Options BITMAP_OPTIONS = new android.graphics.BitmapFactory.Options();
    private static final java.lang.String[] DEFAULT_ARTWORK_CHECKSUMS = {"a980062052ec31e96ae73029ec7e6772", "6bbaf7d1b3318d9414d51d8a71705a04", "3f89e08544651f0345b0ee39cb3ef3a4", "59d9cdaa3c6f52efd34ce5e8d282cf5e", "1cb6a67160592a08bf314e9742e0fed6", "4dba0b6fe14bf656c92aed8d50b56b8d"};
    private static final java.util.Map<com.navdy.service.library.events.audio.MusicEvent.Action, java.lang.Integer> KEY_CODES_MAPPING = new com.navdy.client.app.framework.util.MusicUtils.Anon1();
    private static final int MAX_PLAY_RETRIES = 10;
    private static final long PAUSE_BETWEEN_RETRIES = 500;
    private static final java.util.Map<com.navdy.service.library.events.audio.MusicEvent.Action, android.view.KeyEvent> SEEK_KEY_EVENTS_MAPPING = new com.navdy.client.app.framework.util.MusicUtils.Anon2();
    private static final java.lang.String SONG_ART_URI_POSTFIX = "/albumart";
    private static final java.lang.String SONG_URI = "content://media/external/audio/media/";
    private static volatile android.media.AudioManager audioManager = null;
    private static com.navdy.client.app.framework.music.LocalMusicPlayer localMusicPlayer = new com.navdy.client.app.framework.music.LocalMusicPlayer();
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.MusicUtils.class);
    private static android.view.KeyEvent startedContinuousKeyEvent = null;
    private static com.navdy.service.library.events.audio.MusicDataSource startedEventDataSource = null;

    static class Anon1 extends java.util.HashMap<com.navdy.service.library.events.audio.MusicEvent.Action, java.lang.Integer> {
        Anon1() {
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY, java.lang.Integer.valueOf(android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PLAY));
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PAUSE, java.lang.Integer.valueOf(android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PAUSE));
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PREVIOUS, java.lang.Integer.valueOf(88));
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_NEXT, java.lang.Integer.valueOf(87));
        }
    }

    static class Anon2 extends java.util.HashMap<com.navdy.service.library.events.audio.MusicEvent.Action, android.view.KeyEvent> {
        Anon2() {
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_REWIND_START, new android.view.KeyEvent(0, 89));
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_START, new android.view.KeyEvent(0, 90));
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_REWIND_STOP, new android.view.KeyEvent(1, 89));
            put(com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_STOP, new android.view.KeyEvent(1, 90));
        }
    }

    static class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicEvent val$event;

        Anon3(com.navdy.service.library.events.audio.MusicEvent musicEvent) {
            this.val$event = musicEvent;
        }

        public void run() {
            com.navdy.client.app.framework.util.MusicUtils.handleLocalPlayEvent(this.val$event);
        }
    }

    static class Anon4 implements com.navdy.client.app.framework.music.LocalMusicPlayer.Listener {
        private android.graphics.Bitmap albumArt = null;
        @android.support.annotation.NonNull
        private com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.EMPTY_TRACK_INFO;

        class Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.navdy.service.library.events.audio.MusicTrackInfo val$trackInfo;

            /* renamed from: com.navdy.client.app.framework.util.MusicUtils$Anon4$Anon1$Anon1 reason: collision with other inner class name */
            class C0051Anon1 implements java.lang.Runnable {
                final /* synthetic */ android.graphics.Bitmap val$bitmap;

                C0051Anon1(android.graphics.Bitmap bitmap) {
                    this.val$bitmap = bitmap;
                }

                public void run() {
                    if (this.val$bitmap == null || this.val$bitmap.getByteCount() == 0) {
                        com.navdy.client.app.framework.util.MusicUtils.sLogger.e("Received photo has null or empty byte array");
                        com.navdy.client.app.framework.util.MusicUtils.Anon4.this.setMusicTrackInfo(com.navdy.client.app.framework.util.MusicUtils.Anon4.Anon1.this.val$trackInfo);
                        com.navdy.client.app.framework.util.MusicUtils.Anon4.this.sendArtwork();
                        return;
                    }
                    try {
                        com.navdy.client.app.framework.util.MusicUtils.Anon4.this.albumArt = com.navdy.service.library.util.ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(this.val$bitmap, 200, 200, com.navdy.service.library.util.ScalingUtilities.ScalingLogic.FIT);
                        com.navdy.client.app.framework.util.MusicUtils.Anon4.this.setMusicTrackInfo(com.navdy.client.app.framework.util.MusicUtils.Anon4.Anon1.this.val$trackInfo);
                        com.navdy.client.app.framework.util.MusicUtils.Anon4.this.sendArtwork();
                    } catch (java.lang.Exception e) {
                        com.navdy.client.app.framework.util.MusicUtils.sLogger.e("Error updating the artwork", e);
                    }
                }
            }

            Anon1(com.navdy.service.library.events.audio.MusicTrackInfo musicTrackInfo) {
                this.val$trackInfo = musicTrackInfo;
            }

            public void run() {
                new android.os.Handler(android.os.Looper.getMainLooper()).post(new com.navdy.client.app.framework.util.MusicUtils.Anon4.Anon1.C0051Anon1(com.navdy.client.app.framework.util.MusicUtils.getLocalMusicPhoto(java.lang.String.valueOf(this.val$trackInfo.trackId))));
            }
        }

        Anon4() {
        }

        public void onMetadataUpdate(com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
            com.navdy.client.app.framework.util.MusicUtils.sLogger.v("onMetadataUpdate " + trackInfo);
            setMusicTrackInfo(trackInfo);
            this.albumArt = null;
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.MusicUtils.Anon4.Anon1(trackInfo), 1);
        }

        public void onPlaybackStateUpdate(com.navdy.service.library.events.audio.MusicPlaybackState musicPlaybackState) {
            com.navdy.client.app.framework.util.MusicUtils.sLogger.v("onPlaybackStateUpdate " + musicPlaybackState);
            setMusicTrackInfo(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.musicTrackInfo).playbackState(musicPlaybackState).build());
            sendArtwork();
        }

        public void onPositionUpdate(int position, int duration) {
            com.navdy.client.app.framework.util.MusicUtils.sLogger.v("onPositionUpdate");
            setMusicTrackInfo(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder(this.musicTrackInfo).currentPosition(java.lang.Integer.valueOf(position)).duration(java.lang.Integer.valueOf(duration)).build());
        }

        private void setMusicTrackInfo(@android.support.annotation.NonNull com.navdy.service.library.events.audio.MusicTrackInfo trackInfo) {
            com.navdy.client.app.framework.util.MusicUtils.sLogger.v("setMusicTrackInfo " + trackInfo);
            this.musicTrackInfo = trackInfo;
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().setCurrentMediaTrackInfo(this.musicTrackInfo);
        }

        private void sendArtwork() {
            com.navdy.client.app.framework.util.MusicUtils.sLogger.v("sendArtwork " + this.musicTrackInfo);
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().checkAndSetCurrentMediaArtwork(this.musicTrackInfo, this.albumArt);
        }
    }

    static class Anon5 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.audio.MusicEvent val$event;
        final /* synthetic */ java.lang.String val$packageName;

        Anon5(java.lang.String str, com.navdy.service.library.events.audio.MusicEvent musicEvent) {
            this.val$packageName = str;
            this.val$event = musicEvent;
        }

        public void run() {
            int retries = 0;
            while (retries < 10) {
                if (com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().getCurrentMediaTrackInfo().playbackState != com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING) {
                    if (com.navdy.client.app.framework.glances.GlanceConstants.SPOTIFY.equals(this.val$packageName)) {
                        com.navdy.client.app.framework.util.MusicUtils.broadcastMusicEvent(this.val$event);
                    } else {
                        broadcastPlayEventViaIntent(this.val$packageName);
                    }
                    try {
                        java.lang.Thread.sleep(500);
                        retries++;
                    } catch (java.lang.InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    com.navdy.client.app.framework.util.MusicUtils.sLogger.i("Started music");
                    return;
                }
            }
            com.navdy.client.app.framework.util.MusicUtils.sLogger.w("Failed to start music");
        }

        private void broadcastPlayEventViaIntent(java.lang.String packageName) {
            android.content.Intent intent = new android.content.Intent("android.intent.action.MEDIA_BUTTON");
            android.view.KeyEvent playButtonEvent = new android.view.KeyEvent(0, android.support.v4.media.TransportMediator.KEYCODE_MEDIA_PLAY);
            intent.setPackage(packageName);
            intent.putExtra("android.intent.extra.KEY_EVENT", playButtonEvent);
            com.navdy.client.app.NavdyApplication.getAppContext().sendOrderedBroadcast(intent, null);
        }
    }

    private static class MediaKeyEventRunnable implements java.lang.Runnable {
        private com.navdy.service.library.events.audio.MusicDataSource dataSource;
        private android.view.KeyEvent event;
        private boolean isDownUpEvent;

        public MediaKeyEventRunnable(com.navdy.service.library.events.audio.MusicDataSource dataSource2, android.view.KeyEvent event2, boolean isDownUpEvent2) {
            this.dataSource = dataSource2;
            this.event = event2;
            this.isDownUpEvent = isDownUpEvent2;
        }

        private void executeKeyDownUp(android.view.KeyEvent eventToRun) {
            com.navdy.client.app.framework.util.MusicUtils.audioManager.dispatchMediaKeyEvent(android.view.KeyEvent.changeAction(eventToRun, 0));
            com.navdy.client.app.framework.util.MusicUtils.audioManager.dispatchMediaKeyEvent(android.view.KeyEvent.changeAction(eventToRun, 1));
        }

        public void run() {
            if (this.isDownUpEvent) {
                executeKeyDownUp(this.event);
            } else {
                executeSplitKeyEvent(this.dataSource, this.event);
            }
        }

        private static synchronized void executeSplitKeyEvent(com.navdy.service.library.events.audio.MusicDataSource dataSource2, android.view.KeyEvent event2) {
            synchronized (com.navdy.client.app.framework.util.MusicUtils.MediaKeyEventRunnable.class) {
                int eventAction = event2.getAction();
                if (eventAction == 0) {
                    if (com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent != null) {
                        com.navdy.client.app.framework.util.MusicUtils.sLogger.w("Other key down event already started pressed event - ending it: " + java.lang.String.valueOf(com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent));
                        com.navdy.client.app.framework.util.MusicUtils.executeLongPressedKeyUp();
                    }
                    com.navdy.client.app.framework.util.MusicUtils.dispatchMediaKeyEvent(event2);
                    long uptime = android.os.SystemClock.uptimeMillis();
                    com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent = android.view.KeyEvent.changeTimeRepeat(event2, uptime, 0);
                    com.navdy.client.app.framework.util.MusicUtils.startedEventDataSource = dataSource2;
                    com.navdy.client.app.framework.util.MusicUtils.sLogger.v("Executing specific key event: " + java.lang.String.valueOf(com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent) + " with after-event for long press.");
                    com.navdy.client.app.framework.util.MusicUtils.dispatchMediaKeyEvent(com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent);
                    com.navdy.client.app.framework.util.MusicUtils.dispatchMediaKeyEvent(android.view.KeyEvent.changeTimeRepeat(event2, uptime, 1, event2.getFlags() | 128));
                } else if (eventAction != 1) {
                    com.navdy.client.app.framework.util.MusicUtils.dispatchMediaKeyEvent(event2);
                } else if (com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent != null) {
                    if (event2.getKeyCode() != com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent.getKeyCode()) {
                        com.navdy.client.app.framework.util.MusicUtils.sLogger.w("Unmatched key up event - ending the current just in case: " + java.lang.String.valueOf(com.navdy.client.app.framework.util.MusicUtils.startedContinuousKeyEvent));
                    }
                    com.navdy.client.app.framework.util.MusicUtils.executeLongPressedKeyUp();
                } else {
                    com.navdy.client.app.framework.util.MusicUtils.sLogger.w("Unexpected key up event - doing nothing: " + java.lang.String.valueOf(event2));
                }
            }
        }
    }

    private static void populateAudioManagerFromContext() {
        if (audioManager == null) {
            audioManager = (android.media.AudioManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("audio");
        }
    }

    public static synchronized void executeMusicAction(com.navdy.service.library.events.audio.MusicEvent event) throws java.lang.UnsupportedOperationException, android.content.pm.PackageManager.NameNotFoundException, java.io.IOException {
        synchronized (com.navdy.client.app.framework.util.MusicUtils.class) {
            sLogger.d("executeMusicAction " + event);
            populateAudioManagerFromContext();
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler musicServiceHandler = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance();
            com.navdy.service.library.events.audio.MusicEvent.Action action = event.action;
            com.navdy.service.library.events.audio.MusicTrackInfo currentTrackInfo = musicServiceHandler.getCurrentMediaTrackInfo();
            if (com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(event.collectionSource) && com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY.equals(action) && (event.index != null || event.shuffleMode == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS || (localMusicPlayer != null && localMusicPlayer.isActive()))) {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.MusicUtils.Anon3(event), 1);
            } else if (com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(currentTrackInfo.collectionSource)) {
                if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY.equals(action)) {
                    localMusicPlayer.play();
                } else if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PAUSE.equals(action)) {
                    localMusicPlayer.pause(true);
                } else if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_NEXT.equals(action)) {
                    localMusicPlayer.next();
                } else if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PREVIOUS.equals(action)) {
                    localMusicPlayer.previous();
                } else if (SEEK_KEY_EVENTS_MAPPING.containsKey(action)) {
                    if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_START.equals(action)) {
                        localMusicPlayer.startFastForward();
                    } else if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_FAST_FORWARD_STOP.equals(action)) {
                        localMusicPlayer.stopFastForward();
                    } else if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_REWIND_START.equals(action)) {
                        localMusicPlayer.startRewind();
                    } else if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_REWIND_STOP.equals(action)) {
                        localMusicPlayer.stopRewind();
                    }
                } else if (com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_MODE_CHANGE.equals(action)) {
                    localMusicPlayer.shuffle(event.shuffleMode);
                }
            } else if (musicServiceHandler.isMusicPlayerActive() || !com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY.equals(action)) {
                broadcastMusicEvent(event);
            } else {
                startMediaPlayer(event);
            }
        }
    }

    private static void handleLocalPlayEvent(com.navdy.service.library.events.audio.MusicEvent event) {
        android.database.Cursor membersCursor;
        if (isNewCollection(event)) {
            java.util.List<com.navdy.service.library.events.audio.MusicTrackInfo> trackList = new java.util.ArrayList<>();
            if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(event.collectionType)) {
                android.database.Cursor membersCursor2 = com.navdy.client.app.framework.util.MusicDbUtils.getPlaylistMembersCursor(event.collectionId);
                if (membersCursor2 != null) {
                    try {
                        if (membersCursor2.moveToFirst()) {
                            do {
                                trackList.add(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().collectionSource(event.collectionSource).collectionType(event.collectionType).collectionId(event.collectionId).name(membersCursor2.getString(membersCursor2.getColumnIndex("title"))).author(membersCursor2.getString(membersCursor2.getColumnIndex("artist"))).album(membersCursor2.getString(membersCursor2.getColumnIndex("album"))).trackId(membersCursor2.getString(membersCursor2.getColumnIndex("SourceId"))).playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING).isNextAllowed(java.lang.Boolean.valueOf(true)).isPreviousAllowed(java.lang.Boolean.valueOf(true)).build());
                            } while (membersCursor2.moveToNext());
                            membersCursor2.close();
                        }
                    } catch (Throwable th) {
                        com.navdy.service.library.util.IOUtils.closeObject(membersCursor2);
                        throw th;
                    }
                }
                com.navdy.service.library.util.IOUtils.closeObject(membersCursor2);
            } else {
                if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(event.collectionType)) {
                    membersCursor = com.navdy.client.app.framework.util.MusicDbUtils.getAlbumMembersCursor(event.collectionId);
                } else if (com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(event.collectionType)) {
                    membersCursor = com.navdy.client.app.framework.util.MusicDbUtils.getArtistMembersCursor(event.collectionId);
                } else {
                    sLogger.e("Unknown type");
                    return;
                }
                if (membersCursor != null) {
                    try {
                        if (membersCursor.moveToFirst()) {
                            do {
                                trackList.add(new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().collectionSource(event.collectionSource).collectionType(event.collectionType).collectionId(event.collectionId).name(membersCursor.getString(membersCursor.getColumnIndex("title"))).author(membersCursor.getString(membersCursor.getColumnIndex("artist"))).album(membersCursor.getString(membersCursor.getColumnIndex("album"))).trackId(java.lang.String.valueOf(membersCursor.getInt(membersCursor.getColumnIndex("_id")))).playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING).isNextAllowed(java.lang.Boolean.valueOf(true)).isPreviousAllowed(java.lang.Boolean.valueOf(true)).build());
                            } while (membersCursor.moveToNext());
                            membersCursor.close();
                        }
                    } catch (Throwable th2) {
                        com.navdy.service.library.util.IOUtils.closeObject(membersCursor);
                        throw th2;
                    }
                }
                com.navdy.service.library.util.IOUtils.closeObject(membersCursor);
            }
            localMusicPlayer.initWithQueue(trackList);
            if (!(event.shuffleMode == null || event.shuffleMode == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN)) {
                localMusicPlayer.shuffle(event.shuffleMode);
            }
            if (localMusicPlayer.getListener() == null) {
                localMusicPlayer.setListener(new com.navdy.client.app.framework.util.MusicUtils.Anon4());
            }
        }
        if (event.index != null) {
            localMusicPlayer.play(event.index.intValue());
        } else if (event.shuffleMode == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS) {
            localMusicPlayer.shuffle(event.shuffleMode);
            localMusicPlayer.play(0);
        } else {
            sLogger.e("No event index and not shuffling!!!");
        }
    }

    public static void stopInternalMusicPlayer() {
        if (localMusicPlayer != null) {
            localMusicPlayer.pause(true);
        }
    }

    private static boolean isNewCollection(com.navdy.service.library.events.audio.MusicEvent event) {
        com.navdy.service.library.events.audio.MusicTrackInfo currentTrackInfo = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().getCurrentMediaTrackInfo();
        return (currentTrackInfo.collectionSource == event.collectionSource && currentTrackInfo.collectionType == event.collectionType && com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(currentTrackInfo.collectionId, event.collectionId) && currentTrackInfo.shuffleMode == event.shuffleMode) ? false : true;
    }

    private static void broadcastMusicEvent(com.navdy.service.library.events.audio.MusicEvent event) {
        sLogger.d("broadcastMusicEvent");
        com.navdy.client.app.framework.servicehandler.MusicServiceHandler musicServiceHandler = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance();
        com.navdy.service.library.events.audio.MusicEvent.Action action = event.action;
        com.navdy.client.app.framework.util.MusicUtils.MediaKeyEventRunnable eventRunnable = null;
        if (KEY_CODES_MAPPING.containsKey(action)) {
            eventRunnable = new com.navdy.client.app.framework.util.MusicUtils.MediaKeyEventRunnable(event.dataSource, new android.view.KeyEvent(0, ((java.lang.Integer) KEY_CODES_MAPPING.get(action)).intValue()), true);
        } else if (SEEK_KEY_EVENTS_MAPPING.containsKey(action)) {
            com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper ffAndRewindRunnable = musicServiceHandler.getMusicSeekHelper();
            if (ffAndRewindRunnable != null) {
                ffAndRewindRunnable.executeMusicSeekAction(action);
                return;
            }
            eventRunnable = new com.navdy.client.app.framework.util.MusicUtils.MediaKeyEventRunnable(event.dataSource, (android.view.KeyEvent) SEEK_KEY_EVENTS_MAPPING.get(action), false);
        }
        if (eventRunnable != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(eventRunnable, 1);
        }
    }

    private static void startMediaPlayer(com.navdy.service.library.events.audio.MusicEvent event) {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        android.content.Intent intent = resolveMediaPlayerIntent(context);
        if (intent != null) {
            java.lang.String packageName = intent.getPackage();
            sLogger.i("startMediaPlayer " + packageName);
            intent.addFlags(268435456);
            context.startActivity(intent);
            sendPlaybackIntents(packageName, event);
            return;
        }
        sLogger.w("startMediaPlayer couldn't create an intent to start a media player");
    }

    private static void sendPlaybackIntents(java.lang.String packageName, com.navdy.service.library.events.audio.MusicEvent event) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.util.MusicUtils.Anon5(packageName, event), 10);
    }

    @android.support.annotation.Nullable
    private static android.content.Intent resolveMediaPlayerIntent(android.content.Context context) {
        java.lang.String packageName = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().getLastMusicApp();
        if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(packageName) || !com.navdy.client.app.framework.util.SystemUtils.isPackageInstalled(packageName)) {
            if (com.navdy.client.app.framework.util.SystemUtils.isPackageInstalled(com.navdy.client.app.framework.glances.GlanceConstants.PANDORA)) {
                packageName = com.navdy.client.app.framework.glances.GlanceConstants.PANDORA;
            } else if (com.navdy.client.app.framework.util.SystemUtils.isPackageInstalled(com.navdy.client.app.framework.glances.GlanceConstants.SPOTIFY)) {
                packageName = com.navdy.client.app.framework.glances.GlanceConstants.SPOTIFY;
            } else {
                packageName = com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_MUSIC;
            }
        }
        return context.getPackageManager().getLaunchIntentForPackage(packageName);
    }

    public static synchronized void executeLongPressedKeyUp() {
        synchronized (com.navdy.client.app.framework.util.MusicUtils.class) {
            if (startedContinuousKeyEvent != null) {
                populateAudioManagerFromContext();
                android.view.KeyEvent event = android.view.KeyEvent.changeAction(startedContinuousKeyEvent, 1);
                sLogger.v("Executing specific key event: " + java.lang.String.valueOf(event));
                dispatchMediaKeyEvent(event);
                startedContinuousKeyEvent = null;
            }
        }
    }

    private static void dispatchMediaKeyEvent(android.view.KeyEvent event) {
        sLogger.i("Dispatching key event: " + event);
        audioManager.dispatchMediaKeyEvent(event);
    }

    public static android.graphics.Bitmap getMusicPhoto(java.lang.String songIdStr) {
        sLogger.d("Trying to fetch music artwork for song with ID " + songIdStr);
        android.graphics.Bitmap currentArtwork = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.getInstance().getCurrentMediaArtwork();
        return currentArtwork != null ? currentArtwork : getLocalMusicPhoto(songIdStr);
    }

    public static android.graphics.Bitmap getLocalMusicPhoto(java.lang.String songIdStr) {
        android.graphics.Bitmap bitmap = null;
        sLogger.d("Trying to fetch local music artwork for song with ID " + songIdStr);
        if (songIdStr == null) {
            sLogger.e("Null sent as song ID");
            return bitmap;
        }
        try {
            long songId = java.lang.Long.parseLong(songIdStr);
            android.content.ContentResolver cr = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver();
            try {
                return getArtworkForAlbumWithSong(cr, songId);
            } catch (java.io.FileNotFoundException | java.lang.IllegalArgumentException e) {
                sLogger.e("Cannot get artwork for album for the song with ID " + songIdStr, e);
                sLogger.d("Couldn't get artwork for album, trying song");
                try {
                    return getArtworkForSong(cr, songIdStr);
                } catch (java.lang.NumberFormatException e2) {
                    sLogger.e("Cannot parse received song ID " + songIdStr + " to long ", e2);
                    return bitmap;
                } catch (java.io.FileNotFoundException | java.lang.IllegalArgumentException | java.lang.IllegalStateException e3) {
                    sLogger.e("Cannot get artwork for song with ID " + songIdStr, e3);
                    return bitmap;
                }
            }
        } catch (java.lang.NumberFormatException e4) {
            sLogger.i("Cannot parse long from song's ID: " + songIdStr + " - not local song", e4);
            return bitmap;
        }
    }

    public static boolean isDefaultArtwork(java.lang.String checksum) {
        return java.util.Arrays.asList(DEFAULT_ARTWORK_CHECKSUMS).contains(checksum);
    }

    protected static long getAlbumIdForSong(android.content.ContentResolver cr, long songId) throws java.lang.IllegalArgumentException {
        android.database.Cursor song = null;
        try {
            android.content.ContentResolver contentResolver = cr;
            song = contentResolver.query(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new java.lang.String[]{"_id", "album_id"}, "_id=?", new java.lang.String[]{java.lang.String.valueOf(songId)}, null);
            if (song != null && song.moveToFirst()) {
                return song.getLong(song.getColumnIndexOrThrow("album_id"));
            }
            throw new java.lang.IllegalArgumentException("Song with ID " + songId + " not found in media store");
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(song);
        }
    }

    private static android.graphics.Bitmap getArtworkForAlbumWithSong(android.content.ContentResolver cr, long songId) throws java.io.FileNotFoundException, java.lang.IllegalArgumentException {
        long albumId = getAlbumIdForSong(cr, songId);
        if (albumId < 0) {
            throw new java.lang.IllegalArgumentException("albumId must be positive or 0");
        }
        android.net.Uri uri = android.content.ContentUris.withAppendedId(ALBUM_ART_URI, albumId);
        if (uri == null) {
            throw new java.io.FileNotFoundException("Cannot build URI for album art");
        }
        java.io.InputStream in = cr.openInputStream(uri);
        try {
            return android.graphics.BitmapFactory.decodeStream(in, null, BITMAP_OPTIONS);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(in);
        }
    }

    public static android.graphics.Bitmap getArtworkForAlbum(android.content.ContentResolver cr, long albumId) {
        android.graphics.Bitmap bitmap = null;
        android.net.Uri uri = android.content.ContentUris.withAppendedId(ALBUM_ART_URI, albumId);
        if (uri == null) {
            sLogger.e("Couldn't get URI for album: " + albumId);
        } else {
            java.io.InputStream in = null;
            try {
                in = com.navdy.client.app.NavdyApplication.getAppContext().getContentResolver().openInputStream(uri);
                bitmap = android.graphics.BitmapFactory.decodeStream(in, null, BITMAP_OPTIONS);
            } catch (Throwable e) {
                sLogger.e("Couldn't get album art", e);
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(in);
            }
        }
        return bitmap;
    }

    /* JADX INFO: finally extract failed */
    public static android.graphics.Bitmap getArtworkForArtist(android.content.ContentResolver cr, long artistId) {
        sLogger.d("getArtworkForArtist " + artistId);
        android.database.Cursor songsCursor = null;
        try {
            songsCursor = cr.query(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new java.lang.String[]{"_id"}, "artist_id = ?", new java.lang.String[]{java.lang.String.valueOf(artistId)}, null);
            if (songsCursor != null && songsCursor.moveToFirst()) {
                do {
                    android.graphics.Bitmap bitmap = getLocalMusicPhoto(java.lang.String.valueOf(songsCursor.getInt(0)));
                    if (bitmap != null) {
                        songsCursor.close();
                        com.navdy.service.library.util.IOUtils.closeStream(songsCursor);
                        return bitmap;
                    }
                } while (songsCursor.moveToNext());
                songsCursor.close();
            }
            com.navdy.service.library.util.IOUtils.closeStream(songsCursor);
            return null;
        } catch (Throwable th) {
            com.navdy.service.library.util.IOUtils.closeStream(songsCursor);
            throw th;
        }
    }

    public static android.graphics.Bitmap getArtworkForPlaylist(android.content.ContentResolver cr, long playlistId) {
        android.database.Cursor membersCursor = com.navdy.client.app.framework.util.MusicDbUtils.getPlaylistMembersCursor((int) playlistId);
        if (membersCursor != null) {
            try {
                if (membersCursor.moveToFirst()) {
                    android.graphics.Bitmap bitmap = getLocalMusicPhoto(membersCursor.getString(membersCursor.getColumnIndex("SourceId")));
                    if (bitmap != null) {
                        membersCursor.close();
                        return bitmap;
                    }
                    membersCursor.close();
                }
            } finally {
                com.navdy.service.library.util.IOUtils.closeStream(membersCursor);
            }
        }
        com.navdy.service.library.util.IOUtils.closeStream(membersCursor);
        return null;
    }

    private static android.graphics.Bitmap getArtworkForSong(android.content.ContentResolver cr, java.lang.String songIdStr) throws java.lang.IllegalArgumentException, java.io.FileNotFoundException, java.lang.IllegalStateException {
        sLogger.d("getArtworkForSong " + songIdStr);
        if (java.lang.Long.parseLong(songIdStr) >= 0) {
            return getBitmapFromUri(cr, android.net.Uri.parse(SONG_URI + songIdStr + SONG_ART_URI_POSTFIX));
        }
        throw new java.lang.IllegalArgumentException("songId must be positive or 0");
    }

    private static android.graphics.Bitmap getBitmapFromUri(android.content.ContentResolver cr, android.net.Uri uri) throws java.io.FileNotFoundException {
        sLogger.d("getBitmapFromUri " + uri);
        android.os.ParcelFileDescriptor pfd = cr.openFileDescriptor(uri, "r");
        if (pfd == null) {
            throw new java.io.FileNotFoundException("Cannot open ParcelFileDescriptor for URI: " + uri);
        }
        try {
            return android.graphics.BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor(), null, BITMAP_OPTIONS);
        } finally {
            com.navdy.service.library.util.IOUtils.closeStream(pfd);
        }
    }
}
