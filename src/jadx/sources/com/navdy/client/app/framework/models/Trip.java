package com.navdy.client.app.framework.models;

public class Trip {
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.Trip.class);
    public long arrivedAtDestination;
    public int destinationId;
    public double endLat;
    public double endLong;
    public int endOdometer;
    public long endTime;
    public int id;
    public int offset;
    public double startLat;
    public double startLong;
    public int startOdometer;
    public long startTime;
    public long tripNumber;

    public Trip(int id2, long tripNumber2, long startTime2, int offset2, int startOdometer2, double startLat2, double startLong2, long endTime2, int endOdometer2, double endLat2, double endLong2, long arrivedAtDestination2, int destinationId2) {
        this.id = id2;
        this.tripNumber = tripNumber2;
        this.startTime = startTime2;
        this.offset = offset2;
        this.startOdometer = startOdometer2;
        this.startLat = startLat2;
        this.startLong = startLong2;
        this.endTime = endTime2;
        this.endOdometer = endOdometer2;
        this.endLat = endLat2;
        this.endLong = endLong2;
        this.arrivedAtDestination = arrivedAtDestination2;
        this.destinationId = destinationId2;
    }

    public android.net.Uri saveToDb(android.content.Context context) {
        android.content.ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver != null) {
            android.content.ContentValues tripValues = new android.content.ContentValues();
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_TRIP_NUMBER, java.lang.Long.valueOf(this.tripNumber));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_TIME, java.lang.Long.valueOf(this.startTime));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_TIME_ZONE_N_DST, java.lang.Integer.valueOf(this.offset));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_ODOMETER, java.lang.Integer.valueOf(this.startOdometer));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_LAT, java.lang.Double.valueOf(this.startLat));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_LONG, java.lang.Double.valueOf(this.startLong));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_TIME, java.lang.Long.valueOf(this.endTime));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_ODOMETER, java.lang.Integer.valueOf(this.endOdometer));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_LAT, java.lang.Double.valueOf(this.endLat));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_LONG, java.lang.Double.valueOf(this.endLong));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_ARRIVED_AT_DESTINATION, java.lang.Long.valueOf(this.arrivedAtDestination));
            tripValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_DESTINATION_ID, java.lang.Integer.valueOf(this.destinationId));
            return contentResolver.insert(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI, tripValues);
        }
        logger.e("Unable to get contentResolver !");
        return null;
    }

    public static android.net.Uri saveStartingTripUpdate(android.content.Context context, com.navdy.service.library.events.TripUpdate tripUpdate) {
        logger.v("inserting trip_number: " + tripUpdate.trip_number);
        android.content.ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        android.content.ContentValues contentValues = new android.content.ContentValues();
        int destinationId2 = 0;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(tripUpdate.chosen_destination_id)) {
            destinationId2 = java.lang.Integer.parseInt(tripUpdate.chosen_destination_id);
        }
        int offset2 = com.navdy.client.app.framework.util.SystemUtils.getTimeZoneAndDaylightSavingOffset(tripUpdate.timestamp);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_TRIP_NUMBER, tripUpdate.trip_number);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_TIME, tripUpdate.timestamp);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_TIME_ZONE_N_DST, java.lang.Integer.valueOf(offset2));
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_ODOMETER, tripUpdate.distance_traveled);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_LAT, tripUpdate.current_position.latitude);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_START_LONG, tripUpdate.current_position.longitude);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_DESTINATION_ID, java.lang.Integer.valueOf(destinationId2));
        return contentResolver.insert(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI, contentValues);
    }

    public static int saveLastTripUpdate(android.content.Context context, com.navdy.service.library.events.TripUpdate tripUpdate) {
        int arrivedAtDestination2 = 0;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(tripUpdate.arrived_at_destination_id)) {
            arrivedAtDestination2 = 1;
        }
        android.content.ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return -1;
        }
        android.content.ContentValues contentValues = new android.content.ContentValues();
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_TIME, tripUpdate.timestamp);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_ODOMETER, tripUpdate.distance_traveled);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_LAT, tripUpdate.current_position.latitude);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_END_LONG, tripUpdate.current_position.longitude);
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_ARRIVED_AT_DESTINATION, java.lang.Integer.valueOf(arrivedAtDestination2));
        return contentResolver.update(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI, contentValues, java.lang.String.format("%s=?", new java.lang.Object[]{com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_TRIP_NUMBER}), new java.lang.String[]{java.lang.String.valueOf(tripUpdate.trip_number)});
    }

    public int setDestinationIdAndSaveToDb(int destId) {
        logger.v("updating trip with destination id: " + this.destinationId);
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        this.destinationId = destId;
        android.content.ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return -1;
        }
        android.content.ContentValues contentValues = new android.content.ContentValues();
        contentValues.put(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_DESTINATION_ID, java.lang.Integer.valueOf(this.destinationId));
        return contentResolver.update(com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_CONTENT_URI, contentValues, java.lang.String.format("%s=?", new java.lang.Object[]{com.navdy.client.app.providers.NavdyContentProviderConstants.TRIPS_TRIP_NUMBER}), new java.lang.String[]{java.lang.String.valueOf(this.tripNumber)});
    }

    public java.lang.String toString() {
        return "Trip{id=" + this.id + ",\ttripNumber=" + this.tripNumber + ",\tstartTime=" + this.startTime + ",\toffset=" + this.offset + ",\tstartOdometer=" + this.startOdometer + ",\tstartLat=" + this.startLat + ",\tstartLong=" + this.startLong + ",\tendTime=" + this.endTime + ",\tendOdometer=" + this.endOdometer + ",\tendLat=" + this.endLat + ",\tendLong=" + this.endLong + ",\tarrivedAtDestination=" + this.arrivedAtDestination + ",\tdestinationId=" + this.destinationId + '}';
    }
}
