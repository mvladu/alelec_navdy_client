package com.navdy.client.app.framework.service;

public class TaskRemovalService extends android.app.Service {
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.service.TaskRemovalService.class);
    private boolean startedByAppInstance;

    public int onStartCommand(android.content.Intent intent, int flags, int startId) {
        logger.i("Service started");
        this.startedByAppInstance = true;
        return super.onStartCommand(intent, flags, startId);
    }

    @android.support.annotation.Nullable
    public android.os.IBinder onBind(android.content.Intent intent) {
        return null;
    }

    public void onTaskRemoved(android.content.Intent rootIntent) {
        logger.i("Task removed - started by app: " + this.startedByAppInstance);
        if (this.startedByAppInstance) {
            com.navdy.client.app.framework.map.HereMapsManager.getInstance().killSelfAndMapEngine();
        }
        super.onTaskRemoved(rootIntent);
    }

    public static void startService(android.content.Context context) {
        context.startService(new android.content.Intent(context, com.navdy.client.app.framework.service.TaskRemovalService.class));
    }
}
