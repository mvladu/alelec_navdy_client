package com.navdy.client.app.framework.search;

public class SearchResults {
    private java.util.List<com.navdy.client.app.framework.models.Destination> contactDestinations;
    private java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts;
    private java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinationsFromDatabase;
    private java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleResults;
    private java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> hudSearchResults;
    private java.lang.String query;

    SearchResults() {
    }

    public void setContacts(java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts2) {
        this.contacts = contacts2;
        this.contactDestinations = getDestinationListFromContactResults(contacts2);
    }

    void setGoogleResults(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleResults2) {
        this.googleResults = googleResults2;
    }

    void setHudSearchResults(java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> hudSearchResults2) {
        this.hudSearchResults = hudSearchResults2;
    }

    void setDestinationsFromDatabase(java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinationsFromDatabase2) {
        this.destinationsFromDatabase = destinationsFromDatabase2;
    }

    public void setQuery(java.lang.String query2) {
        this.query = query2;
    }

    public java.util.List<com.navdy.client.app.framework.models.ContactModel> getContacts() {
        return this.contacts;
    }

    private java.util.List<com.navdy.client.app.framework.models.Destination> getContactDestinations() {
        if (this.contactDestinations == null) {
            this.contactDestinations = getDestinationListFromContactResults(this.contacts);
        }
        return this.contactDestinations;
    }

    public java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> getGoogleResults() {
        return this.googleResults;
    }

    public java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getDestinationsFromDatabase() {
        return this.destinationsFromDatabase;
    }

    public java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> getHudSearchResults() {
        return this.hudSearchResults;
    }

    public java.lang.String getQuery() {
        return this.query;
    }

    @android.support.annotation.Nullable
    private java.util.List<com.navdy.client.app.framework.models.Destination> getDestinationListFromContactResults(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.ContactModel> contacts2) {
        if (contacts2 == null || contacts2.isEmpty()) {
            return null;
        }
        java.util.List<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>(contacts2.size());
        for (com.navdy.client.app.framework.models.ContactModel contact : contacts2) {
            if (contact != null) {
                for (com.navdy.client.app.framework.models.Address address : contact.addresses) {
                    java.lang.String fullAddress = com.navdy.client.app.ui.search.SearchRecyclerAdapter.getCleanAddressString(address);
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(fullAddress)) {
                        com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                        destination.name = contact.name;
                        destination.rawAddressNotForDisplay = fullAddress;
                        destination.searchResultType = com.navdy.client.app.framework.models.Destination.SearchType.CONTACT.getValue();
                        destination.contactLookupKey = contact.lookupKey;
                        destination.lastKnownContactId = contact.id;
                        destination.lastContactLookup = contact.lookupTimestamp;
                        destinations.add(destination);
                    }
                }
            }
        }
        return destinations;
    }

    @android.support.annotation.Nullable
    static java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getDestinationListFromHudSearchResults(@android.support.annotation.Nullable java.util.List<com.navdy.service.library.events.places.PlacesSearchResult> placesSearchResults) {
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>();
        if (placesSearchResults != null) {
            for (com.navdy.service.library.events.places.PlacesSearchResult placesSearchResult : placesSearchResults) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                com.navdy.client.app.framework.models.Destination.placesSearchResultToDestinationObject(placesSearchResult, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    @android.support.annotation.Nullable
    public static java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getDestinationListFromGoogleTextSearchResults(@android.support.annotation.Nullable java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> googleTextSearchResults) {
        java.util.ArrayList<com.navdy.client.app.framework.models.Destination> destinations = new java.util.ArrayList<>();
        if (googleTextSearchResults != null) {
            for (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult googleTextSearchDestinationResult : googleTextSearchResults) {
                com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
                googleTextSearchDestinationResult.toModelDestinationObject(com.navdy.client.app.framework.models.Destination.SearchType.TEXT_SEARCH, destination);
                destinations.add(destination);
            }
        }
        return destinations;
    }

    public static boolean isEmpty(com.navdy.client.app.framework.search.SearchResults sr) {
        java.util.List<com.navdy.client.app.framework.models.Destination> contactDestinations2 = sr.getContactDestinations();
        return (contactDestinations2 == null || contactDestinations2.isEmpty()) && (sr.googleResults == null || sr.googleResults.isEmpty()) && ((sr.hudSearchResults == null || sr.hudSearchResults.isEmpty()) && (sr.destinationsFromDatabase == null || sr.destinationsFromDatabase.isEmpty()));
    }

    public java.util.ArrayList<com.navdy.client.app.framework.models.Destination> getDestinationsList() {
        return (java.util.ArrayList) com.navdy.client.app.framework.models.Destination.mergeAndDeduplicateLists(com.navdy.client.app.framework.models.Destination.mergeAndDeduplicateLists(com.navdy.client.app.framework.models.Destination.mergeAndDeduplicateLists(com.navdy.client.app.framework.models.Destination.mergeAndDeduplicateLists(new java.util.ArrayList<>(0), getContactDestinations()), getDestinationListFromGoogleTextSearchResults(this.googleResults)), getDestinationListFromHudSearchResults(this.hudSearchResults)), this.destinationsFromDatabase);
    }

    public com.navdy.client.app.framework.models.Destination getFirstDestination() {
        java.util.List<com.navdy.client.app.framework.models.Destination> destinations = getContactDestinations();
        if (destinations != null) {
            for (com.navdy.client.app.framework.models.Destination contactDestination : destinations) {
                if (contactDestination != null) {
                    return contactDestination;
                }
            }
        }
        java.util.List<com.navdy.client.app.framework.models.Destination> destinations2 = getDestinationListFromGoogleTextSearchResults(this.googleResults);
        if (destinations2 != null) {
            for (com.navdy.client.app.framework.models.Destination contactDestination2 : destinations2) {
                if (contactDestination2 != null) {
                    return contactDestination2;
                }
            }
        }
        java.util.List<com.navdy.client.app.framework.models.Destination> destinations3 = getDestinationListFromHudSearchResults(this.hudSearchResults);
        if (destinations3 != null) {
            for (com.navdy.client.app.framework.models.Destination contactDestination3 : destinations3) {
                if (contactDestination3 != null) {
                    return contactDestination3;
                }
            }
        }
        java.util.List<com.navdy.client.app.framework.models.Destination> destinations4 = this.destinationsFromDatabase;
        if (destinations4 != null) {
            for (com.navdy.client.app.framework.models.Destination contactDestination4 : destinations4) {
                if (contactDestination4 != null) {
                    return contactDestination4;
                }
            }
        }
        return null;
    }
}
