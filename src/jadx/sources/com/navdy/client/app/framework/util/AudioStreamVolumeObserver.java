package com.navdy.client.app.framework.util;

public class AudioStreamVolumeObserver {
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.AudioStreamVolumeObserver.class);
    private com.navdy.client.app.framework.util.AudioStreamVolumeObserver.AudioStreamVolumeContentObserver mAudioStreamVolumeContentObserver;
    private final android.content.Context mContext;

    private static class AudioStreamVolumeContentObserver extends android.database.ContentObserver {
        private final android.media.AudioManager mAudioManager;
        private final int mAudioStreamType;
        private int mLastVolume = this.mAudioManager.getStreamVolume(this.mAudioStreamType);
        private final com.navdy.client.app.framework.util.AudioStreamVolumeObserver.OnAudioStreamVolumeChangedListener mListener;

        public AudioStreamVolumeContentObserver(@android.support.annotation.NonNull android.os.Handler handler, @android.support.annotation.NonNull android.media.AudioManager audioManager, int audioStreamType, @android.support.annotation.NonNull com.navdy.client.app.framework.util.AudioStreamVolumeObserver.OnAudioStreamVolumeChangedListener listener) {
            super(handler);
            this.mAudioManager = audioManager;
            this.mAudioStreamType = audioStreamType;
            this.mListener = listener;
        }

        public void onChange(boolean selfChange) {
            if (this.mAudioManager == null) {
                com.navdy.client.app.framework.util.AudioStreamVolumeObserver.logger.w("Unable to get the audio manager service.");
                return;
            }
            int currentVolume = this.mAudioManager.getStreamVolume(this.mAudioStreamType);
            if (currentVolume != this.mLastVolume) {
                this.mLastVolume = currentVolume;
                if (this.mListener != null) {
                    this.mListener.onAudioStreamVolumeChanged(this.mAudioStreamType, currentVolume);
                }
            }
        }
    }

    public interface OnAudioStreamVolumeChangedListener {
        void onAudioStreamVolumeChanged(int i, int i2);
    }

    public AudioStreamVolumeObserver(@android.support.annotation.NonNull android.content.Context context) {
        this.mContext = context;
    }

    public void start(int audioStreamType, @android.support.annotation.NonNull com.navdy.client.app.framework.util.AudioStreamVolumeObserver.OnAudioStreamVolumeChangedListener listener) {
        stop();
        this.mAudioStreamVolumeContentObserver = new com.navdy.client.app.framework.util.AudioStreamVolumeObserver.AudioStreamVolumeContentObserver(new android.os.Handler(android.os.Looper.getMainLooper()), (android.media.AudioManager) this.mContext.getSystemService("audio"), audioStreamType, listener);
        this.mContext.getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, this.mAudioStreamVolumeContentObserver);
    }

    public void stop() {
        if (this.mAudioStreamVolumeContentObserver != null) {
            this.mContext.getContentResolver().unregisterContentObserver(this.mAudioStreamVolumeContentObserver);
            this.mAudioStreamVolumeContentObserver = null;
        }
    }
}
