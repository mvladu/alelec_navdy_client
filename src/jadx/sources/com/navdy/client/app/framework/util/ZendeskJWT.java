package com.navdy.client.app.framework.util;

public class ZendeskJWT {
    public static final java.lang.String EMAIL = "zendesk-sso@navdy.com";
    public static final java.lang.String NAME = "Navdy";
    public static final java.lang.String SSO_URL = "https://navdy.zendesk.com/access/jwt?jwt=";

    public static android.net.Uri getZendeskUri() {
        com.nimbusds.jose.JWSObject jwsObject = new com.nimbusds.jose.JWSObject(new com.nimbusds.jose.JWSHeader.Builder(com.nimbusds.jose.JWSAlgorithm.HS256).contentType("text/plain").build(), new com.nimbusds.jose.Payload(new com.nimbusds.jwt.JWTClaimsSet.Builder().issueTime(new java.util.Date()).jwtID(java.util.UUID.randomUUID().toString()).claim("name", "Navdy").claim("email", EMAIL).build().toJSONObject()));
        try {
            jwsObject.sign(new com.nimbusds.jose.crypto.MACSigner(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.zendesk_jwt_secret).getBytes()));
        } catch (com.nimbusds.jose.JOSEException e) {
            e.printStackTrace();
        }
        return android.net.Uri.parse(SSO_URL + jwsObject.serialize());
    }
}
