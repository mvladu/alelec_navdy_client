package com.navdy.client.app.framework.util;

public interface CustomItemClickListener {
    void onClick(android.view.View view, int i);
}
