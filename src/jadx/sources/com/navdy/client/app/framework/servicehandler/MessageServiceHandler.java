package com.navdy.client.app.framework.servicehandler;

public class MessageServiceHandler {
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.MessageServiceHandler.class);

    public MessageServiceHandler() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onSmsMessageRequest(com.navdy.service.library.events.messaging.SmsMessageRequest request) {
        try {
            sLogger.v("[message] sending sms");
            android.telephony.SmsManager.getDefault().sendTextMessage(request.number, null, request.message, null, null);
            sLogger.v("[message] sent sms");
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.messaging.SmsMessageResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, request.number, request.name, request.id));
        } catch (Throwable t) {
            sLogger.e(t);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.messaging.SmsMessageResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, request != null ? request.number : null, request != null ? request.name : null, request != null ? request.id : null));
        }
    }
}
