package com.navdy.client.app.framework.util;

public class CrashlyticsAppender implements com.navdy.service.library.log.LogAppender {
    public static final java.lang.String SEPARATOR = ": ";

    public void v(java.lang.String tag, java.lang.String msg) {
        log(tag, msg);
    }

    public void v(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void d(java.lang.String tag, java.lang.String msg) {
        log(tag, msg);
    }

    public void d(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void i(java.lang.String tag, java.lang.String msg) {
        log(tag, msg);
    }

    public void i(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void w(java.lang.String tag, java.lang.String msg) {
        log(tag, msg);
    }

    public void w(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void e(java.lang.String tag, java.lang.String msg) {
        log(tag, msg);
    }

    public void e(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void flush() {
    }

    public void close() {
    }

    private void log(java.lang.String tag, java.lang.String msg) {
        try {
            com.crashlytics.android.Crashlytics.log(tag + SEPARATOR + msg);
        } catch (Throwable th) {
        }
    }

    private void logWithThrowable(java.lang.String tag, java.lang.String msg, java.lang.Throwable tr) {
        try {
            com.crashlytics.android.Crashlytics.log(tag + SEPARATOR + msg);
            logThrowableToCrashlytics(tr);
        } catch (Throwable th) {
        }
    }

    private void logThrowableToCrashlytics(java.lang.Throwable tr) {
        for (java.lang.StackTraceElement stackTraceElement : tr.getStackTrace()) {
            com.crashlytics.android.Crashlytics.log(stackTraceElement.toString());
        }
    }
}
