package com.navdy.client.app.framework.util;

@java.lang.Deprecated
public class CarMdClient {
    public static final java.lang.String ACCESS_IMAGE_URL = "accessImageURL";
    public static final java.lang.String ACCESS_NOTES = "accessNotes";
    private static final okhttp3.HttpUrl CAR_MD_API_URL = okhttp3.HttpUrl.parse("https://api2.carmd.com/v2.0/decode");
    private static final okhttp3.HttpUrl CAR_MD_OBD_API_URL = okhttp3.HttpUrl.parse("http://api2.carmd.com/v2.0/articles/dlclocationbyymm");
    private static final int CONNECTION_TIMEOUT_IN_SECONDS = 30;
    public static final java.lang.String DATA = "data";
    public static final java.lang.String LOCATION_IMAGE_URL = "locationImageURL";
    public static final java.lang.String NOTES = "notes";
    private static com.navdy.client.app.framework.util.CarMdClient instance = null;
    private static com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.CarMdClient.class);
    private final java.lang.String CAR_MD_AUTH;
    private final java.lang.String CAR_MD_TOKEN;
    private okhttp3.OkHttpClient httpClient = null;
    @javax.inject.Inject
    com.navdy.service.library.network.http.IHttpManager mHttpManager = null;

    class Anon1 implements okhttp3.Callback {
        final /* synthetic */ com.navdy.client.app.framework.util.CarMdCallBack val$callback;

        Anon1(com.navdy.client.app.framework.util.CarMdCallBack carMdCallBack) {
            this.val$callback = carMdCallBack;
        }

        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws java.io.IOException {
            try {
                int responseCode = response.code();
                if (responseCode == 200 || responseCode == 201) {
                    okhttp3.ResponseBody responseBody = response.body();
                    if (responseBody.contentLength() > 0) {
                        this.val$callback.processResponse(responseBody);
                        return;
                    }
                    return;
                }
                this.val$callback.processFailure(new java.io.IOException("Car MD request failed with " + responseCode));
            } catch (Throwable t) {
                this.val$callback.processFailure(t);
            }
        }

        public void onFailure(okhttp3.Call call, java.io.IOException e) {
            this.val$callback.processFailure(e);
        }
    }

    public static class CarMdObdLocationResponse {
        public java.lang.String accessImageURL;
        public java.lang.String accessNote;
        public int locationNumber;
        public java.lang.String note;
    }

    private CarMdClient() {
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.httpClient = this.mHttpManager.getClientCopy().retryOnConnectionFailure(true).readTimeout(30, java.util.concurrent.TimeUnit.SECONDS).connectTimeout(30, java.util.concurrent.TimeUnit.SECONDS).build();
        this.CAR_MD_AUTH = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_car_md_auth));
        this.CAR_MD_TOKEN = com.navdy.client.app.framework.util.CredentialsUtils.getCredentials(com.navdy.client.app.NavdyApplication.getAppContext().getString(com.navdy.client.R.string.metadata_car_md_token));
    }

    public static com.navdy.client.app.framework.util.CarMdClient getInstance() {
        if (instance == null) {
            instance = new com.navdy.client.app.framework.util.CarMdClient();
        }
        return instance;
    }

    public void getMakes(com.navdy.client.app.framework.util.CarMdCallBack callback) {
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_API_URL, null));
    }

    public void getYears(java.lang.String make, com.navdy.client.app.framework.util.CarMdCallBack callback) {
        java.util.ArrayList<android.util.Pair<java.lang.String, java.lang.String>> params = new java.util.ArrayList<>(1);
        params.add(new android.util.Pair("make", make));
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_API_URL, params));
    }

    public void getModels(java.lang.String make, java.lang.String year, com.navdy.client.app.framework.util.CarMdCallBack callback) {
        java.util.ArrayList<android.util.Pair<java.lang.String, java.lang.String>> params = new java.util.ArrayList<>(2);
        params.add(new android.util.Pair("make", make));
        params.add(new android.util.Pair("year", year));
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_API_URL, params));
    }

    public java.util.ArrayList<java.lang.String> getListFromJsonResponse(java.lang.String jsonString) throws org.json.JSONException {
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(jsonString)) {
            org.json.JSONArray results = null;
            try {
                results = new org.json.JSONObject(jsonString).getJSONArray(DATA);
            } catch (java.lang.Exception e) {
                logger.e("Unable to parse car info json response: " + jsonString, e);
            }
            if (results != null) {
                java.util.ArrayList<java.lang.String> makeList = new java.util.ArrayList<>(results.length());
                for (int i = 0; i < results.length(); i++) {
                    java.lang.String make = results.optString(i);
                    if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(make)) {
                        makeList.add(make);
                    }
                }
                return makeList;
            }
        }
        return null;
    }

    public void getObdLocation(com.navdy.client.app.framework.util.CarMdCallBack callback, java.lang.String make, java.lang.String year, java.lang.String model) {
        if (callback == null || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(make) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(year) || com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(model)) {
            logger.e(java.lang.String.format("getObdLocation: One of the parameters is null! callback = %s, make = %s year = %s model = %s", new java.lang.Object[]{callback, make, year, model}));
            return;
        }
        java.util.ArrayList<android.util.Pair<java.lang.String, java.lang.String>> params = new java.util.ArrayList<>(3);
        params.add(new android.util.Pair("make", make));
        params.add(new android.util.Pair("year", year));
        params.add(new android.util.Pair("model", model));
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_OBD_API_URL, params));
    }

    public void sendCarMdRequest(com.navdy.client.app.framework.util.CarMdCallBack callback, okhttp3.Request request) {
        if (callback != null) {
            logger.v("Calling car MD: " + request);
            try {
                this.httpClient.newCall(request).enqueue(new com.navdy.client.app.framework.util.CarMdClient.Anon1(callback));
            } catch (java.lang.Exception e) {
                callback.processFailure(e);
            }
        }
    }

    public okhttp3.Request buildCarMdRequest(okhttp3.HttpUrl requestUrl, java.util.ArrayList<android.util.Pair<java.lang.String, java.lang.String>> queryParams) {
        if (queryParams != null) {
            okhttp3.HttpUrl.Builder builder = requestUrl.newBuilder();
            java.util.Iterator it = queryParams.iterator();
            while (it.hasNext()) {
                android.util.Pair<java.lang.String, java.lang.String> param = (android.util.Pair) it.next();
                if (param != null && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim((java.lang.CharSequence) param.first) && !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim((java.lang.CharSequence) param.second)) {
                    builder.addQueryParameter((java.lang.String) param.first, (java.lang.String) param.second);
                }
            }
            requestUrl = builder.build();
        }
        okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url(requestUrl);
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.CAR_MD_AUTH)) {
            requestBuilder.addHeader("authorization", "Basic " + this.CAR_MD_AUTH);
        } else {
            logger.e("Missing car md auth credential !");
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.CAR_MD_TOKEN)) {
            requestBuilder.addHeader("partner-token", this.CAR_MD_TOKEN);
        } else {
            logger.e("Missing car md token credential !");
        }
        return requestBuilder.build();
    }

    public com.navdy.client.app.framework.util.CarMdClient.CarMdObdLocationResponse parseCarMdObdLocationResponse(okhttp3.ResponseBody responseBody) throws java.lang.Exception {
        com.navdy.client.app.framework.util.CarMdClient.CarMdObdLocationResponse response = new com.navdy.client.app.framework.util.CarMdClient.CarMdObdLocationResponse();
        java.lang.String jsonString = responseBody.string();
        logger.v("Car MD: response: " + jsonString);
        org.json.JSONObject data = new org.json.JSONObject(jsonString).getJSONObject(DATA);
        response.accessImageURL = data.getString(ACCESS_IMAGE_URL);
        java.lang.String locationImageURL = data.getString(LOCATION_IMAGE_URL);
        java.lang.String notes = data.getString(NOTES);
        java.lang.String accessNotes = data.getString(ACCESS_NOTES);
        response.note = notes;
        response.accessNote = accessNotes;
        response.locationNumber = -1;
        if (locationImageURL.contains("dlc/position/1.jpg")) {
            response.locationNumber = 0;
        } else if (locationImageURL.contains("dlc/position/2.jpg")) {
            response.locationNumber = 1;
        } else if (locationImageURL.contains("dlc/position/3.jpg")) {
            response.locationNumber = 2;
        } else if (locationImageURL.contains("dlc/position/4.jpg")) {
            response.locationNumber = 3;
        } else if (locationImageURL.contains("dlc/position/5.jpg")) {
            response.locationNumber = 4;
        } else if (locationImageURL.contains("dlc/position/6.jpg")) {
            response.locationNumber = 5;
        } else if (locationImageURL.contains("dlc/position/7.jpg")) {
            response.locationNumber = 6;
        } else if (locationImageURL.contains("dlc/position/8.jpg")) {
            response.locationNumber = 7;
        } else if (locationImageURL.contains("dlc/position/9.jpg")) {
            response.locationNumber = 8;
        }
        if (response.locationNumber >= 0 && response.locationNumber <= 8) {
            return response;
        }
        throw new java.lang.Exception("Missing Obd Location. locationImageURL = " + locationImageURL);
    }
}
