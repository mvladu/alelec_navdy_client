package com.navdy.client.app.framework.navigation;

class NavigationHelper {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.navigation.NavigationHelper.class);
    private final com.navdy.client.debug.navigation.NavigationManager hudNavigationManager = new com.navdy.client.debug.navigation.HUDNavigationManager();

    NavigationHelper() {
    }

    synchronized void navigateToDestination(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination, java.lang.String requestId, boolean initiatedOnHud) {
        logger.v("navigateToDestination: " + destination.rawAddressNotForDisplay);
        boolean geocodeStreetAddressOnHud = !destination.hasOneValidSetOfCoordinates();
        if (geocodeStreetAddressOnHud) {
            logger.i("No valid coordinates found. Requesting for HUD to geocode the street address for Navigation: " + destination.rawAddressNotForDisplay);
        }
        searchRoutesOnHud(requestId, destination.getNameForDisplay(), destination.rawAddressNotForDisplay, new com.google.android.gms.maps.model.LatLng(destination.navigationLat, destination.navigationLong), java.lang.String.valueOf(destination.id), destination.getFavoriteTypeForProto(), com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(destination.displayLat, destination.displayLong), geocodeStreetAddressOnHud, initiatedOnHud, destination.toProtobufDestinationObject());
    }

    void cancelRouteCalculation(@android.support.annotation.NonNull java.lang.String cancelHandle) {
        if (com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.navigation.NavigationRouteCancelRequest(cancelHandle))) {
            logger.v("cancelRouteCalculation sent successfully.");
        } else {
            logger.w("cancelRouteCalculation failed to send.");
        }
    }

    void cancelRoute(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED, route);
    }

    void getNavigationSessionState() {
        if (com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.navigation.GetNavigationSessionState())) {
            logger.v("getNavigationSessionState sent successfully");
        } else {
            logger.w("getNavigationSessionState failed to send");
        }
    }

    private void searchRoutesOnHud(java.lang.String requestId, java.lang.String name, java.lang.String address, com.google.android.gms.maps.model.LatLng navigationCoordinate, java.lang.String destinationId, com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType, com.navdy.service.library.events.location.Coordinate display, boolean geocodeAddressOnHud, boolean initiatedOnHud, com.navdy.service.library.events.destination.Destination destination) {
        com.navdy.service.library.events.location.Coordinate coordinate = new com.navdy.service.library.events.location.Coordinate(java.lang.Double.valueOf(navigationCoordinate.latitude), java.lang.Double.valueOf(navigationCoordinate.longitude), java.lang.Float.valueOf(0.0f), java.lang.Double.valueOf(0.0d), java.lang.Float.valueOf(0.0f), java.lang.Float.valueOf(0.0f), java.lang.Long.valueOf(0), "Google");
        logger.v("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        logger.v("Requesting new route for " + destination);
        logger.v("sending route to HUD:" + name + " address: " + address + " destinationId: " + destinationId + " favoriteType:" + favoriteType + " coordinate:" + coordinate + " display: " + display + " streetAddress:" + address + " useStreetAddress: " + geocodeAddressOnHud + " initiatedOnHud: " + initiatedOnHud);
        logger.v("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        this.hudNavigationManager.startRouteRequest(coordinate, name, null, address, destinationId, favoriteType, display, requestId, geocodeAddressOnHud, initiatedOnHud, destination);
    }

    private void sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState newState, com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
        sendStateChangeRequest(newState, route, 0);
    }

    private void sendStateChangeRequest(com.navdy.service.library.events.navigation.NavigationSessionState newState, com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route, int simulationSpeed) {
        logger.d("Attempting to change state: " + newState);
        if (newState == null) {
            logger.e("sendStateChangeRequest failed because new NavigationSessionState is null");
        } else if (!com.navdy.client.app.framework.DeviceConnection.isConnected()) {
            logger.v("Device is no longer valid. Creating new Device Connection");
        } else if (route == null) {
            logger.w("Didn't get any route as chosen");
        } else {
            if (com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.navigation.NavigationSessionRequest(newState, route.getDestination().name, route.routeId, java.lang.Integer.valueOf(simulationSpeed), java.lang.Boolean.valueOf(false)))) {
                logger.v("State change request sent successfully.");
            } else {
                logger.w("State change request failed to send.");
            }
        }
    }
}
