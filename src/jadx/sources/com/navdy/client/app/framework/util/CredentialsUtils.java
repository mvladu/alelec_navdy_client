package com.navdy.client.app.framework.util;

public class CredentialsUtils {
    private static final boolean VERBOSE = false;
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.CredentialsUtils.class);
    private android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();

    public static java.lang.String getCredentials(java.lang.String metaName) {
        java.lang.String credentials = "";
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(metaName, "com.google.android.geo.API_KEY")) {
            java.lang.String googleMapsApiKey = sharedPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.GOOGLE_MAPS_API_KEY, null);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(googleMapsApiKey)) {
                return googleMapsApiKey;
            }
        }
        if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(metaName, "GOOGLE_WEB_SERVICE_KEY")) {
            java.lang.String googleServicesApiKey = sharedPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.GOOGLE_SERVICES_API_KEY, null);
            if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(googleServicesApiKey)) {
                return googleServicesApiKey;
            }
        }
        try {
            credentials = com.navdy.client.app.NavdyApplication.getAppContext().getPackageManager().getApplicationInfo(com.navdy.client.app.NavdyApplication.getAppContext().getPackageName(), 128).metaData.getString(metaName);
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            logger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (java.lang.NullPointerException e2) {
            logger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
        }
        return credentials;
    }
}
