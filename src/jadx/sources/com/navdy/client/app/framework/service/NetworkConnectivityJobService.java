package com.navdy.client.app.framework.service;

public class NetworkConnectivityJobService extends android.app.job.JobService {
    public static final int NETWORK_SERVICE = 100;
    public static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.service.NetworkConnectivityJobService.class);

    public boolean onStartJob(android.app.job.JobParameters jobParameters) {
        logger.d("onStartJob");
        com.navdy.client.app.framework.util.SupportTicketService.startSupportTicketService(com.navdy.client.app.NavdyApplication.getAppContext());
        return false;
    }

    public boolean onStopJob(android.app.job.JobParameters jobParameters) {
        boolean pendingTicketsExist = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.PENDING_TICKETS_EXIST, false);
        logger.d("onStopJob - JobService will reschedule: " + pendingTicketsExist);
        return pendingTicketsExist;
    }

    public static void scheduleNetworkServiceForNougatOrAbove() {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
            android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService(android.app.job.JobScheduler.class);
            if (jobScheduler == null || jobScheduler.getPendingJob(100) == null) {
                logger.d("Schedule network connectivity job service");
                android.app.job.JobInfo job = new android.app.job.JobInfo.Builder(100, new android.content.ComponentName(context, com.navdy.client.app.framework.service.NetworkConnectivityJobService.class)).setRequiredNetworkType(1).build();
                if (jobScheduler != null) {
                    jobScheduler.schedule(job);
                    return;
                }
                return;
            }
            logger.d("NetworkConnectivityJobService already pending");
        }
    }
}
