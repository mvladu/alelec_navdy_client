package com.navdy.client.app.framework.util;

public abstract class CarMdCallBack {
    public abstract void processFailure(java.lang.Throwable th);

    public abstract void processResponse(okhttp3.ResponseBody responseBody);
}
