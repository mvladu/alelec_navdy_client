package com.navdy.client.app.framework.util;

public class BitmapCache extends android.util.LruCache<java.lang.String, android.graphics.Bitmap> {
    public BitmapCache(int maxSize) {
        super(maxSize);
    }

    /* access modifiers changed from: protected */
    public int sizeOf(java.lang.String key, android.graphics.Bitmap value) {
        return com.navdy.client.app.framework.util.ImageCache.getBitmapSize(value);
    }
}
