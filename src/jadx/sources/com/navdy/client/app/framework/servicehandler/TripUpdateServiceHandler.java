package com.navdy.client.app.framework.servicehandler;

public class TripUpdateServiceHandler {
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler.class);
    private static com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler singleton = null;

    class Anon1 implements java.lang.Runnable {
        final /* synthetic */ com.navdy.service.library.events.TripUpdate val$tripUpdate;

        Anon1(com.navdy.service.library.events.TripUpdate tripUpdate) {
            this.val$tripUpdate = tripUpdate;
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler.this.handleTripUpdate(this.val$tripUpdate);
        }
    }

    private TripUpdateServiceHandler() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    public static com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler getInstance() {
        if (singleton == null) {
            singleton = new com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler();
        }
        return singleton;
    }

    @com.squareup.otto.Subscribe
    public void onTripUpdate(com.navdy.service.library.events.TripUpdate tripUpdate) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler.Anon1(tripUpdate), 1);
    }

    private void handleTripUpdate(com.navdy.service.library.events.TripUpdate tripUpdate) {
        if (tripUpdate != null) {
            if (tripUpdate.current_position != null) {
                com.navdy.client.app.framework.util.BusProvider.getInstance().post(new com.navdy.client.app.framework.location.NavdyLocationManager.CarLocationChangedEvent(com.navdy.client.app.framework.map.MapUtils.buildNewCoordinate(tripUpdate.current_position.latitude.doubleValue(), tripUpdate.current_position.longitude.doubleValue())));
            }
            if (tripUpdate.trip_number != null) {
                java.lang.String tripNumber = tripUpdate.trip_number.toString();
                logger.v("TripUpdate received, trip number: " + tripNumber);
                android.content.Context appContext = com.navdy.client.app.NavdyApplication.getAppContext();
                try {
                    if (com.navdy.client.app.providers.NavdyContentProvider.getThisTrip(tripNumber) == null) {
                        logger.v("The following trip was inserted: " + com.navdy.client.app.framework.models.Trip.saveStartingTripUpdate(appContext, tripUpdate));
                        return;
                    }
                    logger.v("Updated this many trips: " + com.navdy.client.app.framework.models.Trip.saveLastTripUpdate(appContext, tripUpdate));
                } catch (java.lang.Exception e) {
                    logger.e("Something went wrong while trying to save trip info: ", e);
                }
            }
        }
    }
}
