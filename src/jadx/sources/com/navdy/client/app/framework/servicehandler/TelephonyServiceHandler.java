package com.navdy.client.app.framework.servicehandler;

public class TelephonyServiceHandler {
    private static final int TURN_SPEAKER_ON_DELAY = 3000;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.class);
    private int currentServiceState = 1;
    private android.content.Context mContext;
    private com.navdy.client.app.framework.callcontrol.TelephonyInterface mTelephony;
    private android.telephony.PhoneStateListener phoneStateListener = new com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.Anon1();

    class Anon1 extends android.telephony.PhoneStateListener {
        Anon1() {
        }

        public void onServiceStateChanged(android.telephony.ServiceState serviceState) {
            com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.this.currentServiceState = serviceState.getState();
            com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.sLogger.v("service state changed:" + com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.this.currentServiceState);
        }
    }

    class Anon2 extends java.util.TimerTask {
        Anon2() {
        }

        public void run() {
            com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.sLogger.d("HFP is not connected, turning on the speaker phone");
            android.media.AudioManager mAudioManager = (android.media.AudioManager) com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.this.mContext.getSystemService("audio");
            mAudioManager.setMode(2);
            mAudioManager.setSpeakerphoneOn(true);
        }
    }

    public TelephonyServiceHandler(android.content.Context context) {
        this.mContext = context;
        this.mTelephony = com.navdy.client.app.framework.callcontrol.TelephonyInterfaceFactory.getTelephonyInterface(context);
        sLogger.v("Telephony model[" + android.os.Build.MODEL + "] manufacturer[" + android.os.Build.MANUFACTURER + "] device[" + android.os.Build.DEVICE + "]");
        sLogger.v("Telephony controller is :" + this.mTelephony.getClass().getName());
        ((android.telephony.TelephonyManager) context.getSystemService("phone")).listen(this.phoneStateListener, 1);
        sLogger.v("registeted phone state listener");
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent event) {
        ((android.telephony.TelephonyManager) this.mContext.getSystemService("phone")).listen(this.phoneStateListener, 0);
    }

    @com.squareup.otto.Subscribe
    public void onTelephonyRequest(com.navdy.service.library.events.callcontrol.TelephonyRequest telephonyRequest) {
        switch (telephonyRequest.action) {
            case CALL_ACCEPT:
                this.mTelephony.acceptRingingCall();
                turnOnSpeakerPhoneIfNeeded();
                break;
            case CALL_REJECT:
                this.mTelephony.rejectRingingCall();
                break;
            case CALL_END:
                this.mTelephony.endCall();
                break;
            case CALL_DIAL:
                if (this.currentServiceState == 0) {
                    java.lang.String uri = "tel:" + telephonyRequest.number.trim();
                    android.content.Intent intent = new android.content.Intent("android.intent.action.CALL");
                    intent.setFlags(402653184);
                    intent.setData(android.net.Uri.parse(uri));
                    if (android.support.v4.app.ActivityCompat.checkSelfPermission(com.navdy.client.app.NavdyApplication.getAppContext(), "android.permission.CALL_PHONE") != 0) {
                        sLogger.e("Missing CALL_PHONE permission !");
                        break;
                    } else {
                        this.mContext.startActivity(intent);
                        turnOnSpeakerPhoneIfNeeded();
                        break;
                    }
                } else {
                    com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.callcontrol.PhoneEvent(com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DISCONNECTING, telephonyRequest.number, null, null, null));
                    com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.callcontrol.TelephonyResponse(telephonyRequest.action, com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, null));
                    return;
                }
            case CALL_MUTE:
            case CALL_UNMUTE:
                this.mTelephony.toggleMute();
                break;
            default:
                sLogger.e("Unknown TelephonyServiceHandler request action: " + telephonyRequest.action);
                break;
        }
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.callcontrol.TelephonyResponse(telephonyRequest.action, com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, null));
    }

    @com.squareup.otto.Subscribe
    public void onPhoneStatusRequest(com.navdy.service.library.events.callcontrol.PhoneStatusRequest phoneStatusRequest) {
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.callcontrol.PhoneStatusResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, com.navdy.client.app.framework.receiver.PhoneCallReceiver.getCurrentStatus()));
    }

    private void turnOnSpeakerPhoneIfNeeded() {
        if (!isHFPSpeakerConnected()) {
            new java.util.Timer().schedule(new com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.Anon2(), com.here.odnp.config.OdnpConfigStatic.CELL_NO_CHANGE_LIMITER_TIME);
        }
    }

    private boolean isHFPSpeakerConnected() {
        return android.bluetooth.BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(1) == 2;
    }
}
