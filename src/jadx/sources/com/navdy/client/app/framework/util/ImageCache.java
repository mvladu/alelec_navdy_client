package com.navdy.client.app.framework.util;

public class ImageCache {
    private static final int DEFAULT_CACHE_SIZE = 20971520;
    private static final int DEFAULT_IMAGE_MAX_SIZE = 4194304;
    private static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.ImageCache.class);
    private final com.navdy.client.app.framework.util.BitmapCache cache;
    private final int cacheCriteriaSize;

    public ImageCache() {
        this(DEFAULT_CACHE_SIZE, 4194304);
    }

    public ImageCache(int cacheSize, int cacheCriteriaSize2) {
        this.cache = new com.navdy.client.app.framework.util.BitmapCache(cacheSize);
        this.cacheCriteriaSize = cacheCriteriaSize2;
    }

    public android.graphics.Bitmap getBitmap(java.lang.String key) {
        if (key == null) {
            return null;
        }
        android.graphics.Bitmap bitmap = (android.graphics.Bitmap) this.cache.get(key);
        if (bitmap == null) {
            return bitmap;
        }
        sLogger.v("cache hit [" + key + "] size[" + getBitmapSize(bitmap) + "]");
        return bitmap;
    }

    public void putBitmap(java.lang.String key, android.graphics.Bitmap bitmap) {
        if (key != null && bitmap != null) {
            int size = getBitmapSize(bitmap);
            if (size >= this.cacheCriteriaSize) {
                sLogger.e("cache image size [" + size + "] > large [" + this.cacheCriteriaSize + "]");
                return;
            }
            this.cache.put(key, bitmap);
            sLogger.v("cache put [" + key + "] size[" + getBitmapSize(bitmap) + "]");
        }
    }

    public void clearCache() {
        this.cache.evictAll();
        java.lang.System.gc();
        sLogger.v("clearCache-explicit gc");
    }

    public static int getBitmapSize(android.graphics.Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getAllocationByteCount();
    }
}
