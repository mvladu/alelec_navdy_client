package com.navdy.client.app.framework.glances;

public class EmailNotificationHandler {
    private static com.navdy.service.library.log.Logger sLogger = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger;

    public static void handleEmailNotification(android.service.notification.StatusBarNotification sbn) {
        java.lang.String packageName = sbn.getPackageName();
        if (!com.navdy.client.app.ui.glances.GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName) || !com.navdy.client.app.framework.glances.GlanceConstants.isPackageInGroup(packageName, com.navdy.client.app.framework.glances.GlanceConstants.Group.EMAIL_GROUP)) {
            sLogger.w("email notification not handled [" + packageName + "]");
            return;
        }
        android.app.Notification notification = sbn.getNotification();
        android.os.Bundle extras = android.support.v7.app.NotificationCompat.getExtras(notification);
        java.lang.String senderOrTitle = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.title");
        java.lang.String toEmail = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.subText");
        java.lang.String subject = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        java.lang.String body = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.bigText");
        java.lang.Object textLines = extras.get("android.textLines");
        if (!packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.GOOGLE_MAIL) && textLines != null && (textLines instanceof java.lang.CharSequence[])) {
            java.lang.CharSequence[] lines = (java.lang.CharSequence[]) textLines;
            java.lang.StringBuilder newBody = new java.lang.StringBuilder();
            for (int i = 0; i < lines.length; i++) {
                newBody.append(lines[i]);
                if (i + 1 < lines.length) {
                    newBody.append("\n");
                }
            }
            if (newBody.length() > 0) {
                subject = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.getStringSafely(extras, "android.summaryText");
                body = newBody.toString();
            }
        } else if (!(body == null || subject == null)) {
            if (packageName.equals(com.navdy.client.app.framework.glances.GlanceConstants.MICROSOFT_OUTLOOK)) {
                body = body.substring(body.indexOf("\n") + 1);
                if (subject.contains(body)) {
                    subject = subject.substring(0, subject.indexOf(body));
                }
            } else if (body.startsWith(subject)) {
                int index = body.indexOf(subject + "\n");
                if (index != -1) {
                    body = body.substring(subject.length() + index + 1);
                }
            }
        }
        if (body == null && subject != null) {
            int index2 = subject.indexOf("\n");
            if (index2 != -1) {
                body = subject.substring(index2 + 1).trim();
                subject = subject.substring(0, index2).trim();
            }
        }
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        java.lang.String peopleStr = null;
        java.lang.String[] people = (java.lang.String[]) extras.get("android.people");
        if (people != null) {
            for (int i2 = 0; i2 < people.length; i2++) {
                java.lang.String p = people[i2];
                if (p != null && p.startsWith(com.navdy.client.app.framework.glances.GlanceConstants.MAIL_TO)) {
                    p = p.substring(com.navdy.client.app.framework.glances.GlanceConstants.MAIL_TO.length());
                }
                if (i2 != 0) {
                    builder.append(",");
                }
                builder.append(p);
            }
            peopleStr = builder.toString();
        }
        if (peopleStr == null && senderOrTitle != null) {
            peopleStr = senderOrTitle;
        }
        sLogger.v("[navdyinfo-gmail] senderOrTitle[" + senderOrTitle + "] from[" + peopleStr + "] to[" + toEmail + "] subject[" + subject + "] body[" + body + "]");
        if ((subject == null && body == null) || peopleStr == null) {
            sLogger.v("[navdyinfo-gmail] invalid data");
            return;
        }
        java.lang.String id = com.navdy.client.app.framework.glances.GlancesHelper.getId();
        java.util.List<com.navdy.service.library.events.glances.KeyValue> data = new java.util.ArrayList<>();
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(peopleStr)) {
            if (peopleStr.contains(com.navdy.client.app.framework.glances.GlanceConstants.EMAIL_AT)) {
                com.navdy.service.library.events.glances.KeyValue keyValue = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), senderOrTitle);
                data.add(keyValue);
                com.navdy.service.library.events.glances.KeyValue keyValue2 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_EMAIL.name(), peopleStr);
                data.add(keyValue2);
            } else {
                com.navdy.service.library.events.glances.KeyValue keyValue3 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_FROM_NAME.name(), peopleStr);
                data.add(keyValue3);
            }
        }
        if (toEmail != null) {
            if (toEmail.contains(com.navdy.client.app.framework.glances.GlanceConstants.EMAIL_AT)) {
                com.navdy.service.library.events.glances.KeyValue keyValue4 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_TO_EMAIL.name(), toEmail);
                data.add(keyValue4);
            } else {
                com.navdy.service.library.events.glances.KeyValue keyValue5 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_TO_NAME.name(), toEmail);
                data.add(keyValue5);
            }
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(subject)) {
            com.navdy.service.library.events.glances.KeyValue keyValue6 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name(), subject);
            data.add(keyValue6);
        }
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(body)) {
            com.navdy.service.library.events.glances.KeyValue keyValue7 = new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name(), body);
            data.add(keyValue7);
        }
        com.navdy.client.app.framework.glances.GlancesHelper.sendEvent(new com.navdy.service.library.events.glances.GlanceEvent.Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(packageName).id(id).postTime(java.lang.Long.valueOf(notification.when)).glanceData(data).build());
    }
}
