package com.navdy.client.app.framework.models;

public class CalendarEvent {
    public boolean allDay = false;
    public long calendarId = 0;
    public com.navdy.service.library.events.destination.Destination destination;
    public long displayColor = 0;
    public java.lang.String displayName = "";
    public long endTimestamp = 0;
    public com.google.android.gms.maps.model.LatLng latLng;
    public java.lang.String location = "";
    public long startTimestamp = 0;

    public CalendarEvent(java.lang.String displayName2, long calendarId2, long startTimestamp2, long endTimestamp2, java.lang.String location2, long displayColor2, boolean allDay2) {
        this.displayName = displayName2;
        this.calendarId = calendarId2;
        this.startTimestamp = startTimestamp2;
        this.location = location2;
        this.endTimestamp = endTimestamp2;
        this.displayColor = displayColor2;
        this.allDay = allDay2;
    }

    public java.lang.String toString() {
        return java.lang.String.format("display_name: %s, \tcalendarId: %s\tlocation: %s\tstart_time: %s\tend_time: %s\tdisplay_color: %s\tall_day: %s", new java.lang.Object[]{this.displayName, java.lang.Long.valueOf(this.calendarId), this.location, java.lang.Long.valueOf(this.startTimestamp), java.lang.Long.valueOf(this.endTimestamp), java.lang.Long.valueOf(this.displayColor), java.lang.Boolean.valueOf(this.allDay)});
    }

    public com.navdy.service.library.events.calendars.CalendarEvent toProtobufObject() {
        return new com.navdy.service.library.events.calendars.CalendarEvent(this.displayName, java.lang.Long.valueOf(this.startTimestamp), java.lang.Long.valueOf(this.endTimestamp), java.lang.Boolean.valueOf(this.allDay), java.lang.Integer.valueOf((int) this.displayColor), this.location, java.lang.String.valueOf(this.calendarId), this.destination);
    }

    public void setDestination(com.navdy.service.library.events.destination.Destination destination2) {
        this.destination = destination2;
    }

    public java.lang.String getDateAndTime() {
        return com.navdy.client.app.ui.homescreen.CalendarUtils.getDateAndTime(this.startTimestamp);
    }

    public java.lang.String getStartTime() {
        return com.navdy.client.app.ui.homescreen.CalendarUtils.getTime(this.startTimestamp);
    }

    public java.lang.String getEndTime() {
        return com.navdy.client.app.ui.homescreen.CalendarUtils.getTime(this.endTimestamp);
    }
}
