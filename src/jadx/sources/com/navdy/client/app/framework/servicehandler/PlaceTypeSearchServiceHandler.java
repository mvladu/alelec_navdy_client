package com.navdy.client.app.framework.servicehandler;

public class PlaceTypeSearchServiceHandler {
    private static final java.lang.String GOOGLE_PLACES_ID_HEADER = "google-places-id:";
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.class);

    class Anon1 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
        final /* synthetic */ com.navdy.service.library.events.places.DestinationSelectedRequest val$destinationSelectedRequest;

        Anon1(com.navdy.service.library.events.places.DestinationSelectedRequest destinationSelectedRequest) {
            this.val$destinationSelectedRequest = destinationSelectedRequest;
        }

        public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
            reply(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, destination);
        }

        public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
            reply(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, destination);
        }

        private void reply(com.navdy.service.library.events.RequestStatus status, com.navdy.client.app.framework.models.Destination destination) {
            com.navdy.service.library.events.places.DestinationSelectedResponse destinationSelectedResponse = new com.navdy.service.library.events.places.DestinationSelectedResponse.Builder().request_id(this.val$destinationSelectedRequest.request_id).request_status(status).destination(com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.this.EnsurePhoneNumbersAreNotLost(this.val$destinationSelectedRequest.destination, destination.toProtobufDestinationObject())).build();
            com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.printDestinationSelectedResponse(destinationSelectedResponse);
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) destinationSelectedResponse);
        }
    }

    private static class PlaceTypeSearchListener implements com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener {
        private static final int MAX_RESULTS = 7;
        private static final int MIN_ACCEPTABLE_RESULTS = 5;
        private static final int RETRY_RADIUS = 16093;
        private final android.os.Handler handler;
        private boolean hasRetried;
        private final java.lang.String requestId;
        private final com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceType;

        /* synthetic */ PlaceTypeSearchListener(java.lang.String x0, com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes x1, com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.Anon1 x2) {
            this(x0, x1);
        }

        private PlaceTypeSearchListener(java.lang.String requestId2, com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceType2) {
            this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
            this.requestId = requestId2;
            this.serviceType = serviceType2;
            this.hasRetried = false;
        }

        public void onGoogleSearchResult(java.util.List<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinations, com.navdy.client.app.framework.search.GooglePlacesSearch.Query queryType, com.navdy.client.app.framework.search.GooglePlacesSearch.Error error) {
            if (error == com.navdy.client.app.framework.search.GooglePlacesSearch.Error.SERVICE_ERROR) {
                postFailure();
            } else if ((destinations == null || destinations.size() < 5) && !this.hasRetried) {
                retryWithBiggerRadius();
            } else if (destinations == null || destinations.size() == 0) {
                postFailure();
            } else {
                java.util.List<com.navdy.service.library.events.destination.Destination> outboundList = new java.util.ArrayList<>();
                java.util.Iterator<com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult> destinationsIterator = destinations.iterator();
                while (destinationsIterator.hasNext() && outboundList.size() < 7) {
                    com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult destination = (com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult) destinationsIterator.next();
                    outboundList.add(new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(0.0d), java.lang.Double.valueOf(0.0d))).display_position(new com.navdy.service.library.events.location.LatLong(java.lang.Double.valueOf(java.lang.Double.parseDouble(destination.lat)), java.lang.Double.valueOf(java.lang.Double.parseDouble(destination.lng)))).destination_title(destination.name).destination_subtitle((destination.streetNumber == null || destination.streetName == null) ? destination.address : destination.streetNumber + " " + destination.streetName).favorite_type(com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE).full_address(destination.address).identifier(java.util.UUID.randomUUID().toString()).place_type(getPlaceTypeForGoogleServiceType(this.serviceType)).place_id("google-places-id:" + destination.place_id).build());
                }
                com.navdy.service.library.events.places.PlaceTypeSearchResponse placeTypeSearchResponse = new com.navdy.service.library.events.places.PlaceTypeSearchResponse.Builder().request_id(this.requestId).request_status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).destinations(outboundList).build();
                com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.printPlaceTypeSearchResponse(placeTypeSearchResponse);
                com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) placeTypeSearchResponse);
            }
        }

        void runSearch() {
            com.navdy.client.app.framework.search.NavdySearch.runServiceSearch(new com.navdy.client.app.framework.search.GooglePlacesSearch(this, this.handler), this.serviceType);
        }

        private void postFailure() {
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.places.PlaceTypeSearchResponse.Builder().destinations(new java.util.ArrayList()).request_id(this.requestId).request_status(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE).build());
        }

        private void retryWithBiggerRadius() {
            com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.logger.v("retrying with a 10 mile radius since results count is less than 5");
            this.hasRetried = true;
            new com.navdy.client.app.framework.search.GooglePlacesSearch(this, this.handler).runServiceSearchWebApi(this.serviceType, RETRY_RADIUS);
        }

        private com.navdy.service.library.events.places.PlaceType getPlaceTypeForGoogleServiceType(com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceType2) {
            switch (serviceType2) {
                case GAS:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS;
                case PARKING:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARKING;
                case FOOD:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_RESTAURANT;
                case COFFEE:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_COFFEE;
                case ATM:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ATM;
                case HOSPITAL:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_HOSPITAL;
                case STORE:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_STORE;
                default:
                    return com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
            }
        }
    }

    public PlaceTypeSearchServiceHandler() {
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    private static void printPlaceTypeSearchResponse(com.navdy.service.library.events.places.PlaceTypeSearchResponse placeTypeSearchResponse) {
        if (placeTypeSearchResponse == null) {
            logger.e("printPlaceTypeSearchResponse, placeTypeSearchResponse is null!");
        } else {
            logger.d("placeTypeSearchResponse: " + placeTypeSearchResponse);
        }
    }

    private static void printDestinationSelectedResponse(com.navdy.service.library.events.places.DestinationSelectedResponse destinationSelectedResponse) {
        if (destinationSelectedResponse == null) {
            logger.e("printDestinationSelectedResponse, destinationSelectedResponse is null!");
        } else {
            logger.d("destinationSelectedResponse: " + destinationSelectedResponse);
        }
    }

    @com.squareup.otto.Subscribe
    public void onPlaceTypeSearch(@android.support.annotation.Nullable com.navdy.service.library.events.places.PlaceTypeSearchRequest placeTypeSearchRequest) {
        if (placeTypeSearchRequest == null) {
            logger.w("event is null on handlePlaceTypeSearch");
            return;
        }
        com.navdy.service.library.events.places.PlaceType placeType = placeTypeSearchRequest.place_type;
        logger.i("received new PlaceTypeSearchRequest for " + placeType);
        com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceType = getGoogleServiceTypeForPlaceType(placeType);
        if (serviceType != null) {
            performSearch(placeTypeSearchRequest.request_id, serviceType);
            return;
        }
        logger.e("invalid PlaceType, returning REQUEST_SERVICE_ERROR to HUD");
        com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.places.PlaceTypeSearchResponse.Builder().request_id(placeTypeSearchRequest.request_id).destinations(new java.util.ArrayList()).request_status(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR).build());
    }

    @com.squareup.otto.Subscribe
    public void onDestinationSelectedRequest(@android.support.annotation.Nullable com.navdy.service.library.events.places.DestinationSelectedRequest destinationSelectedRequest) {
        if (destinationSelectedRequest == null || destinationSelectedRequest.destination == null) {
            logger.w("event or its destination is null on handleDestinationSelectedRequest");
            return;
        }
        logger.v("received new DestinationSelectedRequest event for destination " + destinationSelectedRequest);
        com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(new com.navdy.client.app.framework.models.Destination(destinationSelectedRequest.destination), new com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.Anon1(destinationSelectedRequest));
    }

    private com.navdy.service.library.events.destination.Destination EnsurePhoneNumbersAreNotLost(com.navdy.service.library.events.destination.Destination input, com.navdy.service.library.events.destination.Destination output) {
        com.navdy.service.library.events.destination.Destination.Builder builder = new com.navdy.service.library.events.destination.Destination.Builder(output);
        if (output.phoneNumbers == null || output.phoneNumbers.size() <= 0) {
            builder.phoneNumbers(input.phoneNumbers);
        }
        if (output.contacts == null || output.contacts.size() <= 0) {
            builder.contacts(input.contacts);
        }
        return builder.build();
    }

    private void performSearch(java.lang.String requestId, com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes serviceType) {
        new com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler.PlaceTypeSearchListener(requestId, serviceType, null).runSearch();
    }

    private com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes getGoogleServiceTypeForPlaceType(com.navdy.service.library.events.places.PlaceType placeType) {
        switch (placeType) {
            case PLACE_TYPE_GAS:
                return com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.GAS;
            case PLACE_TYPE_PARKING:
                return com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.PARKING;
            case PLACE_TYPE_RESTAURANT:
                return com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.FOOD;
            case PLACE_TYPE_COFFEE:
                return com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.COFFEE;
            case PLACE_TYPE_ATM:
                return com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.ATM;
            case PLACE_TYPE_HOSPITAL:
                return com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.HOSPITAL;
            case PLACE_TYPE_STORE:
                return com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes.STORE;
            default:
                return null;
        }
    }
}
