package com.navdy.client.app.framework.models;

public class Suggestion {
    private static final int NO_RESOURCE = -1;
    public boolean calculateSuggestedRoute = false;
    public com.navdy.client.app.framework.models.Destination destination;
    public com.navdy.client.app.framework.models.CalendarEvent event;
    private com.navdy.client.app.framework.models.Suggestion.SuggestionType type;

    public enum SuggestionType {
        LOADING,
        HERE_HEADER,
        GOOGLE_HEADER,
        SECTION_HEADER,
        ACTIVE_TRIP,
        PENDING_TRIP,
        DEMO_VIDEO,
        ENABLE_GLANCES,
        ENABLE_MICROPHONE,
        GOOGLE_NOW,
        HUD_LOCAL_MUSIC_BROWSER,
        VOICE_SEARCH,
        ENABLE_GESTURES,
        TRY_GESTURES,
        ADD_HOME,
        ADD_WORK,
        OTA,
        ADD_FAVORITE,
        CALENDAR,
        RECOMMENDATION,
        RECENT,
        GOOGLE_FOOTER,
        HERE_FOOTER
    }

    public Suggestion(com.navdy.client.app.framework.models.Destination destination2, com.navdy.client.app.framework.models.Suggestion.SuggestionType type2) {
        this.destination = destination2;
        this.type = type2;
    }

    public Suggestion(com.navdy.client.app.framework.models.Destination destination2, com.navdy.client.app.framework.models.Suggestion.SuggestionType type2, com.navdy.client.app.framework.models.CalendarEvent event2) {
        this.destination = destination2;
        this.type = type2;
        this.event = event2;
    }

    public boolean equals(java.lang.Object obj) {
        if (obj == null || !(obj instanceof com.navdy.client.app.framework.models.Suggestion)) {
            return false;
        }
        com.navdy.client.app.framework.models.Suggestion other = (com.navdy.client.app.framework.models.Suggestion) obj;
        if (other.type != this.type || !com.navdy.client.app.framework.models.Destination.equals(other.destination, this.destination)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        int i3 = 0;
        if (this.destination != null) {
            result = this.destination.hashCode();
        } else {
            result = 0;
        }
        int i4 = result * 31;
        if (this.event != null) {
            i = this.event.hashCode();
        } else {
            i = 0;
        }
        int i5 = (i4 + i) * 31;
        if (this.calculateSuggestedRoute) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        int i6 = (i5 + i2) * 31;
        if (this.type != null) {
            i3 = this.type.hashCode();
        }
        return i6 + i3;
    }

    public com.navdy.client.app.framework.models.Suggestion.SuggestionType getType() {
        return this.type;
    }

    public boolean shouldSendToHud() {
        return this.destination.hasOneValidSetOfCoordinates() && (isCalendar() || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECENT || isRecommendation());
    }

    public boolean canBeRoutedTo() {
        return this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECENT || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECOMMENDATION || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP;
    }

    public boolean hasInfoButton() {
        return this.type != com.navdy.client.app.framework.models.Suggestion.SuggestionType.OTA && !isTip();
    }

    public boolean isTip() {
        return this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.DEMO_VIDEO || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_GLANCES || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_MICROPHONE || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.VOICE_SEARCH || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_NOW || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.HUD_LOCAL_MUSIC_BROWSER || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_GESTURES || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.TRY_GESTURES || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_HOME || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_WORK || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.OTA || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_FAVORITE;
    }

    public boolean isRecommendation() {
        return this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.RECOMMENDATION;
    }

    public boolean isCalendar() {
        return this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.CALENDAR;
    }

    public boolean isActiveTrip() {
        return this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ACTIVE_TRIP;
    }

    public boolean isPendingTrip() {
        return this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP;
    }

    public boolean shouldShowRightChevron() {
        return this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_GLANCES || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_MICROPHONE || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.VOICE_SEARCH || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.GOOGLE_NOW || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.HUD_LOCAL_MUSIC_BROWSER || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ENABLE_GESTURES || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.TRY_GESTURES || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_HOME || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_WORK || this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.ADD_FAVORITE;
    }

    private boolean isColorful() {
        return isTip() || isCalendar() || isRecommendation();
    }

    public int getBadgeAsset(boolean isSelected) {
        if (isSelected) {
            return com.navdy.client.R.drawable.icon_suggested_edit;
        }
        switch (this.type) {
            case LOADING:
                return com.navdy.client.R.drawable.content_loading_loop;
            case PENDING_TRIP:
            case ACTIVE_TRIP:
                if (this.type == com.navdy.client.app.framework.models.Suggestion.SuggestionType.PENDING_TRIP) {
                    return this.destination.getBadgeAssetForPendingTrip();
                }
                return this.destination.getBadgeAssetForSuggestion();
            case DEMO_VIDEO:
                return com.navdy.client.R.drawable.icon_badge_video;
            case ENABLE_MICROPHONE:
            case VOICE_SEARCH:
                return com.navdy.client.R.drawable.icon_badge_voice_search;
            case ENABLE_GESTURES:
            case TRY_GESTURES:
                return com.navdy.client.R.drawable.icon_settings_gestures;
            case GOOGLE_NOW:
                return com.navdy.client.R.drawable.icon_googlenow;
            case HUD_LOCAL_MUSIC_BROWSER:
                return com.navdy.client.R.drawable.icon_music;
            case ENABLE_GLANCES:
                return com.navdy.client.R.drawable.icon_badge_glances;
            case CALENDAR:
                return com.navdy.client.R.drawable.icon_badge_calendar;
            case OTA:
                return com.navdy.client.R.drawable.icon_badge_softwareupdate;
            case ADD_FAVORITE:
                return isColorful() ? com.navdy.client.R.drawable.icon_badge_favorite : com.navdy.client.R.drawable.icon_badge_favorite_light;
            case ADD_HOME:
                return isColorful() ? com.navdy.client.R.drawable.icon_badge_home : com.navdy.client.R.drawable.icon_badge_home_light;
            case ADD_WORK:
                return isColorful() ? com.navdy.client.R.drawable.icon_badge_work : com.navdy.client.R.drawable.icon_badge_work_light;
            case RECOMMENDATION:
                return this.destination.getBadgeAsset();
            case RECENT:
                return this.destination.getBadgeAssetForRecent();
            default:
                return -1;
        }
    }

    public com.navdy.service.library.events.destination.Destination toProtobufDestinationObject() {
        return new com.navdy.service.library.events.destination.Destination.Builder(this.destination.toProtobufDestinationObject()).suggestion_type(getSuggestionType()).is_recommendation(java.lang.Boolean.valueOf(isRecommendation())).build();
    }

    private com.navdy.service.library.events.destination.Destination.SuggestionType getSuggestionType() {
        if (isCalendar()) {
            return com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR;
        }
        return com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT;
    }

    public java.lang.String toString() {
        return "Suggestion{type=" + this.type + ", calculateSuggestedRoute=" + this.calculateSuggestedRoute + ", destination=" + this.destination + '}';
    }
}
