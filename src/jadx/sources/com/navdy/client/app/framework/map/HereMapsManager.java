package com.navdy.client.app.framework.map;

public class HereMapsManager {
    private static final java.lang.String HERE_MAPS_INTERNAL_FOLDER = "/.here-maps";
    private static final java.lang.String MAP_ENGINE_PROCESS_NAME = "Navdy.Mapservice";
    private static final java.lang.String MAP_SERVICE_INTENT = "com.navdy.mapservice.MapService";
    private static final java.lang.Object initListenersLock = new java.lang.Object();
    private static final com.navdy.client.app.framework.map.HereMapsManager instance = new com.navdy.client.app.framework.map.HereMapsManager();
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.map.HereMapsManager.class);
    private com.here.android.mpa.common.OnEngineInitListener.Error initError;
    private final java.util.Queue<com.here.android.mpa.common.OnEngineInitListener> initListeners = new java.util.LinkedList();
    private final android.os.Handler mainThread = new android.os.Handler(android.os.Looper.getMainLooper());
    private final com.here.android.mpa.common.MapEngine mapEngine = com.here.android.mpa.common.MapEngine.getInstance();
    private final com.here.android.mpa.common.OnEngineInitListener onEngineInitListener = new com.navdy.client.app.framework.map.HereMapsManager.Anon1();
    private volatile com.navdy.client.app.framework.map.HereMapsManager.State state = com.navdy.client.app.framework.map.HereMapsManager.State.IDLE;

    class Anon1 implements com.here.android.mpa.common.OnEngineInitListener {

        /* renamed from: com.navdy.client.app.framework.map.HereMapsManager$Anon1$Anon1 reason: collision with other inner class name */
        class C0041Anon1 implements java.lang.Runnable {
            final /* synthetic */ com.here.android.mpa.common.OnEngineInitListener.Error val$error;

            /* renamed from: com.navdy.client.app.framework.map.HereMapsManager$Anon1$Anon1$Anon1 reason: collision with other inner class name */
            class C0042Anon1 implements java.lang.Runnable {
                C0042Anon1() {
                }

                public void run() {
                    synchronized (com.navdy.client.app.framework.map.HereMapsManager.initListenersLock) {
                        while (!com.navdy.client.app.framework.map.HereMapsManager.this.initListeners.isEmpty()) {
                            ((com.here.android.mpa.common.OnEngineInitListener) com.navdy.client.app.framework.map.HereMapsManager.this.initListeners.poll()).onEngineInitializationCompleted(com.navdy.client.app.framework.map.HereMapsManager.Anon1.C0041Anon1.this.val$error);
                        }
                    }
                }
            }

            C0041Anon1(com.here.android.mpa.common.OnEngineInitListener.Error error) {
                this.val$error = error;
            }

            public void run() {
                com.navdy.client.app.framework.map.HereMapsManager.this.initError = this.val$error;
                if (this.val$error == com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                    com.navdy.client.app.framework.map.HereMapsManager.logger.i("MapEngine has initialized correctly");
                    com.navdy.client.app.framework.map.HereMapsManager.logger.i("HERE SDK version " + com.here.android.mpa.common.Version.getSdkVersion());
                    com.navdy.client.app.framework.map.HereMapsConfigurator.getInstance().updateMapsConfig();
                    com.navdy.client.app.framework.map.HereMapsManager.this.state = com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZED;
                    com.navdy.client.app.framework.map.HereMapsManager.this.mainThread.post(new com.navdy.client.app.framework.map.HereMapsManager.Anon1.C0041Anon1.C0042Anon1());
                    return;
                }
                com.navdy.client.app.framework.map.HereMapsManager.this.state = com.navdy.client.app.framework.map.HereMapsManager.State.FAILED;
                com.navdy.client.app.framework.map.HereMapsManager.logger.e("Map failed to initialize: " + this.val$error);
                com.navdy.client.app.framework.map.HereMapsManager.logger.e("ERROR ON MAP ENGINE INIT, RESTARTING");
                com.navdy.client.app.framework.map.HereMapsManager.this.killSelfAndMapEngine();
            }
        }

        Anon1() {
        }

        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.HereMapsManager.Anon1.C0041Anon1(error), 11);
        }
    }

    class Anon2 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.common.OnEngineInitListener val$listener;

        Anon2(com.here.android.mpa.common.OnEngineInitListener onEngineInitListener) {
            this.val$listener = onEngineInitListener;
        }

        public void run() {
            this.val$listener.onEngineInitializationCompleted(com.navdy.client.app.framework.map.HereMapsManager.this.initError);
        }
    }

    class Anon3 implements java.lang.Runnable {
        final /* synthetic */ com.here.android.mpa.common.OnEngineInitListener val$listener;

        Anon3(com.here.android.mpa.common.OnEngineInitListener onEngineInitListener) {
            this.val$listener = onEngineInitListener;
        }

        public void run() {
            synchronized (com.navdy.client.app.framework.map.HereMapsManager.initListenersLock) {
                com.navdy.client.app.framework.map.HereMapsManager.this.initListeners.add(this.val$listener);
            }
            if (com.navdy.client.app.framework.map.HereMapsManager.this.state == com.navdy.client.app.framework.map.HereMapsManager.State.IDLE) {
                com.navdy.client.app.framework.map.HereMapsManager.this.initializeIfPermitted();
            }
        }
    }

    private enum State {
        IDLE,
        INITIALIZING,
        INITIALIZED,
        FAILED
    }

    public static com.navdy.client.app.framework.map.HereMapsManager getInstance() {
        return instance;
    }

    private HereMapsManager() {
    }

    public void addOnInitializedListener(com.here.android.mpa.common.OnEngineInitListener listener) {
        if (this.state != com.navdy.client.app.framework.map.HereMapsManager.State.FAILED) {
            if (this.state == com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZED) {
                this.mainThread.post(new com.navdy.client.app.framework.map.HereMapsManager.Anon2(listener));
            } else {
                com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.framework.map.HereMapsManager.Anon3(listener), 11);
            }
        }
    }

    public boolean isInitialized() {
        return this.state == com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZED;
    }

    public void killSelfAndMapEngine() {
        java.util.Iterator it = ((android.app.ActivityManager) com.navdy.client.app.NavdyApplication.getAppContext().getSystemService("activity")).getRunningAppProcesses().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            android.app.ActivityManager.RunningAppProcessInfo processInfo = (android.app.ActivityManager.RunningAppProcessInfo) it.next();
            if (processInfo.processName.equals(MAP_ENGINE_PROCESS_NAME)) {
                android.os.Process.killProcess(processInfo.pid);
                break;
            }
        }
        com.navdy.client.app.NavdyApplication.kill();
    }

    private void initializeIfPermitted() {
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        boolean readExternalStoragePermission = com.navdy.client.app.ui.base.BaseActivity.weHaveThisPermission(context, "android.permission.READ_EXTERNAL_STORAGE");
        boolean writeExternalStoragePermission = com.navdy.client.app.ui.base.BaseActivity.weHaveThisPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        boolean accessFineLocationPermission = com.navdy.client.app.ui.base.BaseActivity.weHaveThisPermission(context, "android.permission.ACCESS_FINE_LOCATION");
        if (!readExternalStoragePermission || !writeExternalStoragePermission || !accessFineLocationPermission) {
            logger.d("initialization is not permitted yet, permission granted for READ_EXTERNAL_STORAGE=" + readExternalStoragePermission + "; writeExternalStoragePermission=" + writeExternalStoragePermission + "; accessFineLocationPermission=" + accessFineLocationPermission);
            return;
        }
        logger.v("initialization is permitted, initializing...");
        initialize();
    }

    private void initialize() {
        logger.v("initializing here map engine");
        this.state = com.navdy.client.app.framework.map.HereMapsManager.State.INITIALIZING;
        android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
        com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(context.getFilesDir() + HERE_MAPS_INTERNAL_FOLDER, MAP_SERVICE_INTENT);
        this.mapEngine.init(context, this.onEngineInitListener);
    }
}
