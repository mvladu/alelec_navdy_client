package com.navdy.client.app.framework.models;

public class DestinationCacheEntry {
    public com.navdy.client.app.framework.models.Destination destination;
    public int destinationId;
    public int id = -1;
    public long lastResponseDate;
    public java.lang.String locationString;

    public DestinationCacheEntry(int id2, long lastResponseDate2, java.lang.String locationString2, int destinationId2) {
        this.id = id2;
        this.lastResponseDate = lastResponseDate2;
        this.locationString = locationString2;
        this.destinationId = destinationId2;
    }

    public java.lang.String toString() {
        return "DestinationCacheEntry{id=" + this.id + ", lastResponseDate=" + this.lastResponseDate + ", locationString='" + this.locationString + '\'' + ", destinationId=" + this.destinationId + ", destination=" + this.destination + '}';
    }
}
