package com.navdy.client.app.framework.util;

public class CalendarObserver extends android.database.ContentObserver {
    private static final int DELAY_THRESHOLD = 10000;
    private static final boolean VERBOSE = false;
    private static final com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.CalendarObserver.class);
    private android.os.Handler handler;
    private long lastSendTime = 0;
    private java.lang.Runnable sendRunnable = new com.navdy.client.app.framework.util.CalendarObserver.Anon1();
    private java.util.concurrent.atomic.AtomicBoolean shouldSendToHud = new java.util.concurrent.atomic.AtomicBoolean(false);

    class Anon1 implements java.lang.Runnable {
        Anon1() {
        }

        public void run() {
            com.navdy.client.app.framework.util.CalendarObserver.logger.i("should send calendar events in runnable: " + com.navdy.client.app.framework.util.CalendarObserver.this.shouldSendToHud);
            if (com.navdy.client.app.framework.util.CalendarObserver.this.shouldSendToHud.getAndSet(false)) {
                com.navdy.client.app.framework.util.CalendarObserver.this.lastSendTime = java.lang.System.currentTimeMillis();
                com.navdy.client.app.ui.homescreen.CalendarUtils.sendCalendarsToHud();
                com.navdy.client.app.framework.util.CalendarObserver.this.postSendCalendarRunnable();
            }
        }
    }

    public CalendarObserver(android.os.Handler handler2) {
        super(handler2);
        this.handler = handler2;
    }

    public void postSendCalendarRunnable() {
        this.handler.removeCallbacks(this.sendRunnable);
        this.handler.postDelayed(this.sendRunnable, 10000);
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        synchronized (this) {
            this.shouldSendToHud.set(true);
            if (java.lang.System.currentTimeMillis() >= this.lastSendTime + 10000) {
                this.sendRunnable.run();
            }
        }
    }
}
