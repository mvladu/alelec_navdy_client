package com.navdy.client.app.framework.callcontrol;

public class TelephonyInterfaceFactory {
    public static com.navdy.client.app.framework.callcontrol.TelephonyInterface getTelephonyInterface(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT > 25) {
            return new com.navdy.client.app.framework.callcontrol.TelephonySupport26(context);
        }
        if (android.os.Build.VERSION.SDK_INT < 21) {
            return new com.navdy.client.app.framework.callcontrol.TelephonySupport(context);
        }
        if (!android.os.Build.MANUFACTURER.equalsIgnoreCase("lge")) {
            if (android.os.Build.MANUFACTURER.equalsIgnoreCase("samsung")) {
                if (android.os.Build.MODEL.contains("SM-G920") || android.os.Build.MODEL.contains("SM-G935") || android.os.Build.MODEL.contains("SM-G930") || android.os.Build.MODEL.contains("SM-N900") || android.os.Build.MODEL.contains("SM-N910") || android.os.Build.MODEL.contains("SM-G900")) {
                    return new com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G920I(context);
                }
                if (android.os.Build.MODEL.contains("SM-G950")) {
                    return new com.navdy.client.app.framework.callcontrol.TelephonySupport21_SAMSUNG_G950(context);
                }
            }
            if (!android.os.Build.MANUFACTURER.equalsIgnoreCase("htc") || !android.os.Build.MODEL.contains("HTC6545LVW")) {
                return new com.navdy.client.app.framework.callcontrol.TelephonySupport21(context);
            }
            return new com.navdy.client.app.framework.callcontrol.TelephonySupport21HTC10(context);
        } else if (android.os.Build.MODEL.toLowerCase().contains("nexus")) {
            return new com.navdy.client.app.framework.callcontrol.TelephonySupport21(context);
        } else {
            if (android.os.Build.DEVICE.equalsIgnoreCase("h1") || android.os.Build.DEVICE.equalsIgnoreCase("p1")) {
                return new com.navdy.client.app.framework.callcontrol.TelephonySupport21(context);
            }
            return new com.navdy.client.app.framework.callcontrol.TelephonySupport21_LG_G3(context);
        }
    }
}
