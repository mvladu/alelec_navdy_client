package com.navdy.client.app.framework.servicehandler;

public class SpeechServiceHandler {
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.SpeechServiceHandler.class);
    @javax.inject.Inject
    com.navdy.client.app.framework.util.TTSAudioRouter audioRouter;
    @javax.inject.Inject
    android.content.SharedPreferences sharedPreferences;

    public SpeechServiceHandler() {
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
        this.audioRouter.setSpeechServiceHadler(this);
        com.navdy.client.app.framework.util.BusProvider.getInstance().register(this);
    }

    @com.squareup.otto.Subscribe
    public void onSpeechRequest(com.navdy.service.library.events.audio.SpeechRequest request) {
        java.lang.String words = request.words;
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(words) && shouldRequestBeProcessed(request)) {
            sLogger.v("[tts-play] category[" + request.category + "] id [" + request.id + "] text[" + words + "]");
            this.audioRouter.processTTSRequest(request.words, request.id, request.sendStatus != null ? request.sendStatus.booleanValue() : false);
        }
    }

    @com.squareup.otto.Subscribe
    public void onCancelSpeechRequest(com.navdy.service.library.events.audio.CancelSpeechRequest request) {
        java.lang.String id = request.id;
        sLogger.v("[tts-cancel] id[" + id + "]");
        if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(id)) {
            this.audioRouter.cancelTTSRequest(id);
        }
    }

    @com.squareup.otto.Subscribe
    public void onPlayAudioRequest(com.navdy.service.library.events.audio.PlayAudioRequest request) {
        this.audioRouter.processPlayAudioRequest(request);
    }

    private boolean shouldRequestBeProcessed(com.navdy.service.library.events.audio.SpeechRequest request) {
        if (request.category != com.navdy.service.library.events.audio.Category.SPEECH_WELCOME_MESSAGE || this.sharedPreferences.getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, false)) {
            return true;
        }
        return false;
    }

    public void sendSpeechNotification(com.navdy.service.library.events.audio.SpeechRequestStatus event) {
        try {
            sLogger.v("send speech notification id[" + event.id + "] type[" + event.status + "]");
            com.navdy.client.app.framework.DeviceConnection.postEvent((com.squareup.wire.Message) event);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
