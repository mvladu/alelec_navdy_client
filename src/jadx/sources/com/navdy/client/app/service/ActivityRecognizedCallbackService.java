package com.navdy.client.app.service;

public class ActivityRecognizedCallbackService extends android.app.IntentService {
    public static final int MINIMUM_CONFIDENCE_THRESHOLD = 50;
    private static long STATE_EXPIRY_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(3);
    public static final int STATE_IN_VEHICLE = 1;
    public static final int STATE_ON_FOOT = 2;
    public static final int STATE_UNKNOWN = 0;
    public static final int WALKING_ACTIVITY_CONFIDENCE_MINIMUM_THRESHOLD = 75;
    private static int currentState = 0;
    private static int lastState = 0;
    private static long lastStateUpdateTime;
    public static final com.navdy.service.library.log.Logger sLogger = new com.navdy.service.library.log.Logger(com.navdy.client.app.service.ActivityRecognizedCallbackService.class);
    long[] DEBUG_VIBRATE_PATTERN = {0, 500, 100, 500, 100, 500};
    @javax.inject.Inject
    com.navdy.client.app.framework.util.TTSAudioRouter audioRouter;

    public ActivityRecognizedCallbackService() {
        super("ActivityRecognizedCallbackService");
    }

    public void onCreate() {
        super.onCreate();
        com.navdy.client.app.framework.Injector.inject(com.navdy.client.app.NavdyApplication.getAppContext(), this);
    }

    protected void onHandleIntent(android.content.Intent intent) {
        if (com.google.android.gms.location.ActivityRecognitionResult.hasResult(intent)) {
            sLogger.d("onHandleIntent, Activity recognized callback has results");
            handleDetectedActivities(com.google.android.gms.location.ActivityRecognitionResult.extractResult(intent).getProbableActivities());
        }
    }

    private void handleDetectedActivities(java.util.List<com.google.android.gms.location.DetectedActivity> probableActivities) {
        boolean isInVehicle = false;
        int isInVehicleConfidence = 0;
        boolean isOnFoot = false;
        int isOnFootConfidence = 0;
        for (com.google.android.gms.location.DetectedActivity activity : probableActivities) {
            switch (activity.getType()) {
                case 0:
                    isInVehicle = true;
                    isInVehicleConfidence = activity.getConfidence();
                    break;
                case 2:
                    isOnFoot = true;
                    isOnFootConfidence = activity.getConfidence();
                    break;
            }
            if (isInVehicle || isOnFoot) {
                sLogger.d("Activity " + activity + ", Confidence : " + activity.getConfidence());
            }
        }
        if (android.os.SystemClock.elapsedRealtime() - lastStateUpdateTime > STATE_EXPIRY_INTERVAL) {
            sLogger.d("Last update is expired");
            lastState = currentState;
            setCurrentState(0);
        } else {
            lastState = currentState;
        }
        if (!isOnFoot || isOnFootConfidence <= 50) {
            if (!isInVehicle || isInVehicleConfidence <= 50) {
                sLogger.d("Currently UNKNOWN");
            } else {
                setCurrentState(1);
            }
        } else if (!isInVehicle || isOnFootConfidence > isInVehicleConfidence) {
            setCurrentState(2);
        } else {
            setCurrentState(1);
        }
        com.navdy.client.app.service.DataCollectionService.getInstance().handleDetectedActivities(isInVehicle, isInVehicleConfidence);
    }

    private void setCurrentState(int state) {
        sLogger.d("setCurrentState " + state);
        currentState = state;
        lastStateUpdateTime = android.os.SystemClock.elapsedRealtime();
        switch (state) {
            case 1:
                sLogger.d("Currently IN_VEHICLE");
                java.lang.String message = "You got into your vehicle";
                break;
            case 2:
                sLogger.d("Currently ON_FOOT");
                java.lang.String message2 = "You are walking now";
                break;
            default:
                return;
        }
        if (lastState != currentState) {
        }
        if (lastState == 1 && currentState == 2) {
            sLogger.d("Transition from in vehicle to on foot, time to send accelerate shutdown");
            com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null) {
                remoteDevice.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.hudcontrol.AccelerateShutdown.Builder().reason(com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason.ACCELERATE_REASON_WALKING).build());
            }
        }
    }
}
