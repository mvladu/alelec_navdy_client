package com.navdy.client.app.service;

public final class ActivityRecognizedCallbackService$$InjectAdapter extends dagger.internal.Binding<com.navdy.client.app.service.ActivityRecognizedCallbackService> implements javax.inject.Provider<com.navdy.client.app.service.ActivityRecognizedCallbackService>, dagger.MembersInjector<com.navdy.client.app.service.ActivityRecognizedCallbackService> {
    private dagger.internal.Binding<com.navdy.client.app.framework.util.TTSAudioRouter> audioRouter;

    public ActivityRecognizedCallbackService$$InjectAdapter() {
        super("com.navdy.client.app.service.ActivityRecognizedCallbackService", "members/com.navdy.client.app.service.ActivityRecognizedCallbackService", false, com.navdy.client.app.service.ActivityRecognizedCallbackService.class);
    }

    public void attach(dagger.internal.Linker linker) {
        this.audioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", com.navdy.client.app.service.ActivityRecognizedCallbackService.class, getClass().getClassLoader());
    }

    public void getDependencies(java.util.Set<dagger.internal.Binding<?>> set, java.util.Set<dagger.internal.Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.audioRouter);
    }

    public com.navdy.client.app.service.ActivityRecognizedCallbackService get() {
        com.navdy.client.app.service.ActivityRecognizedCallbackService result = new com.navdy.client.app.service.ActivityRecognizedCallbackService();
        injectMembers(result);
        return result;
    }

    public void injectMembers(com.navdy.client.app.service.ActivityRecognizedCallbackService object) {
        object.audioRouter = (com.navdy.client.app.framework.util.TTSAudioRouter) this.audioRouter.get();
    }
}
