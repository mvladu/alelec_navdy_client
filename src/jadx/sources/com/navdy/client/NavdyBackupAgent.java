package com.navdy.client;

public class NavdyBackupAgent extends android.app.backup.BackupAgentHelper {
    com.navdy.service.library.log.Logger logger = new com.navdy.service.library.log.Logger(com.navdy.client.NavdyBackupAgent.class);

    public void onCreate() {
        super.onCreate();
        this.logger.v("onCreate");
    }

    public void onBackup(android.os.ParcelFileDescriptor oldState, android.app.backup.BackupDataOutput data, android.os.ParcelFileDescriptor newState) throws java.io.IOException {
        super.onBackup(oldState, data, newState);
        this.logger.v("onBackup: oldState = " + oldState + ", data = " + data + ", newState = " + newState);
    }

    public void onFullBackup(android.app.backup.FullBackupDataOutput data) throws java.io.IOException {
        super.onFullBackup(data);
        this.logger.v("onFullBackup: data = " + data);
    }

    public void onQuotaExceeded(long backupDataBytes, long quotaBytes) {
        super.onQuotaExceeded(backupDataBytes, quotaBytes);
        this.logger.e("Backup quota exceeded! backupDataBytes = " + backupDataBytes + ", quotaBytes = " + quotaBytes);
    }

    public void onRestore(android.app.backup.BackupDataInput data, int appVersionCode, android.os.ParcelFileDescriptor newState) throws java.io.IOException {
        super.onRestore(data, appVersionCode, newState);
        this.logger.v("onRestore: data = " + data + ", appVersionCode = " + appVersionCode + ", newState = " + newState);
    }

    public void onRestoreFile(android.os.ParcelFileDescriptor data, long size, java.io.File destination, int type, long mode, long mtime) throws java.io.IOException {
        super.onRestoreFile(data, size, destination, type, mode, mtime);
        this.logger.v("onRestoreFile: data = " + data + ", size = " + size + ", destination = " + destination + ", type = " + type + ", mode = " + mode + ", mtime = " + mtime);
    }

    public void onRestoreFinished() {
        super.onRestoreFinished();
        this.logger.v("onRestoreFinished");
        com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit().remove(com.navdy.client.app.ui.settings.SettingsConstants.GMS_IS_MISSING).remove(com.navdy.client.app.ui.settings.SettingsConstants.GMS_VERSION_NUMBER).apply();
    }

    public void onDestroy() {
        super.onDestroy();
        this.logger.v("onDestroy");
    }
}
