package com.navdy.client.debug;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.navdy.client.BuildConfig;
import com.navdy.client.R.layout;
import com.navdy.client.R.string;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.obd.ObdStatusRequest;
import com.navdy.service.library.events.obd.ObdStatusResponse;
import com.squareup.wire.Message;

import net.hockeyapp.android.UpdateManagerListener;

import butterknife.ButterKnife;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class AboutFragment extends Fragment {
    // @butterknife.InjectView(2131755553)
    @DexIgnore
    TextView appVersionLabel;
    // @butterknife.InjectView(2131755556)
    @DexIgnore
    TextView displayInfo;
    // @butterknife.InjectView(2131755557)
    @DexIgnore
    TextView serialLabel;
    // @butterknife.InjectView(2131755558)
    @DexIgnore
    TextView serialNumber;
    // @butterknife.InjectView(2131755554)
    @DexIgnore
    TextView updateStatusTextView;
    // @butterknife.InjectView(2131755560)
    @DexIgnore
    TextView vin;
    // @butterknife.InjectView(2131755559)
    @DexIgnore
    TextView vinLabel;

    @DexIgnore
    class Anon1 extends UpdateManagerListener {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void onNoUpdateAvailable() {
            AboutFragment.this.updateStatusTextView.setVisibility(View.VISIBLE);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        BusProvider.getInstance().register(this);
        View rootView = inflater.inflate(layout.fragment_about, container, false);
        ButterKnife.inject(this, rootView);
        this.appVersionLabel.setText(BuildConfig.VERSION_NAME);
        RemoteDevice device = AppInstance.getInstance().getRemoteDevice();
        int visibility = 8;
        if (device != null) {
            visibility = 0;
            DeviceInfo info = device.getDeviceInfo();
            this.serialNumber.setText(info != null ? info.deviceUuid : getResources().getString(string.unknown));
        }
        this.displayInfo.setVisibility(visibility);
        this.serialLabel.setVisibility(visibility);
        this.serialNumber.setVisibility(visibility);
        return rootView;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        this.vinLabel.setVisibility(View.GONE);
        this.vin.setVisibility(View.GONE);
        DeviceConnection.postEvent(new ObdStatusRequest());
    }

    // @butterknife.OnClick({2131755555})
    @DexIgnore
    public void onCheckForUpdate(Button button) {
        this.updateStatusTextView.setVisibility(View.INVISIBLE);
        ((MainDebugActivity) getActivity()).checkForUpdates(new Anon1());
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
    }

    @DexReplace
    public void onPause() {
        try {
            BusProvider.getInstance().unregister(this);
        } catch(IllegalArgumentException ignored) {
        }
        super.onPause();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onObdStatusResponse(ObdStatusResponse response) {
        this.vinLabel.setVisibility(View.VISIBLE);
        this.vin.setVisibility(View.VISIBLE);
        this.vin.setText(response.vin != null ? response.vin : getResources().getString(string.unknown));
    }
}
