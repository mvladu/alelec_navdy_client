package com.navdy.client.debug.navdebug;

import android.annotation.SuppressLint;

import com.here.android.mpa.routing.RouteWaypoint;
import com.navdy.client.app.framework.map.HereMapsManager;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.IGNORE)
public class NavRouteBrowserFragment extends android.app.ListFragment implements com.here.android.mpa.common.PositioningManager.OnPositionChangedListener, com.here.android.mpa.common.OnEngineInitListener {
    @DexIgnore
    private static /* final */ double[] DEFAULT_DESTINATION; // = {49.24831d, -122.980013d};
    @DexIgnore
    private static /* final */ double[] DEFAULT_START_POINT; // = {49.260024d, -123.006984d};
    @DexIgnore
    public static /* final */ java.lang.String EXTRA_COORDS_END; // = "dest_coords";
    @DexIgnore
    public static /* final */ java.lang.String EXTRA_COORDS_START; // = "start_coords";
    @DexIgnore
    public static /* final */ com.navdy.service.library.log.Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.debug.navdebug.NavRouteBrowserFragment.class);
    @DexIgnore
    protected boolean mCalculatingRoute;
    @DexIgnore
    protected com.here.android.mpa.common.GeoPosition mCurrentGeoPosition;
    @DexIgnore
    private com.here.android.mpa.common.GeoCoordinate mEndCoord;
    @DexIgnore
    protected boolean mEngineAvailable;
    @DexIgnore
    protected boolean mEngineInitialized;
    @DexIgnore
    protected com.navdy.client.debug.navdebug.ManeuverArrayAdapter mManeuverArrayAdapter;
    @DexIgnore
    protected java.util.ArrayList<com.here.android.mpa.routing.Maneuver> mManeuvers;
    @DexIgnore
    private com.here.android.mpa.routing.Route mRoute; // = null;
    @DexIgnore
    private com.here.android.mpa.common.GeoCoordinate mStartCoord;

    static {}

    @DexIgnore
    public NavRouteBrowserFragment() {}

    @DexReplace
    class Anon1 implements com.here.android.mpa.routing.CoreRouter.Listener {
        Anon1() {
        }

        public void onCalculateRouteFinished(java.util.List<com.here.android.mpa.routing.RouteResult> results, com.here.android.mpa.routing.RoutingError error) {
            com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.mCalculatingRoute = false;
            if (error != com.here.android.mpa.routing.RoutingError.NONE) {
                com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.showError("Failed: " + error.toString());
            } else if (results.size() > 0) {
                com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.setRoute(((com.here.android.mpa.routing.RouteResult) results.get(0)).getRoute());
            } else {
                com.navdy.client.debug.navdebug.NavRouteBrowserFragment.this.showError("No results.");
            }
        }

        public void onProgress(int percentage) {
            com.navdy.client.debug.navdebug.NavRouteBrowserFragment.sLogger.i("... " + percentage + " percent done ...");
        }
    }

    @DexIgnore
    public static com.navdy.client.debug.navdebug.NavRouteBrowserFragment newInstance(com.here.android.mpa.common.GeoCoordinate startCoord, com.here.android.mpa.common.GeoCoordinate endCoord) {
        com.navdy.client.debug.navdebug.NavRouteBrowserFragment fragment = new com.navdy.client.debug.navdebug.NavRouteBrowserFragment();
        android.os.Bundle args = new android.os.Bundle();
        if (startCoord == null || endCoord == null) {
            sLogger.e("createIntentWithCoords: bad coordinates");
            return null;
        }
        args.putDoubleArray("start_coords", latLonArrayFromGeoCoordinate(startCoord));
        args.putDoubleArray("dest_coords", latLonArrayFromGeoCoordinate(endCoord));
        fragment.setArguments(args);
        return fragment;
    }

    @DexIgnore
    public void loadArguments() {
        double[] startLatLon = getArguments().getDoubleArray("start_coords");
        double[] endLatLon = getArguments().getDoubleArray("dest_coords");
        if (startLatLon == null || startLatLon.length != 2) {
            sLogger.e("Missing valid start coordinates. Falling back.");
            startLatLon = DEFAULT_START_POINT;
        }
        if (endLatLon == null || endLatLon.length != 2) {
            sLogger.e("Missing end coordinates. Falling back.");
            endLatLon = DEFAULT_DESTINATION;
        }
        this.mStartCoord = new com.here.android.mpa.common.GeoCoordinate(startLatLon[0], startLatLon[1]);
        this.mEndCoord = new com.here.android.mpa.common.GeoCoordinate(endLatLon[0], endLatLon[1]);
    }

    @DexIgnore
    public static double[] latLonArrayFromGeoCoordinate(com.here.android.mpa.common.GeoCoordinate coordinate) {
        return new double[]{coordinate.getLatitude(), coordinate.getLongitude()};
    }

    @DexIgnore
    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            loadArguments();
        }
        initEngine();
        this.mManeuvers = new java.util.ArrayList<>();
        setListAdapter(new com.navdy.client.debug.navdebug.ManeuverArrayAdapter(getActivity(), this.mManeuvers));
    }

    @DexIgnore
    public android.view.View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, android.os.Bundle savedInstanceState) {
        return inflater.inflate(com.navdy.client.R.layout.fragment_nav_route_browser, container, false);
    }

    @DexReplace
    public void initEngine() {
        if (this.mEngineInitialized) {
            sLogger.e("Already inited.");
            return;
        }
        try {
            HereMapsManager.mapEngineInit(com.here.android.mpa.common.MapEngine.getInstance(), getActivity(), this);
        } catch (java.lang.Exception e) {
            sLogger.e("Unable to init MapEngine");
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
        if (error == com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
            sLogger.e("MapEngine initialized.");
            this.mEngineInitialized = true;
            initializePosition();
            return;
        }
        sLogger.e("MapEngine init completed with error: " + error.toString());
    }

    @DexIgnore
    protected void initializePosition() {
        com.here.android.mpa.common.PositioningManager positioningManager = com.here.android.mpa.common.PositioningManager.getInstance();
        positioningManager.addListener(new java.lang.ref.WeakReference(this));
        if (!positioningManager.isActive()) {
            positioningManager.start(com.here.android.mpa.common.PositioningManager.LocationMethod.GPS_NETWORK);
        }
    }

    @DexIgnore
    public void onPositionUpdated(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.GeoPosition geoPosition, boolean isMapMatched) {
        sLogger.d("onPositionUpdated method: " + locationMethod + " position: " + geoPosition.getCoordinate().toString());
        this.mCurrentGeoPosition = geoPosition;
        if (!this.mEngineAvailable) {
            this.mEngineAvailable = true;
            onEngineAvailable();
        }
    }

    @DexIgnore
    public void onEngineAvailable() {
        buildRoute();
    }

    @DexIgnore
    public void onPositionFixChanged(com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod, com.here.android.mpa.common.PositioningManager.LocationStatus locationStatus) {
        sLogger.d("onPositionFixChanged: method:" + locationMethod + " status: " + locationStatus);
    }

    @DexReplace
    public void buildRoute() {
        if (this.mRoute == null && !this.mCalculatingRoute && this.mEngineAvailable) {
            this.mCalculatingRoute = true;
            com.here.android.mpa.routing.CoreRouter routeManager = new com.here.android.mpa.routing.CoreRouter();
            com.here.android.mpa.routing.RoutePlan routePlan = new com.here.android.mpa.routing.RoutePlan();
            com.here.android.mpa.routing.RouteOptions routeOptions = new com.here.android.mpa.routing.RouteOptions();
            routeOptions.setTransportMode(com.here.android.mpa.routing.RouteOptions.TransportMode.CAR);
            routeOptions.setRouteType(com.here.android.mpa.routing.RouteOptions.Type.FASTEST);
            routePlan.setRouteOptions(routeOptions);
            routePlan.addWaypoint(new RouteWaypoint(this.mStartCoord));
            routePlan.addWaypoint(new RouteWaypoint(this.mEndCoord));
            routeManager.calculateRoute(routePlan, new com.navdy.client.debug.navdebug.NavRouteBrowserFragment.Anon1());
        }
    }

    @DexIgnore
    protected void setRoute(com.here.android.mpa.routing.Route route) {
        this.mRoute = route;
        if (route.getManeuvers() == null) {
            showError("No maneuvers in route.");
            return;
        }
        this.mManeuvers.clear();
        this.mManeuvers.addAll(route.getManeuvers());
        ((com.navdy.client.debug.navdebug.ManeuverArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    protected void showError(java.lang.String error) {
        sLogger.e(error);
        android.widget.Toast.makeText(getActivity(), error, 0).show();
    }
}
