package com.navdy.client.ota;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public interface OTAUpdateManager {
    @DexIgnore
    void abortDownload();

    @DexIgnore
    void abortUpload();

    @DexIgnore
    void checkForUpdate(int i, boolean z);

    @DexIgnore
    void download(com.navdy.client.ota.model.UpdateInfo updateInfo, java.io.File file, int i);

    @DexIgnore
    void uploadToHUD(java.io.File file, long j, java.lang.String str);

    @DexAdd
    void installAPK(java.io.File file);
}
