package com.navdy.client.ota.impl;

import android.os.AsyncTask;
import android.widget.Toast;

import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import lanchon.dexpatcher.annotation.DexAdd;

import static org.droidparts.Injector.getApplicationContext;

@DexAdd
class DownloadFileFromURL extends AsyncTask<String, String, String> {
    private static final Logger sLogger = new Logger(DownloadFileFromURL.class);

    public TransferObserver observer = new TransferObserver();
    File destination;
    URLConnection conection;
    URL _url;

    public interface DownloadListener {
        void onError(int var1, Exception var2);

        void onProgressChanged(int var1, long bytesTransferred, long bytesTotal);

        void onStateChanged(int var1, TransferState var2);
    }

    public void start(String url, String path) {
        destination = new File(path);
        boolean success = true;

        try {
            _url = new URL(url);
            conection = _url.openConnection();
            conection.connect();
            // this will be useful so that you can show a tipical 0-100% progress bar
            observer.bytesTotal = conection.getContentLength();

            if (destination.exists()) {
                if (destination.length() == observer.bytesTotal) {
                    observer.setTransferState(TransferState.COMPLETED);
                    return;
                } else {
                    if (!destination.delete()) {
                        observer.setTransferState(TransferState.FAILED);
                        return;
                    }
                    // IOUtils.deleteFile(this, destination.getAbsolutePath());
                }
            }

            // Start background download
            execute();

        } catch (Exception e) {
            sLogger.e("Error: ", e);
            if (observer.transferListener != null) {
                observer.transferListener.onError(0, e);
            }
        }
    }

    // /**
    //  * Before starting background thread
    //  * Show Progress Bar Dialog
    //  * */
    // @Override
    // protected void onPreExecute() {
    //     super.onPreExecute();
    //     showDialog(progress_bar_type);
    //
    // }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(String... args) {
        int count;
        try {


            // download the file
            InputStream input = new BufferedInputStream(_url.openStream(), 8192);

            // Output stream
            //extension must change (mp3,mp4,zip,apk etc.)
            // TODO fix
            OutputStream output = new FileOutputStream(destination);

            byte[] data = new byte[64*1024];

            long total = 0;

            observer.setTransferState(TransferState.IN_PROGRESS);

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                // publishProgress(""+(int)((total*100)/lenghtOfFile));

                observer.bytesTransferred = total;
                if (observer.transferListener != null) {
                    observer.transferListener.onProgressChanged(0, observer.bytesTransferred, observer.bytesTotal);
                }

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
            observer.setTransferState(TransferState.COMPLETED);

        } catch (Exception e) {
            sLogger.e("Error: ", e);
            if (observer.transferListener != null) {
                observer.transferListener.onError(0, e);
            }
        }

        return null;
    }

    // /**
    //  * Updating progress bar
    //  * */
    // protected void onProgressUpdate(String... progress) {
    //     // setting progress percentage
    //     pDialog.setProgress(Integer.parseInt(progress[0]));
    // }

    /**
     * After completing background task
     * Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        // dismiss the dialog after the file was downloaded
        // dismissDialog(progress_bar_type);

        // Toast.makeText(NavdyApplication.getAppContext(),file_url+" downloaded.", Toast.LENGTH_LONG).show();
    }


    public enum TransferState {
        CANCELED,
        COMPLETED,
        FAILED,
        IN_PROGRESS,
        PART_COMPLETED,
        PAUSED,
        PENDING_CANCEL,
        PENDING_NETWORK_DISCONNECT,
        PENDING_PAUSE,
        RESUMED_WAITING,
        UNKNOWN,
        WAITING,
        WAITING_FOR_NETWORK;
    }

    public class TransferObserver {
        private String bucket;
        private long bytesTotal;
        private long bytesTransferred;
        // private final TransferDBUtil dbUtil;
        private String filePath;
        // private final int id;
        private String key;
        private TransferObserver.TransferStatusListener statusListener;
        private DownloadListener transferListener;
        private TransferState transferState;

        private void setTransferState(TransferState state) {
            transferState = state;
            if (transferListener != null) {
                transferListener.onStateChanged(0, state);
            }
        }

        // TransferObserver(int var1, TransferDBUtil var2, Cursor var3) {
        //     this.id = var1;
        //     this.dbUtil = var2;
        //     this.updateFromDB(var3);
        // }

        // TransferObserver(int var1, TransferDBUtil var2, String var3, String var4, File var5) {
        //     this.id = var1;
        //     this.dbUtil = var2;
        //     this.bucket = var3;
        //     this.key = var4;
        //     this.filePath = var5.getAbsolutePath();
        //     this.bytesTotal = var5.length();
        //     this.transferState = TransferState.WAITING;
        // }

        // private void updateFromDB(Cursor var1) {
        // this.bucket = var1.getString(var1.getColumnIndexOrThrow("bucket_name"));
        // this.key = var1.getString(var1.getColumnIndexOrThrow("key"));
        // this.bytesTotal = var1.getLong(var1.getColumnIndexOrThrow("bytes_total"));
        // this.bytesTransferred = var1.getLong(var1.getColumnIndexOrThrow("bytes_current"));
        // this.transferState = TransferState.getState(var1.getString(var1.getColumnIndexOrThrow("state")));
        // this.filePath = var1.getString(var1.getColumnIndexOrThrow("file"));
        // }

        public void setDownloadListener(DownloadListener param1) {
            synchronized (this) {
                transferListener = param1;
                if (transferState != null) {
                    transferListener.onStateChanged(0, transferState);
                }
            }

        }

        public void cleanTransferListener() {
            synchronized(this){
                transferListener = null;
            }
            //
            // Throwable var10000;
            // boolean var10001;
            // label196: {
            //     try {
            //         if (this.transferListener != null) {
            //             TransferStatusUpdater.unregisterListener(this.id, this.transferListener);
            //             this.transferListener = null;
            //         }
            //     } catch (Throwable var21) {
            //         var10000 = var21;
            //         var10001 = false;
            //         break label196;
            //     }
            //
            //     try {
            //         if (this.statusListener != null) {
            //             TransferStatusUpdater.unregisterListener(this.id, this.statusListener);
            //             this.statusListener = null;
            //         }
            //     } catch (Throwable var20) {
            //         var10000 = var20;
            //         var10001 = false;
            //         break label196;
            //     }
            //
            //     label186:
            //     try {
            //         return;
            //     } catch (Throwable var19) {
            //         var10000 = var19;
            //         var10001 = false;
            //         break label186;
            //     }
            // }
            //
            // while(true) {
            //     Throwable var1 = var10000;
            //
            //     try {
            //         throw var1;
            //     } catch (Throwable var18) {
            //         var10000 = var18;
            //         var10001 = false;
            //         continue;
            //     }
            // }
        }

        public String getAbsoluteFilePath() {
            return this.filePath;
        }

        public String getBucket() {
            return this.bucket;
        }

        public long getBytesTotal() {
            return this.bytesTotal;
        }

        public long getBytesTransferred() {
            return this.bytesTransferred;
        }

        // public int getId() {
        //     return this.id;
        // }

        public String getKey() {
            return this.key;
        }

        public TransferState getState() {
            return this.transferState;
        }

        public void refresh() {
            // Cursor var1 = this.dbUtil.queryTransferById(this.id);
            //
            // try {
            //     if (var1.moveToFirst()) {
            //         this.updateFromDB(var1);
            //     }
            // } finally {
            //     var1.close();
            // }

        }

        private class TransferStatusListener {
            private TransferStatusListener() {
            }

            public void onError(int var1, Exception var2) {
            }

            public void onProgressChanged(int var1, long bytesTransferred, long bytesTotal) {
                // TransferObserver.this.bytesTransferred = bytesTransferred;
                // TransferObserver.this.bytesTotal = bytesTotal;
            }

            public void onStateChanged(int var1, TransferState var2) {
                // TransferObserver.this.transferState = var2;
            }
        }
    }
}
