package com.navdy.client.ota.impl;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build.VERSION;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.google.gson.Gson;
import com.here.android.mpa.common.ApplicationContext;
import com.navdy.client.BuildConfig;
import com.navdy.client.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.file.FileTransferManager.FileTransferListener;
import com.navdy.client.debug.file.RemoteFileTransferManager;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.debug.util.S3Constants.BuildSource;
import com.navdy.client.ota.OTAUpdateListener;
import com.navdy.client.ota.OTAUpdateListener.CheckOTAUpdateResult;
import com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus;
import com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus;
import com.navdy.client.ota.OTAUpdateManager;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.client.ota.model.UpdateInfo;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import com.vdurmont.semver4j.Semver;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabTag;
// import org.gitlab4j.api.GitLabApi;
// import org.gitlab4j.api.GitLabApiException;
// import org.gitlab4j.api.models.Project;
// import org.gitlab4j.api.models.Tag;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

import static com.navdy.client.ota.impl.DownloadFileFromURL.*;
import static org.droidparts.Injector.getApplicationContext;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public class OTAUpdateManagerImpl extends Binder implements OTAUpdateManager {
    @DexIgnore
    public static final int S3_CONNECTION_TIMEOUT = 15000;
    @DexIgnore
    public static final int S3_DOWNLOAD_TIMEOUT = 60000;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new Logger(OTAUpdateManagerImpl.class);
    @DexIgnore
    protected AmazonS3Client mClient;
    @DexIgnore
    private Context mContext;
    @DexIgnore
    protected AtomicBoolean mDownloadAborted = new AtomicBoolean(false);
    @DexAdd
    protected DownloadFileFromURL.TransferObserver mDownloadMonitor;
    @DexIgnore
    private RemoteFileTransferManager mRemoteFileTransferManager;
    @DexIgnore
    protected TransferUtility mTransferUtility;
    @DexIgnore
    private OTAUpdateListener mUpdateListener;
    @DexIgnore
    protected AtomicBoolean mUploadAborted = new AtomicBoolean(false);

    @DexAdd
    static String gitlab_project_url = "https://gitlab.com/alelec/navdy/alelec_navdy_client";
    @DexAdd
    static int gitlab_project_id = 9565076; // https://gitlab.com/alelec/navdy/alelec_navdy_client

    @DexAdd
    String available_update_url;

    @DexIgnore
    OTAUpdateManagerImpl() {
    }


    @DexReplace
    public OTAUpdateManagerImpl(Context context, OTAUpdateListener listener) {
       this.mContext = context;
       this.mUpdateListener = listener;
       if (context == null) {
           throw new IllegalArgumentException("Context cannot be null");
       }
    }

    @DexReplace
    public static AmazonS3Client createS3Client() {
//        AWSCredentials credentials = new BasicAWSCredentials(CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_aws_account_id)), CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_aws_secret)));
//        ClientConfiguration config = new ClientConfiguration();
//        config.setConnectionTimeout(15000);
//        config.setSocketTimeout(60000);
//        return new AmazonS3Client(credentials, config);
        return null;
    }

    @DexReplace
    public void checkForUpdate(int currentIncrementalVersion, boolean forceFullUpdate) {
        try {
            GitlabAPI gitlabAPI = GitlabAPI.connect("https://gitlab.com", "");
            List<GitlabTag> tags = gitlabAPI.getTags(gitlab_project_id);

            Semver latest_versions = null;
            GitlabTag latest_tag = null;
            for (GitlabTag tag: tags) {
                try {
                    Semver sem = new Semver(tag.getName().replace("v", ""), Semver.SemverType.LOOSE);
                    if ((latest_versions == null) || (sem.isGreaterThan(latest_versions))) {
                        latest_versions = sem;
                        latest_tag = tag;
                    }
                } catch (Exception ignored) {}
            }
            if (latest_versions == null) {
                sLogger.e("Could not query latest versio nfrom gitlab");
                return;
            }
            String current = HomescreenActivity.getAppVersionString().replace("v", "");

            // TODO Enable this to test OTA
            // current = "3.4";

            Semver current_version = new Semver(current, Semver.SemverType.LOOSE);

            int version = Integer.parseInt(latest_versions.getValue().replace(".", ""));
            while (version < 1000) {
                version *= 10;
            }

            if (!latest_versions.isGreaterThan(current_version)) {
                if (this.mUpdateListener != null) {

                    UpdateInfo latestUpdateInfo = new UpdateInfo();
                    latestUpdateInfo.description = latest_tag.getRelease().getDescription();
                    latestUpdateInfo.incremental = false;
                    latestUpdateInfo.url = available_update_url;
                    latestUpdateInfo.versionName = latest_versions.getValue();
                    latestUpdateInfo.version = version;

                    this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.UPTODATE, latestUpdateInfo);
                    return;
                }
            }

            sLogger.i("We have a winner!");
            Pattern urls_pattern = Pattern.compile("\\((\\S+\\.apk)\\)");
            Matcher matcher = urls_pattern.matcher(latest_tag.getRelease().getDescription());

            String release_url = null;
            String debug_url = null;

            while (matcher.find()) {
                String url = matcher.group(1);
                if (url.contains("debug")) {
                    if (debug_url != null) {
                        sLogger.e("Found multiple debug urls");
                    }
                    debug_url = gitlab_project_url + url;
                } else {
                    if (release_url != null) {
                        sLogger.e("Found multiple release urls");
                    }
                    release_url = gitlab_project_url + url;
                }
            }

            if (BuildConfig.DEBUG) {
                available_update_url = debug_url;
            } else {
                available_update_url = release_url;
            }

            if (available_update_url != null) {
                if (this.mUpdateListener != null) {
                    UpdateInfo latestUpdateInfo = new UpdateInfo();
                    latestUpdateInfo.description = latest_tag.getRelease().getDescription();
                    latestUpdateInfo.incremental = false;
                    latestUpdateInfo.url = available_update_url;
                    latestUpdateInfo.versionName = latest_versions.getValue();
                    latestUpdateInfo.version = version;

                    HttpURLConnection con = null;
                    int length = 0;
                    try {
                        con = (HttpURLConnection) new URL(available_update_url).openConnection();
                        con.setInstanceFollowRedirects(true);
                        con.setRequestMethod("HEAD");
                        String head = con.getResponseMessage();
                        String slength = con.getHeaderField("Content-Length");
                        length = Integer.parseInt(slength);
                    } catch (Throwable ex) {
                        if (con != null) {
                            con.disconnect();
                        }
                    }
                    latestUpdateInfo.size = length;

                    this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.AVAILABLE, latestUpdateInfo);
                    return;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
           if (this.mUpdateListener != null) {
               this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.SERVER_ERROR, null);
               return;
           }

        }

               // sLogger.d("Checking for OTA update , current version :" + currentIncrementalVersion);
//        if (SystemUtils.isConnectedToNetwork(this.mContext)) {
//            UpdateInfo incrementalUpdateInfo = null;
//            BuildSource buildSource = getOtaTopLevelFolderName();
//            if (!(forceFullUpdate || currentIncrementalVersion == -1)) {
//                String fileNameForIncrementalVersion = buildSource.getPrefix() + S3Constants.S3_FILE_DELIMITER + S3Constants.LOOKUP_FOLDER_PREFIX + S3Constants.S3_FILE_DELIMITER + currentIncrementalVersion + S3Constants.S3_FILE_DELIMITER + S3Constants.OTA_LOOKUP_FILE_NAME;
//                try {
//                    incrementalUpdateInfo = fetchUpdateInfo(buildSource.getBucket(), fileNameForIncrementalVersion);
//                } catch (Exception e) {
//                    if ((e instanceof AmazonS3Exception) && ((AmazonS3Exception) e).getStatusCode() == 404) {
//                        sLogger.v("File does not exist in Amazon S3: " + fileNameForIncrementalVersion);
//                    } else {
//                        sLogger.e("Server error while checking for OTA update, current version :" + currentIncrementalVersion + " " + e);
//                        if (this.mUpdateListener != null) {
//                            this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.SERVER_ERROR, null);
//                            return;
//                        }
//                        return;
//                    }
//                } catch (Throwable t) {
//                    sLogger.e("Bad listener ", t);
//                    return;
//                }
//            }
//            UpdateInfo latestUpdateInfo = null;
//            try {
//                latestUpdateInfo = fetchUpdateInfo(buildSource.getBucket(), buildSource.getPrefix() + S3Constants.S3_FILE_DELIMITER + S3Constants.LOOKUP_FOLDER_PREFIX + S3Constants.S3_FILE_DELIMITER + S3Constants.OTA_LOOKUP_FILE_NAME);
//            } catch (Exception e2) {
//                sLogger.e("Server error while checking for OTA update, current version :" + currentIncrementalVersion);
//                if (this.mUpdateListener != null) {
//                    this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.SERVER_ERROR, null);
//                }
//            } catch (Throwable t2) {
//                sLogger.e("Bad listener ", t2);
//            }
//            String metaData = null;
//            if (latestUpdateInfo != null) {
//                String metaDataFileName = buildSource.getPrefix() + S3Constants.S3_FILE_DELIMITER + String.format(S3Constants.OTA_RELEASE_NOTES_FILE_NAME_FORMAT, new Object[]{Integer.valueOf(latestUpdateInfo.version)});
//                sLogger.d("Fetching the meta data from :" + metaDataFileName);
//                metaData = fetchMetaData(buildSource.getBucket(), metaDataFileName);
//            }
//            if (latestUpdateInfo == null || currentIncrementalVersion >= latestUpdateInfo.version) {
//                if (latestUpdateInfo == null) {
//                    sLogger.e("Couldn't retrieve the latest info data (latestUpdateInfo == null)");
//                }
//                if (this.mUpdateListener != null) {
//                    sLogger.d("S/w up to date");
//                    try {
//                        if (!StringUtils.isEmptyAfterTrim(metaData)) {
//                            latestUpdateInfo.metaData = metaData;
//                        }
//                        this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.UPTODATE, latestUpdateInfo);
//                        return;
//                    } catch (Throwable t22) {
//                        sLogger.e("Bad listener ", t22);
//                        return;
//                    }
//                }
//                return;
//            } else if (incrementalUpdateInfo == null || incrementalUpdateInfo.version != latestUpdateInfo.version) {
//                sLogger.d("A full update is available : " + latestUpdateInfo.version);
//                if (!StringUtils.isEmptyAfterTrim(metaData)) {
//                    latestUpdateInfo.metaData = metaData;
//                }
//                try {
//                    this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.AVAILABLE, latestUpdateInfo);
//                    return;
//                } catch (Throwable t222) {
//                    sLogger.e("Bad listener ", t222);
//                    return;
//                }
//            } else {
//                sLogger.d("An incremental update is available, from :" + currentIncrementalVersion + " to:" + incrementalUpdateInfo.version);
//                try {
//                    incrementalUpdateInfo.incremental = true;
//                    incrementalUpdateInfo.fromVersion = currentIncrementalVersion;
//                    if (!StringUtils.isEmptyAfterTrim(metaData)) {
//                        incrementalUpdateInfo.metaData = metaData;
//                    }
//                    this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.AVAILABLE, incrementalUpdateInfo);
//                    return;
//                } catch (Throwable t2222) {
//                    sLogger.d("Bad listener ", t2222);
//                    return;
//                }
//            }
//        }
//        sLogger.d("No connectivity for checking OTA update");
//        try {
//            if (this.mUpdateListener != null) {
//                this.mUpdateListener.onCheckForUpdateFinished(CheckOTAUpdateResult.NO_CONNECTIVITY, null);
//            }
//        } catch (Throwable t22222) {
//            sLogger.e("Bad listener", t22222);
//        }
    }

//    private BuildSource getOtaTopLevelFolderName() {
//        BuildSource buildSource = BuildSource.STABLE;
//        if (SettingsUtils.getSharedPreferences() != null) {
//            return OTAUpdateService.getBuildSource();
//        }
//        return buildSource;
//    }

    @DexReplace
    public void download(final UpdateInfo info, File outputFile, int downloadId) {
       sLogger.d("Downloading the update..");
       try {
           this.mDownloadAborted.set(false);
           if (info == null || info.version <= 0 || info.url == null || info.url.equals("")) {
               throw new IllegalArgumentException("Update information is not valid");
           }
           // this.mTransferUtility = new TransferUtility(this.mClient, this.mContext);

           DownloadFileFromURL downloader = new DownloadFileFromURL();
           this.mDownloadMonitor = downloader.observer;

           downloader.start(info.url, outputFile.getAbsolutePath());
           downloadId = info.url.hashCode();

           // boolean recordFound = false;
           // if (downloadId != -1) {
               if (this.mDownloadMonitor != null) {
                   if (this.mDownloadMonitor.getState() == TransferState.COMPLETED) {
                       sLogger.d("Download completed successfully");
                       this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.COMPLETED, downloadId, 0, 0);
                       return;

                   } else if (this.mDownloadMonitor.getState() == TransferState.PAUSED) {

                       throw new Exception("Not Implemented");

                       // this.mDownloadMonitor = this.mTransferUtility.resume(downloadId);
                       // if (this.mDownloadMonitor != null) {
                       //     downloadId = this.mDownloadMonitor.getId();
                       //     recordFound = true;
                       // }
                   }
               }
           // }
           // if (!recordFound) {
           //     String[] parsedResult = parseS3Url(info.url);
           //     String bucket = parsedResult[0];
           //     String key = parsedResult[1];
           //     if (bucket == null || bucket.equals("") || key == null || key.equals("")) {
           //         throw new IllegalArgumentException("Not a valid url");
           //     }
           //     this.mDownloadMonitor = this.mTransferUtility.download(bucket, key, outputFile);
           //     downloadId = this.mDownloadMonitor.getId();
           // }
           if (this.mDownloadMonitor != null) {
               this.mDownloadMonitor.setDownloadListener(new OtaDownloadListener(info));
           }
       } catch (Throwable th) {
           if (this.mUpdateListener != null) {
               long size = 0;
               if (info != null) {
                   size = info.size;
               }
               this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.DOWNLOAD_FAILED, downloadId, 0, size);
           }
       }
    }

    @DexAdd
    public void installAPK(File updateFile) {
        // Intent intent = new Intent(Intent.ACTION_VIEW);
        // intent.addCategory("android.intent.category.DEFAULT");
        // intent.setDataAndType(Uri.fromFile(updateFile), "application/vnd.android.package-archive");
        // NavdyApplication.getAppContext().startActivity(intent, Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri;
        if (VERSION.SDK_INT >= 23) {
            Context context = NavdyApplication.getAppContext();
            uri = FileProvider.getUriForFile(context, "com.navdy.client.provider", updateFile);
        } else {
            uri = Uri.fromFile(updateFile);
        }


        Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
        intent.setData( uri );
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    @DexReplace
    public void uploadToHUD(File updateFile, long offset, String file) {
//        sLogger.d("Uploading the update file to HUD");
//        this.mUploadAborted.set(false);
//        final long total = updateFile.length();
//        this.mRemoteFileTransferManager = new RemoteFileTransferManager(this.mContext, updateFile, FileType.FILE_TYPE_OTA, file, new FileTransferListener() {
//            public void onFileTransferResponse(FileTransferResponse response) {
//                OTAUpdateManagerImpl.sLogger.d("Response " + response);
//                if (OTAUpdateManagerImpl.this.mUploadAborted.get()) {
//                    OTAUpdateManagerImpl.sLogger.d("onFileTransferResponse: Upload is already aborted , so ignoring the response");
//                } else if (!response.success.booleanValue()) {
//                    try {
//                        OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(UploadToHUDStatus.UPLOAD_FAILED, 0, total, response.error);
//                    } catch (Throwable t) {
//                        OTAUpdateManagerImpl.sLogger.d("Bad listener ", t);
//                    }
//                }
//            }
//
//            public void onFileTransferStatus(FileTransferStatus status) {
//                OTAUpdateManagerImpl.sLogger.d("Status " + status);
//                if (OTAUpdateManagerImpl.this.mUploadAborted.get()) {
//                    OTAUpdateManagerImpl.sLogger.d("onFileTransferStatus: Upload is already aborted , so ignoring the status");
//                    return;
//                }
//                try {
//                    if (!status.success.booleanValue()) {
//                        OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(UploadToHUDStatus.UPLOAD_FAILED, 0, total, status.error);
//                    } else if (status.transferComplete.booleanValue()) {
//                        OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(UploadToHUDStatus.COMPLETED, total, total, null);
//                    } else {
//                        OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(UploadToHUDStatus.UPLOADING, status.totalBytesTransferred.longValue(), total, null);
//                    }
//                } catch (Throwable t) {
//                    OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
//                }
//            }
//
//            public void onError(FileTransferError errorCode, String error) {
//                OTAUpdateManagerImpl.sLogger.e("Transfer Error :" + error);
//                if (OTAUpdateManagerImpl.this.mUploadAborted.get()) {
//                    OTAUpdateManagerImpl.sLogger.d("onFileTransferError: Upload is already aborted , so ignoring the error");
//                    return;
//                }
//                try {
//                    OTAUpdateManagerImpl.this.mUpdateListener.onUploadProgress(UploadToHUDStatus.UPLOAD_FAILED, 0, 0, errorCode);
//                } catch (Throwable t) {
//                    OTAUpdateManagerImpl.sLogger.e("Bad listener :", t);
//                }
//            }
//        });
//        if (offset > 0) {
//            this.mUpdateListener.onUploadProgress(UploadToHUDStatus.UPLOADING, offset, total, null);
//            this.mRemoteFileTransferManager.sendFile(offset);
//            return;
//        }
//        this.mRemoteFileTransferManager.sendFile();
    }

    @DexReplace
    public void abortDownload() {
//        sLogger.d("Download aborted");
//        if (this.mDownloadAborted.compareAndSet(false, true) && this.mDownloadMonitor != null) {
//            try {
//                this.mTransferUtility.cancel(this.mDownloadMonitor.getId());
//            } catch (Throwable t) {
//                sLogger.d("Error aborting the download", t);
//            }
//        }
    }

    @DexReplace
    public void abortUpload() {
//        sLogger.d("abortUpload: " + this.mUploadAborted.get());
//        if (!this.mUploadAborted.compareAndSet(false, true)) {
//            sLogger.e("Cannot abort upload as it is already aborted");
//        } else if (this.mRemoteFileTransferManager != null) {
//            this.mRemoteFileTransferManager.cancelFileUpload();
//        }
    }

//    private static String[] parseS3Url(String url) throws IllegalArgumentException {
//        if (url == null || !url.startsWith(S3Constants.S3_PROTOCOL)) {
//            throw new IllegalArgumentException("Invalid url");
//        }
//        String shortForm = url.substring(S3Constants.S3_PROTOCOL.length());
//        String bucketName = shortForm.substring(0, shortForm.indexOf(S3Constants.S3_FILE_DELIMITER));
//        String key = shortForm.substring(shortForm.indexOf(S3Constants.S3_FILE_DELIMITER) + 1);
//        return new String[]{bucketName, key};
//    }
//
//    private UpdateInfo fetchUpdateInfo(String bucket, String fileName) throws Exception {
//        String jsonString = IOUtils.convertInputStreamToString(this.mClient.getObject(bucket, fileName).getObjectContent(), "UTF-8");
//        sLogger.v("Fetched update info: " + jsonString);
//        return (UpdateInfo) new Gson().fromJson(jsonString, UpdateInfo.class);
//    }
//
//    private String fetchMetaData(String bucket, String fileName) {
//        try {
//            return IOUtils.convertInputStreamToString(this.mClient.getObject(bucket, fileName).getObjectContent(), "UTF-8");
//        } catch (Throwable t) {
//            sLogger.e("Error while reading the meta data ", t);
//            return null;
//        }
//    }

   @DexAdd
    private class OtaDownloadListener implements DownloadListener {
       private final UpdateInfo info;
       boolean mStarted;

       public OtaDownloadListener(UpdateInfo info) {
           this.info = info;
           mStarted = false;
       }

       public void onStateChanged(final int id, final TransferState state) {
           if (!(state == TransferState.COMPLETED || state == TransferState.IN_PROGRESS || state == TransferState.WAITING || state == TransferState.PAUSED)) {
               OTAUpdateManagerImpl.this.mTransferUtility.cancel(id);
           }
           if (!OTAUpdateManagerImpl.this.mDownloadAborted.get()) {
               TaskManager.getInstance().execute(new Runnable() {
                   public void run() {
                       if (!OtaDownloadListener.this.mStarted) {
                           OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.STARTED, id, 0, 0);
                           OtaDownloadListener.this.mStarted = true;
                       }
                       try {
                           switch (state) {
                               case COMPLETED:
                                   OTAUpdateManagerImpl.sLogger.d("Download completed successfully");
                                   OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.COMPLETED, id, 0, 0);
                                   return;
                               case FAILED:
                                   OTAUpdateManagerImpl.sLogger.d("Download failed");
                                   OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.DOWNLOAD_FAILED, id, 0, info.size);
                                   return;
                               case PAUSED:
                                   OTAUpdateManagerImpl.sLogger.d("Download failed");
                                   OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.PAUSED, id, 0, 0);
                                   return;
                               case IN_PROGRESS:
                                   OTAUpdateManagerImpl.sLogger.d("Download in progress");
                                   return;
                               default:
                                   OTAUpdateManagerImpl.sLogger.e("Unknown state: " + state);
                                   return;
                           }
                       } catch (Throwable t) {
                           OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
                       }
                   }
               }, 2);
           }
       }

       public void onProgressChanged(final int id, final long bytesCurrent, long bytesTotal) {
           if (!OTAUpdateManagerImpl.this.mDownloadAborted.get()) {
               TaskManager.getInstance().execute(new Runnable() {
                   public void run() {
                       try {
                           OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.DOWNLOADING, id, bytesCurrent, info.size);
                       } catch (Throwable t) {
                           OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
                       }
                   }
               }, 2);
           }
       }

       public void onError(final int id, Exception ex) {
           OTAUpdateManagerImpl.sLogger.e("OTA on error: ", ex.fillInStackTrace());
           if (!OTAUpdateManagerImpl.this.mDownloadAborted.get()) {
               TaskManager.getInstance().execute(new Runnable() {
                   public void run() {
                       try {
                           OTAUpdateManagerImpl.this.mUpdateListener.onDownloadProgress(DownloadUpdateStatus.DOWNLOAD_FAILED, id, 0, info.size);
                       } catch (Throwable t) {
                           OTAUpdateManagerImpl.sLogger.e("Bad listener ", t);
                       }
                   }
               }, 2);
           }
       }
   }
}
