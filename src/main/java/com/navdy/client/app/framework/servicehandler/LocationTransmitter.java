package com.navdy.client.app.framework.servicehandler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
// import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;

import com.alelec.navdyclient.BuildConfig;
// import com.mapzen.android.lost.api.LocationListener;
// import com.mapzen.android.lost.api.LocationRequest;
// import com.mapzen.android.lost.api.LocationServices;
// import com.mapzen.android.lost.api.LostApiClient;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import android.location.LocationListener;

import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.CrashlyticsAppender;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.connection.Connection;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;

import java.util.List;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
// public class LocationTransmitter implements LostApiClient.ConnectionCallbacks, LocationListener {
public class LocationTransmitter extends LocationCallback implements LocationListener {
    @DexIgnore
    public static Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.LocationTransmitter.class);
    @DexIgnore
    protected boolean isRunning;
    @DexIgnore
    protected boolean mConnected;
    @DexIgnore
    protected final Context mContext;
    @DexIgnore
    private LocationListener mLocationListener; // = new com.navdy.client.app.framework.servicehandler.LocationTransmitter.Anon1();
    @DexIgnore
    protected LocationManager mLocationManager;
    @DexIgnore
    protected final Looper mLocationReceiverLooper;
    @DexIgnore
    protected final RemoteDevice mRemoteDevice;

    // @DexAdd
    // private LostApiClient lostApiClient;
    @DexAdd
    protected FusedLocationProviderClient mFusedLocationManager;
    @DexAdd
    protected HomescreenActivity mHomescreenActivity;


    @DexReplace
    class Anon1 implements LocationListener {
        Anon1() {
        }

        public void onLocationChanged(Location location) {
            LocationTransmitter.this.transmitLocation(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            LocationTransmitter.sLogger.v("status: " + provider + CrashlyticsAppender.SEPARATOR + status);
        }

        public void onProviderEnabled(String provider) {
            LocationTransmitter.sLogger.e("enabled: " + provider);
        }

        public void onProviderDisabled(String provider) {
            LocationTransmitter.sLogger.e("disabled: " + provider);
        }
    }

    @DexReplace
    class Anon2 implements RemoteDevice.Listener {
        Anon2() {
        }

        public void onDeviceConnecting(RemoteDevice device) {
        }

        public void onDeviceConnected(RemoteDevice device) {
            LocationTransmitter.this.mConnected = true;
        }

        public void onDeviceDisconnected(RemoteDevice device, Connection.DisconnectCause cause) {
            LocationTransmitter.this.mConnected = false;
        }

        public void onDeviceConnectFailure(RemoteDevice device, Connection.ConnectionFailureCause cause) {
        }

        public void onNavdyEventReceived(RemoteDevice device, NavdyEvent event) {
        }

        public void onNavdyEventReceived(RemoteDevice device, byte[] event) {
        }
    }

    @DexEdit
    public LocationTransmitter(Context context, RemoteDevice remoteDevice, @DexIgnore Void tag) {
        HandlerThread handlerThread = new HandlerThread("location-transmitter");
        handlerThread.start();
        this.mLocationReceiverLooper = handlerThread.getLooper();
        this.mContext = context;
        this.mRemoteDevice = remoteDevice;
        remoteDevice.addListener(new LocationTransmitter.Anon2());
        this.mConnected = this.mRemoteDevice.isConnected();
        this.mLocationManager = (LocationManager) this.mContext.getSystemService(Context.LOCATION_SERVICE);
    }

    @DexAdd
    public LocationTransmitter(Context context, RemoteDevice remoteDevice) {
        this(context, remoteDevice, (Void) null);
        // lostApiClient = new LostApiClient.Builder(context)
        //         .addConnectionCallbacks(this)
        //         .build();
        // lostApiClient.connect();
        this.mFusedLocationManager = LocationServices.getFusedLocationProviderClient(this.mContext);
    }

    // @DexAdd
    // public Location getLastKnownLocation() {
    //     if (NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_GRANTED) {
    //         if (lostApiClient.isConnected()) {
    //             return LocationServices.FusedLocationApi.getLastLocation(lostApiClient);
    //         }
    //     }
    //     return null;
    // }

    // @DexAdd
    // @Override
    // public void onConnected() {
    //
    //     if (NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_GRANTED) {
    //         LocationRequest request = LocationRequest.create()
    //             .setInterval(300)
    //             .setSmallestDisplacement(0)
    //             .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    //         LocationServices.FusedLocationApi.requestLocationUpdates(lostApiClient, request, (LocationListener) this);
    //         request = LocationRequest.create()
    //             .setInterval(2000)
    //             .setSmallestDisplacement(0)
    //             .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    //         LocationServices.FusedLocationApi.requestLocationUpdates(lostApiClient, request, (LocationListener) this);
    //         if (mConnected) {
    //             sendLastLocation();
    //         }
    //     }
    // }

    // @Override
    // @DexAdd
    // public void onConnectionSuspended() {
    //
    // }

    @DexAdd
    @Override
    public void onLocationResult(LocationResult locationResult) {
        LocationTransmitter.this.transmitLocation(locationResult.getLastLocation());
    }

    @DexAdd
    public void setHomescreenActivity(HomescreenActivity a) {
        mHomescreenActivity = a;
    }

    @DexAdd
    @Override
    public void onLocationChanged(Location location) {
        try {
            LocationTransmitter.this.transmitLocation(location);
        } catch (Throwable th) {
            sLogger.e("Can't transmit: ", th);
        }
    }

    @DexAdd
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        LocationTransmitter.sLogger.v("status: " + provider + CrashlyticsAppender.SEPARATOR + status);
    }

    @DexAdd
    @Override
    public void onProviderEnabled(String provider) {
        LocationTransmitter.sLogger.e("enabled: " + provider);
    }

    @DexAdd
    @Override
    public void onProviderDisabled(String provider) {
        LocationTransmitter.sLogger.e("disabled: " + provider);
    }

    @DexReplace
    protected boolean transmitLocation(Location androidLocation) {
        if (!isRunning) {
            return false;
        }
        if (!this.mConnected) {
            sLogger.e("Can't transmit: not connected");
            return false;
        } else if (androidLocation == null) {
            sLogger.e("Invalid location - null");
            return false;
        } else {
            if (BuildConfig.DEBUG && mHomescreenActivity != null) {
                mHomescreenActivity.pulseConnectionIndicator();
            }
            Coordinate coordinate = new Coordinate(androidLocation.getLatitude(), androidLocation.getLongitude(), androidLocation.getAccuracy(), androidLocation.getAltitude(), androidLocation.getBearing(), androidLocation.getSpeed(), System.currentTimeMillis(), androidLocation.getProvider());
            sLogger.d("sending coordinate: lat=" + coordinate.latitude + ", lng=" + coordinate.longitude);
            return this.mRemoteDevice.postEvent((Message) coordinate);
        }
    }

    @DexReplace
    public boolean start() {
        try {
            if (this.isRunning) {
                sLogger.i("location transmitter already started.");
                return false;
            } else if (NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_GRANTED) {
                SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
                boolean gpsUseFused = sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_GPS_USE_FUSED, false);
                if (!gpsUseFused) {
                    this.mLocationManager.requestLocationUpdates("gps", 0, 0.0f, this.mLocationListener, this.mLocationReceiverLooper);
                } else {
                    sLogger.v("starting location updates");
                    // if (lostApiClient.isConnected()) {
                    //     sendLastLocation();
                    // }
                    LocationRequest currentLocationRequest = new LocationRequest();
                    currentLocationRequest.setInterval(300)
                            .setFastestInterval(10)
                            .setMaxWaitTime(2000)
                            .setSmallestDisplacement(0)
                            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    this.mFusedLocationManager.requestLocationUpdates(currentLocationRequest, this,
                            this.mLocationReceiverLooper);
                }
                sendLastLocation();
                this.isRunning = true;

                return true;
            } else {
                sLogger.v("permission for ACCESS_FINE_LOCATION failed");
                return false;
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }

    @DexReplace
    public boolean stop() {
        if (!this.isRunning) {
            sLogger.e("location transmitter not running");
            return false;
        }
        try {
            if (NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == PackageManager.PERMISSION_GRANTED) {
                this.mLocationManager.removeUpdates(this.mLocationListener);
                // LocationServices.FusedLocationApi.removeLocationUpdates(lostApiClient, (com.mapzen.android.lost.api.LocationListener)this);
                // lostApiClient.disconnect();
                this.mFusedLocationManager.removeLocationUpdates(this);

            } else {
                sLogger.v("mLocationManager.removeUpdates failed. Permission for ACCESS_FINE_LOCATION was not found.");
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
        sLogger.v("stopping location updates");
        this.isRunning = false;
        return true;
    }

    @DexReplace
    private void sendLastLocation() {
        try {
            int permission = com.navdy.client.app.NavdyApplication.getAppContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
            android.location.Location lastLocation = null;
            java.util.List<java.lang.String> providers = this.mLocationManager.getAllProviders();
            if (permission == 0) {
                for (java.lang.String str : providers) {
                    android.location.Location l = this.mLocationManager.getLastKnownLocation(str);
                    if (l != null) {
                        if (lastLocation == null) {
                            lastLocation = l;
                        } else if (l.getTime() > lastLocation.getTime()) {
                            lastLocation = l;
                        }
                    }
                }
                if (lastLocation == null) {
                    sLogger.v("no last location to transmit");
                    return;
                }
                sLogger.v("transmit last location:" + lastLocation);
                transmitLocation(lastLocation);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
