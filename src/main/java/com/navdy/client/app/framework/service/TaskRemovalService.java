package com.navdy.client.app.framework.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.navdy.client.app.framework.map.HereMapsManager;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.service.library.log.Logger;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexRemove;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class TaskRemovalService extends android.support.v4.app.JobIntentService {
    @DexIgnore
    public static /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.service.TaskRemovalService.class);
    @DexIgnore
    private boolean startedByAppInstance;

    @DexReplace
    public int onStartCommand(Intent intent, int flags, int startId) {
        logger.i("Service started");
        this.startedByAppInstance = true;
        return super.onStartCommand(intent, flags, startId);
    }

    // @android.support.annotation.Nullable
    @DexRemove
    public IBinder onBind(Intent intent) {
        return null;
    }

    @DexAdd
    public static int JOB_ID = 2;

    // @DexAdd
    // public static void enqueueWork(Context context, Intent work) {
    //     JOB_ID = 2;
    //     enqueueWork(context, DestinationSuggestionService.class, JOB_ID, work);
    // }

    @DexAdd
    public void onHandleWork(@NonNull Intent intent) {

    }

    @DexReplace
    public void onTaskRemoved(Intent rootIntent) {
        logger.i("Task removed - started by app: " + this.startedByAppInstance);
        if (this.startedByAppInstance) {
            HereMapsManager.getInstance().killSelfAndMapEngine();
        }
        super.onTaskRemoved(rootIntent);
    }

    @DexReplace
    public static void startService(Context context) {
        // context.startService(new Intent(context, TaskRemovalService.class));
        JOB_ID = 2;
        enqueueWork(context, TaskRemovalService.class, JOB_ID, new Intent());
    }
}
