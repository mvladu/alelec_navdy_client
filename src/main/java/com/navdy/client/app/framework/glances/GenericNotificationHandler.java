package com.navdy.client.app.framework.glances;

import android.app.Notification;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
// import android.support.v7.app.NotificationCompat;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.KeyValue;

import java.util.ArrayList;
import java.util.List;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class GenericNotificationHandler {
    @DexReplace
    public static void handleGenericNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        if (GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            String title = getTitle(notification);
            String message = getBestAvailableText(notification);
            if (!StringUtils.isEmptyAfterTrim(message)) {
                String appName = "";
                try {
                    PackageManager pm = NavdyApplication.getAppContext().getPackageManager();
                    ApplicationInfo applicationInfo = pm.getApplicationInfo(packageName, 0);
                    appName = pm.getApplicationLabel(applicationInfo).toString();
                } catch (PackageManager.NameNotFoundException | NullPointerException e) {
                    e.printStackTrace();
                }

                String id = GlancesHelper.getId();
                List<KeyValue> data = new ArrayList<>();
                data.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), message));
                if (!TextUtils.isEmpty(appName)) {
                    data.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), appName + "\n" + title));
                }
                GlancesHelper.sendEvent(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).provider(packageName).id(id).postTime(notification.when).glanceData(data).build());
            }
        }
    }

    // @android.support.annotation.Nullable
    @DexReplace
    private static String getBestAvailableText(Notification notification) {
        CharSequence ticker = notification.tickerText;
        Bundle extras = NotificationCompat.getExtras(notification);
        if (extras != null) {
            String bigText = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.bigText");
            if (!StringUtils.isEmptyAfterTrim(bigText)) {
                return bigText;
            }
            String text = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
            if (!StringUtils.isEmptyAfterTrim(text)) {
                return text;
            }
            String subText = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.subText");
            if (!StringUtils.isEmptyAfterTrim(subText)) {
                return subText;
            }
            String title = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.title");
            if (!StringUtils.isEmptyAfterTrim(title)) {
                return title;
            }
        }
        if (ticker != null) {
            return ticker.toString();
        }
        return null;
    }

    @DexAdd
    private static String getTitle(Notification notification) {
        Bundle extras = NotificationCompat.getExtras(notification);
        if (extras != null) {
            String title = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.title");
            if (!StringUtils.isEmptyAfterTrim(title)) {
                return title;
            }
        }
        return "";
    }
}
