package com.navdy.client.app.framework.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.alelec.navdyclient.BuildConfig;
import com.navdy.client.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;

import java.util.Random;

import lanchon.dexpatcher.annotation.DexReplace;

@DexReplace
public class CredentialsUtils {
    private static final boolean VERBOSE = false;
    public static final Logger logger = new Logger(CredentialsUtils.class);
    private Context context = NavdyApplication.getAppContext();

    static String GOOGLE_API_KEY;

    static String here_maps_appid;
    static String here_maps_apptoken;
    static String here_maps_license_key;
    public static boolean fixed = false;

    public static String get_google_api_key(Context ctx) {
        synchronized (PackageManagerWrapper.class) {

            String[] api_keys = BuildConfig.mapsApiKeys.split(",");
            int sel = new Random().nextInt(api_keys.length);
            String api_key = api_keys[sel].trim();

            Log.i("GMaps.API","Using key " + api_key.substring(0,3) + "..." + api_key.substring(api_key.length()-3));

            return api_key;
        }
    }

    public static void get_here_api_key(Context ctx) {
        int sel = new Random().nextInt(2);
        switch (sel) {
           case 1:
               // logger.v("Using Here key 1");
               here_maps_appid = ctx.getResources().getString(com.alelec.navdyclient.R.string.here_maps_appid_2);
               here_maps_apptoken = ctx.getResources().getString(com.alelec.navdyclient.R.string.here_maps_apptoken_2);
               here_maps_license_key = ctx.getResources().getString(com.alelec.navdyclient.R.string.here_maps_license_key_2);
               break;

            case 0:
            default:
                here_maps_appid = ctx.getResources().getString(com.alelec.navdyclient.R.string.here_maps_appid_1);
                here_maps_apptoken = ctx.getResources().getString(com.alelec.navdyclient.R.string.here_maps_apptoken_1);
                here_maps_license_key = ctx.getResources().getString(com.alelec.navdyclient.R.string.here_maps_license_key_1);

                break;
        }
        Log.i("Here.API","Using Here key " + here_maps_appid.substring(0,3) + "..." + here_maps_appid.substring(here_maps_appid.length()-3));
    }

    static {
        GOOGLE_API_KEY = get_google_api_key(NavdyApplication.getAppContext());
        get_here_api_key(NavdyApplication.getAppContext());
    }

    @SuppressLint("WrongConstant")
    public static String getCredentials(String str) {
        String str2 = "";
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();

        // Google Key
        if (StringUtils.equalsOrBothEmptyAfterTrim(str, "com.google.android.geo.GOOGLE_API_KEY")) {
            String string = sharedPreferences.getString(SettingsConstants.GOOGLE_MAPS_API_KEY, null);
            if (!StringUtils.isEmptyAfterTrim(string)) {
                return string;
            } else {
                return GOOGLE_API_KEY;
            }
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(str, "GOOGLE_WEB_SERVICE_KEY")) {
            String string2 = sharedPreferences.getString(SettingsConstants.GOOGLE_SERVICES_API_KEY, null);
            if (!StringUtils.isEmptyAfterTrim(string2)) {
                return string2;
            } else {
                return GOOGLE_API_KEY;
            }
        }

        // HERE Keys
        if (StringUtils.equalsOrBothEmptyAfterTrim(str, "NAVIGATION_HERE_APPID")) {
            String string2 = sharedPreferences.getString(SettingsConstants.NAVIGATION_HERE_APPID, null);
            if (!StringUtils.isEmptyAfterTrim(string2)) {
                return string2;
            } else {
                fixed = true;
                return here_maps_appid;
            }
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(str, "NAVIGATION_HERE_APPCODE")) {
            String string2 = sharedPreferences.getString(SettingsConstants.NAVIGATION_HERE_APPCODE, null);
            if (!StringUtils.isEmptyAfterTrim(string2)) {
                return string2;
            } else {
                fixed = true;
                return here_maps_apptoken;
            }
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(str, "NAVIGATION_HERE_LICENCE")) {
            String string2 = sharedPreferences.getString(SettingsConstants.NAVIGATION_HERE_LICENCE, null);
            if (!StringUtils.isEmptyAfterTrim(string2)) {
                return string2;
            } else {
                fixed = true;
                return here_maps_license_key;
            }
        }

        try {
            return NavdyApplication.getAppContext().getPackageManager().getApplicationInfo(NavdyApplication.getAppContext().getPackageName(), 128).metaData.getString(str);
        } catch (NameNotFoundException e) {
            logger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
            return str2;
        } catch (NullPointerException e2) {
            logger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
            return str2;
        }
    }
}
