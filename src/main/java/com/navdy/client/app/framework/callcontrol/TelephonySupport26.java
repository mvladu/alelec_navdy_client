package com.navdy.client.app.framework.callcontrol;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.telecom.TelecomManager;

// import com.alelec.navdyclient.Manifest;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class TelephonySupport26 extends com.navdy.client.app.framework.callcontrol.TelephonySupport21 {
    @DexIgnore
    private static com.navdy.service.library.log.Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.callcontrol.TelephonySupport26.class);

    @DexIgnore
    public TelephonySupport26(android.content.Context context) {
        super(context);
    }

    @android.annotation.TargetApi(26)
    @DexReplace
    public void acceptRingingCall() {
        TelecomManager telecomManager = (TelecomManager) this.context.getSystemService(Context.TELECOM_SERVICE);
        if (telecomManager == null) {
            sLogger.e("Unable to handleCallAction. telecomManager is null");
            acceptRingingCallFallback();
            return;
        }

        try {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ANSWER_PHONE_CALLS) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.MODIFY_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        telecomManager.acceptRingingCall();
                        telecomManager.acceptRingingCall(0);
                    }
                } catch (Exception ex) {
                    sLogger.e("Unable to use the Telecom Manager directly.", ex);
                    telecomManager.getClass().getMethod("acceptRingingCall").invoke(telecomManager);
                }
            }
        } catch (Exception e) {
            sLogger.e("Unable to use the Telecom Manager directly.", e);
            acceptRingingCallFallback();
        }
    }

    @DexIgnore
    private void acceptRingingCallFallback() {
        super.acceptRingingCall();
    }

    @DexIgnore
    public void endCall() {
        if (!callTelephonyManagerMethod("endCall")) {
            super.endCall();
        }
    }

    @DexIgnore
    private boolean callTelephonyManagerMethod(java.lang.String methodName) {
        android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) this.context.getSystemService(Context.TELEPHONY_SERVICE);
        if (tm == null) {
            sLogger.e("Unable to get the Telephony Manager.");
            return false;
        }
        try {
            tm.getClass().getMethod(methodName).invoke(tm);
            return true;
        } catch (java.lang.Exception e) {
            sLogger.e("Unable to use the Telephony Manager directly.", e);
            return false;
        }
    }
}
