package com.navdy.client.app.framework.models;

import android.support.annotation.NonNull;

import com.amazonaws.services.s3.internal.Constants;
import com.navdy.client.app.framework.i18n.AddressUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.service.library.log.Logger;

import org.droidparts.contract.SQL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class GoogleTextSearchDestinationResult {
    @DexIgnore
    public static /* final */ String FORMATTED_PHONE_NUMBER; // = "formatted_phone_number";
    @DexIgnore
    public static /* final */ String INTERNATIONAL_PHONE_NUMBER; // = "formatted_phone_number";
    @DexIgnore
    static /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult.class);
    @DexIgnore
    public String address;
    @DexIgnore
    public String city;
    @DexIgnore
    public String country;
    @DexIgnore
    public String countryCode;
    @DexIgnore
    public double distance;
    @DexIgnore
    public String icon;
    @DexIgnore
    public String jsonString;
    @DexIgnore
    public String lat;
    @DexIgnore
    public String lng;
    @DexIgnore
    public String name;
    @DexIgnore
    public List<String> open_hours;
    @DexIgnore
    public boolean open_now;
    @DexIgnore
    public String phone;
    @DexIgnore
    public String place_id;
    @DexIgnore
    public String price_level;
    @DexIgnore
    public String rating;
    @DexIgnore
    public String state;
    @DexIgnore
    public String streetName;
    @DexIgnore
    public String streetNumber;
    @DexIgnore
    public String[] types;
    @DexIgnore
    public String url;
    @DexIgnore
    public String zipCode;

    @DexIgnore
    public static GoogleTextSearchDestinationResult createDestinationObject(String jsonString2) {
        try {
            return new GoogleTextSearchDestinationResult(new JSONObject(jsonString2));
        } catch (JSONException e) {
            logger.e("Unable to parse the json string for a Google text search result: " + jsonString2, e);
            return null;
        }
    }

    @DexIgnore
    public static GoogleTextSearchDestinationResult createDestinationObject(JSONObject jsonObject) {
        try {
            return new GoogleTextSearchDestinationResult(jsonObject);
        } catch (JSONException e) {
            logger.e("Unable to parse the json object for a Google text search result: " + jsonObject, e);
            return null;
        }
    }

    @DexReplace
    private GoogleTextSearchDestinationResult(JSONObject jsonObject) throws JSONException {
        this.jsonString = jsonObject.toString();
        if (jsonObject.has("name")) {
            this.name = jsonObject.getString("name");
        } else if (jsonObject.has("terms")) {
            JSONArray terms = jsonObject.getJSONArray("terms");
            this.name = terms.getJSONObject(0).getString("value");
            ArrayList<String> addressParts = new ArrayList<>();
            for (int i = 1; i < terms.length(); i++) {
                addressParts.add(terms.getJSONObject(i).getString("value"));
            }
            this.address = StringUtils.join(addressParts, SQL.DDL.SEPARATOR);
        } else if (jsonObject.has("description")) {
            this.name = jsonObject.getString("description");
        }
        if (jsonObject.has("geometry")) {
            JSONObject location = jsonObject.getJSONObject("geometry").getJSONObject("location");
            this.lat = location.getString("lat");
            this.lng = location.getString("lng");
        }
        if (jsonObject.has("routes")) {
            JSONArray results = (JSONArray) jsonObject.get("routes");
            logger.d("Routes: " + (results != null ? results.toString() : Constants.NULL_VERSION_ID));
            if (results != null && results.length() > 0) {
                JSONObject firstRoute = results.getJSONObject(0);
                if (firstRoute != null) {
                    JSONArray legs = (JSONArray) firstRoute.get("legs");
                    if (legs != null && legs.length() > 0) {
                        JSONObject lastLeg = legs.getJSONObject(legs.length() - 1);
                        if (lastLeg != null) {
                            JSONObject endLocation = lastLeg.getJSONObject("end_location");
                            if (endLocation != null) {
                                String endLocationLng = endLocation.getString("lng");
                                String endLocationLat = endLocation.getString("lat");
                                logger.d("Received endLocation from the last leg: " + endLocationLat + SQL.DDL.SEPARATOR + endLocationLng);
                                if (!StringUtils.isEmptyAfterTrim(endLocationLat) && !StringUtils.isEmptyAfterTrim(endLocationLng)) {
                                    this.lat = endLocationLat;
                                    this.lng = endLocationLng;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (jsonObject.has("lat") && jsonObject.has("lng")) {
            this.lat = jsonObject.getString("lat");
            this.lng = jsonObject.getString("lng");
        }
        if (jsonObject.has("opening_hours")) {
            this.open_hours = new ArrayList<>();
            JSONObject opening_hours = jsonObject.getJSONObject("opening_hours");
            this.open_now = Boolean.valueOf(opening_hours.getString("open_now"));
            if (jsonObject.has("weekday_text")) {
                JSONArray weekday_text = opening_hours.getJSONArray("weekday_text");
                if (weekday_text.length() > 0) {
                    for (int i2 = 0; i2 < weekday_text.length(); i2++) {
                        String dayOfWeek = weekday_text.getString(i2);
                        if (dayOfWeek != null) {
                            this.open_hours.add(dayOfWeek);
                            logger.v("day of week: " + dayOfWeek);
                        }
                    }
                }
            }
        }
        if (jsonObject.has("formatted_address")) {
            this.address = jsonObject.getString("formatted_address");
        } else if (jsonObject.has("vicinity")) {
            this.address = jsonObject.getString("vicinity");
        }
        if (AddressUtils.isTitleIsInTheAddress(this.name, this.address)) {
            this.name = "";
        }
        JSONArray addressComponents = jsonObject.optJSONArray("address_components");
        if (addressComponents != null) {
            for (int aci = 0; aci < addressComponents.length(); aci++) {
                try {
                    JSONObject obj = addressComponents.getJSONObject(aci);
                    String longName = obj.getString("long_name");
                    JSONArray types2 = obj.getJSONArray("types");
                    int ti = 0;
                    while (true) {
                        if (ti >= types2.length()) {
                            break;
                        }
                        String type = types2.getString(ti);
                        if (NavdyContentProviderConstants.DESTINATIONS_STREET_NUMBER.equals(type)) {
                            this.streetNumber = longName;
                            break;
                        } else if ("route".equals(type)) {
                            this.streetName = longName;
                            break;
                        } else if ("locality".equals(type)) {
                            this.city = longName;
                            break;
                        } else if ("administrative_area_level_1".equals(type)) {
                            this.state = longName;
                            break;
                        } else if (NavdyContentProviderConstants.DESTINATIONS_COUNTRY.equals(type)) {
                            this.country = longName;
                            this.countryCode = obj.getString("short_name");
                            break;
                        } else if ("postal_code".equals(type)) {
                            this.zipCode = longName;
                            break;
                        } else {
                            ti++;
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        if (jsonObject.has("formatted_phone_number")) {
            this.phone = jsonObject.getString("formatted_phone_number");
        }
        if (jsonObject.has("website")) {
            this.url = jsonObject.getString("website");
        }
        if (jsonObject.has(SettingsJsonConstants.APP_ICON_KEY)) {
            this.icon = jsonObject.getString(SettingsJsonConstants.APP_ICON_KEY);
        }
        if (jsonObject.has(NavdyContentProviderConstants.DESTINATIONS_PLACE_ID)) {
            this.place_id = jsonObject.getString(NavdyContentProviderConstants.DESTINATIONS_PLACE_ID);
        }
        if (jsonObject.has("types")) {
            this.types = jsonObject.getJSONArray("types").toString().split(SQL.DDL.SEPARATOR);
        }
        if (jsonObject.has("price_level")) {
            this.price_level = jsonObject.getString("price_level");
        }
        if (jsonObject.has("rating")) {
            this.rating = jsonObject.getString("rating");
        }
        jsonObject.remove("reviews");
        jsonObject.remove("photos");
        jsonObject.remove("reference");
        this.jsonString = jsonObject.toString();
    }

    @NonNull
    @DexIgnore
    public String toString() {
        return String.format("Name: %s,\tLat: %s,\tLng: %s,\tAddress: %s,\tstreetNumber: %s,\tstreetName: %s,\tcity: %s,\tstate: %s,\tzipCode: %s,\tcountry: %s (%s),\tSuggestionType: %s,\tRating: %s,\tPrice Level: %s,\tPlace Id: %s,\tOpen Hours: %s,\tOpen Now: %s,\tPhone: %s,\tUrl: %s", this.name, this.lat, this.lng, this.address, this.streetNumber, this.streetName, this.city, this.state, this.zipCode, this.country, this.countryCode, Arrays.toString(this.types), this.rating, this.price_level, this.place_id, this.open_hours, this.open_now, this.phone, this.url);
    }

    @DexIgnore
    public void toModelDestinationObject(Destination.SearchType searchResultType, Destination destination) {
        destination.name = this.name;
        double newDisplayLat = Double.parseDouble(this.lat);
        double newDisplayLong = Double.parseDouble(this.lng);
        if (!(newDisplayLat == destination.displayLat && newDisplayLong == destination.displayLong)) {
            destination.navigationLat = 0.0d;
            destination.navigationLong = 0.0d;
        }
        destination.displayLat = newDisplayLat;
        destination.displayLong = newDisplayLong;
        destination.rawAddressNotForDisplay = this.address;
        destination.streetNumber = this.streetNumber;
        destination.streetName = this.streetName;
        destination.city = this.city;
        destination.state = this.state;
        destination.zipCode = this.zipCode;
        destination.country = this.country;
        destination.setCountryCode(this.countryCode);
        destination.placeId = this.place_id;
        destination.searchResultType = searchResultType.getValue();
        destination.placeDetailJson = this.jsonString;
        destination.type = getType(this.types);
    }

    @DexIgnore
    private Destination.Type getType(String[] types2) {
        Destination.Type destinationType = Destination.Type.UNKNOWN;
        if (types2 == null || types2.length <= 0) {
            return destinationType;
        }
        try {
            return parseTypes(new JSONArray(types2[0]));
        } catch (JSONException e) {
            e.printStackTrace();
            return destinationType;
        }
    }

    @DexIgnore
    public static Destination.Type parseTypes(JSONArray array) throws JSONException {
        Destination.Type destinationType = Destination.Type.UNKNOWN;
        if (array.length() <= 0) {
            return destinationType;
        }
        for (int i = 0; i < array.length(); i++) {
            Destination.Type tempType = getType((String) array.get(i));
            if (tempType != Destination.Type.UNKNOWN && tempType != Destination.Type.PLACE) {
                return tempType;
            }
            if (destinationType == Destination.Type.UNKNOWN) {
                destinationType = tempType;
            }
        }
        return destinationType;
    }

    // @android.support.annotation.NonNull
    @DexIgnore
    private static Destination.Type getType(@NonNull String type) {
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "accounting")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "airport")) {
            return Destination.Type.AIRPORT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "amusement_park")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "aquarium")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "art_gallery")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "atm")) {
            return Destination.Type.ATM;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bakery")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bank")) {
            return Destination.Type.BANK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bar")) {
            return Destination.Type.BAR;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "beauty_salon")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bicycle_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "book_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bowling_alley")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "bus_station")) {
            return Destination.Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "cafe")) {
            return Destination.Type.COFFEE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "campground")) {
            return Destination.Type.PARK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_dealer")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_rental")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_repair")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "car_wash")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "casino")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "cemetery")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "church")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "city_hall")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "clothing_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "convenience_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "courthouse")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "dentist")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "department_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "doctor")) {
            return Destination.Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "electrician")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "electronics_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "embassy")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "establishment")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "finance")) {
            return Destination.Type.BANK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "fire_station")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "florist")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "food")) {
            return Destination.Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "funeral_home")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "furniture_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "gas_station")) {
            return Destination.Type.GAS_STATION;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "general_contractor")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "grocery_or_supermarket")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "gym")) {
            return Destination.Type.GYM;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hair_care")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hardware_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "health")) {
            return Destination.Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hindu_temple")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "home_goods_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "hospital")) {
            return Destination.Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "insurance_agency")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "jewelry_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "laundry")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "lawyer")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "library")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "liquor_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "local_government_office")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "locksmith")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "lodging")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "meal_delivery")) {
            return Destination.Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "meal_takeaway")) {
            return Destination.Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "mosque")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "movie_rental")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "movie_theater")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "moving_company")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "museum")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "night_club")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "painter")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "park")) {
            return Destination.Type.PARK;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "parking")) {
            return Destination.Type.PARKING;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "pet_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "pharmacy")) {
            return Destination.Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "physiotherapist")) {
            return Destination.Type.HOSPITAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "place_of_worship")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "plumber")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "police")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "post_office")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "real_estate_agency")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "restaurant")) {
            return Destination.Type.RESTAURANT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "roofing_contractor")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "rv_park")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "school")) {
            return Destination.Type.SCHOOL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "shoe_store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "shopping_mall")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "spa")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "stadium")) {
            return Destination.Type.ENTERTAINMENT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "street_address")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "storage")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "store")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "subway_station")) {
            return Destination.Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "synagogue")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "taxi_stand")) {
            return Destination.Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "train_station")) {
            return Destination.Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "transit_station")) {
            return Destination.Type.TRANSIT;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "travel_agency")) {
            return Destination.Type.STORE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "university")) {
            return Destination.Type.SCHOOL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "veterinary_care")) {
            return Destination.Type.PLACE;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(type.toLowerCase(), "zoo")) {
            return Destination.Type.ENTERTAINMENT;
        }
        return Destination.Type.UNKNOWN;
    }
}
