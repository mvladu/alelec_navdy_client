package com.navdy.client.app.framework.servicehandler;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;

import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.callcontrol.TelephonyInterface;
import com.navdy.client.app.framework.callcontrol.TelephonyInterfaceFactory;
import com.navdy.client.app.framework.receiver.PhoneCallReceiver;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.PhoneStatusRequest;
import com.navdy.service.library.events.callcontrol.PhoneStatusResponse;
import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import com.navdy.service.library.events.callcontrol.TelephonyResponse;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class TelephonyServiceHandler {
    @DexIgnore
    private static /* final */ int TURN_SPEAKER_ON_DELAY; // = 3000;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler.class);
    @DexIgnore
    private int currentServiceState = 1;
    @DexIgnore
    private Context mContext;
    @DexIgnore
    private TelephonyInterface mTelephony;

    @DexEdit
    private PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String phoneNumber) {
            PhoneStatus status = PhoneStatus.PHONE_IDLE;
            switch(state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    status = PhoneStatus.PHONE_IDLE;
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    status = PhoneStatus.PHONE_RINGING;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    status = PhoneStatus.PHONE_OFFHOOK;
                    break;
            }
            PhoneCallReceiver.onCallStateChanged(mContext, status, phoneNumber);
        }

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            TelephonyServiceHandler.this.currentServiceState = serviceState.getState();
            TelephonyServiceHandler.sLogger.v("service state changed:" + TelephonyServiceHandler.this.currentServiceState);
        }
    };

    // @DexIgnore
    // class Anon2 extends TimerTask {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         TelephonyServiceHandler.sLogger.d("HFP is not connected, turning on the speaker phone");
    //         AudioManager mAudioManager = (AudioManager) TelephonyServiceHandler.this.mContext.getSystemService("audio");
    //         mAudioManager.setMode(2);
    //         mAudioManager.setSpeakerphoneOn(true);
    //     }
    // }

    @DexReplace
    public TelephonyServiceHandler(Context context) {
        this.mContext = context;
        this.mTelephony = TelephonyInterfaceFactory.getTelephonyInterface(context);
        sLogger.v("Telephony model[" + Build.MODEL + "] manufacturer[" + Build.MANUFACTURER + "] device[" + Build.DEVICE + "]");
        sLogger.v("Telephony controller is :" + this.mTelephony.getClass().getName());
        ((TelephonyManager) context.getSystemService("phone")).listen(this.phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE | PhoneStateListener.LISTEN_SERVICE_STATE);
        sLogger.v("registeted phone state listener");
        BusProvider.getInstance().register(this);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceDisconnectedEvent(DeviceConnection.DeviceDisconnectedEvent event) {
        ((TelephonyManager) this.mContext.getSystemService("phone")).listen(this.phoneStateListener, 0);
    }

    @com.squareup.otto.Subscribe
    @DexReplace
    public void onTelephonyRequest(TelephonyRequest telephonyRequest) {
        switch (telephonyRequest.action) {
            case CALL_ACCEPT:
                this.mTelephony.acceptRingingCall();
                turnOnSpeakerPhoneIfNeeded();
                break;
            case CALL_REJECT:
                this.mTelephony.rejectRingingCall();
                break;
            case CALL_END:
                this.mTelephony.endCall();
                break;
            case CALL_DIAL:
                if (this.currentServiceState == 0) {
                    String uri = "tel:" + telephonyRequest.number.trim();
                    Intent intent = new Intent("android.intent.action.CALL");
                    intent.setFlags(402653184);
                    intent.setData(Uri.parse(uri));
                    if (ActivityCompat.checkSelfPermission(NavdyApplication.getAppContext(), "android.permission.CALL_PHONE") != 0) {
                        sLogger.e("Missing CALL_PHONE permission !");
                        break;
                    } else {
                        this.mContext.startActivity(intent);
                        turnOnSpeakerPhoneIfNeeded();
                        break;
                    }
                } else {
                    DeviceConnection.postEvent((Message) new PhoneEvent(PhoneStatus.PHONE_DISCONNECTING, telephonyRequest.number, null, null, null));
                    DeviceConnection.postEvent((Message) new TelephonyResponse(telephonyRequest.action, RequestStatus.REQUEST_SERVICE_ERROR, null));
                    return;
                }
            case CALL_MUTE:
            case CALL_UNMUTE:
                this.mTelephony.toggleMute();
                break;
            default:
                sLogger.e("Unknown TelephonyServiceHandler request action: " + telephonyRequest.action);
                break;
        }
        DeviceConnection.postEvent((Message) new TelephonyResponse(telephonyRequest.action, RequestStatus.REQUEST_SUCCESS, null));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onPhoneStatusRequest(PhoneStatusRequest phoneStatusRequest) {
        DeviceConnection.postEvent((Message) new PhoneStatusResponse(RequestStatus.REQUEST_SUCCESS, PhoneCallReceiver.getCurrentStatus()));
    }

    @DexIgnore
    private void turnOnSpeakerPhoneIfNeeded() {
        // if (!isHFPSpeakerConnected()) {
        //     new Timer().schedule(new TelephonyServiceHandler.Anon2(), 3000);
        // }
    }

    @DexIgnore
    private boolean isHFPSpeakerConnected() {
        return BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(1) == 2;
    }
}
