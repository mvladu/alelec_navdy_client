//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.navdy.client.app.framework.glances;

import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lanchon.dexpatcher.annotation.DexReplace;

@DexReplace
public class GlanceConstants {
    public static final String ACTION_EMAIL_GUESTS = "Email guests";
    public static final String ACTION_MAP = "Map";
    public static final String ANDROID_BIG_TEXT = "android.bigText";

    public static final String ANDROID_PEOPLE = "android.people";
    public static final String ANDROID_SUB_TEXT = "android.subText";
    public static final String ANDROID_SUMMARY_TEXT = "android.summaryText";
    public static final String ANDROID_TEMPLATE = "android.template";
    public static final String ANDROID_TEXT = "android.text";
    public static final String ANDROID_TEXT_LINES = "android.textLines";
    public static final String ANDROID_TITLE = "android.title";

    public static final String ANDROID_CALENDAR = "com.android.calendar";
    public static final String APPLE_MUSIC = "com.apple.android.music";
    public static final String FACEBOOK = "com.facebook.katana";
    public static final String FACEBOOK_MESSENGER = "com.facebook.orca";
    public static final String FUEL_PACKAGE = "com.navdy.fuel";
    public static final String GOOGLE_CALENDAR = "com.google.android.calendar";
    public static final String GOOGLE_HANGOUTS = "com.google.android.talk";
    public static final String GOOGLE_INBOX = "com.google.android.apps.inbox";
    public static final String GOOGLE_MAIL = "com.google.android.gm";
    public static final String GOOGLE_MUSIC = "com.google.android.music";
    public static final String MAIL_TO = "mailto:";
    public static final String MICROSOFT_OUTLOOK = "com.microsoft.office.outlook";
    public static final String MUSIC_PACKAGE = "com.navdy.music";
    public static final String NAPSTER = "com.rhapsody";
    public static final String NEW_LINE = "\n";
    public static final String NOTIFICATION_INBOX_STYLE = "android.app.Notification$InboxStyle";
    public static final String PANDORA = "com.pandora.android";
    public static final String PHONE_PACKAGE = "com.navdy.phone";
    public static final String SAMSUNG_MUSIC = "com.sec.android.app.music";
    public static final String SLACK = "com.Slack";
    public static final String SMS_PACKAGE = "com.navdy.sms";
    public static final String SOUNDCLOUD = "com.soundcloud.android";
    public static final String SPOTIFY = "com.spotify.music";
    public static final String TIDAL = "com.aspiro.tidal";
    public static final String TRAFFIC_PACKAGE = "com.navdy.traffic";
    public static final String TWITTER = "com.twitter.android";
    public static final String WHATS_APP = "com.whatsapp";
    public static final String YOUTUBE_MUSIC = "com.google.android.apps.youtube.music";
    public static final String MESSAGING_PACKAGE = "com.google.android.apps.messaging";

    public static final String CAR_EXT = "android.car.EXTENSIONS";
    public static final String CAR_EXT_CONVERSATION = "car_conversation";
    public static final String CAR_EXT_CONVERSATION_MESSAGES = "messages";
    public static final String CAR_EXT_CONVERSATION_MESSAGES_AUTHOR = "author";
    public static final String CAR_EXT_CONVERSATION_MESSAGES_TEXT = "text";
    public static final String CAR_EXT_CONVERSATION_PARTICIPANTS = "participants";
    public static final String COMMA = ",";
    public static final String EMAIL_AT = "@";
    public static final String EMPTY = "";


    public static final boolean GLANCES_APP_GLANCES_ENABLED_DEFAULT = false;
    public static final boolean GLANCES_DRIVING_GLANCES_ENABLED_DEFAULT = true;
    public static final boolean GLANCES_OTHER_APPS_ENABLED_DEFAULT = false;

    private static final Map<GlanceConstants.Group, Set<String>> groups = new HashMap<GlanceConstants.Group, Set<String>>() {
        {
            this.put(GlanceConstants.Group.DRIVING_GLANCES, new HashSet<>());
            this.put(GlanceConstants.Group.WHITE_LIST, new HashSet<>());
            this.put(GlanceConstants.Group.EMAIL_GROUP, new HashSet<>());
            this.put(GlanceConstants.Group.MESSAGING_GROUP, new HashSet<>());
            this.put(GlanceConstants.Group.CALENDAR_GROUP, new HashSet<>());
            this.put(GlanceConstants.Group.SOCIAL_GROUP, new HashSet<>());
            this.put(GlanceConstants.Group.MUSIC_GROUP, new HashSet<>());
            this.put(GlanceConstants.Group.IGNORE_GROUP, new HashSet<>());
        }
    };
    private static final Logger logger = new Logger(GlanceConstants.class);

    static {
        Set<String> group_DRIVING_GLANCES = groups.get(Group.DRIVING_GLANCES);
        Set<String> group_WHITE_LIST = groups.get(Group.WHITE_LIST);
        Set<String> group_EMAIL_GROUP = groups.get(Group.EMAIL_GROUP);
        Set<String> group_MESSAGING_GROUP = groups.get(Group.MESSAGING_GROUP);
        Set<String> group_CALENDAR_GROUP = groups.get(Group.CALENDAR_GROUP);
        Set<String> group_SOCIAL_GROUP = groups.get(Group.SOCIAL_GROUP);
        Set<String> group_MUSIC_GROUP = groups.get(Group.MUSIC_GROUP);
        Set<String> group_IGNORE_GROUP = groups.get(Group.IGNORE_GROUP);
        group_EMAIL_GROUP.add(GOOGLE_MAIL);
        group_EMAIL_GROUP.add(GOOGLE_INBOX);
        group_EMAIL_GROUP.add(MICROSOFT_OUTLOOK);
        group_MESSAGING_GROUP.add(SLACK);
        group_MESSAGING_GROUP.add(GOOGLE_HANGOUTS);
        group_MESSAGING_GROUP.add(WHATS_APP);
        group_MESSAGING_GROUP.add(FACEBOOK_MESSENGER);
        group_CALENDAR_GROUP.add(GOOGLE_CALENDAR);
        group_SOCIAL_GROUP.add(FACEBOOK);
        group_SOCIAL_GROUP.add(TWITTER);
        group_MUSIC_GROUP.add(GOOGLE_MUSIC);
        group_MUSIC_GROUP.add(PANDORA);
        group_MUSIC_GROUP.add(TIDAL);
        group_MUSIC_GROUP.add(SPOTIFY);
        group_MUSIC_GROUP.add(APPLE_MUSIC);
        group_MUSIC_GROUP.add(YOUTUBE_MUSIC);
        group_MUSIC_GROUP.add(SAMSUNG_MUSIC);
        group_MUSIC_GROUP.add(NAPSTER);
        group_MUSIC_GROUP.add(SOUNDCLOUD);
        group_DRIVING_GLANCES.add(PHONE_PACKAGE);
        group_DRIVING_GLANCES.add(SMS_PACKAGE);
        group_DRIVING_GLANCES.add(FUEL_PACKAGE);
        group_DRIVING_GLANCES.add(MUSIC_PACKAGE);
        group_DRIVING_GLANCES.add(TRAFFIC_PACKAGE);
        group_WHITE_LIST.add(GOOGLE_MAIL);
        group_WHITE_LIST.addAll(group_MESSAGING_GROUP);
        group_WHITE_LIST.addAll(group_CALENDAR_GROUP);
        group_WHITE_LIST.addAll(group_SOCIAL_GROUP);
        group_IGNORE_GROUP.add("com.google.android.gms");
        group_IGNORE_GROUP.add("com.android.mms");
        group_IGNORE_GROUP.add("com.android.systemui");
    }

    public GlanceConstants() {
    }

    public static void addPackageToGroup(String packageName, GlanceConstants.Group group) {
        Set<String> var2 = groups.get(group);
        if (var2 != null) {
            var2.add(packageName);
        } else {
            logger.e("Cannot get package for group " + group);
        }

    }

    public static boolean isPackageInGroup(String packageName, GlanceConstants.Group group) {
        Set<String> var2 = groups.get(group);
        boolean var3;
        if (var2 != null) {
            var3 = var2.contains(packageName);
        } else {
            logger.e("Cannot get package for group " + group);
            var3 = false;
        }

        return var3;
    }

    @DexReplace
    public enum Group {
        CALENDAR_GROUP,
        DRIVING_GLANCES,
        EMAIL_GROUP,
        IGNORE_GROUP,
        MESSAGING_GROUP,
        MUSIC_GROUP,
        SOCIAL_GROUP,
        WHITE_LIST;

        Group() {
        }
    }
}
