package com.navdy.client.app.framework.service;

import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.navdy.client.app.framework.servicehandler.ContactServiceHandler;
import com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler;
import com.navdy.client.app.framework.servicehandler.DisplayNavigationServiceHandler;
import com.navdy.client.app.framework.servicehandler.MessageServiceHandler;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.servicehandler.PhotoServiceHandler;
import com.navdy.client.app.framework.servicehandler.PlaceTypeSearchServiceHandler;
import com.navdy.client.app.framework.servicehandler.SettingsServiceHandler;
import com.navdy.client.app.framework.servicehandler.SpeechServiceHandler;
import com.navdy.client.app.framework.servicehandler.TelephonyServiceHandler;
import com.navdy.client.app.framework.servicehandler.TripUpdateServiceHandler;
import com.navdy.client.app.framework.servicehandler.VoiceServiceHandler;
import com.navdy.client.app.framework.util.ContactObserver;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.proxy.ProxyListener;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.connection.AcceptorListener;
import com.navdy.service.library.device.connection.ConnectionListener;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.ProxyService;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.BTSocketAcceptor;
import com.navdy.service.library.util.Listenable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class ClientConnectionService extends ConnectionService {
    @DexIgnore
    private /* final */ IBinder binder; // = new com.navdy.client.app.framework.service.ClientConnectionService.LocalBinder();
    @DexIgnore
    private ClientConnectionService.DeviceChangeBroadcaster deviceChangeBroadcaster; // = new com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster();
    @DexIgnore
    private List<Object> serviceHandlers;

    @DexIgnore
    public static class DeviceChangeBroadcaster extends Listenable<ClientConnectionService.DeviceChangeBroadcaster.Listener> {

        @DexIgnore
        class Anon1 implements ClientConnectionService.DeviceChangeBroadcaster.EventDispatcher {
            @DexIgnore
            final /* synthetic */ RemoteDevice val$newDevice;

            @DexIgnore
            Anon1(RemoteDevice remoteDevice) {
                this.val$newDevice = remoteDevice;
            }

            @DexIgnore
            public void dispatchEvent(ClientConnectionService.DeviceChangeBroadcaster source, ClientConnectionService.DeviceChangeBroadcaster.Listener listener) {
                listener.onDeviceChanged(this.val$newDevice);
            }
        }

        @DexIgnore
        interface EventDispatcher extends Listenable.EventDispatcher<ClientConnectionService.DeviceChangeBroadcaster, ClientConnectionService.DeviceChangeBroadcaster.Listener> {
        }

        @DexIgnore
        public interface Listener extends Listenable.Listener {
            void onDeviceChanged(RemoteDevice remoteDevice);
        }

        @DexIgnore
        void dispatchDeviceChangedEvent(RemoteDevice newDevice) {
            dispatchToListeners(new ClientConnectionService.DeviceChangeBroadcaster.Anon1(newDevice));
        }
    }

    @DexIgnore
    public class LocalBinder extends Binder {
        @DexIgnore
        public LocalBinder() {
        }

        @DexIgnore
        public ClientConnectionService getService() {
            return ClientConnectionService.this;
        }
    }

    @DexIgnore
    public void addListener(ClientConnectionService.DeviceChangeBroadcaster.Listener listener) {
        this.deviceChangeBroadcaster.addListener(listener);
    }

    @DexIgnore
    public void removeListener(ClientConnectionService.DeviceChangeBroadcaster.Listener listener) {
        this.deviceChangeBroadcaster.removeListener(listener);
    }

    @DexReplace
    public void onCreate() {
        super.onCreate();
        Logger logger = new Logger(ClientConnectionService.class);
        ContactServiceHandler contactServiceHandler = ContactServiceHandler.getInstance();
        this.serviceHandlers = new ArrayList<>();
        try { this.serviceHandlers.add(DisplayNavigationServiceHandler.getInstance()); } catch (RuntimeException ex) { logger.e("DisplayNavigationServiceHandler", ex); }
        try { this.serviceHandlers.add(new TelephonyServiceHandler(this)); } catch (RuntimeException ex) { logger.e("TelephonyServiceHandler", ex); }
        try { this.serviceHandlers.add(new SpeechServiceHandler()); } catch (RuntimeException ex) { logger.e("SpeechServiceHandler", ex); }
        try { this.serviceHandlers.add(VoiceServiceHandler.getInstance()); } catch (RuntimeException ex) { logger.e("VoiceServiceHandler", ex); }
        try { this.serviceHandlers.add(MusicServiceHandler.getInstance()); } catch (RuntimeException ex) { logger.e("MusicServiceHandler", ex); }
        try { this.serviceHandlers.add(contactServiceHandler); } catch (RuntimeException ex) { logger.e("contactServiceHandler", ex); }
        try { this.serviceHandlers.add(new PhotoServiceHandler(this)); } catch (RuntimeException ex) { logger.e("PhotoServiceHandler", ex); }
        try { this.serviceHandlers.add(DestinationsServiceHandler.getInstance()); } catch (RuntimeException ex) { logger.e("DestinationsServiceHandler", ex); }
        try { this.serviceHandlers.add(new MessageServiceHandler()); } catch (RuntimeException ex) { logger.e("MessageServiceHandler", ex); }
        try { this.serviceHandlers.add(new SettingsServiceHandler()); } catch (RuntimeException ex) { logger.e("SettingsServiceHandler", ex); }
        try { this.serviceHandlers.add(TripUpdateServiceHandler.getInstance()); } catch (RuntimeException ex) { logger.e("TripUpdateServiceHandler", ex); }
        try { this.serviceHandlers.add(new PlaceTypeSearchServiceHandler()); } catch (RuntimeException ex) { logger.e("PlaceTypeSearchServiceHandler", ex); }
        try {
            if (BaseActivity.weHaveContactsPermission()) {
                getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, new ContactObserver(contactServiceHandler, this.serviceHandler));
            }
        } catch (RuntimeException ex) { logger.e("ContactsContract.Contacts.CONTENT_URI", ex); }
    }

    @DexIgnore
    protected ConnectionListener[] getConnectionListeners(Context context) {
        return new ConnectionListener[]{new AcceptorListener(context, new BTSocketAcceptor("Navdy", NAVDY_PROTO_SERVICE_UUID), ConnectionType.BT_PROTOBUF)};
    }

    @DexIgnore
    protected RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters() {
        return new RemoteDeviceBroadcaster[0];
    }

    @DexIgnore
    protected ProxyService createProxyService() throws IOException {
        return new ProxyListener(new BTSocketAcceptor("Navdy-Proxy-Tunnel", NAVDY_PROXY_TUNNEL_UUID));
    }

    @DexIgnore
    public RemoteDevice getRemoteDevice() {
        return this.mRemoteDevice;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!getClass().getName().equals(intent.getAction())) {
            return super.onBind(intent);
        }
        this.logger.d("returning local binder");
        return this.binder;
    }

    @DexIgnore
    protected void setRemoteDevice(RemoteDevice remoteDevice) {
        boolean deviceChanged = this.mRemoteDevice != remoteDevice;
        super.setRemoteDevice(remoteDevice);
        if (deviceChanged) {
            this.deviceChangeBroadcaster.dispatchDeviceChangedEvent(remoteDevice);
        }
    }
}
