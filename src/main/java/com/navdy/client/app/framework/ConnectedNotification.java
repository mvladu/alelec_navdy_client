package com.navdy.client.app.framework;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

import static android.app.PendingIntent.FLAG_ONE_SHOT;


public class ConnectedNotification extends Service {
    // public ConnectedNotification() {
    //     super("ConnectedNotificationService");
    // }

    // @Override
    // protected void onHandleIntent(@Nullable Intent intent) {
    //
    // }

    public final static int notification_id = 2;
    public static String notification_channel_id;

    private boolean configured;
    private static boolean displayedNotification;

    private void startNotification() {
        if (!configured) {
            // this.mConfig = new Config(getResources());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notification_channel_id = "navdy_connected"; // The id of the channel.
                CharSequence name = getString(R.string.navdy); // The user-visible name of the channel.
                NotificationChannel mChannel = new NotificationChannel(notification_channel_id, name, NotificationManager.IMPORTANCE_MIN);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.createNotificationChannel(mChannel);
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                notification_channel_id = "";
            }
            configured = true;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, notification_channel_id);
        builder.setContentIntent(getUIPendingIntent());
        builder.setContentTitle(getString(com.navdy.client.R.string.navdy_connected));
        builder.setSmallIcon(com.navdy.client.R.drawable.icon_status_bar);
        // builder.setContentText(getString(com.navdy.client.R.string.update_available));
        builder.setAutoCancel(true);
        builder.setDefaults(0);
        builder.setSound(null);
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(notification_id, builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(notification_channel_id);
            startForeground(notification_id, builder.build());
        }
        displayedNotification = true;
    }

    public void onCreate() {
        startNotification();
        super.onCreate();
    }

    public static Intent getServiceIntent(Context context) {
        return new Intent(context, ConnectedNotification.class);
    }

    public static void startService(Context context) {
        if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.CONNECTION_NOTIFICATION_ENABLED, true)) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(getServiceIntent(context));
                } else {
                    context.startService(getServiceIntent(context));
                }
            } catch (RuntimeException ignored) {
                // Cannot start when app is in background, will try again later
            }
        }

    }

    public static void stopService(Context context) {
        context.stopService(getServiceIntent(context));
    }


    public void removeNotification() {
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(notification_id);
    }

    private PendingIntent getUIPendingIntent() {
        Context appContext = getApplicationContext();
        return PendingIntent.getActivity(appContext, 256, new Intent(appContext, ConnectedNotification.class), FLAG_ONE_SHOT);
    }

    public void onDestroy() {
        if (!displayedNotification) {
            startNotification();
        }
        removeNotification();
        displayedNotification = false;
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        startNotification();
        return super.onStartCommand(intent, flags, startId);
    }
}
