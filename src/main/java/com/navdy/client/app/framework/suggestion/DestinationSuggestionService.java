package com.navdy.client.app.framework.suggestion;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Pair;

import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Trip;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.State;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.places.SuggestedDestination.SuggestionType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;

import org.droidparts.contract.SQL.DDL;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexRemove;
import lanchon.dexpatcher.annotation.DexReplace;

// background service -> JobScheduler
// https://stackoverflow.com/a/49846410/3093392

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public class DestinationSuggestionService extends android.support.v4.app.JobIntentService {
    @DexIgnore
    private static /* final */ String BUCKET_NAME; // = "navdy-trip-data";
    @DexIgnore
    public static /* final */ String COMMAND_AUTO_UPLOAD_TRIP_DATA; // = "COMMAND_AUTO_UPLOAD";
    @DexIgnore
    public static /* final */ String COMMAND_POPULATE; // = "COMMAND_POPULATE";
    @DexIgnore
    public static /* final */ String COMMAND_RESET; // = "COMMAND_RESET";
    @DexIgnore
    public static /* final */ String COMMAND_SUGGEST; // = "COMMAND_SUGGEST";
    @DexIgnore
    public static /* final */ String COMMAND_UPLOAD_TRIP_DATA; // = "COMMAND_UPLOAD";
    @DexIgnore
    private static /* final */ String EXTRA_LOCAL; // = "EXTRA_LOCAL";
    @DexIgnore
    private static /* final */ long MINIMUM_INTERVAL_BETWEEN_UPLOADS; // = java.util.concurrent.TimeUnit.DAYS.toMillis(1);
    @DexIgnore
    private static Destination NO_SUGGESTION; // = null;
    @DexIgnore
    public static /* final */ String PLACE_SUGGESTION_LIBRARY_NAME; // = "placesuggestion";
    @DexIgnore
    private static /* final */ String PREFERENCE_LAST_UPLOAD_ID; // = "PREF_LAST_UPLOAD_ID";
    @DexIgnore
    private static /* final */ String PREFERENCE_LAST_UPLOAD_TIME; // = "PREF_LAST_UPLOAD_TIME";
    @DexIgnore
    private static /* final */ String SERVICE_NAME; // = "SERVICE_NAME";
    @DexIgnore
    public static /* final */ int SUGGESTED_DESTINATION_TRESHOLD; // = 200;
    @DexIgnore
    public static /* final */ int TRIP_TRESHOLD_FOR_UPLOAD; // = 10;
    @DexIgnore
    public static /* final */ int TRIP_UPLOAD_SERVICE_REQ; // = 256;
    @DexIgnore
    private static boolean sColdStart; // = true;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.class);
    @DexIgnore
    private static long sMaxIdentifier; // = -1;

    @DexAdd
    public static int JOB_ID = 1;

    // @DexIgnore
    // class Anon2 implements Listener {
    //     final /* synthetic */ com.navdy.service.library.events.destination.Destination val$destinationMessage;
    //     final /* synthetic */ SuggestionType val$suggestionType;
    //
    //     @DexIgnore
    //     Anon2(com.navdy.service.library.events.destination.Destination destination, SuggestionType suggestionType) {
    //         this.val$destinationMessage = destination;
    //         this.val$suggestionType = suggestionType;
    //     }
    //
    //     @DexIgnore
    //     public void onPreCalculation(@NonNull RouteHandle routeHandle) {
    //     }
    //
    //     @DexIgnore
    //     public void onRouteCalculated(@NonNull HereRouteManager.Error error, Route route) {
    //         int routeDurationWithTraffic = -1;
    //         if (error == HereRouteManager.Error.NONE && route.getTta(TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE) != null) {
    //             routeDurationWithTraffic = route.getTta(TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE).getDuration();
    //         }
    //         DestinationSuggestionService.this.sendSuggestion(new com.navdy.service.library.events.places.SuggestedDestination(this.val$destinationMessage, Integer.valueOf(routeDurationWithTraffic), this.val$suggestionType));
    //     }
    // }
    //
    // @DexIgnore
    // class Anon3 {
    //     final /* synthetic */ Bus val$bus;
    //     final /* synthetic */ long val$maxTripId;
    //     final /* synthetic */ SharedPreferences val$sharedPreferences;
    //
    //     @DexIgnore
    //     Anon3(SharedPreferences sharedPreferences, long j, Bus bus) {
    //         this.val$sharedPreferences = sharedPreferences;
    //         this.val$maxTripId = j;
    //         this.val$bus = bus;
    //     }
    //
    //     @Subscribe
    //     @DexIgnore
    //     public void onProgress(DestinationSuggestionService.UploadResult uploadResult) {
    //         if (uploadResult == DestinationSuggestionService.UploadResult.SUCCESS) {
    //             this.val$sharedPreferences.edit().putLong(DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_TIME, System.currentTimeMillis()).putLong(DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_ID, this.val$maxTripId).apply();
    //             DestinationSuggestionService.this.scheduleNextCheckForUpdate();
    //         }
    //         this.val$bus.unregister(this);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 implements TransferListener {
    //     final /* synthetic */ Bus val$bus;
    //     final /* synthetic */ File val$file;
    //
    //     @DexIgnore
    //     Anon4(File file, Bus bus) {
    //         this.val$file = file;
    //         this.val$bus = bus;
    //     }
    //
    //     @DexIgnore
    //     public void onStateChanged(int id, TransferState state) {
    //         DestinationSuggestionService.sLogger.d("Uploading File " + this.val$file.getName() + ", onStatChanged : " + id + DDL.SEPARATOR + state.name());
    //         if (state == TransferState.COMPLETED) {
    //             if (this.val$bus != null) {
    //                 this.val$bus.post(DestinationSuggestionService.UploadResult.SUCCESS);
    //             }
    //         } else if (state == TransferState.FAILED && this.val$bus != null) {
    //             this.val$bus.post(DestinationSuggestionService.UploadResult.FAILURE);
    //         }
    //     }
    //
    //     @DexIgnore
    //     public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
    //         DestinationSuggestionService.sLogger.d("Uploading File " + this.val$file.getName() + ", onProgressChanged : " + id + ", Size :" + bytesCurrent + ", of " + bytesTotal);
    //     }
    //
    //     @DexIgnore
    //     public void onError(int id, Exception ex) {
    //         DestinationSuggestionService.sLogger.e("Uploading File " + this.val$file.getName() + ", onError : " + id, ex);
    //         if (this.val$bus != null) {
    //             this.val$bus.post(DestinationSuggestionService.UploadResult.FAILURE);
    //         }
    //     }
    // }

    @DexIgnore
    private enum UploadResult {
        SUCCESS,
        FAILURE
    }

    @DexIgnore
    public static native void learn(double d, double d2, long j, double d3, double d4, int i, int i2, int i3);

    @DexIgnore
    public static native void reset();

    @DexIgnore
    public static native SuggestedDestination suggestDestination(double d, double d2, int i, int i2, int i3);

    /* @DexIgnore
    static {
        NO_SUGGESTION = new com.navdy.client.app.framework.models.Destination();
        java.lang.System.loadLibrary(PLACE_SUGGESTION_LIBRARY_NAME);
        NO_SUGGESTION = new com.navdy.client.app.framework.models.Destination();
        NO_SUGGESTION.setId(-1);
    } */

    @DexRemove
    public void onCreate() {
        super.onCreate();
    }

    @DexReplace
    public DestinationSuggestionService() {
        super();
        // super(SERVICE_NAME);
    }

    @DexAdd
    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, DestinationSuggestionService.class, JOB_ID, work);
    }

    @DexAdd
    public void onHandleWork(@NonNull Intent intent) {
        if (intent == null) {
            sLogger.e("received a call for onHandleIntent with a null intent!");
            return;
        }
        String action = intent.getAction();
        if (COMMAND_SUGGEST.equals(action)) {
            handleSuggest(intent);
        } else if (COMMAND_POPULATE.equals(action)) {
            handlePopulate();
        } else if (COMMAND_UPLOAD_TRIP_DATA.equals(action)) {
            handleUploadTripData();
        } else if (COMMAND_AUTO_UPLOAD_TRIP_DATA.equals(action)) {
            handleAutoTripUpload();
        } else if (COMMAND_RESET.equals(action)) {
            reset();
            sColdStart = true;
        }
    }

    @DexReplace
    private void handleSuggest(Intent intent) {
        boolean forThePhoneSuggestionList = intent.getBooleanExtra(EXTRA_LOCAL, true);
        sLogger.d("onHandleIntent : Command suggest, forThePhoneSuggestionList = " + forThePhoneSuggestionList);
        try {
            learnAllTrips();
            if (forThePhoneSuggestionList) {
                suggestDestinationBasedOnContext(true);
                return;
            }
            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice == null || !remoteDevice.isConnected()) {
                sLogger.d("Remote device is not connected, so not making a suggestion");
            } else if (NavdyRouteHandler.getInstance().getCurrentState() == State.EN_ROUTE) {
                sLogger.d("There is an active trip going on in HUD, so not sending suggestion");
            } else {
                SuggestionManager.buildCalendarSuggestions(suggestions -> {
                    if (DestinationSuggestionService.this.firstSuggestionHasValidDestination(suggestions)) {
                        DestinationSuggestionService.this.sendSuggestionToHud(new Suggestion(suggestions.get(0).destination, Suggestion.SuggestionType.CALENDAR), SuggestionType.SUGGESTION_CALENDAR);
                        return;
                    }
                    DestinationSuggestionService.this.suggestDestinationBasedOnContext(false);
                });
            }
        } catch (Throwable t) {
            sLogger.e("Error during destination suggestion ", t);
        }
    }

    @DexReplace
    boolean firstSuggestionHasValidDestination(ArrayList<Suggestion> suggestions) {
        return suggestions != null && suggestions.size() > 0 && suggestions.get(0) != null && suggestions.get(0).destination != null && suggestions.get(0).destination.hasOneValidSetOfCoordinates();
    }

    @DexReplace
    private void suggestDestinationBasedOnContext(boolean forThePhoneSuggestionList) {
        sLogger.d("Suggest Destination based on the context For suggestion list :" + forThePhoneSuggestionList);
        Date date = new Date(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(date);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int dayOfTheWeek = cal.get(Calendar.DAY_OF_WEEK);
        SuggestedDestination destination = null;
        Coordinate currentCoordinate = NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (currentCoordinate != null) {
            sLogger.d("Current location is known: " + currentCoordinate.latitude + DDL.SEPARATOR + currentCoordinate.longitude);
            destination = suggestDestination(currentCoordinate.latitude, currentCoordinate.longitude, hour, minute, dayOfTheWeek);
        } else {
            sLogger.d("Current location is unknown");
            if (forThePhoneSuggestionList) {
                destination = suggestDestination(0.0d, 0.0d, hour, minute, dayOfTheWeek);
            }
        }
        Destination savedDestination = null;
        if (destination != null) {
            savedDestination = NavdyContentProvider.getThisDestination((int) destination.uniqueIdentifier);
            if (savedDestination != null) {
                Location currentLocation = new Location("");
                if (currentCoordinate != null) {
                    currentLocation.setLatitude(currentCoordinate.latitude);
                    currentLocation.setLongitude(currentCoordinate.longitude);
                }
                Location location = new Location("");
                location.setLatitude(savedDestination.displayLat);
                location.setLongitude(savedDestination.displayLong);
                if (((double) location.distanceTo(currentLocation)) <= 200.0d) {
                    if (forThePhoneSuggestionList) {
                        BusProvider.getInstance().post(NO_SUGGESTION);
                    }
                    sLogger.d("Not suggesting as the destination with the identifier is very close to the current location");
                    sLogger.d("DestinationEngine , suggested destiantion,  ID: " + destination.uniqueIdentifier + ", Lat: " + destination.latitude + ", Long: " + destination.longitude + ", Frequency: " + destination.frequency + ", Probability: " + destination.probability);
                    return;
                } else if (forThePhoneSuggestionList) {
                    BusProvider.getInstance().post(savedDestination);
                } else {
                    sLogger.d("DestinationEngine: suggested destiantion: ID = " + destination.uniqueIdentifier + ", Lat = " + destination.latitude + ", Long = " + destination.longitude + ", Frequency = " + destination.frequency + ", Probability = " + destination.probability);
                    Suggestion suggestion = new Suggestion(savedDestination, Suggestion.SuggestionType.RECOMMENDATION);
                    sendSuggestionToHud(suggestion, SuggestionType.SUGGESTION_RECOMMENDATION);
                }
            }
        }
        if (forThePhoneSuggestionList && savedDestination == null) {
            BusProvider.getInstance().post(NO_SUGGESTION);
        }
    }

    @DexIgnore
    private void sendSuggestionToHud(@NonNull Suggestion suggestion, SuggestionType suggestionType) {
        // double longitude;
        // double latitude;
        // com.navdy.service.library.events.destination.Destination destinationMessage = suggestion.toProtobufDestinationObject();
        // if (destinationMessage != null) {
        //     sLogger.d("DestinationEngine , destination message,  Title :" + destinationMessage.destination_title + ", Subtitle :" + destinationMessage.destination_subtitle + ", Type :" + destinationMessage.suggestion_type + ", Is Recommendation :" + destinationMessage.is_recommendation + ", Address :" + destinationMessage.full_address + ", Unique ID: " + destinationMessage.identifier);
        //     if (destinationMessage.navigation_position.latitude == 0.0d && destinationMessage.navigation_position.longitude == 0.0d) {
        //         latitude = destinationMessage.display_position.latitude;
        //         longitude = destinationMessage.display_position.longitude;
        //     } else {
        //         latitude = destinationMessage.navigation_position.latitude;
        //         longitude = destinationMessage.navigation_position.longitude;
        //     }
        //     HereRouteManager.getInstance().calculateRoute(latitude, longitude, new Anon2(destinationMessage, suggestionType));
        // }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x01cb A[SYNTHETIC, Splitter:B:30:0x01cb] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x01d0 A[Catch:{ IOException -> 0x02a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x02b4 A[SYNTHETIC, Splitter:B:67:0x02b4] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x02b9 A[Catch:{ IOException -> 0x02bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:? A[RETURN, SYNTHETIC] */
    @DexIgnore
    private void handlePopulate() {
        // File file = new File(Environment.getExternalStorageDirectory() + File.separator + "data.csv");
        // if (file.exists()) {
        //     FileReader reader = null;
        //     BufferedReader buffReader = null;
        //     try {
        //         FileReader fileReader = new FileReader(file);
        //         try {
        //             BufferedReader bufferedReader = new BufferedReader(fileReader);
        //             while (true) {
        //                 try {
        //                     String line = bufferedReader.readLine();
        //                     if (line == null) {
        //                         break;
        //                     }
        //                     if (!line.startsWith("Z_PK")) {
        //                         String[] parts = line.split(",");
        //                         long id = Long.parseLong(parts[0]);
        //                         long tripNumber = Long.parseLong(parts[5]);
        //                         long arrivedDestination = !StringUtils.isEmptyAfterTrim(parts[6]) ? Long.parseLong(parts[6]) : -1;
        //                         long chosenDestination = !StringUtils.isEmptyAfterTrim(parts[7]) ? Long.parseLong(parts[7]) : -1;
        //                         long inferredDestination = !StringUtils.isEmptyAfterTrim(parts[8]) ? Long.parseLong(parts[8]) : -1;
        //                         long destinationId = arrivedDestination != -1 ? arrivedDestination : chosenDestination != -1 ? chosenDestination : inferredDestination;
        //                         double destLatitude = Double.parseDouble(parts[9]);
        //                         double destLongitude = Double.parseDouble(parts[10]);
        //                         long endTime = (long) (Double.parseDouble(parts[11]) + 9.783072E8d);
        //                         double startLatitude = Double.parseDouble(parts[12]);
        //                         double startLongitude = Double.parseDouble(parts[13]);
        //                         long startTime = (long) (Double.parseDouble(parts[14]) + 9.783072E8d);
        //                         sLogger.d("Start Time " + startTime);
        //                         int offset = SystemUtils.getTimeZoneAndDaylightSavingOffset(Long.valueOf(startTime));
        //                         Calendar cal = Calendar.getInstance(Locale.getDefault());
        //                         cal.setTime(new Date(1000 * startTime));
        //                         int hour = cal.get(11);
        //                         int minute = cal.get(12);
        //                         int dayOfTheWeek = cal.get(7);
        //                         sLogger.d("MM/DD/YYY " + cal.get(2) + S3Constants.S3_FILE_DELIMITER + cal.get(5) + S3Constants.S3_FILE_DELIMITER + cal.get(1) + " , Day :" + dayOfTheWeek);
        //                         new Trip((int) id, tripNumber, startTime, offset, 0, startLatitude, startLongitude, endTime, 0, destLatitude, destLongitude, arrivedDestination, (int) destinationId).saveToDb(this);
        //                         learn(destLatitude, destLongitude, destinationId, startLatitude, startLongitude, hour, minute, dayOfTheWeek);
        //                     }
        //                 } catch (Throwable th) {
        //                     th = th;
        //                     buffReader = bufferedReader;
        //                     reader = fileReader;
        //                     if (buffReader != null) {
        //                     }
        //                     if (reader != null) {
        //                     }
        //                     throw th;
        //                 }
        //             }
        //             for (int day = 1; day <= 7; day++) {
        //                 for (int hour2 = 0; hour2 < 24; hour2++) {
        //                     sLogger.d("Day :" + day + ", Hour :" + hour2);
        //                     SuggestedDestination destination = suggestDestination(0.0d, 0.0d, hour2, 0, day);
        //                     if (destination != null) {
        //                         sLogger.d("Destination :" + destination.latitude + DDL.SEPARATOR + destination.longitude + " ; ID :" + destination.uniqueIdentifier);
        //                     }
        //                 }
        //             }
        //             if (bufferedReader != null) {
        //                 try {
        //                     bufferedReader.close();
        //                 } catch (IOException e) {
        //                     sLogger.e("Error closing the buffered reader");
        //                     return;
        //                 }
        //             }
        //             if (fileReader != null) {
        //                 fileReader.close();
        //             }
        //         } catch (Throwable th2) {
        //             th = th2;
        //             reader = fileReader;
        //             if (buffReader != null) {
        //             }
        //             if (reader != null) {
        //             }
        //             throw th;
        //         }
        //     } catch (Throwable th3) {
        //         t = th3;
        //         sLogger.e("Exception ", t);
        //         if (buffReader != null) {
        //         }
        //         if (reader != null) {
        //         }
        //     }
        // }
    }

    @DexIgnore
    private void handleAutoTripUpload() {
        // Cursor tripCursor;
        // sLogger.d("handleAutoTripUpload : checking if we need an auto upload");
        // SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        // long lastUploadTimeStamp = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_TIME, 0);
        // long currentTime = System.currentTimeMillis();
        // long lastUploadedTripId = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_ID, -1);
        // long maxTripId = NavdyContentProvider.getMaxTripId();
        // boolean dataBaseChanged = maxTripId < lastUploadedTripId;
        // if (dataBaseChanged) {
        //     sLogger.d("Database has changed");
        // }
        // if (dataBaseChanged || ((currentTime - lastUploadTimeStamp > MINIMUM_INTERVAL_BETWEEN_UPLOADS && (lastUploadedTripId == -1 || lastUploadedTripId < maxTripId)) || maxTripId > 10 + lastUploadedTripId)) {
        //     sLogger.d("Uploading the trip DB");
        //     Cursor tripCursor2 = null;
        //     if (lastUploadedTripId > 0) {
        //         try {
        //             tripCursor = getTripCursor(lastUploadedTripId);
        //         } catch (Throwable th) {
        //             IOUtils.closeStream(tripCursor2);
        //             throw th;
        //         }
        //     } else {
        //         tripCursor = NavdyContentProvider.getTripsCursor();
        //     }
        //     String fullName = (SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.FULL_NAME, SettingsConstants.LAST_HUD_UUID_DEFAULT) + "_" + Build.MANUFACTURER + "_" + Build.MODEL).replaceAll("[^a-zA-Z0-9.-]", "_");
        //     SimpleDateFormat simpleDateFormat = new SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", Locale.US);
        //     String uniqueName = fullName + simpleDateFormat.format(new Date(System.currentTimeMillis())) + "_Android_V" + 1 + ".csv";
        //     sLogger.d("Uploading the file to S3, Key : " + uniqueName);
        //     Bus bus = new Bus(ThreadEnforcer.ANY);
        //     bus.register(new Anon3(sharedPreferences, maxTripId, bus));
        //     uploadToS3(uniqueName, tripCursor, bus);
        //     IOUtils.closeStream(tripCursor);
        //     scheduleNextCheckForUpdate();
        // }
    }

    @DexReplace
    private void scheduleNextCheckForUpdate() {
        sLogger.d("Scheduling for next check for update");
        Intent intent = new Intent(this, DestinationSuggestionService.class);
        intent.setAction(COMMAND_AUTO_UPLOAD_TRIP_DATA);
        ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).set(
                AlarmManager.RTC,
                System.currentTimeMillis() + MINIMUM_INTERVAL_BETWEEN_UPLOADS,
                PendingIntent.getService(this, TRIP_UPLOAD_SERVICE_REQ, intent, PendingIntent.FLAG_CANCEL_CURRENT)
        );
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x020b, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x020c, code lost:
        sLogger.e("Error closing the cursor ", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0214, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0215, code lost:
        sLogger.e("Error closing the file writer ", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01a6, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01a7, code lost:
        if (r19 != null) goto L_0x01a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        r19.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01b0, code lost:
        sLogger.e("Error closing the cursor ", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01b8, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01b9, code lost:
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        r18.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01b8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01bc A[SYNTHETIC, Splitter:B:79:0x01bc] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01c1 A[SYNTHETIC, Splitter:B:82:0x01c1] */
    @DexIgnore
    private void uploadToS3(String fileName, Cursor tripDbCursor, Bus bus) {
        // String str;
        // double d;
        // double d2;
        // FileWriter fileWriter = null;
        // try {
        //     File file = new File(getFilesDir() + File.separator + fileName);
        //     if (file.exists()) {
        //         IOUtils.deleteFile(NavdyApplication.getAppContext(), file.getAbsolutePath());
        //     }
        //     if (!file.createNewFile()) {
        //         if (tripDbCursor != null) {
        //             try {
        //                 tripDbCursor.close();
        //             } catch (Throwable t) {
        //                 sLogger.e("Error closing the cursor ", t);
        //             }
        //         }
        //         if (fileWriter != null) {
        //             try {
        //                 fileWriter.close();
        //             } catch (IOException e) {
        //                 sLogger.e("Error closing the file writer ", e);
        //             }
        //         }
        //     } else {
        //         FileWriter fileWriter2 = new FileWriter(file, true);
        //         if (tripDbCursor != null) {
        //             try {
        //                 if (tripDbCursor.getCount() > 0) {
        //                     fileWriter2.write("TRIP_ID,TRIP_NUMBER,TRIP_START_TIME,TRIP_START_TIME_ZONE_N_DST,TRIP_ODOMETER,TRIP_LATITUDE,TRIP_LONGITUDE,TRIP_END_TIME,TRIP_END_ODOMETER,TRIP_END_LATITUDE,TRIP_END_LONGITUDE,TRIP_ARRIVED_AT_DESTINATION,TRIP_DESTINATION_ID,TRIP_DESTINATION_LAT,TRIP_DESTINATION_LONG\n");
        //                     while (tripDbCursor.moveToNext()) {
        //                         Trip trip = NavdyContentProvider.getTripsItemAt(tripDbCursor, tripDbCursor.getPosition());
        //                         if (trip != null) {
        //                             Destination savedDestination = NavdyContentProvider.getThisDestination(trip.destinationId);
        //                             StringBuilder append = new StringBuilder().append(trip.id).append(",").append(trip.tripNumber).append(",").append(trip.startTime).append(",").append(trip.offset).append(",").append(trip.startOdometer).append(",").append(trip.startLat).append(",").append(trip.startLong).append(",").append(trip.endTime).append(",").append(trip.endOdometer).append(",").append(trip.endLat).append(",").append(trip.endLong).append(",");
        //                             if (trip.arrivedAtDestination == 0) {
        //                                 str = "false";
        //                             } else {
        //                                 str = "true";
        //                             }
        //                             StringBuilder append2 = append.append(str).append(",").append(trip.destinationId <= 0 ? "NA" : Integer.valueOf(trip.destinationId)).append(",");
        //                             if (savedDestination != null) {
        //                                 d = savedDestination.navigationLat;
        //                             } else {
        //                                 d = 0.0d;
        //                             }
        //                             StringBuilder append3 = append2.append(d).append(",");
        //                             if (savedDestination != null) {
        //                                 d2 = savedDestination.navigationLong;
        //                             } else {
        //                                 d2 = 0.0d;
        //                             }
        //                             fileWriter2.write(append3.append(d2).append("\n").toString());
        //                         }
        //                     }
        //                     tripDbCursor.close();
        //                     fileWriter2.flush();
        //                     uploadFileToS3(file, bus);
        //                     if (tripDbCursor != null) {
        //                         try {
        //                             tripDbCursor.close();
        //                         } catch (Throwable t2) {
        //                             sLogger.e("Error closing the cursor ", t2);
        //                         }
        //                     }
        //                     if (fileWriter2 != null) {
        //                         try {
        //                             fileWriter2.close();
        //                             FileWriter fileWriter3 = fileWriter2;
        //                             return;
        //                         } catch (IOException e2) {
        //                             sLogger.e("Error closing the file writer ", e2);
        //                             FileWriter fileWriter4 = fileWriter2;
        //                             return;
        //                         }
        //                     } else {
        //                         return;
        //                     }
        //                 }
        //             } catch (Throwable th) {
        //             }
        //         }
        //         if (bus != null) {
        //             bus.post(DestinationSuggestionService.UploadResult.FAILURE);
        //         }
        //         if (tripDbCursor != null) {
        //             try {
        //                 tripDbCursor.close();
        //             } catch (Throwable t3) {
        //                 sLogger.e("Error closing the cursor ", t3);
        //             }
        //         }
        //         if (fileWriter2 != null) {
        //             try {
        //                 fileWriter2.close();
        //             } catch (IOException e3) {
        //                 sLogger.e("Error closing the file writer ", e3);
        //             }
        //         }
        //         FileWriter fileWriter5 = fileWriter2;
        //     }
        // } catch (Throwable th2) {
        //     if (bus != null) {
        //         try {
        //             bus.post(DestinationSuggestionService.UploadResult.FAILURE);
        //         } catch (Throwable th3) {
        //             th = th3;
        //             if (tripDbCursor != null) {
        //             }
        //             if (fileWriter != null) {
        //             }
        //             throw th;
        //         }
        //     }
        //     sLogger.e("Error while dumping the trip database to the csv file");
        //     if (tripDbCursor != null) {
        //         try {
        //             tripDbCursor.close();
        //         } catch (Throwable t4) {
        //             sLogger.e("Error closing the cursor ", t4);
        //         }
        //     }
        //     if (fileWriter != null) {
        //         try {
        //             fileWriter.close();
        //         } catch (IOException e4) {
        //             sLogger.e("Error closing the file writer ", e4);
        //         }
        //     }
        // }
    }

    @DexIgnore
    private void handleUploadTripData() {
        sLogger.d("Uploading the trip data base to the S3");
        String fullName = SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.FULL_NAME, SettingsConstants.LAST_HUD_UUID_DEFAULT).replaceAll("[^a-zA-Z0-9.-]", "_");
        uploadToS3(fullName + "_" + Build.SERIAL + new SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", Locale.US).format(new Date(System.currentTimeMillis())) + ".csv", getTripCursor(), null);
    }

    @DexIgnore
    private void learnAllTrips() {
        Cursor cursor = null;
        try {
            cursor = getTripCursor();
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Trip trip = NavdyContentProvider.getTripsItemAt(cursor, cursor.getPosition());
                    if (trip != null) {
                        learn(trip);
                    }
                }
                sColdStart = false;
            }
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    @DexIgnore
    private void uploadFileToS3(File file, Bus bus) {
        // if (file != null && file.exists()) {
        //     sLogger.d("Length of the file being uploaded: " + file.length());
        //     if (file.length() != 0) {
        //         new TransferUtility(OTAUpdateManagerImpl.createS3Client(), this).upload(BUCKET_NAME, file.getName(), file).setTransferListener(new Anon4(file, bus));
        //     } else if (bus != null) {
        //         bus.post(DestinationSuggestionService.UploadResult.FAILURE);
        //     }
        // }
    }

    @DexIgnore
    private void sendSuggestion(com.navdy.service.library.events.places.SuggestedDestination suggestedDestination) {
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null && remoteDevice.isConnected()) {
            remoteDevice.postEvent(suggestedDestination);
        }
    }

    @DexIgnore
    private Cursor getTripCursor() {
        if (sColdStart) {
            sLogger.d("cold start, getting all the trips for learning");
            return NavdyContentProvider.getTripsCursor();
        }
        sLogger.d("warm start, getting trips greater than " + sMaxIdentifier);
        return getTripCursor(sMaxIdentifier);
    }

    @DexIgnore
    private Cursor getTripCursor(long maxId) {
        return NavdyContentProvider.getTripsCursor(new Pair<>("_id > ?", new String[]{Long.toString(maxId)}));
    }

    @DexIgnore
    private void learn(Trip trip) {
        if (((long) trip.id) > sMaxIdentifier) {
            sMaxIdentifier = (long) trip.id;
        }
        long destinationIdentifier = trip.destinationId > 0 ? (long) trip.destinationId : -1;
        Destination savedDestination = NavdyContentProvider.getThisDestination((int) destinationIdentifier);
        if (savedDestination == null || !savedDestination.doNotSuggest) {
            Calendar cal = Calendar.getInstance(Locale.getDefault());
            cal.setTime(new Date(trip.startTime));
            learn(trip.endLat, trip.endLong, destinationIdentifier, trip.startLat, trip.startLong, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.DAY_OF_WEEK));
        }
    }

    @DexReplace
    public static void suggestDestination(Context context, boolean local) {
        Intent intent = new Intent(context, DestinationSuggestionService.class);
        intent.setAction(COMMAND_SUGGEST);
        intent.putExtra(EXTRA_LOCAL, local);
        // context.startService(intent);
        enqueueWork(context, intent);
    }

    @DexReplace
    public static void uploadTripDatabase(Context context, boolean manual) {
        if (!manual) {
            sLogger.d("UploadTripDatabase :Not a debug build so skipping");
            return;
        }
        Intent intent = new Intent(context, DestinationSuggestionService.class);
        intent.setAction(manual ? COMMAND_UPLOAD_TRIP_DATA : COMMAND_AUTO_UPLOAD_TRIP_DATA);
        // context.startService(intent);
        enqueueWork(context, intent);
    }

    @DexReplace
    public static void reset(Context context) {
        Intent intent = new Intent(context, DestinationSuggestionService.class);
        intent.setAction(COMMAND_RESET);
        // context.startService(intent);
        enqueueWork(context, intent);
    }
}
