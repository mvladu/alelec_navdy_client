package com.navdy.client.app.framework.util;

//import android.os.Build;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
//
//import com.navdy.hud.app.BuildConfig;
//import com.navdy.hud.app.R;
//import com.navdy.hud.app.framework.toast.ToastManager;
//import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
//import com.navdy.hud.app.framework.toast.ToastPresenter;
//import com.navdy.hud.app.storage.PathManager;

import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

//import ch.qos.logback.core.util.FileUtil;

import okhttp3.MediaType;
//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.codec.binary.StringUtils;

//import com.sendgrid.*;

//import android.util.Base64;

//import org.apache.commons.io.FileUtils;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class GitlabSender {
    private static final String TAG = GitlabSender.class.getName();
    private static Logger sLogger = new Logger(TAG);
    private static String emailAddress = "incoming+alelec-navdy-alelec-navdy-client-9565076-issue-@incoming.gitlab.com";

    private static final MediaType MEDIA_TYPE_ZIP = MediaType.parse("application/zip");
    private static final MediaType MEDIA_TYPE_TXT = MediaType.parse("text/plain");
    private static final MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");

    public static Boolean uploadReport(String version,
                                       String description,
                                       String contact,
                                       DeviceInfo deviceInfo,
                                       File attachments) {

//        (lastReport.deviceInfo != null) ? FileUtils.readFileToByteArray(lastReport.deviceInfo): null,

//        return uploadReportMailjet(log, description, userID, contact, deviceInfo, screenshot, settings);
        return uploadReportSendgrid(version, description, contact, deviceInfoString(deviceInfo), attachments);
    }

    private static String deviceInfoString(DeviceInfo a) {
        if (a == null)
            return "";

        return "\nDevice Information\n" +
                "-----------------------\n\n" +
                "Platform :" + a.platform.name() + "\n" +
                "Name :" + a.deviceName + "\n" +
                "System Version :" + a.systemVersion + "\n" +
                "Model :" + a.model + "\n" +
                "Device ID :" + a.deviceId + "\n" +
                "Make :" + a.deviceMake + "\n" +
                "Build Type :" + a.buildType + "\n" +
                "API level :" + a.systemApiLevel + "\n" +
                "Protocol version :" + a.protocolVersion + "\n" +
                "Force Full Update Flag :" + a.forceFullUpdate + "\n";
    }

    static final String SENDGRID_USER = "apikey";
    static final String SENDGRID_API_KEY = "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";
    static final String SENDGRID_API_PW = SENDGRID_API_KEY;

//    public static Boolean uploadReportSendgrid(File log, File log_warn, String description,
//                                               String userID, String contact,
//                                               String deviceInfo,
//                                               File screenshot,
//                                               File settings) {

    public static Boolean uploadReportSendgrid(String version,
                                               String description,
                                               String contact,
                                               String deviceInfo,
                                               File attachments) {
        Boolean success = false;
        String error = "";

        //Creating properties
        Properties props = new Properties();

        //Configuring properties for gmail
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", "smtp.sendgrid.net");

        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");

        props.put("mail.smtp.auth", "true");

        try {

            //Creating a new session
            Session session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        //Authenticating the password
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(SENDGRID_USER, SENDGRID_API_PW);
                        }
                    });



            //Creating MimeMessage object
            MimeMessage message = new MimeMessage(session);

            //Setting sender address
            message.setFrom(new InternetAddress(contact));
            //Adding receiver
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailAddress));
            //Adding subject
            message.setSubject("Client");
            //Adding message
//            message.setText(body);
//
            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            String body = "<h1>Client Report</h1>" + description + "<br/>" + version + "<br/>";
            if (deviceInfo != null) {
                body += deviceInfo.replace("\n", "<br>");
            }
            messageBodyPart.setText(body);

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            if (attachments != null) {
                messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(attachments);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName("details.zip");
                multipart.addBodyPart(messageBodyPart);
            }

            // Send the complete message parts
            message.setContent(multipart);

            //Sending email
            Transport.send(message);

            success = true;

        } catch (Exception ex) {
            ex.printStackTrace();
            error = ex.toString();
        }

//        showReportSendToast(success, error);
        return success;
    }
    /*
        public static Boolean uploadReportMailjet(String log, String description,
                                           String userID, String contact,
                                           byte[] deviceInfo,
                                           byte[] screenshot,
                                           byte[] settings) {

            String MJ_APIKEY_PUBLIC = "75baf831a027f5157b7ac4cb6d13243e";
            String MJ_APIKEY_PRIVATE = "9fda4ab23656e723d79c55963125b29e";

            // https://dev.mailjet.com/guides/?java#sending-with-attached-files

    //        byte[] descriptionEncoded = Base64.encodeBase64(description.getBytes());
            byte[] deviceInfoEncoded = (deviceInfo != null)?Base64.encodeBase64(deviceInfo):null;
            byte[] screenshotEncoded = (screenshot != null)?Base64.encodeBase64(screenshot):null;
            byte[] settingsEncoded = (settings != null)?Base64.encodeBase64(settings):null;

            Boolean success = false;
            String error = "";

            MailjetClient client;
            MailjetRequest request;
            MailjetResponse response;
            client = new MailjetClient(MJ_APIKEY_PUBLIC, MJ_APIKEY_PRIVATE);
            try {
                request = new MailjetRequest(com.mailjet.client.resource.Email.resource)
                        .property(com.mailjet.client.resource.Email.FROMEMAIL, "navdy@alelec.net")
                        .property(com.mailjet.client.resource.Email.FROMNAME, "Navdy")
                        .property(com.mailjet.client.resource.Email.SUBJECT, "Diagnostic Report")
                        .property(com.mailjet.client.resource.Email.TEXTPART, GitlabSender.createCrashLog(log, contact))
                        .property(com.mailjet.client.resource.Email.RECIPIENTS, new JSONArray()
                                .put(new JSONObject()
                                        .put("Email", emailAddress)))
                        .property(com.mailjet.client.resource.Email.ATTACHMENTS, new JSONArray()
                                .put(new JSONObject()
                                        .put("Content-type", "image/jpeg")
                                        .put("Filename", "screenshot.jpeg")
                                        .put("content", screenshotEncoded))
                                .put(new JSONObject()
                                        .put("Content-type", "text/plain")
                                        .put("Filename", "settings.txt")
                                        .put("content", settingsEncoded))
                                .put(new JSONObject()
                                        .put("Content-type", "text/plain")
                                        .put("Filename", "deviceInfo.txt")
                                        .put("content", deviceInfoEncoded)));

                response = client.post(request);
                sLogger.i(response.toString());

                success = true;

            } catch (JSONException | MailjetSocketTimeoutException | MailjetException e) {
                e.printStackTrace();
                error = e.toString();
            }

            showReportSendToast(success, error);
            return success;
        }
    */
    public static MediaType getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return MediaType.parse(type);
    }

//    public static String createCrashLog(String log, String contact) {
//        Date now = new Date();
//        String header = ("Package: " + BuildConfig.APPLICATION_ID + "\n") +
//                "Version Code: " + BuildConfig.VERSION_CODE + "\n" +
//                "Version Name: " + BuildConfig.VERSION_NAME + "\n" +
//                "Incremental: " + Build.VERSION.INCREMENTAL + "\n" +
//                "Android: " + Build.VERSION.RELEASE + "\n" +
//                "Manufacturer: " + "Navdy" + "\n" +
//                "Model: " + "Display" + "\n" +
//                "Date: " + now + "\n" +
//                "CrashReporter Key: " + CrashReporter.getUserID() + "\n\n" +
//                "Manual Report Submission: \"" + contact + "\"\n" +
//                "  at User.Report(User_Report:-1)\n\n";
//
//        // Hockeyapp limits log to 200kB
//        String details = log;
//        while (header.length() + details.length() >= 192*1024) {
//            details = details.substring(details.indexOf('\n')+1);
//        }
//        return header + details;
//    }

    public static void showReportSendToast(Boolean sent, String error) {
        if (sent) {
            Toast.makeText(NavdyApplication.getAppContext(), "Report Sent", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(NavdyApplication.getAppContext(), "Report Error: " + error, Toast.LENGTH_LONG).show();
        }
//        Bundle bundle = new Bundle();
//        int icon;
//        ToastManager toastManager = ToastManager.getInstance();
//        String detail;
//        if (sent) {
//            icon = R.drawable.icon_success;
//            detail = "Report Sent, please follow up on gitlab and/or reddit";
//        } else {
//            icon = R.drawable.icon_msg_failed;
//            detail = "Report upload failed, please try again.\n" + error;
//        }
//        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 3000);
//        String id = "report-send";
//        bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
//        toastManager.dismissCurrentToast(id);
//        toastManager.clearPendingToast(id);
//
//        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, icon);
//        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, detail);
//        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
//        if (!TextUtils.equals(id, toastManager.getCurrentToastId())) {
//            ToastManager.getInstance().addToast(new ToastManager$ToastParams(id, bundle, null, false, false));
//        }
    }


}