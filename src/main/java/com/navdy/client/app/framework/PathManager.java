package com.navdy.client.app.framework;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public class PathManager {
    @DexIgnore
    private static /* final */ java.lang.String DISKCACHE_DIR; // = "diskcache-v4";
    @DexIgnore
    private static /* final */ java.lang.String HERE_MAPS_CONFIG_BASE_PATH; // = ".here-maps";
    @DexIgnore
    private static /* final */ java.util.regex.Pattern HERE_MAPS_CONFIG_DIRS_PATTERN; // = java.util.regex.Pattern.compile(HERE_MAPS_CONFIG_DIRS_REGEX);
    @DexIgnore
    private static /* final */ java.lang.String HERE_MAPS_CONFIG_DIRS_REGEX; // = "[0-9]{10}";
    @DexIgnore
    private static /* final */ java.lang.String LOGS_FOLDER; // = "logs";
    @DexIgnore
    private static /* final */ java.lang.String OTA_INCREMENTAL_UPDATE_FILE_FORMAT; // = "navdy_ota_%d_to_%d.zip";
    @DexIgnore
    private static /* final */ java.lang.String OTA_UPDATE_FILE_FORMAT; // = "navdy_ota_%d.zip";
    @DexIgnore
    private static /* final */ java.lang.String OTA_UPDATE_FILE_NAME; // = "hudupdate.zip";
    @DexIgnore
    private static /* final */ java.lang.String OTA_UPDATE_FOLDER; // = "update";
    @DexIgnore
    private static /* final */ java.lang.String S3_PROGRESS_FILE_EXTENSION; // = ".s3";
    @DexIgnore
    private static /* final */ java.lang.String S3_PROGRESS_FILE_PREFIX; // = "navdy_ota_progress_";
    @DexIgnore
    public static /* final */ java.lang.String TICKETS_FOLDER_NAME; // = "navdy/support_tickets/";
    @DexIgnore
    private static /* final */ java.lang.String TIMESTAMP_MWCONFIG_LATEST; // = "1482069896";
    @DexIgnore
    private static /* final */ com.navdy.service.library.log.Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.PathManager.class);
    @DexIgnore
    private static /* final */ com.navdy.client.app.framework.PathManager sSingleton; // = new com.navdy.client.app.framework.PathManager();
    @DexIgnore
    private java.io.File hereMapsConfigDirs;
    @DexIgnore
    private java.lang.String mLogsFolder;
    @DexIgnore
    private java.lang.String mOtaUpdatePath;

    @DexIgnore
    class Anon1 implements java.io.FileFilter {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public boolean accept(java.io.File file) {
            return file.isDirectory() && com.navdy.client.app.framework.PathManager.HERE_MAPS_CONFIG_DIRS_PATTERN.matcher(file.getName()).matches();
        }
    }

    @DexIgnore
    public static com.navdy.client.app.framework.PathManager getInstance() {
        return sSingleton;
    }

    @DexIgnore
    private PathManager() {
        java.lang.String internalAppPath = com.navdy.client.app.NavdyApplication.getAppContext().getFilesDir().getAbsolutePath();
        this.mOtaUpdatePath = internalAppPath + java.io.File.separator + OTA_UPDATE_FOLDER;
        this.mLogsFolder = internalAppPath + java.io.File.separator + LOGS_FOLDER;
        com.navdy.service.library.util.IOUtils.createDirectory(this.mOtaUpdatePath);
        com.navdy.service.library.util.IOUtils.createDirectory(this.mLogsFolder);
        this.hereMapsConfigDirs = new java.io.File(internalAppPath + java.io.File.separator + HERE_MAPS_CONFIG_BASE_PATH);
    }

    @DexIgnore
    public java.lang.String getOtaUpdateFolderPath() {
        return this.mOtaUpdatePath;
    }

    @DexIgnore
    public java.lang.String getOTAUpdateFilePath() {
        return this.mOtaUpdatePath + java.io.File.separator + OTA_UPDATE_FILE_NAME;
    }

    @DexIgnore
    public java.lang.String getTicketFolderPath() {
        return android.os.Environment.getExternalStorageDirectory() + java.io.File.separator + TICKETS_FOLDER_NAME;
    }

    @DexIgnore
    public java.lang.String getOtaUpdateFileNameOnHUD(int version) {
        return java.lang.String.format(OTA_UPDATE_FILE_FORMAT, new java.lang.Object[]{java.lang.Integer.valueOf(version)});
    }

    @DexIgnore
    public java.lang.String getOtaUpdateFileNameOnHUD(int fromVersion, int targetVersion) {
        return java.lang.String.format(OTA_INCREMENTAL_UPDATE_FILE_FORMAT, new java.lang.Object[]{java.lang.Integer.valueOf(fromVersion), java.lang.Integer.valueOf(targetVersion)});
    }

    @DexIgnore
    public java.lang.String getS3DownloadProgressFile(int incrementalVersion) {
        return this.mOtaUpdatePath + java.io.File.separator + S3_PROGRESS_FILE_PREFIX + incrementalVersion + S3_PROGRESS_FILE_EXTENSION;
    }

    @DexIgnore
    public java.lang.String getLogsFolder() {
        return this.mLogsFolder;
    }

    @DexIgnore
    public java.util.List<java.lang.String> getHereMapsConfigDirs() {
        java.util.List<java.lang.String> pathList = new java.util.ArrayList<>();
        java.io.File[] hereDirs = this.hereMapsConfigDirs.listFiles(new com.navdy.client.app.framework.PathManager.Anon1());
        if (hereDirs != null) {
            for (java.io.File f : hereDirs) {
                pathList.add(f.getAbsolutePath());
            }
        }
        return pathList;
    }

    @DexIgnore
    public boolean hasDiskCacheDir() {
        return new java.io.File(this.hereMapsConfigDirs.getAbsolutePath() + java.io.File.separator + DISKCACHE_DIR).exists();
    }

    @DexIgnore
    public java.lang.String getLatestHereMapsConfigPath() {
        return this.hereMapsConfigDirs.getAbsolutePath() + java.io.File.separator + TIMESTAMP_MWCONFIG_LATEST;
    }
}
