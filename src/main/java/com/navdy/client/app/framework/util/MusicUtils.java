package com.navdy.client.app.framework.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.provider.MediaStore.Audio;
import android.support.annotation.NonNull;
import android.view.KeyEvent;

import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.music.LocalMusicPlayer;
import com.navdy.client.app.framework.music.LocalMusicPlayer.Listener;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public class MusicUtils {
    @DexIgnore
    private static /* final */ Uri ALBUM_ART_URI; // = android.net.Uri.parse("content://media/external/audio/albumart");
    @DexIgnore
    public static /* final */ int ARTWORK_SIZE; // = 200;
    @DexIgnore
    private static /* final */ Options BITMAP_OPTIONS; // = new android.graphics.BitmapFactory.Options();
    @DexIgnore
    private static /* final */ String[] DEFAULT_ARTWORK_CHECKSUMS; // = {"a980062052ec31e96ae73029ec7e6772", "6bbaf7d1b3318d9414d51d8a71705a04", "3f89e08544651f0345b0ee39cb3ef3a4", "59d9cdaa3c6f52efd34ce5e8d282cf5e", "1cb6a67160592a08bf314e9742e0fed6", "4dba0b6fe14bf656c92aed8d50b56b8d"};
    @DexIgnore
    private static /* final */ Map<Action, Integer> KEY_CODES_MAPPING; // = new com.navdy.client.app.framework.util.MusicUtils.Anon1();
    @DexIgnore
    private static /* final */ int MAX_PLAY_RETRIES; // = 10;
    @DexIgnore
    private static /* final */ long PAUSE_BETWEEN_RETRIES; // = 500;
    @DexIgnore
    private static /* final */ Map<Action, KeyEvent> SEEK_KEY_EVENTS_MAPPING; // = new com.navdy.client.app.framework.util.MusicUtils.Anon2();
    @DexIgnore
    private static /* final */ String SONG_ART_URI_POSTFIX; // = "/albumart";
    @DexIgnore
    private static /* final */ String SONG_URI; // = "content://media/external/audio/media/";
    @DexIgnore
    private static volatile AudioManager audioManager; // = null;
    @DexIgnore
    private static LocalMusicPlayer localMusicPlayer; // = new com.navdy.client.app.framework.music.LocalMusicPlayer();
    @DexIgnore
    public static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.MusicUtils.class);
    @DexIgnore
    private static KeyEvent startedContinuousKeyEvent; // = null;
    @DexIgnore
    private static MusicDataSource startedEventDataSource; // = null;

    @DexIgnore
    static class Anon1 extends HashMap<Action, Integer> {
        @DexIgnore
        Anon1() {
            put(Action.MUSIC_ACTION_PLAY, KeyEvent.KEYCODE_MEDIA_PLAY);
            put(Action.MUSIC_ACTION_PAUSE, KeyEvent.KEYCODE_MEDIA_PAUSE);
            put(Action.MUSIC_ACTION_PREVIOUS, KeyEvent.KEYCODE_MEDIA_PREVIOUS);
            put(Action.MUSIC_ACTION_NEXT, KeyEvent.KEYCODE_MEDIA_NEXT);
        }
    }

    @DexIgnore
    static class Anon2 extends HashMap<Action, KeyEvent> {
        @DexIgnore
        Anon2() {
            put(Action.MUSIC_ACTION_REWIND_START, new KeyEvent(0, KeyEvent.KEYCODE_MEDIA_REWIND));
            put(Action.MUSIC_ACTION_FAST_FORWARD_START, new KeyEvent(0, KeyEvent.KEYCODE_MEDIA_FAST_FORWARD));
            put(Action.MUSIC_ACTION_REWIND_STOP, new KeyEvent(1, KeyEvent.KEYCODE_MEDIA_REWIND));
            put(Action.MUSIC_ACTION_FAST_FORWARD_STOP, new KeyEvent(1, KeyEvent.KEYCODE_MEDIA_FAST_FORWARD));
        }
    }

    @DexIgnore
    static class Anon3 implements Runnable {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        /* final */ /* synthetic */ MusicEvent val$event;

        @DexIgnore
        Anon3(MusicEvent musicEvent) {
            this.val$event = musicEvent;
        }

        @DexIgnore
        public void run() {
            MusicUtils.handleLocalPlayEvent(this.val$event);
        }
    }

    @DexIgnore
    static class Anon4 implements Listener {
        @DexIgnore
        private Bitmap albumArt = null;
        @NonNull
        @DexIgnore
        private MusicTrackInfo musicTrackInfo = MusicServiceHandler.EMPTY_TRACK_INFO;

        @DexIgnore
        class Anon1 implements Runnable {
            @DexIgnore
            /* final */ /* synthetic */ MusicTrackInfo val$trackInfo;

            /* renamed from: com.navdy.client.app.framework.util.MusicUtils$Anon4$Anon1$Anon1 reason: collision with other inner class name */
            // class C0051Anon1 implements Runnable {
            //     @DexIgnore
            //     /* final */ /* synthetic */ Bitmap val$bitmap;
            //
            //     C0051Anon1(Bitmap bitmap) {
            //         this.val$bitmap = bitmap;
            //     }
            //
            //     public void run() {
            //         if (this.val$bitmap == null || this.val$bitmap.getByteCount() == 0) {
            //             MusicUtils.sLogger.e("Received photo has null or empty byte array");
            //             Anon4.this.setMusicTrackInfo(Anon4.Anon1.this.val$trackInfo);
            //             Anon4.this.sendArtwork();
            //             return;
            //         }
            //         try {
            //             Anon4.this.albumArt = ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(this.val$bitmap, 200, 200, ScalingLogic.FIT);
            //             Anon4.this.setMusicTrackInfo(Anon4.Anon1.this.val$trackInfo);
            //             Anon4.this.sendArtwork();
            //         } catch (Exception e) {
            //             MusicUtils.sLogger.e("Error updating the artwork", e);
            //         }
            //     }
            // }

            @DexIgnore
            Anon1(MusicTrackInfo musicTrackInfo) {
                this.val$trackInfo = musicTrackInfo;
            }

            @DexIgnore
            public void run() {
                // new Handler(Looper.getMainLooper()).post(new Anon4.Anon1.C0051Anon1(MusicUtils.getLocalMusicPhoto(String.valueOf(this.val$trackInfo.trackId))));
            }
        }

        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        public void onMetadataUpdate(MusicTrackInfo trackInfo) {
            MusicUtils.sLogger.v("onMetadataUpdate " + trackInfo);
            setMusicTrackInfo(trackInfo);
            this.albumArt = null;
            TaskManager.getInstance().execute(new Anon4.Anon1(trackInfo), 1);
        }

        @DexIgnore
        public void onPlaybackStateUpdate(MusicPlaybackState musicPlaybackState) {
            MusicUtils.sLogger.v("onPlaybackStateUpdate " + musicPlaybackState);
            setMusicTrackInfo(new Builder(this.musicTrackInfo).playbackState(musicPlaybackState).build());
            sendArtwork();
        }

        @DexIgnore
        public void onPositionUpdate(int position, int duration) {
            MusicUtils.sLogger.v("onPositionUpdate");
            setMusicTrackInfo(new Builder(this.musicTrackInfo).currentPosition(position).duration(duration).build());
        }

        @DexIgnore
        private void setMusicTrackInfo(@NonNull MusicTrackInfo trackInfo) {
            MusicUtils.sLogger.v("setMusicTrackInfo " + trackInfo);
            this.musicTrackInfo = trackInfo;
            MusicServiceHandler.getInstance().setCurrentMediaTrackInfo(this.musicTrackInfo);
        }

        @DexIgnore
        private void sendArtwork() {
            MusicUtils.sLogger.v("sendArtwork " + this.musicTrackInfo);
            MusicServiceHandler.getInstance().checkAndSetCurrentMediaArtwork(this.musicTrackInfo, this.albumArt);
        }
    }

    @DexIgnore
    static class Anon5 implements Runnable {
        @DexIgnore
        Anon5() {
        }

        @DexIgnore
        /* final */ /* synthetic */ MusicEvent val$event;
        @DexIgnore
        /* final */ /* synthetic */ String val$packageName;

        @DexIgnore
        Anon5(String str, MusicEvent musicEvent) {
            this.val$packageName = str;
            this.val$event = musicEvent;
        }

        @DexIgnore
        public void run() {
            int retries = 0;
            while (retries < 10) {
                if (MusicServiceHandler.getInstance().getCurrentMediaTrackInfo().playbackState != MusicPlaybackState.PLAYBACK_PLAYING) {
                    if (GlanceConstants.SPOTIFY.equals(this.val$packageName)) {
                        MusicUtils.broadcastMusicEvent(this.val$event);
                    } else {
                        broadcastPlayEventViaIntent(this.val$packageName);
                    }
                    try {
                        Thread.sleep(500);
                        retries++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    MusicUtils.sLogger.i("Started music");
                    return;
                }
            }
            MusicUtils.sLogger.w("Failed to start music");
        }

        @DexIgnore
        private void broadcastPlayEventViaIntent(String packageName) {
            Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
            KeyEvent playButtonEvent = new KeyEvent(0, KeyEvent.KEYCODE_MEDIA_PLAY);
            intent.setPackage(packageName);
            intent.putExtra("android.intent.extra.KEY_EVENT", playButtonEvent);
            NavdyApplication.getAppContext().sendOrderedBroadcast(intent, null);
        }
    }

    @DexIgnore
    private static class MediaKeyEventRunnable implements Runnable {
        @DexIgnore
        private MusicDataSource dataSource;
        @DexIgnore
        private KeyEvent event;
        @DexIgnore
        private boolean isDownUpEvent;

        @DexIgnore
        public MediaKeyEventRunnable(MusicDataSource dataSource2, KeyEvent event2, boolean isDownUpEvent2) {
            this.dataSource = dataSource2;
            this.event = event2;
            this.isDownUpEvent = isDownUpEvent2;
        }

        @DexIgnore
        private void executeKeyDownUp(KeyEvent eventToRun) {
            MusicUtils.audioManager.dispatchMediaKeyEvent(KeyEvent.changeAction(eventToRun, 0));
            MusicUtils.audioManager.dispatchMediaKeyEvent(KeyEvent.changeAction(eventToRun, 1));
        }

        @DexIgnore
        public void run() {
            if (this.isDownUpEvent) {
                executeKeyDownUp(this.event);
            } else {
                executeSplitKeyEvent(this.dataSource, this.event);
            }
        }

        @DexIgnore
        private static synchronized void executeSplitKeyEvent(MusicDataSource dataSource2, KeyEvent event2) {
            synchronized (MusicUtils.MediaKeyEventRunnable.class) {
                int eventAction = event2.getAction();
                if (eventAction == 0) {
                    if (MusicUtils.startedContinuousKeyEvent != null) {
                        MusicUtils.sLogger.w("Other key down event already started pressed event - ending it: " + MusicUtils.startedContinuousKeyEvent);
                        MusicUtils.executeLongPressedKeyUp();
                    }
                    MusicUtils.dispatchMediaKeyEvent(event2);
                    long uptime = SystemClock.uptimeMillis();
                    MusicUtils.startedContinuousKeyEvent = KeyEvent.changeTimeRepeat(event2, uptime, 0);
                    MusicUtils.startedEventDataSource = dataSource2;
                    MusicUtils.sLogger.v("Executing specific key event: " + MusicUtils.startedContinuousKeyEvent + " with after-event for long press.");
                    MusicUtils.dispatchMediaKeyEvent(MusicUtils.startedContinuousKeyEvent);
                    MusicUtils.dispatchMediaKeyEvent(KeyEvent.changeTimeRepeat(event2, uptime, 1, event2.getFlags() | 128));
                } else if (eventAction != 1) {
                    MusicUtils.dispatchMediaKeyEvent(event2);
                } else if (MusicUtils.startedContinuousKeyEvent != null) {
                    if (event2.getKeyCode() != MusicUtils.startedContinuousKeyEvent.getKeyCode()) {
                        MusicUtils.sLogger.w("Unmatched key up event - ending the current just in case: " + MusicUtils.startedContinuousKeyEvent);
                    }
                    MusicUtils.executeLongPressedKeyUp();
                } else {
                    MusicUtils.sLogger.w("Unexpected key up event - doing nothing: " + event2);
                }
            }
        }
    }

    @DexIgnore
    private static void populateAudioManagerFromContext() {
        if (audioManager == null) {
            audioManager = (AudioManager) NavdyApplication.getAppContext().getSystemService(Context.AUDIO_SERVICE);
        }
    }

    @DexIgnore
    public static synchronized void executeMusicAction(MusicEvent event) throws UnsupportedOperationException, NameNotFoundException, IOException {
        synchronized (MusicUtils.class) {
            sLogger.d("executeMusicAction " + event);
            populateAudioManagerFromContext();
            MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
            Action action = event.action;
            MusicTrackInfo currentTrackInfo = musicServiceHandler.getCurrentMediaTrackInfo();
            if (MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(event.collectionSource) && Action.MUSIC_ACTION_PLAY.equals(action) && (event.index != null || event.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS || (localMusicPlayer != null && localMusicPlayer.isActive()))) {
                TaskManager.getInstance().execute(new Anon3(event), 1);
            } else if (MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(currentTrackInfo.collectionSource)) {
                if (Action.MUSIC_ACTION_PLAY.equals(action)) {
                    localMusicPlayer.play();
                } else if (Action.MUSIC_ACTION_PAUSE.equals(action)) {
                    localMusicPlayer.pause(true);
                } else if (Action.MUSIC_ACTION_NEXT.equals(action)) {
                    localMusicPlayer.next();
                } else if (Action.MUSIC_ACTION_PREVIOUS.equals(action)) {
                    localMusicPlayer.previous();
                } else if (SEEK_KEY_EVENTS_MAPPING.containsKey(action)) {
                    if (Action.MUSIC_ACTION_FAST_FORWARD_START.equals(action)) {
                        localMusicPlayer.startFastForward();
                    } else if (Action.MUSIC_ACTION_FAST_FORWARD_STOP.equals(action)) {
                        localMusicPlayer.stopFastForward();
                    } else if (Action.MUSIC_ACTION_REWIND_START.equals(action)) {
                        localMusicPlayer.startRewind();
                    } else if (Action.MUSIC_ACTION_REWIND_STOP.equals(action)) {
                        localMusicPlayer.stopRewind();
                    }
                } else if (Action.MUSIC_ACTION_MODE_CHANGE.equals(action)) {
                    localMusicPlayer.shuffle(event.shuffleMode);
                }
            } else if (musicServiceHandler.isMusicPlayerActive() || !Action.MUSIC_ACTION_PLAY.equals(action)) {
                broadcastMusicEvent(event);
            } else {
                startMediaPlayer(event);
            }
        }
    }

    @DexIgnore
    private static void handleLocalPlayEvent(MusicEvent event) {
        Cursor membersCursor;
        if (isNewCollection(event)) {
            List<MusicTrackInfo> trackList = new ArrayList<>();
            if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(event.collectionType)) {
                Cursor membersCursor2 = MusicDbUtils.getPlaylistMembersCursor(event.collectionId);
                if (membersCursor2 != null) {
                    try {
                        if (membersCursor2.moveToFirst()) {
                            do {
                                trackList.add(new Builder().collectionSource(event.collectionSource).collectionType(event.collectionType).collectionId(event.collectionId).name(membersCursor2.getString(membersCursor2.getColumnIndex("title"))).author(membersCursor2.getString(membersCursor2.getColumnIndex("artist"))).album(membersCursor2.getString(membersCursor2.getColumnIndex("album"))).trackId(membersCursor2.getString(membersCursor2.getColumnIndex("SourceId"))).playbackState(MusicPlaybackState.PLAYBACK_PLAYING).isNextAllowed(Boolean.TRUE).isPreviousAllowed(Boolean.TRUE).build());
                            } while (membersCursor2.moveToNext());
                            membersCursor2.close();
                        }
                    } catch (Throwable th) {
                        IOUtils.closeObject(membersCursor2);
                        throw th;
                    }
                }
                IOUtils.closeObject(membersCursor2);
            } else {
                if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(event.collectionType)) {
                    membersCursor = MusicDbUtils.getAlbumMembersCursor(event.collectionId);
                } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(event.collectionType)) {
                    membersCursor = MusicDbUtils.getArtistMembersCursor(event.collectionId);
                } else {
                    sLogger.e("Unknown type");
                    return;
                }
                if (membersCursor != null) {
                    try {
                        if (membersCursor.moveToFirst()) {
                            do {
                                trackList.add(new Builder().collectionSource(event.collectionSource).collectionType(event.collectionType).collectionId(event.collectionId).name(membersCursor.getString(membersCursor.getColumnIndex("title"))).author(membersCursor.getString(membersCursor.getColumnIndex("artist"))).album(membersCursor.getString(membersCursor.getColumnIndex("album"))).trackId(String.valueOf(membersCursor.getInt(membersCursor.getColumnIndex("_id")))).playbackState(MusicPlaybackState.PLAYBACK_PLAYING).isNextAllowed(Boolean.TRUE).isPreviousAllowed(Boolean.TRUE).build());
                            } while (membersCursor.moveToNext());
                            membersCursor.close();
                        }
                    } catch (Throwable th2) {
                        IOUtils.closeObject(membersCursor);
                        throw th2;
                    }
                }
                IOUtils.closeObject(membersCursor);
            }
            localMusicPlayer.initWithQueue(trackList);
            if (!(event.shuffleMode == null || event.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN)) {
                localMusicPlayer.shuffle(event.shuffleMode);
            }
            if (localMusicPlayer.getListener() == null) {
                localMusicPlayer.setListener(new Anon4());
            }
        }
        if (event.index != null) {
            localMusicPlayer.play(event.index.intValue());
        } else if (event.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS) {
            localMusicPlayer.shuffle(event.shuffleMode);
            localMusicPlayer.play(0);
        } else {
            sLogger.e("No event index and not shuffling!!!");
        }
    }

    @DexIgnore
    public static void stopInternalMusicPlayer() {
        if (localMusicPlayer != null) {
            localMusicPlayer.pause(true);
        }
    }

    @DexIgnore
    private static boolean isNewCollection(MusicEvent event) {
        MusicTrackInfo currentTrackInfo = MusicServiceHandler.getInstance().getCurrentMediaTrackInfo();
        return currentTrackInfo.collectionSource != event.collectionSource || currentTrackInfo.collectionType != event.collectionType || !StringUtils.equalsOrBothEmptyAfterTrim(currentTrackInfo.collectionId, event.collectionId) || currentTrackInfo.shuffleMode != event.shuffleMode;
    }

    @DexIgnore
    private static void broadcastMusicEvent(MusicEvent event) {
        sLogger.d("broadcastMusicEvent");
        MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
        Action action = event.action;
        MusicUtils.MediaKeyEventRunnable eventRunnable = null;
        if (KEY_CODES_MAPPING.containsKey(action)) {
            eventRunnable = new MusicUtils.MediaKeyEventRunnable(event.dataSource, new KeyEvent(0, KEY_CODES_MAPPING.get(action)), true);
        } else if (SEEK_KEY_EVENTS_MAPPING.containsKey(action)) {
            MusicSeekHelper ffAndRewindRunnable = musicServiceHandler.getMusicSeekHelper();
            if (ffAndRewindRunnable != null) {
                ffAndRewindRunnable.executeMusicSeekAction(action);
                return;
            }
            eventRunnable = new MusicUtils.MediaKeyEventRunnable(event.dataSource, SEEK_KEY_EVENTS_MAPPING.get(action), false);
        }
        if (eventRunnable != null) {
            TaskManager.getInstance().execute(eventRunnable, 1);
        }
    }

    @DexIgnore
    private static void startMediaPlayer(MusicEvent event) {
        Context context = NavdyApplication.getAppContext();
        Intent intent = resolveMediaPlayerIntent(context);
        if (intent != null) {
            String packageName = intent.getPackage();
            sLogger.i("startMediaPlayer " + packageName);
            intent.addFlags(268435456);
            context.startActivity(intent);
            sendPlaybackIntents(packageName, event);
            return;
        }
        sLogger.w("startMediaPlayer couldn't create an intent to start a media player");
    }

    @DexIgnore
    private static void sendPlaybackIntents(String packageName, MusicEvent event) {
        TaskManager.getInstance().execute(new Anon5(packageName, event), 10);
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    private static Intent resolveMediaPlayerIntent(Context context) {
        String packageName = MusicServiceHandler.getInstance().getLastMusicApp();
        if (StringUtils.isEmptyAfterTrim(packageName) || !SystemUtils.isPackageInstalled(packageName)) {
            if (SystemUtils.isPackageInstalled(GlanceConstants.PANDORA)) {
                packageName = GlanceConstants.PANDORA;
            } else if (SystemUtils.isPackageInstalled(GlanceConstants.SPOTIFY)) {
                packageName = GlanceConstants.SPOTIFY;
            } else {
                packageName = GlanceConstants.GOOGLE_MUSIC;
            }
        }
        return context.getPackageManager().getLaunchIntentForPackage(packageName);
    }

    @DexIgnore
    public static synchronized void executeLongPressedKeyUp() {
        synchronized (MusicUtils.class) {
            if (startedContinuousKeyEvent != null) {
                populateAudioManagerFromContext();
                KeyEvent event = KeyEvent.changeAction(startedContinuousKeyEvent, 1);
                sLogger.v("Executing specific key event: " + event);
                dispatchMediaKeyEvent(event);
                startedContinuousKeyEvent = null;
            }
        }
    }

    @DexIgnore
    private static void dispatchMediaKeyEvent(KeyEvent event) {
        sLogger.i("Dispatching key event: " + event);
        audioManager.dispatchMediaKeyEvent(event);
    }

    @DexIgnore
    public static Bitmap getMusicPhoto(String songIdStr) {
        sLogger.d("Trying to fetch music artwork for song with ID " + songIdStr);
        Bitmap currentArtwork = MusicServiceHandler.getInstance().getCurrentMediaArtwork();
        return currentArtwork != null ? currentArtwork : getLocalMusicPhoto(songIdStr);
    }

    @DexIgnore
    public static Bitmap getLocalMusicPhoto(String songIdStr) {
        Bitmap bitmap = null;
        sLogger.d("Trying to fetch local music artwork for song with ID " + songIdStr);
        if (songIdStr == null) {
            sLogger.e("Null sent as song ID");
            return bitmap;
        }
        try {
            long songId = Long.parseLong(songIdStr);
            ContentResolver cr = NavdyApplication.getAppContext().getContentResolver();
            try {
                return getArtworkForAlbumWithSong(cr, songId);
            } catch (FileNotFoundException | IllegalArgumentException e) {
                sLogger.e("Cannot get artwork for album for the song with ID " + songIdStr, e);
                sLogger.d("Couldn't get artwork for album, trying song");
                try {
                    return getArtworkForSong(cr, songIdStr);
                } catch (NumberFormatException e2) {
                    sLogger.e("Cannot parse received song ID " + songIdStr + " to long ", e2);
                    return bitmap;
                } catch (FileNotFoundException | IllegalArgumentException | IllegalStateException e3) {
                    sLogger.e("Cannot get artwork for song with ID " + songIdStr, e3);
                    return bitmap;
                }
            }
        } catch (NumberFormatException e4) {
            sLogger.i("Cannot parse long from song's ID: " + songIdStr + " - not local song", e4);
            return bitmap;
        }
    }

    @DexIgnore
    public static boolean isDefaultArtwork(String checksum) {
        return Arrays.asList(DEFAULT_ARTWORK_CHECKSUMS).contains(checksum);
    }

    @DexIgnore
    protected static long getAlbumIdForSong(ContentResolver cr, long songId) throws IllegalArgumentException {
        Cursor song = null;
        try {
            song = cr.query(Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "album_id"}, "_id=?", new String[]{String.valueOf(songId)}, null);
            if (song != null && song.moveToFirst()) {
                return song.getLong(song.getColumnIndexOrThrow("album_id"));
            }
            throw new IllegalArgumentException("Song with ID " + songId + " not found in media store");
        } finally {
            IOUtils.closeStream(song);
        }
    }

    @DexIgnore
    private static Bitmap getArtworkForAlbumWithSong(ContentResolver cr, long songId) throws FileNotFoundException, IllegalArgumentException {
        long albumId = getAlbumIdForSong(cr, songId);
        if (albumId < 0) {
            throw new IllegalArgumentException("albumId must be positive or 0");
        }
        Uri uri = ContentUris.withAppendedId(ALBUM_ART_URI, albumId);
        if (uri == null) {
            throw new FileNotFoundException("Cannot build URI for album art");
        }
        InputStream in = cr.openInputStream(uri);
        try {
            return BitmapFactory.decodeStream(in, null, BITMAP_OPTIONS);
        } finally {
            IOUtils.closeStream(in);
        }
    }

    @DexIgnore
    public static Bitmap getArtworkForAlbum(ContentResolver cr, long albumId) {
        Bitmap bitmap = null;
        Uri uri = ContentUris.withAppendedId(ALBUM_ART_URI, albumId);
        if (uri == null) {
            sLogger.e("Couldn't get URI for album: " + albumId);
        } else {
            InputStream in = null;
            try {
                in = NavdyApplication.getAppContext().getContentResolver().openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(in, null, BITMAP_OPTIONS);
            } catch (Throwable e) {
                sLogger.e("Couldn't get album art", e);
            } finally {
                IOUtils.closeStream(in);
            }
        }
        return bitmap;
    }

    /* JADX INFO: finally extract failed */
    @DexIgnore
    public static Bitmap getArtworkForArtist(ContentResolver cr, long artistId) {
        sLogger.d("getArtworkForArtist " + artistId);
        Cursor songsCursor = null;
        try {
            songsCursor = cr.query(Audio.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "artist_id = ?", new String[]{String.valueOf(artistId)}, null);
            if (songsCursor != null && songsCursor.moveToFirst()) {
                do {
                    Bitmap bitmap = getLocalMusicPhoto(String.valueOf(songsCursor.getInt(0)));
                    if (bitmap != null) {
                        songsCursor.close();
                        IOUtils.closeStream(songsCursor);
                        return bitmap;
                    }
                } while (songsCursor.moveToNext());
                songsCursor.close();
            }
            IOUtils.closeStream(songsCursor);
            return null;
        } catch (Throwable th) {
            IOUtils.closeStream(songsCursor);
            throw th;
        }
    }

    @DexIgnore
    public static Bitmap getArtworkForPlaylist(ContentResolver cr, long playlistId) {
        Cursor membersCursor = MusicDbUtils.getPlaylistMembersCursor((int) playlistId);
        if (membersCursor != null) {
            try {
                if (membersCursor.moveToFirst()) {
                    Bitmap bitmap = getLocalMusicPhoto(membersCursor.getString(membersCursor.getColumnIndex("SourceId")));
                    if (bitmap != null) {
                        membersCursor.close();
                        return bitmap;
                    }
                    membersCursor.close();
                }
            } finally {
                IOUtils.closeStream(membersCursor);
            }
        }
        IOUtils.closeStream(membersCursor);
        return null;
    }

    @DexIgnore
    private static Bitmap getArtworkForSong(ContentResolver cr, String songIdStr) throws IllegalArgumentException, FileNotFoundException, IllegalStateException {
        sLogger.d("getArtworkForSong " + songIdStr);
        if (Long.parseLong(songIdStr) >= 0) {
            return getBitmapFromUri(cr, Uri.parse(SONG_URI + songIdStr + SONG_ART_URI_POSTFIX));
        }
        throw new IllegalArgumentException("songId must be positive or 0");
    }

    @DexIgnore
    private static Bitmap getBitmapFromUri(ContentResolver cr, Uri uri) throws FileNotFoundException {
        sLogger.d("getBitmapFromUri " + uri);
        ParcelFileDescriptor pfd = cr.openFileDescriptor(uri, "r");
        if (pfd == null) {
            throw new FileNotFoundException("Cannot open ParcelFileDescriptor for URI: " + uri);
        }
        try {
            return BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor(), null, BITMAP_OPTIONS);
        } finally {
            IOUtils.closeStream(pfd);
        }
    }
}
