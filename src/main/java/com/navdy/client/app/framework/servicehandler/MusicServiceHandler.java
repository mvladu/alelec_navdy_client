package com.navdy.client.app.framework.servicehandler;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.here.odnp.config.OdnpConfigStatic;
import com.navdy.client.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.MusicDbUtils;
import com.navdy.client.app.framework.util.MusicUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.framework.util.ThrottlingLogger;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicCapabilitiesRequest;
import com.navdy.service.library.events.audio.MusicCapabilitiesResponse;
import com.navdy.service.library.events.audio.MusicCapability;
import com.navdy.service.library.events.audio.MusicCharacterMap;
import com.navdy.service.library.events.audio.MusicCollectionInfo;
import com.navdy.service.library.events.audio.MusicCollectionRequest;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfoRequest;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.photo.PhotoUpdate;
import com.navdy.service.library.events.photo.PhotoUpdatesRequest;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.MusicDataUtils;
import com.navdy.service.library.util.ScalingUtilities;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;

import org.droidparts.contract.SQL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexPrepend;
import lanchon.dexpatcher.annotation.DexReplace;
import okio.ByteString;

@DexEdit(defaultAction = DexAction.ADD)
public class MusicServiceHandler {
    @DexIgnore
    public static /* final */ MusicTrackInfo EMPTY_TRACK_INFO; // = new com.navdy.service.library.events.audio.MusicTrackInfo.Builder().dataSource(com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE).playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).build();
    @DexIgnore
    public static /* final */ int INDEX_PLAY_LIST_RETRY_INTERVAL; // = 10000;
    @DexIgnore
    private static /* final */ int MAX_RETRIES_TO_INDEX_PLAY_LISTS; // = 5;
    @DexIgnore
    public static /* final */ int PLAY_LIST_INDEXING_DELAY_MILLIS; // = 5000;
    @DexIgnore
    private static /* final */ int THROTTLING_LOGGER_WAIT; // = 5000;
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.servicehandler.MusicServiceHandler.class);
    @DexIgnore
    private static MusicServiceHandler singleton; // = null;
    @DexIgnore
    private static /* final */ ThrottlingLogger throttlingLogger; // = new com.navdy.client.app.framework.util.ThrottlingLogger(sLogger, com.here.odnp.config.OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL);
    @DexIgnore
    private String currentArtworkHash;
    @DexIgnore
    private Bitmap currentMediaArtwork = null;
    // @android.support.annotation.NonNull
    @DexIgnore
    private MusicTrackInfo currentMediaTrackInfo = EMPTY_TRACK_INFO;
    @DexIgnore
    private volatile int indexingRetries = 0;
    @DexIgnore
    private Handler musicObserverHandler;
    @DexIgnore
    private Looper musicObserverLooper;
    @DexIgnore
    private MusicServiceHandler.MusicSeekHelper musicSeekHelper = null;
    @DexIgnore
    private boolean postPhotoUpdates = false;
    @DexIgnore
    private Runnable retryIndexingPlayListsRunnable = new com.navdy.client.app.framework.servicehandler.MusicServiceHandler.Anon1();
    @DexIgnore
    private boolean shouldPerformFullPlaylistIndex = true;

    @DexEdit(defaultAction = DexAction.ADD)
    class Anon1 implements Runnable {
        @DexReplace
        Anon1() {
        }

        @DexReplace
        public void run() {
            MusicServiceHandler.this.indexingRetries = MusicServiceHandler.this.indexingRetries + 1;
            MusicServiceHandler.sLogger.e("Retrying initialization of the playlist data base");
            MusicServiceHandler.this.indexAllPlaylists(true);
        }
    }

    @DexAdd
    class RunIndex implements Runnable {
        final MusicServiceHandler val$this;

        RunIndex(MusicServiceHandler _this) {
            val$this = _this;
        }

        public void run() {
           val$this._indexAllPlaylists(true);
        }
    }

    // @DexIgnore
    // class Anon3 implements Runnable {
    //     @DexIgnore
    //     final /* synthetic */ MusicCollectionRequest val$request;
    //
    //     @DexIgnore
    //     Anon3(MusicCollectionRequest musicCollectionRequest) {
    //         this.val$request = musicCollectionRequest;
    //     }
    //
    //     /* JADX WARNING: Removed duplicated region for block: B:161:0x05b9  */
    //     /* JADX WARNING: Removed duplicated region for block: B:211:0x0707  */
    //     /* JADX WARNING: Removed duplicated region for block: B:248:0x0819  */
    //     @DexIgnore
    //     public void run() {
    //     //     Cursor membersCursor;
    //     //     Cursor membersCursor2;
    //     //     MusicServiceHandler.sLogger.d("onMusicCollectionRequest " + this.val$request);
    //     //     int offset = this.val$request.offset != null ? this.val$request.offset : 0;
    //     //     int limit = this.val$request.limit != null ? this.val$request.limit : 0;
    //     //     int count = 0;
    //     //     boolean canShuffle = true;
    //     //     List<MusicCollectionInfo> collectionList = null;
    //     //     ArrayList arrayList = null;
    //     //     List<MusicCharacterMap> characterMap = null;
    //     //     if (MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(this.val$request.collectionSource)) {
    //     //         if (this.val$request.collectionId != null) {
    //     //             if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(this.val$request.collectionType)) {
    //     //                 Cursor membersCursor3 = MusicDbUtils.getPlaylistMembersCursor(this.val$request.collectionId);
    //     //                 if (membersCursor3 != null) {
    //     //                     try {
    //     //                         if (membersCursor3.moveToPosition(offset)) {
    //     //                             count = membersCursor3.getCount();
    //     //                             if (limit == 0) {
    //     //                                 limit = count;
    //     //                             }
    //     //                             ArrayList arrayList2 = new ArrayList(limit);
    //     //                             do {
    //     //                                 try {
    //     //                                     arrayList2.add(new MusicTrackInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS).collectionId(this.val$request.collectionId).playbackState(MusicPlaybackState.PLAYBACK_NONE).name(membersCursor3.getString(membersCursor3.getColumnIndex("title"))).author(membersCursor3.getString(membersCursor3.getColumnIndex("artist"))).album(membersCursor3.getString(membersCursor3.getColumnIndex("album"))).trackId(String.valueOf(membersCursor3.getInt(membersCursor3.getColumnIndex("SourceId")))).build());
    //     //                                     if (!membersCursor3.moveToNext()) {
    //     //                                         break;
    //     //                                     }
    //     //                                 } catch (Throwable th) {
    //     //                                     th = th;
    //     //                                     ArrayList arrayList3 = arrayList2;
    //     //                                     IOUtils.closeObject(membersCursor3);
    //     //                                     throw th;
    //     //                                 }
    //     //                             } while (arrayList2.size() < limit);
    //     //                             arrayList = arrayList2;
    //     //                         }
    //     //                     } catch (Throwable th2) {
    //     //                         t = th2;
    //     //                         MusicServiceHandler.sLogger.e("Error querying GPM database: " + t);
    //     //                         IOUtils.closeObject(membersCursor3);
    //     //                         DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(count).canShuffle(canShuffle).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
    //     //                     }
    //     //                 }
    //     //                 IOUtils.closeObject(membersCursor3);
    //     //             } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(this.val$request.collectionType)) {
    //     //                 boolean groupByAlbums = Wire.get(this.val$request.groupBy, MusicCollectionType.COLLECTION_TYPE_UNKNOWN) == MusicCollectionType.COLLECTION_TYPE_ALBUMS;
    //     //                 Cursor albumsCursor = MusicDbUtils.getArtistsAlbums(this.val$request.collectionId);
    //     //                 Cursor artistTracksCursor = null;
    //     //                 if (groupByAlbums) {
    //     //                     if (albumsCursor != null) {
    //     //                         try {
    //     //                             if (albumsCursor.moveToPosition(offset)) {
    //     //                                 count = albumsCursor.getCount();
    //     //                                 if (limit == 0) {
    //     //                                     limit = count;
    //     //                                 }
    //     //                                 List<MusicCollectionInfo> collectionList2 = new ArrayList<>(limit);
    //     //                                 do {
    //     //                                     try {
    //     //                                         collectionList2.add(new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(String.valueOf(albumsCursor.getString(albumsCursor.getColumnIndex("_id")))).name(albumsCursor.getString(albumsCursor.getColumnIndex("album"))).size(albumsCursor.getInt(albumsCursor.getColumnIndex("numsongs"))).build());
    //     //                                         if (!albumsCursor.moveToNext()) {
    //     //                                             break;
    //     //                                         }
    //     //                                     } catch (Throwable th3) {
    //     //                                         th = th3;
    //     //                                         List<MusicCollectionInfo> list = collectionList2;
    //     //                                         IOUtils.closeObject(albumsCursor);
    //     //                                         IOUtils.closeObject(artistTracksCursor);
    //     //                                         throw th;
    //     //                                     }
    //     //                                 } while (collectionList2.size() < limit);
    //     //                                 collectionList = collectionList2;
    //     //                             }
    //     //                         } catch (Throwable th4) {
    //     //                             t = th4;
    //     //                             MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
    //     //                             IOUtils.closeObject(albumsCursor);
    //     //                             IOUtils.closeObject(artistTracksCursor);
    //     //                             DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(count).canShuffle(canShuffle).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
    //     //                         }
    //     //                     }
    //     //                     canShuffle = MusicDbUtils.getArtistSongsCount(this.val$request.collectionId) > 1;
    //     //                 } else {
    //     //                     artistTracksCursor = MusicDbUtils.getArtistMembersCursor(this.val$request.collectionId);
    //     //                     if (artistTracksCursor != null && artistTracksCursor.moveToPosition(offset)) {
    //     //                         count = artistTracksCursor.getCount();
    //     //                         if (limit == 0) {
    //     //                             limit = count;
    //     //                         }
    //     //                         ArrayList arrayList4 = new ArrayList(limit);
    //     //                         do {
    //     //                             try {
    //     //                                 arrayList4.add(new MusicTrackInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ARTISTS).collectionId(this.val$request.collectionId).playbackState(MusicPlaybackState.PLAYBACK_NONE).name(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("title"))).author(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("artist"))).album(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("album"))).trackId(String.valueOf(artistTracksCursor.getInt(artistTracksCursor.getColumnIndex("_id")))).build());
    //     //                                 if (!artistTracksCursor.moveToNext()) {
    //     //                                     break;
    //     //                                 }
    //     //                             } catch (Throwable th5) {
    //     //                                 th = th5;
    //     //                                 ArrayList arrayList5 = arrayList4;
    //     //                                 IOUtils.closeObject(albumsCursor);
    //     //                                 IOUtils.closeObject(artistTracksCursor);
    //     //                                 throw th;
    //     //                             }
    //     //                         } while (arrayList4.size() < limit);
    //     //                         arrayList = arrayList4;
    //     //                     }
    //     //                     if (artistTracksCursor != null) {
    //     //                         if (artistTracksCursor.moveToFirst() && artistTracksCursor.getCount() > 1) {
    //     //                             canShuffle = true;
    //     //                         }
    //     //                     }
    //     //                     canShuffle = false;
    //     //                 }
    //     //                 IOUtils.closeObject(albumsCursor);
    //     //                 IOUtils.closeObject(artistTracksCursor);
    //     //             } else if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(this.val$request.collectionType)) {
    //     //                 Cursor membersCursor4 = MusicDbUtils.getAlbumMembersCursor(this.val$request.collectionId);
    //     //                 if (membersCursor4 != null) {
    //     //                     try {
    //     //                         if (membersCursor4.moveToPosition(offset)) {
    //     //                             count = membersCursor4.getCount();
    //     //                             if (limit == 0) {
    //     //                                 limit = count;
    //     //                             }
    //     //                             ArrayList arrayList6 = new ArrayList(limit);
    //     //                             do {
    //     //                                 try {
    //     //                                     arrayList6.add(new MusicTrackInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(this.val$request.collectionId).playbackState(MusicPlaybackState.PLAYBACK_NONE).name(membersCursor4.getString(membersCursor4.getColumnIndex("title"))).author(membersCursor4.getString(membersCursor4.getColumnIndex("artist"))).album(membersCursor4.getString(membersCursor4.getColumnIndex("album"))).trackId(String.valueOf(membersCursor4.getInt(membersCursor4.getColumnIndex("_id")))).build());
    //     //                                     if (!membersCursor4.moveToNext()) {
    //     //                                         break;
    //     //                                     }
    //     //                                 } catch (Throwable th6) {
    //     //                                     th = th6;
    //     //                                     ArrayList arrayList7 = arrayList6;
    //     //                                     IOUtils.closeObject(membersCursor4);
    //     //                                     throw th;
    //     //                                 }
    //     //                             } while (arrayList6.size() < limit);
    //     //                             arrayList = arrayList6;
    //     //                         }
    //     //                     } catch (Throwable th7) {
    //     //                         t = th7;
    //     //                         MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
    //     //                         IOUtils.closeObject(membersCursor4);
    //     //                         DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(count).canShuffle(canShuffle).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
    //     //                     }
    //     //                 }
    //     //                 IOUtils.closeObject(membersCursor4);
    //     //             }
    //     //         } else if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(this.val$request.collectionType)) {
    //     //             Cursor playlistsCursor = MusicDbUtils.getPlaylistsCursor();
    //     //             if (playlistsCursor != null) {
    //     //                 try {
    //     //                     if (playlistsCursor.moveToPosition(offset)) {
    //     //                         count = playlistsCursor.getCount();
    //     //                         if (limit == 0) {
    //     //                             limit = count;
    //     //                         }
    //     //                         List<MusicCollectionInfo> collectionList3 = new ArrayList<>(limit);
    //     //                         do {
    //     //                             try {
    //     //                                 int playlistId = playlistsCursor.getInt(playlistsCursor.getColumnIndex("playlist_id"));
    //     //                                 String playlistName = playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name"));
    //     //                                 membersCursor2 = MusicDbUtils.getPlaylistMembersCursor(playlistId);
    //     //                                 if (membersCursor2 != null) {
    //     //                                     if (membersCursor2.moveToFirst()) {
    //     //                                         collectionList3.add(new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS).collectionId(String.valueOf(playlistId)).name(playlistName).size(membersCursor2.getCount()).build());
    //     //                                     }
    //     //                                 }
    //     //                                 IOUtils.closeObject(membersCursor2);
    //     //                                 if (!playlistsCursor.moveToNext()) {
    //     //                                     break;
    //     //                                 }
    //     //                             } catch (Throwable th8) {
    //     //                                 th = th8;
    //     //                                 List<MusicCollectionInfo> list2 = collectionList3;
    //     //                                 IOUtils.closeObject(playlistsCursor);
    //     //                                 throw th;
    //     //                             }
    //     //                         } while (collectionList3.size() < limit);
    //     //                         collectionList = collectionList3;
    //     //                     }
    //     //                 } catch (Throwable th9) {
    //     //                     t = th9;
    //     //                     try {
    //     //                         MusicServiceHandler.sLogger.e("Error querying GPM database: " + t);
    //     //                         IOUtils.closeObject(playlistsCursor);
    //     //                         if (Wire.get(this.val$request.includeCharacterMap, Boolean.FALSE)) {
    //     //                         }
    //     //                         DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(count).canShuffle(canShuffle).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
    //     //                     } catch (Throwable th10) {
    //     //                         th = th10;
    //     //                         IOUtils.closeObject(playlistsCursor);
    //     //                         throw th;
    //     //                     }
    //     //                 }
    //     //             }
    //     //             IOUtils.closeObject(playlistsCursor);
    //     //             if (Wire.get(this.val$request.includeCharacterMap, Boolean.FALSE)) {
    //     //                 characterMap = MusicServiceHandler.this.createCharacterMap(MusicDbUtils.getPlaylistsCharacterMapCursor());
    //     //             }
    //     //         } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(this.val$request.collectionType)) {
    //     //             Cursor artistsCursor = MusicDbUtils.getArtistsCursor();
    //     //             boolean groupByAlbums2 = Wire.get(this.val$request.groupBy, MusicCollectionType.COLLECTION_TYPE_UNKNOWN) == MusicCollectionType.COLLECTION_TYPE_ALBUMS;
    //     //             if (artistsCursor != null) {
    //     //                 try {
    //     //                     if (artistsCursor.moveToPosition(offset)) {
    //     //                         count = artistsCursor.getCount();
    //     //                         if (limit == 0) {
    //     //                             limit = count;
    //     //                         }
    //     //                         List<MusicCollectionInfo> collectionList4 = new ArrayList<>(limit);
    //     //                         do {
    //     //                             try {
    //     //                                 String artistId = artistsCursor.getString(artistsCursor.getColumnIndex("_id"));
    //     //                                 String artistName = artistsCursor.getString(artistsCursor.getColumnIndex("artist"));
    //     //                                 membersCursor = groupByAlbums2 ? MusicDbUtils.getArtistsAlbums(artistId) : MusicDbUtils.getArtistMembersCursor(artistId);
    //     //                                 if (membersCursor != null) {
    //     //                                     if (membersCursor.moveToFirst()) {
    //     //                                         int albumsCount = membersCursor.getCount();
    //     //                                         MusicCollectionInfo.Builder infoBuilder = new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ARTISTS).collectionId(String.valueOf(artistId)).name(artistName).size(membersCursor.getCount());
    //     //                                         if (groupByAlbums2) {
    //     //                                             infoBuilder.subtitle(NavdyApplication.getAppContext().getResources().getQuantityString(R.plurals.album_count, albumsCount, albumsCount));
    //     //                                         }
    //     //                                         collectionList4.add(infoBuilder.build());
    //     //                                     }
    //     //                                 }
    //     //                                 IOUtils.closeObject(membersCursor);
    //     //                                 if (!artistsCursor.moveToNext()) {
    //     //                                     break;
    //     //                                 }
    //     //                             } catch (Throwable th11) {
    //     //                                 th = th11;
    //     //                                 List<MusicCollectionInfo> list3 = collectionList4;
    //     //                                 IOUtils.closeObject(artistsCursor);
    //     //                                 throw th;
    //     //                             }
    //     //                         } while (collectionList4.size() < limit);
    //     //                         collectionList = collectionList4;
    //     //                     }
    //     //                 } catch (Throwable th12) {
    //     //                     t = th12;
    //     //                     MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
    //     //                     IOUtils.closeObject(artistsCursor);
    //     //                     if (Wire.get(this.val$request.includeCharacterMap, Boolean.FALSE)) {
    //     //                     }
    //     //                     DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(count).canShuffle(canShuffle).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
    //     //                 }
    //     //             }
    //     //             IOUtils.closeObject(artistsCursor);
    //     //             if (Wire.get(this.val$request.includeCharacterMap, Boolean.FALSE)) {
    //     //                 characterMap = MusicServiceHandler.this.createCharacterMap(MusicDbUtils.getArtistsCharacterMapCursor());
    //     //             }
    //     //         } else if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(this.val$request.collectionType)) {
    //     //             Cursor albumsCursor2 = MusicDbUtils.getAlbumsCursor();
    //     //             if (albumsCursor2 != null) {
    //     //                 try {
    //     //                     if (albumsCursor2.moveToPosition(offset)) {
    //     //                         count = albumsCursor2.getCount();
    //     //                         if (limit == 0) {
    //     //                             limit = count;
    //     //                         }
    //     //                         List<MusicCollectionInfo> collectionList5 = new ArrayList<>(limit);
    //     //                         do {
    //     //                             try {
    //     //                                 collectionList5.add(new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(String.valueOf(albumsCursor2.getString(albumsCursor2.getColumnIndex("_id")))).name(albumsCursor2.getString(albumsCursor2.getColumnIndex("album"))).size(albumsCursor2.getInt(albumsCursor2.getColumnIndex("numsongs"))).subtitle(albumsCursor2.getString(albumsCursor2.getColumnIndex("artist"))).build());
    //     //                                 if (!albumsCursor2.moveToNext()) {
    //     //                                     break;
    //     //                                 }
    //     //                             } catch (Throwable th13) {
    //     //                                 th = th13;
    //     //                                 List<MusicCollectionInfo> list4 = collectionList5;
    //     //                                 IOUtils.closeObject(albumsCursor2);
    //     //                                 throw th;
    //     //                             }
    //     //                         } while (collectionList5.size() < limit);
    //     //                         collectionList = collectionList5;
    //     //                     }
    //     //                 } catch (Throwable th14) {
    //     //                     t = th14;
    //     //                     MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
    //     //                     IOUtils.closeObject(albumsCursor2);
    //     //                     if (Wire.get(this.val$request.includeCharacterMap, Boolean.FALSE)) {
    //     //                     }
    //     //                     DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(count).canShuffle(canShuffle).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
    //     //                 }
    //     //             }
    //     //             IOUtils.closeObject(albumsCursor2);
    //     //             if (Wire.get(this.val$request.includeCharacterMap, Boolean.FALSE)) {
    //     //                 characterMap = MusicServiceHandler.this.createCharacterMap(MusicDbUtils.getAlbumsCharacterMapCursor());
    //     //             }
    //     //         }
    //     //     }
    //     //     DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).size(count).canShuffle(canShuffle).build()).musicTracks(arrayList).musicCollections(collectionList).characterMap(characterMap).build());
    //     }
    // }
    //
    // @DexIgnore
    // class Anon4 implements Runnable {
    //     final /* synthetic */ MusicArtworkRequest val$request;
    //
    //     @DexIgnore
    //     Anon4(MusicArtworkRequest musicArtworkRequest) {
    //         this.val$request = musicArtworkRequest;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         if (StringUtils.isEmptyAfterTrim(this.val$request.collectionId)) {
    //             MusicServiceHandler.sLogger.e("This artwork request is only for collections");
    //             return;
    //         }
    //         try {
    //             long collectionId = (long) Integer.parseInt(this.val$request.collectionId);
    //             Bitmap bitmap = null;
    //             ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
    //             if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(this.val$request.collectionType)) {
    //                 bitmap = MusicUtils.getArtworkForAlbum(contentResolver, collectionId);
    //             } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(this.val$request.collectionType)) {
    //                 bitmap = MusicUtils.getArtworkForArtist(contentResolver, collectionId);
    //             } else if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(this.val$request.collectionType)) {
    //                 bitmap = MusicUtils.getArtworkForPlaylist(contentResolver, collectionId);
    //             }
    //             if (bitmap == null || bitmap.getByteCount() == 0) {
    //                 MusicServiceHandler.sLogger.i("Received photo has null or empty byte array");
    //             }
    //             Bitmap artwork = null;
    //             ByteString photo = null;
    //             if (bitmap != null) {
    //                 try {
    //                     artwork = ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap, this.val$request.size, this.val$request.size, ScalingUtilities.ScalingLogic.FIT);
    //                     byte[] data = IOUtils.bitmap2ByteBuffer(artwork);
    //                     if (data == null) {
    //                         MusicServiceHandler.sLogger.e("Error artworking");
    //                         if (artwork != null) {
    //                             artwork.recycle();
    //                             return;
    //                         }
    //                         return;
    //                     }
    //                     photo = ByteString.of(data);
    //                 } catch (Exception e) {
    //                     MusicServiceHandler.sLogger.e("Error updating the artwork", e);
    //                     if (artwork != null) {
    //                         artwork.recycle();
    //                         return;
    //                     }
    //                     return;
    //                 } catch (Throwable th) {
    //                     if (artwork != null) {
    //                         artwork.recycle();
    //                     }
    //                     throw th;
    //                 }
    //             }
    //             DeviceConnection.postEvent(new MusicArtworkResponse.Builder().collectionSource(this.val$request.collectionSource).collectionType(this.val$request.collectionType).collectionId(this.val$request.collectionId).photo(photo).build());
    //             if (artwork != null) {
    //                 artwork.recycle();
    //             }
    //         } catch (Throwable e2) {
    //             MusicServiceHandler.sLogger.e("Couldn't parse " + this.val$request.collectionId, e2);
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // class Anon5 implements Runnable {
    //     @DexIgnore
    //     Anon5() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         MusicServiceHandler.this.indexAllPlaylists(false);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon6 implements Runnable {
    //     @DexIgnore
    //     Anon6() {
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         Cursor membersCursor;
    //         Cursor playlistsCursor = NavdyContentProvider.getPlaylistsCursor();
    //         if (playlistsCursor != null) {
    //             try {
    //                 if (playlistsCursor.moveToFirst()) {
    //                     do {
    //                         int playlistId = playlistsCursor.getInt(playlistsCursor.getColumnIndex("playlist_id"));
    //                         String playlistName = playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name"));
    //                         membersCursor = NavdyContentProvider.getPlaylistMembersCursor(playlistId);
    //                         if (membersCursor != null) {
    //                             if (membersCursor.moveToFirst()) {
    //                                 int count = 0;
    //                                 do {
    //                                     String artist = membersCursor.getString(membersCursor.getColumnIndex("artist"));
    //                                     String album = membersCursor.getString(membersCursor.getColumnIndex("album"));
    //                                     String title = membersCursor.getString(membersCursor.getColumnIndex("title"));
    //                                     String sourceId = membersCursor.getString(membersCursor.getColumnIndex("SourceId"));
    //                                     if (sourceId.matches("\\d+")) {
    //                                         if (count == 0) {
    //                                             MusicServiceHandler.sLogger.d("Playlist ID: " + playlistId + ", Name: " + playlistName);
    //                                         }
    //                                         MusicServiceHandler.sLogger.d("- Artist: " + artist + ", Album: " + album + ", Title: " + title + ", ID: " + sourceId);
    //                                         count++;
    //                                     }
    //                                 } while (membersCursor.moveToNext());
    //                             }
    //                         }
    //                         IOUtils.closeStream(membersCursor);
    //                     } while (playlistsCursor.moveToNext());
    //                 }
    //             } catch (Throwable th) {
    //                 IOUtils.closeStream(playlistsCursor);
    //                 throw th;
    //             }
    //         }
    //         IOUtils.closeStream(playlistsCursor);
    //     }
    // }

    @DexIgnore
    private class GpmPlaylistObserver extends ContentObserver {
        @DexIgnore
        Pattern pattern; // = Pattern.compile("/playlists/(\\d+)/?.*");
        // @DexIgnore
        // MusicServiceHandler.GpmPlaylistObserver.GpmObserverRunnable runnable; // = new MusicServiceHandler.GpmPlaylistObserver.GpmObserverRunnable(this, null);

        // @DexIgnore
        // private class GpmObserverRunnable implements Runnable {
        //     @DexIgnore
        //     Integer playlistId;
        //
        //     @DexIgnore
        //     private GpmObserverRunnable() {
        //     }
        //
        //     @DexIgnore
        //     /* synthetic */ GpmObserverRunnable(MusicServiceHandler.GpmPlaylistObserver x0, MusicServiceHandler.Anon1 x1) {
        //         this();
        //     }
        //
        //     @DexIgnore
        //     public void run() {
        //         if (this.playlistId != null) {
        //             MusicServiceHandler.this.indexPlaylist(this.playlistId);
        //         } else {
        //             MusicServiceHandler.this.indexAllPlaylists(true);
        //         }
        //     }
        //
        //     @DexIgnore
        //     void setPlaylistId(Integer playlistId2) {
        //         this.playlistId = playlistId2;
        //     }
        // }

        @DexIgnore
        GpmPlaylistObserver() {
            super(MusicServiceHandler.this.musicObserverHandler);
        }

        @DexIgnore
        public void onChange(boolean selfChange, Uri uri) {
            // String path = uri.getPath();
            // MusicServiceHandler.sLogger.d("onChange: " + path + SQL.DDL.OPENING_BRACE + uri + ")");
            // if (StringUtils.isEmptyAfterTrim(path)) {
            //     MusicServiceHandler.this.musicObserverHandler.removeCallbacks(this.runnable);
            //     this.runnable.setPlaylistId(null);
            //     MusicServiceHandler.this.musicObserverHandler.postDelayed(this.runnable, 200);
            //     return;
            // }
            // Matcher matcher = this.pattern.matcher(path);
            // if (matcher.matches()) {
            //     MusicServiceHandler.sLogger.d("Match: " + matcher.group());
            //     String playlistIdStr = matcher.group(1);
            //     try {
            //         int playlistId = Integer.parseInt(playlistIdStr);
            //         MusicServiceHandler.this.musicObserverHandler.removeCallbacks(this.runnable);
            //         this.runnable.setPlaylistId(playlistId);
            //         MusicServiceHandler.this.musicObserverHandler.postDelayed(this.runnable, 200);
            //     } catch (NumberFormatException e) {
            //         MusicServiceHandler.sLogger.e("Can't convert to integer: " + playlistIdStr);
            //     }
            // }
        }
    }

    @DexIgnore
    public interface MusicSeekHelper {
        @DexIgnore
        void executeMusicSeekAction(MusicEvent.Action action);
    }

    @DexIgnore
    public MusicServiceHandler.MusicSeekHelper getMusicSeekHelper() {
        return this.musicSeekHelper;
    }

    @DexIgnore
    public void setMusicSeekHelper(MusicServiceHandler.MusicSeekHelper musicSeekHelper2) {
        this.musicSeekHelper = musicSeekHelper2;
    }


    @DexReplace
    private MusicServiceHandler() {
        sLogger.d("MusicServiceHandler ctor");
        BusProvider.getInstance().register(this);
        HandlerThread thread = new HandlerThread("MusicObserver");
        thread.start();
        this.musicObserverLooper = thread.getLooper();
        this.musicObserverHandler = new Handler(this.musicObserverLooper);
        MusicServiceHandler.GpmPlaylistObserver gpmPlaylistObserver = new MusicServiceHandler.GpmPlaylistObserver();
        try {
            NavdyApplication.getAppContext().getContentResolver().registerContentObserver(MusicDbUtils.getGpmPlaylistsUri(), true, gpmPlaylistObserver);
        } catch (SecurityException ex) {
            sLogger.e("MusicServiceHandler failed to registerContentObserver", ex);
        }
        this.musicObserverHandler.postDelayed(new MusicServiceHandler.RunIndex(this), PLAY_LIST_INDEXING_DELAY_MILLIS);
    }

    @DexIgnore
    protected void finalize() throws Throwable {
        super.finalize();
        this.musicObserverLooper.quit();
    }

    @DexIgnore
    public static synchronized MusicServiceHandler getInstance() {
        MusicServiceHandler musicServiceHandler;
        synchronized (MusicServiceHandler.class) {
            if (singleton == null) {
                singleton = new MusicServiceHandler();
            }
            musicServiceHandler = singleton;
        }
        return musicServiceHandler;
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onHUDDisconnected(DeviceConnection.DeviceDisconnectedEvent event) {
        sLogger.v("HUD is disconnected - reset internal state");
        this.postPhotoUpdates = false;
        MusicUtils.executeLongPressedKeyUp();
        MusicUtils.stopInternalMusicPlayer();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onHUDConnected(DeviceConnection.DeviceConnectedEvent event) {
        sLogger.v("HUD is connected - doing some initial stuff for music");
        this.postPhotoUpdates = true;
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicCollectionRequest(MusicCollectionRequest request) {
        // TaskManager.getInstance().execute(new MusicServiceHandler.Anon3(request), 1);
    }

    @DexIgnore
    private List<MusicCharacterMap> createCharacterMap(Cursor cursor) {
        List<MusicCharacterMap> characterMap = null;
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    List<MusicCharacterMap> characterMap2 = new ArrayList<>();
                    int characterOffset = 0;
                    boolean indexedNumbers = false;
                    boolean indexedSymbols = false;
                    do {
                        boolean shouldAdd = false;
                        try {
                            String c = cursor.getString(cursor.getColumnIndex(MusicDbUtils.LETTER_COLUMN));
                            int count = cursor.getInt(cursor.getColumnIndex(MusicDbUtils.COUNT_COLUMN));
                            if (StringUtils.isEmptyAfterTrim(c) || c.length() > 1) {
                                sLogger.e("Bad value in character map cursor: " + c);
                            } else {
                                if (c.matches("[A-Z]")) {
                                    shouldAdd = true;
                                } else if (c.matches("\\d")) {
                                    if (!indexedNumbers) {
                                        c = "#";
                                        indexedNumbers = true;
                                        shouldAdd = true;
                                    }
                                } else if (!indexedSymbols) {
                                    c = "%";
                                    indexedSymbols = true;
                                    shouldAdd = true;
                                }
                                if (shouldAdd) {
                                    characterMap2.add(new MusicCharacterMap.Builder().character(c).offset(characterOffset).build());
                                }
                                characterOffset += count;
                            }
                        } catch (Throwable th) {
                            List<MusicCharacterMap> list = characterMap2;
                            IOUtils.closeStream(cursor);
                            throw th;
                        }
                    } while (cursor.moveToNext());
                    characterMap = characterMap2;
                }
            } catch (Throwable th2) {
                sLogger.e("Couldn't generate character map: " + th2);
                IOUtils.closeStream(cursor);
                return characterMap;
            }
        }
        IOUtils.closeStream(cursor);
        return characterMap;
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicCapabilitiesRequest(MusicCapabilitiesRequest musicCapabilitiesRequest) {
        sLogger.d("onMusicCapabilitiesRequest");
        DeviceConnection.postEvent(getMusicCapabilities());
    }

    @DexIgnore
    public static MusicCapabilitiesResponse getMusicCapabilities() {
        MusicCapability.MusicAuthorizationStatus authorizationStatus;
        if (BaseActivity.weHaveStoragePermission()) {
            authorizationStatus = MusicCapability.MusicAuthorizationStatus.MUSIC_AUTHORIZATION_AUTHORIZED;
        } else {
            authorizationStatus = MusicCapability.MusicAuthorizationStatus.MUSIC_AUTHORIZATION_DENIED;
        }
        List<MusicCollectionType> collectionTypes = new ArrayList<>();
        collectionTypes.add(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS);
        collectionTypes.add(MusicCollectionType.COLLECTION_TYPE_ARTISTS);
        collectionTypes.add(MusicCollectionType.COLLECTION_TYPE_ALBUMS);
        List<MusicCapability> capabilitiesList = new ArrayList<>();
        capabilitiesList.add(new MusicCapability.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionTypes(collectionTypes).authorizationStatus(authorizationStatus).build());
        return new MusicCapabilitiesResponse.Builder().capabilities(capabilitiesList).build();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicArtworkRequest(MusicArtworkRequest request) {
        // sLogger.d("onMusicArtworkRequest " + request);
        // TaskManager.getInstance().execute(new MusicServiceHandler.Anon4(request), 1);
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicTrackInfoRequest(MusicTrackInfoRequest request) {
        sLogger.d("Request for track info received.");
        sendCurrentTrackInfo();
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onMusicEvent(MusicEvent event) {
        sLogger.d("Request for performing some action to the music received.");
        try {
            MusicUtils.executeMusicAction(event);
        } catch (IOException e) {
            sLogger.e("Cannot load music", e);
        } catch (PackageManager.NameNotFoundException e2) {
            sLogger.e("Cannot start music player", e2);
        } catch (UnsupportedOperationException e3) {
            sLogger.e("Cannot execute action", e3);
        }
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onPhotoUpdatesRequest(PhotoUpdatesRequest request) {
        sLogger.d("Request for changing photo updating status received with value " + request.start + " for imageResourceIds of type " + request.photoType);
        if (PhotoType.PHOTO_ALBUM_ART.equals(request.photoType)) {
            this.postPhotoUpdates = request.start;
            sendCurrentMediaArtworkAsUpdate();
        }
    }

    @DexIgnore
    private void sendRemoteMessage(Message message) {
        sLogger.v("Sending message to the HUD: " + message);
        DeviceConnection.postEvent(message);
    }

    @DexIgnore
    private void sendCurrentTrackInfo() {
        sendRemoteMessage(getCurrentMediaTrackInfo());
    }

    // @android.support.annotation.NonNull
    @DexIgnore
    public synchronized MusicTrackInfo getCurrentMediaTrackInfo() {
        return this.currentMediaTrackInfo;
    }

    @DexIgnore
    public synchronized void setCurrentMediaTrackInfo(@Nullable MusicTrackInfo currentMediaTrackInfo2) {
        sLogger.v("setCurrentMediaTrackInfo " + currentMediaTrackInfo2);
        if (currentMediaTrackInfo2 == this.currentMediaTrackInfo) {
            sLogger.d("Got same track info object - disregarded");
        } else if (currentMediaTrackInfo2 == null) {
            sLogger.i("Got null track info object - cleaning");
            this.currentMediaTrackInfo = EMPTY_TRACK_INFO;
            setCurrentMediaArtwork(null, null);
            sendRemoteMessage(this.currentMediaTrackInfo);
        } else if (currentMediaTrackInfo2.equals(this.currentMediaTrackInfo)) {
            throttlingLogger.i(1, "Got no new data in the update - disregarded");
        } else {
            this.currentMediaTrackInfo = currentMediaTrackInfo2;
            sendRemoteMessage(this.currentMediaTrackInfo);
        }
    }

    @DexIgnore
    private boolean isTheSameAsCurrentSong(MusicTrackInfo theOtherSong) {
        return StringUtils.equalsOrBothEmptyAfterTrim(MusicDataUtils.songIdentifierFromTrackInfo(theOtherSong), MusicDataUtils.songIdentifierFromTrackInfo(getCurrentMediaTrackInfo()));
    }

    @DexIgnore
    public void checkAndSetCurrentMediaArtwork(MusicTrackInfo trackInfo, Bitmap artwork) {
        if (!isTheSameAsCurrentSong(trackInfo)) {
            sLogger.w("Tried to set artwork from the wrong song - ignored");
            return;
        }
        if (artwork == null) {
            sLogger.i("null set as current media artwork (artwork reset)");
        }
        String artworkHash = IOUtils.hashForBitmap(artwork);
        sLogger.v("Got album artwork with hash " + artworkHash);
        if (MusicUtils.isDefaultArtwork(artworkHash)) {
            sLogger.i("default album art found - ignored");
        } else {
            setCurrentMediaArtwork(artwork, artworkHash);
        }
    }

    @DexIgnore
    private synchronized void setCurrentMediaArtwork(Bitmap artwork, String hash) {
        if (!StringUtils.equalsOrBothEmptyAfterTrim(this.currentArtworkHash, hash)) {
            this.currentMediaArtwork = artwork;
            this.currentArtworkHash = hash;
            sendCurrentMediaArtworkAsUpdate();
        } else {
            sLogger.i("Album art is identical to current, ignoring");
        }
    }

    @DexIgnore
    public synchronized Bitmap getCurrentMediaArtwork() {
        return this.currentMediaArtwork;
    }

    @DexIgnore
    private void sendCurrentMediaArtworkAsUpdate() {
        sLogger.i("sendCurrentMediaArtworkAsUpdate, postPhotoUpdates: " + this.postPhotoUpdates);
        if (this.postPhotoUpdates) {
            sLogger.v("Sending music artwork photo update");
            PhotoUpdate.Builder builder = new PhotoUpdate.Builder();
            builder.photoType(PhotoType.PHOTO_ALBUM_ART);
            builder.identifier(MusicDataUtils.photoIdentifierFromTrackInfo(getCurrentMediaTrackInfo()));
            Bitmap art = getCurrentMediaArtwork();
            sLogger.d("State of currentMediaArtwork: " + art);
            if (art != null) {
                builder = builder.photo(ByteString.of(IOUtils.bitmap2ByteBuffer(art)));
            }
            if (builder.photo == null) {
                sLogger.e("Photo within the photo update is null");
            }
            sendRemoteMessage(builder.build());
        }
    }

    @DexIgnore
    public void setLastMusicApp(String packageName) {
        sLogger.d("setLastMusicApp " + packageName);
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.LAST_MEDIA_APP, packageName).apply();
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    public String getLastMusicApp() {
        String packageName = SettingsUtils.getSharedPreferences().getString(SettingsConstants.LAST_MEDIA_APP, null);
        sLogger.d("getLastMusicApp " + packageName);
        return packageName;
    }

    @DexIgnore
    public boolean isMusicPlayerActive() {
        return !EMPTY_TRACK_INFO.equals(this.currentMediaTrackInfo);
    }

    @DexIgnore
    public void indexPlaylists() {
        // this.musicObserverHandler.post(new MusicServiceHandler.Anon5());
    }

    @DexAdd
    void _indexAllPlaylists(boolean initializing) {
        indexAllPlaylists(initializing);
    }

    // @android.support.annotation.WorkerThread
    @DexIgnore
    private void indexAllPlaylists(boolean initializing) {
        sLogger.d("indexAllPlaylists (" + initializing + ")");
        SystemUtils.ensureNotOnMainThread();
        Cursor playlistsCursor = null;
        try {
            NavdyContentProvider.deleteAllPlaylists();
            int analyticsPlaylistCount = 0;
            int analyticsTrackCount = 0;
            playlistsCursor = MusicDbUtils.getGpmPlaylistsCursor();
            if (playlistsCursor != null && playlistsCursor.moveToFirst()) {
                this.indexingRetries = 0;
                this.shouldPerformFullPlaylistIndex = false;
                do {
                    analyticsTrackCount += indexPlaylistMembers(playlistsCursor.getInt(playlistsCursor.getColumnIndex("_id")), playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name")));
                    analyticsPlaylistCount++;
                } while (playlistsCursor.moveToNext());
                Map<String, String> attributes = new HashMap<>(2);
                attributes.put(TrackerConstants.Attributes.MusicAttributes.NUM_GPM_PLAYLISTS_INDEXED, String.valueOf(analyticsPlaylistCount));
                attributes.put(TrackerConstants.Attributes.MusicAttributes.NUM_GPM_TRACKS_INDEXED, String.valueOf(analyticsTrackCount));
                Tracker.tagEvent(TrackerConstants.Event.MUSIC_PLAYLIST_COMPLETE_REINDEX, attributes);
            } else if (playlistsCursor == null) {
                sLogger.e("Cursor returned is null, we will retry, Retries : " + this.indexingRetries);
                if (!initializing || this.indexingRetries > 5) {
                    sLogger.e("Not retrying as the maximum retries has reached");
                } else {
                    this.musicObserverHandler.removeCallbacks(this.retryIndexingPlayListsRunnable);
                    this.musicObserverHandler.postDelayed(this.retryIndexingPlayListsRunnable, 10000);
                }
            }
        } catch (Throwable t) {
            sLogger.e("Unable to index playlists: " + t);
        } finally {
            IOUtils.closeStream(playlistsCursor);
        }
    }

    // @android.support.annotation.WorkerThread
    @DexIgnore
    private void indexPlaylist(int playlistId) {
        sLogger.d("indexPlaylist: " + playlistId);
        SystemUtils.ensureNotOnMainThread();
        Cursor cursor = null;
        try {
            NavdyContentProvider.deletePlaylist(playlistId);
            cursor = MusicDbUtils.getGpmPlaylistCursor(playlistId);
            if (cursor != null && cursor.moveToFirst()) {
                String playlistName = cursor.getString(cursor.getColumnIndex("playlist_name"));
                if (playlistName != null) {
                    indexPlaylistMembers(playlistId, playlistName);
                }
            }
        } catch (Exception e) {
            sLogger.e("Exception while deleting playlist ", e);
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    // @android.support.annotation.WorkerThread
    @DexIgnore
    private int indexPlaylistMembers(int playlistId, String playlistName) {
        SystemUtils.ensureNotOnMainThread();
        Cursor membersCursor = MusicDbUtils.getGpmPlaylistMembersCursor(playlistId);
        int analyticsTrackCount = 0;
        if (membersCursor != null) {
            try {
                if (membersCursor.moveToFirst()) {
                    int count = 0;
                    do {
                        String sourceId = membersCursor.getString(membersCursor.getColumnIndex("SourceId"));
                        if (sourceId.matches("\\d+")) {
                            if (count == 0) {
                                sLogger.v("Playlist ID: " + playlistId + ", Name: " + playlistName);
                                NavdyContentProvider.addPlaylistToDb(playlistId, playlistName);
                            }
                            String artist = membersCursor.getString(membersCursor.getColumnIndex("artist"));
                            String album = membersCursor.getString(membersCursor.getColumnIndex("album"));
                            String title = membersCursor.getString(membersCursor.getColumnIndex("title"));
                            sLogger.v("- Artist: " + artist + ", Album: " + album + ", Title: " + title + ", ID: " + sourceId);
                            NavdyContentProvider.addPlaylistMemberToDb(playlistId, sourceId, artist, album, title);
                            count++;
                        }
                        analyticsTrackCount++;
                    } while (membersCursor.moveToNext());
                }
            } catch (Throwable th) {
                if (membersCursor != null) {
                    membersCursor.close();
                }
                throw th;
            }
        }
        if (membersCursor != null) {
            membersCursor.close();
        }
        return analyticsTrackCount;
    }

    @DexIgnore
    private void logInternalPlaylistTables() {
        // TaskManager.getInstance().execute(new MusicServiceHandler.Anon6(), 1);
    }

    @DexIgnore
    public boolean shouldPerformFullPlaylistIndex() {
        return this.shouldPerformFullPlaylistIndex;
    }
}
