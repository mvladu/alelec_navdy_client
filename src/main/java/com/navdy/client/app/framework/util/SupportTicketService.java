package com.navdy.client.app.framework.util;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat.Builder;

import com.navdy.client.R.drawable;
import com.navdy.client.R.string;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.PathManager;
import com.navdy.client.app.framework.service.NetworkConnectivityJobService;
import com.navdy.client.app.framework.util.SubmitTicketWorker.OnFinishSubmittingTickets;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.app.ui.settings.ZendeskConstants;
import com.navdy.client.debug.file.FileTransferManager.FileTransferListener;
import com.navdy.client.debug.file.RemoteFileTransferManager;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.zendesk.sdk.network.impl.ZendeskConfig;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class SupportTicketService extends IntentService {
    @DexIgnore
    public static String EXTRA_CREATE_INDETERMINATE_NOTIFICATION; // = "create_indeterminate_notification";
    @DexIgnore
    public static String EXTRA_EMAIL; // = "extra_email";
    @DexIgnore
    public static String EXTRA_RESET_COUNTER; // = "reset_retry_counter";
    @DexIgnore
    public static String EXTRA_TICKET_RANDOM_ID; // = "random_id";
    @DexIgnore
    private static String INTENT_SERVICE_NAME; // = "SUBMIT_TICKET";
    @DexIgnore
    private static long MAX_FILE_SIZE; // = 2097152;
    @DexIgnore
    public static String NEW_FOLDER_FORMAT; // = "yyyy-MM-dd'_'HH:mm:ss.SSS";
    @DexIgnore
    private static long PULL_LOGS_TIMEOUT; // = 15000;
    @DexIgnore
    private static long RETRY_INTERVAL_MILLIS; // = java.util.concurrent.TimeUnit.MINUTES.toMillis(45);
    @DexIgnore
    private static int RETRY_REQUEST_CODE; // = 1;
    @DexIgnore
    private static String SHARED_PREF_TICKET_LAST_RETRY_DATE; // = "ticket_last_retry_date";
    @DexIgnore
    private static String SHARED_PREF_TICKET_RETRY_COUNT; // = "ticket_retry_count";
    @DexIgnore
    private static int SHARED_PREF_TICKET_RETRY_COUNT_DEFAULT; // = 0;
    @DexIgnore
    private static long SHARED_PREF_TICKET_RETRY_DATE_DEFAULT; // = 0;
    @DexIgnore
    private static int SUBMIT_AFTER_INIT_DELAY; // = 10000;
    @DexIgnore
    private static String SUPPORT_TICKET_SHARED_PREFS; // = "support_ticket_shared_prefs";
    @DexIgnore
    private static String TEMP_FILE_TIMESTAMP_FORMAT; // = "yyyy_dd_MM-hh_mm_ss_aa";
    @DexIgnore
    public static String TICKET_ATTACHMENTS_FILENAME; // = "attachments.zip";
    @DexIgnore
    public static String TICKET_FILTER_FOR_READY_FOLDERS; // = "_READY";
    @DexIgnore
    public static String TICKET_FOLDER_README_FILENAME; // = "readme.txt";
    @DexIgnore
    private static int TIMER_REQUEST_CODE; // = 0;
    @DexIgnore
    private static long TRY_MAX_COUNT; // = 4;
    @DexIgnore
    public static Boolean VERBOSE; // = java.lang.Boolean.valueOf(true);
    @DexIgnore
    private static String email;
    @DexIgnore
    public static FilenameFilter filterOnReadyFolders; // = new com.navdy.client.app.framework.util.SupportTicketService.Anon1();
    @DexIgnore
    private static SimpleDateFormat format; // = new java.text.SimpleDateFormat(TEMP_FILE_TIMESTAMP_FORMAT, java.util.Locale.US);
    @DexIgnore
    public static Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.SupportTicketService.class);
    @DexIgnore
    private String lastTicketId; // = "";
    @DexIgnore
    private SubmitTicketWorker submitTicketWorker;

    // @DexIgnore
    // static class Anon1 implements FilenameFilter {
    //     Anon1() {
    //     }
    //
    //     public boolean accept(File file, String string) {
    //         return string.contains(SupportTicketService.TICKET_FILTER_FOR_READY_FOLDERS);
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements OnFinishSubmittingTickets {
    //     final /* synthetic */ File val$ticketsDirectory;
    //
    //     Anon2(File file) {
    //         this.val$ticketsDirectory = file;
    //     }
    //
    //     public void onFinish(int failCount, boolean showSuccessNotification) {
    //         File[] remainingFolders = this.val$ticketsDirectory.listFiles(SupportTicketService.filterOnReadyFolders);
    //         Editor editor = SettingsUtils.getSharedPreferences().edit();
    //         if (remainingFolders == null || remainingFolders.length != 0) {
    //             SupportTicketService.logger.d("pending tickets awaiting hud logs exist");
    //             editor.putBoolean(SettingsConstants.PENDING_TICKETS_EXIST, true);
    //             if (SupportTicketService.isBelowMaxRetryCount()) {
    //                 SupportTicketService.scheduleAlarmForSubmittingPendingTickets(SupportTicketService.RETRY_INTERVAL_MILLIS);
    //             }
    //         } else {
    //             SupportTicketService.logger.d("no pending tickets remaining");
    //             editor.putBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false);
    //             editor.putBoolean(SettingsConstants.TICKETS_AWAITING_HUD_LOG_EXIST, false);
    //         }
    //         editor.apply();
    //         if (failCount == 0) {
    //             SupportTicketService.logger.d("Folders successfully submitted.");
    //             SupportTicketService.resetRetryCount();
    //             SupportTicketService.this.endTicketNotifications();
    //             if (showSuccessNotification) {
    //                 SupportTicketService.this.buildNotificationForSuccess();
    //             }
    //         } else {
    //             SupportTicketService.this.buildNotificationForRetry();
    //             if (SupportTicketService.isBelowMaxRetryCount()) {
    //                 SupportTicketService.scheduleAlarmForSubmittingPendingTickets(SupportTicketService.RETRY_INTERVAL_MILLIS);
    //             }
    //             SupportTicketService.this.incrementTryCount();
    //         }
    //         SupportTicketService.this.incrementUpdateDate();
    //         SupportTicketService.this.finishService(true);
    //     }
    // }
    //
    // @DexIgnore
    // static class Anon3 implements Runnable {
    //     final /* synthetic */ OnLogCollectedInterface val$onLogCollectedInterface;
    //
    //     class Anon1 implements FileTransferListener {
    //         final /* synthetic */ ArrayList val$attachments;
    //         final /* synthetic */ String val$logsFolder;
    //
    //         Anon1(String str, ArrayList arrayList) {
    //             this.val$logsFolder = str;
    //             this.val$attachments = arrayList;
    //         }
    //
    //         public void onFileTransferResponse(FileTransferResponse response) {
    //             if (SupportTicketService.VERBOSE) {
    //                 SupportTicketService.logger.v("onFileTransferResponse: " + response.success);
    //             }
    //             if (!response.success) {
    //                 onError(response.error, "Error collecting display logs");
    //                 return;
    //             }
    //             SupportTicketService.logger.v("HUD file name saved");
    //             this.val$attachments.add(new File(this.val$logsFolder, response.destinationFileName));
    //         }
    //
    //         public void onFileTransferStatus(FileTransferStatus status) {
    //             if (SupportTicketService.VERBOSE) {
    //                 SupportTicketService.logger.v("onFileTransferStatus: " + status.success);
    //             }
    //             if (status.success && status.transferComplete) {
    //                 SupportTicketService.logger.v("Logs collected");
    //                 if (this.val$attachments.size() > 0) {
    //                     Anon3.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
    //                 } else {
    //                     Anon3.this.val$onLogCollectedInterface.onLogCollectionFailed();
    //                 }
    //             }
    //         }
    //
    //         public void onError(FileTransferError errorCode, String error) {
    //             SupportTicketService.logger.v("Error collecting display logs " + error);
    //             if (this.val$attachments.size() > 0) {
    //                 Anon3.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
    //             } else {
    //                 Anon3.this.val$onLogCollectedInterface.onLogCollectionFailed();
    //             }
    //         }
    //     }
    //
    //     Anon3(OnLogCollectedInterface onLogCollectedInterface) {
    //         this.val$onLogCollectedInterface = onLogCollectedInterface;
    //     }
    //
    //     public void run() {
    //         Context applicationContext = NavdyApplication.getAppContext();
    //         String logsFolder = PathManager.getInstance().getLogsFolder();
    //         String timestamp = SupportTicketService.format.format(System.currentTimeMillis());
    //         String deviceDbsFilePath = logsFolder + File.separator + "Database_" + timestamp + ".db";
    //         File file = new File(deviceDbsFilePath);
    //         String deviceSharedPrefsFolderPath = logsFolder + File.separator + timestamp + "-SharedPrefs";
    //         File file2 = new File(deviceSharedPrefsFolderPath);
    //         File tracesFile = null;
    //         try {
    //             File file3 = new File(NavdyApplication.getAppContext().getString(string.trace_file_path));
    //             tracesFile = file3;
    //         } catch (Exception e) {
    //         }
    //         ArrayList<File> attachments = new ArrayList<>();
    //         try {
    //             SupportTicketService.dumpDbs(applicationContext, deviceDbsFilePath);
    //             if (file.exists()) {
    //                 SupportTicketService.logger.v("mobile client Dbs successfully dumped");
    //                 attachments.add(file);
    //             }
    //             if (file2.mkdirs()) {
    //                 SupportTicketService.dumpSharedPrefs(applicationContext, deviceSharedPrefsFolderPath);
    //                 SupportTicketService.logger.v("mobile client SharedPrefs successfully dumped");
    //                 File[] listOfSharedPrefsFiles = file2.listFiles();
    //                 if (listOfSharedPrefsFiles != null) {
    //                     for (File sharedPrefsFile : listOfSharedPrefsFiles) {
    //                         if (sharedPrefsFile != null && sharedPrefsFile.exists() && sharedPrefsFile.isFile()) {
    //                             attachments.add(sharedPrefsFile);
    //                         }
    //                     }
    //                 }
    //             }
    //             Iterator it = NavdyApplication.getLogFiles().iterator();
    //             while (it.hasNext()) {
    //                 File logFile = (File) it.next();
    //                 if (logFile != null && logFile.length() > 0) {
    //                     attachments.add(logFile);
    //                 }
    //             }
    //             if (tracesFile != null) {
    //                 if (tracesFile.length() > 0) {
    //                     attachments.add(tracesFile);
    //                 }
    //             }
    //             new RemoteFileTransferManager(applicationContext, FileType.FILE_TYPE_LOGS, logsFolder, 15000, (FileTransferListener) new Anon3.Anon1(logsFolder, attachments)).pullFile();
    //         } catch (Throwable t) {
    //             SupportTicketService.logger.v("Error collecting device logs: " + t);
    //             if (attachments.size() > 0) {
    //                 this.val$onLogCollectedInterface.onLogCollectionSucceeded(attachments);
    //             } else {
    //                 this.val$onLogCollectedInterface.onLogCollectionFailed();
    //             }
    //         }
    //     }
    // }
    //
    // @DexIgnore
    // static class Anon4 implements Runnable {
    //     final /* synthetic */ OnLogCollectedInterface val$onLogCollectedInterface;
    //
    //     class Anon1 implements FileTransferListener {
    //         final /* synthetic */ ArrayList val$attachments;
    //         final /* synthetic */ String val$logsFolder;
    //
    //         Anon1(String str, ArrayList arrayList) {
    //             this.val$logsFolder = str;
    //             this.val$attachments = arrayList;
    //         }
    //
    //         public void onFileTransferResponse(FileTransferResponse response) {
    //             if (SupportTicketService.VERBOSE) {
    //                 SupportTicketService.logger.v("onFileTransferResponse: " + response.success);
    //             }
    //             if (!response.success) {
    //                 onError(response.error, "Error collecting display logs");
    //                 return;
    //             }
    //             SupportTicketService.logger.v("HUD log saved");
    //             this.val$attachments.add(new File(this.val$logsFolder, response.destinationFileName));
    //         }
    //
    //         public void onFileTransferStatus(FileTransferStatus status) {
    //             if (SupportTicketService.VERBOSE) {
    //                 SupportTicketService.logger.v("onFileTransferStatus: " + status.success);
    //             }
    //             if (status.success && status.transferComplete) {
    //                 SupportTicketService.logger.v("Logs collected");
    //                 if (this.val$attachments.size() > 0) {
    //                     Anon4.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
    //                 } else {
    //                     Anon4.this.val$onLogCollectedInterface.onLogCollectionFailed();
    //                 }
    //             }
    //         }
    //
    //         public void onError(FileTransferError errorCode, String error) {
    //             SupportTicketService.logger.v("Error collecting display logs " + error);
    //             if (this.val$attachments.size() > 0) {
    //                 Anon4.this.val$onLogCollectedInterface.onLogCollectionSucceeded(this.val$attachments);
    //             } else {
    //                 Anon4.this.val$onLogCollectedInterface.onLogCollectionFailed();
    //             }
    //         }
    //     }
    //
    //     Anon4(OnLogCollectedInterface onLogCollectedInterface) {
    //         this.val$onLogCollectedInterface = onLogCollectedInterface;
    //     }
    //
    //     public void run() {
    //         Context applicationContext = NavdyApplication.getAppContext();
    //         String logsFolder = PathManager.getInstance().getLogsFolder();
    //         ArrayList<File> attachments = new ArrayList<>();
    //         try {
    //             new RemoteFileTransferManager(applicationContext, FileType.FILE_TYPE_LOGS, logsFolder, 15000, (FileTransferListener) new Anon4.Anon1(logsFolder, attachments)).pullFile();
    //         } catch (Throwable t) {
    //             SupportTicketService.logger.v("Error collecting hud logs " + t);
    //             if (attachments.size() > 0) {
    //                 this.val$onLogCollectedInterface.onLogCollectionSucceeded(attachments);
    //             } else {
    //                 this.val$onLogCollectedInterface.onLogCollectionFailed();
    //             }
    //         }
    //     }
    // }

    @DexIgnore
    public interface OnLogCollectedInterface {
        void onLogCollectionFailed();

        void onLogCollectionSucceeded(ArrayList<File> arrayList);
    }

    @DexIgnore
    public static class SubmitTicketFinishedEvent {
        private boolean conditionsMet;
        private String ticketId = "";

        SubmitTicketFinishedEvent(boolean conditionsMet2, @NonNull String ticketId2) {
            this.conditionsMet = conditionsMet2;
            this.ticketId = ticketId2;
        }

        public boolean getConditionsMet() {
            return this.conditionsMet;
        }

        @NonNull
        public String getTicketId() {
            return this.ticketId;
        }
    }

    @DexIgnore
    public static void writeJsonObjectToFile(JSONObject jsonObject, String directory, Logger logger2) {
        // FileWriter fileWriter = null;
        // try {
        //     FileWriter fileWriter2 = new FileWriter(directory + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
        //     try {
        //         fileWriter2.write(jsonObject.toString());
        //         fileWriter2.flush();
        //         IOUtils.closeStream(fileWriter2);
        //         FileWriter fileWriter3 = fileWriter2;
        //     } catch (Exception e) {
        //         e = e;
        //         fileWriter = fileWriter2;
        //         try {
        //             logger2.e("Failed to write JSON to file: " + e.toString());
        //             IOUtils.closeStream(fileWriter);
        //         } catch (Throwable th) {
        //             th = th;
        //             IOUtils.closeStream(fileWriter);
        //             throw th;
        //         }
        //     } catch (Throwable th2) {
        //         th = th2;
        //         fileWriter = fileWriter2;
        //         IOUtils.closeStream(fileWriter);
        //         throw th;
        //     }
        // } catch (Exception e2) {
        //     e = e2;
        //     logger2.e("Failed to write JSON to file: " + e.toString());
        //     IOUtils.closeStream(fileWriter);
        // }
    }

    @DexIgnore
    public SupportTicketService() {
        super(INTENT_SERVICE_NAME);
        BusProvider.getInstance().register(this);
        this.submitTicketWorker = new SubmitTicketWorker();
        if (StringUtils.isEmptyAfterTrim(email)) {
            setEmail(SettingsUtils.getCustomerPreferences().getString("email", ""));
        }
    }

    @DexIgnore
    private synchronized void authenticateZendesk() {
        if (VERBOSE) {
            logger.v("authenticateZendesk");
        }
        String zendeskAppId = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(string.metadata_zendesk_appid));
        String zendeskOAuth = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(string.metadata_zendesk_oauth));
        Context context = NavdyApplication.getAppContext();
        ZendeskConfig.INSTANCE.init(context, context.getString(string.zendesk_url), zendeskAppId, zendeskOAuth);
        if (ZendeskConfig.INSTANCE.isInitialized()) {
            logger.d("Zendesk isInitialized == true");
            ZendeskConfig.INSTANCE.setIdentity(new com.zendesk.sdk.model.access.AnonymousIdentity.Builder().withNameIdentifier(SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.FULL_NAME, "")).withEmailIdentifier(email).build());
            scheduleAlarmForSubmittingPendingTickets(10000);
        } else {
            logger.d("Zendesk initialization failed");
            finishService(false);
        }
    }

    @DexReplace
    protected synchronized void onHandleIntent(Intent intent) {
        logger.d("SupportTicketService handling intent");
        if (!BaseActivity.weHaveStoragePermission()) {
            logger.e("We do not have storage permission");
            finishService(false);
        }
        if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext())) {
            if (VERBOSE) {
                logger.d("Network is not accessible. Scheduling another alarm for the future...");
            }
            if (VERSION.SDK_INT >= 24 && !SettingsUtils.isLimitingCellularData()) {
                NetworkConnectivityJobService.scheduleNetworkServiceForNougatOrAbove();
            }
            buildNotificationForRetry();
            finishService(false);
        }
        if (!SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false)) {
            logger.d("pending tickets currently do not exist");
            finishService(false);
        }
        if (intent.getBooleanExtra(EXTRA_RESET_COUNTER, false)) {
            if (VERBOSE) {
                logger.d("reset boolean extra found");
            }
            resetRetryCount();
        }
        if (intent.getBooleanExtra(EXTRA_CREATE_INDETERMINATE_NOTIFICATION, false)) {
            buildNotificationForIndeterminateProgress();
        }
        if (intent.hasExtra(EXTRA_TICKET_RANDOM_ID)) {
            this.lastTicketId = intent.getStringExtra(EXTRA_TICKET_RANDOM_ID);
        }
        String extraEmail = intent.getStringExtra(EXTRA_EMAIL);
        if (!StringUtils.isEmptyAfterTrim(extraEmail)) {
            if (VERBOSE) {
                logger.d("extra intent email found: " + extraEmail);
            }
            setEmail(extraEmail);
        }
        logger.v("onHandleIntent: " + intent);
        if (this.submitTicketWorker.isRunning()) {
            logger.d("SupportTicketService is already running. Avoiding a duplicate call to intent service.");
        // } else if (ZendeskConfig.INSTANCE.isInitialized()) {
        } else {
            submitPendingTickets();
        // } else {
        //     authenticateZendesk();
        }
    }

    @DexIgnore
    private synchronized void submitPendingTickets() {
        // try {
        //     logger.d("submitPendingTickets");
        //     File ticketsDirectory = new File(PathManager.getInstance().getTicketFolderPath());
        //     File[] readyFolders = ticketsDirectory.listFiles(filterOnReadyFolders);
        //     if (readyFolders == null || readyFolders.length <= 0) {
        //         logger.v("There are no pending tickets found");
        //         endTicketNotifications();
        //         finishService(false);
        //     }
        //     this.submitTicketWorker.start(new Anon2(ticketsDirectory), readyFolders);
        // } catch (Exception e) {
        //     logger.e("There was a problem submitting pending tickets: " + e);
        // }
        // return;
    }

    @DexIgnore
    public static SharedPreferences getSupportTicketPreferences() {
        return NavdyApplication.getAppContext().getSharedPreferences(SUPPORT_TICKET_SHARED_PREFS, 0);
    }

    @DexIgnore
    public static void scheduleAlarmForSubmittingPendingTickets(long interval) {
        // if (SettingsUtils.isLimitingCellularData()) {
        //     logger.v("app is limiting cellular data, won't schedule alarm for submitting pending tickets");
        //     return;
        // }
        // if (VERBOSE) {
        //     logger.v("scheduleAlarmForSubmittingPendingTickets retry coming in " + TimeUnit.MILLISECONDS.toMinutes(interval) + " minutes");
        // }
        // Context context = NavdyApplication.getAppContext();
        // ((AlarmManager) context.getSystemService(ALARM_SERVICE)).set(1, System.currentTimeMillis() + interval, PendingIntent.getService(NavdyApplication.getAppContext(), 0, new Intent(context, SupportTicketService.class), 268435456));
    }

    @DexIgnore
    public void buildNotificationForRetry() {
        // if (VERBOSE) {
        //     logger.v("buildNotificationForRetry");
        // }
        // int failureCount = 0;
        // Context context = NavdyApplication.getAppContext();
        // Intent intent = new Intent(context, SupportTicketService.class);
        // intent.putExtra(EXTRA_RESET_COUNTER, true);
        // intent.putExtra(EXTRA_CREATE_INDETERMINATE_NOTIFICATION, true);
        // PendingIntent pendingIntent = PendingIntent.getService(NavdyApplication.getAppContext(), 1, intent, 134217728);
        // File[] folders = new File(PathManager.getInstance().getTicketFolderPath()).listFiles(filterOnReadyFolders);
        // if (folders != null) {
        //     for (File ticketFolder : folders) {
        //         File content = new File(ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
        //         if (VERBOSE) {
        //             logger.v("content filepath: " + ticketFolder.getAbsolutePath() + File.separator + ZendeskConstants.TICKET_JSON_FILENAME);
        //         }
        //         JSONObject jsonObject = SubmitTicketWorker.getJSONObjectFromFile(content);
        //         if (jsonObject != null && !jsonObject.optString(ZendeskConstants.TICKET_ACTION, "").equals(ZendeskConstants.ACTION_FETCH_HUD_LOG_UPLOAD_DELETE)) {
        //             failureCount++;
        //         }
        //     }
        // }
        // if (failureCount > 0) {
        //     if (VERBOSE) {
        //         logger.v("failureCount: " + failureCount);
        //     }
        //     Builder notificationBuilder = new Builder(context).setSmallIcon(drawable.ic_navdy_logo).setContentTitle(context.getString(string.zendesk_unable_to_submit_x_tickets, new Object[]{failureCount})).setContentText(context.getString(string.zendesk_notification_failure_message)).setContentIntent(pendingIntent);
        //     NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        //     if (notificationManager != null) {
        //         notificationManager.notify(2, notificationBuilder.build());
        //     }
        // } else if (VERBOSE) {
        //     logger.v("no failures found. Not building notification for retry");
        // }
    }

    @DexIgnore
    public void buildNotificationForSuccess() {
        // if (VERBOSE) {
        //     logger.v("buildNotificationForSuccess");
        // }
        // Context context = NavdyApplication.getAppContext();
        // Builder notificationBuilder = new Builder(context).setSmallIcon(drawable.ic_navdy_logo).setContentTitle(context.getString(string.ticket_submission_success_title));
        // NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        // if (notificationManager != null) {
        //     notificationManager.notify(4, notificationBuilder.build());
        // }
    }

    @DexIgnore
    public void buildNotificationForIndeterminateProgress() {
        // if (VERBOSE) {
        //     logger.v("buildNotificationForIndeterminateProgress");
        // }
        // Context context = NavdyApplication.getAppContext();
        // Builder notificationBuilder = new Builder(context).setSmallIcon(drawable.ic_navdy_logo).setContentTitle(context.getString(string.zendesk_upload_support_request)).setProgress(100, 0, true);
        // NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        // if (notificationManager != null) {
        //     notificationManager.notify(3, notificationBuilder.build());
        // }
    }

    @DexIgnore
    public void endTicketNotifications() {
        // if (VERBOSE) {
        //     logger.v("endTicketNotifications");
        // }
        // NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        // if (notificationManager != null) {
        //     notificationManager.cancel(3);
        //     notificationManager.cancel(2);
        // }
    }

    @DexIgnore
    public static void resetRetryCount() {
        if (VERBOSE) {
            logger.v("resetRetryCount");
        }
        getSupportTicketPreferences().edit().putInt(SHARED_PREF_TICKET_RETRY_COUNT, 0).putLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, 0).apply();
    }

    @DexIgnore
    public static boolean isBelowMaxRetryCount() {
        if (VERBOSE) {
            logger.v("isBelowMaxRetryCount");
        }
        int retryCount = getSupportTicketPreferences().getInt(SHARED_PREF_TICKET_RETRY_COUNT, 0);
        logger.v("retryCount: " + retryCount);
        if (((long) retryCount) <= 4) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void incrementTryCount() {
        SharedPreferences sharedPreferences = getSupportTicketPreferences();
        int retryCount = sharedPreferences.getInt(SHARED_PREF_TICKET_RETRY_COUNT, 0);
        logger.d("incrementTryCount retryCount: " + retryCount);
        sharedPreferences.edit().putInt(SHARED_PREF_TICKET_RETRY_COUNT, retryCount + 1).apply();
    }

    @DexIgnore
    public void incrementUpdateDate() {
        getSupportTicketPreferences().edit().putLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, new Date().getTime()).apply();
    }

    @DexIgnore
    public static boolean lastRetryDateIsOldEnough() {
        boolean lastRetryDateIsOldEnough = System.currentTimeMillis() - RETRY_INTERVAL_MILLIS > getSupportTicketPreferences().getLong(SHARED_PREF_TICKET_LAST_RETRY_DATE, 0);
        if (VERBOSE) {
            logger.d("lastRetryDateIsOldEnough: " + lastRetryDateIsOldEnough);
        }
        return lastRetryDateIsOldEnough;
    }

    @DexIgnore
    public static void startSupportTicketService(Context context) {
        if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false)) {
            context.startService(new Intent(context, SupportTicketService.class));
        }
    }

    @DexIgnore
    public static synchronized String getEmail() {
        String str;
        synchronized (SupportTicketService.class) {
            str = email;
        }
        return str;
    }

    @DexIgnore
    static synchronized void setEmail(String email2) {
        synchronized (SupportTicketService.class) {
            email = email2;
        }
    }

    @DexIgnore
    public static boolean createFolderForTicket(ArrayList<File> attachments, JSONObject jsonObjectForTicketContents) {
        logger.i("jsonObject: " + jsonObjectForTicketContents);
        if (jsonObjectForTicketContents == null) {
            logger.e("jsonObject unsuccessfully created");
            return false;
        }
        String folderName = new SimpleDateFormat(NEW_FOLDER_FORMAT, Locale.US).format(new Date(System.currentTimeMillis()));
        if (VERBOSE) {
            logger.v("new foldername: " + folderName);
        }
        File unfininishedDirectory = new File(PathManager.getInstance().getTicketFolderPath() + File.separator + folderName);
        boolean folderSuccessfullyCreated = unfininishedDirectory.mkdirs();
        if (folderSuccessfullyCreated) {
            File[] basicAttachmentArray = (File[]) attachments.toArray(new File[attachments.size()]);
            if (!attachments.isEmpty()) {
                IOUtils.compressFilesToZip(NavdyApplication.getAppContext(), basicAttachmentArray, new File(unfininishedDirectory.getAbsolutePath() + File.separator + "attachments.zip").getAbsolutePath());
            }
            writeJsonObjectToFile(jsonObjectForTicketContents, unfininishedDirectory.getAbsolutePath(), logger);
            if (!unfininishedDirectory.renameTo(new File(unfininishedDirectory.getAbsolutePath() + TICKET_FILTER_FOR_READY_FOLDERS))) {
                return folderSuccessfullyCreated;
            }
            logger.v("rename worked: " + unfininishedDirectory.getAbsolutePath());
            return folderSuccessfullyCreated;
        }
        logger.e("folder failed");
        return folderSuccessfullyCreated;
    }

    @DexIgnore
    public static void collectLogs(OnLogCollectedInterface onLogCollectedInterface) {
        // if (VERBOSE) {
        //     logger.v("collectLogsAndThen");
        // }
        // TaskManager.getInstance().execute(new Anon3(onLogCollectedInterface), 1);
    }

    @DexIgnore
    public static void collectHudLog(OnLogCollectedInterface onLogCollectedInterface) {
        // TaskManager.getInstance().execute(new Anon4(onLogCollectedInterface), 1);
    }

    @DexIgnore
    private static synchronized void dumpDbs(Context context, String fileName) throws Throwable {
        synchronized (SupportTicketService.class) {
            File backupDB = new File(fileName);
            if (backupDB.exists()) {
                IOUtils.deleteFile(context, fileName);
            }
            FileChannel src = null;
            FileChannel dst = null;
            try {
                File currentDB = context.getDatabasePath(NavdyContentProviderConstants.DB_NAME);
                if (currentDB.length() > MAX_FILE_SIZE) {
                    logger.e("DB too big: " + currentDB.length() + " bytes");
                } else if (currentDB.exists()) {
                    src = new FileInputStream(currentDB).getChannel();
                    dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                } else {
                    logger.e("invalid DB path: " + currentDB);
                }
            } catch (Exception e) {
                logger.e("Unable to pull the DB from the phone: ", e);
            } finally {
                IOUtils.closeStream(src);
                IOUtils.closeStream(dst);
            }
        }
        return;
    }

    @DexIgnore
    private static synchronized void dumpSharedPrefs(Context context, String destinationFolderPath) throws Throwable {
        // FileChannel src;
        // FileChannel dst;
        // synchronized (SupportTicketService.class) {
        //     File destinationFolder = new File(destinationFolderPath);
        //     if (destinationFolder.exists() || destinationFolder.mkdirs()) {
        //         try {
        //             File[] listOfSharedPrefsFiles = new File(context.getFilesDir().getParent() + "/shared_prefs/").listFiles();
        //             if (listOfSharedPrefsFiles != null) {
        //                 for (File sharedPrefsFile : listOfSharedPrefsFiles) {
        //                     if (sharedPrefsFile != null && sharedPrefsFile.exists() && sharedPrefsFile.isFile()) {
        //                         File backupFile = new File(destinationFolder, "SharedPref_" + sharedPrefsFile.getName());
        //                         src = null;
        //                         dst = null;
        //                         src = new FileInputStream(sharedPrefsFile).getChannel();
        //                         dst = new FileOutputStream(backupFile).getChannel();
        //                         dst.transferFrom(src, 0, src.size());
        //                         IOUtils.closeStream(src);
        //                         IOUtils.closeStream(dst);
        //                     }
        //                 }
        //             }
        //         } catch (Exception e) {
        //             logger.e("Unable to pull the shared prefs files from the phone: ", e);
        //         } catch (Throwable th) {
        //             IOUtils.closeStream(src);
        //             IOUtils.closeStream(dst);
        //             throw th;
        //         }
        //     } else {
        //         logger.e("Unable to create the destination folder: " + destinationFolderPath);
        //     }
        // }
        // return;
    }

    @DexIgnore
    public static void submitTicketsIfConditionsAreMet() {
        boolean isReachable = com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext());
        boolean pendingTicketsExist = SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false);
        boolean lastRetryDateIsOldEnough = lastRetryDateIsOldEnough();
        boolean isBelowMaxRetryCount = isBelowMaxRetryCount();
        if (VERBOSE) {
            logger.d("isReachable: " + isReachable + "\n pendingTicketExists: " + pendingTicketsExist + "\n lastRetryDateIsOldEnough: " + lastRetryDateIsOldEnough + "\n isBelowMaxRetryCount: " + isBelowMaxRetryCount);
        }
        if (isReachable && lastRetryDateIsOldEnough && isBelowMaxRetryCount && pendingTicketsExist) {
            if (VERBOSE) {
                logger.d("Should submit tickets");
            }
            try {
                startSupportTicketService(NavdyApplication.getAppContext());
            } catch (Exception e) {
                logger.e("Exception found: " + e);
            }
        }
    }

    @DexIgnore
    public void finishService(boolean isSuccess) {
        BusProvider.getInstance().post(new SubmitTicketFinishedEvent(isSuccess, this.lastTicketId));
        stopSelf();
    }
}
