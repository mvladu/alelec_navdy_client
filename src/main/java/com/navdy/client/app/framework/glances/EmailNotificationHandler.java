package com.navdy.client.app.framework.glances;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;

import com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.log.Logger;

import java.util.ArrayList;
import java.util.List;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class EmailNotificationHandler {
    @DexIgnore
    private static Logger sLogger; // = com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService.sLogger;

    @DexReplace
    public static void handleEmailNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (!GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName) || !GlanceConstants.isPackageInGroup(packageName, GlanceConstants.Group.EMAIL_GROUP)) {
            sLogger.w("email notification not handled [" + packageName + "]");
            return;
        }
        Notification notification = sbn.getNotification();
        Bundle extras = NotificationCompat.getExtras(notification);
        if (extras == null) {
            sLogger.w("email notification not handled [" + packageName + "]");
            return;
        }
        String senderOrTitle = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.title");
        String toEmail = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.subText");
        String subject = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
        String body = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.bigText");
        Object textLines = extras.get("android.textLines");
        if (!packageName.equals(GlanceConstants.GOOGLE_MAIL) && (textLines instanceof CharSequence[])) {
            CharSequence[] lines = (CharSequence[]) textLines;
            StringBuilder newBody = new StringBuilder();
            for (int i = 0; i < lines.length; i++) {
                newBody.append(lines[i]);
                if (i + 1 < lines.length) {
                    newBody.append("\n");
                }
            }
            if (newBody.length() > 0) {
                subject = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.summaryText");
                body = newBody.toString();
            }
        } else if (!(body == null || subject == null)) {
            if (packageName.equals(GlanceConstants.MICROSOFT_OUTLOOK)) {
                body = body.substring(body.indexOf("\n") + 1);
                if (subject.contains(body)) {
                    subject = subject.substring(0, subject.indexOf(body));
                }
            } else if (body.startsWith(subject)) {
                int index = body.indexOf(subject + "\n");
                if (index != -1) {
                    body = body.substring(subject.length() + index + 1);
                }
            }
        }
        if (body == null && subject != null) {
            int index2 = subject.indexOf("\n");
            if (index2 != -1) {
                body = subject.substring(index2 + 1).trim();
                subject = subject.substring(0, index2).trim();
            }
        }
        StringBuilder builder = new StringBuilder();
        String peopleStr = null;
        String[] people = (String[]) extras.get("android.people");
        if (people != null) {
            for (int i2 = 0; i2 < people.length; i2++) {
                String p = people[i2];
                if (p != null && p.startsWith(GlanceConstants.MAIL_TO)) {
                    p = p.substring(GlanceConstants.MAIL_TO.length());
                }
                if (i2 != 0) {
                    builder.append(",");
                }
                builder.append(p);
            }
            peopleStr = builder.toString();
        }
        if (peopleStr == null && senderOrTitle != null) {
            peopleStr = senderOrTitle;
        }
        sLogger.v("[navdyinfo-gmail] senderOrTitle[" + senderOrTitle + "] from[" + peopleStr + "] to[" + toEmail + "] subject[" + subject + "] body[" + body + "]");
        if ((subject == null && body == null) || peopleStr == null) {
            sLogger.v("[navdyinfo-gmail] invalid data");
            return;
        }
        String id = GlancesHelper.getId();
        List<KeyValue> data = new ArrayList<>();
        if (!StringUtils.isEmptyAfterTrim(peopleStr)) {
            if (peopleStr.contains(GlanceConstants.EMAIL_AT)) {
                KeyValue keyValue = new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), senderOrTitle);
                data.add(keyValue);
                KeyValue keyValue2 = new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), peopleStr);
                data.add(keyValue2);
            } else {
                KeyValue keyValue3 = new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), peopleStr);
                data.add(keyValue3);
            }
        }
        if (toEmail != null) {
            if (toEmail.contains(GlanceConstants.EMAIL_AT)) {
                KeyValue keyValue4 = new KeyValue(EmailConstants.EMAIL_TO_EMAIL.name(), toEmail);
                data.add(keyValue4);
            } else {
                KeyValue keyValue5 = new KeyValue(EmailConstants.EMAIL_TO_NAME.name(), toEmail);
                data.add(keyValue5);
            }
        }
        if (!StringUtils.isEmptyAfterTrim(subject)) {
            KeyValue keyValue6 = new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), subject);
            data.add(keyValue6);
        }
        if (!StringUtils.isEmptyAfterTrim(body)) {
            KeyValue keyValue7 = new KeyValue(EmailConstants.EMAIL_BODY.name(), body);
            data.add(keyValue7);
        }
        GlancesHelper.sendEvent(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_EMAIL).provider(packageName).id(id).postTime(notification.when).glanceData(data).build());
    }
}
