package com.navdy.client.app.framework.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;

import io.fabric.sdk.android.Fabric;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public final class CrashReporter {
    @DexIgnore
    private static /* final */ CrashReporter sInstance; // = new com.navdy.client.app.framework.util.CrashReporter();
    @DexIgnore
    private static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.framework.util.CrashReporter.class);
    @DexIgnore
    private static volatile boolean stopCrashReporting;
    @DexIgnore
    private boolean installed;

    @DexIgnore
    public static CrashReporter getInstance() {
        return sInstance;
    }

    @DexIgnore
    private CrashReporter() {
    }

    @SuppressLint("ApplySharedPref")
    @DexReplace
    public synchronized void installCrashHandler(Context context, String userId) {
        if (!this.installed) {
            Thread.UncaughtExceptionHandler defaultUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtHandler != null) {
                sLogger.v("default uncaught handler:" + defaultUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default uncaught handler is null");
            }
            Fabric.with(new Fabric.Builder(context).kits(new Crashlytics(), new CrashlyticsNdk()).build());
            if (userId != null) {
                Crashlytics.setUserIdentifier(userId);
            }
            Crashlytics.setString("Serial", Build.SERIAL);
            sLogger.i("crashlytics installed with user:" + userId);
            Thread.UncaughtExceptionHandler crashlyticsHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (crashlyticsHandler != null) {
                sLogger.v("crashlytics uncaught handler:" + crashlyticsHandler.getClass().getName());
            } else {
                sLogger.v("crashlytics uncaught handler is null");
            }
            Thread.setDefaultUncaughtExceptionHandler((thread, exception) -> {
                if (exception.toString().contains("RemoteServiceException")) {
                    // Disable connection notification for next time
                    sLogger.v("Disabling Connection Notification");
                    SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.CONNECTION_NOTIFICATION_ENABLED, false).commit();
                }
                CrashReporter.this.handleUncaughtException(thread, exception, crashlyticsHandler);
            });
            Logger.addAppender(new CrashlyticsAppender());
            this.installed = true;
        }
    }

    @DexIgnore
    public synchronized void setUser(String userId) {
        if (userId != null) {
            Crashlytics.setUserIdentifier(userId);
        }
    }

    @DexIgnore
    public void reportNonFatalException(Throwable throwable) {
        if (this.installed) {
            try {
                Crashlytics.logException(throwable);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    @DexIgnore
    private void log(String msg) {
        if (this.installed) {
            try {
                Crashlytics.log(msg);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    @DexIgnore
    private void handleUncaughtException(Thread thread, Throwable exception, Thread.UncaughtExceptionHandler crashlyticsHandler) {
        if (stopCrashReporting) {
            sLogger.i("FATAL-reporting-turned-off", exception);
            return;
        }
        stopCrashReporting = true;
        String tag = "FATAL-CRASH";
        String msg = tag + " Uncaught exception - " + thread.getName() + ":" + thread.getId() + " ui thread id:" + Looper.getMainLooper().getThread().getId();
        Log.e(tag, msg, exception);
        sLogger.e(msg, exception);
        sLogger.i("closing logger");
        Logger.close();
        if (crashlyticsHandler != null) {
            crashlyticsHandler.uncaughtException(thread, exception);
        }
    }
}
