package com.navdy.client.app.framework.service;

import android.app.Notification;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.text.TextUtils;

import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.glances.CalendarNotificationHandler;
import com.navdy.client.app.framework.glances.EmailNotificationHandler;
import com.navdy.client.app.framework.glances.GenericNotificationHandler;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.glances.MessagingNotificationHandler;
import com.navdy.client.app.framework.glances.SocialNotificationHandler;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public abstract class NavdyCustomNotificationListenerService extends NotificationListenerService {
    @DexIgnore
    private static /* final */ int INDENT_INCREMENT; // = 4;
    @DexIgnore
    private static /* final */ long NOTIFICATION_WAIT_FOR_MORE_DELAY; // = 1000;
    @DexIgnore
    private static /* final */ String SPACES; // = " ";
    @DexIgnore
    public static /* final */ Handler notifListenerServiceHandler; // = new android.os.Handler();
    @DexIgnore
    public static /* final */ Logger sLogger; // = new com.navdy.service.library.log.Logger("NavdyNotifHandler");
    @DexIgnore
    public static /* final */ Pattern splitTickerTextOnColon; // = java.util.regex.Pattern.compile("(.*): (.*)");
    @DexIgnore
    private List<String> activeMusicApps; // = new java.util.ArrayList();
    @DexIgnore
    private HashMap<String, Runnable> incomingNotifications; // = new java.util.HashMap<>();
    @DexIgnore
    private HashMap<String, Boolean> knownGroups; // = new java.util.HashMap<>();
    @DexIgnore
    private HashMap<String, Boolean> knownNotifications; // = new java.util.HashMap<>();
    @DexIgnore
    private String lastOngoing; // = "";
    @DexIgnore
    private HashMap<String, Runnable> pendingGroupNotifications; // = new java.util.HashMap<>();
    @DexIgnore
    private /* final */ SharedPreferences sharedPrefs; // = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();

    // @DexIgnore
    // class Anon1 implements Runnable {
    //     final /* synthetic */ StatusBarNotification val$sbn;
    //
    //     @DexIgnore
    //     Anon1(StatusBarNotification statusBarNotification) {
    //         this.val$sbn = statusBarNotification;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         NavdyCustomNotificationListenerService.this.handleStatusBarNotification(this.val$sbn);
    //     }
    // }

    // @DexIgnore
    // class Anon2 implements Runnable {
    //     final /* synthetic */ String val$finalTicker;
    //     final /* synthetic */ Notification val$notif;
    //     final /* synthetic */ String val$packageName;
    //     final /* synthetic */ StatusBarNotification val$sbn;
    //
    //     class Anon1 implements Runnable {
    //
    //         /* renamed from: com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService$Anon2$Anon1$Anon1 reason: collision with other inner class name */
    //         class C0048Anon1 implements Runnable {
    //
    //             /* renamed from: com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService$Anon2$Anon1$Anon1$Anon1 reason: collision with other inner class name */
    //             class C0049Anon1 implements Runnable {
    //                 C0049Anon1() {
    //                 }
    //
    //                 public void run() {
    //                     NavdyCustomNotificationListenerService.this.categorizeAndHandleNotification(NavdyCustomNotificationListenerService.Anon2.this.val$sbn, NavdyCustomNotificationListenerService.Anon2.this.val$packageName, NavdyCustomNotificationListenerService.Anon2.this.val$finalTicker);
    //                 }
    //             }
    //
    //             C0048Anon1() {
    //             }
    //
    //             public void run() {
    //                 TaskManager.getInstance().execute(new NavdyCustomNotificationListenerService.Anon2.Anon1.C0048Anon1.C0049Anon1(), 7);
    //             }
    //         }
    //
    //         Anon1() {
    //         }
    //
    //         public void run() {
    //             String group = NotificationCompat.getGroup(NavdyCustomNotificationListenerService.Anon2.this.val$notif);
    //             Boolean isKnown = !StringUtils.isEmptyAfterTrim(group) && Boolean.TRUE.equals(NavdyCustomNotificationListenerService.this.knownGroups.get(group));
    //             boolean isGroupSummary = NotificationCompat.isGroupSummary(NavdyCustomNotificationListenerService.Anon2.this.val$notif);
    //             NavdyCustomNotificationListenerService.sLogger.d("NotifListenerService: handleStatusBarNotification: pkg [" + NavdyCustomNotificationListenerService.Anon2.this.val$packageName + "]" + " id [" + NavdyCustomNotificationListenerService.Anon2.this.val$sbn.getId() + "]" + " tag [" + NavdyCustomNotificationListenerService.Anon2.this.val$sbn.getTag() + "]" + " ticker[" + NavdyCustomNotificationListenerService.Anon2.this.val$finalTicker + "]" + " group[" + group + "]" + " isKnownGroup[" + isKnown + "]" + " isGroupSummary[" + isGroupSummary + "]");
    //             if (!StringUtils.isEmptyAfterTrim(group)) {
    //                 NavdyCustomNotificationListenerService.this.knownGroups.put(group, Boolean.TRUE);
    //             }
    //             if (!isGroupSummary && !StringUtils.equalsOrBothEmptyAfterTrim(NavdyCustomNotificationListenerService.Anon2.this.val$packageName, GlanceConstants.WHATS_APP)) {
    //                 NavdyCustomNotificationListenerService.sLogger.i("Remove Pending Handling For Group: " + group);
    //                 NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
    //                 NavdyCustomNotificationListenerService.this.categorizeAndHandleNotification(NavdyCustomNotificationListenerService.Anon2.this.val$sbn, NavdyCustomNotificationListenerService.Anon2.this.val$packageName, NavdyCustomNotificationListenerService.Anon2.this.val$finalTicker);
    //             } else if (!Boolean.TRUE.equals(isKnown) || StringUtils.equalsOrBothEmptyAfterTrim(NavdyCustomNotificationListenerService.Anon2.this.val$packageName, GlanceConstants.WHATS_APP)) {
    //                 NavdyCustomNotificationListenerService.sLogger.i("Remove Pending Handling For Group: " + group);
    //                 NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
    //                 Runnable r = new NavdyCustomNotificationListenerService.Anon2.Anon1.C0048Anon1();
    //                 NavdyCustomNotificationListenerService.sLogger.i("Adding Pending Handling For Group: " + group);
    //                 NavdyCustomNotificationListenerService.this.pendingGroupNotifications.put(group, r);
    //                 NavdyCustomNotificationListenerService.notifListenerServiceHandler.postDelayed(r, 1000);
    //             } else {
    //                 NavdyCustomNotificationListenerService.sLogger.i("Ignoring summary notifications to prevent duplicates.");
    //             }
    //         }
    //     }
    //
    //     @DexIgnore
    //     Anon2(Notification notification, String str, StatusBarNotification statusBarNotification, String str2) {
    //         this.val$notif = notification;
    //         this.val$packageName = str;
    //         this.val$sbn = statusBarNotification;
    //         this.val$finalTicker = str2;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         TaskManager.getInstance().execute(new NavdyCustomNotificationListenerService.Anon2.Anon1(), 7);
    //     }
    // }

    // @DexIgnore
    // class Anon3 implements Runnable {
    //     final /* synthetic */ StatusBarNotification val$sbn;
    //
    //     @DexIgnore
    //     Anon3(StatusBarNotification statusBarNotification) {
    //         this.val$sbn = statusBarNotification;
    //     }
    //
    //     @DexIgnore
    //     public void run() {
    //         if (this.val$sbn != null) {
    //             NavdyCustomNotificationListenerService.sLogger.d("onNotificationRemoved: pkg [" + this.val$sbn.getPackageName() + "]" + " tag [" + this.val$sbn.getTag() + "]" + " id [" + this.val$sbn.getId() + "]" + " time [" + this.val$sbn.getPostTime() + "]");
    //             if (NavdyCustomNotificationListenerService.this.isMusicNotification(this.val$sbn)) {
    //                 NavdyCustomNotificationListenerService.this.removeActiveMusicApp(this.val$sbn.getPackageName());
    //             }
    //             NavdyCustomNotificationListenerService.this.cancelPendingHandlerIfAnyFor(this.val$sbn);
    //             if (NotificationCompat.isGroupSummary(this.val$sbn.getNotification())) {
    //                 String group = NotificationCompat.getGroup(this.val$sbn.getNotification());
    //                 NavdyCustomNotificationListenerService.this.knownGroups.remove(group);
    //                 NavdyCustomNotificationListenerService.this.removePendingHandlingForGroup(group);
    //             }
    //             NavdyCustomNotificationListenerService.this.knownNotifications.remove(NavdyCustomNotificationListenerService.this.getHash(this.val$sbn));
    //         }
    //     }
    // }

    @DexIgnore
    public NavdyCustomNotificationListenerService() {
        GlanceConstants.addPackageToGroup(SettingsUtils.getDialerPackage(NavdyApplication.getAppContext().getPackageManager()), GlanceConstants.Group.IGNORE_GROUP);
    }

    @DexIgnore
    public void onCreate() {
        sLogger.d("onCreate");
        super.onCreate();
    }

    @DexIgnore
    public void onDestroy() {
        sLogger.d("onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public void onNotificationPosted(StatusBarNotification sbn) {
        // if (sbn != null) {
        //     TaskManager.getInstance().execute(new NavdyCustomNotificationListenerService.Anon1(sbn), 7);
        // }
    }

    /* access modifiers changed from: private */
    // @android.support.annotation.NonNull
    @DexIgnore
    public String getHash(@NonNull StatusBarNotification sbn) {
        return sbn.getId() + Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + sbn.getPackageName() + Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + sbn.getTag() + Destination.IDENTIFIER_CONTACT_ID_SEPARATOR + getText(sbn);
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    private String getText(@Nullable StatusBarNotification sbn) {
        if (sbn == null) {
            return null;
        }
        Notification notif = sbn.getNotification();
        if (notif == null) {
            return null;
        }
        Bundle extras = NotificationCompat.getExtras(notif);
        if (extras == null) {
            return null;
        }
        String text = getLastMessageInCarConversation(extras);
        if (!StringUtils.isEmptyAfterTrim(text)) {
            return text;
        }
        String text2 = getStringSafely(extras, "android.text");
        if (!StringUtils.isEmptyAfterTrim(text2)) {
            return text2;
        }
        String text3 = getStringSafely(extras, "android.bigText");
        if (!StringUtils.isEmptyAfterTrim(text3)) {
            return text3;
        }
        String text4 = getStringSafely(extras, "android.title");
        if (!StringUtils.isEmptyAfterTrim(text4)) {
            return text4;
        }
        if (notif.tickerText != null) {
            return notif.tickerText.toString();
        }
        return null;
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    public static String getLastMessageInCarConversation(@NonNull Bundle extras) {
        Bundle carExt = extras.getBundle(GlanceConstants.CAR_EXT);
        if (carExt == null) {
            return null;
        }
        Bundle convo = carExt.getBundle(GlanceConstants.CAR_EXT_CONVERSATION);
        if (convo == null) {
            return null;
        }
        Parcelable[] messages = convo.getParcelableArray(GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES);
        if (messages == null || messages.length <= 0) {
            return null;
        }
        Parcelable lastMessage = messages[messages.length - 1];
        if (lastMessage == null || !(lastMessage instanceof Bundle)) {
            return null;
        }
        String text = ((Bundle) lastMessage).getString(GlanceConstants.CAR_EXT_CONVERSATION_MESSAGES_TEXT);
        if (!StringUtils.isEmptyAfterTrim(text)) {
            return text;
        }
        return null;
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    public static String getFirstParticipantInCarConversation(@NonNull Bundle extras) {
        Bundle carExt = extras.getBundle(GlanceConstants.CAR_EXT);
        if (carExt != null) {
            Bundle convo = carExt.getBundle(GlanceConstants.CAR_EXT_CONVERSATION);
            if (convo != null) {
                String[] pariticipants = convo.getStringArray(GlanceConstants.CAR_EXT_CONVERSATION_PARTICIPANTS);
                if (pariticipants != null && pariticipants.length > 0) {
                    return pariticipants[0];
                }
            }
        }
        return null;
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    public static String getLastTextLine(@NonNull Bundle extras) {
        Object textLines = extras.get("android.textLines");
        if (textLines != null && (textLines instanceof CharSequence[])) {
            CharSequence[] lines = (CharSequence[]) textLines;
            CharSequence lastLine = lines[lines.length - 1];
            if (!StringUtils.isEmptyAfterTrim(lastLine)) {
                return lastLine.toString();
            }
        }
        return null;
    }

    // @android.support.annotation.Nullable
    @DexIgnore
    public static String getStringSafely(@NonNull Bundle extras, @NonNull String key) {
        Object o = extras.get(key);
        if (o instanceof String) {
            return (String) o;
        }
        if (o instanceof CharSequence) {
            return o.toString();
        }
        return null;
    }

    @DexIgnore
    private void handleStatusBarNotification(@NonNull StatusBarNotification sbn) {
        // try {
        //     String packageName = sbn.getPackageName();
        //     Notification notif = sbn.getNotification();
        //     Bundle extras = NotificationCompat.getExtras(notif);
        //     if (isMusicNotification(sbn)) {
        //         sLogger.i("Music notification posted: " + packageName);
        //         addActiveMusicApp(packageName);
        //     } else if (!this.sharedPrefs.getBoolean(SettingsConstants.GLANCES, false)) {
        //     } else {
        //         if (!sbn.isOngoing()) {
        //             String ticker = null;
        //             if (notif.tickerText != null) {
        //                 ticker = notif.tickerText.toString();
        //             }
        //             if (GlanceConstants.isPackageInGroup(packageName, GlanceConstants.Group.IGNORE_GROUP)) {
        //                 sLogger.d("handleStatusBarNotification: ignored [" + packageName + "]");
        //             } else if (!isConnectedToHud()) {
        //                 sLogger.v("handleStatusBarNotification: hud not connected [" + packageName + "]");
        //             } else if (extras == null) {
        //                 sLogger.v("handleStatusBarNotification: no extras [" + packageName + "]");
        //             } else {
        //                 String notificationHashKey = getHash(sbn);
        //                 if (Boolean.TRUE.equals(this.knownNotifications.get(notificationHashKey))) {
        //                     sLogger.v("handleStatusBarNotification: already seen this notification: [" + notificationHashKey + "] so won't send to HUD to avoid duplicates");
        //                     return;
        //                 }
        //                 this.knownNotifications.put(notificationHashKey, Boolean.TRUE);
        //                 Runnable runnable = new NavdyCustomNotificationListenerService.Anon2(notif, packageName, sbn, ticker);
        //                 cancelPendingHandlerIfAnyFor(sbn);
        //                 sLogger.d("onNotificationPosted:   Adding a pending runnable for " + notificationHashKey);
        //                 this.incomingNotifications.put(notificationHashKey, runnable);
        //                 notifListenerServiceHandler.postDelayed(runnable, 1000);
        //             }
        //         } else if (!TextUtils.equals(this.lastOngoing, packageName)) {
        //             sLogger.v("handleStatusBarNotification: ongoing notif skip:" + packageName);
        //             this.lastOngoing = packageName;
        //         }
        //     }
        // } catch (Throwable t) {
        //     sLogger.e(t);
        // }
    }

    @DexIgnore
    private void categorizeAndHandleNotification(StatusBarNotification sbn, String packageName, String ticker) {
        sLogger.d("categorizeAndHandleNotification: [" + packageName + "] ticker[" + ticker + "]");
        if (GlanceConstants.isPackageInGroup(packageName, GlanceConstants.Group.MESSAGING_GROUP)) {
            MessagingNotificationHandler.handleMessageNotification(sbn);
        } else if (GlanceConstants.isPackageInGroup(packageName, GlanceConstants.Group.EMAIL_GROUP)) {
            EmailNotificationHandler.handleEmailNotification(sbn);
        } else if (GlanceConstants.isPackageInGroup(packageName, GlanceConstants.Group.CALENDAR_GROUP) || GlanceConstants.ANDROID_CALENDAR.equals(packageName)) {
            if (!GlanceConstants.GOOGLE_CALENDAR.equals(packageName) || ticker != null) {
                CalendarNotificationHandler.handleCalendarNotification(sbn);
            } else {
                sLogger.v("null ticker");
            }
        } else if (GlanceConstants.isPackageInGroup(packageName, GlanceConstants.Group.SOCIAL_GROUP)) {
            SocialNotificationHandler.handleSocialNotification(sbn);
        } else {
            GenericNotificationHandler.handleGenericNotification(sbn);
            sLogger.v("NotifListenerService: generic notification [" + packageName + "]");
        }
    }

    @DexIgnore
    public void onNotificationRemoved(StatusBarNotification sbn) {
        // TaskManager.getInstance().execute(new NavdyCustomNotificationListenerService.Anon3(sbn), 7);
    }

    @DexIgnore
    public void cancelPendingHandlerIfAnyFor(StatusBarNotification sbn) {
        String notificationHashKey = getHash(sbn);
        Runnable previousRunnable = this.incomingNotifications.get(notificationHashKey);
        if (previousRunnable != null) {
            sLogger.d("onNotificationPosted: Removing a pending runnable for " + notificationHashKey);
            notifListenerServiceHandler.removeCallbacks(previousRunnable);
        }
    }

    @DexIgnore
    public void removePendingHandlingForGroup(String group) {
        Runnable pendingRunnable = this.pendingGroupNotifications.get(group);
        if (pendingRunnable != null) {
            notifListenerServiceHandler.removeCallbacks(pendingRunnable);
        }
    }

    @DexReplace
    private boolean isMusicNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        // Spotify was previously handled via MediaPlayerStateUpdateReceiver, but
        // spotify started disabling their api notifications by default
        // if (GlanceConstants.SPOTIFY.equals(packageName)) {
        //     return false;
        // }
        if (Build.VERSION.SDK_INT >= 21) {
            String category = NotificationCompat.getCategory(sbn.getNotification());
            if (category != null && category.equals("transport")) {
                return true;
            }
        }
        return sbn.isOngoing() && GlanceConstants.isPackageInGroup(packageName, GlanceConstants.Group.MUSIC_GROUP);
    }

    @DexReplace
    public void addActiveMusicApp(String packageName) {
        if (!this.activeMusicApps.contains(packageName)) {
            this.activeMusicApps.add(packageName);
        }
        MusicServiceHandler.getInstance().setLastMusicApp(packageName);
    }

    @DexIgnore
    public void removeActiveMusicApp(String packageName) {
        this.activeMusicApps.remove(packageName);
    }

    @DexIgnore
    private static boolean isConnectedToHud() {
        return AppInstance.getInstance().isDeviceConnected();
    }

    @DexIgnore
    private void print(StatusBarNotification sbn) {
        if (sbn != null) {
            Notification notif = sbn.getNotification();
            sLogger.d("################ STATUS BAR NOTIFICATION ###############");
            sLogger.d("Time: " + sbn.getPostTime());
            sLogger.d("ID: " + sbn.getId());
            sLogger.d("Tag: " + sbn.getTag());
            sLogger.d("Package: " + sbn.getPackageName());
            sLogger.d("Ongoing: " + sbn.isOngoing());
            sLogger.d("Clearable: " + sbn.isClearable());
            logNotification(notif, 2);
        }
    }

    @DexIgnore
    private void logNotification(Notification notif, int indentation) {
        sLogger.d(String.format("%" + indentation + "s################ NOTIFICATION", SPACES));
        sLogger.d(String.format("%" + indentation + "sPriority: %s", SPACES, notif.priority));
        sLogger.d(String.format("%" + indentation + "sFlags: %s", SPACES, notif.flags));
        sLogger.d(String.format("%" + indentation + "sTicker text: %s", SPACES, notif.tickerText));
        if (Build.VERSION.SDK_INT >= 21) {
            sLogger.d(String.format("%" + indentation + "sPublic Version: %s", SPACES, notif.publicVersion));
        }
        String category = NotificationCompat.getCategory(notif);
        String group = NotificationCompat.getGroup(notif);
        String sortKey = NotificationCompat.getSortKey(notif);
        boolean isGroupSummary = NotificationCompat.isGroupSummary(notif);
        sLogger.d(String.format("%" + indentation + "sCategory: %s", SPACES, category));
        sLogger.d(String.format("%" + indentation + "sGroup: %s", SPACES, group));
        sLogger.d(String.format("%" + indentation + "sSort Key: %s", SPACES, sortKey));
        sLogger.d(String.format("%" + indentation + "sIs Group Summary: %s", SPACES, isGroupSummary));
        Bundle extras = NotificationCompat.getExtras(notif);
        sLogger.d(String.format("%" + indentation + "s####### BUNDLE", SPACES));
        logBundle(indentation + 4, extras);
        sLogger.d(String.format("%" + indentation + "s####### ACTIONS", SPACES));
        int actionCount = NotificationCompat.getActionCount(notif);
        sLogger.d(String.format("%" + indentation + "sAction Count: %d", SPACES, actionCount));
        for (int i = 0; i < actionCount; i++) {
            logAction(indentation, NotificationCompat.getAction(notif, i));
        }
    }

    @DexIgnore
    public void logBundle(int indentation, Bundle extras) {
        if (extras != null && !extras.isEmpty()) {
            for (String key : extras.keySet()) {
                logUnknownObject(indentation, key, extras.get(key));
            }
        }
    }

    @DexIgnore
    public void logUnknownObject(int indentation, String key, Object value) {
        if (value == null) {
            sLogger.d(String.format("%" + indentation + "s- %s: null", SPACES, key));
        } else if (value instanceof Bundle) {
            sLogger.d(String.format("%" + indentation + "s####### %s: \"%s\" (%s)", SPACES, key, value.toString(), value.getClass().getName()));
            logBundle(indentation + 4, (Bundle) value);
        } else if (value instanceof RemoteInput) {
            logRemoteInput(indentation, (RemoteInput) value);
        } else if (value instanceof Notification.Action) {
            logAction(indentation, (Notification.Action) value);
        } else if (value instanceof Notification) {
            logNotification((Notification) value, indentation);
        } else if (value instanceof Object[]) {
            sLogger.d(String.format("%" + indentation + "s- %s: \"%s\" (%s)", SPACES, key, value.toString(), value.getClass().getName()));
            Object[] array = (Object[]) value;
            for (int i = 0; i < array.length; i++) {
                logUnknownObject(indentation + 4, key + "[" + i + "]", array[i]);
            }
        } else if (value instanceof List) {
            sLogger.d(String.format("%" + indentation + "s- %s: \"%s\" (%s)", SPACES, key, value.toString(), value.getClass().getName()));
            List array2 = (List) value;
            for (int i2 = 0; i2 < array2.size(); i2++) {
                logUnknownObject(indentation + 4, key + "[" + i2 + "]", array2.get(i2));
            }
        } else {
            sLogger.d(String.format("%" + indentation + "s- %s: \"%s\" (%s)", SPACES, key, value.toString(), value.getClass().getName()));
        }
    }

    @DexIgnore
    public void logRemoteInput(int indentation, @NonNull android.app.RemoteInput remoteInput) {
        if (Build.VERSION.SDK_INT >= 20) {
            RemoteInput.Builder rib = new RemoteInput.Builder(remoteInput.getResultKey());
            rib.setLabel(remoteInput.getLabel());
            rib.setChoices(remoteInput.getChoices());
            rib.setAllowFreeFormInput(remoteInput.getAllowFreeFormInput());
            rib.addExtras(remoteInput.getExtras());
            logRemoteInput(indentation, rib.build());
        }
    }

    @DexIgnore
    public void logRemoteInput(int indentation, @NonNull RemoteInput input) {
        sLogger.d(String.format("%" + indentation + "sInput: %s", SPACES, input.getLabel()));
        sLogger.d(String.format("%" + indentation + "sAllowFreeFormInput: %s", SPACES, input.getAllowFreeFormInput()));
        CharSequence[] choices = input.getChoices();
        if (choices != null) {
            for (CharSequence choice : choices) {
                sLogger.d(String.format("%" + indentation + "s  + Choice: %s", SPACES, choice));
            }
        }
    }

    @DexIgnore
    public void logAction(int indentation, @NonNull Notification.Action action) {
        logAction(indentation, new NotificationCompat.Action.Builder(action.icon, action.title, action.actionIntent).build());
    }

    @DexIgnore
    public void logAction(int indentation, @NonNull NotificationCompat.Action action) {
        sLogger.d(String.format("%" + indentation + "s-- %s:", SPACES, action.title));
        sLogger.d(String.format("%" + (indentation + 4) + "sAction Intent: %s", SPACES, action.actionIntent));
        if (Build.VERSION.SDK_INT >= 24) {
            sLogger.d(String.format("%" + (indentation + 4) + "sAllow generated replies: %b", SPACES, action.getAllowGeneratedReplies()));
        }
        if (Build.VERSION.SDK_INT >= 20) {
            logBundle(indentation + 4, action.getExtras());
            RemoteInput[] remoteInputs = action.getRemoteInputs();
            if (remoteInputs != null) {
                for (RemoteInput remoteInput : remoteInputs) {
                    logRemoteInput(indentation + 4, remoteInput);
                }
            }
        }
    }
}
