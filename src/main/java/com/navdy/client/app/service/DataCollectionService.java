package com.navdy.client.app.service;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.JsonWriter;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceInfoEvent;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.service.library.events.TripUpdate;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Locale;

import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexReplace
public class DataCollectionService implements SensorEventListener, ConnectionCallbacks, OnConnectionFailedListener {

//    private static final int FAST_REFRESH_RATE = 83333;
//    private static final String FILE_EXTENSION = ".json.gz";
//    private static final boolean LOG_ALL_SENSOR_EVENTS = false;
//    private static final int LOG_FREQUENCY = 1000;
//    private static final int MAX_FOLDER_SIZE = 52428800;
//    private static final int MAX_S3_UPLOAD_RETRY = 3;
//    private static final int MIN_FREE_SPACE = 5242880;
//    private static final int REQUEST_CODE = 654974;
//    private static final String S3_BUCKET_NAME = "navdy-trip-data";
//    private static final String SENSOR_DATA_FOLDER = "SensorData";
//    private static final int SLOW_REFRESH_RATE = 1000;
//    private static final int SUPER_FAST_REFRESH_RATE = 166666;
//    private static final long UPLOAD_DELAY = TimeUnit.MINUTES.toMillis(30);
//    private static int failCount = 0;
//    private static volatile File folder;
    private static DecimalFormat formatter = new DecimalFormat("0.00000000000000", new DecimalFormatSymbols(Locale.US));
//    private static Logger logger = new Logger(DataCollectionService.class);
//    private static volatile File rootFolder;
//    private static AtomicBoolean sendInProgress = new AtomicBoolean(false);
//    private static TransferObserver transferObserver;
    private static Boolean useNanoTime = null;
//    private Sensor accelerometer = null;
//    private Handler bgHandler;
    public Data data = new Data();
//    private final Object dataLock = new Object();
//    private GoogleApiClient googleApiClient;
//    private Sensor gravity = null;
//    private Sensor gyroscope = null;
//    private boolean isCollecting = false;
//    private boolean isConnected = false;
//    private boolean isDriving = false;
//    private long lastGravityTimeStamp = 0;
//    private long lastGyroscopeTimeStamp = 0;
//    private long lastLinearAccelerationTimeStamp = 0;
//    private Location lastLocation = null;
//    private int lastMagFieldAccuracy = 0;
//    private long lastMagneticFieldTimeStamp = 0;
//    private long lastOrientationTimeStamp = 0;
//    private long lastRotationVectorTimeStamp = 0;
//    private Sensor linearAcceleration = null;
//    private Runnable locationUpdatesAtRegularInterval = new Runnable() {
//        public void run() {
//            DataCollectionService.this.addLastLocationToData();
//        }
//    };
//    private Sensor magneticField = null;
//    private Sensor rotationVector = null;
//    private SensorManager sensorManager = null;
//    private TransferUtility transferUtility = new TransferUtility(OTAUpdateManagerImpl.createS3Client(), NavdyApplication.getAppContext());
//    private Handler uploadScheduler;

    @DexIgnore
    static class Data {
//        static final int MAX_RECORDS = 750000;
//        static final int UNKNOWN_ACCURACY = -1;
//        LinkedList<Activity> activities = new LinkedList<>();
//        String appVersion = HomescreenActivity.getAppVersionString();
//        String device = (Build.MANUFACTURER + " " + Build.MODEL);
//        long endTimestamp;
//        LinkedList<SensorData> gravity = new LinkedList<>();
//        LinkedList<SensorData> gyroscope = new LinkedList<>();
//        boolean isPersistedToFile = false;
//        boolean isSplitData = false;
//        float[] lastAccelerometerData = null;
//        LinkedList<SensorData> linearAcceleration = new LinkedList<>();
//        LinkedList<Location> locations = new LinkedList<>();
//        LinkedList<SensorData> magneticField = new LinkedList<>();
//        LinkedList<SensorData> orientation = new LinkedList<>();
//        String osVersion = (VERSION.SDK_INT + "(" + VERSION.RELEASE + ")");
//        String platform = "Android";
//        LinkedList<SensorData> rotationVector = new LinkedList<>();
//        long startTimestamp;
//        int timeZoneOffset;
//        long tripUuid;
//
//        @DexIgnore
//        public static class Activity {
//            int confidence = 0;
//            boolean isDriving = true;
//            long timestamp;
//
//            public Activity(int confidence) {
//                this.confidence = confidence;
//                this.timestamp = System.currentTimeMillis();
//            }
//
//            public String toString() {
//                return "Activity{confidence=" + this.confidence + ", isDriving=" + this.isDriving + ", timestamp=" + DataCollectionService.getTimestampString(this.timestamp) + '}';
//            }
//
//            void writeToJson(@NonNull JsonWriter writer) throws IOException {
//                writer.beginObject();
//                writer.name("timestamp").value(DataCollectionService.getTimestampString(this.timestamp));
//                writer.name("confidence").value((long) this.confidence);
//                writer.name("is_driving").value(this.isDriving);
//                writer.endObject();
//            }
//        }
//
//        @DexIgnore
//        public static class Location {
//            Float accuracy;
//            Double altitude;
//            Float heading;
//            Double latitude;
//            Double longitude;
//            Float speed;
//            Long timestamp;
//            int tripDistance;
//
//            public String toString() {
//                return "Location{  timestamp=" + DataCollectionService.getTimestampString(this.timestamp) + ", \tlatitude=" + this.latitude + ", \tlongitude=" + this.longitude + ", \taltitude='" + this.altitude + '\'' + ", \tspeed=" + this.speed + ", \theading=" + this.heading + ", \taccuracy='" + this.accuracy + '\'' + ", \ttripDistance=" + this.tripDistance + '}';
//            }
//
//            void writeToJson(@NonNull JsonWriter writer) throws IOException {
//                writer.beginObject();
//                if (this.accuracy != null) {
//                    writer.name("accuracy").value(this.accuracy);
//                }
//                if (this.altitude != null) {
//                    writer.name("altitude").value(DataCollectionService.getDoubleString(this.altitude));
//                }
//                if (this.heading != null) {
//                    writer.name("heading").value(this.heading);
//                }
//                if (this.latitude != null) {
//                    writer.name("latitude").value(this.latitude);
//                }
//                if (this.longitude != null) {
//                    writer.name("longitude").value(this.longitude);
//                }
//                if (this.speed != null) {
//                    writer.name("speed").value(this.speed);
//                }
//                writer.name("trip-distance").value((long) this.tripDistance);
//                writer.name("timestamp").value(DataCollectionService.getTimestampString(this.timestamp));
//                writer.endObject();
//            }
//        }
//
//        @DexIgnore
//        public static class SensorData {
//            int accuracy = -1;
//            boolean hasW = false;
//            long timestamp;
//            float[] values;
//
//            SensorData(SensorEvent event) {
//                this.timestamp = getEpochTimestamp(event);
//                this.values = (float[]) event.values.clone();
//            }
//
//            private long getEpochTimestamp(SensorEvent event) {
//                long theRightDiff;
//                if (DataCollectionService.useNanoTime == null) {
//                    long nanoTime = System.nanoTime();
//                    long realtimeNanos = SystemClock.elapsedRealtimeNanos();
//                    long nanoTimeDiff = Math.abs(event.timestamp - nanoTime);
//                    long realtimeNanosDiff = Math.abs(event.timestamp - realtimeNanos);
//                    if (nanoTimeDiff > realtimeNanosDiff) {
//                        theRightDiff = event.timestamp - realtimeNanos;
//                        if (realtimeNanosDiff > 0 && nanoTimeDiff > 2 * realtimeNanosDiff) {
//                            DataCollectionService.useNanoTime = Boolean.valueOf(false);
//                        }
//                    } else {
//                        theRightDiff = event.timestamp - nanoTime;
//                        if (nanoTimeDiff > 0 && realtimeNanosDiff > 2 * nanoTimeDiff) {
//                            DataCollectionService.useNanoTime = Boolean.valueOf(true);
//                        }
//                    }
//                } else {
//                    theRightDiff = DataCollectionService.useNanoTime.booleanValue() ? event.timestamp - System.nanoTime() : event.timestamp - SystemClock.elapsedRealtimeNanos();
//                }
//                return (theRightDiff / 1000000) + System.currentTimeMillis();
//            }
//
//            public String toString() {
//                return "SensorData{timestamp=" + DataCollectionService.getTimestampString(this.timestamp) + ", \tvalues=" + Arrays.toString(this.values) + (this.accuracy != -1 ? ", \taccuracy=" + this.accuracy : "") + '}';
//            }
//
//            public void writeToJson(@NonNull JsonWriter writer) throws IOException {
//                writer.beginObject();
//                if (this.accuracy != -1) {
//                    writer.name("accuracy").value((long) this.accuracy);
//                }
//                writer.name("timestamp").value(DataCollectionService.getTimestampString(this.timestamp));
//                writer.name("x").value((double) this.values[0]);
//                writer.name("y").value((double) this.values[1]);
//                writer.name("z").value((double) this.values[2]);
//                if (this.hasW && this.values.length > 3) {
//                    writer.name("w").value((double) this.values[3]);
//                }
//                writer.endObject();
//            }
//        }
//
//        @DexIgnore
//        public static class OrientationData extends SensorData {
//            float dt = 0.0f;
//
//            OrientationData(SensorEvent event) {
//                super(event);
//            }
//
//            public String toString() {
//                return "SensorData{timestamp=" + DataCollectionService.getTimestampString(this.timestamp) + ", \tdt=" + this.dt + ", \tazimuth=" + this.values[0] + ", \tpitch=" + this.values[1] + ", \troll=" + this.values[2] + (this.accuracy != -1 ? ", \taccuracy=" + this.accuracy : "") + '}';
//            }
//
//            public void writeToJson(@NonNull JsonWriter writer) throws IOException {
//                writer.beginObject();
//                if (this.accuracy != -1) {
//                    writer.name("accuracy").value((long) this.accuracy);
//                }
//                writer.name("timestamp").value(DataCollectionService.getTimestampString(this.timestamp));
//                writer.name("dt").value((double) this.dt);
//                writer.name("azimuth").value((double) this.values[0]);
//                writer.name("pitch").value((double) this.values[1]);
//                writer.name("roll").value((double) this.values[2]);
//                writer.endObject();
//            }
//        }
//
//        Data() {
//        }
//
//        int getLastTripDistance() {
//            if (this.locations == null || this.locations.size() <= 0) {
//                return 0;
//            }
//            return ((Location) this.locations.getLast()).tripDistance;
//        }
//
//        public String toString() {
//            return "Data{appVersion='" + this.appVersion + '\'' + ", device='" + this.device + '\'' + ", platform='" + this.platform + '\'' + ", osVersion='" + this.osVersion + '\'' + ", tripUuid=" + this.tripUuid + ", timeZoneOffset=" + this.timeZoneOffset + ", startTimestamp=" + this.startTimestamp + ", endTimestamp=" + this.endTimestamp + ", locations.size=" + this.locations.size() + ", activities.size=" + this.activities.size() + ", orientation.size=" + this.orientation.size() + ", linearAcceleration.size=" + this.linearAcceleration.size() + ", gravity.size=" + this.gravity.size() + ", magneticField.size=" + this.magneticField.size() + ", gyroscope.size=" + this.gyroscope.size() + ", rotationVector.size=" + this.rotationVector.size() + ", isPersistedToFile=" + this.isPersistedToFile + ", isSplitData=" + this.isSplitData + '}';
//        }
    }

    @DexIgnore
    private static class DataJsonFormat {
        static final String ACTIVITIES = "android_drivings";
        static final String ANDROID_SENSORS = "android_sensors";
        static final String APP_VERSION = "app_version";
        static final String DEVICE = "device";
        static final String ENDTIMESTAMP = "endTimestamp";
        static final String GRAVITY = "gravities";
        static final String GYROSCOPE = "gyroscopes";
        static final String IS_SPLIT_DATA = "is_split_data";
        static final String LINEAR_ACCELERATION = "linear_accelerations";
        static final String LOCATIONS = "locations";
        static final String MAGNETIC_FIELD = "magnetic_fields";
        static final String ORIENTATION = "orientations";
        static final String OS_VERSION = "os_version";
        static final String PLATFORM = "platform";
        static final String ROTATION_VECTOR = "rotation_vectors";
        static final String STARTTIMESTAMP = "startTimestamp";
        static final String TIME_ZONE_OFFSET = "time_zone_offset";
        static final String TRIP_UUID = "trip_uuid";
        static final String VEHICLE_MAKE = "vehicle-make";
        static final String VEHICLE_MODEL = "vehicle-model";
        static final String VEHICLE_YEAR = "vehicle-year";
        static final String VERSION = "version";
        static final int VERSION_NUMBER = 1;

        @DexIgnore
        static class Activity {
            static final String CONFIDENCE = "confidence";
            static final String IS_DRIVING = "is_driving";
            static final String TIMESTAMP = "timestamp";

            Activity() {
            }
        }

        @DexIgnore
        static class Location {
            static final String ACCURACY = "accuracy";
            static final String ALTITUDE = "altitude";
            static final String HEADING = "heading";
            static final String LATITUDE = "latitude";
            static final String LONGITUDE = "longitude";
            static final String SPEED = "speed";
            static final String TIMESTAMP = "timestamp";
            static final String TRIP_DISTANCE = "trip-distance";

            Location() {
            }
        }

        @DexIgnore
        static class SensorData {
            static final String ACCURACY = "accuracy";
            static final String AZIMUTH = "azimuth";
            static final String DT = "dt";
            static final String PITCH = "pitch";
            static final String ROLL = "roll";
            static final String TIMESTAMP = "timestamp";
            static final String W = "w";
            static final String X = "x";
            static final String Y = "y";
            static final String Z = "z";

            SensorData() {
            }
        }

        private DataJsonFormat() {
        }
    }

    @DexIgnore
    private static class InstanceHolder {
        static final DataCollectionService instance = new DataCollectionService();

        private InstanceHolder() {
        }
    }

//    static /* synthetic */ int access$1808() {
//        int i = failCount;
//        failCount = i + 1;
//        return i;
//    }

    public static DataCollectionService getInstance() {
        return InstanceHolder.instance;
    }

    static String getTimestampString(long timestamp) {
        String str1 = String.valueOf(timestamp / 1000);
        return str1 + "." + String.format(Locale.US, "%03d", timestamp % 1000);
    }

    static String getDoubleString(double d) {
        return formatter.format(d);
    }

    DataCollectionService() {
    }

    public void startService(Context context) {
//        ensureFolderExists();
//        moveAnyTempFileToUploadFolderAndSendIfWiFi();
//        if (!shouldNotCollectData()) {
//            BusProvider.getInstance().register(this);
//            HandlerThread bgHandlerThread = new HandlerThread("BgHandlerThread");
//            bgHandlerThread.start();
//            this.bgHandler = new Handler(bgHandlerThread.getLooper());
//            HandlerThread uploadThread = new HandlerThread("DataCollectionHandler");
//            uploadThread.start();
//            this.uploadScheduler = new Handler(uploadThread.getLooper());
//            this.sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
//            if (this.sensorManager == null) {
//                logger.e("Unable to get sensor manager!!!");
//                return;
//            }
//            this.accelerometer = this.sensorManager.getDefaultSensor(1);
//            if (this.accelerometer == null) {
//                logger.e("The phone doesn't seem to have an accelerometer :'(");
//            }
//            this.linearAcceleration = this.sensorManager.getDefaultSensor(10);
//            if (this.linearAcceleration == null) {
//                logger.e("The phone doesn't seem to have a linearAcceleration :'(");
//            }
//            this.gravity = this.sensorManager.getDefaultSensor(9);
//            if (this.gravity == null) {
//                logger.e("The phone doesn't seem to have a gravity :'(");
//            }
//            this.magneticField = this.sensorManager.getDefaultSensor(2);
//            if (this.magneticField == null) {
//                logger.e("The phone doesn't seem to have a magneticField :'(");
//            }
//            this.gyroscope = this.sensorManager.getDefaultSensor(4);
//            if (this.gyroscope == null) {
//                logger.e("The phone doesn't seem to have a gyroscope :'(");
//            }
//            this.rotationVector = this.sensorManager.getDefaultSensor(11);
//            if (this.rotationVector == null) {
//                logger.e("The phone doesn't seem to have a rotationVector :'(");
//            }
//            initGoogleApi(context);
//        }
    }
//
//    private synchronized void initGoogleApi(Context context) {
//        this.googleApiClient = new Builder(context).addApi(ActivityRecognition.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
//        this.googleApiClient.connect();
//    }

    @Subscribe
    public void onDeviceConnected(DeviceConnectedEvent deviceConnectedEvent) {
//        logger.v("onDeviceConnected");
//        TaskManager.getInstance().execute(new Runnable() {
//            public void run() {
//                synchronized (DataCollectionService.this.dataLock) {
//                    DataCollectionService.this.isConnected = true;
//                }
//                DataCollectionService.this.updateHudUuid();
//                DataCollectionService.this.startCollecting();
//            }
//        }, 1);
    }

    @Subscribe
    public void onDeviceInfoEvent(DeviceInfoEvent deviceInfoEvent) {
//        TaskManager.getInstance().execute(new Runnable() {
//            public void run() {
//                DataCollectionService.this.updateHudUuid();
//            }
//        }, 1);
    }

    @Subscribe
    public void onDeviceDisconnected(DeviceDisconnectedEvent deviceDisconnectedEvent) {
//        logger.v("onDeviceDisconnected");
//        TaskManager.getInstance().execute(new Runnable() {
//            public void run() {
//                synchronized (DataCollectionService.this.dataLock) {
//                    DataCollectionService.this.isConnected = false;
//                }
//                DataCollectionService.this.checkForStop();
//            }
//        }, 1);
    }

    @Subscribe
    public void onNavigationSessionStatusEvent(NavigationSessionStatusEvent event) {
    }
//
//    private String getHudUuidFromAppInstance() {
//        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
//        if (remoteDevice != null) {
//            DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
//            if (!(deviceInfo == null || deviceInfo.deviceUuid == null)) {
//                String str;
//                synchronized (this.dataLock) {
//                    str = deviceInfo.deviceUuid;
//                }
//                return str;
//            }
//        }
//        return null;
//    }
//
//    private void updateHudUuid() {
//        String hudUuid = getHudUuidFromAppInstance();
//        if (!StringUtils.isEmptyAfterTrim(hudUuid)) {
//            SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.LAST_HUD_UUID, hudUuid).apply();
//        }
//    }
//
//    private String getHudUuid() {
//        String hudUuid = getHudUuidFromAppInstance();
//        return !StringUtils.isEmptyAfterTrim(hudUuid) ? hudUuid : SettingsUtils.getSharedPreferences().getString(SettingsConstants.LAST_HUD_UUID, SettingsConstants.LAST_HUD_UUID_DEFAULT);
//    }
//
//    /* JADX WARNING: Missing block: B:10:0x0016, code:
//            logger.i("Starting sensor data collection");
//            initDataIfNotAlready();
//            r5.bgHandler.postDelayed(r5.locationUpdatesAtRegularInterval, 1000);
//     */
//    /* JADX WARNING: Missing block: B:11:0x002b, code:
//            if (r5.accelerometer == null) goto L_0x0037;
//     */
//    /* JADX WARNING: Missing block: B:12:0x002d, code:
//            r5.sensorManager.registerListener(r5, r5.accelerometer, SUPER_FAST_REFRESH_RATE);
//     */
//    /* JADX WARNING: Missing block: B:14:0x0039, code:
//            if (r5.linearAcceleration == null) goto L_0x0042;
//     */
//    /* JADX WARNING: Missing block: B:15:0x003b, code:
//            r5.sensorManager.registerListener(r5, r5.linearAcceleration, FAST_REFRESH_RATE);
//     */
//    /* JADX WARNING: Missing block: B:17:0x0044, code:
//            if (r5.gravity == null) goto L_0x004d;
//     */
//    /* JADX WARNING: Missing block: B:18:0x0046, code:
//            r5.sensorManager.registerListener(r5, r5.gravity, FAST_REFRESH_RATE);
//     */
//    /* JADX WARNING: Missing block: B:20:0x004f, code:
//            if (r5.magneticField == null) goto L_0x0058;
//     */
//    /* JADX WARNING: Missing block: B:21:0x0051, code:
//            r5.sensorManager.registerListener(r5, r5.magneticField, FAST_REFRESH_RATE);
//     */
//    /* JADX WARNING: Missing block: B:23:0x005a, code:
//            if (r5.gyroscope == null) goto L_0x0063;
//     */
//    /* JADX WARNING: Missing block: B:24:0x005c, code:
//            r5.sensorManager.registerListener(r5, r5.gyroscope, FAST_REFRESH_RATE);
//     */
//    /* JADX WARNING: Missing block: B:26:0x0065, code:
//            if (r5.rotationVector == null) goto L_0x006e;
//     */
//    /* JADX WARNING: Missing block: B:27:0x0067, code:
//            r5.sensorManager.registerListener(r5, r5.rotationVector, FAST_REFRESH_RATE);
//     */
//    /* JADX WARNING: Missing block: B:28:0x006e, code:
//            requestActivityRecognitionUpdates();
//     */
//    /* JADX WARNING: Missing block: B:36:?, code:
//            return;
//     */
//    /* JADX WARNING: Missing block: B:37:?, code:
//            return;
//     */
//    /* Code decompiled incorrectly, please refer to instructions dump. */
//    private void startCollecting() {
//        synchronized (this.dataLock) {
//            if (shouldNotCollectData() || this.isCollecting) {
//            } else {
//                this.isCollecting = true;
//            }
//        }
//    }
//
//    private static boolean shouldNotCollectData() {
//        return !SettingsUtils.isBetaUser() || isAboveTheLimitOfFolderSize();
//    }
//
//    private void stopCollecting() {
//        logger.i("Stopping sensor data collection");
//        this.bgHandler.removeCallbacksAndMessages(null);
//        this.sensorManager.unregisterListener(this);
//        removeActivityRecognitionUpdates();
//        writeDataToZipFileAndScheduleForUpload();
//        synchronized (this.dataLock) {
//            this.isCollecting = false;
//        }
//    }
//
//    private PendingIntent getPendingIntent() {
//        Context context = NavdyApplication.getAppContext();
//        return PendingIntent.getService(context, REQUEST_CODE, new Intent(context, ActivityRecognizedCallbackService.class), 134217728);
//    }
//
//    private synchronized void requestActivityRecognitionUpdates() {
//        logger.d("GoogleApiClient : requesting activity updates");
//        if (this.googleApiClient == null || !this.googleApiClient.isConnected()) {
//            logger.d("The GoogleApiClient is not connected yet. Waiting for it to be connected");
//        } else {
//            logger.d("GoogleApiClient is already connected, requesting the updates");
//            PendingIntent pendingIntent = getPendingIntent();
//            if (ActivityRecognition.ActivityRecognitionApi != null) {
//                ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(this.googleApiClient, 1000, pendingIntent);
//            }
//        }
//    }
//
//    private synchronized void removeActivityRecognitionUpdates() {
//        logger.d("GoogleApiClient : removing activity updates");
//        if (this.googleApiClient != null && this.googleApiClient.isConnected()) {
//            PendingIntent pendingIntent = getPendingIntent();
//            if (ActivityRecognition.ActivityRecognitionApi != null) {
//                ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(this.googleApiClient, pendingIntent);
//            }
//        }
//    }

    public synchronized void onConnected(@Nullable Bundle bundle) {
//        logger.d("GoogleApiClient : connected ");
//        if (AppInstance.getInstance().isDeviceConnected()) {
//            logger.d("HUD is already connected");
//            requestActivityRecognitionUpdates();
//        }
    }

    public void onConnectionSuspended(int i) {
//        logger.d("GoogleApiClient : onConnectionSuspended , Connection :" + i);
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        logger.d("GoogleApiClient : onConnectionFailed , Connection result : " + connectionResult);
    }

   void handleDetectedActivities(final boolean isDriving, final int confidence) {
//        TaskManager.getInstance().execute(new Runnable() {
//            public void run() {
//                synchronized (DataCollectionService.this.dataLock) {
//                    Activity activity = new Activity(confidence);
//                    activity.isDriving = isDriving;
//                    DataCollectionService.this.isDriving = isDriving;
//                    DataCollectionService.this.data.activities.add(activity);
//                    if (DataCollectionService.this.data.activities.size() % 1000 == 0) {
//                        DataCollectionService.logger.d("ACTIVITY_MONITOR   : nbRecords=" + DataCollectionService.this.data.activities.size() + " \tisDriving=" + isDriving + " \tconfidence=" + confidence);
//                    }
//                    if (DataCollectionService.this.data.activities.size() > 750000) {
//                        DataCollectionService.this.writeDataToFileAndStartNewData(DataCollectionService.this.data.tripUuid);
//                    }
//                    DataCollectionService.this.checkForStop();
//                }
//            }
//        }, 1);
   }

//    private void checkForStop() {
//        synchronized (this.dataLock) {
//            if (!(this.isDriving || this.isConnected || !this.isCollecting)) {
//                stopCollecting();
//            }
//        }
//    }

    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
//        if (sensor == this.magneticField) {
//            this.lastMagFieldAccuracy = accuracy;
//        }
    }

    public final void onSensorChanged(final SensorEvent event) {
//        TaskManager.getInstance().execute(new Runnable() {
//            public void run() {
//                DataCollectionService.this.handleSensorEvent(event);
//            }
//        }, 1);
    }

//    private void handleSensorEvent(SensorEvent event) {
//        SensorData sensorData = new SensorData(event);
//        synchronized (this.dataLock) {
//            if (event.sensor == this.accelerometer) {
//                this.data.lastAccelerometerData = event.values;
//            } else if (event.sensor == this.linearAcceleration) {
//                if (event.timestamp - this.lastLinearAccelerationTimeStamp >= 83333000) {
//                    if (this.data.linearAcceleration.size() % 1000 == 0) {
//                        logger.d("LINEAR_ACCELERATION: nbRecords=" + this.data.linearAcceleration.size() + " \tdata=" + sensorData);
//                    }
//                    this.lastLinearAccelerationTimeStamp = event.timestamp;
//                    this.data.linearAcceleration.add(sensorData);
//                }
//            } else if (event.sensor == this.gravity) {
//                if (event.timestamp - this.lastGravityTimeStamp >= 83333000) {
//                    if (this.data.gravity.size() % 1000 == 0) {
//                        logger.d("GRAVITY            : nbRecords=" + this.data.gravity.size() + " \tdata=" + sensorData);
//                    }
//                    this.lastGravityTimeStamp = event.timestamp;
//                    this.data.gravity.add(sensorData);
//                }
//            } else if (event.sensor == this.magneticField) {
//                if (event.timestamp - this.lastMagneticFieldTimeStamp >= 83333000) {
//                    sensorData.accuracy = this.lastMagFieldAccuracy;
//                    if (this.data.magneticField.size() % 1000 == 0) {
//                        logger.d("MAGNETIC_FIELD     : nbRecords=" + this.data.magneticField.size() + " \tdata=" + sensorData);
//                    }
//                    this.lastMagneticFieldTimeStamp = event.timestamp;
//                    this.data.magneticField.add(sensorData);
//                    if (this.data.lastAccelerometerData != null && this.data.lastAccelerometerData.length > 2) {
//                        float[] R = new float[9];
//                        if (SensorManager.getRotationMatrix(R, new float[9], this.data.lastAccelerometerData, event.values)) {
//                            OrientationData orientationData = new OrientationData(event);
//                            SensorManager.getOrientation(R, orientationData.values);
//                            if (this.lastOrientationTimeStamp > 0) {
//                                orientationData.dt = ((float) (event.timestamp - this.lastOrientationTimeStamp)) / 1000000.0f;
//                            }
//                            this.lastOrientationTimeStamp = event.timestamp;
//                            this.data.orientation.add(orientationData);
//                            if (this.data.orientation.size() % 1000 == 0) {
//                                logger.d("ORIENTATION        : nbRecords=" + this.data.orientation.size() + " \tdata=" + orientationData);
//                            }
//                        }
//                    }
//                }
//            } else if (event.sensor == this.gyroscope) {
//                if (event.timestamp - this.lastGyroscopeTimeStamp >= 83333000) {
//                    if (this.data.gyroscope.size() % 1000 == 0) {
//                        logger.d("GYROSCOPE          : nbRecords=" + this.data.gyroscope.size() + " \tdata=" + sensorData);
//                    }
//                    this.lastGyroscopeTimeStamp = event.timestamp;
//                    this.data.gyroscope.add(sensorData);
//                }
//            } else if (event.sensor == this.rotationVector && event.timestamp - this.lastRotationVectorTimeStamp >= 83333000) {
//                if (this.data.rotationVector.size() % 1000 == 0) {
//                    logger.d("ROTATION_VECTOR    : nbRecords=" + this.data.rotationVector.size() + " \tdata=" + sensorData);
//                }
//                this.lastRotationVectorTimeStamp = event.timestamp;
//                sensorData.hasW = true;
//                this.data.rotationVector.add(sensorData);
//            }
//            if (this.data.orientation.size() > 750000 || this.data.linearAcceleration.size() > 750000 || this.data.gravity.size() > 750000 || this.data.magneticField.size() > 750000 || this.data.gyroscope.size() > 750000 || this.data.rotationVector.size() > 750000) {
//                writeDataToFileAndStartNewData(this.data.tripUuid);
//            }
//        }
//    }

//    private void addLastLocationToData() {
//        TaskManager.getInstance().execute(new Runnable() {
//            public void run() {
//                synchronized (DataCollectionService.this.dataLock) {
//                    if (DataCollectionService.this.lastLocation != null) {
//                        DataCollectionService.logger.d("LOCATION           : nbRecords=" + DataCollectionService.this.data.locations.size() + " \tdata=" + DataCollectionService.this.lastLocation);
//                        DataCollectionService.this.data.locations.add(DataCollectionService.this.lastLocation);
//                        DataCollectionService.this.lastLocation = null;
//                    }
//                    DataCollectionService.this.bgHandler.postDelayed(DataCollectionService.this.locationUpdatesAtRegularInterval, 1000);
//                }
//            }
//        }, 1);
//    }

    @Subscribe
    public void onTripUpdate(final TripUpdate tripUpdate) {
//        synchronized (this.dataLock) {
//            if (this.isCollecting) {
//                TaskManager.getInstance().execute(new Runnable() {
//                    public void run() {
//                        DataCollectionService.this.handleTripUpdate(tripUpdate);
//                    }
//                }, 1);
//                return;
//            }
//        }
    }

//    private void handleTripUpdate(TripUpdate tripUpdate) {
//        if (tripUpdate != null) {
//            if (this.data == null) {
//                resetTheData();
//            }
//            if (this.data.isPersistedToFile) {
//                if (tripUpdate.meters_traveled_since_boot >= this.data.getLastTripDistance()) {
//                    removeAnyTemporaryFiles();
//                    this.data.isPersistedToFile = false;
//                } else {
//                    moveAnyTempFileToUploadFolderAndSendIfWiFi();
//                    resetTheData();
//                }
//            }
//            this.lastLocation = new Location();
//            if (tripUpdate.last_raw_coordinate != null) {
//                this.lastLocation.accuracy = tripUpdate.last_raw_coordinate.accuracy;
//                this.lastLocation.altitude = tripUpdate.last_raw_coordinate.altitude;
//                this.lastLocation.heading = tripUpdate.last_raw_coordinate.bearing;
//                this.lastLocation.latitude = tripUpdate.last_raw_coordinate.latitude;
//                this.lastLocation.longitude = tripUpdate.last_raw_coordinate.longitude;
//                this.lastLocation.speed = tripUpdate.last_raw_coordinate.speed;
//                this.lastLocation.timestamp = tripUpdate.last_raw_coordinate.timestamp;
//            } else {
//                this.lastLocation.timestamp = Long.valueOf(System.currentTimeMillis());
//            }
//            this.lastLocation.tripDistance = tripUpdate.meters_traveled_since_boot;
//        }
//    }
//
//    private static void ensureFolderExists() {
//        if (rootFolder == null || !rootFolder.exists() || folder == null || !folder.exists()) {
//            File internalStorage = NavdyApplication.getAppContext().getFilesDir();
//            String appVersionCode = getAppVersionCode();
//            String rootFolderPath = internalStorage + File.separator + SENSOR_DATA_FOLDER;
//            rootFolder = new File(rootFolderPath);
//            folder = new File(rootFolderPath + File.separator + appVersionCode);
//            if (!folder.exists() && !folder.mkdirs()) {
//                logger.e("Unable to create the output folder for the sensor data: " + folder);
//            }
//        }
//    }
//
//    private void writeDataToFileAndStartNewData(long tripUuid) {
//        synchronized (this.dataLock) {
//            this.data.isSplitData = true;
//            writeDataToZipFileAndScheduleForUpload();
//            moveAnyTempFileToUploadFolderAndSendIfWiFi();
//            resetTheData();
//            this.data.isSplitData = true;
//            if (shouldNotCollectData()) {
//                stopCollecting();
//            }
//            logger.i("Starting new sensor data collection");
//            this.data.tripUuid = tripUuid;
//        }
//    }
//
//    private void resetTheData() {
//        synchronized (this.dataLock) {
//            this.data = new Data();
//            this.lastLocation = null;
//            this.lastMagFieldAccuracy = 0;
//            this.lastLinearAccelerationTimeStamp = 0;
//            this.lastGravityTimeStamp = 0;
//            this.lastMagneticFieldTimeStamp = 0;
//            this.lastGyroscopeTimeStamp = 0;
//            this.lastRotationVectorTimeStamp = 0;
//            this.lastOrientationTimeStamp = 0;
//            initDataIfNotAlready();
//        }
//    }
//
//    private void initDataIfNotAlready() {
//        if (this.data == null) {
//            this.data = new Data();
//        }
//        if (this.data.startTimestamp == 0) {
//            this.data.startTimestamp = System.currentTimeMillis();
//        }
//        if (this.data.timeZoneOffset == 0) {
//            TimeZone tz = TimeZone.getDefault();
//            this.data.timeZoneOffset = tz.getOffset(System.currentTimeMillis()) / 1000;
//        }
//        if (this.data.tripUuid == 0) {
//            this.data.tripUuid = System.currentTimeMillis();
//        }
//    }
//
//    private static boolean isAboveTheLimitOfFolderSize() {
//        return fileSize(rootFolder) > 52428800 || availableSdCardSpace() < 5242880;
//    }
//
//    private static long fileSize(File fileOrFolder) {
//        long result = 0;
//        if (fileOrFolder == null || !fileOrFolder.exists()) {
//            return 0;
//        }
//        if (!fileOrFolder.isDirectory()) {
//            return 0 + fileOrFolder.length();
//        }
//        File[] fileList = fileOrFolder.listFiles();
//        if (fileList == null) {
//            return 0;
//        }
//        for (File file : fileList) {
//            result += fileSize(file);
//        }
//        return result;
//    }

//    private static long availableSdCardSpace() {
//        StatFs stat = new StatFs(rootFolder.getPath());
//        return stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
//    }
//
//    private void writeDataToZipFileAndScheduleForUpload() {
//        String path = writeDataToZipFile();
//        if (!StringUtils.isEmptyAfterTrim(path)) {
//            File file = new File(path);
//            if (!file.exists() || file.length() <= 0) {
//                logger.e("Unable find output file: " + file + " doesn't exist or is 0 length");
//                if (!file.delete()) {
//                    logger.e("Unable to erase data file: " + file);
//                    return;
//                }
//                return;
//            }
//            scheduleForUpload();
//        }
//    }

//    private void scheduleForUpload() {
//        this.uploadScheduler.postDelayed(new Runnable() {
//            public void run() {
//                DataCollectionService.this.moveAnyTempFileToUploadFolderAndSendIfWiFi();
//            }
//        }, UPLOAD_DELAY);
//    }

    public void handleLowMemory() {
//        TaskManager.getInstance().execute(new Runnable() {
//            public void run() {
//                synchronized (DataCollectionService.this.dataLock) {
//                    DataCollectionService.logger.w("We are about to run out of memory");
//                    if (DataCollectionService.this.isCollecting) {
//                        DataCollectionService.this.writeDataToFileAndStartNewData(DataCollectionService.this.data.tripUuid);
//                    } else {
//                        DataCollectionService.this.moveAnyTempFileToUploadFolderAndSendIfWiFi();
//                        DataCollectionService.this.resetTheData();
//                    }
//                }
//            }
//        }, 1);
    }

//    private void moveAnyTempFileToUploadFolderAndSendIfWiFi() {
//        moveAnyPendingFileToUploadFolder();
//        sendDataToS3IfOnWiFi();
//    }
//
//    private void moveAnyPendingFileToUploadFolder() {
//        if (this.uploadScheduler != null) {
//            this.uploadScheduler.removeCallbacksAndMessages(null);
//        }
//        ensureFolderExists();
//        boolean pendingFilesExist = false;
//        File[] files = rootFolder.listFiles();
//        if (files == null) {
//            logger.e("Unable to list files in rootFolder: " + rootFolder);
//            return;
//        }
//        for (File from : files) {
//            if (!from.isDirectory()) {
//                File to = new File(folder.getPath() + File.separator + from.getName());
//                logger.d("Moving " + from + " to " + to);
//                if (from.renameTo(to)) {
//                    pendingFilesExist = true;
//                } else {
//                    logger.e("Unable to move " + from + " to upload folder: " + to);
//                }
//            }
//        }
//        if (pendingFilesExist) {
//            SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.PENDING_SENSOR_DATA_EXIST, true).apply();
//        }
//    }

//    private void removeAnyTemporaryFiles() {
//        if (this.uploadScheduler != null) {
//            this.uploadScheduler.removeCallbacksAndMessages(null);
//        }
//        ensureFolderExists();
//        if (rootFolder != null) {
//            File[] files = rootFolder.listFiles();
//            if (files == null) {
//                logger.e("Unable to list files in rootFolder: " + rootFolder);
//                return;
//            }
//            for (File file : files) {
//                if (!file.isDirectory()) {
//                    if (file.delete()) {
//                        logger.d("Removed " + file);
//                    } else {
//                        logger.e("Unable to erase temporary file: " + file);
//                    }
//                }
//            }
//        }
//    }

//    /* JADX WARNING: Removed duplicated region for block: B:25:0x0349 A:{SYNTHETIC, Splitter: B:25:0x0349} */
//    /* JADX WARNING: Removed duplicated region for block: B:33:0x035e A:{SYNTHETIC, Splitter: B:33:0x035e} */
//    /* Code decompiled incorrectly, please refer to instructions dump. */
//    @Nullable
//    @CheckResult
//    private String writeDataToZipFile() {
//        IOException e;
//        Throwable th;
//        String path = null;
//        synchronized (this.dataLock) {
//            this.data.endTimestamp = System.currentTimeMillis();
//            JsonWriter writer = null;
//            try {
//                ensureFolderExists();
//                SimpleDateFormat photoDateFormat = new SimpleDateFormat("yyyy-MM-dd'-'HH-mm-ss", Locale.US);
//                Date currentTime = new Date();
//                path = rootFolder.getPath() + File.separator + ((getHudUuid() + "-" + photoDateFormat.format(currentTime)) + FILE_EXTENSION);
//                logger.i("Writing sensor data to file: " + path);
//                JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(path)), "UTF-8"));
//                try {
//                    jsonWriter.beginObject();
//                    jsonWriter.name("version").value(1);
//                    jsonWriter.name("app_version").value(this.data.appVersion);
//                    jsonWriter.name("device").value(this.data.device);
//                    jsonWriter.name("os_version").value(this.data.osVersion);
//                    jsonWriter.name(TransitDeparture.DEPARTURE_PLATFORM_KEY_NAME).value(this.data.platform);
//                    jsonWriter.name("startTimestamp").value(getTimestampString(this.data.startTimestamp));
//                    jsonWriter.name("endTimestamp").value(getTimestampString(this.data.endTimestamp));
//                    jsonWriter.name("time_zone_offset").value((long) this.data.timeZoneOffset);
//                    jsonWriter.name("trip_uuid").value(this.data.tripUuid);
//                    SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
//                    String yearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
//                    String makeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
//                    String modelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
//                    jsonWriter.name(ProfilePreferences.CAR_YEAR).value(yearString);
//                    jsonWriter.name(ProfilePreferences.CAR_MAKE).value(makeString);
//                    jsonWriter.name(ProfilePreferences.CAR_MODEL).value(modelString);
//                    jsonWriter.name("is_split_data").value(this.data.isSplitData);
//                    writeActivityData(jsonWriter);
//                    writeLocationData(jsonWriter);
//                    jsonWriter.name("android_sensors");
//                    jsonWriter.beginObject();
//                    writeThisSensorData(jsonWriter, "gravities", this.data.gravity);
//                    writeThisSensorData(jsonWriter, "gyroscopes", this.data.gyroscope);
//                    writeThisSensorData(jsonWriter, "linear_accelerations", this.data.linearAcceleration);
//                    writeThisSensorData(jsonWriter, "magnetic_fields", this.data.magneticField);
//                    writeThisSensorData(jsonWriter, "orientations", this.data.orientation);
//                    writeThisSensorData(jsonWriter, "rotation_vectors", this.data.rotationVector);
//                    jsonWriter.endObject();
//                    jsonWriter.endObject();
//                    this.data.isPersistedToFile = true;
//                    if (jsonWriter != null) {
//                        try {
//                            jsonWriter.close();
//                        } catch (IOException e2) {
//                            logger.e("Unable to close the file writer for sensor data", e2);
//                        }
//                    }
//                    writer = jsonWriter;
//                    return path;
//                } catch (IOException e3) {
//                    writer = jsonWriter;
//                    try {
//                        logger.e("Unable to write sensor data to file. Erasing it.", e3);
//                        IOUtils.deleteFile(NavdyApplication.getAppContext(), path);
//                        if (writer != null) {
//                        }
//                        return null;
//                    } catch (Throwable th2) {
//                        th = th2;
//                        if (writer != null) {
//                            try {
//                                writer.close();
//                            } catch (IOException e22) {
//                                logger.e("Unable to close the file writer for sensor data", e22);
//                            }
//                        }
//                        logger.e("Exception in writeDataToZipFile", th);
//                    }
//                } catch (Throwable th3) {
//                    th = th3;
//                    writer = jsonWriter;
//                    if (writer != null) {
//                    }
//                    logger.e("Exception in writeDataToZipFile", th);
//                }
//            } catch (IOException e4) {
//                logger.e("Unable to write sensor data to file. Erasing it.", e4);
//                IOUtils.deleteFile(NavdyApplication.getAppContext(), path);
//                if (writer != null) {
//                    try {
//                        writer.close();
//                    } catch (IOException e222) {
//                        logger.e("Unable to close the file writer for sensor data", e222);
//                    }
//                }
//            }
//        }
//        return null;
//    }

//    private void writeActivityData(JsonWriter writer) throws IOException {
//        if (this.data.activities != null && this.data.activities.size() > 0) {
//            writer.name("android_drivings");
//            writer.beginArray();
//            Iterator it = this.data.activities.iterator();
//            while (it.hasNext()) {
//                ((Activity) it.next()).writeToJson(writer);
//            }
//            writer.endArray();
//        }
//    }
//
//    private void writeLocationData(JsonWriter writer) throws IOException {
//        if (this.data.locations != null && this.data.locations.size() > 0) {
//            writer.name("locations");
//            writer.beginArray();
//            Iterator it = this.data.locations.iterator();
//            while (it.hasNext()) {
//                ((Location) it.next()).writeToJson(writer);
//            }
//            writer.endArray();
//        }
//    }
//
//    private void writeThisSensorData(@NonNull JsonWriter writer, @NonNull String sensorName, @Nullable LinkedList<SensorData> sensorData) throws IOException {
//        if (sensorData != null && sensorData.size() > 0) {
//            writer.name(sensorName);
//            writer.beginArray();
//            Iterator it = sensorData.iterator();
//            while (it.hasNext()) {
//                ((SensorData) it.next()).writeToJson(writer);
//            }
//            writer.endArray();
//        }
//    }

    public static void sendDataToS3IfOnWiFi() {
//        if (SystemUtils.isConnectedToWifi(NavdyApplication.getAppContext())) {
//            sendToS3IfNotAlreadySending();
//        } else if (VERSION.SDK_INT >= 24) {
//            UnmeteredNetworkConnectivityJobService.scheduleNetworkServiceForNougatOrAbove();
//        }
    }

    public static void sendToS3IfNotAlreadySending() {
//        if (sendInProgress.getAndSet(true)) {
//            logger.d("Sending is already in progress");
//        } else {
//            sendToS3();
//        }
    }

//    private static void sendToS3() {
//        int i = 0;
//        logger.i("Sending sensor data to S3");
//        if (rootFolder == null || !rootFolder.exists()) {
//            logger.e("sendToS3: Unable to find data rootFolder: " + rootFolder);
//            sendInProgress.set(false);
//            return;
//        }
//        File[] subFolders = rootFolder.listFiles();
//        if (subFolders == null || subFolders.length <= 0) {
//            finishSendingToS3(rootFolder);
//            return;
//        }
//        boolean calledSend = false;
//        int length = subFolders.length;
//        while (i < length) {
//            File subFolder = subFolders[i];
//            if (subFolder.isDirectory()) {
//                sendToS3(subFolder, subFolder.getName());
//                calledSend = true;
//                break;
//            }
//            i++;
//        }
//        if (!calledSend) {
//            finishSendingToS3(rootFolder);
//        }
//    }
//
//    private static void finishSendingToS3(File folder) {
//        logger.v("sendToS3: Empty data folder: " + folder);
//        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.PENDING_SENSOR_DATA_EXIST, false).apply();
//        sendInProgress.set(false);
//    }
//
//    private static void sendToS3(File folder, String appVersionCode) {
//        final File[] files = folder.listFiles();
//        if (files == null || files.length <= 0) {
//            EraseThisFileAndSendNextFile(folder);
//            return;
//        }
//        final File file = files[0];
//        if (file == null) {
//            return;
//        }
//        if (file.length() <= 0 || !file.exists()) {
//            EraseThisFileAndSendNextFile(file);
//            return;
//        }
//        String key = "insurance-logs/android/" + appVersionCode + S3Constants.S3_FILE_DELIMITER + new SimpleDateFormat("yyyy-MM", Locale.US).format(new Date()) + S3Constants.S3_FILE_DELIMITER + file.getName();
//        logger.d("Length of the file being uploaded: " + file.length() + " bytes");
//        transferObserver = getInstance().transferUtility.upload(S3_BUCKET_NAME, key, file);
//        transferObserver.setTransferListener(new TransferListener() {
//            public void onStateChanged(int id, TransferState state) {
//                DataCollectionService.logger.d("Uploading File " + file.getName() + ", onStatChanged : " + id + DDL.SEPARATOR + state.name());
//                if (state == TransferState.FAILED) {
//                    DataCollectionService.access$1808();
//                    DataCollectionService.logger.d("Uploading File " + file.getName() + " failed. This is attempt number " + DataCollectionService.failCount + S3Constants.S3_FILE_DELIMITER + 3);
//                    if (DataCollectionService.failCount < 3) {
//                        DataCollectionService.sendToS3();
//                        return;
//                    }
//                    DataCollectionService.failCount = 0;
//                    DataCollectionService.EraseThisFileAndSendNextFile(file);
//                } else if (state == TransferState.COMPLETED) {
//                    DataCollectionService.failCount = 0;
//                    DataCollectionService.EraseThisFileAndSendNextFile(file);
//                }
//            }
//
//            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                DataCollectionService.logger.d("Uploading File " + files[0].getName() + ", onProgressChanged : " + id + ", Size :" + bytesCurrent + ", of " + bytesTotal);
//            }
//
//            public void onError(int id, Exception ex) {
//                DataCollectionService.logger.e("Uploading File " + file.getName() + ", onError : " + id, ex);
//            }
//        });
//    }
//
//    private static String getAppVersionCode() {
//        PackageInfo pInfo = null;
//        Context context = NavdyApplication.getAppContext();
//        try {
//            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
//        } catch (NameNotFoundException e) {
//            e.printStackTrace();
//        }
//        if (pInfo != null && pInfo.versionCode > 0) {
//            return String.valueOf(pInfo.versionCode);
//        }
//        if (pInfo == null || pInfo.versionName == null) {
//            return context.getString(R.string.unknown);
//        }
//        return pInfo.versionName;
//    }

    public void cancelAnyOngoingUpload() {
//        if (this.transferUtility != null && transferObserver != null) {
//            this.transferUtility.cancel(transferObserver.getId());
//        }
    }
//
//    private static void EraseThisFileAndSendNextFile(@Nullable File file) {
//        if (file == null || file.delete()) {
//            sendToS3();
//        } else {
//            logger.e("Error while trying to delete file: " + file);
//        }
//    }
}
