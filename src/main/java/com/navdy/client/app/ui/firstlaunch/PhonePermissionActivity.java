package com.navdy.client.app.ui.firstlaunch;

import android.annotation.SuppressLint;
import android.net.Uri;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class PhonePermissionActivity extends com.navdy.client.app.ui.base.BaseToolbarActivity {
    @DexIgnore
    com.navdy.client.app.ui.firstlaunch.AppSetupScreen appSetupScreen; // = com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.getScreen(8);
    @DexIgnore
    private com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment bottomCard;
    @DexIgnore
    private android.widget.ImageView hud;
    @DexIgnore
    private boolean showingFail; // = false;

    // @DexIgnore
    // class Anon1 implements java.lang.Runnable {
    //     Anon1() {
    //     }
    //
    //     public void run() {
    //         com.navdy.client.app.ui.firstlaunch.PhonePermissionActivity.this.goToHomeScreen();
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 implements java.lang.Runnable {
    //     Anon2() {
    //     }
    //
    //     public void run() {
    //         com.navdy.client.app.ui.firstlaunch.PhonePermissionActivity.this.showFailureForCurrentScreen();
    //     }
    // }

    @DexIgnore
    protected void onCreate(@android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) com.navdy.client.R.layout.fle_app_setup_permission);
        new com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder().title((int) com.navdy.client.R.string.app_setup).build();
        this.hud = (android.widget.ImageView) findViewById(com.navdy.client.R.id.hud);
        if (this.hud != null) {
            this.hud.setImageResource(this.appSetupScreen.hudRes);
        }
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        if (fm != null) {
            android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
            if (ft != null) {
                this.bottomCard = new com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment();
                android.os.Bundle bundle = new android.os.Bundle();
                bundle.putParcelable("screen", this.appSetupScreen);
                this.bottomCard.setArguments(bundle);
                ft.replace(com.navdy.client.R.id.bottom_card, this.bottomCard);
                ft.commit();
            }
        }
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    private void goToHomeScreen() {
        android.content.Intent i = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.homescreen.HomescreenActivity.class);
        i.setFlags(268468224);
        startActivity(i);
        finish();
    }

    @DexIgnore
    private void showFailureForCurrentScreen() {
        this.logger.d("showFailureForCurrentScreen");
        if (this.bottomCard != null) {
            this.showingFail = true;
            this.bottomCard.showFail();
        }
        if (this.hud != null) {
            this.hud.setImageResource(this.appSetupScreen.hudResFail);
        }
    }

    @DexIgnore
    public void onPrivacyPolicyClick(android.view.View view) {
        android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
        browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.PRIVACY);
        startActivity(browserIntent);
    }

    @DexIgnore
    public void onContactSupportClick(android.view.View view) {
        startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.ContactUsActivity.class));
    }

    @DexIgnore
    public void onDescriptionClick(android.view.View view) {
        if (this.showingFail) {
            com.navdy.client.app.framework.util.SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
        }
    }

    @DexReplace
    public void onHelpCenterClick(android.view.View view) {
        // openBrowserFor(com.navdy.client.app.framework.util.ZendeskJWT.getZendeskUri());
        openBrowserFor(Uri.parse("http://navdy.alelec.net"));
    }

    @DexIgnore
    public void onButtonClick(android.view.View v) {
        // if (this.appSetupScreen != null && !com.navdy.client.app.ui.base.BaseActivity.isEnding(this)) {
        //     if (this.appSetupScreen.isMandatory || !this.showingFail) {
        //         requestPhonePermission(new com.navdy.client.app.ui.firstlaunch.PhonePermissionActivity.Anon1(), new com.navdy.client.app.ui.firstlaunch.PhonePermissionActivity.Anon2());
        //     } else {
        //         goToHomeScreen();
        //     }
        // }
    }
}
