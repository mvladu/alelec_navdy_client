package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.navdy.client.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.ContactImageHelper;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;

import java.util.logging.MemoryHandler;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class DestinationImageView extends ImageView {
    @DexIgnore
    private static /* final */ String EMPTY; // = "";
    @DexIgnore
    private static /* final */ String SPACE; // = " ";
    @DexIgnore
    private static int greyColor;
    @DexIgnore
    private static int largeStyleTextSize;
    @DexIgnore
    private static int smallStyleTextSize;
    @DexIgnore
    private static Typeface typefaceLarge;
    @DexIgnore
    private static Typeface typefaceSmall;
    @DexIgnore
    private static boolean valuesSet;
    @DexIgnore
    private static int whiteColor;
    @DexIgnore
    private ContactImageHelper contactImageHelper;
    @DexIgnore
    private String initials;
    @DexIgnore
    private Paint paint;
    @DexIgnore
    private DestinationImageView.Style style;

    @DexIgnore
    public enum Style {
        INITIALS,
        DEFAULT
    }

    @DexIgnore
    public DestinationImageView(Context context) {
        this(context, null, 0);
    }

    @DexIgnore
    public DestinationImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @DexIgnore
    public DestinationImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.style = DestinationImageView.Style.DEFAULT;
        init();
    }

    @DexIgnore
    private void init() {
        this.contactImageHelper = ContactImageHelper.getInstance();
        if (!valuesSet) {
            valuesSet = true;
            Resources resources = getResources();
            greyColor = ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.material_grey_800);
            whiteColor = ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.white);
            smallStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_small_text);
            largeStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_large_text);
            typefaceSmall = Typeface.create("sans-serif-medium", 0);
            typefaceLarge = Typeface.create("sans-serif", 1);
        }
        this.paint = new Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    @DexIgnore
    public void setInitials(String initials2, DestinationImageView.Style style2) {
        this.initials = initials2;
        this.style = style2;
        invalidate();
    }

    @DexReplace
    public void setImage(int resourceId, String initials2, DestinationImageView.Style style2) {
        try {
            setImageResource(resourceId);
        } catch (OutOfMemoryError ex) {
            Logger logger = new Logger(DestinationImageView.class);
            logger.e(ex);
        }
        setInitials(initials2, style2);
    }

    @DexIgnore
    public void setImage(Bitmap bitmap) {
        clearInitials();
        setImageBitmap(createCircleBitmap(bitmap));
    }

    @DexIgnore
    public void setImage(String name) {
        setImage(this.contactImageHelper.getResourceId(this.contactImageHelper.getContactImageIndex(name)), name);
    }

    @DexIgnore
    public void setImage(int resourceId, String name) {
        setImage(resourceId, getInitials(name), DestinationImageView.Style.INITIALS);
    }

    @DexIgnore
    public static synchronized String getInitials(String name) {
        String str;
        synchronized (DestinationImageView.class) {
            StringBuilder builder = new StringBuilder();
            if (StringUtils.isEmptyAfterTrim(name)) {
                str = "";
            } else {
                String name2 = name.trim();
                int index = name2.indexOf(SPACE);
                if (index > 0) {
                    String first = name2.substring(0, index).trim();
                    String last = name2.substring(index + 1).trim();
                    builder.setLength(0);
                    if (!StringUtils.isEmptyAfterTrim(first)) {
                        builder.append(first.charAt(0));
                    }
                    if (!StringUtils.isEmptyAfterTrim(last)) {
                        builder.append(last.charAt(0));
                    }
                    str = builder.toString().toUpperCase();
                } else if (name2.length() > 0) {
                    str = String.valueOf(name2.charAt(0)).toUpperCase();
                } else {
                    str = "";
                }
            }
        }
        return str;
    }

    @DexIgnore
    public void clearInitials() {
        this.initials = "";
        this.style = DestinationImageView.Style.DEFAULT;
    }

    @DexIgnore
    public static Bitmap createCircleBitmap(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Context context = NavdyApplication.getAppContext();
        int width = context.getResources().getDimensionPixelSize(R.dimen.contact_image_width);
        int height = context.getResources().getDimensionPixelSize(R.dimen.contact_image_height);
        Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, width, height, false);
        Bitmap newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Path path = new Path();
        path.addCircle(((float) width) / 2.0f, ((float) height) / 2.0f, Math.min((float) width, ((float) height) / 2.0f), Path.Direction.CCW);
        Canvas canvas = new Canvas(newBitmap);
        canvas.clipPath(path);
        canvas.drawBitmap(bitmap2, 0.0f, 0.0f, null);
        return newBitmap;
    }

    @DexIgnore
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.style != DestinationImageView.Style.DEFAULT) {
            int width = getWidth();
            int height = getHeight();
            if (this.initials != null) {
                this.paint.setColor(whiteColor);
                this.paint.setTextSize((float) largeStyleTextSize);
                this.paint.setTypeface(typefaceLarge);
                this.paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(this.initials, ((float) width) / 2.0f, (((float) height) / 2.0f) - ((this.paint.descent() + this.paint.ascent()) / 2.0f), this.paint);
            }
        }
    }
}
