package com.navdy.client.app.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;

import com.navdy.client.R;
import com.navdy.client.app.framework.i18n.I18nManager;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.task.TaskManager;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexPrepend;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class GeneralSettingsActivity extends BaseEditActivity {
    @DexIgnore
    private /* final */ BusProvider bus; // = com.navdy.client.app.framework.util.BusProvider.getInstance();
    @DexIgnore
    private Switch gestureSwitch;
    @DexIgnore
    private RadioButton googleNow;
    @DexIgnore
    private /* final */ I18nManager i18nManager; // = com.navdy.client.app.framework.i18n.I18nManager.getInstance();
    @DexIgnore
    private Switch limitCellDataSwitch;
    @DexIgnore
    private /* final */ CompoundButton.OnCheckedChangeListener onCheckedChangeListener; // = new com.navdy.client.app.ui.settings.GeneralSettingsActivity.Anon2();
    @DexIgnore
    private RadioButton placeSearch;
    @DexIgnore
    private /* final */ SharedPreferences sharedPrefs; // = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
    @DexIgnore
    private Switch smartSuggestionsSwitch;
    @DexAdd
    private Switch connectionNotificationSwitch;
    @DexIgnore
    private /* final */ View.OnClickListener unitSystemClickListener; // = new com.navdy.client.app.ui.settings.GeneralSettingsActivity.Anon1();
    @DexIgnore
    private RadioButton unitSystemImperial;
    @DexIgnore
    private RadioButton unitSystemMetric;

    @DexIgnore
    class Anon1 implements View.OnClickListener {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void onClick(View v) {
            GeneralSettingsActivity.this.somethingChanged = true;
        }
    }

    @DexIgnore
    class Anon2 implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            GeneralSettingsActivity.this.somethingChanged = true;
        }
    }

    @DexIgnore
    class Anon3 implements Runnable {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void run() {
            SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
        }
    }

    @DexIgnore
    class Anon4 implements View.OnClickListener {
        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        public void onClick(View v) {
            GeneralSettingsActivity.this.somethingChanged = true;
            Tracker.tagEvent(TrackerConstants.Event.SWITCHED_TO_DEFAULT);
        }
    }

    @DexIgnore
    class Anon5 implements View.OnClickListener {
        @DexIgnore
        Anon5() {
        }

        @DexIgnore
        public void onClick(View v) {
            GeneralSettingsActivity.this.somethingChanged = true;
            Tracker.tagEvent(TrackerConstants.Event.SWITCHED_TO_PLACE_SEARCH);
        }
    }

    @DexIgnore
    public static class LimitCellDataEvent {
        @DexIgnore
        public final boolean hasLimitedCellData;

        @DexIgnore
        LimitCellDataEvent(boolean hasLimitedCellData2) {
            this.hasLimitedCellData = hasLimitedCellData2;
        }
    }

    @DexIgnore
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_general);
        new BaseToolbarActivity.ToolbarBuilder().title((int) R.string.menu_general).build();
    }

    @DexReplace
    protected void onResume() {
        super.onResume();
        initUnitSystemSettings();
        initDialLongPress();
        initGesturesSettings();
        initLimitCellularDataSettings();
        initSmartSuggestionsSettings();
        initConnectionNotificationSettings();
    }

    @DexAdd
    public void onConnectionNotificationClick(View view) {
        this.connectionNotificationSwitch.toggle();
    }

    @DexIgnore
    public void onSmartSuggestionsClick(View view) {
        this.smartSuggestionsSwitch.toggle();
    }

    @DexIgnore
    public void onLimitCellularDataClick(View view) {
        this.limitCellDataSwitch.toggle();
    }

    @DexIgnore
    public void onAllowGesturesClick(View view) {
        this.gestureSwitch.toggle();
    }

    @DexPrepend
    protected void saveChanges() {
        this.sharedPrefs.edit().putBoolean(SettingsConstants.CONNECTION_NOTIFICATION_ENABLED, this.connectionNotificationSwitch.isChecked()).apply();
    }

    // @DexIgnore
    // protected void saveChanges() {
    //     int value;
    //     if (this.unitSystemMetric.isChecked()) {
    //         this.i18nManager.setUnitSystem(DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC);
    //     } else if (this.unitSystemImperial.isChecked()) {
    //         this.i18nManager.setUnitSystem(DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL);
    //     } else {
    //         this.logger.e("invalid state for radio group");
    //     }
    //     GlanceUtils.saveGlancesConfigurationChanges(SettingsConstants.HUD_GESTURE, this.gestureSwitch.isChecked());
    //     SettingsUtils.sendHudSettings(this.gestureSwitch.isChecked());
    //     if (SettingsUtils.isLimitingCellularData() != this.limitCellDataSwitch.isChecked()) {
    //         SettingsUtils.setLimitCellularData(this.limitCellDataSwitch.isChecked());
    //         this.bus.post(new GeneralSettingsActivity.LimitCellDataEvent(this.limitCellDataSwitch.isChecked()));
    //     }
    //     SharedPreferences.Editor putBoolean = this.sharedPrefs.edit().putBoolean(SettingsConstants.RECOMMENDATIONS_ENABLED, this.smartSuggestionsSwitch.isChecked());
    //     String str = SettingsConstants.DIAL_LONG_PRESS_ACTION;
    //     if (this.placeSearch.isChecked()) {
    //         value = DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue();
    //     } else {
    //         value = DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT.getValue();
    //     }
    //     putBoolean.putInt(str, value).apply();
    //     TaskManager.getInstance().execute(new GeneralSettingsActivity.Anon3(), 1);
    // }

    @DexIgnore
    private void initUnitSystemSettings() {
        this.unitSystemMetric = (RadioButton) findViewById(R.id.unit_system_metric);
        this.unitSystemImperial = (RadioButton) findViewById(R.id.unit_system_imperial);
        DriverProfilePreferences.UnitSystem unitSystem = this.i18nManager.getUnitSystem();
        if (unitSystem == DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC) {
            this.unitSystemMetric.setChecked(true);
        } else if (unitSystem == DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL) {
            this.unitSystemImperial.setChecked(true);
        } else {
            this.logger.e("invalid unit system");
        }
        this.unitSystemMetric.setOnClickListener(this.unitSystemClickListener);
        this.unitSystemImperial.setOnClickListener(this.unitSystemClickListener);
    }

    @DexIgnore
    private void initDialLongPress() {
        this.googleNow = (RadioButton) findViewById(R.id.dial_long_press_google_now);
        this.placeSearch = (RadioButton) findViewById(R.id.dial_long_press_place_search);
        SharedPreferences settingsPrefs = SettingsUtils.getSharedPreferences();
        if (!settingsPrefs.getBoolean(SettingsConstants.HUD_DIAL_LONG_PRESS_CAPABLE, false)) {
            hideIfFound(R.id.dial_long_press);
            hideIfFound(R.id.dial_long_press_group);
            hideIfFound(R.id.dial_long_press_desc);
            return;
        }
        if (settingsPrefs.getInt(SettingsConstants.DIAL_LONG_PRESS_ACTION, SettingsConstants.DIAL_LONG_PRESS_ACTION_DEFAULT) != DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue()) {
            this.googleNow.setChecked(true);
        } else {
            this.placeSearch.setChecked(true);
        }
        this.googleNow.setOnClickListener(new GeneralSettingsActivity.Anon4());
        this.placeSearch.setOnClickListener(new GeneralSettingsActivity.Anon5());
    }

    @DexIgnore
    private void hideIfFound(@IdRes int resId) {
        View v = findViewById(resId);
        if (v != null) {
            v.setVisibility(View.GONE);
        }
    }

    @DexIgnore
    private void initGesturesSettings() {
        boolean gesturesAreEnabled = this.sharedPrefs.getBoolean(SettingsConstants.HUD_GESTURE, true);
        this.gestureSwitch = (Switch) findViewById(R.id.allow_gestures_switch);
        if (this.gestureSwitch != null) {
            this.gestureSwitch.setChecked(gesturesAreEnabled);
            this.gestureSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    @DexIgnore
    private void initLimitCellularDataSettings() {
        boolean limitCellularDataEnabled = SettingsUtils.isLimitingCellularData();
        this.limitCellDataSwitch = (Switch) findViewById(R.id.limit_cell_data_switch);
        if (this.limitCellDataSwitch != null) {
            this.limitCellDataSwitch.setChecked(limitCellularDataEnabled);
            this.limitCellDataSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    @DexIgnore
    private void initSmartSuggestionsSettings() {
        boolean smartSuggestionsEnabled = this.sharedPrefs.getBoolean(SettingsConstants.RECOMMENDATIONS_ENABLED, true);
        this.smartSuggestionsSwitch = (Switch) findViewById(R.id.smart_suggestions_switch);
        if (this.smartSuggestionsSwitch != null) {
            this.smartSuggestionsSwitch.setChecked(smartSuggestionsEnabled);
            this.smartSuggestionsSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    @DexAdd
    private void initConnectionNotificationSettings() {
        boolean connectionNotificationEnabled = this.sharedPrefs.getBoolean(SettingsConstants.CONNECTION_NOTIFICATION_ENABLED, true);
        this.connectionNotificationSwitch = findViewById(com.alelec.navdyclient.R.id.connection_notification_switch);
        if (this.connectionNotificationSwitch != null) {
            this.connectionNotificationSwitch.setChecked(connectionNotificationEnabled);
            this.connectionNotificationSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }


    @DexIgnore
    public void onLearnMoreClick(View view) {
        startActivity(new Intent(getApplicationContext(), GestureDialogActivity.class));
    }
}
