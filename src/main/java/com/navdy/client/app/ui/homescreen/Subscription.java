package com.navdy.client.app.ui.homescreen;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.alelec.navdyclient.R;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.crashlytics.android.Crashlytics;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Subscription extends BaseActivity implements PurchasesUpdatedListener {
    private static BillingClient billingClient = null;
    private static SkuDetails skuDetail = null;
    public static boolean subscribed = false;
    public static boolean manual_open = false;

    protected synchronized void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (billingClient == null) {
            billingClient = BillingClient.newBuilder(this).setListener(this).build();
        }
        if (asked() && !manual_open) {
            finish();
        } else {
            manual_open = false;
            billingClient.startConnection(new BillingClientStateListener() {
                @Override
                public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                    if (billingResponseCode == BillingClient.BillingResponse.OK) {
                        // The BillingClient is ready. You can query purchases here.
                        List<String> skuList = new ArrayList<>();
                        skuList.add("navigation");
                        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
                        billingClient.querySkuDetailsAsync(params.build(),
                                (responseCode, skuDetailsList) -> {
                                    // Process the result.
                                    for (SkuDetails skuDetail : skuDetailsList) {
                                        logger.v(skuDetail.toString());
                                        Subscription.skuDetail = skuDetail;

                                        // Show subscription request
                                        setContentView(R.layout.dialog_subscription);
                                        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.USER_SUBSCRIPTION_ASKED, true).apply();
                                        break;
                                    }
                                });
                    }
                }
                @Override
                public void onBillingServiceDisconnected() {
                    // Try to restart the connection on the next request to
                    // Google Play by calling the startConnection() method.
                }
            });
        }
    }

    public void finish() {
        manual_open = false;
        super.finish();
    }

    public static boolean asked() {
        return SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.USER_SUBSCRIPTION_ASKED, false);
    }

    public void onCloseClick(View view) {
        logResult("CLOSED");
        onBackPressed();
    }

    public void onPaypalPressed(View view) {
        // https://stackoverflow.com/a/40309792/3093392
        // https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables/#recurring-payment-variables
        String url = "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick-subscriptions&business=andrew@alelec.net&lc=US&item_name=Donation+for+Navdy+Navigation+Services&no_note=0&cn=&currency_code=USD&no_shipping=1&a3=1.5&t3=M&p3=1&src=1&return=https%3A%2F%2Fgitlab.com%2Falelec%2Fnavdy%2Fdisplay-rom%2Fwikis%2Fhome";
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
        logResult("PAYPAL");
        finish();
    }

    public void onOkPressed(View view) {
        if ((billingClient != null) && (skuDetail != null)) {
            logResult("OK");
            // Retrieve a value for "skuDetails" by calling querySkuDetailsAsync().
            BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                    .setSkuDetails(skuDetail)
                    .build();
            billingClient.launchBillingFlow(this, flowParams);
        } else {
            logResult("NOT_READY");
        }
        finish();
    }

    @Override
    public void onPurchasesUpdated(@BillingClient.BillingResponse int responseCode, List<Purchase> purchases) {
        if (responseCode == BillingClient.BillingResponse.OK
                && purchases != null) {
            for (Purchase purchase : purchases) {
                if (purchase.isAutoRenewing()) {
                    subscribed = true;
                }
            }
        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED) {
            logResult("CANCELLED");
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
            logResult("ERROR");
        }
    }

    public class SubscriptionException extends Exception {
        SubscriptionException(String errorMessage) {
            super(errorMessage);
        }
    }

    private void logResult(String result) {
        if (!manual_open) {
            SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.USER_SUBSCRIPTION_RESPONSE, result).apply();
        }
        // https://stackoverflow.com/questions/12343014/how-to-save-and-retrieve-date-in-sharedpreferences
        Date date = new Date();
        SettingsUtils.getSharedPreferences().edit().putLong(SettingsConstants.USER_SUBSCRIPTION_ASKED_DATE, date.getTime()).apply();
        try {
            throw new SubscriptionException(result);
        } catch (SubscriptionException ex) {
            Crashlytics.logException(ex);
        }
    }
}
