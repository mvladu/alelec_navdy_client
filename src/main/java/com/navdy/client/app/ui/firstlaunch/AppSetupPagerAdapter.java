package com.navdy.client.app.ui.firstlaunch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import com.navdy.client.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType;
import com.navdy.service.library.log.Logger;
import java.lang.ref.WeakReference;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class AppSetupPagerAdapter extends FragmentStatePagerAdapter {
//    static final int BT_SCREEN_INDEX = 1;
//    private static final int CACHE_SIZE = 20971520;
//    private static final int FADE_DURATION = 200;
//    private static final int IMAGE_MAX_SIZE = 4194304;
//    public static final int PHONE_SCREEN_INDEX = 8;
//    private static final int PROFILE_SCREEN_INDEX = 0;
    @DexIgnore
    private static AppSetupScreen[] screens;

//    private SparseArray<Fragment> cache = new SparseArray(screens.length);
//    private ImageCache imageCache = new ImageCache(CACHE_SIZE, 4194304);
//    private final Logger logger = new Logger(AppSetupPagerAdapter.class);
    @DexIgnore
    private boolean pretendBtConnected = false;
    @DexIgnore
    private int screenCount;

    @DexIgnore
    AppSetupPagerAdapter(FragmentManager fm, ImageCache imageCache) {
        super(fm);
//        recalculateScreenCount();
//        this.imageCache = imageCache;
    }
//
//    public Fragment getItem(int position) {
//        Fragment screen = (Fragment) this.cache.get(position);
//        if (screen != null) {
//            return screen;
//        }
//        Fragment fragment;
//        if (position == 0) {
//            fragment = new AppSetupProfileFragment();
//            this.cache.put(position, fragment);
//            return fragment;
//        }
//        fragment = new AppSetupBottomCardGenericFragment();
//        Bundle bundle = new Bundle();
//        bundle.putParcelable("screen", screens[position]);
//        fragment.setArguments(bundle);
//        this.cache.put(position, fragment);
//        return fragment;
//    }
//
//    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
//        this.cache.remove(position);
//    }
//
//    public void destroyItem(View container, int position, Object object) {
//        super.destroyItem(container, position, object);
//        this.cache.remove(position);
//    }
//
//    void clearCache() {
//        this.cache.clear();
//        this.imageCache.clearCache();
//    }
//
//    public int getCount() {
//        return this.screenCount;
//    }

    @DexReplace
    public void recalculateScreenCount() {
        int count = 1;
        for (AppSetupScreen screen : screens) {
            // if (screen.screenType != ScreenType.BLUETOOTH) {
                if (screen.screenType == ScreenType.END || !weHavePermissionForThisScreen(screen)) {
                    break;
                }
//            } else {
//                boolean deviceIsConnected;
//                AppInstance appInstance = AppInstance.getInstance();
//                if (this.pretendBtConnected || appInstance.isDeviceConnected()) {
//                    deviceIsConnected = true;
//                } else {
//                    deviceIsConnected = false;
//                }
//                if (!deviceIsConnected) {
//                    break;
//                }
//             }
            count++;
        }
        this.screenCount = count;
        notifyDataSetChanged();
    }

    @DexIgnore
    public Fragment getItem(int i) {
        return null;
    }

    @DexIgnore
    public int getCount() {
        return 0;
    }
//
//    void updateIllustration(WeakReference<ImageView> hudRef, int position, boolean showingFail) {
//        if (hudRef == null) {
//            this.logger.e("Trying to call updateIllustration with null layout elements");
//            return;
//        }
//        final ImageView hud = (ImageView) hudRef.get();
//        if (hud != null) {
//            AppSetupScreen screen = screens[position];
//            if (screen != null) {
//                final int hudRes;
//                if (showingFail) {
//                    hudRes = screen.hudResFail;
//                } else {
//                    hudRes = screen.hudRes;
//                }
//                Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
//                fadeOut.setInterpolator(new AccelerateInterpolator());
//                fadeOut.setDuration(200);
//                final Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
//                fadeIn.setInterpolator(new DecelerateInterpolator());
//                fadeIn.setDuration(200);
//                fadeOut.setAnimationListener(new AnimationListener() {
//                    public void onAnimationStart(Animation animation) {
//                    }
//
//                    public void onAnimationEnd(Animation animation) {
//                        ImageUtils.loadImage(hud, hudRes, AppSetupPagerAdapter.this.imageCache);
//                        hud.startAnimation(fadeIn);
//                    }
//
//                    public void onAnimationRepeat(Animation animation) {
//                    }
//                });
//                hud.clearAnimation();
//                hud.setAlpha(1.0f);
//                hud.startAnimation(fadeOut);
//            }
//        }
//    }

    @DexIgnore
    public static int getScreensCount() {
       return screens.length;
   }

    @DexIgnore
    public static AppSetupScreen getScreen(int position) {
       if (position < 0 || position >= screens.length) {
           return null;
       }
       return screens[position];
   }

    @DexIgnore
    public static boolean weHavePermissionForThisScreen(AppSetupScreen screen) {
        throw null;
    }
//        if (screen.screenType == ScreenType.PROFILE) {
//            return Tracker.isUserRegistered();
//        }
//        if (screen.screenType == ScreenType.NOTIFICATIONS) {
//            return BaseActivity.weHaveNotificationPermission();
//        }
//        if (screen.screenType == ScreenType.ACCESS_FINE_LOCATION) {
//            if (BaseActivity.weHaveLocationPermission() || (!screen.isMandatory && BaseActivity.alreadyAskedForLocationPermission())) {
//                return true;
//            }
//            return false;
//        } else if (screen.screenType == ScreenType.USE_MICROPHONE) {
//            if (BaseActivity.weHaveMicrophonePermission() || (!screen.isMandatory && BaseActivity.alreadyAskedForMicrophonePermission())) {
//                return true;
//            }
//            return false;
//        } else if (screen.screenType == ScreenType.READ_CONTACTS) {
//            if (BaseActivity.weHaveContactsPermission() || (!screen.isMandatory && BaseActivity.alreadyAskedForContactsPermission())) {
//                return true;
//            }
//            return false;
//        } else if (screen.screenType == ScreenType.RECEIVE_SMS) {
//            if (BaseActivity.weHaveSmsPermission() || (!screen.isMandatory && BaseActivity.alreadyAskedForSmsPermission())) {
//                return true;
//            }
//            return false;
//        } else if (screen.screenType == ScreenType.CALL_PHONE) {
//            if (BaseActivity.weHavePhonePermission() || (!screen.isMandatory && BaseActivity.alreadyAskedForPhonePermission())) {
//                return true;
//            }
//            return false;
//        } else if (screen.screenType == ScreenType.READ_CALENDAR) {
//            if (BaseActivity.weHaveCalendarPermission() || (!screen.isMandatory && BaseActivity.alreadyAskedForCalendarPermission())) {
//                return true;
//            }
//            return false;
//        } else if (screen.screenType != ScreenType.WRITE_EXTERNAL_STORAGE) {
//            return true;
//        } else {
//            if (BaseActivity.weHaveStoragePermission() || (!screen.isMandatory && BaseActivity.alreadyAskedForStoragePermission())) {
//                return true;
//            }
//            return false;
//        }
//    }
//
//    void pretendBtConnected() {
//        this.pretendBtConnected = true;
//    }
}
