package com.navdy.client.app.ui.glances;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;

import com.navdy.client.R.color;
import com.navdy.client.R.id;
import com.navdy.client.R.layout;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

import java.util.List;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
class GlancesAppAdapter extends ArrayAdapter<ApplicationInfo> {
    @DexIgnore
    private /* final */ List<ApplicationInfo> appsList;
    @DexIgnore
    private boolean glancesAreEnabled; // = false;
    @DexIgnore
    private /* final */ LayoutInflater layoutInflater;
    @DexIgnore
    private /* final */ PackageManager packageManager;
    @DexIgnore
    private SharedPreferences sharedPrefs;

    @DexIgnore
    class Anon1 implements OnClickListener {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        /* final */ /* synthetic */ Switch val$appName;
        @DexIgnore
        /* final */ /* synthetic */ ApplicationInfo val$data;

        @DexIgnore
        Anon1(ApplicationInfo applicationInfo, Switch switchR) {
            this.val$data = applicationInfo;
            this.val$appName = switchR;
        }

        @DexIgnore
        public void onClick(View v) {
            GlanceUtils.saveGlancesConfigurationChanges(this.val$data.packageName, this.val$appName.isChecked());
            GlancesAppAdapter.this.setSwitchColor(this.val$appName, this.val$appName.isChecked());
        }
    }

    @DexIgnore
    class Anon2 implements OnClickListener {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        /* final */ /* synthetic */ Switch val$appName;

        @DexIgnore
        Anon2(Switch switchR) {
            this.val$appName = switchR;
        }

        @DexIgnore
        public void onClick(View v) {
            if (this.val$appName != null && this.val$appName.isEnabled()) {
                this.val$appName.performClick();
            }
        }
    }

    @DexIgnore
    GlancesAppAdapter(@NonNull Context context, @LayoutRes int textViewResourceId, List<ApplicationInfo> appsList2) {
        super(context, textViewResourceId, appsList2);
        this.appsList = appsList2;
        this.sharedPrefs = SettingsUtils.getSharedPreferences();
        if (this.sharedPrefs != null) {
            this.glancesAreEnabled = this.sharedPrefs.getBoolean(SettingsConstants.GLANCES, false);
        }
        this.packageManager = context.getPackageManager();
        this.layoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    @DexIgnore
    public int getCount() {
        if (this.appsList != null) {
            return this.appsList.size();
        }
        return 0;
    }

    @DexIgnore
    public ApplicationInfo getItem(int position) {
        if (this.appsList == null || position == 0) {
            return null;
        }
        return this.appsList.get(position);
    }

    @DexIgnore
    public long getItemId(int position) {
        return (long) position;
    }

    @NonNull
    @DexIgnore
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null || Boolean.TRUE.equals(convertView.getTag())) {
            convertView = this.layoutInflater.inflate(layout.app_list_row, parent, false);
        }
        convertView.setTag(Boolean.FALSE);
        if (this.appsList != null) {
            ApplicationInfo data = this.appsList.get(position);
            if (data != null) {
                Switch appName = convertView.findViewById(id.app_name);
                if (appName != null) {
                    appName.setText(data.loadLabel(this.packageManager));
                    appName.setEnabled(this.glancesAreEnabled);
                    appName.setOnClickListener(new Anon1(data, appName));
                    boolean isChecked = this.sharedPrefs.getBoolean(data.packageName, false);
                    appName.setChecked(isChecked);
                    setSwitchColor(appName, isChecked);
                }
                ImageView iconView = convertView.findViewById(id.app_icon);
                if (iconView != null) {
                    try {
                        iconView.setImageDrawable(data.loadIcon(this.packageManager));
                    } catch (OutOfMemoryError ignored) {
                    }
                    iconView.setOnClickListener(new Anon2(appName));
                }
            }
        }
        return convertView;
    }

    @DexIgnore
    public void setSwitchColor(@NonNull Switch appSwitch, boolean isChecked) {
        Context context = NavdyApplication.getAppContext();
        int trackColor = ContextCompat.getColor(context, isChecked ? color.blue_high : color.grey);
        int thumbColor = ContextCompat.getColor(context, isChecked ? color.blue : color.grey_3);
        Drawable trackDrawable = appSwitch.getTrackDrawable();
        Drawable thumbDrawable = appSwitch.getThumbDrawable();
        if (VERSION.SDK_INT < 23) {
            if (trackDrawable != null) {
                trackDrawable.setColorFilter(trackColor, Mode.MULTIPLY);
            }
            if (thumbDrawable != null) {
                thumbDrawable.setColorFilter(thumbColor, Mode.MULTIPLY);
                return;
            }
            return;
        }
        if (trackDrawable != null) {
            trackDrawable.setTint(trackColor);
        }
        if (thumbDrawable != null) {
            thumbDrawable.setTint(thumbColor);
        }
    }
}
