package com.navdy.client.app.ui.settings;

import com.navdy.client.app.ui.firstlaunch.AppSetupActivity;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit
public class BluetoothPairActivity extends com.navdy.client.app.ui.base.BaseActivity {
    // public static final java.lang.String bluetoothDialogTag = "BLUETOOTH_DIALOG_FRAGMENT";
    //
    // class Anon1 implements android.app.FragmentManager.OnBackStackChangedListener {
    //     Anon1() {
    //     }
    //
    //     public void onBackStackChanged() {
    //         if (com.navdy.client.app.ui.settings.BluetoothPairActivity.this.getFragmentManager().getBackStackEntryCount() == 0) {
    //             com.navdy.client.app.ui.settings.BluetoothPairActivity.this.finish();
    //         }
    //     }
    // }
    //
    // public void onCreate(android.os.Bundle savedInstanceState) {
    //     this.logger.v("::onCreate");
    //     super.onCreate(savedInstanceState);
    //     android.app.FragmentManager fragmentManager = getFragmentManager();
    //     fragmentManager.beginTransaction().addToBackStack(null).add(new com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment(), bluetoothDialogTag).commit();
    //     fragmentManager.addOnBackStackChangedListener(new com.navdy.client.app.ui.settings.BluetoothPairActivity.Anon1());
    // }

    @DexReplace
    protected void onResume() {
        super.onResume();
        com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Settings.BLUETOOTH);
        AppSetupActivity.hasOpenedBluetoothPairingDialog_ = true;

    }
}
