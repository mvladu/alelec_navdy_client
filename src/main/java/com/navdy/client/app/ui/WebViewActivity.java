package com.navdy.client.app.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.common.GoogleApiAvailability;
import com.navdy.client.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.service.library.util.IOUtils;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class WebViewActivity extends BaseToolbarActivity {
    @DexIgnore
    public static /* final */ String ATTRIBUTION; // = "Attributions";
    @DexIgnore
    public static /* final */ String EXTRA_HTML; // = "extra_html";
    @DexIgnore
    public static /* final */ String EXTRA_PREVENT_LINKS; // = "prevent_links";
    @DexIgnore
    public static /* final */ String EXTRA_TYPE; // = "type";
    @DexIgnore
    public static /* final */ String PRIVACY; // = "Privacy";
    @DexIgnore
    public static /* final */ String TERMS; // = "Terms";
    @DexIgnore
    public static /* final */ String ZENDESK; // = "Zendesk";
    @DexIgnore
    private String type;

    @DexIgnore
    WebViewActivity() {}

    // @DexIgnore
    // class Anon1 extends WebViewClient {
    //     @DexIgnore
    //     Anon1() {
    //     }
    //
    //     @DexIgnore
    //     public boolean shouldOverrideUrlLoading(WebView view, String url) {
    //         return true;
    //     }
    //
    //     @DexIgnore
    //     public void onLoadResource(WebView view, String url) {
    //     }
    // }
    //
    // @DexIgnore
    // class Anon2 extends WebViewClient {
    //     @DexIgnore
    //     Anon2() {
    //     }
    //
    //     @DexIgnore
    //     public void onPageFinished(WebView finishedWebView, String url) {
    //         try {
    //             WebViewActivity.this.injectMobileStyleForZendeskPage(finishedWebView);
    //         } catch (Exception e) {
    //             WebViewActivity.this.logger.e("IOException: " + e);
    //         }
    //         super.onPageFinished(finishedWebView, url);
    //     }
    // }

    @Override
    @DexReplace
    protected void onCreate(final Bundle bundle) {
        int n = 0;
        super.onCreate(bundle);
        this.setContentView(R.layout.web_view_activity);
        final WebView webView = (WebView)this.findViewById(R.id.web_view);
        if (webView == null) {
            BaseActivity.showLongToast(R.string.unable_to_load_webview);
            this.finish();
        }
        else {
            final Intent intent = this.getIntent();
            this.type = intent.getStringExtra("type");
            final boolean booleanExtra = intent.getBooleanExtra("prevent_links", false);
            final String type = this.type;
            Label_0114: {
                switch (type.hashCode()) {
                    case 1350155112: {
                        if (type.equals(PRIVACY)) {
                            break Label_0114;
                        }
                        break;
                    }
                    case 80697703: {
                        if (type.equals(TERMS)) {
                            n = 1;
                            break Label_0114;
                        }
                        break;
                    }
                    case -852110348: {
                        if (type.equals(ATTRIBUTION)) {
                            n = 2;
                            break Label_0114;
                        }
                        break;
                    }
                }
                n = -1;
            }
            switch (n) {
                case 0: {
                    new ToolbarBuilder().title(R.string.privacy_policy).build();
                    break;
                }
                case 1: {
                    new ToolbarBuilder().title(R.string.terms_of_service).build();
                    break;
                }
                case 2: {
                    new ToolbarBuilder().title(R.string.acknowledgments).build();
                    break;
                }
            }
            if (booleanExtra || "Privacy".equals(this.type) || "Terms".equals(this.type)) {
                webView.setWebViewClient(new WebViewClient() {
                    // final /* synthetic */ WebViewActivity this$Anon0;

                    // WebViewActivity$Anon1() {
                    //     this.this$Anon0 = this$Anon0;
                    //     super();
                    // }

                    public void onLoadResource(final WebView webView, final String s) {
                    }

                    public boolean shouldOverrideUrlLoading(final WebView webView, final String s) {
                        return true;
                    }
                });
            }
            else if ("Zendesk".equals(this.type)) {
                webView.setWebViewClient(new WebViewClient() {
                    // final /* synthetic */ WebViewActivity this$Anon0;
                    //
                    // WebViewActivity$Anon2() {
                    //     super();
                    //     this.this$Anon0 = this$Anon0;
                    // }

                    public void onPageFinished(final WebView webView, final String s) {
                        while (true) {
                            try {
                                WebViewActivity.this.injectMobileStyleForZendeskPage(webView);
                                super.onPageFinished(webView, s);
                            }
                            catch (Exception ex) {
                                WebViewActivity.this.logger.e("IOException: " + ex);
                                continue;
                            }
                            break;
                        }
                    }
                });
            }
            Label_0391: {
                try {
                    if (!ATTRIBUTION.equals(this.type)) {
                        break Label_0391;
                    }
                    final String convertInputStreamToString = IOUtils.convertInputStreamToString(this.getResources().openRawResource(R.raw.attribution), "UTF-8");
                    webView.getSettings().setUseWideViewPort(true);
                    webView.loadDataWithBaseURL(null, convertInputStreamToString, "text/plain", null, null);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
                return;
            }
            String stringExtra = null;
            if ("Privacy".equals(this.type)) {
                stringExtra = "file:///android_res/raw/privacypolicy.html";
            }
            else if ("Terms".equals(this.type)) {
                stringExtra = "file:///android_res/raw/terms.html";
            }
            else if ("Zendesk".equals(this.type)) {
                stringExtra = intent.getStringExtra("extra_html");
            }
            webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
            webView.loadUrl(stringExtra);
        }
    }

    @DexIgnore
    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(TrackerConstants.Screen.Web.tag(this.type));
    }

    @DexIgnore
    public void injectMobileStyleForZendeskPage(WebView webView) {
        try {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl("javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('style');style.type = 'text/css';style.appendChild(document.createTextNode(\".request-nav, .footer, .comments-title, .comment-list, .comment-load-more-comments, .comment-closed-notification, .comment-submit-request { display: none !important }\"));parent.appendChild(style);})()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
