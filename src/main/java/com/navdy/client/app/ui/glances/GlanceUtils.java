package com.navdy.client.app.ui.glances;

import android.content.Context;
import android.content.SharedPreferences;

import com.navdy.client.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.events.glances.GlanceIconConstants;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.notification.NotificationSetting;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit
public class GlanceUtils {
    @DexIgnore
    private static Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.glances.GlanceUtils.class);

    @DexIgnore
    public static boolean isDrivingGlance(String pkg) {
        return GlanceConstants.isPackageInGroup(pkg, GlanceConstants.Group.DRIVING_GLANCES);
    }

    @DexIgnore
    public static boolean isWhiteListedApp(String pkg) {
        return GlanceConstants.isPackageInGroup(pkg, GlanceConstants.Group.WHITE_LIST);
    }

    @DexIgnore
    public static boolean isCallGlancesEnabled() {
        Context appContext = NavdyApplication.getAppContext();
        SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        String dialerPackage = SettingsUtils.getDialerPackage(appContext.getPackageManager());
        return sharedPrefs.getBoolean(SettingsConstants.GLANCES, false) && sharedPrefs.getBoolean(dialerPackage, true);
    }

    @DexIgnore
    public static boolean isSmsGlancesEnabled() {
        SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        String smsPackage = SettingsUtils.getSmsPackage();
        return sharedPrefs.getBoolean(SettingsConstants.GLANCES, false) && sharedPrefs.getBoolean(smsPackage, true);
    }

    @DexIgnore
    public static boolean isThisGlanceEnabledAsWellAsGlobal(String pkg) {
        SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        boolean defaultVal = getDefaultValueFor(pkg);
        logger.d("glances are globally turned " + (sharedPrefs.getBoolean(SettingsConstants.GLANCES, false) ? "on" : "off") + " and are " + (sharedPrefs.getBoolean(pkg, defaultVal) ? "on" : "off") + " for " + pkg + " default value for this package is " + defaultVal);
        return sharedPrefs.getBoolean(SettingsConstants.GLANCES, false) && sharedPrefs.getBoolean(pkg, defaultVal);
    }

    @DexIgnore
    public static boolean isThisGlanceEnabledInItself(String pkg) {
        SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        boolean defaultVal = getDefaultValueFor(pkg);
        logger.d("glances are " + (sharedPrefs.getBoolean(pkg, defaultVal) ? "on" : "off") + " for " + pkg + " default value for this package is " + defaultVal);
        return sharedPrefs.getBoolean(pkg, defaultVal);
    }

    @DexIgnore
    public static void saveGlancesConfigurationChanges(String key, boolean value) {
        SettingsUtils.getSharedPreferences().edit().putBoolean(key, value).apply();
        Tracker.tagEvent(TrackerConstants.Event.GLANCES_CONFIGURED);
    }

    @DexIgnore
    public static NotificationPreferences buildGlancesPreferences(long serialNumber, boolean glancesAreEnabled, boolean readAloud, boolean showContent) {
        ArrayList<NotificationSetting> settings = new ArrayList<>();
        settings.add(new NotificationSetting.Builder().app(GlanceConstants.PHONE_PACKAGE).enabled(isThisGlanceEnabledInItself(GlanceConstants.PHONE_PACKAGE)).build());
        settings.add(new NotificationSetting.Builder().app(GlanceConstants.SMS_PACKAGE).enabled(isThisGlanceEnabledInItself(GlanceConstants.SMS_PACKAGE)).build());
        settings.add(new NotificationSetting.Builder().app(GlanceConstants.FUEL_PACKAGE).enabled(isThisGlanceEnabledInItself(GlanceConstants.FUEL_PACKAGE)).build());
        settings.add(new NotificationSetting.Builder().app(GlanceConstants.MUSIC_PACKAGE).enabled(isThisGlanceEnabledInItself(GlanceConstants.MUSIC_PACKAGE)).build());
        settings.add(new NotificationSetting.Builder().app(GlanceConstants.TRAFFIC_PACKAGE).enabled(isThisGlanceEnabledInItself(GlanceConstants.TRAFFIC_PACKAGE)).build());
        return buildGlancesPreferences(serialNumber, glancesAreEnabled, readAloud, showContent, settings);
    }

    @DexIgnore
    public static NotificationPreferences buildGlancesPreferences(long serialNumber, boolean glancesAreEnabled, boolean readAloud, boolean showContent, List<NotificationSetting> settings) {
        NotificationPreferences.Builder notifPrefBldr = new NotificationPreferences.Builder();
        notifPrefBldr.serial_number = serialNumber;
        notifPrefBldr.enabled = glancesAreEnabled;
        notifPrefBldr.readAloud = readAloud;
        notifPrefBldr.showContent = showContent;
        notifPrefBldr.settings = settings;
        return notifPrefBldr.build();
    }

    @DexIgnore
    public static boolean sendGlancesSettingsToTheHudBasedOnSharedPrefValue() {
        SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        return SettingsUtils.sendGlancesSettingsToTheHud(buildGlancesPreferences(sharedPrefs.getLong(SettingsConstants.GLANCES_SERIAL_NUM, 0), sharedPrefs.getBoolean(SettingsConstants.GLANCES, false), sharedPrefs.getBoolean(SettingsConstants.GLANCES_READ_ALOUD, true), sharedPrefs.getBoolean(SettingsConstants.GLANCES_SHOW_CONTENT, false)));
    }

    @DexIgnore
    public static boolean getDefaultValueFor(String pkg) {
        if (isDrivingGlance(pkg)) {
            return true;
        }
        if (isWhiteListedApp(pkg)) {
        }
        return false;
    }

    @DexIgnore
    public static void sendTestGlance(BaseActivity activity) {
        sendTestGlance(activity, null);
    }

    @DexIgnore
    public static void sendTestGlance(BaseActivity activity, Logger logger2) {
        List<KeyValue> data = new ArrayList<>();
        Context context = NavdyApplication.getAppContext();
        AppInstance appInstance = AppInstance.getInstance();
        ArrayList<String> glanceMessageArray = new ArrayList<>();
        String[] glanceMessages = activity.getResources().getStringArray(R.array.glance_messages);
        String title = context.getString(R.string.title_activity_main);
        String firstMessage = context.getString(R.string.glance_test_message_one);
        glanceMessageArray.add(firstMessage);
        glanceMessageArray.addAll(Arrays.asList(glanceMessages));
        if (!appInstance.isDeviceConnected() || appInstance.getRemoteDevice() == null || glanceMessageArray.isEmpty()) {
            if (logger2 != null) {
                logger2.v("Device is disconnected. Cannot send test glance");
            }
            if (!activity.isFinishing()) {
                activity.showSimpleDialog(0, activity.getString(R.string.navdy_display_disconnected), activity.getString(R.string.glance_failure_explanation));
                return;
            }
            return;
        }
        if (logger2 != null) {
            logger2.v("Device is connected. Sending test glance");
        }
        data.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), title));
        data.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), firstMessage));
        for (String glanceMessage : glanceMessageArray) {
            data.add(new KeyValue(GenericConstants.GENERIC_TITLE.name(), title));
            data.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), glanceMessage));
            data.add(new KeyValue(GenericConstants.GENERIC_MAIN_ICON.name(), GlanceIconConstants.GLANCE_ICON_NAVDY_MAIN.name()));
            data.add(new KeyValue(GenericConstants.GENERIC_SIDE_ICON.name(), GlanceIconConstants.GLANCE_ICON_MESSAGE_SIDE_BLUE.name()));
            appInstance.getRemoteDevice().postEvent(new GlanceEvent.Builder().glanceType(GlanceEvent.GlanceType.GLANCE_TYPE_GENERIC).id(UUID.randomUUID().toString()).postTime(System.currentTimeMillis()).provider("Navdy").glanceData(data).build());
        }
    }
}
