package com.navdy.client.app.ui.firstlaunch;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.navdy.client.R.id;
import com.navdy.client.R.layout;
import com.navdy.client.R.string;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.UserAccountInfo;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.util.ContactsManager;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType;
import com.navdy.client.app.ui.settings.ProfileSettingsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.task.TaskManager;
import com.squareup.wire.Message;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class AppSetupActivity extends ProfileSettingsActivity {
    @DexIgnore
    private ViewPager bottomCard;
    @DexIgnore
    private boolean hasNeverShownBtSuccess; // = true;
    @DexIgnore
    private int hiddenButtonClickCount; // = 0;
    @DexIgnore
    private ImageView hud;
    @DexIgnore
    private AppSetupPagerAdapter setupPagerAdapter;
    @DexIgnore
    private boolean showingFail; // = false;

    @DexAdd
    public static boolean hasOpenedBluetoothPairingDialog_; // = false;


    @DexIgnore
    AppSetupActivity() {}

    // @DexEdit(defaultAction = DexAction.ADD)
    // class Anon1 implements Runnable {
    //     @DexIgnore
    //     final AppSetupActivity this$Anon0;
    //
    //     /* renamed from: com.navdy.client.app.ui.firstlaunch.AppSetupActivity$Anon1$Anon1 reason: collision with other inner class name */
    //     @DexAdd
    //     class _Anon1 implements Runnable {
    //         @DexAdd
    //         _Anon1() {
    //         }
    //
    //         @DexAdd
    //         public void run() {
    //             SettingsUtils.incrementDriverProfileSerial();
    //             SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
    //         }
    //     }
    //
    //     @DexAdd
    //     Anon1(AppSetupActivity _this) {
    //         this$Anon0 = _this;
    //     }
    //
    //     @DexReplace
    //     public void run() {
    //         UserAccountInfo userAccountInfo = ContactsManager.lookUpUserProfileInfo();
    //         if (userAccountInfo != null) {
    //             boolean incrementAndSendToHud = false;
    //             if (this$Anon0.userPhoto == null && userAccountInfo.photo != null) {
    //                 Tracker.saveUserPhotoToInternalStorage(userAccountInfo.photo);
    //                 incrementAndSendToHud = true;
    //             }
    //             if (this$Anon0.customerPrefs != null) {
    //                 Editor editor = this$Anon0.customerPrefs.edit();
    //                 if (!StringUtils.isValidName(this$Anon0.name) && StringUtils.isValidName(userAccountInfo.fullName)) {
    //                     editor.putString(ProfilePreferences.FULL_NAME, userAccountInfo.fullName);
    //                     incrementAndSendToHud = true;
    //                 }
    //                 if (!StringUtils.isValidEmail(this$Anon0.email)) {
    //                     if (!StringUtils.isValidEmail(userAccountInfo.email)) {
    //                         Account[] accounts = ((AccountManager) this$Anon0.getSystemService(Context.ACCOUNT_SERVICE)).getAccounts();
    //                         int length = accounts.length;
    //                         int i = 0;
    //                         while (true) {
    //                             if (i >= length) {
    //                                 break;
    //                             }
    //                             Account account = accounts[i];
    //                             if (account.type.equalsIgnoreCase("com.google") && StringUtils.isValidEmail(account.name)) {
    //                                 editor.putString("email", account.name);
    //                                 incrementAndSendToHud = true;
    //                                 break;
    //                             }
    //                             i++;
    //                         }
    //                     } else {
    //                         editor.putString("email", userAccountInfo.email);
    //                         incrementAndSendToHud = true;
    //                     }
    //                 }
    //                 editor.apply();
    //             }
    //             if (incrementAndSendToHud) {
    //                 TaskManager.getInstance().execute(new Anon1._Anon1(), 1);
    //             }
    //         }
    //     }
    // }

    // class Anon2 implements android.support.v4.view.ViewPager.OnPageChangeListener {
    //     Anon2() {
    //     }
    //
    //     public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    //     }
    //
    //     public void onPageSelected(int position) {
    //         com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.showingFail = false;
    //         com.navdy.client.app.framework.util.SystemUtils.dismissKeyboard(com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this);
    //         com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.getCurrentScreen();
    //         if (currentScreen == null || currentScreen.screenType != com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.PROFILE) {
    //             com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.hideProfileStuff();
    //             com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.updateIllustration(position);
    //             if (com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.setupPagerAdapter != null) {
    //                 com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment frag = (com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment) com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.setupPagerAdapter.getItem(position);
    //                 if (frag != null) {
    //                     frag.showNormal();
    //                 }
    //             }
    //         } else {
    //             com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.showProfileStuff();
    //         }
    //         if (currentScreen != null && com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.hasNeverShownBtSuccess && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH && com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.isDeviceConnected(com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.getApplicationContext())) {
    //             com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.hasNeverShownBtSuccess = false;
    //             com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.moveToNextScreen();
    //         }
    //     }
    //
    //     public void onPageScrollStateChanged(int state) {
    //     }
    // }

    @DexIgnore
    class Anon3 implements Runnable {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void run() {
            // com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.moveToNextScreen();
        }
    }

    @DexIgnore
    class Anon4 implements Runnable {
        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        public void run() {
            // com.navdy.client.app.ui.firstlaunch.AppSetupActivity.this.showFailureForCurrentScreen();
        }
    }

    @DexIgnore
    class Anon5 implements Runnable {
        @DexIgnore
        Anon5() {
        }

        @DexIgnore
        public void run() {
            AppSetupActivity.this.moveToNextScreen();
            AppInstance appInstance = AppInstance.getInstance();
            if (appInstance.mLocationTransmitter != null) {
                appInstance.mLocationTransmitter.start();
            }
        }
    }

    @DexIgnore
    class Anon6 implements Runnable {
        @DexIgnore
        Anon6() {
        }

        @DexIgnore
        public void run() {
            AppSetupActivity.this.moveToNextScreen();
        }
    }

    @DexIgnore
    class Anon7 implements Runnable {
        @DexIgnore
        Anon7() {
        }

        @DexIgnore
        public void run() {
            AppSetupActivity.this.moveToNextScreen();
            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null) {
                remoteDevice.postEvent((Message) MusicServiceHandler.getMusicCapabilities());
            }
            MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
            if (musicServiceHandler != null && musicServiceHandler.shouldPerformFullPlaylistIndex()) {
                musicServiceHandler.indexPlaylists();
            }
        }
    }

    @DexIgnore
    public static void goToAppSetup(Activity activity) {
        activity.startActivity(new Intent(activity, AppSetupActivity.class));
    }

    // @DexReplace
    @DexIgnore
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView((int) layout.fle_app_setup);
        // new ToolbarBuilder().title((int) string.app_setup).build();
        // this.hud = (ImageView) findViewById(id.hud);
        // this.bottomCard = (ViewPager) findViewById(id.bottom_card);
        // if (this.hud == null || this.bottomCard == null) {
        //     this.logger.e("Ui element missing !");
        //     return;
        // }
        // this.userPhoto = Tracker.getUserProfilePhoto();
        // if (this.customerPrefs != null) {
        //     this.name = this.customerPrefs.getString(ProfilePreferences.FULL_NAME, "");
        //     this.email = this.customerPrefs.getString("email", "");
        // }
        // if (!StringUtils.isValidName(this.name) || !StringUtils.isValidEmail(this.email)) {
        //     requestContactsPermission(new Anon1(this), null);
        // }
        // setUpViewPager();
    }

    @DexIgnore
    private void setUpViewPager() {
        // this.bottomCard.setOffscreenPageLimit(1);
        // this.setupPagerAdapter = new com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter(getSupportFragmentManager(), this.imageCache);
        // this.bottomCard.addOnPageChangeListener(new com.navdy.client.app.ui.firstlaunch.AppSetupActivity.Anon2());
        // this.bottomCard.setAdapter(this.setupPagerAdapter);
    }

    @DexIgnore
    protected void onPause() {
        super.onPause();
    }

    @DexReplace
    public void onResume() {
        super.onResume();
        if (!Tracker.weHaveCarInfo()) {
            startActivity(new Intent(getApplicationContext(), EditCarInfoActivity.class));
        }
        this.setupPagerAdapter.recalculateScreenCount();
        AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null) {
            if (currentScreen.screenType == ScreenType.BLUETOOTH) {
                if (isDeviceConnected(getApplicationContext())) {
                    moveToNextScreen();
                } else if (hasOpenedBluetoothPairingDialog_) {
                    showFailureForCurrentScreen();
                }
            }
            if (currentScreen.screenType == ScreenType.NOTIFICATIONS) {
                if (BaseActivity.weHaveNotificationPermission()) {
                    moveToNextScreen();
                } else {
                    showFailureForCurrentScreen();
                }
            }
        }
        Tracker.tagScreen("First_Launch");
    }
    //
    // protected void onDestroy() {
    //     if (this.setupPagerAdapter != null) {
    //         this.setupPagerAdapter.clearCache();
    //     }
    //     super.onDestroy();
    // }
    //
    // public boolean onCreateOptionsMenu(android.view.Menu menu) {
    //     return true;
    // }
    //
    // public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull java.lang.String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
    //     super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    //     if (this.setupPagerAdapter != null) {
    //         this.setupPagerAdapter.recalculateScreenCount();
    //     }
    // }
    //
    // @com.squareup.otto.Subscribe
    // public void appInstanceDeviceConnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent deviceConnectedEvent) {
    //     handleConnectionChange(true);
    // }
    //
    // @com.squareup.otto.Subscribe
    // public void appInstanceDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent deviceDisconnectedEvent) {
    //     handleConnectionChange(false);
    // }
    //
    // private void handleConnectionChange(boolean connected) {
    //     if (this.setupPagerAdapter != null) {
    //         this.setupPagerAdapter.recalculateScreenCount();
    //     }
    //     if (!connected) {
    //         this.bottomCard.setCurrentItem(1);
    //         return;
    //     }
    //     com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
    //     if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
    //         moveToNextScreen();
    //     }
    // }

    @DexIgnore
    public boolean isDeviceConnected(Context applicationContext) {
        return AppInstance.getInstance().isDeviceConnected();
    }

    // public void onPrivacyPolicyClick(android.view.View view) {
    //     android.content.Intent browserIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.WebViewActivity.class);
    //     browserIntent.putExtra("type", com.navdy.client.app.ui.WebViewActivity.PRIVACY);
    //     startActivity(browserIntent);
    // }
    //
    // public void onContactSupportClick(android.view.View view) {
    //     startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.ContactUsActivity.class));
    // }
    //
    // public void onDescriptionClick(android.view.View view) {
    //     if (this.showingFail) {
    //         com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
    //         if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
    //             onHelpCenterClick(null);
    //         } else if (currentScreen == null || !(currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.PROFILE || currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.NOTIFICATIONS)) {
    //             com.navdy.client.app.framework.util.SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
    //         }
    //     }
    // }

    @DexReplace
    public void onHelpCenterClick(View view) {
        openBrowserFor(Uri.parse("http://navdy.alelec.net"));
    }

    @DexReplace
    public void onButtonClick(View v) {
        AppSetupScreen currentScreen = getCurrentScreen();
        if (currentScreen != null && !BaseActivity.isEnding(this)) {
            Runnable doThingsThatRequirePermission = new Anon3();
            Runnable handlePermissionDenial = new Anon4();
            Context applicationContext = NavdyApplication.getAppContext();
            switch (currentScreen.screenType) {
                case BLUETOOTH:
                    if (isDeviceConnected(applicationContext)) {
                        moveToNextScreen();
                        return;
                    }
                    requestLocationPermission(new Runnable() {
                        public void run() {
                            AppSetupActivity.this.hasNeverShownBtSuccess = true;
                            openBtConnectionDialog();
                        }
                    }, handlePermissionDenial, AppSetupActivity.this);
                    return;
                case NOTIFICATIONS:
                    if (!BaseActivity.weHaveNotificationPermission()) {
                        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case ACCESS_FINE_LOCATION:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestLocationPermission(new Anon5(), handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case USE_MICROPHONE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestMicrophonePermission(new Anon6(), handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case READ_CONTACTS:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestContactsPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case RECEIVE_SMS:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestSmsPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case CALL_PHONE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestPhonePermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case READ_CALENDAR:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestCalendarPermission(doThingsThatRequirePermission, handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case WRITE_EXTERNAL_STORAGE:
                    if (currentScreen.isMandatory || !this.showingFail) {
                        requestStoragePermission(new Anon7(), handlePermissionDenial);
                        return;
                    } else {
                        moveToNextScreen();
                        return;
                    }
                case END:
                    SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.FINISHED_APP_SETUP, true).apply();
                    moveToNextScreen();
                    return;
                default:
                    moveToNextScreen();
                    return;
            }
        }
    }

    // public void onProfileInfoSet() {
    //     onButtonClick(null);
    // }
    //
    // private void showProfileStuff() {
    //     changeProfileStuffVisibility(0);
    // }
    //
    // private void hideProfileStuff() {
    //     changeProfileStuffVisibility(8);
    // }
    //
    // private void changeProfileStuffVisibility(int visibility) {
    //     if (this.photo != null) {
    //         this.photo.setVisibility(visibility);
    //     }
    //     if (this.photoHint != null) {
    //         this.photoHint.setVisibility(visibility);
    //     }
    //     if (this.hud != null && visibility == 0) {
    //         this.hud.setImageResource(com.navdy.client.R.drawable.image_navdy_profile_empty);
    //     }
    // }
    //
    // protected boolean validateAndSaveName(boolean success) {
    //     boolean success2 = super.validateAndSaveName(success);
    //     if (this.setupPagerAdapter != null) {
    //         this.setupPagerAdapter.recalculateScreenCount();
    //     }
    //     return success2;
    // }
    //
    // protected boolean validateAndSaveEmail(boolean success) {
    //     boolean success2 = super.validateAndSaveEmail(success);
    //     if (this.setupPagerAdapter != null) {
    //         this.setupPagerAdapter.recalculateScreenCount();
    //     }
    //     return success2;
    // }
    //
    // public void updateIllustration(int position) {
    //     this.setupPagerAdapter.updateIllustration(new java.lang.ref.WeakReference<>(this.hud), position, this.showingFail);
    // }

    @DexIgnore
    private void showFailureForCurrentScreen() {
    //     this.logger.d("showFailureForCurrentScreen");
    //     if (this.setupPagerAdapter != null && this.bottomCard != null) {
    //         int currentPosition = this.bottomCard.getCurrentItem();
    //         com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment fragment = (com.navdy.client.app.ui.firstlaunch.AppSetupBottomCardGenericFragment) this.setupPagerAdapter.getItem(currentPosition);
    //         this.showingFail = true;
    //         fragment.showFail();
    //         updateIllustration(currentPosition);
    //     }
    }

    @Nullable
    @DexIgnore
    public AppSetupScreen getCurrentScreen() {
    //     int currentIndex = this.bottomCard != null ? this.bottomCard.getCurrentItem() : 0;
    //     if (this.setupPagerAdapter != null) {
    //         return com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.getScreen(currentIndex);
    //     }
        return null;
    }

    @DexIgnore
    private void moveToNextScreen() {
    //     this.setupPagerAdapter.recalculateScreenCount();
    //     int nextIndex = this.bottomCard.getCurrentItem() + 1;
    //     com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
    //     if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.END) {
    //         goToHomeScreen();
    //     } else if (nextIndex < this.setupPagerAdapter.getCount()) {
    //         this.bottomCard.setCurrentItem(nextIndex);
    //     }
    }

    // private void goToHomeScreen() {
    //     com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.FIRST_LAUNCH_COMPLETED);
    //     android.content.SharedPreferences customerPrefs = com.navdy.client.app.ui.settings.SettingsUtils.getCustomerPreferences();
    //     java.lang.String carYear = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_YEAR, "");
    //     java.lang.String carMake = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MAKE, "");
    //     java.lang.String carModel = customerPrefs.getString(com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences.CAR_MODEL, "");
    //     android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
    //     java.lang.String box = sharedPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.BOX, "Old_Box");
    //     int nbConfig = sharedPreferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.NB_CONFIG, 0);
    //     java.lang.String mount = com.navdy.client.app.ui.firstlaunch.InstallPagerAdapter.getCurrentMountType(sharedPreferences).getValue();
    //     java.util.HashMap<java.lang.String, java.lang.String> attributes = new java.util.HashMap<>(5);
    //     attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_YEAR, carYear);
    //     attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MAKE, carMake);
    //     attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.CAR_MODEL, carModel);
    //     attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.SELECTED_MOUNT, mount);
    //     attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.SELECTED_BOX, box);
    //     attributes.put(com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes.NB_CONFIGURATIONS, java.lang.String.valueOf(nbConfig));
    //     com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.Install.CONFIGURATION_AT_COMPLETION, attributes);
    //     com.navdy.client.app.framework.servicehandler.ContactServiceHandler.getInstance().forceSendFavoriteContactsToHud();
    //     android.content.Intent i = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.homescreen.HomescreenActivity.class);
    //     i.setFlags(268468224);
    //     startActivity(i);
    //     finish();
    // }
    //
    // public void onBackPressed() {
    //     int nextIndex = this.bottomCard.getCurrentItem() - 1;
    //     if (nextIndex < 0) {
    //         super.onBackPressed();
    //     } else {
    //         this.bottomCard.setCurrentItem(nextIndex);
    //     }
    // }
    //
    // public static boolean userHasFinishedAppSetup() {
    //     return com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.FINISHED_APP_SETUP, false);
    // }
    //
    // public void onHiddenButtonClick(android.view.View view) {
    //     com.navdy.client.app.ui.firstlaunch.AppSetupScreen currentScreen = getCurrentScreen();
    //     if (currentScreen != null && currentScreen.screenType == com.navdy.client.app.ui.firstlaunch.AppSetupScreen.ScreenType.BLUETOOTH) {
    //         this.hiddenButtonClickCount++;
    //         if (this.hiddenButtonClickCount >= 10) {
    //             this.hiddenButtonClickCount = 0;
    //             this.setupPagerAdapter.pretendBtConnected();
    //             moveToNextScreen();
    //         }
    //     }
    // }
}
