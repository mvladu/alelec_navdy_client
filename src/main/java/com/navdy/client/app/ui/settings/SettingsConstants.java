package com.navdy.client.app.ui.settings;

import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexReplace
public class SettingsConstants
{
    public static final String AUDIO_ALL_AUDIO = "audio_all_audio";
    public static final boolean AUDIO_ALL_AUDIO_DEFAULT = true;
    public static final String AUDIO_BT_HFP_VOLUME = "audio_bt_HFP_volume";
    public static final int AUDIO_BT_HFP_VOLUME_DEFAULT = 5;
    public static final String AUDIO_CAMERA_WARNINGS = "audio_camera_warnings";
    public static final boolean AUDIO_CAMERA_WARNINGS_DEFAULT = true;
    public static final String AUDIO_DRIVE_SCORE_WARNINGS = "audio_drive_score_warnings";
    public static final boolean AUDIO_DRIVE_SCORE_WARNINGS_DEFAULT = true;
    public static final String AUDIO_HFP_DELAY_LEVEL = "audio_hfp_delay_level";
    public static final int AUDIO_HFP_DELAY_LEVEL_DEFAULT = 1;
    public static final String AUDIO_HFP_DELAY_MEASURED = "audio_hfp_delay_measured";
    public static final String AUDIO_HUD_VOLUME = "audio_hud_volume";
    public static final int AUDIO_HUD_VOLUME_DEFAULT = 100;
    public static final String AUDIO_MENU_FEEDBACK = "audio_menu_feedback";
    public static final boolean AUDIO_MENU_FEEDBACK_DEFAULT = true;
    public static final String AUDIO_NOTIFICATIONS = "audio_notifications";
    public static final boolean AUDIO_NOTIFICATIONS_DEFAULT = true;
    public static final String AUDIO_OUTPUT_PREFERENCE = "audio_output_preference";
    public static final int AUDIO_OUTPUT_PREFERENCE_DEFAULT;
    @Deprecated
    public static final String AUDIO_PHONE_VOLUME = "audio_phone_volume";
    @Deprecated
    public static final int AUDIO_PHONE_VOLUME_DEFAULT = 75;
    public static final String AUDIO_PLAY_WELCOME_MESSAGE = "audio_play_welcome";
    public static final boolean AUDIO_PLAY_WELCOME_MESSAGE_DEFAULT = false;
    public static final String AUDIO_SERIAL_NUM = "audio_preferences_serial_number";
    public static final String AUDIO_SPEED_WARNINGS = "audio_speed_warnings";
    public static final boolean AUDIO_SPEED_WARNINGS_DEFAULT = false;
    public static final String AUDIO_TTS_VOLUME = "audio_tts_volume";
    public static final float AUDIO_TTS_VOLUME_DEFAULT = 0.6f;
    public static final float AUDIO_TTS_VOLUME_HIGH = 1.0f;
    public static final float AUDIO_TTS_VOLUME_LOW = 0.3f;
    public static final float AUDIO_TTS_VOLUME_NORMAL = 0.6f;
    public static final String AUDIO_TURN_BY_TURN_INSTRUCTIONS = "audio_turn_by_turn_instructions";
    public static final boolean AUDIO_TURN_BY_TURN_INSTRUCTIONS_DEFAULT = true;
    public static final String AUDIO_VOICE = "audio_voice";
    public static final String AUDIO_VOICE_DEFAULT = "en-US";
    public static final boolean AUTO_ATTACH_LOGS_DEFAULT = false;
    public static final String AUTO_ATTACH_LOGS_ENABLED = "attach_logs";
    public static final String BOX = "box";
    public static final String BOX_DEFAULT = "Old_Box";
    public static final String CALENDARS_ENABLED = "calendars_enabled";
    public static final boolean CALENDARS_ENABLED_DEFAULT = true;
    public static final String CALENDAR_PREFIX = "calendar_";
    public static final int CURRENT_SHARED_PREF_VERSION = 1;
    public static final String DIAL_LONG_PRESS_ACTION = "dial_long_press_action";
    public static final int DIAL_LONG_PRESS_ACTION_DEFAULT;
    public static final String DISPLAY_AUDIO_SERIAL_NUM = "audio_serial_number";
    public static final String EXTRA_WAS_SKIPPED = "extra_was_skipped";
    public static final String FEATURE_MODE = "feature-mode";
    public static final String FEATURE_MODE_DEFAULT;
    public static final String FINISHED_APP_SETUP = "finished_app_setup";
    public static final String FINISHED_INSTALL = "finished_install";
    public static final String FINISHED_MARKETING = "finished_marketing";
    public static final String FIRST_TIME_ON_HOMESCREEN = "first_time_on_homescreen";
    public static final boolean FIRST_TIME_ON_HOMESCREEN_DEFAULT = true;
    public static final String GLANCES = "glances";
    public static final boolean GLANCES_CALL_ENABLED_DEFAULT = true;
    public static final boolean GLANCES_ENABLED_DEFAULT = false;
    public static final String GLANCES_READ_ALOUD = "glances_read_aloud";
    public static final boolean GLANCES_READ_ALOUD_DEFAULT = true;
    public static final String GLANCES_SERIAL_NUM = "glances_serial_number";
    public static final String GLANCES_SHOW_CONTENT = "glances_show_content";
    public static final boolean GLANCES_SHOW_CONTENT_DEFAULT = false;
    public static final boolean GLANCES_SMS_ENABLED_DEFAULT = true;
    public static final String GMS_IS_MISSING = "gms_is_missing";
    public static final boolean GMS_IS_MISSING_DEFAULT = false;
    public static final String GMS_VERSION_NUMBER = "gms_version_number";
    public static final int GMS_VERSION_NUMBER_DEFAULT = -1;
    public static final String HAS_SKIPPED_INSTALL = "has_skipped_install";
    public static final String HUD_AUTO_ON = "hud_auto_on_enabled";
    public static final boolean HUD_AUTO_ON_DEFAULT = true;
    public static final String HUD_BLACKLIST_OVERRIDDEN = "user_overrided_blacklist";
    public static final boolean HUD_BLACKLIST_OVERRIDDEN_DEFAULT = false;
    public static final String HUD_CANNED_RESPONSE_CAPABLE = "hud_canned_response_capable";
    public static final boolean HUD_CANNED_RESPONSE_CAPABLE_DEFAULT = false;
    public static final String HUD_COMPACT_CAPABLE = "hud_compact_capable";
    public static final boolean HUD_COMPACT_CAPABLE_DEFAULT = false;
    public static final String HUD_DIAL_LONG_PRESS_CAPABLE = "hud_dial_long_press_capable";
    public static final boolean HUD_DIAL_LONG_PRESS_CAPABLE_DEFAULT = false;
    public static final String HUD_GESTURE = "hud_gesture";
    public static final boolean HUD_GESTURE_DEFAULT = true;
    public static final String HUD_LOCAL_MUSIC_BROWSER_CAPABLE = "hud_music_capable";
    public static final boolean HUD_LOCAL_MUSIC_BROWSER_CAPABLE_DEFAULT = false;
    public static final String HUD_OBD_ON = "hud_obd_on_enabled";
    public static final String HUD_OBD_ON_DEFAULT;
    public static final String HUD_SEARCH_RESULT_LIST_CAPABLE = "hud_search_result_list_capable";
    public static final boolean HUD_SEARCH_RESULT_LIST_CAPABLE_DEFAULT = false;
    public static final String HUD_SERIAL_NUM = "hud_serial_number";
    public static final String HUD_UI_SCALING_ON = "hud_ui_scaling";
    public static final boolean HUD_UI_SCALING_ON_DEFAULT = false;
    public static final String HUD_VOICE_SEARCH_CAPABLE = "hud_voice_search_capable";
    public static final boolean HUD_VOICE_SEARCH_CAPABLE_DEFAULT = false;
    public static final String HUD_VOICE_USED_BEFORE = "hud_voice_used_before";
    public static final boolean HUD_VOICE_USED_BEFORE_DEFAULT = false;
    public static final String LAST_BUILD_VERSION = "last_build_version";
    public static final String LAST_CONFIG = "last_config";
    public static final String LAST_CONFIG_DEFAULT = "";
    public static final String LAST_HUD_UUID = "last_hud_uuid";
    public static final String LAST_HUD_UUID_DEFAULT = "UNKNOWN";
    public static final String LAST_MEDIA_APP = "last_media_app";
    public static final String LIMIT_CELLULAR_DATA = "limit_cellular_data";
    public static final boolean LIMIT_CELLULAR_DATA_DEFAULT = false;
    public static final String MESSAGING_HAS_SEEN_CAPABLE_HUD = "messaging_has_seen_capable_hud";
    public static final boolean MESSAGING_HAS_SEEN_CAPABLE_HUD_DEFAULT = false;
    public static final String MESSAGING_REPLIES = "messaging_replies";
    public static final String MESSAGING_SERIAL_NUM = "nav_serial_number";
    public static final String MOUNT_TYPE = "mount_type";
    public static final String NAVIGATION_AUTO_RECALC = "nav_auto_recalc";
    public static final boolean NAVIGATION_AUTO_RECALC_DEFAULT = false;
    public static final String NAVIGATION_AUTO_TRAINS = "nav_auto_trains";
    public static final boolean NAVIGATION_AUTO_TRAINS_DEFAULT = true;
    public static final String NAVIGATION_FERRIES = "nav_ferries";
    public static final boolean NAVIGATION_FERRIES_DEFAULT = true;
    public static final String NAVIGATION_HIGHWAYS = "nav_highways";
    public static final boolean NAVIGATION_HIGHWAYS_DEFAULT = true;
    public static final String NAVIGATION_GPS_USE_FUSED = "nav_gps_use_fused";
    public static final String NAVIGATION_ROUTE_CALCULATION = "nav_route_calculation";
    public static final boolean NAVIGATION_ROUTE_CALCULATION_DEFAULT = false;
    public static final String NAVIGATION_SERIAL_NUM = "nav_serial_number";
    public static final String NAVIGATION_TOLL_ROADS = "nav_toll_roads";
    public static final boolean NAVIGATION_TOLL_ROADS_DEFAULT = true;
    public static final String NAVIGATION_TUNNELS = "nav_tunnels";
    public static final boolean NAVIGATION_TUNNELS_DEFAULT = true;
    public static final String NAVIGATION_UNPAVED_ROADS = "nav_unpaved_roads";
    public static final boolean NAVIGATION_UNPAVED_ROADS_DEFAULT = true;
    public static final String GOOGLE_MAPS_API_KEY = "gmak";
    public static final String GOOGLE_SERVICES_API_KEY = "gsak";
    public static final String NAVIGATION_HERE_GEO_APPID = "nav_here_maps_geo_appid";
    public static final String NAVIGATION_HERE_GEO_APPCODE = "nav_here_maps_geo_appcode";
    public static final String NAVIGATION_HERE_APPID = "nav_here_maps_appid";
    public static final String NAVIGATION_HERE_APPCODE = "nav_here_maps_appcode";
    public static final String NAVIGATION_HERE_LICENCE = "nav_here_maps_licence";
    public static final String NB_CONFIG = "nb_config";
    public static final int NB_CONFIG_DEFAULT = 0;
    public static final String NEW_BOX = "New_Box";
    public static final String NEW_BOX_PLUS_MOUNTS = "New_Box_Plus_Mounts";
    public static final String OLD_BOX = "Old_Box";
    public static final String OTA_AUTO_DOWNLOAD = "ota_auto_download";
    public static final String OTA_BUILD_TYPE_DEFAULT;
    public static final String OTA_DESCRIPTION = "OTA_DESCRIPTION";
    public static final String OTA_DESCRIPTION_DEFAULT = "";
    public static final String OTA_FORCE_FULL_UPDATE = "FORCE_FULL_UPDATE";
    public static final boolean OTA_FORCE_FULL_UPDATE_DEFAULT = false;
    public static final String OTA_FROM_VERSION = "OTA_FROM_VERSION";
    public static final int OTA_FROM_VERSION_DEFAULT = 0;
    public static final String OTA_IS_INCREMENTAL = "OTA_IS_INCREMENTAL";
    public static final boolean OTA_IS_INCREMENTAL_DEFAULT = false;
    public static final String OTA_META_DATA = "OTA_META_DATA";
    public static final String OTA_SERIAL_NUM = "ota_serial_number";
    public static final String OTA_SIZE = "OTA_SIZE";
    public static final long OTA_SIZE_DEFAULT = 0L;
    public static final String OTA_STATUS = "OTA_STATUS";
    public static final String OTA_STATUS_DEFAULT;
    public static final String OTA_URL = "OTA_URL";
    public static final String OTA_URL_DEFAULT = "";
    public static final String OTA_VERSION = "OTA_VERSION";
    public static final int OTA_VERSION_DEFAULT = -1;
    public static final String OTA_VERSION_NAME = "OTA_VERSION_NAME";
    public static final String OTA_VERSION_NAME_DEFAULT = "";
    public static final String PENDING_SENSOR_DATA_EXIST = "pending_sensor_data_exist";
    public static final String PENDING_TICKETS_EXIST = "pending_tickets_exist";
    public static final String PERMISSIONS = "permissions";
    public static final String PERMISSIONS_VERSION = "permissions_version";
    public static final int PERMISSIONS_VERSION_DEFAULT = 1;
    public static final String POWER_CABLE_SELECTION = "power_cable_selection";
    public static final String POWER_CABLE_SELECTION_CLA = "CLA";
    public static final String POWER_CABLE_SELECTION_DEFAULT = "OBD";
    public static final String POWER_CABLE_SELECTION_OBD = "OBD";
    public static final String RECOMMENDATIONS_ENABLED = "recommendations_enabled";
    public static final String CONNECTION_NOTIFICATION_ENABLED = "connection_notification";
    public static final boolean RECOMMENDATIONS_ENABLED_DEFAULT = true;
    public static final String RELEASE_NOTES = "release_notes";
    public static final String SCREENSHOT = "screenshot";
    public static final long SERIAL_NUM_DEFAULT = 0L;
    public static final String SETTINGS = "settings";
    public static final String SHARED_PREF_VERSION = "shared_pref_version";
    public static final int SHARED_PREF_VERSION_DEFAULT = 0;
    public static final String SHOULD_ASK_WHAT_MOUNT_WORKED = "should_ask_what_mount_worked";
    public static final boolean SHOULD_ASK_WHAT_MOUNT_WORKED_DEFAULT = false;
    public static final String TICKETS_AWAITING_HUD_LOG_EXIST = "tickets_awaiting_hud_log_exist";
    public static final String USER_ALREADY_SAW_ADD_HOME_TIP = "user_already_saw_add_home_tip";
    public static final boolean USER_ALREADY_SAW_ADD_HOME_TIP_DEFAULT = false;
    public static final String USER_ALREADY_SAW_ADD_WORK_TIP = "user_already_saw_add_work_tip";
    public static final boolean USER_ALREADY_SAW_ADD_WORK_TIP_DEFAULT = false;
    public static final String USER_ALREADY_SAW_GESTURE_TIP = "user_already_saw_gesture_tip";
    public static final boolean USER_ALREADY_SAW_GESTURE_TIP_DEFAULT = false;
    public static final String USER_ALREADY_SAW_GOOGLE_NOW_TIP = "user_already_saw_google_now_tip";
    public static final boolean USER_ALREADY_SAW_GOOGLE_NOW_TIP_DEFAULT = false;
    public static final String USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP = "user_already_saw_music_tip";
    public static final boolean USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP_DEFAULT = false;
    public static final String USER_ALREADY_SAW_MICROPHONE_TIP = "user_already_saw_microphone_tip";
    public static final boolean USER_ALREADY_SAW_MICROPHONE_TIP_DEFAULT = false;
    public static final String USER_ENABLED_GLANCES_ONCE_BEFORE = "user_enabled_glances_once_before";
    public static final boolean USER_ENABLED_GLANCES_ONCE_BEFORE_DEFAULT = false;
    public static final String USER_TRIED_GESTURES_ONCE_BEFORE = "user_tried_gestures_once_before";
    public static final boolean USER_TRIED_GESTURES_ONCE_BEFORE_DEFAULT = false;
    public static final String USER_WATCHED_THE_DEMO = "user_watched_the_demo";
    public static final boolean USER_WATCHED_THE_DEMO_DEFAULT = false;
    public static final String USE_EXPERIMENTAL_HUD_VOICE_ENABLED = "use_experimental_hud_voice_enabled";
    public static final boolean USE_EXPERIMENTAL_HUD_VOICE_ENABLED_DEFAULT = false;
    public static final String USER_SUBSCRIPTION_ASKED = "user_asked_subscription";
    public static final String USER_SUBSCRIPTION_ASKED_DATE = "user_subscription_asked_data";
    public static final String USER_SUBSCRIPTION_RESPONSE = "user_subscription_response";

    static {
        HUD_OBD_ON_DEFAULT = DriverProfilePreferences.ObdScanSetting.SCAN_DEFAULT_OFF.toString();
        FEATURE_MODE_DEFAULT = DriverProfilePreferences.FeatureMode.FEATURE_MODE_RELEASE.toString();
        AUDIO_OUTPUT_PREFERENCE_DEFAULT = TTSAudioRouter.AudioOutput.BEST_AVAILABLE.ordinal();
        OTA_BUILD_TYPE_DEFAULT = S3Constants.BuildType.user.name();
        OTA_STATUS_DEFAULT = null;
        DIAL_LONG_PRESS_ACTION_DEFAULT = DriverProfilePreferences.DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT.getValue();
    }

    @DexReplace
    public static class ProfilePreferences
    {
        public static final String CAR_MAKE = "vehicle-make";
        public static final String CAR_MANUAL_ENTRY = "vehicle-data-entry-manual";
        public static final String CAR_MODEL = "vehicle-model";
        public static final String CAR_OBD_LOCATION_ACCESS_NOTE = "car_obd_location_access_note";
        public static final String CAR_OBD_LOCATION_NOTE = "car_obd_location_note";
        public static final String CAR_OBD_LOCATION_NUM = "CAR_OBD_LOCATION_NUM";
        public static final String CAR_YEAR = "vehicle-year";
        public static final String CUSTOMER_PREF_FILE = "tracker";
        public static final String EMAIL = "email";
        public static final String FULL_NAME = "fname";
        public static final String OBD_IMAGE_FILE_NAME = "obdImage";
        public static final String SERIAL_NUM = "profile_serial_number";
        public static final Long SERIAL_NUM_DEFAULT;
        public static final String SKIPPED_SIGNUP = "skipped";
        public static final String UNIT_SYSTEM = "unit-system";
        public static final String UNIT_SYSTEM_DEFAULT = "";
        public static final String USER_PHOTO_FILE_NAME = "userPhoto";

        static {
            SERIAL_NUM_DEFAULT = 1L;
        }
    }
}
