package com.navdy.client.app.ui.base;

import android.annotation.SuppressLint;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class BaseHereMapFragment extends com.here.android.mpa.mapping.MapFragment {
    @DexIgnore
    private static /* final */ int CHECK_DIMENSIONS_INTERVAL; // = 100;
    @DexIgnore
    private static /* final */ float DEFAULT_ORIENTATION; // = 0.0f;
    @DexIgnore
    private static /* final */ float DEFAULT_TILT; // = 0.0f;
    @DexIgnore
    private static /* final */ double DEFAULT_ZOOM; // = 14.0d;
    @DexIgnore
    private static /* final */ java.lang.String HERE_MAP_SCHEME; // = "normal.day";
    @DexIgnore
    private static /* final */ java.lang.String HERE_MAP_THREAD_TAG; // = "here-map-fragment-";
    @DexIgnore
    private static /* final */ int POS_INDICATOR_Z_INDEX; // = com.navdy.client.app.NavdyApplication.getAppContext().getResources().getInteger(com.navdy.client.R.integer.position_indicator_z_index);
    @DexIgnore
    private static /* final */ java.util.concurrent.atomic.AtomicInteger hereMapFragmentThreadCounter; // = new java.util.concurrent.atomic.AtomicInteger(1);
    @DexIgnore
    private static /* final */ com.navdy.service.library.log.Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.base.BaseHereMapFragment.class);
    @DexIgnore
    private static com.here.android.mpa.common.Image markerImg;
    @DexIgnore
    private /* final */ java.util.Queue<com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentAttached> attachCompleteListeners;
    @DexIgnore
    private /* final */ android.os.Handler backgroundThread;
    @DexIgnore
    private /* final */ com.navdy.client.app.framework.util.BusProvider bus;
    @DexIgnore
    private /* final */ java.lang.Runnable checkDimensions; // = new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon2();
    @DexIgnore
    private com.navdy.client.app.ui.base.BaseHereMapFragment.Error currentError;
    @DexIgnore
    private com.here.android.mpa.mapping.MapMarker currentPositionMarker;
    @DexIgnore
    private com.navdy.client.app.ui.base.BaseHereMapFragment.State currentState;
    /* access modifiers changed from: private */
    // @android.support.annotation.Nullable
    @DexIgnore
    public android.graphics.PointF currentTransformCenter;
    /* access modifiers changed from: private */
    // @android.support.annotation.Nullable
    @DexIgnore
    public com.here.android.mpa.common.ViewRect currentViewRect;
    @DexIgnore
    private /* final */ com.here.android.mpa.common.OnEngineInitListener fragmentInitListener; // = new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon3();
    @DexIgnore
    private com.here.android.mpa.mapping.Map hereMap;
    @DexIgnore
    private /* final */ java.util.Queue<com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized> initCompleteListeners;
    @DexIgnore
    private boolean isAttached;
    @DexIgnore
    private /* final */ com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener navdyLocationListener; // = new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon4();
    @DexIgnore
    private /* final */ com.navdy.client.app.framework.location.NavdyLocationManager navdyLocationManager;
    @DexIgnore
    private /* final */ java.util.Queue<com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady> readyCompleteListeners;
    @DexIgnore
    private /* final */ android.os.Handler uiThread; // = new android.os.Handler(android.os.Looper.getMainLooper());

    @DexIgnore
    static class Anon1 implements com.here.android.mpa.common.OnEngineInitListener {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            com.navdy.client.app.ui.base.BaseHereMapFragment.markerImg = new com.here.android.mpa.common.Image();
            try {
                com.navdy.client.app.ui.base.BaseHereMapFragment.markerImg.setImageResource(com.navdy.client.R.drawable.icon_user_location);
            } catch (java.io.IOException e) {
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.e("could not initialize map marker img", e);
            }
        }
    }

    @DexIgnore
    class Anon10 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady {
        @DexIgnore
        Anon10() {
        }

        @DexIgnore
        /* final */ /* synthetic */ com.here.android.mpa.mapping.Map.Animation val$animation;
        @DexIgnore
        /* final */ /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;

        @DexIgnore
        Anon10(com.navdy.client.app.framework.models.Destination destination, com.here.android.mpa.mapping.Map.Animation animation) {
            this.val$destination = destination;
            this.val$animation = animation;
        }

        @DexIgnore
        public void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.here.android.mpa.common.GeoCoordinate destinationGeo;
            com.navdy.service.library.events.location.Coordinate location = com.navdy.client.app.ui.base.BaseHereMapFragment.this.navdyLocationManager.getSmartStartCoordinates();
            if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(this.val$destination.displayLat, this.val$destination.displayLong)) {
                destinationGeo = new com.here.android.mpa.common.GeoCoordinate(this.val$destination.displayLat, this.val$destination.displayLong);
            } else {
                destinationGeo = new com.here.android.mpa.common.GeoCoordinate(this.val$destination.navigationLat, this.val$destination.navigationLong);
            }
            if (location != null) {
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.centerOnTwoPoints(new com.here.android.mpa.common.GeoCoordinate(location.latitude.doubleValue(), location.longitude.doubleValue()), destinationGeo, this.val$animation);
                return;
            }
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.move(destinationGeo, this.val$animation);
        }

        @DexIgnore
        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    class Anon11 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady {
        @DexIgnore
        Anon11() {
        }

        @DexIgnore
        /* final */ /* synthetic */ com.here.android.mpa.mapping.Map.Animation val$animation;
        @DexIgnore
        /* final */ /* synthetic */ com.here.android.mpa.common.GeoPolyline val$route;

        @DexIgnore
        Anon11(com.here.android.mpa.common.GeoPolyline geoPolyline, com.here.android.mpa.mapping.Map.Animation animation) {
            this.val$route = geoPolyline;
            this.val$animation = animation;
        }

        @DexIgnore
        public void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.here.android.mpa.common.GeoBoundingBox geoBoundingBox = this.val$route.getBoundingBox();
            com.here.android.mpa.mapping.Map.Animation sanitizedAnimation = this.val$animation != null ? this.val$animation : com.here.android.mpa.mapping.Map.Animation.NONE;
            if (hereMap.getHeight() > 0 && hereMap.getWidth() > 0) {
                if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentViewRect != null) {
                    hereMap.zoomTo(geoBoundingBox, com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentViewRect, sanitizedAnimation, 0.0f);
                } else {
                    hereMap.zoomTo(geoBoundingBox, sanitizedAnimation, 0.0f);
                }
            }
        }

        @DexIgnore
        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    class Anon12 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady {
        @DexIgnore
        Anon12() {
        }

        @DexIgnore
        /* final */ /* synthetic */ int val$paddingBottom;
        @DexIgnore
        /* final */ /* synthetic */ int val$paddingLeft;
        @DexIgnore
        /* final */ /* synthetic */ int val$paddingRight;
        @DexIgnore
        /* final */ /* synthetic */ int val$paddingTop;

        @DexIgnore
        Anon12(int i, int i2, int i3, int i4) {
            this.val$paddingTop = i;
            this.val$paddingBottom = i2;
            this.val$paddingLeft = i3;
            this.val$paddingRight = i4;
        }

        @DexIgnore
        public void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            int width = hereMap.getWidth();
            int height = hereMap.getHeight();
            if (this.val$paddingTop + this.val$paddingBottom >= height || this.val$paddingLeft + this.val$paddingRight >= width) {
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.w("invalid paddings. width=" + width + " height=" + height + " paddingTop=" + this.val$paddingTop + " paddingBottom=" + this.val$paddingBottom + " paddingLeft=" + this.val$paddingLeft + " paddingRight=" + this.val$paddingRight);
                return;
            }
            android.graphics.PointF transformCenter = hereMap.getTransformCenter();
            android.graphics.PointF newTransformCenter = new android.graphics.PointF((transformCenter.x + ((float) ((int) (((double) this.val$paddingLeft) / 2.0d)))) - ((float) ((int) (((double) this.val$paddingRight) / 2.0d))), (transformCenter.y + ((float) ((int) (((double) this.val$paddingTop) / 2.0d)))) - ((float) ((int) (((double) this.val$paddingBottom) / 2.0d))));
            hereMap.setTransformCenter(newTransformCenter);
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentViewRect = new com.here.android.mpa.common.ViewRect(this.val$paddingLeft, this.val$paddingTop, (width - this.val$paddingLeft) - this.val$paddingRight, (height - this.val$paddingTop) - this.val$paddingBottom);
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentTransformCenter = newTransformCenter;
        }

        @DexIgnore
        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    @DexEdit(defaultAction = DexAction.ADD)
    class Anon13 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        @DexIgnore
        // $FF: synthetic field
        final BaseHereMapFragment this$Anon0;

        @DexIgnore
        Anon13() {
            this$Anon0 = null;
        }

        @DexIgnore
        /* final */ /* synthetic */ com.here.android.mpa.mapping.MapGesture.OnGestureListener val$listener;

        @DexIgnore
        Anon13(BaseHereMapFragment var1, com.here.android.mpa.mapping.MapGesture.OnGestureListener onGestureListener) {
            this$Anon0 = var1;
            this.val$listener = onGestureListener;
        }

        @DexReplace
        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.here.android.mpa.mapping.MapGesture mapGesture = this$Anon0.getMapGesture();
            if (mapGesture != null) {
                // mapGesture.addOnGestureListener(this.val$listener);
                mapGesture.addOnGestureListener(this.val$listener, 1, true);
            } else {
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.w("could not add map gesture due MapGesture being null");
            }
        }

        @DexIgnore
        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
            com.navdy.client.app.ui.base.BaseHereMapFragment.logger.e("could not add map gesture due to error: " + error.name());
        }
    }

    @DexIgnore
    class Anon14 implements com.here.android.mpa.common.OnEngineInitListener {

        @DexIgnore
        class Anon1 implements java.lang.Runnable {
            @DexIgnore
            Anon1() {
            }

            @DexIgnore
            public void run() {
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.init(com.navdy.client.app.ui.base.BaseHereMapFragment.this.fragmentInitListener);
            }
        }

        @DexIgnore
        Anon14() {
        }

        @DexIgnore
        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.backgroundThread.post(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon14.Anon1());
        }
    }

    @DexIgnore
    class Anon15 implements java.lang.Runnable {
        @DexIgnore
        Anon15() {
        }

        @DexIgnore
        /* final */ /* synthetic */ com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentAttached val$attachListener;

        @DexIgnore
        Anon15(com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentAttached onHereMapFragmentAttached) {
            this.val$attachListener = onHereMapFragmentAttached;
        }

        @DexIgnore
        public void run() {
            if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.isAdded()) {
                this.val$attachListener.onAttached();
            } else if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.isRemoving()) {
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.w("whenAttached, isRemoving, no-op");
            } else {
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.v("queueing up a whenAttachListener");
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.attachCompleteListeners.add(this.val$attachListener);
            }
        }
    }

    @DexIgnore
    class Anon16 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        @DexIgnore
        Anon16() {
        }

        @DexIgnore
        /* final */ /* synthetic */ boolean val$active;

        @DexIgnore
        Anon16(boolean z) {
            this.val$active = z;
        }

        @DexIgnore
        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            hereMap.setTrafficInfoVisible(this.val$active);
        }

        @DexIgnore
        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    class Anon2 implements java.lang.Runnable {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        public void run() {
            if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap == null) {
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.e("checkDimensions, hereMap is null!");
            } else if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap.getWidth() == 0 && com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap.getHeight() == 0) {
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.backgroundThread.postDelayed(this, 100);
            } else {
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentState = com.navdy.client.app.ui.base.BaseHereMapFragment.State.READY;
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.v("fragment ready, calling readyCompleteListeners");
                while (!com.navdy.client.app.ui.base.BaseHereMapFragment.this.readyCompleteListeners.isEmpty()) {
                    ((com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady) com.navdy.client.app.ui.base.BaseHereMapFragment.this.readyCompleteListeners.poll()).onReady(com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap);
                }
            }
        }
    }

    @DexIgnore
    class Anon3 implements com.here.android.mpa.common.OnEngineInitListener {

        @DexIgnore
        class Anon1 implements java.lang.Runnable {
            @DexIgnore
            /* final */ /* synthetic */ com.here.android.mpa.common.OnEngineInitListener.Error val$error;

            @DexIgnore
            Anon1(com.here.android.mpa.common.OnEngineInitListener.Error error) {
                this.val$error = error;
            }

            @DexIgnore
            public void run() {
                if (this.val$error != com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                    com.navdy.client.app.ui.base.BaseHereMapFragment.logger.e("Could not initialize HereMapFragment: " + this.val$error.name());
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentState = com.navdy.client.app.ui.base.BaseHereMapFragment.State.FAILED;
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentError = com.navdy.client.app.ui.base.BaseHereMapFragment.Error.MAP_ENGINE_ERROR;
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.callOnErrors(com.navdy.client.app.ui.base.BaseHereMapFragment.Error.MAP_ENGINE_ERROR);
                    return;
                }
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap = com.navdy.client.app.ui.base.BaseHereMapFragment.this.getMap();
                if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap == null) {
                    com.navdy.client.app.ui.base.BaseHereMapFragment.logger.e("hereMap is null at fragment init");
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentState = com.navdy.client.app.ui.base.BaseHereMapFragment.State.FAILED;
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentError = com.navdy.client.app.ui.base.BaseHereMapFragment.Error.MAP_ENGINE_ERROR;
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.callOnErrors(com.navdy.client.app.ui.base.BaseHereMapFragment.Error.NO_MAP_ERROR);
                    return;
                }
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.initMap();
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentState = com.navdy.client.app.ui.base.BaseHereMapFragment.State.INITIALIZED;
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.v("fragment initialized, calling initCompleteListeners");
                while (true) {
                    if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.initCompleteListeners.isEmpty()) {
                        break;
                    } else if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap == null) {
                        com.navdy.client.app.ui.base.BaseHereMapFragment.logger.e("Calling initCompleteListeners.onInit with a null hereMap!");
                        break;
                    } else {
                        ((com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized) com.navdy.client.app.ui.base.BaseHereMapFragment.this.initCompleteListeners.poll()).onInit(com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap);
                    }
                }
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.backgroundThread.postDelayed(com.navdy.client.app.ui.base.BaseHereMapFragment.this.checkDimensions, 100);
            }
        }

        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.backgroundThread.post(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon3.Anon1(error));
        }
    }

    @DexIgnore
    class Anon4 implements com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener {

        @DexIgnore
        class Anon1 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
            @DexIgnore
            /* final */ /* synthetic */ com.navdy.service.library.events.location.Coordinate val$phoneLocation;

            @DexIgnore
            Anon1(com.navdy.service.library.events.location.Coordinate coordinate) {
                this.val$phoneLocation = coordinate;
            }

            @DexIgnore
            public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
                if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentPositionMarker == null) {
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentPositionMarker = com.navdy.client.app.ui.base.BaseHereMapFragment.this.initPositionMarker(this.val$phoneLocation);
                    if (com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentPositionMarker != null) {
                        hereMap.addMapObject(com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentPositionMarker);
                        return;
                    }
                    return;
                }
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentPositionMarker.setCoordinate(new com.here.android.mpa.common.GeoCoordinate(this.val$phoneLocation.latitude.doubleValue(), this.val$phoneLocation.longitude.doubleValue()));
            }

            @DexIgnore
            public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
            }
        }

        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        public void onPhoneLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate phoneLocation) {
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.whenInitialized(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon4.Anon1(phoneLocation));
        }

        @DexIgnore
        public void onCarLocationChanged(@android.support.annotation.NonNull com.navdy.service.library.events.location.Coordinate carLocation) {
        }
    }

    @DexIgnore
    class Anon5 implements java.lang.Runnable {
        @DexIgnore
        Anon5() {
        }

        @DexIgnore
        /* final */ /* synthetic */ com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized val$initListener;

        @DexIgnore
        Anon5(com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized onHereMapFragmentInitialized) {
            this.val$initListener = onHereMapFragmentInitialized;
        }

        @DexIgnore
        public void run() {
            switch (com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentState) {
                case READY:
                case INITIALIZED:
                    this.val$initListener.onInit(com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap);
                    return;
                case FAILED:
                    this.val$initListener.onError(com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentError);
                    return;
                default:
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.initCompleteListeners.add(this.val$initListener);
                    return;
            }
        }
    }

    @DexIgnore
    class Anon6 implements java.lang.Runnable {
        @DexIgnore
        Anon6() {
        }

        @DexIgnore
        /* final */ /* synthetic */ com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady val$readyListener;

        @DexIgnore
        Anon6(com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady onHereMapFragmentReady) {
            this.val$readyListener = onHereMapFragmentReady;
        }

        @DexIgnore
        public void run() {
            switch (com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentState) {
                case READY:
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.setupTransformCenter();
                    this.val$readyListener.onReady(com.navdy.client.app.ui.base.BaseHereMapFragment.this.hereMap);
                    return;
                case FAILED:
                    this.val$readyListener.onError(com.navdy.client.app.ui.base.BaseHereMapFragment.this.currentError);
                    return;
                default:
                    com.navdy.client.app.ui.base.BaseHereMapFragment.this.readyCompleteListeners.add(this.val$readyListener);
                    return;
            }
        }
    }

    @DexIgnore
    class Anon7 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentAttached {
        @DexIgnore
        Anon7() {
        }

        @SuppressLint("ResourceType")
        @DexIgnore
        public void onAttached() {
            com.navdy.client.app.ui.base.BaseHereMapFragment.logger.v("show");
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.getFragmentManager().beginTransaction().setCustomAnimations(17498112, 17498113).show(com.navdy.client.app.ui.base.BaseHereMapFragment.this).commitAllowingStateLoss();
        }
    }

    @DexIgnore
    class Anon8 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentAttached {
        @DexIgnore
        Anon8() {
        }

        @DexIgnore
        public void onAttached() {
            com.navdy.client.app.ui.base.BaseHereMapFragment.logger.v("hide");
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.getFragmentManager().beginTransaction().hide(com.navdy.client.app.ui.base.BaseHereMapFragment.this).commitAllowingStateLoss();
            com.navdy.client.app.ui.base.BaseHereMapFragment.this.getFragmentManager().executePendingTransactions();
        }
    }

    @DexIgnore
    class Anon9 implements com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized {
        @DexIgnore
        Anon9() {
        }

        @DexIgnore
        /* final */ /* synthetic */ com.here.android.mpa.mapping.Map.Animation val$animation;

        @DexIgnore
        Anon9(com.here.android.mpa.mapping.Map.Animation animation) {
            this.val$animation = animation;
        }

        @DexIgnore
        public void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map hereMap) {
            com.navdy.service.library.events.location.Coordinate phoneLocation = com.navdy.client.app.ui.base.BaseHereMapFragment.this.navdyLocationManager.getPhoneCoordinates();
            com.navdy.service.library.events.location.Coordinate carLocation = com.navdy.client.app.ui.base.BaseHereMapFragment.this.navdyLocationManager.getCarCoordinates();
            com.here.android.mpa.common.GeoCoordinate newCenter = null;
            if (phoneLocation != null) {
                newCenter = new com.here.android.mpa.common.GeoCoordinate(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue());
            } else if (carLocation != null) {
                newCenter = new com.here.android.mpa.common.GeoCoordinate(carLocation.latitude.doubleValue(), carLocation.longitude.doubleValue());
            }
            if (newCenter != null) {
                com.navdy.client.app.ui.base.BaseHereMapFragment.this.move(newCenter, this.val$animation);
            } else {
                com.navdy.client.app.ui.base.BaseHereMapFragment.logger.w("could not center on user location, no location");
            }
        }

        @DexIgnore
        public void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    public enum Error {
        MAP_ENGINE_ERROR,
        NO_MAP_ERROR
    }

    @DexIgnore
    private interface OnHereMapFragmentAttached {
        void onAttached();
    }

    @DexIgnore
    public interface OnHereMapFragmentInitialized {
        void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error);

        void onInit(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map map);
    }

    @DexIgnore
    public interface OnHereMapFragmentReady {
        void onError(@android.support.annotation.NonNull com.navdy.client.app.ui.base.BaseHereMapFragment.Error error);

        void onReady(@android.support.annotation.NonNull com.here.android.mpa.mapping.Map map);
    }

    @DexIgnore
    private enum State {
        INITIALIZING,
        INITIALIZED,
        READY,
        FAILED
    }

    /* @DexIgnore
    static {
        initMarkerImage();
    } */

    @DexIgnore
    private static void initMarkerImage() {
        com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon1());
    }

    @DexIgnore
    public BaseHereMapFragment() {
        android.os.HandlerThread background = new android.os.HandlerThread(HERE_MAP_THREAD_TAG + hereMapFragmentThreadCounter.getAndIncrement());
        background.start();
        this.backgroundThread = new android.os.Handler(background.getLooper());
        this.navdyLocationManager = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
        this.bus = com.navdy.client.app.framework.util.BusProvider.getInstance();
        this.attachCompleteListeners = new java.util.LinkedList();
        this.initCompleteListeners = new java.util.LinkedList();
        this.readyCompleteListeners = new java.util.LinkedList();
        this.currentState = com.navdy.client.app.ui.base.BaseHereMapFragment.State.INITIALIZING;
        this.currentError = null;
        this.isAttached = false;
    }

    @DexIgnore
    public void whenInitialized(com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized initListener) {
        if (initListener == null) {
            logger.w("tried to add a null initListener");
        } else {
            this.backgroundThread.post(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon5(initListener));
        }
    }

    @DexIgnore
    public void whenReady(com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady readyListener) {
        if (readyListener == null) {
            logger.w("tried to add a null readyListener");
        } else {
            this.backgroundThread.post(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon6(readyListener));
        }
    }

    @DexIgnore
    public void show() {
        whenAttached(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon7());
    }

    @DexIgnore
    public void hide() {
        whenAttached(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon8());
    }

    @DexIgnore
    public void centerOnUserLocation() {
        centerOnUserLocation(null);
    }

    @DexIgnore
    public void centerOnUserLocation(@android.support.annotation.Nullable com.here.android.mpa.mapping.Map.Animation animation) {
        whenInitialized(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon9(animation));
    }

    @DexIgnore
    public void centerOnUserLocationAndDestination(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
        centerOnUserLocationAndDestination(destination, null);
    }

    @DexIgnore
    public void centerOnUserLocationAndDestination(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination, @android.support.annotation.Nullable com.here.android.mpa.mapping.Map.Animation animation) {
        whenReady(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon10(destination, animation));
    }

    @DexIgnore
    public void centerOnRoute(@android.support.annotation.NonNull com.here.android.mpa.common.GeoPolyline route) {
        centerOnRoute(route, null);
    }

    @DexIgnore
    public void centerOnRoute(@android.support.annotation.NonNull com.here.android.mpa.common.GeoPolyline route, @android.support.annotation.Nullable com.here.android.mpa.mapping.Map.Animation animation) {
        whenReady(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon11(route, animation));
    }

    @DexIgnore
    public void setUsableArea(int paddingTop, int paddingRight, int paddingBottom, int paddingLeft) {
        whenReady(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon12(paddingTop, paddingBottom, paddingLeft, paddingRight));
    }

    @DexIgnore
    public void setMapGesture(com.here.android.mpa.mapping.MapGesture.OnGestureListener listener) {
        whenInitialized(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon13(this, listener));
    }

    // @com.squareup.otto.Subscribe
    @DexIgnore
    public void onLimitCellDataEvent(com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent event) {
        toggleTraffic(!event.hasLimitedCellData);
    }

    @DexIgnore
    public void onAttach(android.content.Context context) {
        super.onAttach(context);
        this.isAttached = true;
        this.bus.register(this);
        logger.v("onAttach, attachCompleteListeners executing");
        while (!this.attachCompleteListeners.isEmpty()) {
            ((com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentAttached) this.attachCompleteListeners.poll()).onAttached();
        }
    }

    @DexIgnore
    public android.view.View onCreateView(android.view.LayoutInflater layoutInflater, android.view.ViewGroup viewGroup, android.os.Bundle bundle) {
        com.navdy.client.app.framework.map.HereMapsManager.getInstance().addOnInitializedListener(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon14());
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        this.navdyLocationManager.addListener(this.navdyLocationListener);
    }

    @DexIgnore
    public void onPause() {
        this.navdyLocationManager.removeListener(this.navdyLocationListener);
        super.onPause();
    }

    @DexIgnore
    public void onDetach() {
        if (this.isAttached) {
            this.bus.unregister(this);
        }
        this.isAttached = false;
        super.onDetach();
    }

    @DexIgnore
    private void whenAttached(com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentAttached attachListener) {
        if (attachListener == null) {
            logger.w("tried to add a null attachListener");
        } else {
            this.uiThread.post(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon15(attachListener));
        }
    }

    @DexIgnore
    private void centerOnTwoPoints(@android.support.annotation.NonNull com.here.android.mpa.common.GeoCoordinate startGeoCoordinates, @android.support.annotation.NonNull com.here.android.mpa.common.GeoCoordinate endGeoCoordinates, @android.support.annotation.Nullable com.here.android.mpa.mapping.Map.Animation animation) {
        double maxLat;
        double maxLng;
        double minLat;
        double minLng;
        if (!isValid(startGeoCoordinates) || !isValid(endGeoCoordinates)) {
            logger.w("called centerOnTwoPoints and coordinates are invalid, no-op. startGeoCoordinates=" + startGeoCoordinates.getLatitude() + "," + startGeoCoordinates.getLongitude() + "; endGeoCoordinates=" + endGeoCoordinates.getLatitude() + "," + endGeoCoordinates.getLongitude());
            return;
        }
        double startLat = startGeoCoordinates.getLatitude();
        double startLng = startGeoCoordinates.getLongitude();
        double endLat = endGeoCoordinates.getLatitude();
        double endLng = endGeoCoordinates.getLongitude();
        if (startLat > endLat) {
            maxLat = startLat;
        } else {
            maxLat = endLat;
        }
        if (startLng > endLng) {
            maxLng = startLng;
        } else {
            maxLng = endLng;
        }
        if (startLat < endLat) {
            minLat = startLat;
        } else {
            minLat = endLat;
        }
        if (startLng < endLng) {
            minLng = startLng;
        } else {
            minLng = endLng;
        }
        com.here.android.mpa.common.GeoBoundingBox geoBoundingBox = new com.here.android.mpa.common.GeoBoundingBox(new com.here.android.mpa.common.GeoCoordinate(maxLat, minLng), new com.here.android.mpa.common.GeoCoordinate(minLat, maxLng));
        com.here.android.mpa.mapping.Map.Animation appliedAnimation = animation != null ? animation : com.here.android.mpa.mapping.Map.Animation.NONE;
        if (this.hereMap.getHeight() > 0 && this.hereMap.getWidth() > 0) {
            if (this.currentViewRect != null) {
                this.hereMap.zoomTo(geoBoundingBox, this.currentViewRect, animation, 0.0f);
            } else {
                this.hereMap.zoomTo(geoBoundingBox, appliedAnimation, 0.0f);
            }
        }
    }

    @DexIgnore
    private void move(com.here.android.mpa.common.GeoCoordinate geo, @android.support.annotation.Nullable com.here.android.mpa.mapping.Map.Animation animation) {
        if (animation == null) {
            animation = com.here.android.mpa.mapping.Map.Animation.NONE;
        }
        this.hereMap.setCenter(geo, animation, (double) DEFAULT_ZOOM, 0.0f, 0.0f);
    }

    @DexIgnore
    private void initMap() {
        this.hereMap.setMapScheme("normal.day");
        centerOnUserLocation();
        addPositionIndicator();
        toggleTraffic(!com.navdy.client.app.ui.settings.SettingsUtils.isLimitingCellularData());
    }

    @DexIgnore
    private void callOnErrors(com.navdy.client.app.ui.base.BaseHereMapFragment.Error error) {
        while (!this.initCompleteListeners.isEmpty()) {
            ((com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized) this.initCompleteListeners.poll()).onError(error);
        }
    }

    @DexIgnore
    private void addPositionIndicator() {
        if (this.currentPositionMarker == null) {
            this.currentPositionMarker = initPositionMarker(this.navdyLocationManager.getPhoneCoordinates());
            if (this.currentPositionMarker != null) {
                this.hereMap.addMapObject(this.currentPositionMarker);
            }
        }
    }

    @DexIgnore
    private boolean isValid(com.here.android.mpa.common.GeoCoordinate geoCoordinate) {
        return (geoCoordinate == null || (geoCoordinate.getLatitude() == 0.0d && geoCoordinate.getLongitude() == 0.0d)) ? false : true;
    }

    @DexIgnore
    private void toggleTraffic(boolean active) {
        whenInitialized(new com.navdy.client.app.ui.base.BaseHereMapFragment.Anon16(active));
    }

    /* access modifiers changed from: private */
    // @android.support.annotation.Nullable
    @DexIgnore
    public com.here.android.mpa.mapping.MapMarker initPositionMarker(@android.support.annotation.Nullable com.navdy.service.library.events.location.Coordinate phoneLocation) {
        if (phoneLocation == null) {
            return null;
        }
        if (markerImg == null) {
            logger.e("markerImg is null and it shouldn't be!");
            return null;
        }
        com.here.android.mpa.mapping.MapMarker positionMarker = new com.here.android.mpa.mapping.MapMarker(new com.here.android.mpa.common.GeoCoordinate(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()), markerImg);
        positionMarker.setZIndex(POS_INDICATOR_Z_INDEX);
        return positionMarker;
    }

    @DexIgnore
    private void setupTransformCenter() {
        if (this.hereMap != null && !this.hereMap.getTransformCenter().equals(this.currentTransformCenter)) {
            this.hereMap.setTransformCenter(this.currentTransformCenter);
        }
    }
}
