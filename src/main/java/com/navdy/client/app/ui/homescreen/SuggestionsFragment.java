package com.navdy.client.app.ui.homescreen;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapPolyline;
import com.navdy.client.R;
import com.navdy.client.R.dimen;
import com.navdy.client.R.drawable;
import com.navdy.client.R.id;
import com.navdy.client.R.layout;
import com.navdy.client.R.string;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Suggestion.SuggestionType;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.client.app.framework.util.CustomItemClickListener;
import com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener;
import com.navdy.client.app.framework.util.GmsUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener;
import com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback;
import com.navdy.client.app.tracking.SetDestinationTracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.client.app.ui.UiUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment;
import com.navdy.client.app.ui.base.BaseHereMapFragment;
import com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized;
import com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import com.navdy.client.app.ui.customviews.TripCardView;
import com.navdy.client.app.ui.details.DetailsActivity;
import com.navdy.client.app.ui.search.DropPinActivity;
import com.navdy.client.app.ui.search.SearchActivity;
import com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES;
import com.navdy.client.app.ui.settings.GestureDialogActivity;
import com.navdy.client.app.ui.settings.OtaSettingsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus;
import com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.client.ota.OTAUpdateService.State;
import com.navdy.client.ota.OTAUpdateServiceInterface;
import com.navdy.client.ota.OTAUpdateUIClient;
import com.navdy.client.ota.OTAUpdateUIClient.Error;
import com.navdy.client.ota.model.UpdateInfo;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;


@DexEdit(defaultAction = DexAction.IGNORE)
public class SuggestionsFragment extends BaseSupportFragment implements OTAUpdateUIClient, ServiceConnection, NavdyRouteListener, SuggestionBuildListener {
    @DexIgnore
    private static /* final */ int HERE_PADDING; // = com.navdy.client.app.framework.map.MapUtils.hereMapSidePadding;
    @DexIgnore
    private static /* final */ int HERE_TOP_PADDING;
    @DexIgnore
    private static /* final */ String LAST_ARRIVAL_MARKER_STRING;
    @DexIgnore
    private static /* final */ int MIN_DISTANCE_REBUILD_SUGGESTIONS; // = 500;
    @DexIgnore
    private static /* final */ boolean VERBOSE; // = true;
    @DexIgnore
    private static /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.SuggestionsFragment.class);
    @DexIgnore
    private /* final */ CustomItemClickListener clickListener; // = new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon3();
    @DexIgnore
    private MapMarker destinationMarker;
    @DexIgnore
    private BaseGoogleMapFragment googleMapFragment;
    @DexIgnore
    private BaseHereMapFragment hereMapFragment;
    @DexIgnore
    private Marker lastArrivalMarker;
    @DexIgnore
    private Coordinate lastUpdate;
    @DexIgnore
    private LinearLayoutManager layoutManager;
    @DexIgnore
    private /* final */ OnNavdyLocationChangedListener locationListener; // = new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon1();
    @DexIgnore
    private /* final */ Object lock; // = new java.lang.Object();
    @DexIgnore
    private /* final */ NavdyLocationManager navdyLocationManager; // = com.navdy.client.app.framework.location.NavdyLocationManager.getInstance();
    @DexIgnore
    private /* final */ NavdyRouteHandler navdyRouteHandler; // = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    @DexIgnore
    private RelativeLayout offlineBanner;
    @DexIgnore
    private /* final */ CustomSuggestionsLongItemClickListener onLongClickListener; // = new com.navdy.client.app.ui.homescreen.SuggestionsFragment.Anon2();
    @DexIgnore
    private OTAUpdateServiceInterface otaUpdateService;
    @DexIgnore
    private MapPolyline routePolyline;
    @DexIgnore
    private MapPolyline routeProgressPolyline;
    @DexIgnore
    private SuggestionsFragment.SuggestionScrollListener scrollListener;
    @DexIgnore
    SuggestedDestinationsAdapter suggestionAdapter;
    @DexIgnore
    private WeakReference<SuggestionBuildListener> suggestionListener; // = new java.lang.ref.WeakReference<>(this);
    @DexIgnore
    private RecyclerView suggestionRecycler;

    @DexIgnore
    class Anon1 implements OnNavdyLocationChangedListener {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        public void onPhoneLocationChanged(@NonNull Coordinate phoneLocation) {
            if (SuggestionsFragment.this.suggestionAdapter == null) {
                return;
            }
            if (SuggestionsFragment.this.lastUpdate == null || MapUtils.distanceBetween(phoneLocation, SuggestionsFragment.this.lastUpdate) > 500.0d) {
                SuggestionsFragment.this.lastUpdate = phoneLocation;
                SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
        }

        @DexIgnore
        public void onCarLocationChanged(@NonNull Coordinate carLocation) {
            if (SuggestionsFragment.this.suggestionAdapter == null) {
                return;
            }
            if (SuggestionsFragment.this.lastUpdate == null || MapUtils.distanceBetween(carLocation, SuggestionsFragment.this.lastUpdate) > 500.0d) {
                SuggestionsFragment.this.lastUpdate = carLocation;
                SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
        }
    }

    @DexIgnore
    class Anon10 implements OnHereMapFragmentInitialized {
        @DexIgnore
        Anon10() {
        }

        @DexIgnore
        public void onInit(@NonNull Map hereMap) {
            SuggestionsFragment.this.setDestinationMarker(hereMap, null);
            SuggestionsFragment.this.setRoutePolyline(hereMap, null);
            SuggestionsFragment.this.setProgressPolyline(hereMap, null);
        }

        @DexIgnore
        public void onError(@NonNull BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    class Anon11 implements BaseGoogleMapFragment.OnShowMapListener {
        @DexIgnore
        Anon11() {
        }

        @DexIgnore
        public void onShow() {
            SuggestionsFragment.logger.v("onShow googleMap");
        }
    }

    @DexIgnore
    class Anon2 implements CustomSuggestionsLongItemClickListener {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        public boolean onItemLongClick(View selectedCardRow, int actionModePosition, Suggestion suggestion) {
            HomescreenActivity homescreenActivity = (HomescreenActivity) SuggestionsFragment.this.getActivity();
            if (homescreenActivity == null || homescreenActivity.isFinishing()) {
                return false;
            }
            homescreenActivity.startSelectionActionMode(new SuggestionsFragment.CustomSuggestionsActionCallback(suggestion, homescreenActivity, actionModePosition, selectedCardRow));
            return true;
        }
    }

    @DexIgnore
    class Anon3 implements CustomItemClickListener {

        @DexIgnore
        class Anon1 implements Runnable {
            @DexIgnore
            /* final */ /* synthetic */ HomescreenActivity val$activity;

            @DexIgnore
            Anon1(HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            @DexIgnore
            public void run() {
                this.val$activity.goToGlancesTab();
                SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), GlanceDialogActivity.class));
            }
        }

        @DexIgnore
        class Anon10 implements Runnable {
            @DexIgnore
            /* final */ /* synthetic */ Destination val$destination;
            @DexIgnore
            /* final */ /* synthetic */ SetDestinationTracker val$setDestinationTracker;

            @DexIgnore
            Anon10(SetDestinationTracker setDestinationTracker, Destination destination) {
                this.val$setDestinationTracker = setDestinationTracker;
                this.val$destination = destination;
            }

            @DexIgnore
            public void run() {
                this.val$setDestinationTracker.tagSetDestinationEvent(this.val$destination);
                SuggestionsFragment.this.navdyRouteHandler.requestNewRoute(this.val$destination);
            }
        }

        @DexIgnore
        class Anon2 implements Runnable {
            @DexIgnore
            Anon2() {
            }

            @DexIgnore
            public void run() {
                SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), MicPermissionDialogActivity.class));
            }
        }

        /* renamed from: com.navdy.client.app.ui.homescreen.SuggestionsFragment$Anon3$Anon3 reason: collision with other inner class name */
        // class C0064Anon3 implements Runnable {
        //     C0064Anon3() {
        //     }
        //
        //     public void run() {
        //         Intent i = new Intent(SuggestionsFragment.this.getContext(), InfoDialogActivity.class);
        //         i.putExtra(InfoDialogActivity.EXTRA_LAYOUT, layout.dialog_voice_search);
        //         SuggestionsFragment.this.startActivity(i);
        //     }
        // }

        @DexIgnore
        class Anon4 implements Runnable {
            @DexIgnore
            Anon4() {
            }

            @DexIgnore
            public void run() {
                Intent i = new Intent(SuggestionsFragment.this.getContext(), InfoDialogActivity.class);
                i.putExtra(InfoDialogActivity.EXTRA_LAYOUT, layout.dialog_google_now);
                SuggestionsFragment.this.startActivity(i);
            }
        }

        @DexIgnore
        class Anon5 implements Runnable {
            @DexIgnore
            Anon5() {
            }

            @DexIgnore
            public void run() {
                Intent i = new Intent(SuggestionsFragment.this.getContext(), InfoDialogActivity.class);
                i.putExtra(InfoDialogActivity.EXTRA_LAYOUT, layout.dialog_hud_local_music_browser);
                SuggestionsFragment.this.startActivity(i);
            }
        }

        @DexIgnore
        class Anon6 implements Runnable {
            @DexIgnore
            Anon6() {
            }

            @DexIgnore
            /* final */ /* synthetic */ HomescreenActivity val$activity;

            @DexIgnore
            Anon6(HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            @DexIgnore
            public void run() {
                this.val$activity.goToGesture();
            }
        }

        @DexIgnore
        class Anon7 implements Runnable {
            @DexIgnore
            Anon7() {
            }

            @DexIgnore
            public void run() {
                SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), GestureDialogActivity.class));
            }
        }

        @DexIgnore
        class Anon8 implements Runnable {
            @DexIgnore
            Anon8() {
            }

            @DexIgnore
            /* final */ /* synthetic */ HomescreenActivity val$activity;

            @DexIgnore
            Anon8(HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            @DexIgnore
            public void run() {
                this.val$activity.goToFavoritesTab();
                SearchActivity.startSearchActivityFor(SEARCH_TYPES.HOME, false, this.val$activity);
            }
        }

        @DexIgnore
        class Anon9 implements Runnable {
            @DexIgnore
            Anon9() {
            }

            @DexIgnore
            /* final */ /* synthetic */ HomescreenActivity val$activity;

            @DexIgnore
            Anon9(HomescreenActivity homescreenActivity) {
                this.val$activity = homescreenActivity;
            }

            @DexIgnore
            public void run() {
                this.val$activity.goToFavoritesTab();
                SearchActivity.startSearchActivityFor(SEARCH_TYPES.WORK, false, this.val$activity);
            }
        }

        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        public void onClick(View v, int position) {
            // boolean isHeader;
            // SuggestionsFragment.logger.v("position clicked: " + position);
            // if (SuggestionsFragment.this.suggestionAdapter == null) {
            //     SuggestionsFragment.logger.e("onClick with a null adapter! WTF!");
            //     return;
            // }
            // Suggestion suggestion = SuggestionsFragment.this.suggestionAdapter.getItem(position);
            // if (position == 0) {
            //     isHeader = true;
            // } else {
            //     isHeader = false;
            // }
            // if (isHeader || (v instanceof TripCardView)) {
            //     boolean isInPendingTrip = SuggestionsFragment.this.navdyRouteHandler.isInOneOfThePendingTripStates();
            //     boolean isInActiveTrip = SuggestionsFragment.this.navdyRouteHandler.isInOneOfTheActiveTripStates();
            //     if (isInPendingTrip) {
            //         SuggestionsFragment.this.suggestionAdapter.clickPendingTripCard(SuggestionsFragment.this.getActivity());
            //     } else if (isInActiveTrip) {
            //         SuggestionsFragment.this.suggestionAdapter.clickActiveTripCard();
            //     } else {
            //         SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), DropPinActivity.class));
            //     }
            // } else if (suggestion != null) {
            //     HomescreenActivity activity = (HomescreenActivity) SuggestionsFragment.this.getActivity();
            //     if (suggestion.getType() == SuggestionType.LOADING) {
            //         return;
            //     }
            //     if (suggestion.isTip()) {
            //         switch (suggestion.getType()) {
            //             case ENABLE_MICROPHONE:
            //                 handleTipClick(SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, new Anon3.Anon2());
            //                 return;
            //             case VOICE_SEARCH:
            //                 handleTipClick(SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, new Anon3.C0064Anon3());
            //                 return;
            //             case GOOGLE_NOW:
            //                 handleTipClick(SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, new Anon3.Anon4());
            //                 return;
            //             case HUD_LOCAL_MUSIC_BROWSER:
            //                 handleTipClick(SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, new Anon3.Anon5());
            //                 return;
            //             case ENABLE_GESTURES:
            //                 handleTipClick(SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, new Anon3.Anon6(activity));
            //                 return;
            //             case TRY_GESTURES:
            //                 handleTipClick(SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, new Anon3.Anon7());
            //                 return;
            //             case ADD_HOME:
            //                 handleTipClick(SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, new Anon3.Anon8(activity));
            //                 return;
            //             case ADD_WORK:
            //                 handleTipClick(SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, new Anon3.Anon9(activity));
            //                 return;
            //             case ADD_FAVORITE:
            //                 SearchActivity.startSearchActivityFor(SEARCH_TYPES.FAVORITE, false, activity);
            //                 return;
            //             case OTA:
            //                 SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), OtaSettingsActivity.class));
            //                 return;
            //             case DEMO_VIDEO:
            //                 SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), DemoVideoDialogActivity.class));
            //                 return;
            //             default:
            //                 handleTipClick(SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, new Anon3.Anon1(activity));
            //                 return;
            //         }
            //     } else {
            //         Destination destination = suggestion.destination;
            //         SuggestionsFragment.logger.v("destination object status: " + destination.toString());
            //         SetDestinationTracker setDestinationTracker = SetDestinationTracker.getInstance();
            //         setDestinationTracker.setSourceValue(Attributes.DestinationAttributes.SOURCE_VALUES.SUGGESTION_LIST);
            //         setDestinationTracker.setDestinationType(Attributes.DestinationAttributes.TYPE_VALUES.getSetDestinationTypeFromSuggestion(suggestion));
            //         if (v.getId() != id.nav_button) {
            //             activity.showRequestNewRouteDialog(new Anon3.Anon10(setDestinationTracker, destination));
            //         } else if (v instanceof ImageButton) {
            //             DetailsActivity.startDetailsActivity(destination, activity);
            //         }
            //     }
            // }
        }

        @DexIgnore
        private void handleTipClick(String sharedPrefKey, Runnable runnable) {
            Editor sharedPrefEditor = SettingsUtils.getSharedPreferences().edit();
            sharedPrefEditor.putBoolean(sharedPrefKey, true);
            sharedPrefEditor.apply();
            if (SuggestionsFragment.this.suggestionAdapter != null) {
                SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
            }
            runnable.run();
        }
    }

    @DexIgnore
    class Anon4 implements Runnable {
        @DexIgnore
        /* final */ /* synthetic */ ArrayList val$suggestions;

        @DexIgnore
        Anon4(ArrayList arrayList) {
            this.val$suggestions = arrayList;
        }

        @DexIgnore
        public void run() {
            if (SuggestionsFragment.this.suggestionAdapter != null) {
                SuggestionsFragment.this.suggestionAdapter.onSuggestionBuildComplete(this.val$suggestions);
            }
            SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
            if (sharedPrefs.getBoolean(SettingsConstants.FIRST_TIME_ON_HOMESCREEN, true)) {
                sharedPrefs.edit().putBoolean(SettingsConstants.FIRST_TIME_ON_HOMESCREEN, false).apply();
                if (!sharedPrefs.getBoolean(SettingsConstants.USER_WATCHED_THE_DEMO, false)) {
                    SuggestionsFragment.this.startActivity(new Intent(SuggestionsFragment.this.getContext(), DemoVideoDialogActivity.class));
                }
            }
        }
    }

    @DexIgnore
    class Anon5 implements Runnable {
        @DexIgnore
        /* final */ /* synthetic */ State val$state;

        @DexIgnore
        Anon5(State state) {
            this.val$state = state;
        }

        @DexIgnore
        public void run() {
            SuggestionManager.updateOtaStatus(this.val$state);
        }
    }

    @DexIgnore
    class Anon6 implements OnHereMapFragmentInitialized {
        @DexIgnore
        Anon6() {
        }

        @DexIgnore
        /* final */ /* synthetic */ NavdyRouteInfo val$progress;

        @DexIgnore
        Anon6(NavdyRouteInfo navdyRouteInfo) {
            this.val$progress = navdyRouteInfo;
        }

        @DexIgnore
        public void onInit(@NonNull Map hereMap) {
            SuggestionsFragment.this.setProgressPolyline(hereMap, this.val$progress.getProgress());
        }

        @DexIgnore
        public void onError(@NonNull BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    class Anon7 implements OnMapReadyCallback {
        @DexIgnore
        /* final */ /* synthetic */ Destination val$destination;

        @DexIgnore
        Anon7(Destination destination) {
            this.val$destination = destination;
        }

        @DexIgnore
        public void onMapReady(GoogleMap googleMap) {
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(this.val$destination.getRecentPinAsset());
            LatLng position = new LatLng(this.val$destination.getDisplayLat(), this.val$destination.getDisplayLng());
            if (SuggestionsFragment.this.lastArrivalMarker != null) {
                SuggestionsFragment.this.lastArrivalMarker.setIcon(icon);
                SuggestionsFragment.this.lastArrivalMarker.setPosition(position);
                return;
            }
            SuggestionsFragment.this.lastArrivalMarker = googleMap.addMarker(new MarkerOptions().position(position).title(SuggestionsFragment.LAST_ARRIVAL_MARKER_STRING).icon(icon));
        }
    }

    @DexIgnore
    class Anon8 implements OnHereMapFragmentInitialized {
        @DexIgnore
        Anon8() {
        }

        @DexIgnore
        /* final */ /* synthetic */ Destination val$destination;

        @DexIgnore
        Anon8(Destination destination) {
            this.val$destination = destination;
        }

        @DexIgnore
        public void onInit(@NonNull Map hereMap) {
            SuggestionsFragment.this.setDestinationMarker(hereMap, this.val$destination);
            SuggestionsFragment.this.hereMapFragment.centerOnUserLocationAndDestination(this.val$destination, Animation.LINEAR);
        }

        @DexIgnore
        public void onError(@NonNull BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    class Anon9 implements OnHereMapFragmentReady {
        @DexIgnore
        Anon9() {
        }

        @DexIgnore
        /* final */ /* synthetic */ NavdyRouteInfo val$route;

        @DexIgnore
        Anon9(NavdyRouteInfo navdyRouteInfo) {
            this.val$route = navdyRouteInfo;
        }

        @DexIgnore
        public void onReady(@NonNull Map hereMap) {
            GeoPolyline geoPolyline = this.val$route.getRoute();
            SuggestionsFragment.this.setRoutePolyline(hereMap, geoPolyline);
            if (geoPolyline != null) {
                SuggestionsFragment.this.hereMapFragment.centerOnRoute(geoPolyline, Animation.LINEAR);
            }
        }

        @DexIgnore
        public void onError(@NonNull BaseHereMapFragment.Error error) {
        }
    }

    @DexIgnore
    private class CustomSuggestionsActionCallback implements Callback {
        @DexIgnore
        private final int actionModePosition;
        @DexIgnore
        private final HomescreenActivity homescreenActivity;
        @DexIgnore
        private final View selectedCardRow;
        @DexIgnore
        private final Suggestion suggestion;

        @DexIgnore
        class Anon1 implements QueryResultCallback {
            @DexIgnore
            Anon1() {
            }

            @DexIgnore
            public void onQueryCompleted(int nbRows, Uri uri) {
                SuggestionsFragment.this.hideProgressDialog();
                if (nbRows <= 0) {
                    SuggestionsFragment.CustomSuggestionsActionCallback.this.showFavoriteEditErrorToast();
                    return;
                }
                SuggestionsFragment.CustomSuggestionsActionCallback.this.showFavoriteEditSuccessToast();
                if (!SuggestionsFragment.CustomSuggestionsActionCallback.this.homescreenActivity.isFinishing()) {
                    SuggestionsFragment.CustomSuggestionsActionCallback.this.homescreenActivity.onFavoriteListChanged();
                }
            }
        }

        @DexIgnore
        class Anon2 implements OnClickListener {
            @DexIgnore
            /* final */ /* synthetic */ Suggestion val$suggestion;

            @DexIgnore
            Anon2(Suggestion suggestion) {
                this.val$suggestion = suggestion;
            }

            @DexIgnore
            public void onClick(DialogInterface dialog, int which) {
                this.val$suggestion.destination.doNotSuggest = true;
                DestinationSuggestionService.reset(NavdyApplication.getAppContext());
                this.val$suggestion.destination.updateDoNotSuggestAsync();
                if (SuggestionsFragment.this.suggestionAdapter != null) {
                    SuggestionsFragment.this.suggestionAdapter.rebuildSuggestions();
                }
            }
        }

        @DexIgnore
        CustomSuggestionsActionCallback(Suggestion suggestion2, HomescreenActivity homescreenActivity2, int actionModePosition2, View selectedCardRow2) {
            this.suggestion = suggestion2;
            this.homescreenActivity = homescreenActivity2;
            this.actionModePosition = actionModePosition2;
            this.selectedCardRow = selectedCardRow2;
        }

        @DexIgnore
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            if (this.suggestion.isActiveTrip()) {
                inflater.inflate(R.menu.menu_active_trip_edit, menu);
                mode.setTitle(string.active_trip_title);
            } else if (this.suggestion.isPendingTrip()) {
                inflater.inflate(R.menu.menu_next_trip_edit, menu);
                mode.setTitle(string.next_trip_title);
            } else if (this.suggestion.canBeRoutedTo()) {
                inflater.inflate(R.menu.menu_suggestion_edit, menu);
                mode.setTitle(string.edit_suggestion_title);
            }
            return true;
        }

        @DexIgnore
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            if (SuggestionsFragment.this.suggestionAdapter != null) {
                SuggestionsFragment.this.suggestionAdapter.setSelectedSuggestion(this.selectedCardRow, this.suggestion.getType(), this.actionModePosition);
                Suggestion suggestion2 = SuggestionsFragment.this.suggestionAdapter.getItem(this.actionModePosition);
                if (!(suggestion2 == null || suggestion2.destination == null || !suggestion2.destination.isFavoriteDestination())) {
                    SuggestionsFragment.logger.v("Suggestion is a Favorite. Save option is disabled");
                    menu.findItem(id.menu_save).setVisible(false);
                }
            }
            hideTabs();
            return true;
        }

        @DexIgnore
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (SuggestionsFragment.this.suggestionAdapter == null) {
                return false;
            }
            Suggestion suggestion2 = SuggestionsFragment.this.suggestionAdapter.getItem(this.actionModePosition);
            switch (item.getItemId()) {
                case id.menu_save /*2131756066*/:
                    saveSuggestionAsFavorite(suggestion2);
                    break;
                case id.menu_stop /*2131756067*/:
                    stopRoute(suggestion2);
                    break;
                case id.menu_delete /*2131756069*/:
                    deleteSuggestionInMenu(suggestion2);
                    break;
                default:
                    return false;
            }
            mode.finish();
            return true;
        }

        @DexIgnore
        public void onDestroyActionMode(ActionMode mode) {
            SuggestionsFragment.logger.v("onDestroyActionMode from Suggestion: " + mode);
            if (SuggestionsFragment.this.suggestionAdapter != null) {
                SuggestionsFragment.this.suggestionAdapter.endSelectionMode();
            }
            showTabs();
        }

        @DexIgnore
        private void hideTabs() {
            if (this.homescreenActivity != null && !this.homescreenActivity.isFinishing()) {
                View tabs = this.homescreenActivity.findViewById(id.tabs_cover);
                if (tabs != null) {
                    tabs.setVisibility(View.VISIBLE);
                }
            }
        }

        @DexIgnore
        private void showTabs() {
            if (this.homescreenActivity != null && !this.homescreenActivity.isFinishing()) {
                View tabs = this.homescreenActivity.findViewById(id.tabs_cover);
                if (tabs != null) {
                    tabs.setVisibility(View.GONE);
                }
            }
        }

        @DexIgnore
        private void saveSuggestionAsFavorite(Suggestion suggestion2) {
            SuggestionsFragment.logger.v("menu save");
            if (suggestion2.destination != null) {
                SuggestionsFragment.logger.v("suggestion type: " + suggestion2.getType());
                SuggestionsFragment.logger.v("suggestion name: " + suggestion2.destination.name);
                Destination destination = suggestion2.destination;
                if (destination.isFavoriteDestination()) {
                    showFavoriteEditErrorToast();
                }
                SuggestionsFragment.this.showProgressDialog();
                destination.saveDestinationAsFavoritesAsync(-1, new SuggestionsFragment.CustomSuggestionsActionCallback.Anon1());
            }
        }

        @DexIgnore
        private void deleteSuggestionInMenu(Suggestion suggestion2) {
            SuggestionsFragment.logger.v("menu delete");
            if (suggestion2 != null && suggestion2.destination != null) {
                new Builder(SuggestionsFragment.this.getActivity()).setTitle(string.are_you_sure).setMessage(string.do_not_suggest_desc).setPositiveButton(string.YES, new SuggestionsFragment.CustomSuggestionsActionCallback.Anon2(suggestion2)).setNegativeButton(string.no_thanks, null).show();
            }
        }

        @DexIgnore
        private void stopRoute(Suggestion suggestion2) {
            SuggestionsFragment.logger.v("menu stop");
            SuggestionsFragment.this.navdyRouteHandler.stopRouting();
            if (this.suggestion.isPendingTrip() && suggestion2 != null && suggestion2.destination != null) {
                if (SuggestionsFragment.this.suggestionAdapter != null) {
                    SuggestionsFragment.this.suggestionAdapter.endSelectionMode();
                }
                SuggestionsFragment.this.scrollToTheTop();
            }
        }

        @DexIgnore
        private void showFavoriteEditSuccessToast() {
            BaseActivity.showLongToast(string.toast_favorite_edit_saved);
        }

        @DexIgnore
        private void showFavoriteEditErrorToast() {
            BaseActivity.showLongToast(string.toast_favorite_edit_error);
        }
    }

    @DexIgnore
    private class SuggestionScrollListener extends OnScrollListener {
        @DexIgnore
        private boolean drawerIsOpen;
        @DexIgnore
        private boolean drawerWasOpen;
        @DexIgnore
        private int mapSizeInPixels;
        @DexIgnore
        private int y;

        @DexIgnore
        private SuggestionScrollListener() {
            this.mapSizeInPixels = 0;
            this.y = 0;
            this.drawerIsOpen = false;
            this.drawerWasOpen = false;
        }

        /* synthetic */ SuggestionScrollListener(SuggestionsFragment x0, Anon1 x1) {
            this();
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            boolean z = false;
            this.y += dy;
            if (this.y < 0) {
                this.y = 0;
            }
            if (this.drawerIsOpen != this.drawerWasOpen) {
                return;
            }
            if ((this.drawerWasOpen && this.y < this.mapSizeInPixels) || (!this.drawerWasOpen && this.y > 0)) {
                if (!this.drawerWasOpen) {
                    z = true;
                }
                this.drawerWasOpen = z;
                updateChevron(this.drawerWasOpen);
            }
        }

        @DexIgnore
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == 0) {
                if (SuggestionsFragment.this.suggestionAdapter == null) {
                    if (this.mapSizeInPixels == 0) {
                        return;
                    }
                } else if (SuggestionsFragment.this.hereMapFragment == null || !SuggestionsFragment.this.hereMapFragment.isVisible()) {
                    this.mapSizeInPixels = SuggestionsFragment.this.suggestionAdapter.getGoogleMapHeight();
                } else {
                    this.mapSizeInPixels = SuggestionsFragment.this.suggestionAdapter.getHereMapHeight();
                }
                SuggestionsFragment.logger.d("Scrolled to: " + this.y + " drawer was opened: " + this.drawerIsOpen + " mapSizeInPixels: " + this.mapSizeInPixels);
                if (SuggestionsFragment.this.layoutManager != null) {
                    if (!this.drawerIsOpen && this.y > 0) {
                        if (this.y < this.mapSizeInPixels) {
                            SuggestionsFragment.this.layoutManager.scrollToPositionWithOffset(0, -this.mapSizeInPixels);
                            this.y = this.mapSizeInPixels;
                        }
                        this.drawerIsOpen = true;
                    } else if (this.drawerIsOpen && this.y < this.mapSizeInPixels) {
                        if (this.y != 0) {
                            if (SuggestionsFragment.this.suggestionRecycler != null) {
                                SuggestionsFragment.this.suggestionRecycler.smoothScrollToPosition(0);
                            }
                            this.y = 0;
                        }
                        this.drawerIsOpen = false;
                    }
                    this.drawerWasOpen = this.drawerIsOpen;
                    updateChevron(this.drawerIsOpen);
                    return;
                }
                SuggestionsFragment.logger.e("Unable to get layoutManager !");
            }
        }

        @DexIgnore
        public void reset() {
            this.y = 0;
            this.drawerIsOpen = false;
            updateChevron(false);
        }

        @DexIgnore
        private void updateChevron(boolean drawerIsOpen2) {
            ImageView chevron = null;
            if (SuggestionsFragment.this.suggestionRecycler != null) {
                chevron = SuggestionsFragment.this.suggestionRecycler.findViewById(id.chevron);
            }
            if (chevron != null) {
                if (drawerIsOpen2) {
                    chevron.setImageResource(drawable.icon_suggest_arrow_down);
                } else {
                    chevron.setImageResource(drawable.ic_chevron);
                }
            }
            FragmentActivity activity = SuggestionsFragment.this.getActivity();
            if (activity != null && !activity.isFinishing()) {
                ((HomescreenActivity) activity).hideConnectionBanner(null);
            }
        }
    }

    /* @DexIgnore
    static {
        android.content.res.Resources res = com.navdy.client.app.NavdyApplication.getAppContext().getResources();
        HERE_TOP_PADDING = res.getDimensionPixelSize(com.navdy.client.R.dimen.search_bar_height) + (res.getDimensionPixelSize(com.navdy.client.R.dimen.search_bar_margin) * 2) + com.navdy.client.app.framework.map.MapUtils.hereMapTopDownPadding;
        LAST_ARRIVAL_MARKER_STRING = res.getString(com.navdy.client.R.string.last_arrival_marker);
    } */

    @DexIgnore
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = null;
        FragmentActivity activity = getActivity();
        try {
            rootView = inflater.inflate(layout.hs_fragment_suggestions, container, false);
            Injector.inject(NavdyApplication.getAppContext(), this);
            this.offlineBanner = rootView.findViewById(id.offline_banner);
        } catch (Throwable t) {
            logger.e("Error while inflating the suggestions fragment.", t);
            GmsUtils.finishIfGmsIsNotUpToDate(activity);
        }
        this.googleMapFragment = (BaseGoogleMapFragment) activity.getFragmentManager().findFragmentById(id.home_fragment_google_map_fragment);
        if (this.googleMapFragment != null) {
            this.googleMapFragment.hide();
        }
        this.hereMapFragment = (BaseHereMapFragment) activity.getFragmentManager().findFragmentById(id.home_fragment_here_map);
        if (this.hereMapFragment != null) {
            this.hereMapFragment.hide();
            this.hereMapFragment.setUsableArea(HERE_TOP_PADDING, HERE_PADDING, HERE_PADDING, HERE_PADDING);
        }
        if (rootView != null) {
            setRecyclerView(rootView);
        }
        SuggestionManager.addListener(this.suggestionListener);
        return rootView;
    }

    // @android.support.annotation.WorkerThread
    @DexIgnore
    public void onSuggestionBuildComplete(ArrayList<Suggestion> suggestions) {
        this.handler.post(new Anon4(suggestions));
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        this.lastUpdate = this.navdyLocationManager.getSmartStartCoordinates();
        updateOfflineBannerVisibility();
        if (this.suggestionAdapter == null) {
            initSuggestionAdapter();
        } else {
            try {
                this.suggestionAdapter.rebuildSuggestions();
            } catch (Exception e) {
                logger.e("rebuilding suggestions failed: " + this.suggestionAdapter);
            }
        }
        connectToService();
        this.navdyRouteHandler.addListener(this);
        this.navdyLocationManager.addListener(this.locationListener);
    }

    @DexIgnore
    public void onPause() {
        this.navdyLocationManager.removeListener(this.locationListener);
        this.navdyRouteHandler.removeListener(this);
        if (this.suggestionAdapter != null) {
            this.suggestionAdapter.endSelectionMode();
            this.suggestionAdapter.stopRefreshingAllEtas();
        }
        if (this.otaUpdateService != null) {
            this.otaUpdateService.unregisterUIClient();
        }
        disconnectService();
        super.onPause();
    }

    @DexIgnore
    public void onDestroy() {
        SuggestionManager.removeListener(this.suggestionListener);
        super.onDestroy();
    }

    @DexIgnore
    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service != null) {
            this.otaUpdateService = (OTAUpdateServiceInterface) service;
            this.otaUpdateService.registerUIClient(this);
            if (!this.otaUpdateService.isCheckingForUpdate()) {
                State state = this.otaUpdateService.getOTAUpdateState();
                if (state != State.UPLOADING && state != State.DOWNLOADING_UPDATE) {
                    this.otaUpdateService.checkForUpdate();
                }
            }
        }
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName name) {
        this.otaUpdateService = null;
    }

    @DexIgnore
    public void onErrorCheckingForUpdate(Error error) {
    }

    @DexIgnore
    public void onStateChanged(State state, UpdateInfo updateInfo) {
        TaskManager.getInstance().execute(new Anon5(state), 1);
    }

    @DexIgnore
    public void onDownloadProgress(DownloadUpdateStatus progress, long bytesDownloaded, byte percentage) {
    }

    @DexIgnore
    public void onUploadProgress(UploadToHUDStatus progress, long completed, byte percentage) {
    }

    @DexIgnore
    public void onPendingRouteCalculating(@NonNull Destination destination) {
        clearMapObjects();
        addTripDestinationMarkerAndCenterMap(destination);
    }

    @DexIgnore
    public void onPendingRouteCalculated(@NonNull NavdyRouteHandler.Error error, @Nullable NavdyRouteInfo pendingRoute) {
        clearMapObjects();
        if (pendingRoute != null) {
            addTripDestinationMarkerAndCenterMap(pendingRoute.getDestination());
        }
        if (error == NavdyRouteHandler.Error.NONE) {
            addTripRouteAndCenterMap(pendingRoute);
        }
    }

    @DexIgnore
    public void onRouteCalculating(@NonNull Destination destination) {
        clearLastArrival();
        if (this.hereMapFragment != null) {
            this.hereMapFragment.centerOnUserLocation();
        }
    }

    @DexIgnore
    public void onRouteCalculated(@NonNull NavdyRouteHandler.Error error, @NonNull NavdyRouteInfo route) {
        startMapRoute(route);
    }

    @DexIgnore
    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        logger.v("onRouteStarted");
        startMapRoute(route);
    }

    @DexIgnore
    public void onTripProgress(@NonNull NavdyRouteInfo progress) {
        if (this.hereMapFragment == null) {
            logger.e("Calling onTripProgress with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new Anon6(progress));
        }
    }

    @DexIgnore
    public void onReroute() {
    }

    @DexIgnore
    public void onRouteArrived(@NonNull Destination destination) {
        if (this.googleMapFragment == null) {
            logger.e("Calling onRouteArrived with a null googleMapFragment");
        } else {
            this.googleMapFragment.getMapAsync(new Anon7(destination));
        }
    }

    @DexIgnore
    public void onStopRoute() {
        logger.v("onStopRoute");
        clearMapObjects();
        showGoogleMap();
        this.suggestionAdapter.rebuildSuggestions();
    }

    @DexIgnore
    private void setRecyclerView(View rootView) {
        this.suggestionRecycler = rootView.findViewById(id.home_fragment_recycler_view);
        this.layoutManager = new LinearLayoutManager(getContext());
        this.layoutManager.setOrientation(1);
        this.scrollListener = new SuggestionsFragment.SuggestionScrollListener(this, null);
        if (this.suggestionRecycler != null) {
            this.suggestionRecycler.setLayoutManager(this.layoutManager);
            this.suggestionRecycler.addOnScrollListener(this.scrollListener);
        }
    }

    @DexIgnore
    private void initSuggestionAdapter() {
        Resources resources = getResources();
        Activity activity = getActivity();
        int displayHeight = UiUtils.getDisplayHeight(activity);
        int toolbarHeight = UiUtils.getActionBarHeight(activity);
        int tabsHeight = UiUtils.getTabsHeight(resources);
        this.suggestionAdapter = new SuggestedDestinationsAdapter(getContext(), ((displayHeight - toolbarHeight) - tabsHeight) - UiUtils.getGoogleMapBottomPadding(resources), ((displayHeight - toolbarHeight) - tabsHeight) - UiUtils.getHereMapBottomPadding(resources), resources.getDimensionPixelSize(dimen.list_header_height));
        if (this.suggestionRecycler != null) {
            this.suggestionRecycler.setAdapter(this.suggestionAdapter);
        }
        this.suggestionAdapter.setClickListener(this.clickListener);
        this.suggestionAdapter.setSuggestionLongClickListener(this.onLongClickListener);
    }

    @DexIgnore
    private void addTripDestinationMarkerAndCenterMap(Destination destination) {
        scrollToTheTop();
        showHereMap();
        if (this.hereMapFragment == null) {
            logger.e("Calling addTripDestinationMarkerAndCenterMap with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new Anon8(destination));
        }
    }

    @DexIgnore
    private void addTripRouteAndCenterMap(NavdyRouteInfo route) {
        if (this.hereMapFragment == null) {
            logger.e("Calling addTripRouteAndCenterMap with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenReady(new Anon9(route));
        }
    }

    @DexIgnore
    private void clearMapObjects() {
        if (this.hereMapFragment == null) {
            logger.e("Calling clearMapObjects with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new Anon10());
        }
    }

    @DexIgnore
    private void setDestinationMarker(@NonNull Map hereMap, @Nullable Destination destination) {
        MapMarker marker;
        if (destination != null) {
            marker = destination.getHereMapMarker();
        } else {
            marker = null;
        }
        synchronized (this.lock) {
            if (this.destinationMarker != null) {
                hereMap.removeMapObject(this.destinationMarker);
            }
            this.destinationMarker = marker;
            if (this.destinationMarker != null) {
                hereMap.addMapObject(this.destinationMarker);
            }
        }
    }

    @DexIgnore
    private void setRoutePolyline(@NonNull Map hereMap, @Nullable GeoPolyline route) {
        MapPolyline polyline;
        if (route != null) {
            polyline = MapUtils.generateRoutePolyline(route);
        } else {
            polyline = null;
        }
        synchronized (this.lock) {
            if (this.routePolyline != null) {
                hereMap.removeMapObject(this.routePolyline);
            }
            this.routePolyline = polyline;
            if (this.routePolyline != null) {
                hereMap.addMapObject(this.routePolyline);
            }
        }
    }

    @DexIgnore
    private synchronized void setProgressPolyline(@NonNull Map hereMap, @Nullable GeoPolyline progressPolyline) {
        MapPolyline polyline;
        if (progressPolyline != null) {
            polyline = MapUtils.generateProgressPolyline(progressPolyline);
        } else {
            polyline = null;
        }
        synchronized (this.lock) {
            if (this.routeProgressPolyline != null) {
                hereMap.removeMapObject(this.routeProgressPolyline);
            }
            this.routeProgressPolyline = polyline;
            if (this.routeProgressPolyline != null) {
                hereMap.addMapObject(this.routeProgressPolyline);
            }
        }
    }

    @DexIgnore
    private void clearLastArrival() {
        if (this.lastArrivalMarker != null) {
            this.lastArrivalMarker.remove();
            this.lastArrivalMarker = null;
        }
    }

    @DexIgnore
    private void showHereMap() {
        logger.v("showHereMap");
        if (!isInForeground()) {
            logger.w("showHereMap, not in foreground, no-op");
            return;
        }
        if (this.googleMapFragment != null) {
            this.googleMapFragment.hide();
        }
        if (this.hereMapFragment != null) {
            this.hereMapFragment.show();
        }
        scrollToTheTop();
    }

    @DexIgnore
    private void showGoogleMap() {
        logger.v("showGoogleMap");
        if (!isInForeground()) {
            logger.w("showGoogleMap, not in foreground, no-op");
            return;
        }
        if (this.hereMapFragment != null) {
            this.hereMapFragment.hide();
        }
        if (this.googleMapFragment != null) {
            this.googleMapFragment.show(new Anon11());
        }
        scrollToTheTop();
    }

    @DexIgnore
    private void startMapRoute(@NonNull NavdyRouteInfo route) {
        clearMapObjects();
        addTripDestinationMarkerAndCenterMap(route.getDestination());
        addTripRouteAndCenterMap(route);
    }

    @DexReplace
    private void connectToService() {
        Context context = getContext();
        try {
            if (context != null) {
                context.bindService(OTAUpdateService.getServiceIntent(context), this, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexReplace
    private void disconnectService() {
        Context context = getContext();
        try {
            if (context != null) {
                context.unbindService(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    private void scrollToTheTop() {
        if (this.layoutManager != null) {
            this.layoutManager.scrollToPosition(0);
        }
        if (this.scrollListener != null) {
            this.scrollListener.reset();
        }
    }

    @DexIgnore
    public void onDetach() {
        if (this.suggestionAdapter != null) {
            this.suggestionAdapter.clearAllAnimations();
            this.suggestionAdapter.stopRefreshingAllEtas();
        }
        super.onDetach();
    }

    @Subscribe
    @DexIgnore
    public void handleReachabilityStateChange(ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            logger.v("reachability event received: " + reachabilityEvent.isReachable);
            if (this.suggestionAdapter != null) {
                this.suggestionAdapter.notifyDataSetChanged();
            }
            updateOfflineBannerVisibility();
        }
    }

    @DexIgnore
    private void updateOfflineBannerVisibility() {
        if (this.offlineBanner != null) {
            this.offlineBanner.setVisibility(AppInstance.getInstance().canReachInternet() ? View.GONE : View.VISIBLE);
        }
    }
}
