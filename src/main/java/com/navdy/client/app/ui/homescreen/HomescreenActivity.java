package com.navdy.client.app.ui.homescreen;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.alelec.navdyclient.R;

import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public class HomescreenActivity extends com.navdy.client.app.ui.base.BaseActivity implements android.support.v4.view.ViewPager.OnPageChangeListener, com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener {
    @DexIgnore
    public static int EASTER_EGG_HIDE_ALL_TIPS;  // = 15;
    @DexIgnore
    public static int EASTER_EGG_SHOW_ALL_TIPS;  // = 20;
    @DexIgnore
    public static int EASTER_EGG_SHOW_TRIP_TAB;  // = 10;
    @DexIgnore
    public static java.lang.String EXTRA_DESTINATION;  // = "extra_destination";
    @DexIgnore
    private static java.lang.String EXTRA_HOMESCREEN_EXPLICITLY_ROUTING;  // = "extra_homescreen_explicitly_routing";
    @DexIgnore
    private static java.lang.String GOOGLE_MAPS_HOST;  // = "maps.google.com";
    @DexIgnore
    private static java.lang.String GOOGLE_NAVIGATION_SCHEME;  // = "google.navigation";
    @DexIgnore
    private static int SUCCESS_BANNER_DISMISS_DELAY;  // = 2000;
    @DexIgnore
    private static long VERSION_CHECK_INTERVAL;  // = 30000;
    @DexIgnore
    private android.widget.TextView addFavoriteLabel;
    @DexIgnore
    private android.support.design.widget.FloatingActionButton addHome;
    @DexIgnore
    private android.widget.TextView addHomeLabel;
    @DexIgnore
    private android.support.design.widget.FloatingActionButton addWork;
    @DexIgnore
    private android.widget.TextView addWorkLabel;
    @DexIgnore
    private android.support.design.widget.FloatingActionButton fab;
    @DexIgnore
    private android.view.View film;
    @DexIgnore
    private boolean highlightGlanceSwitchOnResume;  // = false;
    @DexIgnore
    private boolean isFabShowing;  // = false;
    @DexIgnore
    private long lastCheck;  // = 0;
    @DexIgnore
    private android.content.Intent lastExternalIntentHandled;  // = null;
    @DexIgnore
    private com.navdy.client.app.framework.AppInstance mAppInstance;
    @DexIgnore
    private android.support.v7.view.ActionMode mode;
    @DexIgnore
    private com.navdy.client.app.framework.navigation.NavdyRouteHandler navdyRouteHandler;  // = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance();
    @DexIgnore
    private android.support.design.widget.NavigationView navigationView;
    @DexIgnore
    com.navdy.client.app.ui.homescreen.HomescreenPagerAdapter pagerAdapter;
    @DexIgnore
    private boolean reloadProfile;  // = false;
    @DexIgnore
    private android.support.design.widget.TabLayout tabLayout;
    @DexIgnore
    private android.support.v7.widget.Toolbar toolbar;
    @DexIgnore
    private java.lang.String trackerEvent;  // = com.navdy.client.app.tracking.TrackerConstants.Event.NAVIGATE_USING_SEARCH_RESULTS;
    @DexIgnore
    private android.support.v4.view.ViewPager viewPager;

    @DexIgnore
    HomescreenActivity() {}

    // class Anon1 implements android.view.View.OnClickListener {
    //     Anon1() {
    //     }
    //
    //     public void onClick(android.view.View v) {
    //         android.widget.RelativeLayout successBanner = (android.widget.RelativeLayout) com.navdy.client.app.ui.homescreen.HomescreenActivity.this.findViewById(com.navdy.client.R.id.connection_success_banner);
    //         android.widget.RelativeLayout failureBanner = (android.widget.RelativeLayout) com.navdy.client.app.ui.homescreen.HomescreenActivity.this.findViewById(com.navdy.client.R.id.connection_failure_banner);
    //         if (successBanner != null && failureBanner != null) {
    //             if (com.navdy.client.app.ui.homescreen.HomescreenActivity.this.mAppInstance.isDeviceConnected()) {
    //                 if (successBanner.getVisibility() == View.VISIBLE) {
    //                     com.navdy.client.app.ui.homescreen.HomescreenActivity.this.hideConnectionBanner(null);
    //                 } else {
    //                     com.navdy.client.app.ui.homescreen.HomescreenActivity.this.showConnectivityBanner();
    //                 }
    //             } else if (failureBanner.getVisibility() == View.VISIBLE) {
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.hideConnectionBanner(null);
    //             } else {
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.showConnectivityBanner();
    //             }
    //         }
    //     }
    // }
    //
    // class Anon10 implements java.lang.Runnable {
    //     Anon10() {
    //     }
    //
    //     public void run() {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.hideConnectionBanner(null);
    //     }
    // }
    //
    // class Anon11 implements android.content.DialogInterface.OnClickListener {
    //     Anon11() {
    //     }
    //
    //     public void onClick(android.content.DialogInterface dialog, int which) {
    //         if (which == -1) {
    //             com.navdy.service.library.device.RemoteDevice device = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
    //             if (device != null) {
    //                 device.postEvent((com.squareup.wire.Message) new com.navdy.service.library.events.connection.DisconnectRequest(java.lang.Boolean.valueOf(false)));
    //             }
    //         }
    //     }
    // }
    //
    // class Anon12 implements android.view.View.OnClickListener {
    //     Anon12() {
    //     }
    //
    //     public void onClick(android.view.View v) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.callSearchFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.HOME);
    //     }
    // }
    //
    // class Anon13 implements android.view.View.OnClickListener {
    //     Anon13() {
    //     }
    //
    //     public void onClick(android.view.View v) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.callSearchFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.WORK);
    //     }
    // }
    //
    // class Anon14 implements android.view.View.OnClickListener {
    //     Anon14() {
    //     }
    //
    //     public void onClick(android.view.View v) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.hideOtherFabs();
    //     }
    // }
    //
    // class Anon15 extends android.support.design.widget.FloatingActionButton.OnVisibilityChangedListener {
    //     Anon15() {
    //     }
    //
    //     public void onHidden(android.support.design.widget.FloatingActionButton fab) {
    //         fab.show();
    //     }
    // }
    //
    // class Anon16 extends android.os.AsyncTask<java.lang.Void, java.lang.Void, com.navdy.client.app.framework.models.Destination> {
    //     final /* synthetic */ android.content.Intent val$callingIntent;
    //
    //     Anon16(android.content.Intent intent) {
    //         this.val$callingIntent = intent;
    //     }
    //
    //     /* access modifiers changed from: protected */
    //     public com.navdy.client.app.framework.models.Destination doInBackground(java.lang.Void... voids) {
    //         return com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(this.val$callingIntent.getIntExtra(com.navdy.client.app.ui.homescreen.HomescreenActivity.EXTRA_DESTINATION, -1));
    //     }
    //
    //     /* access modifiers changed from: protected */
    //     public void onPostExecute(com.navdy.client.app.framework.models.Destination destination) {
    //         if (destination != null) {
    //             com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().requestNewRoute(destination);
    //         }
    //     }
    // }
    //
    // class Anon17 implements java.lang.Runnable {
    //     final /* synthetic */ android.content.Intent val$intent;
    //     final /* synthetic */ java.lang.String val$sharedText;
    //
    //     class Anon1 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
    //         Anon1() {
    //         }
    //
    //         public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.routeToDestination(destination);
    //         }
    //
    //         public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.logger.e("Failure: " + error.name());
    //         }
    //     }
    //
    //     Anon17(java.lang.String str, android.content.Intent intent) {
    //         this.val$sharedText = str;
    //         this.val$intent = intent;
    //     }
    //
    //     public void run() {
    //         if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(this.val$sharedText)) {
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.logger.d("shared text is empty");
    //             return;
    //         }
    //         java.lang.String displayName = "";
    //         java.lang.String address = "";
    //         if (this.val$sharedText.contains(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_MAPS_SHORT_URL)) {
    //             if (!this.val$sharedText.contains(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_MAPS_TO_SYNTAX) || !this.val$sharedText.contains(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_MAPS_VIA_SYNTAX)) {
    //                 displayName = this.val$intent.getStringExtra("android.intent.extra.SUBJECT");
    //             } else {
    //                 displayName = this.val$sharedText.substring(this.val$sharedText.indexOf(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_MAPS_TO_SYNTAX) + 2, this.val$sharedText.indexOf(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_MAPS_VIA_SYNTAX)).trim();
    //             }
    //         } else if (this.val$sharedText.contains(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_ASSISTANT_SHORT_URL)) {
    //             displayName = this.val$sharedText.substring(0, this.val$sharedText.indexOf(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_ASSISTANT_SHORT_URL));
    //         } else if (this.val$sharedText.contains(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_SEARCH_SHORT_URL)) {
    //             displayName = this.val$sharedText.substring(0, this.val$sharedText.indexOf(com.navdy.client.app.ui.search.SearchConstants.GOOGLE_SEARCH_SHORT_URL));
    //         } else if (this.val$sharedText.contains(com.navdy.client.app.ui.search.SearchConstants.YELP_MAPS_URL)) {
    //             displayName = this.val$sharedText.substring(0, this.val$sharedText.indexOf(com.navdy.client.app.ui.search.SearchConstants.YELP_MAPS_URL)).trim();
    //         }
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.logger.d("display name: " + displayName);
    //         if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address)) {
    //             com.navdy.client.app.framework.models.Destination d = new com.navdy.client.app.framework.models.Destination();
    //             d.setName(displayName);
    //             d.setRawAddressNotForDisplay(address);
    //             com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(com.navdy.client.app.providers.NavdyContentProvider.getThisDestination(d), new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon17.Anon1());
    //         } else if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(displayName)) {
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.routeToFirstResultFor(displayName);
    //         } else {
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.logger.d("display name and address are both empty so we can't navigate anywhere");
    //         }
    //     }
    // }
    //
    // class Anon18 implements java.lang.Runnable {
    //     final /* synthetic */ android.content.Intent val$searchIntent;
    //
    //     Anon18(android.content.Intent intent) {
    //         this.val$searchIntent = intent;
    //     }
    //
    //     public void run() {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivityForResult(this.val$searchIntent, com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.SEARCH.getCode());
    //     }
    // }
    //
    // class Anon19 implements java.lang.Runnable {
    //     final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
    //
    //     class Anon1 implements java.lang.Runnable {
    //         Anon1() {
    //         }
    //
    //         public void run() {
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon19.this.val$destination.updateAllColumnsOrInsertNewEntryInDb();
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.navdyRouteHandler.requestNewRoute(com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon19.this.val$destination);
    //         }
    //     }
    //
    //     Anon19(com.navdy.client.app.framework.models.Destination destination) {
    //         this.val$destination = destination;
    //     }
    //
    //     public void run() {
    //         com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon19.Anon1(), 1);
    //     }
    // }
    //
    // class Anon2 implements android.view.View.OnClickListener {
    //     Anon2() {
    //     }
    //
    //     public void onClick(android.view.View view) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.pickFabAction();
    //     }
    // }
    //
    // class Anon20 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
    //     Anon20() {
    //     }
    //
    //     public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.hideProgressDialog();
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.routeToDestination(destination);
    //     }
    //
    //     public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.logger.w("routeToAddressOrCoords, couldn't get coords either from here or google, trying with just address");
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.hideProgressDialog();
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.routeToDestination(destination);
    //     }
    // }
    //
    // class Anon21 implements java.lang.Runnable {
    //     final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
    //
    //     Anon21(com.navdy.client.app.framework.models.Destination destination) {
    //         this.val$destination = destination;
    //     }
    //
    //     public void run() {
    //         com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().requestNewRoute(this.val$destination);
    //     }
    // }
    //
    // class Anon3 implements android.support.design.widget.NavigationView.OnNavigationItemSelectedListener {
    //     final /* synthetic */ android.content.Context val$applicationContext;
    //
    //     Anon3(android.content.Context context) {
    //         this.val$applicationContext = context;
    //     }
    //
    //     public boolean onNavigationItemSelected(@android.support.annotation.NonNull android.view.MenuItem item) {
    //         switch (item.getItemId()) {
    //             case com.navdy.client.R.id.menu_car /*2131756046*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.CarSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_audio /*2131756047*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.AudioSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_calendar /*2131756048*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.CalendarSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_navigation /*2131756049*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.NavigationSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_messaging /*2131756050*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.MessagingSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_software_update /*2131756051*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.OtaSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_general /*2131756052*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.GeneralSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_support /*2131756053*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.SupportActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_legal /*2131756054*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(this.val$applicationContext, com.navdy.client.app.ui.settings.LegalSettingsActivity.class));
    //                 break;
    //             case com.navdy.client.R.id.menu_debug /*2131756055*/:
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.HomescreenActivity.this, com.navdy.client.debug.MainDebugActivity.class));
    //                 break;
    //             default:
    //                 return true;
    //         }
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.closeDrawer();
    //         return true;
    //     }
    // }
    //
    // class Anon4 implements android.view.View.OnClickListener {
    //     Anon4() {
    //     }
    //
    //     public void onClick(android.view.View v) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.startActivity(new android.content.Intent(com.navdy.client.app.ui.homescreen.HomescreenActivity.this.getApplicationContext(), com.navdy.client.app.ui.settings.ProfileSettingsActivity.class));
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.reloadProfile = true;
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.closeDrawer();
    //     }
    // }
    //
    // class Anon5 implements java.lang.Runnable {
    //     Anon5() {
    //     }
    //
    //     public void run() {
    //         com.navdy.client.app.framework.util.SuggestionManager.forceSuggestionFullRefresh();
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.rebuildSuggestions();
    //     }
    // }
    //
    // class Anon6 implements java.lang.Runnable {
    //     final /* synthetic */ com.navdy.client.app.framework.models.Destination val$destination;
    //
    //     Anon6(com.navdy.client.app.framework.models.Destination destination) {
    //         this.val$destination = destination;
    //     }
    //
    //     public void run() {
    //         com.navdy.client.app.tracking.SetDestinationTracker.getInstance().tagSetDestinationEvent(this.val$destination);
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.navdyRouteHandler.requestNewRoute(this.val$destination);
    //     }
    // }
    //
    // class Anon7 implements com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback {
    //     final /* synthetic */ int val$finalSpecialType;
    //
    //     class Anon1 implements com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback {
    //         Anon1() {
    //         }
    //
    //         public void onQueryCompleted(int nbRows, @android.support.annotation.Nullable android.net.Uri uri) {
    //             if (nbRows <= 0) {
    //                 com.navdy.client.app.ui.homescreen.HomescreenActivity.this.logger.e("Something went wrong while trying to save the favorite to the db.");
    //             }
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.onFavoriteListChanged();
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.hideProgressDialog();
    //         }
    //     }
    //
    //     Anon7(int i) {
    //         this.val$finalSpecialType = i;
    //     }
    //
    //     public void onSuccess(com.navdy.client.app.framework.models.Destination destination) {
    //         updateOrSaveFavoriteToDbAsync(destination);
    //     }
    //
    //     public void onFailure(com.navdy.client.app.framework.models.Destination destination, com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error error) {
    //         com.navdy.client.app.ui.homescreen.HomescreenActivity.this.logger.v("navigationHelper failed to get latLng for ContactModel Destination");
    //         updateOrSaveFavoriteToDbAsync(destination);
    //     }
    //
    //     private void updateOrSaveFavoriteToDbAsync(com.navdy.client.app.framework.models.Destination destination) {
    //         destination.saveDestinationAsFavoritesAsync(this.val$finalSpecialType, new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon7.Anon1());
    //     }
    // }
    //
    // class Anon8 implements java.lang.Runnable {
    //     Anon8() {
    //     }
    //
    //     public void run() {
    //         if (com.navdy.client.app.ui.homescreen.HomescreenActivity.this.viewPager != null && com.navdy.client.app.ui.homescreen.HomescreenActivity.this.pagerAdapter != null && com.navdy.client.app.ui.homescreen.HomescreenActivity.this.pagerAdapter.getCount() >= 2) {
    //             com.navdy.client.app.ui.favorites.FavoritesFragment favoritesFragment = (com.navdy.client.app.ui.favorites.FavoritesFragment) com.navdy.client.app.ui.homescreen.HomescreenActivity.this.pagerAdapter.getItem(1);
    //             if (favoritesFragment != null) {
    //                 if (favoritesFragment.favoritesAdapter != null) {
    //                     favoritesFragment.favoritesAdapter.notifyDbChanged();
    //                 }
    //                 favoritesFragment.showOrHideSplashScreen();
    //             }
    //             com.navdy.client.app.ui.homescreen.HomescreenActivity.this.rebuildSuggestions();
    //         }
    //     }
    // }
    //
    @DexAdd
    class LogoClicked implements android.view.View.OnClickListener {
        @DexIgnore
        int count;// = 0;

        @DexAdd
        HomescreenActivity this_;

        @DexAdd
        LogoClicked(HomescreenActivity _this) {
            this_ = _this;
        }

        @DexReplace
        public void onClick(android.view.View v) {
            this_.logger.d("Clicked on the logo " + (this.count + 1) + " time(s)");
            int i = this.count + 1;
            this.count = i;
            if (i == 3) { // Was 10
                com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.here_you_go);
                if (this_.pagerAdapter != null) {
                    this_.pagerAdapter.showHiddenTabs();
                    this_.tabLayout.setupWithViewPager(this_.viewPager);
                }
                this_.navigationView = (android.support.design.widget.NavigationView) this_.findViewById(com.navdy.client.R.id.nav_view);
                if (this_.navigationView != null) {
                    this_.updateMenuVisibility(true, com.navdy.client.R.id.menu_debug);
                }
                com.navdy.client.app.framework.util.SuggestionManager.forceSuggestionFullRefresh();
                this_.rebuildSuggestions();
                com.navdy.client.app.providers.NavdyContentProvider.clearCache();
            }
            if (this.count >= 10) { // Was 15
                com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.hide_all_tips);
                com.navdy.client.app.ui.homescreen.HomescreenActivity.hideAllTips();
                com.navdy.client.app.framework.util.SuggestionManager.forceSuggestionFullRefresh();
                this_.rebuildSuggestions();
            }
            if (this.count == 15) { // Was 20
                com.navdy.client.app.ui.base.BaseActivity.showShortToast(com.navdy.client.R.string.show_all_tips);
                this.count = 0;
                com.navdy.client.app.ui.homescreen.HomescreenActivity.showAllTips();
                com.navdy.client.app.framework.util.SuggestionManager.forceSuggestionFullRefresh();
                this_.rebuildSuggestions();
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    @DexAppend
    protected void onCreate(android.os.Bundle savedInstanceState) {
        this.mAppInstance.mHomescreenActivity = this;
        handler.postDelayed(() -> HomescreenActivity.this.startActivity(new Intent(HomescreenActivity.this, Subscription.class)), 5000);
    }

    @DexAdd
    void disableBatteryOpt() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }
    }

    // @DexIgnore
    // protected void onCreate(android.os.Bundle savedInstanceState) {
    //     this.logger.v("HomescreenActivity::onCreate" + java.lang.System.identityHashCode(this));
    //     super.onCreate(savedInstanceState);
    //     setContentView((int) com.navdy.client.R.layout.homescreen_container);
    //     this.mAppInstance = com.navdy.client.app.framework.AppInstance.getInstance();
    //     this.toolbar = (android.support.v7.widget.Toolbar) findViewById(com.navdy.client.R.id.homescreen_toolbar);
    //     setSupportActionBar(this.toolbar);
    //     setConnectionIndicator();
    //     setUpLogoEasterEgg();
    //     android.view.View navdyDisplay = findViewById(com.navdy.client.R.id.navdy_display);
    //     if (navdyDisplay != null) {
    //         navdyDisplay.setOnClickListener(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon1());
    //     }
    //     setupBurgerMenu();
    //     this.viewPager = (android.support.v4.view.ViewPager) findViewById(com.navdy.client.R.id.viewpager);
    //     setupViewPager(this.viewPager);
    //     this.tabLayout = (android.support.design.widget.TabLayout) findViewById(com.navdy.client.R.id.tab_layout);
    //     if (this.tabLayout != null) {
    //         this.tabLayout.setupWithViewPager(this.viewPager);
    //     }
    //     this.fab = (android.support.design.widget.FloatingActionButton) findViewById(com.navdy.client.R.id.fab);
    //     this.fab.startAnimation(android.view.animation.AnimationUtils.loadAnimation(this, com.navdy.client.R.anim.simple_grow));
    //     this.fab.setOnClickListener(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon2());
    //     setUpFloatingActionMenu();
    //     android.content.Intent i = getIntent();
    //     java.lang.String action = i.getAction();
    //     boolean isComingFromGoogleNow = com.google.android.gms.actions.SearchIntents.ACTION_SEARCH.equals(action) || com.navdy.client.app.ui.search.SearchConstants.ACTION_SEARCH.equals(action);
    //     this.logger.d("Search is coming from Google Now = " + isComingFromGoogleNow);
    //     if (isComingFromGoogleNow && i.hasExtra("query")) {
    //         android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.search.SearchActivity.class);
    //         intent.setAction(action);
    //         intent.putExtra("query", i.getStringExtra("query"));
    //         startActivityForResult(intent, com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.SEARCH.getCode());
    //         this.trackerEvent = com.navdy.client.app.tracking.TrackerConstants.Event.NAVIGATE_USING_GOOGLE_NOW;
    //     }
    // }

    @DexReplace
    protected void onResume() {
        boolean usesOreoPermissions;
        super.onResume();
        android.content.SharedPreferences sharedPreferences = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences();
        if (sharedPreferences.getInt(com.navdy.client.app.ui.settings.SettingsConstants.PERMISSIONS_VERSION, 1) > 25) {
            usesOreoPermissions = true;
        } else {
            usesOreoPermissions = false;
        }
        if (android.os.Build.VERSION.SDK_INT > 25 && !usesOreoPermissions) {
            if (weHaveThisPermission("android.permission.ACCESS_FINE_LOCATION")) {
                requestLocationPermission(null, null);
            }
            if (weHaveThisPermission("android.permission.RECEIVE_SMS")) {
                requestSmsPermission(null, null);
            }
            if (weHaveThisPermission("android.permission.CALL_PHONE")) {
                requestPhonePermission(null, null);
            }
            if (weHaveThisPermission("android.permission.WRITE_EXTERNAL_STORAGE")) {
                requestStoragePermission(null, null);
            }
            sharedPreferences.edit().putInt(com.navdy.client.app.ui.settings.SettingsConstants.PERMISSIONS_VERSION, 26).apply();
        }
        // if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(sharedPreferences.getString(com.navdy.client.app.ui.settings.SettingsConstants.GOOGLE_SERVICES_API_KEY, null))) {
        //     android.content.Intent intent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.homescreen.AskForGoogleApiKey.class);
        //     intent.putExtra(com.navdy.client.app.ui.homescreen.AskForGoogleApiKey.EXTRA_TYPE, com.navdy.client.app.ui.homescreen.AskForGoogleApiKey.TYPE_SERVICES);
        //     startActivity(intent);
        // }
        for (int i = 0; i < com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.getScreensCount(); i++) {
            com.navdy.client.app.ui.firstlaunch.AppSetupScreen screen = com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.getScreen(i);
            if (screen != null && screen.isMandatory && !com.navdy.client.app.ui.firstlaunch.AppSetupPagerAdapter.weHavePermissionForThisScreen(screen)) {
                this.logger.e("Screen " + i + "(" + screen + ") is mandatory and we seem to be missing this permission.");
                com.navdy.client.app.ui.firstlaunch.AppSetupActivity.goToAppSetup(this);
                finish();
            }
        }
        if (this.reloadProfile) {
            this.reloadProfile = false;
            setUsernameAndAddress();
        }
        updateHudVersionString();
        updateMessagingVisibility();
        android.content.Intent callingIntent = getIntent();
        if (callingIntent != this.lastExternalIntentHandled && handleIntent(callingIntent)) {
            this.lastExternalIntentHandled = callingIntent;
        }
        if (this.viewPager.getCurrentItem() != 1) {
            this.fab.setVisibility(View.GONE);
        }
        if (!com.navdy.client.app.NavdyApplication.isDeveloperBuild()) {
            checkForUpdates(null);
        }
        this.navdyRouteHandler.addListener(this);
        if (this.highlightGlanceSwitchOnResume) {
            this.highlightGlanceSwitchOnResume = false;
            if (this.viewPager != null && !isFinishing()) {
                this.viewPager.setCurrentItem(2);
                android.support.v4.app.Fragment fragment = this.pagerAdapter.getItem(2);
                if (fragment instanceof com.navdy.client.app.ui.glances.GlancesFragment) {
                    ((com.navdy.client.app.ui.glances.GlancesFragment) fragment).highlightGlanceSwitch();
                }
            }
        }
        showObdDialogIfCarHasBeenAddedToBlacklist();
        setConnectionIndicator();
    }

    @SuppressLint("MissingSuperCall")
    @DexIgnore
    protected void onPause() {
    //     this.navdyRouteHandler.removeListener(this);
    //     if (this.mode != null) {
    //         stopSelectionActionMode();
    //     }
    //     super.onPause();
    }

    @SuppressLint("MissingSuperCall")
    @DexIgnore
    protected void onDestroy() {
    //     super.onDestroy();
    //     this.logger.v("HomescreenActivity::onDestroy" + java.lang.System.identityHashCode(this));
    }

    @DexIgnore
    private void setupBurgerMenu() {
    //     android.support.v4.widget.DrawerLayout drawer = (android.support.v4.widget.DrawerLayout) findViewById(com.navdy.client.R.id.drawer_layout);
    //     if (drawer != null) {
    //         android.support.v7.app.ActionBarDrawerToggle toggle = new android.support.v7.app.ActionBarDrawerToggle(this, drawer, this.toolbar, com.navdy.client.R.string.menu_drawer_open, com.navdy.client.R.string.menu_drawer_close);
    //         drawer.addDrawerListener(toggle);
    //         toggle.syncState();
    //         this.navigationView = (android.support.design.widget.NavigationView) findViewById(com.navdy.client.R.id.nav_view);
    //         if (this.navigationView != null) {
    //             this.navigationView.setItemIconTintList(null);
    //             android.content.Context applicationContext = getApplicationContext();
    //             setUsernameAndAddress();
    //             android.view.Menu menu = this.navigationView.getMenu();
    //             if (menu != null) {
    //                 if (menu.findItem(com.navdy.client.R.id.menu_debug) != null) {
    //                 }
    //                 android.view.MenuItem messagingMenuItem = menu.findItem(com.navdy.client.R.id.menu_messaging);
    //                 if (messagingMenuItem != null) {
    //                     messagingMenuItem.setVisible(com.navdy.client.app.ui.settings.SettingsUtils.getPermissionsSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.HUD_CANNED_RESPONSE_CAPABLE, false));
    //                 }
    //                 android.view.MenuItem versionItem = menu.findItem(com.navdy.client.R.id.menu_app_version);
    //                 if (versionItem != null) {
    //                     android.widget.TextView appVersion = (android.widget.TextView) versionItem.getActionView().findViewById(com.navdy.client.R.id.menu_version);
    //                     if (appVersion != null) {
    //                         java.lang.String appVersionRes = getString(com.navdy.client.R.string.menu_app_version);
    //                         java.lang.String appVersionString = getAppVersionString();
    //                         if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(appVersionString, getString(com.navdy.client.R.string.unknown))) {
    //                             appVersionString = "v." + appVersionString;
    //                         }
    //                         appVersion.setText(java.lang.String.format(appVersionRes, new java.lang.Object[]{appVersionString}));
    //                     }
    //                     updateHudVersionString();
    //                 }
    //             }
    //             this.navigationView.setNavigationItemSelectedListener(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon3(applicationContext));
    //         }
    //     }
    }

    @DexIgnore
    private void updateHudVersionString() {
    //     android.content.Context applicationContext = getApplicationContext();
    //     android.view.Menu menu = this.navigationView.getMenu();
    //     if (menu != null) {
    //         android.view.MenuItem versionItem = menu.findItem(com.navdy.client.R.id.menu_hud_version);
    //         if (versionItem != null) {
    //             android.view.View actionView = versionItem.getActionView();
    //             android.widget.TextView hudVersion = (android.widget.TextView) actionView.findViewById(com.navdy.client.R.id.menu_version);
    //             if (hudVersion != null) {
    //                 java.lang.String hudVersionRes = getString(com.navdy.client.R.string.menu_display_version);
    //                 java.lang.String displayVersionString = getDisplayVersionString(applicationContext);
    //                 if (!com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(displayVersionString, getString(com.navdy.client.R.string.unknown))) {
    //                     displayVersionString = "v." + displayVersionString;
    //                 }
    //                 hudVersion.setText(java.lang.String.format(hudVersionRes, new java.lang.Object[]{displayVersionString}));
    //             }
    //             android.widget.TextView hudSerial = (android.widget.TextView) actionView.findViewById(com.navdy.client.R.id.menu_serial);
    //             if (hudSerial != null) {
    //                 hudSerial.setText(java.lang.String.format(getString(com.navdy.client.R.string.menu_display_serial), new java.lang.Object[]{getDisplaySerialString(applicationContext)}));
    //             }
    //         }
    //     }
    }

    @DexIgnore
    private void closeDrawer() {
    //     android.support.v4.widget.DrawerLayout drawer = (android.support.v4.widget.DrawerLayout) findViewById(com.navdy.client.R.id.drawer_layout);
    //     if (drawer != null) {
    //         drawer.closeDrawer((int) android.support.v4.view.GravityCompat.START);
    //     }
    }

    @DexIgnore
    public static java.lang.String getAppVersionString() {
        throw null;
    }
    //     android.content.pm.PackageInfo pInfo = null;
    //     android.content.Context context = com.navdy.client.app.NavdyApplication.getAppContext();
    //     try {
    //         pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
    //     } catch (android.content.pm.PackageManager.NameNotFoundException e) {
    //         e.printStackTrace();
    //     }
    //     if (pInfo == null || pInfo.versionName == null) {
    //         return context.getString(com.navdy.client.R.string.unknown);
    //     }
    //     return pInfo.versionName.split("-")[0];
    // }

    @DexIgnore
    private static java.lang.String getDisplayVersionString(android.content.Context context) {
        throw null;
    }
    //     com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
    //     if (remoteDevice != null) {
    //         com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
    //         if (!(deviceInfo == null || deviceInfo.clientVersion == null)) {
    //             java.lang.String[] clientVersionParts = deviceInfo.clientVersion.split("-");
    //             if (clientVersionParts.length > 0) {
    //                 return clientVersionParts[0];
    //             }
    //         }
    //     }
    //     java.lang.String lastDeviceVersionText = com.navdy.client.ota.OTAUpdateService.getLastDeviceVersionText();
    //     return com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(lastDeviceVersionText) ? context.getString(com.navdy.client.R.string.unknown) : lastDeviceVersionText;
    // }

    @DexIgnore
    private static java.lang.String getDisplaySerialString(android.content.Context context) {
        throw null;
    }
    //     com.navdy.service.library.device.RemoteDevice remoteDevice = com.navdy.client.app.framework.AppInstance.getInstance().getRemoteDevice();
    //     if (remoteDevice != null) {
    //         com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
    //         if (deviceInfo != null) {
    //             return deviceInfo.deviceUuid;
    //         }
    //     }
    //     return context.getString(com.navdy.client.R.string.unknown);
    // }

    @DexIgnore
    private void setUsernameAndAddress() {
    //     if (this.navigationView != null) {
    //         android.view.View headerView = this.navigationView.getHeaderView(0);
    //         if (headerView != null) {
    //             headerView.setOnClickListener(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon4());
    //             com.navdy.client.app.framework.models.UserAccountInfo accountInfo = com.navdy.client.app.framework.models.UserAccountInfo.getUserAccountInfo();
    //             com.navdy.client.app.ui.customviews.DestinationImageView accountPhoto = (com.navdy.client.app.ui.customviews.DestinationImageView) headerView.findViewById(com.navdy.client.R.id.account_photo);
    //             if (accountPhoto != null) {
    //                 if (accountInfo.photo != null) {
    //                     accountPhoto.setImage(accountInfo.photo);
    //                 } else {
    //                     accountPhoto.setImage(com.navdy.client.R.drawable.icon_profile_user_blue, accountInfo.fullName);
    //                 }
    //             }
    //             android.widget.TextView accountName = (android.widget.TextView) headerView.findViewById(com.navdy.client.R.id.account_name);
    //             if (accountName != null) {
    //                 if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(accountInfo.fullName)) {
    //                     accountName.setText(accountInfo.fullName);
    //                 } else {
    //                     accountName.setText(com.navdy.client.R.string.go_to_profile_to_set_name);
    //                 }
    //             }
    //             android.widget.TextView accountEmail = (android.widget.TextView) headerView.findViewById(com.navdy.client.R.id.account_email);
    //             if (accountEmail == null) {
    //                 return;
    //             }
    //             if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(accountInfo.email)) {
    //                 accountEmail.setText(accountInfo.email);
    //             } else if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(accountInfo.fullName)) {
    //                 accountEmail.setText(com.navdy.client.R.string.and_email);
    //             } else {
    //                 accountEmail.setText(com.navdy.client.R.string.go_to_profile_to_set_email);
    //             }
    //         }
    //     }
    }

    @DexIgnore
    public void onBackPressed() {
    //     android.support.v4.widget.DrawerLayout drawer = (android.support.v4.widget.DrawerLayout) findViewById(com.navdy.client.R.id.drawer_layout);
    //     if (isEnding(this)) {
    //         this.logger.w("Activity already ending so ignoring onBackPressed.");
    //     } else if (drawer != null && drawer.isDrawerOpen((int) android.support.v4.view.GravityCompat.START)) {
    //         drawer.closeDrawer((int) android.support.v4.view.GravityCompat.START);
    //     } else if (this.viewPager.getCurrentItem() != 0) {
    //         this.viewPager.setCurrentItem(0);
    //         this.tabLayout.setupWithViewPager(this.viewPager);
    //         hideOtherFabs();
    //     } else {
    //         super.onBackPressed();
    //     }
    }

    @DexIgnore
    public void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
    //     this.logger.v("onActivityResult: requestCode = " + requestCode + " resultCode = " + resultCode);
    //     if (resultCode == -1) {
    //         switch (requestCode) {
    //             case 0:
    //                 com.navdy.client.app.tracking.Tracker.tagEvent(this.trackerEvent);
    //                 this.trackerEvent = com.navdy.client.app.tracking.TrackerConstants.Event.NAVIGATE_USING_SEARCH_RESULTS;
    //                 sendDestinationToNavdy(resultCode, data);
    //                 return;
    //             case 1:
    //             case 2:
    //             case 3:
    //                 saveDestinationToFavorites(requestCode, resultCode, data);
    //                 return;
    //             case 5:
    //                 if (data.getBooleanExtra(com.navdy.client.app.ui.details.DetailsActivity.EXTRA_UPDATED_DESTINATION, false)) {
    //                     this.logger.d("Details Code reached");
    //                     com.navdy.client.app.framework.models.Destination destination = com.navdy.client.app.framework.navigation.NavdyRouteHandler.getInstance().getCurrentDestination();
    //                     if (destination != null) {
    //                         this.logger.d("destination contents: " + destination.isFavoriteDestination());
    //                         destination.reloadSelfFromDatabaseAsync(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon5());
    //                         return;
    //                     }
    //                     return;
    //                 }
    //                 return;
    //             default:
    //                 return;
    //         }
    //     }
    }

    @DexIgnore
    private void sendDestinationToNavdy(int resultCode, android.content.Intent data) {
    //     if (resultCode == -1) {
    //         com.navdy.client.app.framework.models.Destination destination = (com.navdy.client.app.framework.models.Destination) data.getParcelableExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT);
    //         this.logger.v("destination address: " + destination.rawAddressNotForDisplay);
    //         java.lang.Runnable requestRouteRunnable = new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon6(destination);
    //         if (data.getBooleanExtra(com.navdy.client.app.ui.search.SearchConstants.EXTRA_IS_VOICE_SEARCH, false)) {
    //             requestRouteRunnable.run();
    //         } else {
    //             showRequestNewRouteDialog(requestRouteRunnable);
    //         }
    //     }
    }

    @DexIgnore
    private void saveDestinationToFavorites(int requestCode, int resultCode, android.content.Intent data) {
    //     int specialType;
    //     if (resultCode == -1) {
    //         com.navdy.client.app.framework.models.Destination d = (com.navdy.client.app.framework.models.Destination) data.getParcelableExtra(com.navdy.client.app.ui.search.SearchConstants.SEARCH_RESULT);
    //         this.logger.v("saveDestinationToFavorites: " + d.rawAddressNotForDisplay);
    //         showProgressDialog();
    //         d.setFavoriteLabel(d.name);
    //         switch (requestCode) {
    //             case 2:
    //                 specialType = -3;
    //                 d.setFavoriteLabel(getString(com.navdy.client.R.string.home));
    //                 com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SET_HOME);
    //                 break;
    //             case 3:
    //                 specialType = -2;
    //                 d.setFavoriteLabel(getString(com.navdy.client.R.string.work));
    //                 com.navdy.client.app.tracking.Tracker.tagEvent(com.navdy.client.app.tracking.TrackerConstants.Event.SET_WORK);
    //                 break;
    //             default:
    //                 if (d.type != com.navdy.client.app.framework.models.Destination.Type.CONTACT) {
    //                     specialType = -1;
    //                     break;
    //                 } else {
    //                     specialType = -4;
    //                     break;
    //                 }
    //         }
    //         com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(d, new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon7(specialType));
    //     }
    }

    @DexIgnore
    public void onFavoriteListChanged() {
    //     runOnUiThread(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon8());
    }

    @DexIgnore
    public void rebuildSuggestions() {
    //     if (this.pagerAdapter != null) {
    //         com.navdy.client.app.ui.homescreen.SuggestionsFragment suggestionsFragment = (com.navdy.client.app.ui.homescreen.SuggestionsFragment) this.pagerAdapter.getItem(0);
    //         if (suggestionsFragment != null && suggestionsFragment.suggestionAdapter != null) {
    //             suggestionsFragment.suggestionAdapter.rebuildSuggestions();
    //         }
    //     }
    }

    @DexIgnore
    private void setupViewPager(android.support.v4.view.ViewPager viewPager2) {
    //     this.pagerAdapter = new com.navdy.client.app.ui.homescreen.HomescreenPagerAdapter(getSupportFragmentManager());
    //     viewPager2.setAdapter(this.pagerAdapter);
    //     viewPager2.setOffscreenPageLimit(3);
    //     viewPager2.addOnPageChangeListener(this);
    }

    @DexReplace
    private void setUpLogoEasterEgg() {
        android.widget.ImageView logo = (android.widget.ImageView) findViewById(com.alelec.navdyclient.R.id.navdy_logo);
        if (logo != null) {
            logo.setOnClickListener(new LogoClicked(this));
        }
    }

    @DexIgnore
    private void updateMenuVisibility(boolean visible, @android.support.annotation.IdRes int menuItemId) {
    //     android.view.Menu menu = this.navigationView.getMenu();
    //     if (menu != null) {
    //         android.view.MenuItem messagingMenuItem = menu.findItem(menuItemId);
    //         if (messagingMenuItem != null) {
    //             messagingMenuItem.setVisible(visible);
    //         }
    //     }
    }

    @DexIgnore
    private static void showAllTips() {
    //     android.content.SharedPreferences.Editor editor = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit();
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_WATCHED_THE_DEMO, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, false);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, false);
    //     editor.apply();
    }

    @DexIgnore
    public static void hideAllTips() {
    //     android.content.SharedPreferences.Editor editor = com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().edit();
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_WATCHED_THE_DEMO, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, true);
    //     editor.putBoolean(com.navdy.client.app.ui.settings.SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, true);
    //     editor.apply();
    }

    @DexIgnore
    private void setConnectionIndicator() {
    //     android.widget.ImageView indicator = (android.widget.ImageView) findViewById(com.navdy.client.R.id.navdy_display);
    //     if (indicator != null) {
    //         if (this.mAppInstance.isDeviceConnected()) {
    //             indicator.setImageResource(com.navdy.client.R.drawable.icon_display_connected);
    //         } else {
    //             indicator.setImageResource(com.navdy.client.R.drawable.icon_display_disconnected);
    //         }
    //         showConnectivityBanner();
    //     }
    }

    @DexAdd
    public void pulseConnectionIndicator() {
        this.handler.post(new Runnable() {
            public void run() {

                final ImageView indicator = (ImageView) findViewById(R.id.navdy_display);
                if (indicator != null) {
//                    indicator.setBackgroundColor(getResources().getColor(R.color.grey));
                    indicator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.rounded_grey));

                    HomescreenActivity.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            indicator.setBackground(null);
//                            indicator.setBackgroundColor(getResources().getColor(R.color.transparent));
                        }
                    }, 150);
                }
            }});
    }

    @DexIgnore
    public void hideConnectionBanner(android.view.View v) {
    //     android.widget.RelativeLayout banner = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.connection_success_banner);
    //     if (banner != null) {
    //         banner.setVisibility(View.GONE);
    //     }
    //     android.widget.RelativeLayout banner2 = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.connection_failure_banner);
    //     if (banner2 != null) {
    //         banner2.setVisibility(View.GONE);
    //     }
    }

    @DexIgnore
    private void showConnectivityBanner() {
    //     android.widget.RelativeLayout successBanner = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.connection_success_banner);
    //     android.widget.RelativeLayout failureBanner = (android.widget.RelativeLayout) findViewById(com.navdy.client.R.id.connection_failure_banner);
    //     if (successBanner != null && failureBanner != null) {
    //         if (this.mAppInstance.isDeviceConnected()) {
    //             successBanner.setVisibility(View.VISIBLE);
    //             failureBanner.setVisibility(View.GONE);
    //             android.widget.TextView text = (android.widget.TextView) findViewById(com.navdy.client.R.id.connection_success_banner_text);
    //             if (text != null) {
    //                 com.navdy.service.library.device.RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
    //                 java.lang.String deviceName = null;
    //                 if (remoteDevice != null) {
    //                     com.navdy.service.library.events.DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
    //                     if (deviceInfo != null) {
    //                         deviceName = deviceInfo.deviceName;
    //                     }
    //                 }
    //                 if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(deviceName)) {
    //                     text.setText(getString(com.navdy.client.R.string.connected_to, new java.lang.Object[]{deviceName}));
    //                 } else {
    //                     text.setText(com.navdy.client.R.string.navdy_connected);
    //                 }
    //             }
    //             this.handler.postDelayed(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon10(), 2000);
    //             return;
    //         }
    //         successBanner.setVisibility(View.GONE);
    //         failureBanner.setVisibility(View.VISIBLE);
    //     }
    }

    @DexIgnore
    public void onFailureBannerClick(android.view.View v) {
    //     if (!this.mAppInstance.isDeviceConnected()) {
    //         openBtConnectionDialog();
    //     }
    }

    @DexIgnore
    public void onSuccessBannerClick(android.view.View v) {
    //     this.handler.removeCallbacksAndMessages(null);
    //     showQuestionDialog(com.navdy.client.R.string.disconnect_from_display, com.navdy.client.R.string.disconnect_from_display_desc, com.navdy.client.R.string.yes_button, com.navdy.client.R.string.cancel_button, new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon11(), null);
    }

    @DexIgnore
    public void onHelpConnectClick(android.view.View v) {
    //     startActivity(new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.settings.SupportActivity.class));
    }

    @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceConnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent deviceConnectedEvent) {
    //     this.logger.v("onDeviceConnectedEvent: " + deviceConnectedEvent);
    //     setConnectionIndicator();
    //     updateMessagingVisibility();
    }

    @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceDisconnectedEvent(com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent deviceDisconnectedEvent) {
    //     this.logger.v("onDeviceDisconnectedEvent: " + deviceDisconnectedEvent);
    //     setConnectionIndicator();
    }

    @com.squareup.otto.Subscribe
    @DexIgnore
    public void onDeviceInfoEvent(com.navdy.client.app.framework.DeviceConnection.DeviceInfoEvent deviceInfoEvent) {
    //     this.logger.v("onDeviceInfoEvent: " + deviceInfoEvent);
    //     updateHudVersionString();
    //     updateMessagingVisibility();
    }

    @DexIgnore
    private void updateMessagingVisibility() {
        // updateMenuVisibility(com.navdy.client.app.ui.settings.SettingsUtils.getSharedPreferences().getBoolean(com.navdy.client.app.ui.settings.SettingsConstants.MESSAGING_HAS_SEEN_CAPABLE_HUD, false), com.navdy.client.R.id.menu_messaging);
    }

    @DexIgnore
    private void pickFabAction() {
    //     int position = this.viewPager.getCurrentItem();
    //     this.logger.v("pickFabAction: " + position);
    //     switch (position) {
    //         case 1:
    //             favoritesFragmentFabAction();
    //             return;
    //         default:
    //             this.logger.e("Unsupported viewPager position for fab: " + position);
    //             return;
    //     }
    }

    @DexIgnore
    private void favoritesFragmentFabAction() {
    //     this.logger.v("favoritesFragmentFabAction");
    //     if (this.isFabShowing || (com.navdy.client.app.providers.NavdyContentProvider.isHomeSet() && com.navdy.client.app.providers.NavdyContentProvider.isWorkSet())) {
    //         callSearchFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.FAVORITE);
    //     } else {
    //         showOtherFabs();
    //     }
    }

    @DexIgnore
    private void setUpFloatingActionMenu() {
    //     this.addFavoriteLabel = (android.widget.TextView) findViewById(com.navdy.client.R.id.addFavoriteLabel);
    //     this.addHome = (android.support.design.widget.FloatingActionButton) findViewById(com.navdy.client.R.id.addHome);
    //     this.addHomeLabel = (android.widget.TextView) findViewById(com.navdy.client.R.id.addHomeLabel);
    //     this.addWork = (android.support.design.widget.FloatingActionButton) findViewById(com.navdy.client.R.id.addWork);
    //     this.addWorkLabel = (android.widget.TextView) findViewById(com.navdy.client.R.id.addWorkLabel);
    //     this.film = findViewById(com.navdy.client.R.id.film);
    //     this.addHome.setOnClickListener(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon12());
    //     this.addWork.setOnClickListener(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon13());
    //     this.film.setOnClickListener(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon14());
    }

    @DexIgnore
    private void callSearchFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES searchType) {
    //     callSearchFor(searchType, false);
    }

    @DexIgnore
    private void callSearchFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES searchType, boolean searchMic) {
    //     hideOtherFabs();
    //     com.navdy.client.app.ui.search.SearchActivity.startSearchActivityFor(searchType, searchMic, this);
    }

    @DexIgnore
    private void showOtherFabs() {
    //     this.isFabShowing = true;
    //     setOtherFabsVisibility(0);
    }

    @DexIgnore
    private void hideOtherFabs() {
    //     this.isFabShowing = false;
    //     setOtherFabsVisibility(8);
    }

    @DexIgnore
    private void setOtherFabsVisibility(int visibility) {
    //     this.addFavoriteLabel.setVisibility(visibility);
    //     if (!com.navdy.client.app.providers.NavdyContentProvider.isHomeSet()) {
    //         this.addHome.setVisibility(visibility);
    //         this.addHomeLabel.setVisibility(visibility);
    //     }
    //     if (!com.navdy.client.app.providers.NavdyContentProvider.isWorkSet()) {
    //         this.addWork.setVisibility(visibility);
    //         this.addWorkLabel.setVisibility(visibility);
    //     }
    //     this.film.setVisibility(visibility);
    }

    @DexIgnore
    public void onPageScrollStateChanged(int state) {
    }

    @DexIgnore
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @DexIgnore
    public void onPageSelected(int position) {
    //     this.logger.v("onPageSelected: " + position);
    //     switch (position) {
    //         case 1:
    //             this.fab.setVisibility(View.VISIBLE);
    //             this.fab.hide(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon15());
    //             break;
    //         default:
    //             this.fab.hide();
    //             break;
    //     }
    //     if (position != 0) {
    //         stopSelectionActionMode();
    //     }
    //     hideConnectionBanner(null);
    //     com.navdy.client.app.tracking.Tracker.tagScreen(com.navdy.client.app.tracking.TrackerConstants.Screen.Home.tag(position));
    }

    @DexIgnore
    private boolean handleIntent(@android.support.annotation.NonNull android.content.Intent callingIntent) {
        throw null;
    //     try {
    //         boolean isActionView = "android.intent.action.VIEW".equals(callingIntent.getAction());
    //         if (!isActionView || !callingIntent.hasExtra(EXTRA_DESTINATION)) {
    //             this.logger.d("handleIntent: " + callingIntent.getAction());
    //             com.navdy.client.app.framework.util.SystemUtils.logIntentExtras(callingIntent, this.logger);
    //             com.navdy.client.app.tracking.SetDestinationTracker setDestinationTracker = com.navdy.client.app.tracking.SetDestinationTracker.getInstance();
    //             setDestinationTracker.setSourceValue(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES.THIRD_PARTY_INTENT);
    //             setDestinationTracker.setDestinationType(com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES.INTENT);
    //             if (isActionView) {
    //                 this.logger.v("ACTION_VIEW Intent found");
    //                 android.net.Uri uri = callingIntent.getData();
    //                 java.lang.String host = uri.getHost();
    //                 this.logger.v("uri: " + uri);
    //                 this.logger.v("uri host: " + host);
    //                 this.logger.v("uri scheme: " + uri.getScheme());
    //                 this.logger.v("uri schemeSpecificPart: " + uri.getSchemeSpecificPart());
    //                 if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(host, GOOGLE_MAPS_HOST) || com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(uri.getScheme(), GOOGLE_NAVIGATION_SCHEME)) {
    //                     routeToUri(uri);
    //                     return true;
    //                 } else if (com.navdy.client.app.framework.util.StringUtils.equalsOrBothEmptyAfterTrim(uri.getScheme(), "geo")) {
    //                     routeToGeoUri(uri);
    //                     return true;
    //                 }
    //             } else if ("android.intent.action.SEND".equals(callingIntent.getAction()) && callingIntent.getType().equals("text/plain")) {
    //                 java.lang.String sharedText = callingIntent.getStringExtra("android.intent.extra.TEXT");
    //                 if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(sharedText)) {
    //                     this.logger.d("ACTION_SEND Intent had bad text");
    //                     return false;
    //                 }
    //                 this.logger.i("ACTION_SEND Intent found: " + sharedText);
    //                 searchOrRouteToSharedText(sharedText, callingIntent);
    //                 return true;
    //             }
    //             return false;
    //         }
    //         new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon16(callingIntent).execute(new java.lang.Void[0]);
    //         return true;
    //     } catch (java.lang.Exception e) {
    //         this.logger.e((java.lang.Throwable) e);
    //     }
    }

    @DexIgnore
    private void routeToUri(@android.support.annotation.NonNull android.net.Uri uri) {
    //     java.lang.String query;
    //     this.logger.v("routeToUri: " + uri);
    //     java.lang.String address = null;
    //     java.lang.String latLngStr = null;
    //     java.lang.String daddr = null;
    //     java.lang.String query2 = null;
    //     if (uri.isHierarchical()) {
    //         daddr = uri.getQueryParameter("daddr");
    //         query2 = uri.getQueryParameter("q");
    //     }
    //     java.lang.String schemeSpecificPart = uri.getSchemeSpecificPart();
    //     if (daddr != null) {
    //         java.lang.String[] parts = daddr.split(" +?@");
    //         if (parts.length > 1) {
    //             address = parts[0];
    //             latLngStr = parts[1];
    //         } else if (parts[0].matches(com.navdy.client.app.framework.util.StringUtils.LAT_LNG_REGEXP)) {
    //             latLngStr = parts[0];
    //         } else {
    //             address = parts[0];
    //         }
    //     } else if (schemeSpecificPart != null && schemeSpecificPart.startsWith("q=")) {
    //         java.lang.String query3 = schemeSpecificPart.substring(2, schemeSpecificPart.length()).trim();
    //         try {
    //             query = java.net.URLDecoder.decode(query3, "UTF-8");
    //         } catch (java.io.UnsupportedEncodingException e) {
    //             query = query3.replaceAll("%2C", ",");
    //             e.printStackTrace();
    //         }
    //         if (com.navdy.client.app.framework.AppInstance.getInstance().canReachInternet()) {
    //             routeToFirstResultFor(query);
    //             return;
    //         }
    //         java.lang.String address2 = null;
    //         com.google.android.gms.maps.model.LatLng latLng = com.navdy.client.app.framework.map.MapUtils.parseLatLng(query);
    //         if (latLng == null) {
    //             address2 = query;
    //         }
    //         routeToAddressOrCoords(null, address2, latLng);
    //         return;
    //     } else if (query2 != null) {
    //         java.util.regex.Matcher matcher = java.util.regex.Pattern.compile(com.navdy.client.app.framework.util.StringUtils.LAT_LNG_REGEXP).matcher(query2);
    //         if (matcher.find()) {
    //             routeToAddressOrCoords(query2.substring(matcher.end(), query2.length()).replace("(", "").replace(")", "").trim(), null, com.navdy.client.app.framework.map.MapUtils.parseLatLng(matcher.group()));
    //             return;
    //         }
    //     }
    //     if (uri.isHierarchical()) {
    //         if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address)) {
    //             address = uri.getQueryParameter("q");
    //         }
    //         if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(latLngStr)) {
    //             latLngStr = uri.getQueryParameter("ll");
    //         }
    //     }
    //     routeToAddressOrCoords(null, address, com.navdy.client.app.framework.map.MapUtils.parseLatLng(latLngStr));
    }

    @DexIgnore
    private void routeToGeoUri(android.net.Uri uri) {
    //     sLogger.d("routeToGeoUri: " + uri);
    //     if (uri == null) {
    //         this.logger.e("Unable to route to a null URI");
    //         return;
    //     }
    //     java.lang.String partsString = android.net.Uri.decode(uri.getSchemeSpecificPart());
    //     if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(partsString)) {
    //         java.lang.String[] parts = partsString.split("\\?");
    //         java.lang.String address = null;
    //         com.google.android.gms.maps.model.LatLng latLng = null;
    //         if (parts.length > 0) {
    //             latLng = com.navdy.client.app.framework.map.MapUtils.parseLatLng(parts[0]);
    //         }
    //         java.lang.String q = parseParamValue(uri, "q");
    //         if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(q)) {
    //             if (latLng == null) {
    //                 latLng = com.navdy.client.app.framework.map.MapUtils.parseLatLng(q);
    //             }
    //             address = parseLabel(q);
    //             if (com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address)) {
    //                 address = q;
    //             }
    //         }
    //         routeToAddressOrCoords(null, address, latLng);
    //     }
    }

    @DexIgnore
    private void searchOrRouteToSharedText(java.lang.String sharedText, android.content.Intent intent) {
    //     com.navdy.service.library.task.TaskManager.getInstance().execute(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon17(sharedText, intent), 3);
    }

    @DexIgnore
    public void routeToFirstResultFor(java.lang.String searchTerm) {
    //     android.content.Intent searchIntent = new android.content.Intent(getApplicationContext(), com.navdy.client.app.ui.search.SearchActivity.class);
    //     searchIntent.setAction(com.navdy.client.app.ui.search.SearchConstants.ACTION_SEND);
    //     searchIntent.putExtra("query", searchTerm);
    //     runOnUiThread(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon18(searchIntent));
    }

    @DexIgnore
    private void routeToAddressOrCoords(@android.support.annotation.Nullable java.lang.String name, @android.support.annotation.Nullable java.lang.String address, @android.support.annotation.Nullable com.google.android.gms.maps.model.LatLng latLng) {
    //     this.logger.i("routeToAddressOrCoords, address: " + address + "\n " + "lat/lng: " + latLng);
    //     if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(name) || !com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address) || com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(latLng)) {
    //         com.navdy.client.app.framework.models.Destination destination = new com.navdy.client.app.framework.models.Destination();
    //         if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(name)) {
    //             destination.name = name;
    //         }
    //         if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address)) {
    //             destination.rawAddressNotForDisplay = address;
    //         }
    //         if (com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(latLng)) {
    //             destination.displayLat = latLng.latitude;
    //             destination.displayLong = latLng.longitude;
    //         }
    //         if (!com.navdy.client.app.framework.util.StringUtils.isEmptyAfterTrim(address) || !com.navdy.client.app.framework.map.MapUtils.isValidSetOfCoordinates(latLng)) {
    //             showProgressDialog();
    //             com.navdy.client.app.framework.map.NavCoordsAddressProcessor.processDestination(destination, new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon20());
    //             return;
    //         }
    //         destination.setCoordsToSame(latLng);
    //         com.navdy.client.app.framework.map.MapUtils.doReverseGeocodingFor(latLng, destination, new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon19(destination));
    //         return;
    //     }
    //     showLongToast(com.navdy.client.R.string.could_not_route_to_intent, new java.lang.Object[0]);
    }

    @DexIgnore
    private void routeToDestination(com.navdy.client.app.framework.models.Destination destination) {
    //     showRequestNewRouteDialog(new com.navdy.client.app.ui.homescreen.HomescreenActivity.Anon21(destination));
    }

    @android.support.annotation.Nullable
    @DexIgnore
    private java.lang.String parseLabel(java.lang.String str) {
        throw null;
    //     java.util.regex.Matcher matcher = java.util.regex.Pattern.compile(com.navdy.client.app.framework.util.StringUtils.LAT_LNG_REGEXP).matcher(str);
    //     if (!matcher.find()) {
    //         return str;
    //     }
    //     java.lang.String latLngStr = matcher.group();
    //     if (matcher.end() == str.length()) {
    //         return latLngStr;
    //     }
    //     return str.substring(matcher.end(), str.length()).replace("(", "").replace(")", "").trim();
    }

    @DexIgnore
    private java.lang.String parseParamValue(android.net.Uri uri, java.lang.String param) {
        throw null;
    //     android.net.UrlQuerySanitizer querySanitizer = new android.net.UrlQuerySanitizer(uri.toString());
    //     querySanitizer.registerParameter(param, android.net.UrlQuerySanitizer.getAllButNulAndAngleBracketsLegal());
    //     try {
    //         java.lang.String value = querySanitizer.getValue(param);
    //         if (value != null) {
    //             return java.net.URLDecoder.decode(value.replaceAll("_", " "), java.nio.charset.Charset.defaultCharset().name());
    //         }
    //         return null;
    //     } catch (java.io.UnsupportedEncodingException e) {
    //         this.logger.e("Couldn't decode parameter '" + param + "' in uri: " + uri);
    //         return null;
    //     }
    }

    @DexIgnore
    private void checkForUpdates(net.hockeyapp.android.UpdateManagerListener listener) {
    }

    @DexIgnore
    public void onSearchClick(android.view.View view) {
    //     callSearchFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.SEARCH);
    }

    @DexIgnore
    public void onSearchMicClick(android.view.View view) {
    //     callSearchFor(com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES.SEARCH, true);
    }

    @DexIgnore
    void startSelectionActionMode(android.support.v7.view.ActionMode.Callback actionModeCallback) {
    //     this.mode = startSupportActionMode(actionModeCallback);
    }

    @DexIgnore
    private void stopSelectionActionMode() {
    //     if (this.mode != null) {
    //         this.mode.finish();
    //     }
    }

    @DexIgnore
    protected void onNewIntent(android.content.Intent intent) {
    //     super.onNewIntent(intent);
    //     setIntent(intent);
    //     if (intent.getBooleanExtra(EXTRA_HOMESCREEN_EXPLICITLY_ROUTING, false)) {
    //         this.viewPager.setCurrentItem(0);
    //     }
    }

    @DexIgnore
    public void onPendingRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    @DexIgnore
    public void onPendingRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo pendingRoute) {
    }

    @DexIgnore
    public void onRouteCalculating(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    //     com.navdy.client.app.ui.routing.RoutingActivity.startRoutingActivity(this);
    }

    @DexIgnore
    public void onRouteCalculated(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error error, @android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
    //     openStartTab();
    }

    @DexIgnore
    public void onRouteStarted(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo route) {
    //     openStartTab();
    }

    @DexIgnore
    private void openStartTab() {
    //     android.support.v4.widget.DrawerLayout drawer = (android.support.v4.widget.DrawerLayout) findViewById(com.navdy.client.R.id.drawer_layout);
    //     if (drawer != null && drawer.isDrawerOpen((int) android.support.v4.view.GravityCompat.START)) {
    //         drawer.closeDrawer((int) android.support.v4.view.GravityCompat.START);
    //     }
    //     if (this.viewPager.getCurrentItem() != 0) {
    //         this.viewPager.setCurrentItem(0);
    //         this.tabLayout.setupWithViewPager(this.viewPager);
    //         hideOtherFabs();
    //     }
    }

    @DexIgnore
    public void onTripProgress(@android.support.annotation.NonNull com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo progress) {
    }

    @DexIgnore
    public void onReroute() {
    }

    @DexIgnore
    public void onRouteArrived(@android.support.annotation.NonNull com.navdy.client.app.framework.models.Destination destination) {
    }

    @DexIgnore
    public void onStopRoute() {
    }

    @DexIgnore
    void goToFavoritesTab() {
    //     if (this.viewPager != null && !isFinishing()) {
    //         this.viewPager.setCurrentItem(1);
    //     }
    }

    @DexIgnore
    void goToGlancesTab() {
    //     if (this.viewPager != null && !isFinishing()) {
    //         this.viewPager.setCurrentItem(2);
    //         this.highlightGlanceSwitchOnResume = true;
    //     }
    }

    @DexIgnore
    void goToGesture() {
    //     startActivity(new android.content.Intent(this, com.navdy.client.app.ui.settings.GeneralSettingsActivity.class));
    }

    @DexIgnore
    public void onRefreshConnectivityClick(android.view.View view) {
    //     com.navdy.client.app.framework.AppInstance.getInstance().checkForNetwork();
    }
}
