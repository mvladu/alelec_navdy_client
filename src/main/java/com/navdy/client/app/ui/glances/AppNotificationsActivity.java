package com.navdy.client.app.ui.glances;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import com.navdy.client.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.settings.SettingsUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;

@DexEdit
public class AppNotificationsActivity extends BaseToolbarActivity {
    @DexIgnore
    private GlancesAppAdapter listAdaptor; // = null;
    @DexIgnore
    private PackageManager packageManager;
    @DexIgnore
    private SharedPreferences sharedPrefs;

    // @DexIgnore
    // private class LoadApplications extends AsyncTask<Void, Void, Void> {
    //
    //     class Anon1 implements Comparator<ApplicationInfo> {
    //         Anon1() {
    //         }
    //
    //         public int compare(ApplicationInfo lhs, ApplicationInfo rhs) {
    //             if (lhs == rhs) {
    //                 return 0;
    //             }
    //             if (lhs == null) {
    //                 return -1;
    //             }
    //             if (rhs == null) {
    //                 return 1;
    //             }
    //             try {
    //                 if ("/system/priv-app/Keyguard.apk".equals(lhs.sourceDir) || "/system/priv-app/Keyguard.apk".equals(rhs.sourceDir)) {
    //                     return lhs.packageName.compareTo(rhs.packageName);
    //                 }
    //                 CharSequence obj1 = lhs.loadLabel(AppNotificationsActivity.this.packageManager);
    //                 CharSequence obj2 = rhs.loadLabel(AppNotificationsActivity.this.packageManager);
    //                 if (obj1 == obj2) {
    //                     return 0;
    //                 }
    //                 if (obj1 == null) {
    //                     return -1;
    //                 }
    //                 if (obj2 == null) {
    //                     return 1;
    //                 }
    //                 return obj1.toString().compareToIgnoreCase(obj2.toString());
    //             } catch (Exception e) {
    //                 return lhs.packageName.compareTo(rhs.packageName);
    //             }
    //         }
    //     }
    //
    //     @DexIgnore
    //     private LoadApplications() {
    //     }
    //
    //     /* access modifiers changed from: protected */
    //     @DexIgnore
    //     public Void doInBackground(Void... params) {
    //         String dialerPackage = SettingsUtils.getDialerPackage(AppNotificationsActivity.this.packageManager);
    //         AppNotificationsActivity.this.logger.d("GLANCES: The dialer package is: " + dialerPackage);
    //         if (!AppNotificationsActivity.this.sharedPrefs.contains(dialerPackage)) {
    //             GlanceUtils.saveGlancesConfigurationChanges(dialerPackage, true);
    //         }
    //         String messengerPackage = SettingsUtils.getSmsPackage();
    //         AppNotificationsActivity.this.logger.d("GLANCES: The messenger package is: " + messengerPackage);
    //         if (!AppNotificationsActivity.this.sharedPrefs.contains(messengerPackage)) {
    //             GlanceUtils.saveGlancesConfigurationChanges(messengerPackage, true);
    //         }
    //         List<ApplicationInfo> appList = AppNotificationsActivity.this.removeAppsThatWeDontWant(AppNotificationsActivity.this.packageManager.getInstalledApplications(128));
    //         try {
    //             Collections.sort(appList, new AppNotificationsActivity.LoadApplications.Anon1());
    //         } catch (Exception e) {
    //             AppNotificationsActivity.this.logger.e("Unable to sort the list of apps.", e);
    //         }
    //         Context context = AppNotificationsActivity.this.getApplicationContext();
    //         if (context == null) {
    //             context = NavdyApplication.getAppContext();
    //         }
    //         if (context != null) {
    //             AppNotificationsActivity.this.listAdaptor = new GlancesAppAdapter(context, R.layout.app_list_row, appList);
    //         }
    //         return null;
    //     }
    //
    //     /* access modifiers changed from: protected */
    //     @DexIgnore
    //     public void onPostExecute(Void result) {
    //         ListView listView = (ListView) AppNotificationsActivity.this.findViewById(R.id.app_list);
    //         if (listView != null) {
    //             listView.setAdapter(AppNotificationsActivity.this.listAdaptor);
    //         }
    //         super.onPostExecute(result);
    //     }
    // }

    @DexIgnore
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.app_notifications);
        new BaseToolbarActivity.ToolbarBuilder().title((int) R.string.notification_glances).build();
        this.packageManager = getPackageManager();
        this.sharedPrefs = SettingsUtils.getSharedPreferences();
        ListView appList = (ListView) findViewById(R.id.app_list);
        if (appList != null) {
            appList.setEmptyView(findViewById(R.id.app_list_empty_view));
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        // new AppNotificationsActivity.LoadApplications().execute(new Void[0]);
        // Tracker.tagScreen(TrackerConstants.Screen.NOTIFICATION_GLANCES);
    }

    @DexIgnore
    private List<ApplicationInfo> removeAppsThatWeDontWant(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> appList = new ArrayList<>();
        for (ApplicationInfo info : list) {
            try {
                if (this.packageManager.getLaunchIntentForPackage(info.packageName) != null && !StringUtils.equalsOrBothEmptyAfterTrim(getPackageName(), info.packageName) && !GlanceUtils.isWhiteListedApp(info.packageName)) {
                    appList.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return appList;
    }
}
