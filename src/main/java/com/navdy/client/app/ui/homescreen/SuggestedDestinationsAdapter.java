package com.navdy.client.app.ui.homescreen;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.RecycledViewPool;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;

import com.navdy.client.R.color;
import com.navdy.client.R.dimen;
import com.navdy.client.R.drawable;
import com.navdy.client.R.id;
import com.navdy.client.R.layout;
import com.navdy.client.R.string;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Suggestion.SuggestionType;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.util.CustomItemClickListener;
import com.navdy.client.app.framework.util.CustomSuggestionsLongItemClickListener;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.ui.customviews.ActiveTripCardView;
import com.navdy.client.app.ui.customviews.PendingTripCardView;
import com.navdy.service.library.log.Logger;

import java.util.ArrayList;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class SuggestedDestinationsAdapter extends Adapter<ViewHolder> implements OnLongClickListener, NavdyRouteListener {
    @DexIgnore
    private static /* final */ float DESELECTED_SUGGESTION_ELEVATION; // = 0.0f;
    @DexIgnore
    private static /* final */ float SELECTED_SUGGESTION_ELEVATION; // = 8.0f;
    @DexIgnore
    public static /* final */ int SUGGESTION_LIST_MAX_SIZE; // = 20;
    @DexIgnore
    private static /* final */ boolean VERBOSE; // = false;
    @DexIgnore
    private static /* final */ Logger logger; // = new com.navdy.service.library.log.Logger(com.navdy.client.app.ui.homescreen.SuggestedDestinationsAdapter.class);
    @DexIgnore
    private ActiveTripCardView activeTripCardView;
    @DexIgnore
    private /* final */ Context context;
    @DexIgnore
    int googleMapHeight; // = -1;
    @DexIgnore
    int hereMapHeight; // = -1;
    @DexIgnore
    private CustomItemClickListener listener;
    @DexIgnore
    private CustomSuggestionsLongItemClickListener longClickListener;
    @DexIgnore
    private /* final */ NavdyRouteHandler navdyRouteHandler;
    @DexIgnore
    int offlineBannerHeight; // = -1;
    @DexIgnore
    private PendingTripCardView pendingTripCardView;
    @DexIgnore
    private RecyclerView recyclerView;
    @DexIgnore
    private View selectedItem;
    @DexIgnore
    private int selectedItemPosition; // = -1;
    @DexIgnore
    private SuggestionType selectedSuggestionType;
    // @android.support.annotation.NonNull
    @DexIgnore
    private ArrayList<Suggestion> suggestions; // = new java.util.ArrayList<>(20);

    @DexIgnore
    class Anon1 implements OnClickListener {
        @DexIgnore
        Anon1() {
        }

        @DexIgnore
        /* final */ /* synthetic */ ViewHolder val$viewHolder;

        @DexIgnore
        Anon1(ViewHolder viewHolder) {
            this.val$viewHolder = viewHolder;
        }

        @DexIgnore
        public void onClick(View v) {
            if (SuggestedDestinationsAdapter.this.selectedItemPosition < 0) {
                SuggestedDestinationsAdapter.this.listener.onClick(this.val$viewHolder.itemView, this.val$viewHolder.getAdapterPosition());
            }
        }
    }

    @DexIgnore
    class Anon2 implements OnClickListener {
        @DexIgnore
        Anon2() {
        }

        @DexIgnore
        /* final */ /* synthetic */ ActiveTripViewHolder val$activeTripViewHolder;

        @DexIgnore
        Anon2(ActiveTripViewHolder activeTripViewHolder) {
            this.val$activeTripViewHolder = activeTripViewHolder;
        }

        @DexIgnore
        public void onClick(View view) {
            SuggestedDestinationsAdapter.this.listener.onClick(view, this.val$activeTripViewHolder.getAdapterPosition());
        }
    }

    @DexIgnore
    class Anon3 implements OnClickListener {
        @DexIgnore
        Anon3() {
        }

        @DexIgnore
        /* final */ /* synthetic */ ActiveTripViewHolder val$activeTripViewHolder;
        @DexIgnore
        /* final */ /* synthetic */ Suggestion val$suggestion;

        @DexIgnore
        Anon3(ActiveTripViewHolder activeTripViewHolder, Suggestion suggestion) {
            this.val$activeTripViewHolder = activeTripViewHolder;
            this.val$suggestion = suggestion;
        }

        @DexIgnore
        public void onClick(View view) {
            int adapterPosition = this.val$activeTripViewHolder.getAdapterPosition();
            if (SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                SuggestedDestinationsAdapter.this.triggerLongClick(true, this.val$activeTripViewHolder.itemView, adapterPosition, this.val$suggestion);
            } else if (SuggestedDestinationsAdapter.this.listener != null) {
                SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
            }
        }
    }

    @DexIgnore
    class Anon4 implements OnLongClickListener {
        @DexIgnore
        Anon4() {
        }

        @DexIgnore
        /* final */ /* synthetic */ ActiveTripViewHolder val$activeTripViewHolder;
        @DexIgnore
        /* final */ /* synthetic */ Suggestion val$suggestion;

        @DexIgnore
        Anon4(ActiveTripViewHolder activeTripViewHolder, Suggestion suggestion) {
            this.val$activeTripViewHolder = activeTripViewHolder;
            this.val$suggestion = suggestion;
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            SuggestedDestinationsAdapter.this.longClickListener.onItemLongClick(view, this.val$activeTripViewHolder.getAdapterPosition(), this.val$suggestion);
            return true;
        }
    }

    @DexIgnore
    class Anon5 implements OnClickListener {
        @DexIgnore
        Anon5() {
        }

        @DexIgnore
        /* final */ /* synthetic */ PendingTripViewHolder val$pendingTripViewHolder;
        @DexIgnore
        /* final */ /* synthetic */ Suggestion val$suggestion;

        @DexIgnore
        Anon5(PendingTripViewHolder pendingTripViewHolder, Suggestion suggestion) {
            this.val$pendingTripViewHolder = pendingTripViewHolder;
            this.val$suggestion = suggestion;
        }

        @DexIgnore
        public void onClick(View view) {
            int adapterPosition = this.val$pendingTripViewHolder.getAdapterPosition();
            if (SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                SuggestedDestinationsAdapter.this.triggerLongClick(true, this.val$pendingTripViewHolder.itemView, adapterPosition, this.val$suggestion);
            } else if (SuggestedDestinationsAdapter.this.listener != null) {
                SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
            }
        }
    }

    @DexIgnore
    class Anon6 implements OnLongClickListener {
        @DexIgnore
        Anon6() {
        }

        @DexIgnore
        /* final */ /* synthetic */ PendingTripViewHolder val$pendingTripViewHolder;
        @DexIgnore
        /* final */ /* synthetic */ Suggestion val$suggestion;

        @DexIgnore
        Anon6(PendingTripViewHolder pendingTripViewHolder, Suggestion suggestion) {
            this.val$pendingTripViewHolder = pendingTripViewHolder;
            this.val$suggestion = suggestion;
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            SuggestedDestinationsAdapter.this.longClickListener.onItemLongClick(view, this.val$pendingTripViewHolder.getAdapterPosition(), this.val$suggestion);
            return true;
        }
    }

    @DexIgnore
    class Anon7 implements OnClickListener {
        @DexIgnore
        Anon7() {
        }

        @DexIgnore
        /* final */ /* synthetic */ View val$background;
        @DexIgnore
        /* final */ /* synthetic */ boolean val$shouldSendToHud;
        @DexIgnore
        /* final */ /* synthetic */ Suggestion val$suggestion;
        @DexIgnore
        /* final */ /* synthetic */ SuggestionsViewHolder val$viewHolder;

        @DexIgnore
        Anon7(SuggestionsViewHolder suggestionsViewHolder, boolean z, View view, Suggestion suggestion) {
            this.val$viewHolder = suggestionsViewHolder;
            this.val$shouldSendToHud = z;
            this.val$background = view;
            this.val$suggestion = suggestion;
        }

        @DexIgnore
        public void onClick(View view) {
            int adapterPosition = this.val$viewHolder.getAdapterPosition();
            if (SuggestedDestinationsAdapter.this.selectedItemPosition >= 0) {
                SuggestedDestinationsAdapter.this.triggerLongClick(this.val$shouldSendToHud, this.val$background, adapterPosition, this.val$suggestion);
            } else if (SuggestedDestinationsAdapter.this.listener != null) {
                SuggestedDestinationsAdapter.this.listener.onClick(view, adapterPosition);
            }
        }
    }

    @DexIgnore
    class Anon8 implements OnLongClickListener {
        @DexIgnore
        Anon8() {
        }

        @DexIgnore
        /* final */ /* synthetic */ View val$background;
        @DexIgnore
        /* final */ /* synthetic */ boolean val$shouldSendToHud;
        @DexIgnore
        /* final */ /* synthetic */ Suggestion val$suggestion;
        @DexIgnore
        /* final */ /* synthetic */ SuggestionsViewHolder val$viewHolder;

        @DexIgnore
        Anon8(boolean z, View view, SuggestionsViewHolder suggestionsViewHolder, Suggestion suggestion) {
            this.val$shouldSendToHud = z;
            this.val$background = view;
            this.val$viewHolder = suggestionsViewHolder;
            this.val$suggestion = suggestion;
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            return SuggestedDestinationsAdapter.this.triggerLongClick(this.val$shouldSendToHud, this.val$background, this.val$viewHolder.getAdapterPosition(), this.val$suggestion);
        }
    }

    SuggestedDestinationsAdapter(Context context2, int googleMapHeight2, int hereMapHeight2, int offlineBannerHeight2) {
        this.context = context2;
        this.navdyRouteHandler = NavdyRouteHandler.getInstance();
        this.googleMapHeight = googleMapHeight2;
        this.hereMapHeight = hereMapHeight2;
        this.offlineBannerHeight = offlineBannerHeight2;
        rebuildSuggestions();
    }

    @DexIgnore
    int getGoogleMapHeight() {
        return this.googleMapHeight - (AppInstance.getInstance().canReachInternet() ? 0 : this.offlineBannerHeight);
    }

    @DexIgnore
    int getHereMapHeight() {
        return this.hereMapHeight - (AppInstance.getInstance().canReachInternet() ? 0 : this.offlineBannerHeight);
    }

    // @android.support.annotation.UiThread
    @DexIgnore
    void onSuggestionBuildComplete(ArrayList<Suggestion> suggestions2) {
        this.suggestions = suggestions2;
        notifyDataSetChanged();
    }

    @MainThread
    @DexIgnore
    void rebuildSuggestions() {
        stopRefreshingAllEtas();
        SystemUtils.ensureOnMainThread();
        this.selectedItem = null;
        this.selectedItemPosition = -1;
        this.selectedSuggestionType = null;
        this.suggestions = SuggestionManager.getEmptySuggestionState();
        notifyDataSetChanged();
        SuggestionManager.rebuildSuggestionListAndSendToHudAsync();
        if (this.pendingTripCardView != null) {
            this.pendingTripCardView.setNameAddressAndIcon(NavdyRouteHandler.getInstance().getCurrentDestination());
        }
    }

    @DexIgnore
    void setClickListener(CustomItemClickListener onClickListener) {
        this.listener = onClickListener;
    }

    @DexIgnore
    void setSuggestionLongClickListener(CustomSuggestionsLongItemClickListener onLongClickListener) {
        this.longClickListener = onLongClickListener;
    }

    @MainThread
    @DexIgnore
    public Suggestion getItem(int position) {
        SystemUtils.ensureOnMainThread();
        int position2 = position - 1;
        if (position2 < 0 || position2 >= this.suggestions.size()) {
            return null;
        }
        return this.suggestions.get(position2);
    }

    @DexIgnore
    void setSelectedSuggestion(View selectedCardRow, SuggestionType suggestionType, int actionModePosition) {
        if (this.selectedItem != null) {
            setViewToNormalMode(this.selectedItem, this.selectedSuggestionType);
        }
        this.selectedItem = selectedCardRow;
        this.selectedItemPosition = actionModePosition;
        this.selectedSuggestionType = suggestionType;
        if (selectedCardRow != null) {
            setViewToSelectedMode(selectedCardRow, suggestionType);
        }
    }

    @DexIgnore
    void endSelectionMode() {
        setViewToNormalMode(this.selectedItem, this.selectedSuggestionType);
        this.selectedItemPosition = -1;
        this.selectedItem = null;
        this.selectedSuggestionType = null;
    }

    @DexIgnore
    void clickPendingTripCard(Context context2) {
        if (this.pendingTripCardView != null) {
            this.pendingTripCardView.handleOnClick(context2);
        }
    }

    @DexIgnore
    void clickActiveTripCard() {
        if (this.activeTripCardView != null) {
            this.activeTripCardView.handleOnClick();
        }
    }

    @DexIgnore
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView2) {
        super.onAttachedToRecyclerView(recyclerView2);
        this.recyclerView = recyclerView2;
        this.navdyRouteHandler.addListener(this);
    }

    @DexIgnore
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView2) {
        this.navdyRouteHandler.removeListener(this);
        clearAllAnimations();
        stopRefreshingAllEtas();
        this.recyclerView = null;
        super.onDetachedFromRecyclerView(recyclerView2);
    }

    @DexIgnore
    public void onViewRecycled(@NonNull ViewHolder holder) {
        clearAnimationIfRunning(holder);
        super.onViewRecycled(holder);
    }

    @DexIgnore
    public boolean onFailedToRecycleView(@NonNull ViewHolder holder) {
        clearAnimationIfRunning(holder);
        return super.onFailedToRecycleView(holder);
    }

    @DexReplace
    void clearAllAnimations() {
        logger.d("Clearing all animations in the suggestion adapter.");
        if (this.recyclerView == null) {
            return;
        }
        int count = this.recyclerView.getChildCount();
        for (int i = 0; i < count; i++) {
            try {
                clearAnimationIfRunning(this.recyclerView.findViewHolderForAdapterPosition(i));
            } catch (Throwable t) {
                logger.e("Unable to clear animations", t);
            }
        }
        RecycledViewPool pool = this.recyclerView.getRecycledViewPool();
        while (true) {
            ViewHolder holder = pool.getRecycledView(SuggestionType.LOADING.ordinal());
            if (holder != null) {
                clearAnimationIfRunning(holder);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    void stopRefreshingAllEtas() {
        logger.d("Removing all ETA refreshers");
        RoutedSuggestionsViewHolder.handler.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    private void clearAnimationIfRunning(ViewHolder holder) {
        if ((holder instanceof ContentLoadingViewHolder)) {
            ((ContentLoadingViewHolder) holder).stopAnimation();
        }
    }

    @DexIgnore
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SuggestionType.LOADING.ordinal()) {
            return new ContentLoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout.list_item_loading, parent, false));
        }
        if (viewType == SuggestionType.GOOGLE_HEADER.ordinal()) {
            View v = LayoutInflater.from(parent.getContext()).inflate(layout.google_map_header, parent, false);
            v.setLayoutParams(new LayoutParams(-1, getGoogleMapHeight()));
            return new SuggestionsViewHolder(v);
        } else if (viewType == SuggestionType.HERE_HEADER.ordinal()) {
            View v2 = LayoutInflater.from(parent.getContext()).inflate(layout.here_map_header, parent, false);
            v2.setLayoutParams(new LayoutParams(-1, getHereMapHeight()));
            return new SuggestionsViewHolder(v2);
        } else if (viewType == SuggestionType.GOOGLE_FOOTER.ordinal() || viewType == SuggestionType.HERE_FOOTER.ordinal()) {
            View v3 = LayoutInflater.from(parent.getContext()).inflate(layout.suggestion_footer, parent, false);
            int footerHeight = getGoogleMapHeight();
            if (viewType == SuggestionType.HERE_FOOTER.ordinal()) {
                footerHeight = getHereMapHeight();
            }
            Resources resources = this.context.getResources();
            if (resources != null) {
                footerHeight += resources.getDimensionPixelSize(dimen.suggestion_list_top_height);
            }
            v3.setLayoutParams(new LayoutParams(-1, footerHeight));
            return new SectionHeaderViewHolder(v3);
        } else if (viewType == SuggestionType.ACTIVE_TRIP.ordinal()) {
            if (this.activeTripCardView == null) {
                this.activeTripCardView = (ActiveTripCardView) LayoutInflater.from(parent.getContext()).inflate(layout.active_trip_card, parent, false);
            }
            ImageView chevron = this.activeTripCardView.findViewById(id.chevron);
            if (chevron != null) {
                chevron.setVisibility(View.VISIBLE);
            }
            return new ActiveTripViewHolder(this.activeTripCardView);
        } else if (viewType == SuggestionType.PENDING_TRIP.ordinal()) {
            if (this.pendingTripCardView == null) {
                this.pendingTripCardView = (PendingTripCardView) LayoutInflater.from(parent.getContext()).inflate(layout.pending_trip_card, parent, false);
            }
            logger.v("viewType for Pending Route found: " + viewType);
            return new PendingTripViewHolder(this.pendingTripCardView);
        } else if (viewType == SuggestionType.SECTION_HEADER.ordinal()) {
            return new SectionHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(layout.suggestion_section_header, parent, false));
        } else {
            View v4 = LayoutInflater.from(parent.getContext()).inflate(layout.list_item, parent, false);
            if (isRoutedSuggestion(viewType)) {
                return new RoutedSuggestionsViewHolder(v4);
            }
            return new SuggestionsViewHolder(v4);
        }
    }

    @DexIgnore
    private boolean isRoutedSuggestion(int viewType) {
        return viewType == SuggestionType.CALENDAR.ordinal() || viewType == SuggestionType.RECOMMENDATION.ordinal();
    }

    @MainThread
    @DexIgnore
    public void onBindViewHolder(ViewHolder viewHolder, int unreliablePosition) {
        SystemUtils.ensureOnMainThread();
        if (viewHolder == null || viewHolder.itemView == null) {
            logger.e("onBindViewHolder called on a null viewHolder !!!");
            return;
        }
        int suggestionPosition = unreliablePosition - 1;
        if (!isValidSuggestion(suggestionPosition) || isNotSuggestionType(viewHolder)) {
            if (viewHolder instanceof ContentLoadingViewHolder) {
                ((ContentLoadingViewHolder) viewHolder).startAnimation();
            }
            if (viewHolder.getItemViewType() == SuggestionType.GOOGLE_HEADER.ordinal()) {
                viewHolder.itemView.setLayoutParams(new LayoutParams(-1, getGoogleMapHeight()));
            }
            if (viewHolder.getItemViewType() == SuggestionType.HERE_HEADER.ordinal()) {
                viewHolder.itemView.setLayoutParams(new LayoutParams(-1, getHereMapHeight()));
            }
            if (this.listener != null) {
                viewHolder.itemView.setOnClickListener(new Anon1(viewHolder));
                return;
            }
            return;
        }
        Suggestion suggestion = this.suggestions.get(suggestionPosition);
        if (viewHolder instanceof ActiveTripViewHolder) {
            onBindActiveTrip((ActiveTripViewHolder) viewHolder, suggestion);
        } else if (viewHolder instanceof PendingTripViewHolder) {
            logger.v("pendingTripViewHolder found: " + suggestion.destination.name + " at adapter position: " + unreliablePosition);
            onBindPendingTrip((PendingTripViewHolder) viewHolder, suggestion);
        } else if (viewHolder instanceof SectionHeaderViewHolder) {
            onBindSectionHeader((SectionHeaderViewHolder) viewHolder, suggestion, suggestionPosition);
        } else if (viewHolder instanceof SuggestionsViewHolder) {
            SuggestionsViewHolder suggestionsViewHolder = (SuggestionsViewHolder) viewHolder;
            onBindSuggestion(suggestionsViewHolder, suggestion);
            if (viewHolder instanceof RoutedSuggestionsViewHolder) {
                ((RoutedSuggestionsViewHolder) viewHolder).onBindViewHolder(suggestion);
            } else {
                suggestionsViewHolder.subIllustrationText.setVisibility(View.GONE);
            }
        }
    }

    @MainThread
    @DexIgnore
    public int getItemCount() {
        SystemUtils.ensureOnMainThread();
        return this.suggestions.size() + 2;
    }

    @MainThread
    @DexIgnore
    public int getItemViewType(int position) {
        SystemUtils.ensureOnMainThread();
        if (position == 0) {
            if (firstItemIsActiveTrip() || firstItemIsPendingTrip()) {
                return SuggestionType.HERE_HEADER.ordinal();
            }
            return SuggestionType.GOOGLE_HEADER.ordinal();
        } else if (position != getItemCount() - 1) {
            int position2 = position - 1;
            if (isValidIndex(position2)) {
                return this.suggestions.get(position2).getType().ordinal();
            }
            return SuggestionType.RECENT.ordinal();
        } else if (firstItemIsActiveTrip() || firstItemIsPendingTrip()) {
            return SuggestionType.HERE_FOOTER.ordinal();
        } else {
            return SuggestionType.GOOGLE_FOOTER.ordinal();
        }
    }

    @DexIgnore
    public boolean onLongClick(View v) {
        return false;
    }

    @DexIgnore
    public void onPendingRouteCalculating(@NonNull Destination destination) {
        addPendingTripCard(destination);
    }

    @DexIgnore
    public void onPendingRouteCalculated(@NonNull NavdyRouteHandler.Error error, @NonNull NavdyRouteInfo pendingRoute) {
    }

    @DexIgnore
    public void onRouteCalculating(@NonNull Destination destination) {
    }

    @DexIgnore
    public void onRouteCalculated(@NonNull NavdyRouteHandler.Error error, @NonNull NavdyRouteInfo route) {
    }

    @DexIgnore
    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        addActiveTripCard(route.getDestination());
    }

    @DexIgnore
    public void onTripProgress(@NonNull NavdyRouteInfo progress) {
    }

    @DexIgnore
    public void onReroute() {
    }

    @DexIgnore
    public void onRouteArrived(@NonNull Destination destination) {
    }

    @DexIgnore
    public void onStopRoute() {
        removeTripCard();
    }

    @MainThread
    @DexIgnore
    private boolean isValidIndex(int position) {
        SystemUtils.ensureOnMainThread();
        return position >= 0 && !this.suggestions.isEmpty() && position < this.suggestions.size() && this.suggestions.get(position) != null;
    }

    @DexIgnore
    private boolean isNotSuggestionType(ViewHolder viewHolder) {
        return !(viewHolder instanceof SuggestionsViewHolder) && !(viewHolder instanceof ActiveTripViewHolder) && !(viewHolder instanceof PendingTripViewHolder) && !(viewHolder instanceof SectionHeaderViewHolder);
    }

    @MainThread
    @DexIgnore
    private boolean isValidSuggestion(int suggestionPosition) {
        SystemUtils.ensureOnMainThread();
        return isValidIndex(suggestionPosition) && this.suggestions.get(suggestionPosition).destination != null;
    }

    @DexIgnore
    private void onBindActiveTrip(ActiveTripViewHolder activeTripViewHolder, Suggestion suggestion) {
        activeTripViewHolder.itemView.setOnClickListener(new Anon2(activeTripViewHolder));
        if (activeTripViewHolder.getAdapterPosition() == this.selectedItemPosition) {
            setViewToSelectedMode(activeTripViewHolder.itemView, SuggestionType.ACTIVE_TRIP);
        } else {
            setViewToNormalMode(activeTripViewHolder.itemView, SuggestionType.ACTIVE_TRIP);
        }
        activeTripViewHolder.itemView.setOnClickListener(new Anon3(activeTripViewHolder, suggestion));
        if (this.longClickListener != null) {
            activeTripViewHolder.itemView.setOnLongClickListener(new Anon4(activeTripViewHolder, suggestion));
        }
    }

    @DexIgnore
    private void onBindPendingTrip(PendingTripViewHolder pendingTripViewHolder, Suggestion suggestion) {
        if (suggestion != null && suggestion.destination != null) {
            if (suggestion.isPendingTrip()) {
                if (pendingTripViewHolder.getAdapterPosition() == this.selectedItemPosition) {
                    setViewToSelectedMode(pendingTripViewHolder.itemView, SuggestionType.PENDING_TRIP);
                } else {
                    setViewToNormalMode(pendingTripViewHolder.itemView, SuggestionType.PENDING_TRIP);
                }
            }
            pendingTripViewHolder.itemView.setOnClickListener(new Anon5(pendingTripViewHolder, suggestion));
            if (this.longClickListener != null) {
                pendingTripViewHolder.itemView.setOnLongClickListener(new Anon6(pendingTripViewHolder, suggestion));
            }
        }
    }

    @DexIgnore
    private void onBindSectionHeader(SectionHeaderViewHolder sectionHeaderViewHolder, Suggestion suggestion, int suggestionPosition) {
        if (!(sectionHeaderViewHolder.title == null || suggestion == null || suggestion.destination == null)) {
            sectionHeaderViewHolder.title.setText(suggestion.destination.name);
        }
        if (suggestionPosition == 0) {
            sectionHeaderViewHolder.chevron.setVisibility(View.VISIBLE);
        } else {
            sectionHeaderViewHolder.chevron.setVisibility(View.GONE);
        }
    }

    @DexIgnore
    private void onBindSuggestion(SuggestionsViewHolder suggestionViewHolder, Suggestion suggestion) {
        onBindSuggestionIconIllustration(suggestion, suggestionViewHolder);
        if (suggestion.shouldShowRightChevron()) {
            suggestionViewHolder.rightChevron.setVisibility(View.VISIBLE);
        } else {
            suggestionViewHolder.rightChevron.setVisibility(View.GONE);
        }
        suggestionViewHolder.infoButton.setImageResource(drawable.ic_info_outline_grey_500);
        onBindSuggestionNameAndAddress(suggestion, suggestionViewHolder);
        boolean shouldSendToHud = suggestion.shouldSendToHud();
        suggestionViewHolder.separator.setVisibility(View.VISIBLE);
        onBindSuggestionClickListeners(suggestionViewHolder, suggestion, suggestionViewHolder, shouldSendToHud);
    }

    @DexIgnore
    private void onBindSuggestionIconIllustration(Suggestion suggestion, SuggestionsViewHolder suggestionViewHolder) {
        boolean isSelected;
        isSelected = suggestionViewHolder.getAdapterPosition() == this.selectedItemPosition;
        int badgeAsset = suggestion.getBadgeAsset(isSelected);
        if (badgeAsset > 0) {
            suggestionViewHolder.icon.setImageResource(badgeAsset);
        } else {
            suggestionViewHolder.icon.setImageBitmap(null);
        }
        setBackground(suggestionViewHolder.background, suggestion.getType(), false);
    }

    @DexIgnore
    private void onBindSuggestionNameAndAddress(Suggestion suggestion, SuggestionsViewHolder suggestionViewHolder) {
        if (!suggestion.hasInfoButton()) {
            suggestionViewHolder.infoButton.setVisibility(View.INVISIBLE);
        }
        suggestionViewHolder.icon.setVisibility(View.VISIBLE);
        if (suggestion.destination != null) {
            if (suggestion.isTip()) {
                suggestionViewHolder.firstLine.setText(suggestion.destination.name);
                suggestionViewHolder.secondLine.setText(suggestion.destination.getAddressForDisplay());
            } else if (!suggestion.isCalendar() || suggestion.event == null || StringUtils.isEmptyAfterTrim(suggestion.event.displayName)) {
                Pair<String, String> splitAddress = suggestion.destination.getTitleAndSubtitle();
                suggestionViewHolder.firstLine.setText(splitAddress.first);
                suggestionViewHolder.secondLine.setText(splitAddress.second);
            } else {
                suggestionViewHolder.firstLine.setText(suggestion.event.displayName);
                suggestionViewHolder.secondLine.setText(suggestion.destination.getAddressForDisplay());
            }
            if (suggestion.isCalendar()) {
                suggestionViewHolder.thirdLine.setText(this.context.getString(string.event_times, suggestion.event.getStartTime(), suggestion.event.getEndTime()));
                suggestionViewHolder.thirdLine.setVisibility(View.VISIBLE);
                return;
            }
            suggestionViewHolder.thirdLine.setVisibility(View.GONE);
        }
    }

    @DexIgnore
    private void onBindSuggestionClickListeners(SuggestionsViewHolder viewHolder, Suggestion suggestion, SuggestionsViewHolder suggestionViewHolder, boolean shouldSendToHud) {
        View background = viewHolder.background;
        OnClickListener clickListener = new Anon7(viewHolder, shouldSendToHud, background, suggestion);
        suggestionViewHolder.row.setOnClickListener(clickListener);
        suggestionViewHolder.infoButton.setOnClickListener(clickListener);
        suggestionViewHolder.row.setOnLongClickListener(new Anon8(shouldSendToHud, background, viewHolder, suggestion));
    }

    @DexIgnore
    private boolean triggerLongClick(boolean shouldSendToHud, View background, int position, Suggestion suggestion) {
        if (this.longClickListener == null || (!shouldSendToHud && !suggestion.isPendingTrip())) {
            return false;
        }
        this.longClickListener.onItemLongClick(background, position, suggestion);
        return true;
    }

    @DexIgnore
    private void setBackground(View view, SuggestionType type, boolean selected) {
        if (view != null && type != SuggestionType.ACTIVE_TRIP) {
            if (selected) {
                view.setBackgroundColor(ContextCompat.getColor(NavdyApplication.getAppContext(), color.grey_light_card));
            } else {
                view.setBackgroundColor(ContextCompat.getColor(NavdyApplication.getAppContext(), color.white));
            }
        }
    }

    @DexIgnore
    private void setViewToSelectedMode(View view, SuggestionType suggestionType) {
        if (view != null) {
            setBackground(view, suggestionType, true);
            ImageView illustration = getIllustrationView(view);
            if (illustration != null) {
                illustration.setTag(illustration.getDrawable());
                illustration.setImageResource(drawable.icon_suggested_edit);
            }
            if (suggestionType != SuggestionType.PENDING_TRIP && suggestionType != SuggestionType.ACTIVE_TRIP) {
                ViewCompat.setElevation(view, SELECTED_SUGGESTION_ELEVATION);
            }
        }
    }

    @DexIgnore
    private void setViewToNormalMode(View view, SuggestionType suggestionType) {
        if (view != null) {
            setBackground(view, suggestionType, false);
            ImageView illustration = getIllustrationView(view);
            if (illustration != null) {
                illustration.setImageDrawable((Drawable) illustration.getTag());
            }
            ViewCompat.setElevation(view, 0.0f);
        }
    }

    @DexIgnore
    private ImageView getIllustrationView(View view) {
        if (view == null) {
            return null;
        }
        return (ImageView) view.findViewById(id.illustration);
    }

    @DexIgnore
    private void addActiveTripCard(Destination destination) {
        addTripCard(destination, SuggestionType.ACTIVE_TRIP);
    }

    @DexIgnore
    private void addPendingTripCard(Destination destination) {
        addTripCard(destination, SuggestionType.PENDING_TRIP);
    }

    @MainThread
    @DexIgnore
    private void addTripCard(Destination destination, SuggestionType suggestionType) {
        SystemUtils.ensureOnMainThread();
        SuggestionManager.setTripAsync(new Suggestion(destination, suggestionType));
    }

    @MainThread
    @DexIgnore
    private void removeTripCard() {
        SystemUtils.ensureOnMainThread();
        SuggestionManager.setTripAsync(null);
    }

    @DexIgnore
    private boolean firstItemIsActiveTrip() {
        return firstItemIs(SuggestionType.ACTIVE_TRIP);
    }

    @DexIgnore
    private boolean firstItemIsPendingTrip() {
        return firstItemIs(SuggestionType.PENDING_TRIP);
    }

    @MainThread
    @DexIgnore
    private boolean firstItemIs(SuggestionType suggestionType) {
        SystemUtils.ensureOnMainThread();
        return this.suggestions.size() > 0 && this.suggestions.get(0) != null && this.suggestions.get(0).getType() == suggestionType;
    }
}
