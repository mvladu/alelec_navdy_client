package com.navdy.service.library.device.link;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public class ProtobufLink implements com.navdy.service.library.device.link.Link {
    @DexIgnore
    private int bandwidthLevel; // = 1;
    @DexIgnore
    private com.navdy.service.library.device.connection.ConnectionInfo connectionInfo;
    @DexIgnore
    private com.navdy.service.library.device.link.ReaderThread readerThread;
    @DexIgnore
    private com.navdy.service.library.device.link.WriterThread writerThread;

    @DexIgnore
    public ProtobufLink(com.navdy.service.library.device.connection.ConnectionInfo connectionInfo2) {
        this.connectionInfo = connectionInfo2;
    }

    @DexReplace
    public boolean start(com.navdy.service.library.network.SocketAdapter socket, java.util.concurrent.LinkedBlockingDeque<com.navdy.service.library.device.link.EventRequest> outboundEventQueue, com.navdy.service.library.device.link.LinkListener listener) {
        if ((this.writerThread == null || this.writerThread.isClosing()) && (this.readerThread == null || this.readerThread.isClosing())) {
            try {
                this.readerThread = new com.navdy.service.library.device.link.ReaderThread(this.connectionInfo.getType(), socket, listener, true);
                this.writerThread = new com.navdy.service.library.device.link.WriterThread(outboundEventQueue, socket, null);
                this.readerThread.start();
                this.writerThread.start();
                return true;
            } catch (java.io.IOException e) {
                this.readerThread.cancel();
                this.writerThread.cancel();
                this.readerThread = null;
                this.writerThread = null;
                return false;
            }
        } else {
            throw new java.lang.IllegalStateException("Must stop threads before calling startLink");
        }
    }

    @DexIgnore
    public void setBandwidthLevel(int level) {
        this.bandwidthLevel = level;
    }

    @DexIgnore
    public int getBandWidthLevel() {
        return this.bandwidthLevel;
    }

    @DexIgnore
    public void close() {
        if (this.writerThread != null) {
            this.writerThread.cancel();
            this.writerThread = null;
        }
        if (this.readerThread != null) {
            this.readerThread.cancel();
            this.readerThread = null;
        }
    }
}
