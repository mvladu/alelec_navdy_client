package com.navdy.service.library.device.link;

import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.NavdyEventWriter;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.IOUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingDeque;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexReplace
public class WriterThread extends IOThread {
    private final WriterThread.EventProcessor mEventProcessor;
    private final LinkedBlockingDeque<EventRequest> mEventQueue;
    private final NavdyEventWriter mmEventWriter;
    private OutputStream mmOutStream;
    private SocketAdapter mSocket;

    @DexIgnore
    public interface EventProcessor {
        byte[] processEvent(byte[] bArr);
    }

    WriterThread(LinkedBlockingDeque<EventRequest> queue, SocketAdapter socket, WriterThread.EventProcessor processor) throws IOException {
        setName("WriterThread");
        this.logger.d("create WriterThread");
        this.mSocket = socket;
        this.mmOutStream = socket.getOutputStream();
        this.mmEventWriter = new com.navdy.service.library.events.NavdyEventWriter(this.mmOutStream);
        this.mEventQueue = queue;
        this.mEventProcessor = processor;
    }

    public void run() {
        this.logger.i("starting WriterThread loop.");
        while (mSocket.isConnected()) {
            if (this.closing) {
                break;
            }
            EventRequest request = null;
            try {
                request = this.mEventQueue.take();
            } catch (InterruptedException e) {
                this.logger.i("WriterThread interrupted");
            }
            if (this.closing) {
                break;
            } else if (request == null) {
                this.logger.e("WriterThread: unable to retrieve event from queue");
            } else {
                byte[] eventData = request.eventData;
                if (eventData.length > 524288) {
                    throw new RuntimeException("writer Max packet size exceeded [" + eventData.length + "] bytes[" + IOUtils.bytesToHexString(eventData, 0, 50) + "]");
                }
                if (this.mEventProcessor != null) {
                    eventData = this.mEventProcessor.processEvent(eventData);
                }
                if (eventData != null) {
                    try {
                        this.mmEventWriter.write(request.eventData);
                    } catch (IOException e2) {
                        if (!this.closing) {
                            this.logger.e("Error writing event:" + e2.getMessage());
                        }
                        this.logger.i("send failed");
                        request.callCompletionHandlers(RemoteDevice.PostEventStatus.SEND_FAILED);
                    }
                }
            }
        }
        this.logger.i("Writer thread ending");
    }

    public void cancel() {
        super.cancel();
        IOUtils.closeStream(this.mmOutStream);
        this.mmOutStream = null;
    }
}
