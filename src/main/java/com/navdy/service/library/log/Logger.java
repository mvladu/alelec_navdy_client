package com.navdy.service.library.log;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class Logger {
    @DexIgnore
    public static /* final */ java.lang.String ACTION_RELOAD; // = "com.navdy.service.library.log.action.RELOAD";
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static /* final */ java.lang.String DEFAULT_TAG; // = "Navdy";
    @DexIgnore
    private static volatile long logLevelChange; // = -1;
    @DexIgnore
    private static com.navdy.service.library.log.LogAppender[] sAppenders; // = new com.navdy.service.library.log.LogAppender[0];
    @DexIgnore
    private volatile boolean[] loggable;
    @DexIgnore
    private long startTime;
    @DexIgnore
    private /* final */ java.lang.String tagName;
    @DexIgnore
    private volatile long timestamp; // = -2;

    @DexIgnore
    public static void reloadLogLevels() {
        logLevelChange = java.lang.System.currentTimeMillis();
    }

    @DexIgnore
    public static void init(com.navdy.service.library.log.LogAppender[] appenders) {
        if (appenders == null || appenders.length == 0) {
            throw new java.lang.IllegalArgumentException();
        }
        sAppenders = (com.navdy.service.library.log.LogAppender[]) appenders.clone();
    }

    @DexIgnore
    public static synchronized void addAppender(com.navdy.service.library.log.LogAppender appender) {
        synchronized (com.navdy.service.library.log.Logger.class) {
            if (appender == null) {
                throw new java.lang.IllegalArgumentException();
            }
            com.navdy.service.library.log.LogAppender[] appenders = new com.navdy.service.library.log.LogAppender[(sAppenders.length + 1)];
            for (int i = 0; i < appenders.length; i++) {
                if (i < appenders.length - 1) {
                    appenders[i] = sAppenders[i];
                } else {
                    appenders[i] = appender;
                }
            }
            sAppenders = appenders;
        }
    }

    @DexIgnore
    public static void flush() {
        for (com.navdy.service.library.log.LogAppender flush : sAppenders) {
            flush.flush();
        }
    }

    @DexIgnore
    public static void close() {
        for (com.navdy.service.library.log.LogAppender close : sAppenders) {
            close.close();
        }
    }

    @DexIgnore
    public Logger(java.lang.String tagName2) {
        if (tagName2 != null) {
            this.tagName = tagName2.substring(0, java.lang.Math.min(tagName2.length(), 23));
        } else {
            this.tagName = "Navdy";
        }
    }

    @DexIgnore
    public Logger(java.lang.Class clazz) {
        if (clazz != null) {
            java.lang.String name = clazz.getSimpleName();
            this.tagName = name.substring(0, java.lang.Math.min(name.length(), 23));
            return;
        }
        this.tagName = "Navdy";
    }

    @DexIgnore
    public boolean isLoggable(int level) {
        return false;
    }

    @DexReplace
    public void v(java.lang.String msg) {
        for (com.navdy.service.library.log.LogAppender w : sAppenders) {
            if (w instanceof LogcatAppender) {
                w.v(this.tagName, msg);
            }
        }
    }

    @DexReplace
    public void v(java.lang.String msg, java.lang.Throwable tr) {
        for (com.navdy.service.library.log.LogAppender w : sAppenders) {
            if (w instanceof LogcatAppender) {
                w.v(this.tagName, msg, tr);
            }
        }
    }

    @DexIgnore
    public void d(java.lang.String msg) {
    }

    @DexIgnore
    public void d(java.lang.String msg, java.lang.Throwable tr) {
    }

    @DexIgnore
    public void i(java.lang.String msg) {
    }

    @DexIgnore
    public void i(java.lang.String msg, java.lang.Throwable tr) {
    }

    @DexIgnore
    public void w(java.lang.String msg) {
        for (com.navdy.service.library.log.LogAppender w : sAppenders) {
            w.w(this.tagName, msg);
        }
    }

    @DexIgnore
    public void w(java.lang.String msg, java.lang.Throwable tr) {
        for (com.navdy.service.library.log.LogAppender w : sAppenders) {
            w.w(this.tagName, msg, tr);
        }
    }

    @DexIgnore
    public void e(java.lang.String msg) {
        for (com.navdy.service.library.log.LogAppender e : sAppenders) {
            e.e(this.tagName, msg);
        }
    }

    @DexIgnore
    public void e(java.lang.String msg, java.lang.Throwable tr) {
        for (com.navdy.service.library.log.LogAppender e : sAppenders) {
            e.e(this.tagName, msg, tr);
        }
    }

    @DexIgnore
    public void e(java.lang.Throwable tr) {
        e("", tr);
    }

    @DexIgnore
    public void w(java.lang.Throwable tr) {
        w("", tr);
    }

    @DexIgnore
    public void d(java.lang.Throwable tr) {
        d("", tr);
    }

    @DexIgnore
    public void v(java.lang.Throwable tr) {
        v("", tr);
    }

    @DexIgnore
    public void i(java.lang.Throwable tr) {
        i("", tr);
    }

    @DexIgnore
    public void recordStartTime() {
        this.startTime = java.lang.System.currentTimeMillis();
    }

    @DexIgnore
    public void logTimeTaken(java.lang.String message) {
        d(message + " " + (java.lang.System.currentTimeMillis() - this.startTime) + " ms");
    }
}
