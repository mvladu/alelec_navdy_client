package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.ProtoEnum;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicEvent extends Message {
    @DexIgnore
    public static /* final */ Action DEFAULT_ACTION; // = com.navdy.service.library.events.audio.MusicEvent.Action.MUSIC_ACTION_PLAY;
    @DexIgnore
    public static /* final */ String DEFAULT_COLLECTIONID; // = "";
    @DexIgnore
    public static /* final */ MusicCollectionSource DEFAULT_COLLECTIONSOURCE; // = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    @DexIgnore
    public static /* final */ MusicCollectionType DEFAULT_COLLECTIONTYPE; // = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    @DexIgnore
    public static /* final */ MusicDataSource DEFAULT_DATASOURCE; // = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
    @DexIgnore
    public static /* final */ Integer DEFAULT_INDEX; // = java.lang.Integer.valueOf(0);
    @DexIgnore
    public static /* final */ MusicRepeatMode DEFAULT_REPEATMODE; // = com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    @DexIgnore
    public static /* final */ MusicShuffleMode DEFAULT_SHUFFLEMODE; // = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
    @DexIgnore
    private static /* final */ long serialVersionUID; // = 0;
    // @com.squareup.wire.ProtoField(label = com.squareup.wire.Message.Label.REQUIRED, tag = 1, type = com.squareup.wire.Message.Datatype.ENUM)
    @DexIgnore
    public /* final */ Action action;
    // @com.squareup.wire.ProtoField(tag = 13, type = com.squareup.wire.Message.Datatype.STRING)
    @DexIgnore
    public /* final */ String collectionId;
    // @com.squareup.wire.ProtoField(tag = 14, type = com.squareup.wire.Message.Datatype.ENUM)
    @DexIgnore
    public /* final */ MusicCollectionSource collectionSource;
    // @com.squareup.wire.ProtoField(tag = 15, type = com.squareup.wire.Message.Datatype.ENUM)
    @DexIgnore
    public /* final */ MusicCollectionType collectionType;
    // @com.squareup.wire.ProtoField(tag = 2, type = com.squareup.wire.Message.Datatype.ENUM)
    @DexIgnore
    public /* final */ MusicDataSource dataSource;
    // @com.squareup.wire.ProtoField(tag = 16, type = com.squareup.wire.Message.Datatype.INT32)
    @DexIgnore
    public /* final */ Integer index;
    // @com.squareup.wire.ProtoField(tag = 18, type = com.squareup.wire.Message.Datatype.ENUM)
    @DexIgnore
    public /* final */ MusicRepeatMode repeatMode;
    // @com.squareup.wire.ProtoField(tag = 17, type = com.squareup.wire.Message.Datatype.ENUM)
    @DexIgnore
    public /* final */ MusicShuffleMode shuffleMode;

    @DexIgnore
    public enum Action implements ProtoEnum {
        MUSIC_ACTION_PLAY(1),
        MUSIC_ACTION_PAUSE(2),
        MUSIC_ACTION_NEXT(3),
        MUSIC_ACTION_PREVIOUS(4),
        MUSIC_ACTION_REWIND_START(5),
        MUSIC_ACTION_FAST_FORWARD_START(6),
        MUSIC_ACTION_REWIND_STOP(7),
        MUSIC_ACTION_FAST_FORWARD_STOP(8),
        MUSIC_ACTION_MODE_CHANGE(9);
        
        @DexIgnore
        private final int value;

        @DexIgnore
        Action(int value2) {
            this.value = value2;
        }

        @DexIgnore
        public int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public static final class Builder extends Message.Builder<MusicEvent> {
        @DexIgnore
        public Action action;
        @DexIgnore
        public String collectionId;
        @DexIgnore
        public MusicCollectionSource collectionSource;
        @DexIgnore
        public MusicCollectionType collectionType;
        @DexIgnore
        public MusicDataSource dataSource;
        @DexIgnore
        public Integer index;
        @DexIgnore
        public MusicRepeatMode repeatMode;
        @DexIgnore
        public MusicShuffleMode shuffleMode;

        @DexIgnore
        public Builder() {
        }

        @DexIgnore
        public Builder(MusicEvent message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.dataSource = message.dataSource;
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
                this.index = message.index;
                this.shuffleMode = message.shuffleMode;
                this.repeatMode = message.repeatMode;
            }
        }

        @DexIgnore
        public Builder action(Action action2) {
            this.action = action2;
            return this;
        }

        @DexIgnore
        public Builder dataSource(MusicDataSource dataSource2) {
            this.dataSource = dataSource2;
            return this;
        }

        @DexIgnore
        public Builder collectionSource(MusicCollectionSource collectionSource2) {
            this.collectionSource = collectionSource2;
            return this;
        }

        @DexIgnore
        public Builder collectionType(MusicCollectionType collectionType2) {
            this.collectionType = collectionType2;
            return this;
        }

        @DexIgnore
        public Builder collectionId(String collectionId2) {
            this.collectionId = collectionId2;
            return this;
        }

        @DexIgnore
        public Builder index(Integer index2) {
            this.index = index2;
            return this;
        }

        @DexIgnore
        public Builder shuffleMode(MusicShuffleMode shuffleMode2) {
            this.shuffleMode = shuffleMode2;
            return this;
        }

        @DexIgnore
        public Builder repeatMode(MusicRepeatMode repeatMode2) {
            this.repeatMode = repeatMode2;
            return this;
        }

        @DexIgnore
        public MusicEvent build() {
            checkRequiredFields();
            return new MusicEvent(this);
        }
    }

    @DexIgnore
    public MusicEvent(Action action2, MusicDataSource dataSource2, MusicCollectionSource collectionSource2, MusicCollectionType collectionType2, String collectionId2, Integer index2, MusicShuffleMode shuffleMode2, MusicRepeatMode repeatMode2) {
        this.action = action2;
        this.dataSource = dataSource2;
        this.collectionSource = collectionSource2;
        this.collectionType = collectionType2;
        this.collectionId = collectionId2;
        this.index = index2;
        this.shuffleMode = shuffleMode2;
        this.repeatMode = repeatMode2;
    }

    @DexIgnore
    private MusicEvent(Builder builder) {
        this(builder.action, builder.dataSource, builder.collectionSource, builder.collectionType, builder.collectionId, builder.index, builder.shuffleMode, builder.repeatMode);
        setBuilder(builder);
    }

    @DexIgnore
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicEvent)) {
            return false;
        }
        MusicEvent o = (MusicEvent) other;
        return equals(this.action, o.action) && equals(this.dataSource, o.dataSource) && equals(this.collectionSource, o.collectionSource) && equals(this.collectionType, o.collectionType) && equals(this.collectionId, o.collectionId) && equals(this.index, o.index) && equals(this.shuffleMode, o.shuffleMode) && equals(this.repeatMode, o.repeatMode);
    }

    @DexIgnore
    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.action != null ? this.action.hashCode() : 0) * 37;
        if (this.dataSource != null) {
            i = this.dataSource.hashCode();
        } else {
            i = 0;
        }
        int i8 = (hashCode + i) * 37;
        if (this.collectionSource != null) {
            i2 = this.collectionSource.hashCode();
        } else {
            i2 = 0;
        }
        int i9 = (i8 + i2) * 37;
        if (this.collectionType != null) {
            i3 = this.collectionType.hashCode();
        } else {
            i3 = 0;
        }
        int i10 = (i9 + i3) * 37;
        if (this.collectionId != null) {
            i4 = this.collectionId.hashCode();
        } else {
            i4 = 0;
        }
        int i11 = (i10 + i4) * 37;
        if (this.index != null) {
            i5 = this.index.hashCode();
        } else {
            i5 = 0;
        }
        int i12 = (i11 + i5) * 37;
        if (this.shuffleMode != null) {
            i6 = this.shuffleMode.hashCode();
        } else {
            i6 = 0;
        }
        int i13 = (i12 + i6) * 37;
        if (this.repeatMode != null) {
            i7 = this.repeatMode.hashCode();
        }
        int result2 = i13 + i7;
        this.hashCode = result2;
        return result2;
    }
}
